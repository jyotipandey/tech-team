<?php
function encrypt($text, $salt) { 
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
} 

function decrypt($text, $salt){ 
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
}

// Function to display Share Box
function displayShareBox($shareData){
	if(!$shareData['ssurl']){$turl=$shareData['surl'];}else{$turl=$shareData['ssurl'];}
	if(!$shareData['share_fb']){$shareData['share_fb']=0;}
	if(!$shareData['share_tw']){$shareData['share_tw']=0;}
	if(!$shareData['share_ln']){$shareData['share_ln']=0;}
	if(!$shareData['share_go']){$shareData['share_go']=0;}
	if(!$shareData['share_pt']){$shareData['share_pt']=0;}
	
	if($shareData['type']=='small'){
		?>
        <section class="sharebtn_container" style="float: left;">
     <div class="share-btn" ><a style="text-decoration:none;" href="https://twitter.com/intent/tweet?url=<?=$turl?>&original_referer=<?=$shareData['surl']?>&source=dogspot&text=<?=$shareData['stext']?>" target="_blank" onclick="return windowpop(this.href, 545, 433)">
      <span class="share-btn-action share-btn-tweet"><i class="fa fa-twitter"></i>Tweet</span></a>
      <span class="share-btn-count center s_count"><?=$shareData['share_tw']?></span>
    </div>
     <div class="share-btn">  <a style="text-decoration:none;" href="http://www.facebook.com/share.php?u=<?=$shareData['surl']?>" onClick="return fbs_click()" target="_blank" title="Share This on Facebook">
      <span class="share-btn-action  share-btn-share"><i class="fa fa-facebook"></i>Share</span></a>
      <span class="share-btn-count center s_count"><?=$shareData['share_fb']?></span>
    </div>
   <div class="share-btn">  <a style="text-decoration:none;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$shareData['surl']?>&title=<?=$shareData['stext']?>&summary=<?=$shareData['stext']?>&source="dogspot.in"  onclick="return windowpop(this.href, 545, 433)">
      <span class="share-btn-action share-btn-linkedin"><i class="fa fa-linkedin"></i>Linkedin</span></a>
      <span class="share-btn-count center s_count"><?=$shareData['share_ln']?></span>
    </div> 
    <div class="share-btn">  <a style="text-decoration:none;" href="https://plus.google.com/share?url=<?=$shareData['surl']?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
      <span class="share-btn-action share-btn-plus"><i class="fa fa-google-plus"></i>Google+</span></a>
      <span class="share-btn-count center s_count"><?=$shareData['share_go']?></span>
    </div>
  </section>
        <?
	}	
}
?>