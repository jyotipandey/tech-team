<?php 
include($DOCUMENT_ROOT."/constants.php");
require_once($DOCUMENT_ROOT.'/session-no.php'); 
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/banner1.php');
 
$title="Dog Show Result | Dog Event Results";
$keyword="Dog Show Result, Dog Event Results" ;
$desc="Get the result of every Dog Show listed here with event photos and other details" ;
 
    $alternate="https://www.dogspot.in/show-results/";
	$canonical="https://m.dogspot.in/show-results/";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='dog-events';

require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
?>
<link type="text/css" rel="stylesheet" href="/bootstrap/css/dog-show.css?v=3">
<div class="breadcrumbs">
  <div class="container">
    <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
      <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a href="/" itemprop="item"><span itemprop="name">Home</span></a>
        <meta itemprop="position" content="1" />
        </span> <span> / </span> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <span itemprop="item"> <span itemprop="name" class="active-bread">Dog Shows</span></span>
        <meta itemprop="position" content="2" />
        </span> </div>
    </div>
  </div>
</div>
<div class="container dog-show-result">
<div class="row">
<div class="col-md-6">
<h4>dog shows in india</h4>
       <select  onchange="document.location.href=this.value" >  
       <?php require_once($DOCUMENT_ROOT.'/new/dog-events/dogshows.php'); ?></select>
       
</div>
<div class="col-md-6">
<h4>dog show photos and results</h4>
  <?  //showphotoofweek(4);?>  
      <a href="https://www.dogspot.in/show-results/"><img src="/images/dog-show-results.jpg" width="458" height="71" alt="Dog Show Photos and Results" class="img-responsive" style="width:100%;"/></a>
</div>

</div>

</div>

<div class="container show-result-list">

<div class="row">
<div class="col-md-8 col-sm-12">
<ul>
      <?
$selectShow = query_execute("SELECT * FROM show_description  ORDER BY date DESC");
 if(!$selectShow){	die(mysql_error());	}
	
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$album_nicename='';$ring_id='';
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT * FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 if($show_nicename!='122-123-sikc-breed-championship-dog-show'){
	 
	 if($ring_id || $album_nicename){ 
?>  
      
        <li>
        <h2><?=$show_name?></h2>
        <? if($album_nicename){?><a href="/photos/album/<?=$album_nicename?>/" target="_blank"  title="<?=$show_name?> Dog Show Photos" style="text-decoration:underline;">Photos</a> | <? } if($ring_id ){?><a href="/<?=$show_nicename?>/"  title="<?=$show_name?> Dog Show Results" style="text-decoration:underline;">Results</a> | <? } print(showdate($date, "d M Y")); ?></li>
        <? }}}?>
        
        </ul>
        </div>
 <aside class="col-sm-12 col-md-4">
        <div class="row">
         <!-- Widget header -->
         
          
        
        <section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
                                                                <!-- Widget Header -->
                                                                <header class="clearfix">
                                                                        <h4>Sponsored: In the Stores</h4>
                                                                                                        </header>
<? $getItems=query_execute("SELECT si.name,si.nice_name,si.item_id,sia.price,sia.mrp_price,si.item_parent_id FROM shop_items as si,shop_item_affiliate as sia WHERE sia.item_id=si.item_id AND showbanner='1' ");
				 while($fetchaffiliate=mysql_fetch_array($getItems)){
			 $name=$fetchaffiliate['name'];
			 $item_id=$fetchaffiliate['item_id'];
			  $item_parent_id=$fetchaffiliate['item_parent_id'];
			   $nice_name=$fetchaffiliate['nice_name'];
			    $price=$fetchaffiliate['price'];
			   $mrp_price=$fetchaffiliate['mrp_price'];
		 $rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
		 if(!$rowdatM['media_file'])
		 {
		$rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
	     }
					 $imglink = 'https://ik.imagekit.io/2345/tr:h-100,w-100,c-at_max/shop/item-images/orignal/' . $rowdatM['media_file'];
		 ?>
                        <div class="gurgaon_offers"> 
        	
                <div class="gurgaon_offersr"> <a href="/<?=$nice_name?>/?UTM=bannerAmazon"><img src="<?=$imglink?>" width="100" height="93"></a> </div>
                <div class="gurgaon_offersl">
                    <div class="gurgaon_offers_pn"><a href="/<?=$nice_name?>/?UTM=bannerAmazon"><?=$name?></a> </div>
                                        <div class="gurgaon_offers_pr">
                                        <? if($mrp_price>$price){?>
                                        <span>Rs. <del><?=$mrp_price?></del> 
                                        </span> &nbsp; &nbsp; <? }?>
                                        <span class="price-color">
                                        Rs. <?=$price?></span>
                                       <div class="aff-m-logo"> <a href="/<?=$nice_name?>/?UTM=bannerAmazon"><img src="/new/articles/img/amazon.jpg" /></a>
                                       </div>
                                        </div>
                                    </div>
          	
		</div>
     <? }?>
     </section>
        </div>
      </aside>       
        </div>
        </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?> 