<?php

$Afood_type_set=Array(
"1"=>"Grain Free"
);

$Abreed_type_set=Array(
"4"=>"Maxi Breed",
"7"=>"All Breed",
"3"=>"Medium Breed",
"2"=>"Small Breed",
"1"=>"Mini Breed",
"6"=>"Giant Breed",
"5"=>"Large Breed"
);
$Abreed_type_filter=Array(

"7"=>" (For all type of breeds)",
"3"=>" (10-25 kg adult body weight)",
"2"=>" (Upto 10 kg adult body weight)",

"6"=>" (Above 45 kg adult body weight)",
"5"=>" (25-45 kg adult body weight)"
);

$Astyle_breed=Array(
"1"=>"Starter",
//"3"=>"Junior",
"2"=>"Puppy & Junior",
"5"=>"Adult",
"6"=>"Senior",
"4"=>"Mature",
"7"=>"Others",
);

$Astyle_tips=Array(
"1"=>"Mother & Puppy",
"3"=>"Puppy",
"2"=>"Puppy & Junior",
"5"=>"Adult dog",
"6"=>"Senior dog",
"7"=>"Other Stages"
);
$Astyle_filter=Array(
"1"=>" (Up to 2 month old)",
"3"=>" (9 -24 month old)",
"2"=>" (2 -24 month old)",
"5"=>" (2 - 7 year old)",
"6"=>" (More than 7 year old)",
"7"=>" (Products for all life stages, multi life stages, etc)"
);


$Vimage = Array(
"1" => "thosca",
"2" => "rorga",
"3" => "fiassines",
"4" => "cadfq",
"5" => "hadessw",
"6" => "plisket",
"7" => "burvatua",
"8" => "droid",
"9" => "vininte",
"10" => "ingen"
);

$months=Array(
"1"=> "January",
"2"=> "February",
"3"=> "March",
"4"=> "April",
"5"=> "May",
"6"=> "June",
"7"=> "July",
"8"=> "August",
"9"=> "September",
"10"=> "October",
"11"=> "November",
"12"=> "December"
);

$AMonths = Array(
"01" => "January",
"02" => "February",
"03" => "March",
"04" => "April",
"05" => "May",
"06" => "June",
"07" => "July",
"08" => "August",
"09" => "September",
"10" => "October",
"11" => "November",
"12" => "December"
);

$AMonthsByValue = Array(
	"January" => "01",
	"February" => "02",
	"March" => "03",
	"April" => "04",
	"May" => "05",
	"June" => "06",
	"July" => "07",
	"August" => "08",
	"September" => "09",
	"October" => "10",
	"November" => "11",
	"December" => "12"
);

$DogSex=Array(
"M"=> "Male",
"F"=> "Female"
);

$DogSexDB=Array(
"M"=> "DOG",
"F"=> "BITCH"
);

$DogSex2=Array(
"D"=> "Male",
"B"=> "Female"
);

$DogType=Array(
"BI"=> "Bred in India",
"Imp"=> "Imported"
);

$ADogC=Array(
"A"=> "Minor Puppy Dog",
"B"=> "Puppy Dog",
"C"=> "Junior Dog",
"D"=> "Intermediate Dog",
"E"=> "Bred in India Dog",
"F"=> "Open Dog",
"G"=> "Minor Puppy Bitch",
"H"=> "Puppy Bitch",
"I"=> "Junior Bitch",
"J"=> "Intermediate Bitch",
"K"=> "Bred in India Bitch",
"L"=> "Open Bitch",
"M"=> "All Champions Dog Bitch Both"
);

$AGender=Array(
"M"=> "Male",
"F"=> "Female"
);

$ADating=Array(
"Y"=> "Yes",
"N"=> "No"
);

$ATradeType=Array(
"B"=> "Want to buy",
"S"=> "Available for selling"
);

$RSVP=Array(
"0"=> "Not Attending",
"1"=> "Attending",
"2"=> "May be Attending"
);
$Apaid=Array(
"0"=> "Unpaid",
"1"=> "Paid",
);

$ArrayStatus=Array(
"0"=> "Approved",
"1"=> "Waiting for Approval"
);

$ArrayNationality=Array(
"B"=> "B.I.",
"I"=> "IMP."
);

$AShopStock=Array(
"instock"=>"In Stock",
"outofstock"=>"Out of Stock"
);

$Ashop_attribute_set=Array(
"1"=>"Size",
"2"=>"Color",
"3"=>"Subscription"
);

$Ashop_attribute_value=Array("1" => "XSmall",

	"2" => "Small",

	"3" => "Medium",

	"4" => "Large",

	"5" => "XLarge",

	"6" => "XXLarge",

	"7" => "Red",

	"8" => "Blue",

	"9" => "Black",

	"10" => "One Year",

	"11" => "Two Year",

	"12" => "Three Year",

	"13" => "0.75 Pint,XSmall",

	"14" => "1.20 Pint,Small",

	"15" => "1.25 Pint,Medium",

	"16" => "2.00 Pint,Large",

	"17" => "3.00 Pint,Xlarge",

	"18" => "8 Ounce,XSmall",

	"19" => "16 Ounce,Small",

	"20" => "24 Ounce,Medium",

	"21" => "32 Ounce,Large",

	"22" => "64 Ounce,Xlarge",

	"23" => "Extreme",

	"24" => "Orange",

	"25" => "Green",

	"26" => "Kiwi",

	"27" => "Aubergine",

	"28" => "Black - Black",

	"29" => "Black - Orange",

	"30" => "Black - Blue",

	"31" => "Black - Red",

	"32" => "Black - Yellow",

	"33" => "Black - Kiwi",

	"34" => "Black - Light Blue",

	"35" => "Tan",

	"36" => "4.50 Pint,Extreme",

	"37" => "96 Ounce,Extreme",

	"38" => "1 Quart",

	"39" => "2 Quart",

	"40" => "3 Quart",

	"41" => "4 Quart",

	"42" => "5 Quart",

	"43" => "White",

	"44" => "Mini 3m",

	"45" => "Pink",

	"46" => "Brown",

	"47" => "3 Kg",

	"48" => "12 Kg",

	"49" => "1.5 Kg",

	"50" => "Fashion Ladies ColorfulStrips",

	"51" => "Fashion Ladies Pink",

	"52" => "Fashion Ladies White Dots",

	"53" => "Gentleman Profile",

	"54" => "Gentleman Carbon",

	"55" => "Gentleman Metal",

	"56" => "13 Kg",

	"57" => "200 ML",

	"58" => "250 ML",

	"59" => "1000 ML",

	"60" => "Gallon",

	"61" => "Grey",

	"62" => "185 ML",

	"63" => "450 ML",

	"64" => "4 Pieces",

	"65" => "12 Pieces",

	"66" => "1Large",

	"67" => "4 KG",

	"68" => "15 KG",

	"69" => "Pink",

	"70" => "Navy",

	"71" => "Olive",

	"72" => "Navy-Red",

	"73" => "Red-Navy",

	"74" => "Turquoise",

	"75" => "Golden",

	"76" => "Purple",

	"77" => "Clear Crystal",

	"78" => "118 ML",

	"79" => "500 ML",

	"80" => "Yellow",

	"81" => "7.5 Kg",

	"82" => "8 Kg",

	"83" => "8.5 Kg",

	"84" => "9 Kg",

	"85" => "10 Kg",

	"86" => "20 Kg",

	"87" => "XXS",

	"88" => "2XL",

	"89" => "3XL",

	"90" => "4XL",

	"91" => "5XL",

	"92" => "1.4 Kg",

	"93" => "2.3 Kg",

	"94" => "2.5 Kg",

	"95" => "12.5 Kg",

	"96" => "19 Kg",

	"97" => "20 Kg",

	"98" => "18 KG",

	"99" => "Fashion Flower",

	"100" => "350G",
	"101" => "Cream");
?>