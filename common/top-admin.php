<? 
// Record user
$display_user_Name = dispUname($userid);
$ip = ipCheck();
recordOnlineUser($userid, $display_user_Name, $ip, $requested);
// Record user END
//Remove User/
removeOnlineUsers();
//Remove User/END
?>
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Blogs" href="http://feeds.feedburner.com/dogspotblog" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Q&A" href="http://feeds.feedburner.com/DogspotinQna" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Gallery " href="http://feeds.feedburner.com/DogspotDogs" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Puppies Available" href="http://feeds.feedburner.com/DogspotPuppies" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function clickclear(thisfield, defaulttext) {
if (thisfield.value == defaulttext) {
thisfield.value = "";
}
}
function clickrecall(thisfield, defaulttext) {
if (thisfield.value == "") {
thisfield.value = defaulttext;
}
}
</script>
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript" src="/js/highslide-html.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
    hs.outlineType = 'rounded-white';
    hs.outlineWhileAnimating = true;
    hs.objectLoadTime = 'after';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<link href="/css/top-navi.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/top-navi.js"></script>
</head>
<? if($sitesection == "friends"){?>
<body>
<? }elseif($sitesection == "msg"){?>
<body onLoad="LoadHAjax()">
<? }elseif($ajaxbody == "HOME"){?>
<body>
<? }elseif($ajaxbody == "puppie"){?>
<body>
<? }elseif($ajaxbody == "dog"){ ?>
<body>
<? }elseif($ajaxbody == "dognew"){ ?>
<body onLoad="ajaxload()" class='yui-skin-sam'>
<? }elseif($ajaxbody == "puppynew"){ ?>
<body onLoad="ajaxload()" class='yui-skin-sam'>
<? }elseif($ajaxbody == "cat"){ ?>
<body >
<? }else{
echo"<body>";
}
?>
<?
if($sitesection == "HOME"){
//echo"<div id='mainHome'>";
echo"<div id='main'>";
}else{
 echo"<div id='main'>";
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td width="170" rowspan="2"><a href="https://www.dogspot.in/"><img src="/images/dogspot-logo-2.jpg" alt="DogSpot Spot for all your Dogs Needs" width="167" height="57" border="0"></a></td>
  <td>
   <? if($sitesection != "reg-log"){?>  
  <div id="userstat">Welcome! <span class="redbtext"><? if ($userid != "Guest" && $sessionName){ print (dispUname($userid)); echo"<br><a href = '/home.php?logoff=logoff'>Logout</a> | <a href = '/profile/'>My Profile</a> <br></span>"; }else {?> Guest </span>
    <img src="/images/trans.gif" alt="trans" width="2" height="4" /><br />
      New User? <span class="redbtext"><a href="/reg-log/register-1.php">Register</a> | <a href="/login.php" >Login</a></span><br />   <? } ?>
        </div>
<? } ?>
    
    <div id="tsearch">
 <!-- SiteSearch Google -->
    <form action="https://www.dogspot.in/search.php" id="cse-search-box">
        <input type="hidden" name="cx" value="partner-pub-7080808620760591:8w0nse-xhme" />
        <input type="hidden" name="cof" value="FORID:11" />
        <input type="hidden" name="ie" value="ISO-8859-1" />
        <input type="text" name="q" size="50" value="Search DogSpot" onClick="clickclear(this, 'Search DogSpot')" onBlur="clickrecall(this,'Search DogSpot')" />
        <input type="submit" name="sa" value="Search" class="searchBut"/>
    </form>
<!-- SiteSearch Google -->
  </div></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td colspan="2"><div id="topNavi">
<ul id="sddm">
    <li><span id="spanOff"><a href="/" >Home</a></span></li>
    
    <li><span <? if($sitesection == "articles"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>>
    <a href="/dog-blog/" title="Dog Blogs" onMouseOver="javascript:mopen('m1')">Dog Articles</a></span>
    <div id="m1" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
        <a href="<? echo"/dog-blog/author/$userid/";?>" title="My Dog Blogs">My Dog Articles</a>
        <a href="/dog-blog/new.php" title="Write new Dog Blogs">Write new Dog Article</a>
        <a href="/dog-blog/" title="Read all Dog Blogs">Read all Articles</a>
        <a href="/dog-blog/category/dog-training/" title="Dog Training">Dog Training</a>
        <a href="/dog-blog/category/dog-kennel/" title="Dog Kennel">Dog Kennel</a>
        <a href="/dog-blog/category/dogs-travel/" title=">Dogs Travel">Dogs Travel</a>
        <a href="/dog-blog/category/reproduction/" title="Dogs Reproduction">Dogs Reproduction</a>
        <a href="/dog-blog/category/dog-shows/" title="Dog Shows">Dog Shows</a>
        <a href="/dog-blog/category/adoption/" title="Dog Adoption">Dog Adoption</a>
        <a href="/dog-blog/category/deworming/" title="Dog Deworming">Dog Deworming</a>
        <a href="/dog-blog/category/buying-puppy/" title="Buying/Selling a puppy">Buying/Selling a puppy</a>
        <a href="/dog-blog/category/news/" title="Dog News">Dog News</a>
        <a href="/dog-blog/category/dog-nutrition/" title="Dog Nutrition">Dog Nutrition</a>
        <a href="/dog-blog/category/puppy-care/" title="Puppy Care">Puppy Care</a>
        <a href="/dog-blog/category/obedience-training/" title="Obedience Dog Training">Obedience Dog Training</a>
        <a href="/dog-blog/category/events/" title="Dog Events">Dog Events</a>
        <a href="/dog-blog/category/preventive-medicine/" title="Dog Preventive medicine">Dog Preventive medicine</a>
        <a href="/dog-blog/category/vaccination/" title="Dog Vaccination">Dog Vaccination</a>
        <a href="/dog-blog/category/dogspot-featured-kennel/" title="DogSpot Featured Kennel">DogSpot Featured Kennel</a>
        <a href="/dog-blog/category/surgery/" title="Dog Surgery">Dog Surgery</a>
        <a href="/dog-blog/category/lost-found/" title="Dogs Lost and Found">Dogs Lost and Found</a>
        <a href="/dog-blog/category/dog-tricks-fun/" title="Dog Tricks and Fun">Dog Tricks and Fun</a>
        </div>
    </li>
    <li><span <? if($sitesection == "qna"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>>
    <a href="/qna/" title="Dog Forum and dog questions" onMouseOver="javascript:mopen('m2')">Dogs Forum</a></span>
    <div id="m2" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
        <a href="<? echo"/qna/author/$userid/";?>" title="My Dog Questions">My Dog Questions</a>
        <a href="/qna/new.php" title="Ask New Dog Questions">Ask New Dog Questions</a>
        <a href="/qna/" title="Read all Dog Questions & Answers">Read all Dog Questions & Answers</a>       
        <a href="/qna/category/puppy-care/" title="Puppy Care Forum">Puppy Care</a>
        <a href="/qna/category/dog-training/" title="Dog Training Forum">Dog Training</a>
        <a href="/qna/category/diseases/" title="Dog Diseases Forum">Dog Diseases</a>
        <a href="/qna/category/adoption/" title=">Dogs Adoption Forum">Dogs Adoption</a>
        <a href="/qna/category/vaccination/" title="Dogs Vaccination Forum">Dogs Vaccination</a>
        <a href="/qna/category/reproduction/" title="Dog Reproduction Forum">Dog Reproduction</a>
        <a href="/qna/category/kennel-club-india/" title="Kennel Club of India Forum">Kennel Club of India</a>
        <a href="/qna/category/buying-puppy/" title="Buying a puppy Forum">Buying a puppy</a>
        <a href="/qna/category/obedience-training/" title="Dogs Obedience Training Forum">Dogs Obedience Training</a>
        <a href="/qna/category/dog-kennel/" title="Dog Kennel Forum">Dog Kennel</a>
        <a href="/qna/category/dog-shows/" title="Dog Shows Forum">Dog Shows</a>
        <a href="/qna/category/pet-shops/" title="Pet Shops Forum">Pet Shops</a>
        <a href="/qna/category/preventive-medicine/" title="Preventive medicine Forum">Preventive medicine</a>
        <a href="/qna/category/selling-puppy/" title="Selling a puppy Forum">Selling a puppy</a>
        <a href="/qna/category/cruelty-against-animals/" title="Cruelty against Animals Forum">Cruelty against Animals</a>
        <a href="/qna/category/surgery/" title="Dog Surgery Forum">Dog Surgery</a>
        <a href="/qna/category/skin-problems/" title="Dogs Skin Problems Forum">Dogs Skin Problems</a>
        </div>
    </li>
    <li><span <? if($sitesection == "dog"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>><a href="/dogs/" title="Dog Breeds" onMouseOver="javascript:mopen('m3')">Dogs Gallery</a></span>
    <div id="m3" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
        <a href="<? echo"/dogs/owner/$userid/";?>" title="Dog Breeds">My Dogs</a>
        <a href="/dogs/new.php" title="Add new Dog Breeds">Add new Dog</a>
        <a href="/dogs/" title="All Dogs Breeds">View all Dogs</a>
        <a href="/dogs/breeds/" title="Dog Breeds">Dog Breeds</a>
        <a href="/dogs/category/buy/" title="Dogs for Sale">Dogs for Sale</a></div>
    </li>
    <li><span <? if($sitesection == "puppie"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>><a href="/puppies/" title="Puppies for Sale, Adoption, Free Puppies" onMouseOver="javascript:mopen('m4')">Puppies Available</a></span>
    <div id="m4" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
     <a href="<? echo"/puppies/owner/$userid/";?>" title="Puppies for Sale">My Puppy</a>
     <a href="/puppies/new.php" title="New Puppies for Sale, Adoption, Free Puppies">Add new Puppy</a>
     <a href="/puppies/" title="Puppies for Sale, Adoption, Free Puppies of all Breeds">View all Puppies</a>
     <a href="/puppies/category/sale/" title="Puppies for Sale">Puppies for Sale</a>
     <a href="/puppies/category/buy/" title="Puppies for Adoption, Free Puppies">Puppies Wanted</a>
     <a href="/puppies/category/male/" title="Dog Puppies for Sale, Adoption, Free Puppies of all Breeds">Puppies Dog</a>
     <a href="/puppies/category/female/" title="Puppies Bitch for Sale, Adoption, Free Puppies of all Breeds">Puppies Bitch</a>
     <a href="/puppies/breeds/" title="Free Puppies of all Breeds">Puppy Breeds</a>
     </div>
  </li>
	<li><span <? if($sitesection == "dog-events"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>><a href="/dog-events/" title="Dog Show" onMouseOver="javascript:mopen('m5')">Dog Events</a></span>
    <div id="m5" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
     <a href="<? echo"/dog-events/member/$userid/";?>" title="Dog Events">My dog Events</a>
     <a href="/dog-events/list.php" title="Dog Shows">View all Events</a>
     <a href="/dog-events/new-events.php" title="Submit new Dog Shows">Submit an Event FREE!</a>
    </div>
   </li>
   <li><span <? if($sitesection == "dog-business"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>><a href="/dog-listing/" onMouseOver="javascript:mopen('m6')">Dog Business</a></span>
    <div id="m6" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
     <a href="/dog-listing/member/<? echo"$userid";?>/">My dog Business</a>
     <a href="/dog-listing/listing-cat.php">View all Business</a>
     <a href="/dog-listing/add-business.php">Submit Dog Business FREE!</a>
    </div>
   </li>
  <li><span <? if($sitesection == "photos"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>><a href="/photos/" onMouseOver="javascript:mopen('m7')">Photos</a></span>
    <div id="m7" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
     <a href="<? echo"/photos/member/$userid/";?>">My Photos</a>
     <a href="/photos/">Add new Dogs Photos</a>
     <a href="/photos/">View all Photos</a>
     <a href="/photos/album/royal-kennel-club-delhi/">Royal Kennel Club, Delhi</a>
     <a href="/photos/album/agra-dog-show-3-feb-2008/">Agra Dog Show Pictures</a>
     <a href="/photos/album/the-westminster-kennel-club-dog-show-2008-results/">Westminster Kennel Club Dog Show</a>    </div>
   </li>
   <li><span <? if($sitesection == "videos"){echo"id='spanOn'";}else{?>id="spanOff"<? }?>><a href="/videos/" onMouseOver="javascript:mopen('m8')">Videos</a></span>
    <div id="m8" onMouseOver="mcancelclosetime()" onMouseOut="mclosetime()">
     <a href="<? echo"/videos/member/$userid/";?>">My Videos</a>
     <a href="/videos/">Add new Dogs Videos</a>
     <a href="/videos/">View all Videos</a>
    </div>
   </li>
   
</ul>
<div id="clearall"></div>
</div></td>
  </tr>
</table>
<div id="topNaviLine"></div>
