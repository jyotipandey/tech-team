
<div id="leftnav">
<div id="userinfo">For Adoption</div>
<a href="/adoption/category/male/" class="userbutton" title="Puppy Dog">Male Puppies</a>
<a href="/adoption/category/female/" class="userbutton" title="Puppy Bitch">Female Puppies</a>
<ul id="MenuBar1" class="MenuBarVertical">
  <li><a class="MenuBarItemSubmenu" href="/adoption/breeds/" title="Puppy Breeds">Adoption Breeds</a>
      <ul>
      
        <li><a href="/adoption/breed/affen-pinscher/" title="Affen Pinscher adoption">Affen Pinscher</a></li>
      
        <li><a href="/adoption/breed/afghan-hound/" title="Afghan Hound adoption">Afghan Hound</a></li>
      
        <li><a href="/adoption/breed/airedale-terrier/" title="Airedale Terrier adoption">Airedale Terrier</a></li>
      
        <li><a href="/adoption/breed/akita/" title="Akita adoption">Akita</a></li>
      
        <li><a href="/adoption/breed/alangu/" title="Alangu adoption">Alangu</a></li>
      
        <li><a href="/adoption/breed/alaskan-malamute/" title="Alaskan Malamute adoption">Alaskan Malamute</a></li>
      
        <li><a href="/adoption/breed/American Staffordshire Terrier/" title=" adoption"></a></li>
      
        <li><a href="/adoption/breed/american-staffordshire-terrier/" title="American Staffordshire Terrier adoption">American Staffordshire Terrier</a></li>
      
        <li><a href="/adoption/breed/australian-terrier/" title="Australian Terrier adoption">Australian Terrier</a></li>
      
        <li><a href="/adoption/breed/basset-hound/" title="Basset Hound adoption">Basset Hound</a></li>
      
        <li><a href="/adoption/breed/beagle/" title="Beagle adoption">Beagle</a></li>
      
        <li><a href="/adoption/breed/bearded-collie/" title="Bearded Collie adoption">Bearded Collie</a></li>
      
        <li><a href="/adoption/breed/bedlington-terrier/" title="Bedlington Terrier adoption">Bedlington Terrier</a></li>
      
        <li><a href="/adoption/breed/belgian-malinois/" title="Belgian Malinois adoption">Belgian Malinois</a></li>
      
        <li><a href="/adoption/breed/bernese-mountain-dog/" title="Bernese Mountain Dog adoption">Bernese Mountain Dog</a></li>
      
        <li><a href="/adoption/breed/bichon-frise/" title="Bichon Frise adoption">Bichon Frise</a></li>
      
        <li><a href="/adoption/breed/blood-hound/" title="Blood Hound adoption">Blood Hound</a></li>
      
        <li><a href="/adoption/breed/bordeaux-mastiff/" title="Bordeaux Mastiff adoption">Bordeaux Mastiff</a></li>
      
        <li><a href="/adoption/breed/border-collie/" title="Border Collie adoption">Border Collie</a></li>
      
        <li><a href="/adoption/breed/boston-terrier/" title="Boston Terrier adoption">Boston Terrier</a></li>
      
        <li><a href="/adoption/breed/boxer/" title="Boxer adoption">Boxer</a></li>
      
        <li><a href="/adoption/breed/bull-dog/" title="Bull Dog adoption">Bull Dog</a></li>
      
        <li><a href="/adoption/breed/bull-mastiff/" title="Bull Mastiff adoption">Bull Mastiff</a></li>
      
        <li><a href="/adoption/breed/bull-terrier/" title="Bull Terrier adoption">Bull Terrier</a></li>
      
        <li><a href="/adoption/breed/bull-terrier-miniature/" title="Bull Terrier (Miniature) adoption">Bull Terrier (Miniature)</a></li>
      
        <li><a href="/adoption/breed/cane-corso-italiano/" title="Cane Corso Italiano adoption">Cane Corso Italiano</a></li>
      
        <li><a href="/adoption/breed/caravan-hound/" title="Caravan Hound adoption">Caravan Hound</a></li>
      
        <li><a href="/adoption/breed/cavalier-king-charles-spaniel/" title="Cavalier King Charles Spaniel adoption">Cavalier King Charles Spaniel</a></li>
      
        <li><a href="/adoption/breed/chihuahua-long-smooth-coat/" title="Chihuahua (Long & Smooth Coat) adoption">Chihuahua (Long & Smooth Coat)</a></li>
      
        <li><a href="/adoption/breed/chinese-crested/" title="Chinese Crested adoption">Chinese Crested</a></li>
      
        <li><a href="/adoption/breed/chippiparai/" title="Chippiparai adoption">Chippiparai</a></li>
      
        <li><a href="/adoption/breed/chow-chow/" title="Chow Chow adoption">Chow Chow</a></li>
      
        <li><a href="/adoption/breed/clumber-spaniel/" title="Clumber Spaniel adoption">Clumber Spaniel</a></li>
      
        <li><a href="/adoption/breed/cocker-spaniel-american/" title="Cocker Spaniel (American) adoption">Cocker Spaniel (American)</a></li>
      
        <li><a href="/adoption/breed/cocker-spaniel-english/" title="Cocker Spaniel (English) adoption">Cocker Spaniel (English)</a></li>
      
        <li><a href="/adoption/breed/collie-smooth-/" title="Collie (Smooth ) adoption">Collie (Smooth )</a></li>
      
        <li><a href="/adoption/breed/collierough/" title="Collie(Rough) adoption">Collie(Rough)</a></li>
      
        <li><a href="/adoption/breed/czechoslovakian-wolfdog/" title="Czechoslovakian Wolfdog adoption">Czechoslovakian Wolfdog</a></li>
      
        <li><a href="/adoption/breed/dachshund-miniature-long-smooth-wire-haired/" title="Dachshund Miniature (Long, Smooth & Wire Haired) adoption">Dachshund Miniature (Long, Smooth & Wire Haired)</a></li>
      
        <li><a href="/adoption/breed/dachshund-miniature-smooth-haired/" title="Dachshund Miniature ( Smooth Haired) adoption">Dachshund Miniature ( Smooth Haired)</a></li>
      </ul>
  </li>
  </ul>
  <div id="clearall"></div>
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</div>