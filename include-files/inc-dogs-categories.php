
<div id="leftnav">
<div id="userinfo">Dogs</div>
<a href="/dogs/category/sale/" class="userbutton" title="Looking for a Dogs">Looking for a Dog</a>
<a href="/dogs/category/buy/" class="userbutton" title="Dogs for Sale">Available for Sale</a>
<a href="/dogs/category/dating/" class="userbutton" title="Breeding Dogs">For Dating</a>
<a href="/dogs/category/male/" class="userbutton" title="Dogs India">Male Dogs</a>
<a href="/dogs/category/female/" class="userbutton" title="Bitch Dog India">Female Dogs</a>

<ul id="MenuBar1" class="MenuBarVertical">
  <li><a class="MenuBarItemSubmenu" href="/dogs/breeds/" title="Dog Breeds">Dog Breeds</a>
      <ul>
      
        <li><a href="/dogs/breed//" title=" Dogs"></a></li>
      
        <li><a href="/dogs/breed/affen-pinscher/" title="Affen Pinscher Dogs">Affen Pinscher</a></li>
      
        <li><a href="/dogs/breed/afghan-hound/" title="Afghan Hound Dogs">Afghan Hound</a></li>
      
        <li><a href="/dogs/breed/airedale-terrier/" title="Airedale Terrier Dogs">Airedale Terrier</a></li>
      
        <li><a href="/dogs/breed/akita/" title="Akita Dogs">Akita</a></li>
      
        <li><a href="/dogs/breed/alangu/" title="Alangu Dogs">Alangu</a></li>
      
        <li><a href="/dogs/breed/alaskan-malamute/" title="Alaskan Malamute Dogs">Alaskan Malamute</a></li>
      
        <li><a href="/dogs/breed/American Staffordshire Terrier/" title=" Dogs"></a></li>
      
        <li><a href="/dogs/breed/american-eskimo/" title="American Eskimo Dogs">American Eskimo</a></li>
      
        <li><a href="/dogs/breed/american-staffordshire-terrier/" title="American Staffordshire Terrier Dogs">American Staffordshire Terrier</a></li>
      
        <li><a href="/dogs/breed/australian-cattle-dog/" title="Australian Cattle Dog Dogs">Australian Cattle Dog</a></li>
      
        <li><a href="/dogs/breed/australian-kelpie/" title="Australian Kelpie Dogs">Australian Kelpie</a></li>
      
        <li><a href="/dogs/breed/australian-terrier/" title="Australian Terrier Dogs">Australian Terrier</a></li>
      
        <li><a href="/dogs/breed/basenji/" title="Basenji Dogs">Basenji</a></li>
      
        <li><a href="/dogs/breed/basset-hound/" title="Basset Hound Dogs">Basset Hound</a></li>
      
        <li><a href="/dogs/breed/beagle/" title="Beagle Dogs">Beagle</a></li>
      
        <li><a href="/dogs/breed/bearded-collie/" title="Bearded Collie Dogs">Bearded Collie</a></li>
      
        <li><a href="/dogs/breed/bedlington-terrier/" title="Bedlington Terrier Dogs">Bedlington Terrier</a></li>
      
        <li><a href="/dogs/breed/belgian-malinois/" title="Belgian Malinois Dogs">Belgian Malinois</a></li>
      
        <li><a href="/dogs/breed/bernese-mountain-dog/" title="Bernese Mountain Dog Dogs">Bernese Mountain Dog</a></li>
      
        <li><a href="/dogs/breed/bichon-frise/" title="Bichon Frise Dogs">Bichon Frise</a></li>
      
        <li><a href="/dogs/breed/blood-hound/" title="Blood Hound Dogs">Blood Hound</a></li>
      
        <li><a href="/dogs/breed/bordeaux-mastiff/" title="Bordeaux Mastiff Dogs">Bordeaux Mastiff</a></li>
      
        <li><a href="/dogs/breed/border-collie/" title="Border Collie Dogs">Border Collie</a></li>
      
        <li><a href="/dogs/breed/borzoi/" title="Borzoi Dogs">Borzoi</a></li>
      
        <li><a href="/dogs/breed/boston-terrier/" title="Boston Terrier Dogs">Boston Terrier</a></li>
      
        <li><a href="/dogs/breed/bouvier-des-flandres/" title="Bouvier des Flandres Dogs">Bouvier des Flandres</a></li>
      
        <li><a href="/dogs/breed/boxer/" title="Boxer Dogs">Boxer</a></li>
      
        <li><a href="/dogs/breed/briard/" title="Briard Dogs">Briard</a></li>
      
        <li><a href="/dogs/breed/bull-dog/" title="Bull Dog Dogs">Bull Dog</a></li>
      
        <li><a href="/dogs/breed/bull-mastiff/" title="Bull Mastiff Dogs">Bull Mastiff</a></li>
      
        <li><a href="/dogs/breed/bull-terrier/" title="Bull Terrier Dogs">Bull Terrier</a></li>
      
        <li><a href="/dogs/breed/bull-terrier-miniature/" title="Bull Terrier (Miniature) Dogs">Bull Terrier (Miniature)</a></li>
      
        <li><a href="/dogs/breed/cane-corso-italiano/" title="Cane Corso Italiano Dogs">Cane Corso Italiano</a></li>
      
        <li><a href="/dogs/breed/caravan-hound/" title="Caravan Hound Dogs">Caravan Hound</a></li>
      
        <li><a href="/dogs/breed/caucasian-shepherd/" title="Caucasian Shepherd Dogs">Caucasian Shepherd</a></li>
      
        <li><a href="/dogs/breed/cavalier-king-charles-spaniel/" title="Cavalier King Charles Spaniel Dogs">Cavalier King Charles Spaniel</a></li>
      
        <li><a href="/dogs/breed/central-asian-shepherd-alabai/" title="Central Asian shepherd (alabai) Dogs">Central Asian shepherd (alabai)</a></li>
      
        <li><a href="/dogs/breed/chihuahua-long-smooth-coat/" title="Chihuahua (Long & Smooth Coat) Dogs">Chihuahua (Long & Smooth Coat)</a></li>
      
        <li><a href="/dogs/breed/chinese-crested/" title="Chinese Crested Dogs">Chinese Crested</a></li>
      </ul>
  </li>
  </ul>
  <div id="clearall"></div>
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>

<hr />

</div>