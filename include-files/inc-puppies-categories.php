
<div id="leftnav">
<div id="userinfo">Puppies</div>
<a href="/puppies/category/sale/" class="userbutton" title="Puppies for Sale">Puppies for Sale</a>
<a href="/puppies/category/buy/" class="userbutton" title="Puppies India">Puppies Wanted</a>
<a href="/puppies/category/male/" class="userbutton" title="Puppy Dog">Male Puppies</a>
<a href="/puppies/category/female/" class="userbutton" title="Puppy Bitch">Female Puppies</a>
<ul id="MenuBar1" class="MenuBarVertical">
  <li><a class="MenuBarItemSubmenu" href="/puppies/breeds/" title="Puppy Breeds">Puppy Breeds</a>
      <ul>
      
        <li><a href="/puppies/breed/affen-pinscher/" title="Affen Pinscher Puppies">Affen Pinscher</a></li>
      
        <li><a href="/puppies/breed/afghan-hound/" title="Afghan Hound Puppies">Afghan Hound</a></li>
      
        <li><a href="/puppies/breed/airedale-terrier/" title="Airedale Terrier Puppies">Airedale Terrier</a></li>
      
        <li><a href="/puppies/breed/akita/" title="Akita Puppies">Akita</a></li>
      
        <li><a href="/puppies/breed/alangu/" title="Alangu Puppies">Alangu</a></li>
      
        <li><a href="/puppies/breed/alaskan-malamute/" title="Alaskan Malamute Puppies">Alaskan Malamute</a></li>
      
        <li><a href="/puppies/breed/American Staffordshire Terrier/" title=" Puppies"></a></li>
      
        <li><a href="/puppies/breed/american-staffordshire-terrier/" title="American Staffordshire Terrier Puppies">American Staffordshire Terrier</a></li>
      
        <li><a href="/puppies/breed/australian-terrier/" title="Australian Terrier Puppies">Australian Terrier</a></li>
      
        <li><a href="/puppies/breed/basset-hound/" title="Basset Hound Puppies">Basset Hound</a></li>
      
        <li><a href="/puppies/breed/beagle/" title="Beagle Puppies">Beagle</a></li>
      
        <li><a href="/puppies/breed/bearded-collie/" title="Bearded Collie Puppies">Bearded Collie</a></li>
      
        <li><a href="/puppies/breed/bedlington-terrier/" title="Bedlington Terrier Puppies">Bedlington Terrier</a></li>
      
        <li><a href="/puppies/breed/belgian-malinois/" title="Belgian Malinois Puppies">Belgian Malinois</a></li>
      
        <li><a href="/puppies/breed/bernese-mountain-dog/" title="Bernese Mountain Dog Puppies">Bernese Mountain Dog</a></li>
      
        <li><a href="/puppies/breed/bichon-frise/" title="Bichon Frise Puppies">Bichon Frise</a></li>
      
        <li><a href="/puppies/breed/blood-hound/" title="Blood Hound Puppies">Blood Hound</a></li>
      
        <li><a href="/puppies/breed/bordeaux-mastiff/" title="Bordeaux Mastiff Puppies">Bordeaux Mastiff</a></li>
      
        <li><a href="/puppies/breed/border-collie/" title="Border Collie Puppies">Border Collie</a></li>
      
        <li><a href="/puppies/breed/boston-terrier/" title="Boston Terrier Puppies">Boston Terrier</a></li>
      
        <li><a href="/puppies/breed/boxer/" title="Boxer Puppies">Boxer</a></li>
      
        <li><a href="/puppies/breed/bull-dog/" title="Bull Dog Puppies">Bull Dog</a></li>
      
        <li><a href="/puppies/breed/bull-mastiff/" title="Bull Mastiff Puppies">Bull Mastiff</a></li>
      
        <li><a href="/puppies/breed/bull-terrier/" title="Bull Terrier Puppies">Bull Terrier</a></li>
      
        <li><a href="/puppies/breed/bull-terrier-miniature/" title="Bull Terrier (Miniature) Puppies">Bull Terrier (Miniature)</a></li>
      
        <li><a href="/puppies/breed/cane-corso-italiano/" title="Cane Corso Italiano Puppies">Cane Corso Italiano</a></li>
      
        <li><a href="/puppies/breed/caravan-hound/" title="Caravan Hound Puppies">Caravan Hound</a></li>
      
        <li><a href="/puppies/breed/caucasian-shepherd/" title="Caucasian Shepherd Puppies">Caucasian Shepherd</a></li>
      
        <li><a href="/puppies/breed/cavalier-king-charles-spaniel/" title="Cavalier King Charles Spaniel Puppies">Cavalier King Charles Spaniel</a></li>
      
        <li><a href="/puppies/breed/central-asian-shepherd-alabai/" title="Central Asian shepherd (alabai) Puppies">Central Asian shepherd (alabai)</a></li>
      
        <li><a href="/puppies/breed/chihuahua-long-smooth-coat/" title="Chihuahua (Long & Smooth Coat) Puppies">Chihuahua (Long & Smooth Coat)</a></li>
      
        <li><a href="/puppies/breed/chinese-crested/" title="Chinese Crested Puppies">Chinese Crested</a></li>
      
        <li><a href="/puppies/breed/chippiparai/" title="Chippiparai Puppies">Chippiparai</a></li>
      
        <li><a href="/puppies/breed/chow-chow/" title="Chow Chow Puppies">Chow Chow</a></li>
      
        <li><a href="/puppies/breed/clumber-spaniel/" title="Clumber Spaniel Puppies">Clumber Spaniel</a></li>
      
        <li><a href="/puppies/breed/cocker-spaniel-american/" title="Cocker Spaniel (American) Puppies">Cocker Spaniel (American)</a></li>
      
        <li><a href="/puppies/breed/cocker-spaniel-english/" title="Cocker Spaniel (English) Puppies">Cocker Spaniel (English)</a></li>
      
        <li><a href="/puppies/breed/collie-smooth-/" title="Collie (Smooth ) Puppies">Collie (Smooth )</a></li>
      
        <li><a href="/puppies/breed/collierough/" title="Collie(Rough) Puppies">Collie(Rough)</a></li>
      
        <li><a href="/puppies/breed/czechoslovakian-wolfdog/" title="Czechoslovakian Wolfdog Puppies">Czechoslovakian Wolfdog</a></li>
      </ul>
  </li>
  </ul>
  <div id="clearall"></div>
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
<hr />

</div>