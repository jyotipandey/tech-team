<?php
// initialize ob_gzhandler to send and compress data
//ob_start('ob_gzhandler');
 
// required header info and character set
header("Content-type: text/css;charset: UTF-8");
 
// duration of cached content (24 hours)
$offset = 60 * 60 * 24;
 
// cache control to process
header ('Cache-Control: max-age=' . $offset . ', must-revalidate');
 
// expiration header format
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s",time() + $offset) . " GMT";
 
// send cache expiration header to browser
header($ExpStr);
 
// initialize compress function for white-space removal
//ob_start("compress");
 
// Begin function compress
function compress($buffer) {
 
// remove comments
//    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
 
// remove tabs, spaces, new lines, etc. 
//    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
 
// remove unnecessary spaces
    $buffer = str_replace('{ ', '{', $buffer);
    $buffer = str_replace(' }', '}', $buffer);
    $buffer = str_replace('; ', ';', $buffer);
    $buffer = str_replace(', ', ',', $buffer);
    $buffer = str_replace(' {', '{', $buffer);
    $buffer = str_replace('} ', '}', $buffer);
    $buffer = str_replace(': ', ':', $buffer);
    $buffer = str_replace(' ,', ',', $buffer);
    $buffer = str_replace(' ;', ';', $buffer);
return $buffer;
}
 
/**
INCLUDE YOUR STYLE SHEETS HERE
**/
require_once($DOCUMENT_ROOT.'/new/css/main.css');
require_once($DOCUMENT_ROOT.'/new/css/shop.css');
require_once($DOCUMENT_ROOT.'/new/css/headfoot.css');
require_once($DOCUMENT_ROOT.'/new/css/dropdown.css');
require_once($DOCUMENT_ROOT.'/new/css/dropdown.vertical.css');
require_once($DOCUMENT_ROOT.'/js/starmatrix.css');
require_once($DOCUMENT_ROOT.'/new/slide/skins.css');
require_once($DOCUMENT_ROOT.'/new/slide/jquery.jcarousel.css');
require_once($DOCUMENT_ROOT.'/jqzoom_ev-2.3/css/jquery.jqzoom.css');
require_once($DOCUMENT_ROOT.'/new/css/featured.css');
require_once($DOCUMENT_ROOT.'/new/css/rcbreed.css');
require_once($DOCUMENT_ROOT.'/shapopup/shapopup.css');
require_once($DOCUMENT_ROOT.'/accordion-more/style.css');
require_once($DOCUMENT_ROOT.'/new/css/login-style.css');
 
// end compression
ob_end_flush();
ob_end_clean();
?>