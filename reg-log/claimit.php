<?php
ob_start();
session_start();
$ajaxbody = "reg-log";
$sitesection = "reg-log";

require_once('../database.php');
require_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Registration : Spot for all your Dog's Needs</title>
<meta name="keywords" content="Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog : DogSpot" />
<meta name="description" content="Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog : DogSpot" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript" src="/js/highslide-html.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
    hs.outlineType = 'rounded-white';
    hs.outlineWhileAnimating = true;
    hs.objectLoadTime = 'after';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<script src="/js/ajax.js" type="text/javascript"></script>
</head>
<div id="head">
    <div id="logo"><a href="https://www.dogspot.in/"><img src="/images/logo-new.jpg" alt="DogSpot.in" width="253" height="83" border="0" /></a></div>
  </div>
<div id="pagebody">

</div>
</div>
<?php require_once('../common/bottom.php'); ?>