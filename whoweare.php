<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dogs India : DogSpot : Who we are : Dog Lovers Community</title>
<meta name="keywords" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="pagebody">
    <div id="inbody">
      <h1>Who We Are?</h1>
      <p>We are a bunch of dog enthusiasts hell-bent on organizing all information on dogs in India, and providing you one single window to access it. We aim to digitize all available information on dogs, clean the rust, and give it a new shine. </p>
      <p>For content that are not available, we have a panel of experts ranging from professors in veterinary sciences, to dog experts, to vets, to top kennel club members, to trainers who will help us in creating insightful articles addressing the most common to the rarest of pet issues that exist. You may even ask your questions and get them answered absolutely free! </p>
      <p>That’s not all, DogSpot will allow you to make new friends, find your dog a date, help you buy or sell pets, order your pet supplies online.</p>
      </div>
      </div>
</body>



</html>