<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shipping &amp; Delivery  Policy for Products | DogSpot</title>
<meta name="keywords" content="Shipping &amp; Delivery  Policy for Products | DogSpot" />
<meta name="description" content="Shipping &amp; Delivery  Policy for Products | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>
<div class="cont980">
<div class="vs10"></div>
 <h1>Shipping &amp; Delivery  Policy for Products </h1><br />
  <p>We ship only in India.<br />
For domestic (India) buyers, orders are shipped through registered domestic  courier companies and /or speed post only. Orders are shipped within 10 working  days or as per the delivery date agreed at the time of order confirmation and  delivering of the shipment subject to Courier Company / post office norms.  PetsGlam Services Pvt Ltd (DogSpot.in) is not liable for any delay in delivery  by the courier company / postal authorities and only guarantees to hand over  the consignment to the courier company or postal authorities within 10 working  days from the date of the order and payment or as per the delivery date agreed  at the time of order confirmation. Delivery of all orders will be to registered  address of the buyer as per the credit/debit card only at all times (Unless  specified at the time of Order). PetsGlam Services Pvt Ltd (DogSpot.in) is in  no way responsible for any damage to the order while in transit to the buyer. </p>
<p>Shipping Charge of Rs.49 is applicable on every order.</p>  
<p> Rs. 50  applicable   for Cash On Delivery (COD) payment method.</p>
  <p>    In India, some state governments levies the Octroi  charges when the product enters the state. <br />
  <strong>The  Octroi charge is payable by the recipient at the time of delivery</strong>. The  courier will collect the Octroi amount from the recipient at the time of  delivery.</p>



</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
