<?php
require_once('constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();
 if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/contactus-bootstrap.php');
  exit;
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="canonical" href="https://www.dogspot.in/contactus.php" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/contactus.php" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact DogSpot - Online Pet Supply Store</title>
<meta name="keywords" content="Contact Dogspot, Contact Pet supplies store, get in touch with Pet Shop, Dog store." />
<meta name="description" content="Contact DogSpot for any questions regarding pets supplies, accessories & online products purchase. You can get in touch via phone calls or by filling inquiry form." />

<div itemscope itemtype="http://schema.org/Organization">
<a itemprop="url" href="https://www.dogspot.in/"><meta itemprop="name" content="DogSpot.in">
</a>
<meta itemprop="description" content="DogSpot.in offers pet products including dogs, cats and small pets online.">
<meta itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<?php /*?><meta itemprop="streetAddress" content="Plot no - 545, S.Lal Tower Sector - 20, Dundahera">
<meta itemprop="addressLocality" content="Gurgaon">
<meta itemprop="addressRegion" content="Haryana">
<meta itemprop="postalCode" content="122016">
<meta itemprop="addressCountry" content="India"><?php */?>
</div>
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
//require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->


<script type="text/javascript">
 jQuery(document).ready(function() {
	  jQuery("#formC").validate({
		  
	});
});
</script>




<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div class="cont980">
<div class="vs10"></div>
<h1>Send us an Email</h1>
<div class="vs10"></div>
 <div style="float:left; width:460px;display:none;" id="displaymsg">
 
 </div>
  <div style="float:left; width:460px; " id="leftside">
 <link rel="stylesheet" type="text/css" href="/css/widget.css?p=e">
 <div style="margin: 0;width: 100%;">
 <div class="modal-header">
  <div class="modal-header-bg">
    <h3 class="ellipsis lead pull-left form-title" title="Help &amp; Support">Help &amp; Support</h3></div></div>
<div class="form-placeholder" style="margin-top: 15px;">
<div id="email-validate" style="display:none;"><font color="#FF0000">Please enter valid emaild id!</font></div>
<div id="form-validate" style="display:none;"><font color="#FF0000">Please fill all the fields!</font></div>
<div id="form-recapchavalidate" style="display:none;"><font color="#FF0000">Captcha verification failed, try again!</font></div>

<div id="form-mobilevalidate" style="display:none;"><font color="#FF0000">Please enter valid mobile number!</font></div>

		  			<div class="control-group default_requester">
		  				 <label class=" required control-label requester-label " for="helpdesk_ticket_email">Email Id</label>
            <div class="controls   ">
              <div class="row-fluid">
	<input class="span12 email required" id="helpdesk_ticket_email" name="helpdesk_ticket[email]" autocomplete="on" size="30" type="email" />
		
	
</div>
            </div> 
		  			</div>
                    <div class="control-group default_requester">
		  				 <label class=" required control-label requester-label " for="helpdesk_ticket_mobile">Mobile No.</label>
            <div class="controls   ">
              <div class="row-fluid">
	<input class="span12 text required" id="helpdesk_ticket_mobile" name="helpdesk_ticket_mobile[email]" autocomplete="on" size="30" type="text" maxlength="10" />
		
	<span id="errmsg"></span>
</div>
            </div> 
		  			</div>
		  			<div class="control-group default_subject">
		  				 <label class=" required control-label subject-label " for="helpdesk_ticket_subject">Subject</label>
            <div class="controls   ">
              <input class=" required text span12" id="helpdesk_ticket_subject" name="helpdesk_ticket[subject]" size="30" type="text" />
            </div> 
		  			</div>
		  			<div class="control-group default_description">
		  				 <label class=" required control-label description-label " for="helpdesk_ticket_description">Description</label>
            <div class="controls   ">
               <textarea class="required_redactor html_paragraph span12"  cols="40" id="helpdesk_ticket_ticket_body_attributes_description_html" name="helpdesk_ticket_ticket_body_attributes_description_html" rows="6">
</textarea>   
            </div> 
		  			</div>
		  	<input id="helpdesk_ticket_source" name="helpdesk_ticket[source]" type="hidden" value="9" />
	  	</div>
 
  <div class="control-group">
    <div class="controls recaptcha-control">
      <div id="captcha_wrap">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="g-recaptcha " data-sitekey="6LdvejUUAAAAAEn6wjOFcPVRyQr4KtAJ03ltA1SQ" display="themewhitesizecompact"></div>
          <noscript>
            <div>
              <div style="width: 302px; height: 422px; position: relative;">
                <div style="width: 302px; height: 422px; position: absolute;">
                  <iframe
                    src="https://www.google.com/recaptcha/api/fallback?k=6LdvejUUAAAAAEn6wjOFcPVRyQr4KtAJ03ltA1SQ"
                    frameborder="0" scrolling="no"
                    style="width: 302px; height:422px; border-style: none;">
                  </iframe>
                </div>
              </div>
              <div style="width: 300px; height: 60px; border-style: none;
                bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
                background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
                <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                  class="g-recaptcha-response"
                  style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
                  margin: 10px 25px; padding: 0px; resize: none;" value="">
                </textarea>
              </div>
            </div>
          </noscript>

       <?php /*?> <div class="recaptcha-error-message hide" id="helpdesk_ticket_captcha-error">
          CAPTCHA verification is required.
        </div><?php */?>
      </div>
      <!-- end captch_wrap -->
      <input type="hidden" name="check_captcha" value="true" />
    </div>
  </div>
  <div class="extra-space"></div>


<div class="span6 omega" style="float: left; !important" >
      <input class="btn btn-primary omega" style="float: left; !important" data-loading-text="Submitting.." id="helpdesk_ticket_submit" name="commit" type="submit" value="Send Feedback">
    </div>
    </div>
</div> 
  <div style="float:right; width:495px;" class="contactDetails_wrapper">
 <h3>Call Us:</h3> 
  <p>Customer Care +91- 9818011567<br /></p>
  <p> Timing: 9 AM to 6 PM - Mon to Sat <br />
  <span>(Standard Calling Charges Apply)</span>
  </p> 
   <br />   <p> Marketing Alliances - marketing@dogspot.in </p>
  <br />
  <h3>Mail Us:</h3>
  <table><tr>
  <td style="padding-left:90px;">
  Om Sai Complex, First Floor,<br /> 
Plot No. 37/19/22, <br />
Behind IGL CNG Petrol Pump,<br /> 
Kapashera, New Delhi-110037
</td></tr></table>
  <br /><h3>Please Note:  </h3>
  <p>DogSpot.in is an open platform for information sharing and is not involved in
the sale or purchase of puppies and dogs. The users can interact with each other
directly for adoption, sale or purchase of Puppies and Dogs. DogSpot.in does not
undertake any responsibility for Transactions made based on information posted
on the website.</p>
  </div>
</div>

<style>
.modal-header {
    width: 100%;
    display: inline-block;
    top: 0;
    padding-bottom: 10px;
    background: transparent;
    padding: 6px 0;
    border-bottom: 1px solid #999;
}
</style>


<script type="text/javascript" src="/ckeditor/ckeditor_basic.js"></script>
<script>
 CKEDITOR.replace( 'helpdesk_ticket_ticket_body_attributes_description_html',
                        {
                            toolbar : 'Basic', /* this does the magic */
                            uiColor : '#9AB8F3'
                        });
</script>
<script type="text/javascript">
  $(document).ready(function() {


      $('#helpdesk_ticket_submit').click(function(e){
        e.preventDefault();


        var helpdesk_ticket_email = $("#helpdesk_ticket_email").val();
		 var helpdesk_ticket_mobile = $("#helpdesk_ticket_mobile").val();
		
        var helpdesk_ticket_subject = $("#helpdesk_ticket_subject").val();
    //    var desc = $("#cke_contents_helpdesk_ticket_ticket_body_attributes_description_html").val();
	 var desc =$("#cke_contents_helpdesk_ticket_ticket_body_attributes_description_html iframe").contents().find("body").text();
     //   var message = $("#recaptcha-accessible-status").html();
		//var message1 = $("#recaptcha-accessible-status").text();
		var vali=validateEmail(helpdesk_ticket_email);
		  var count = $('#helpdesk_ticket_mobile').val().length;
	//  alert(count);
    if(count==10){
   
		if(grecaptcha.getResponse()){
       if(vali) { 
       if(helpdesk_ticket_email && helpdesk_ticket_subject && desc && helpdesk_ticket_mobile){
        $.ajax({
            type: "POST",
            url: "/formProcess.php",
            dataType: "json",
            data: {helpdesk_ticket_email:helpdesk_ticket_email, helpdesk_ticket_subject:helpdesk_ticket_subject, desc:desc,helpdesk_ticket_mobile:helpdesk_ticket_mobile},
            success : function(data){
				//alert(data);
                if (data.code == "200"){
					
                     $("#displaymsg").html("<ul><h3><center>"+data.msg+"</center></h3></ul>");
                    $("#displaymsg").css("display","block");
					 $("#leftside").css("display","none");
					 $("html, body").animate({ scrollTop: 0 }, "slow");
                  return false;
					
                } else {x
                    $(".display-error").html("<ul>"+data.msg+"</ul>");
                    $(".display-error").css("display","block");
                }
            }
        });
	   }else if(helpdesk_ticket_email== "")
	   {
	//	
	 
		 $("#helpdesk_ticket_email").css("border-color","red");
		 $("#form-validate").css("display","block");  
		// event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
	   }
	   else if(helpdesk_ticket_mobile== "")
	   {
	//	
	 
		 $("#helpdesk_ticket_mobile").css("border-color","red");
		 $("#form-validate").css("display","block");  
		// event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
	   }
	   else if(helpdesk_ticket_subject=="")
	   {
	      $("#helpdesk_ticket_subject").css("border-color","red");
		 $("#form-validate").css("display","block");  
	   
	   //event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
		}
	   else if(desc=="")
	   {
	      $("#helpdesk_ticket_ticket_body_attributes_description_html").css("border-color","red"); 
		 $("#form-validate").css("display","block");  
	   
	   $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
		
		}
		}else
		{
				$("#helpdesk_ticket_email").val('');
		$("#helpdesk_ticket_email").css("border-color","red");
		 $("#email-validate").css("display","block");	
		$("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
		
		}
		}else
		{
		$("#form-recapchavalidate").css("display","block"); 
		
		$("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
		
		}
	}else
	{
	$("#helpdesk_ticket_mobile").css("border-color","red");	
	$("#form-mobilevalidate").css("display","block"); 
				
	}
      });
	   function validateEmail($email) {
	//   alert($email);
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  //alert(emailReg.test( $email ));
  return emailReg.test( $email );
}
 
$("#helpdesk_ticket_mobile").keypress(function (e) {
	  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
});
 
  });
  

</script>


<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
