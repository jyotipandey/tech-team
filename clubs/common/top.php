<? 
//Empty sesstion array if it is not photos secton..
//require becaues we user can also come from different page.
if($sitesection != "photos"){
	$image_session_array = "";
	$BkUrl = "";
}
// END-------------------------------------------------------
include($DOCUMENT_ROOT."/chquserstat.php");
// Record user
$display_user_Name = dispUname($userid);
$ip = ipCheck();
recordOnlineUser($userid, $display_user_Name, $ip, $requested);
// Record user END
//Remove User/
removeOnlineUsers();
//Remove User/END
include($DOCUMENT_ROOT."/common/savesocialsettings.php");
?>
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Blogs" href="https://feeds.feedburner.com/dogspotblog" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Q&A" href="https://feeds.feedburner.com/DogspotinQna" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Gallery " href="https://feeds.feedburner.com/DogspotDogs" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Puppies Available" href="https://feeds.feedburner.com/DogspotPuppies" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/css/linkbutton.css" rel="stylesheet" type="text/css" />
<link href="/cssun/popup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/popup.js"></script> 
<script type="text/javascript">
<!--
window.onload=function(){
<? if($sitesection == "photos"){?>
	uploadPhotos();
<? }?>	
<? if($sitesection == "puppynew"){ ?>
	ajaxload();
	loadShapopup();
	
<? } ?>
<? if($sitesection == "HOME" || $sitesection == "puppie"){ ?>
	puppyAds();
	adsTop();
<? } ?>	
<? if($sitesection == "articles-new"){ ?>
	var sBasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('_samples')) ;

	var oFCKeditor = new FCKeditor( 'art_body' ) ;
	oFCKeditor.BasePath = '/fckeditor/' ;
	oFCKeditor.ReplaceTextarea() ;
<? }?>
<? if($sitesection == "microsite-new"){ ?>
	var sBasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('_samples')) ;

	var oFCKeditor = new FCKeditor( 'section_body' ) ;
	oFCKeditor.BasePath = '/fckeditor/' ;
	oFCKeditor.ReplaceTextarea() ;
<? }?>
<!--Social Popup BOX-->
<? if($sitesection == "qna" || $sitesection == "articles"){?>
<? if($sessionSocialaction==0){?>
<? if($sessionFacebook || $sessionTwitter){?>
	var popupdata = '<h2>Publish on Social Network</h2>';
	<? if($sessionFacebook){?>
	popupdata=popupdata+'<p style="margin-left:20px"><input name="facebook" type="checkbox" id="facebook" value="f" checked="checked" /> Tell <img src="/images/facebook-icon.jpg" alt="facebook" width="64" height="23" align="absmiddle"></p>';
	<? } if($sessionTwitter){?>
	popupdata=popupdata+'<p style="margin-left:20px"><input name="twitter" type="checkbox" id="twitter" value="t" checked="checked" /> Tell <img src="/images/twitter-con.jpg" alt="Twitter" width="99" height="25" align="absmiddle"></p>';
	<? }?>
	popupdata=popupdata+'<p><input name="social_action" type="checkbox" id="social_action" value="1" /> Never ask me this again. (save my action for the next time)</p><a href="javascript:closeSubmit();" class="grayButton"><span>Publish</span></a><input name="savesocial" type="hidden" id="save" value="savesocial" />';
	document.getElementById('poplight').innerHTML = popupdata;
<? }else{?>
	var popupdata = '<h2>Publish on Social Network</h2><p>Connect with <a href="/facebookconnect/"><img src="/images/facebook-icon.jpg" alt="facebook" width="64" height="23" border="0" align="absmiddle"></a></p><p>Connect with <a href="/twitterconnect/"><img src="/images/twitter-con.jpg" alt="Twitter" width="99" height="25" border="0" align="absmiddle"></a></p><input name="socialconnect" type="hidden" id="socialconnect" value="0">';
	document.getElementById('poplight').innerHTML = popupdata;
<? } if($socialconnect == "0"){ ?>
$('').popOpen();
<? }}}?>
<!--Social Popup BOX END-->
}
// -->
</script>
<script type="text/javascript">
function clickclear(thisfield, defaulttext) {
if (thisfield.value == defaulttext) {
thisfield.value = "";
}
}
function clickrecall(thisfield, defaulttext) {
if (thisfield.value == "") {
thisfield.value = defaulttext;
}
}
</script>
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript" src="/js/highslide-html.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
    hs.outlineType = 'rounded-white';
    hs.outlineWhileAnimating = true;
    hs.objectLoadTime = 'after';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<link href="/css/top-navi.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/top-navi.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1552614-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<? if($sitesection == "friends"){?>
<body>
<? }elseif($sitesection == "msg"){?>
<body onLoad="LoadHAjax()">
<? }elseif($ajaxbody == "HOME"){?>
<body>
<? }elseif($ajaxbody == "puppie"){?>
<body>
<? }elseif($ajaxbody == "dog"){ ?>
<body>
<? }elseif($ajaxbody == "dognew"){ ?>
<body onLoad="ajaxload();">
<? }elseif($ajaxbody == "puppynew"){ ?>
<body>
<? }elseif($ajaxbody == "cat"){ ?>
<body >
<? }else{
echo"<body>";
}
?>
<?
if($sitesection == "HOME"){
//echo"<div id='mainHome'>";
echo"<div id='main'>";
}else{
 echo"<div id='main'>";
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td width="170" rowspan="2"><a href="https://www.dogspot.in/"><img src="/images/dogspot-logo-2.jpg" alt="DogSpot Spot for all your Dogs Needs" width="167" height="57" border="0"></a></td>
  <td>
   <? if($sitesection != "reg-log"){?>  
  <div id="userstat">Welcome! <span class="redbtext"><? if ($userid != "Guest" && $sessionName){  print getTeaser(dispUname($userid),20); echo"<br /><a href = '/home.php?logoff=logoff'>Logout</a> | <a href = '/profile/'>My Profile</a> <br></span>"; }else {?> Guest </span>
    <img src="/images/trans.gif" alt="trans" width="2" height="4" /><br />
      New User? <span class="redbtext"><a href="/reg-log/register-1.php">Register</a> | <a href="/login.php" >Login</a></span><br />   <? } ?>
</div>
<? } ?>
 
    <div id="tsearch">
 <!-- SiteSearch Google -->
    <form action="https://www.dogspot.in/search.php" id="cse-search-box">
        <input type="hidden" name="cx" value="partner-pub-7080808620760591:8w0nse-xhme" />
        <input type="hidden" name="cof" value="FORID:11" />
        <input type="hidden" name="ie" value="ISO-8859-1" />
        <input type="text" name="q" size="38" value="Search DogSpot" onClick="clickclear(this, 'Search DogSpot')" onBlur="clickrecall(this,'Search DogSpot')" />
        <input type="submit" name="sa" value="Search" class="searchBut"/>
    </form>
<!-- SiteSearch Google -->
  </div>
  
</td>
  </tr>
  <tr>
    <td></td>
  </tr>
  
</table>
<div id="topNaviLine"></div>

