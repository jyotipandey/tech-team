<?
require_once($DOCUMENT_ROOT.'/session-require.php'); 
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require($DOCUMENT_ROOT."/classCtrl/dbClassCtrl.php");

if($sessionLevel == 0){
	header("location: /");
	exit();
}

if($delClub){
	$updateResult = query_execute("DELETE FROM club_section WHERE club_id ='$club_id'");
	$deleteResult = query_execute("DELETE FROM club_main WHERE club_id ='$club_id'");
	header("location: /clubs/index.php");
}

$sitesectionMicro = "home";
$sqlobj = new MySqlClass();

$data_fields          = array('*');
$data_where_condition = array('club_id >' => 0);
$clubsData = $sqlobj -> getData("club_main", $data_fields, $data_where_condition, "club_name ASC");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<title>MicroSite</title> 
<meta content="" name="keywords">
<meta content="" name="description">
<link href="css/index_txt.css" rel="stylesheet" type="text/css" />
<link href="/clubs/css/top-navi-micro.css" rel="stylesheet" type="text/css" />
<link href="/css/linkbutton.css" rel="stylesheet" type="text/css" />
<script language="javascript">
function confirmDelete(delUrl) {
  if (confirm("Are you sure you want to delete")) {
    document.location = delUrl;
  }
}
</script>
<?php require_once($DOCUMENT_ROOT.'/clubs/common/top.php'); ?>
<div id="pagebody">
<div id="MicroSite">
  <h1>Micro Sites (DogSpot.in)</h1>
  
<ul>
<? for($i=0; $i < count($clubsData); $i++){ ?>
  <li><a href="/<? echo $clubsData[$i]['club_nicename']; ?>/"><? echo snippetwop($clubsData[$i]['club_name'], $length=50, $tail="");?></a> &nbsp; &nbsp; &nbsp;
  	  <a href="/clubs/admin/index.php?clbId=<? echo $clubsData[$i]['club_id'];?>">Edit</a> <a href="javascript:confirmDelete('/clubs/index.php?delClub=delClub&club_id=<? echo $clubsData[$i]['club_id']; ?>')">Delete</a></li>
<? } ?>
</ul>
<p><a href="/clubs/admin/new-club.php" class="grayButton"><span>Create new Mocrosite</span></a></p>
<div id="clearall"></div>  
</div>
</div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>