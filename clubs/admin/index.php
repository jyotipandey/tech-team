<?
require_once($DOCUMENT_ROOT.'/session-require.php'); 
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require($DOCUMENT_ROOT."/classCtrl/dbClassCtrl.php");

//----------------------------------------------------------------------------------------------
function createTree($arraySection, $totrecord, $startTree, $dash, $extvariable){
	for($j=0;$j<$totrecord;$j++){
		if($arraySection[$j][1]==$startTree){
			echo"<li id='arrayorder_".$arraySection[$j][0]."' class='ui-state-default ui-corner-all'> $dash <a href='new.php?section_id=".$arraySection[$j][0]."&club=".$club."'>".$arraySection[$j][0]." ".$arraySection[$j][2]."</a> ( <a href='new.php?section_id=".$arraySection[$j][0].$extvariable."'>edit</a> ";

?>
		<a href="javascript:confirmDelete('/clubs/admin/index.php?delSec=delSec&section_id=<? echo $arraySection[$j][0].$extvariable; ?>')">delete</a> )<li />
  <?
			if($arraySection[$j][0]){
			 createTree($arraySection, $totrecord, $arraySection[$j][0], $dash.'--', $extvariable);	
			}
		}
	}	
}//----------------------------------------------------------------------------------------------
function startTree($arraySection, $num_rows, $dash, $extvariable){
	for($i=0;$i<$num_rows;$i++){
		if($arraySection[$i][1]==0){
			echo "<li id='arrayorder_".$arraySection[$i][0]."' class='ui-state-default ui-corner-all'><a href='new.php?section_id=".$arraySection[$i][0]."&club=".$club."'>".$arraySection[$i][0]." ".$arraySection[$i][2]."</a> ( <a href='new.php?section_id=".$arraySection[$i][0].$extvariable."'>edit</a>";
?>
		<a href="javascript:confirmDelete('/clubs/admin/index.php?delSec=delSec&section_id=<? echo $arraySection[$i][0].$extvariable; ?>')">delete</a> )</li>
        <?
			if($arraySection[$i][0]){
			 createTree($arraySection, $num_rows, $arraySection[$i][0], $dash, $extvariable);
			}
    	}   
	}
}
	
if($delSec){
	$updateResult = query_execute("UPDATE club_section SET parent_section_id = '0' WHERE parent_section_id = '$section_id'");
	$deleteResult = query_execute("DELETE FROM club_section WHERE section_id ='$section_id'");
	header("location: /clubs/admin/index.php?clbId=$clbId");
}

$sitesectionMicro = "home";
$sqlobj = new MySqlClass();

$rtn_row_club = $sqlobj -> getSingleRow("club_id, club_name, club_nicename", "club_main", "club_id = '$clbId'");
$club = $rtn_row_club['club_id'];

// Array for Top Navigation 
$selectArtall = query_execute("SELECT section_id, parent_section_id, section_name, section_nice_name FROM club_section WHERE club_id = '$clbId' AND section_navigation = 'top' ORDER BY section_order");
$num_rows = mysql_num_rows($selectArtall);
while($row_sec = mysql_fetch_array($selectArtall)){
 $catID = $row_sec['section_id'];
 $parent_category_id = $row_sec['parent_section_id'];
 $category_name = $row_sec['section_name'];
 
 $arraySection[] = array("$catID","$parent_category_id","$category_name");
 }

// Array for Left Navigation 
$selectArtallLeft = query_execute("SELECT section_id, parent_section_id, section_name, section_nice_name FROM club_section WHERE club_id = '$clbId' AND section_navigation = 'left' ORDER BY section_order");
$num_rowsLeft = mysql_num_rows($selectArtallLeft);
while($row_secLeft = mysql_fetch_array($selectArtallLeft)){
 $catIDLeft = $row_secLeft['section_id'];
 $parent_category_idLeft = $row_secLeft['parent_section_id'];
 $category_nameLeft = $row_secLeft['section_name'];
 
 $arraySectionLeft[] = array("$catIDLeft","$parent_category_idLeft","$category_nameLeft");
 }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><? echo $rtn_row_club['club_name'];?> Edit MicroSite Details</title>
<script src="/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/jquery/jquery-ui.js"></script>

<link href="/clubs/css/admin.css" rel="stylesheet" type="text/css"/>
<link href="/css/linkbutton.css" rel="stylesheet" type="text/css" />
<script language="javascript">
$(document).ready(function() {
	$("li:empty").remove();
	
	function slideout(){
 		setTimeout(function(){
 		$("#response").slideUp("slow", function () {
       });
    
	}, 2000);}
	$("#response").hide();
    $("#sortable").sortable({opacity: 0.8, cursor: 'move',  
		update: function() {
			var order = $(this).sortable("serialize") + '&update=update';
			$.post("updateList.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			});
		}
	});
	$("#sortableLeft").sortable({opacity: 0.8, cursor: 'move',  
		update: function() {
			var order = $(this).sortable("serialize") + '&update=update';
			$.post("updateList.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			});
		}
	});
});

function confirmDelete(delUrl) {
  if (confirm("Are you sure you want to delete")) {
    document.location = delUrl;
  }
}
</script>
<?php //require_once($DOCUMENT_ROOT.'/clubs/common/top.php'); ?>
<div id="DivAdmin">
<div id="response" class="ui-corner-all"></div>
<h1><? echo $rtn_row_club['club_name'];?></h1>

<a href="/clubs/admin/new-club.php?c_id=<? echo"$club";?>" class="grayButton"><span>Edit Site Info</span></a> <a href="new.php?club=<? echo $club;?>" class="grayButton"><span>Create new Section</span></a> <a href="<? echo '/'.$rtn_row_club['club_nicename'].'/';?>" class="grayButton"><span>View Site</span></a>

<br /><br />
<strong>Top Nevigation</strong> <span class="graysmalltext">(Rearrange the section by simply dragging it up down)</span>
<div id="container">
  <div id="list">
<ul id="sortable">
<? print(startTree($arraySection, $num_rows, '--', "&club=$club"));?>
</ul>
</div></div>
<br /><br />
<strong>Left Nevigation</strong> <span class="graysmalltext">(Rearrange the section by simply dragging it up down)</span>
<div id="container">
  <div id="list">
<ul id="sortableLeft">
<? print(startTree($arraySectionLeft, $num_rowsLeft, '--', "&club=$club"));?>


</ul>
</div></div>
</div>
<?php //require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>