<?php
require_once($DOCUMENT_ROOT.'/session-require.php'); 
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
$sitesection = "microsite-new";
require($DOCUMENT_ROOT."/classCtrl/dbClassCtrl.php");

$sqlobj = new MySqlClass();

$return_Data = "9953023991"; //default set value

/* Insert Query */
if(isset($_POST['newpost'])){

$section_nice_name = createSlug($section_name);
$section_nice_name = checkSlugAllSection('club_section', 'section_nice_name', $section_nice_name, " AND club_id='$club_id'");

if($club_home==1){
	$updateHome0 = query_execute("UPDATE club_section SET club_home = '0' WHERE club_id = '$club_id'");
}

$data_fields	= array('parent_section_id' => $parent_section_id, 'club_id' => $club_id, 
'section_navigation' => $section_navigation, 'section_name' => mysql_real_escape_string($section_name), 
'section_nice_name' => $section_nice_name, 'section_title' => mysql_real_escape_string($section_title), 
						'section_title_tag' => mysql_real_escape_string($section_title_tag), 
						'section_body' => mysql_real_escape_string($section_body), 'club_home' => $club_home, 'section_status' => $section_status, 'section_cdate' => NULL, 'section_edate' => NULL);
$return_Data = $sqlobj -> insertRecord("club_section", $data_fields);

}
/*move header if record Insert/Update successfully  */
if($return_Data[0] == 1){
	header("location: /clubs/admin/index.php?clbId=$club_id");
	exit();
}elseif($return_Data[0] == 0){
	$errmsg = "Data not Insert/Update successfully...! <br /> Try again...";	
}
/*move header if record Insert/Update successfully */
/* Insert Query End */

/* update Record */
if(isset($_POST['editpost'])){
	if($club_home==1){
	$updateHome0 = query_execute("UPDATE club_section SET club_home = '0' WHERE club_id = '$club_id'");
}
$data_fields_update	= array('parent_section_id' => $parent_section_id, 'club_id' => $club_id, 'section_navigation' => $section_navigation, 
'section_name' => mysql_real_escape_string($section_name), 'section_title' => mysql_real_escape_string($section_title), 
						'section_title_tag' => mysql_real_escape_string($section_title_tag), 
						'section_body' => mysql_real_escape_string($section_body), 'club_home' => $club_home, 'section_status' => $section_status, 'section_edate' => NULL);

$data_fields_where  = array('section_id = ' => $section_id);

$return_Dataup = $sqlobj -> updateRecord("club_section",$data_fields_update,$data_fields_where);

/*move header if record Insert/Update successfully  */
if($return_Dataup == 1){
	header("location: /clubs/admin/index.php?clbId=$club_id");
	exit();
}elseif($return_Dataup == 0){
	$errmsg = "Data not Insert/Update successfully...! <br /> Try again...";	
}
/*move header if record Insert/Update successfully */
}
/* update Record End */

/* Select Data for edit */
if($section_id && $club){

$rtn_row_editData = $sqlobj -> getSingleRow("*", "club_section", "club_id = '$club' AND section_id = '$section_id'");
  //$cId = $rtn_row_editData['club_id'];
  //$cat_idEdit = $rtn_row_editData['cat_id'];
  $clbSectionId = $rtn_row_editData['section_id'];
  
	$rtnRowUserId = $sqlobj -> getSingleRow("userid, club_nicename", "club_main", "club_id = $club");
	$postUserID = $rtnRowUserId['userid'];
  
if($sessionLevel == 0){
 if($postUserID != $userid){ //compare with session user id
	header ("Location: /");
	exit();
 }
}

}

$cat_idEdit = ($cat_idEdit ? $cat_idEdit : $cat_id);
/* Select Data for edit End */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Create new Microsite Section</title>
<script src="https://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="https://dev.jquery.com/view/trunk/plugins/validate/jquery.validate.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	  $("#formSection").validate({
		  rules: {
			section_title: {
				required: true,
				minlength: 2
			},
			section_navigation:	"required",
			parent_section_id:	"required",
			section_name:	"required",
			section_body:	"required"
		  }
		 
		  /*submitHandler: function() { 
			ShaAjaxJquary("emailNewsletter.php", "#DivSendNews", '', 'formNewsletter', 'POST', 'DivSendNewsLoading', '<img src="/image/loading-round.gif" />','REP');
		  }*/
	  });
});	
</script>
<?php require_once($DOCUMENT_ROOT.'/clubs/common/top.php'); ?>

<div id="MicroSite">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
   <td valign="top"><div id="inbody">
    <p class="sitenavi"><a href="/">Home</a> &gt; Section</p>
<form id="formSection" name="formSection" method="post" action="<?php echo $PHP_SELF; ?>" onsubmit="return submitForms()">

<? echo ($errmsg ? "<div class='redbtext'>$errmsg</div>" : ""); ?>
<div class="dtdl">
 <label>Title:</label> 
 <input name="section_title" tabindex="1" type="text" class="textWrite" id="section_title" value="<? echo 
 stripcslashes(htmlspecialchars_decode($rtn_row_editData['section_title']));?>" size="65" />
 <div id="DivTitle" class="redbtext"></div>
</div>

<div class="dtdl"><label>Title Tag:</label> 
 <input name="section_title_tag" tabindex="1" type="text" class="textWrite" id="section_title_tag" value="<? echo stripcslashes(htmlspecialchars_decode($rtn_row_editData['section_title_tag']));?>" size="65" />
  <div id="DivTags" class="redbtext"></div>
</div>
<div class="dtdl"><label>Navigation:</label> 
  <input name="section_navigation" type="radio" id="section_navigation" value="left" <? if($rtn_row_editData['section_navigation'] == 'left'){echo'checked="checked"';}?> />
  Left 
  <input type="radio" name="section_navigation" id="section_navigation" value="top" <? if($rtn_row_editData['section_navigation'] == 'top'){echo'checked="checked"';}?> /> 
  Top
<label for="section_navigation" class="error" style="display:none" >Please select navigation.</label>
</div>



<div class="dtdl"> <label>Parent Section:</label>
  <select name="parent_section_id" id="parent_section_id" tabindex="2">
    <option value="0">Select Parent Section:</option>
    <?
    $rtn_data_select = $sqlobj -> getAllRows("section_id, section_name", "club_section", "WHERE club_id = '$club'");
	  while($rowData = mysql_fetch_array($rtn_data_select)){
		$section_name = $rowData['section_name'];
		$section_id = $rowData['section_id'];
    ?>
    <option value="<? echo"$section_id";?>" <? echo ($section_id == $rtn_row_editData['parent_section_id'] ? "selected='selected'" : "");?>><? echo $section_name;?></option>
    <? } ?>
 </select>
  <div id="DivCat" class="redbtext"></div>
</div>
 <div class="dtdl"> <label>Section Name:</label>
   <input name="section_name" type="text" id="section_name" value="<? echo stripcslashes(htmlspecialchars_decode($rtn_row_editData['section_name']));?>" size="60" />
 </div>
 <div class="dtdl"> <label>Section Status:</label>
   <select name="section_status" id="section_status">
     <option value="publish" <? echo ($rtn_row_editData['section_status'] == 'publish' ? "selected='selected'" : "");?>>Publish</option>
     <option value="draft" <? echo ($rtn_row_editData['section_status'] == 'draft' ? "selected='selected'" : "");?>>Draft</option>
   </select>
 </div>
 <div class="dtdl"> <label>Site Home Page:</label>
   <input name="club_home" type="checkbox" id="club_home" value="1" <? echo ($rtn_row_editData['club_home'] == '1' ? "checked='checked'" : "");?> />
 </div>
<div class="dtdl">
  <label>Body:</label>
  <textarea name="section_body" id="section_body" tabindex="3" cols="60" rows="5" class="ckeditor">
  <? echo htmlspecialchars_decode(stripallslashes($rtn_row_editData['section_body']));?></textarea>
 <div id="DivBody" class="redbtext"></div>
</div>

<div class="dtdl"></div>
<div class="dtdl">
    <input name="clubNiceName" type="hidden" id="clubNiceName" value="<? echo $rtnRowUserId['club_nicename'];?>" />
  <? if($clbSectionId){ ?> 
    <input name="editpost" type="submit" class="but" id="editpost" value=" Edit section " />
    <input name="section_id" type="hidden" id="section_id" value="<? echo"$clbSectionId";?>" />
  <? }else{ ?>
    
    <input name="newpost" type="submit" class="but" id="newpost" value="Create new section" />
  <? } ?>
  <input name="club_id" type="hidden" id="club_id" value="<? echo"$club";?>" />
</div>
</form>
              
</div>
     </td>
     
    
  </tr>
</table>
</div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>