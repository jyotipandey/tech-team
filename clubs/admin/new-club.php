<?php
require_once($DOCUMENT_ROOT.'/session-require.php'); 
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

require($DOCUMENT_ROOT."/classCtrl/dbClassCtrl.php");

$sqlobj = new MySqlClass();

$return_Data = "9953023991"; //default set value

/* Insert Query */
if($newpost){
if($club_nicename){
	$club_niceName = createSlug($club_name);
	$clubNiceName = checkSlugAll("pages", "club_id != '$c_id' AND nice_name", $club_niceName);
}


/*$data_fields	= array('userid' => $userid, 'club_name' => mysql_real_escape_string($club_name), 
						'club_nicename' => $clubNiceName, 'club_logo' => $club_logo_file_name, 'club_ad' => $club_ad_file_name,
						 'club_rightnavi' => mysql_real_escape_string($club_rightnavi));*/
$data_fields	= array('userid' => $userid, 'club_name' => mysql_real_escape_string($club_name), 
'club_nicename' => $clubNiceName, 'club_logo' => $club_logo_file_name, 'club_ad' => $club_ad_file_name,
'club_rightnavi' => mysql_real_escape_string($club_rightnavi),'header_yes' => $header_yes,'bottom_yes' => $bottom_yes);
$return_Data = $sqlobj -> insertRecord("club_main", $data_fields);

$data_pages=array('nice_name' => $clubNiceName, 'section_name' => 'microsite', 'section_id' => $return_Data[1]);

$return_Data_pages = $sqlobj -> insertRecord("pages", $data_pages);

if($return_Data_pages[0] == 1){
	header("location: /clubs/admin/index.php?clbId=$return_Data[1]");
	exit();
}elseif($return_Data_pages[0] == 0){
	$errmsg = "Data not Insert/Update successfully...! <br /> Try again...";	
}
}
/* Insert Query End */

/* update Record */
if($editpost){
if(!$club_nicename){
	$club_niceName = createSlug($club_name);
	$clubNiceName = checkSlugAll("pages", "club_id = '$c_id' AND nice_name", $club_niceName);
}else{
	$club_niceName = createSlug($club_nicename);
	$clubNiceName = checkSlugAll("pages", "club_id = '$c_id' AND nice_name", $club_niceName);
}

$data_fields_update	= array('club_name' => mysql_real_escape_string($club_name), 'club_logo' => $club_logo_file_name, 
							'club_nicename' => $clubNiceName, 'club_ad' => $club_ad_file_name, 'club_rightnavi' => mysql_real_escape_string($club_rightnavi),'header_yes' => $header_yes,'bottom_yes' => $bottom_yes);

$data_fields_where  = array('club_id = ' => $c_id);
$return_Data = $sqlobj -> updateRecord("club_main", $data_fields_update, $data_fields_where);

$data_pages=array('nice_name' => $clubNiceName);
$data_fields_where_page  = array('section_id = ' => $c_id);
$return_Data_pages = $sqlobj -> updateRecord("pages", $data_pages, $data_fields_where_page);
/*move header if record Insert/Update successfully  */
header("location: /$clubNiceName/");
exit();
/*move header if record Insert/Update successfully */
}
/* update Record End */




/* Select Data for edit */
//$c_id = 1;
if($c_id){

if($sessionLevel == 0){
	$whereFields = "club_id = '$c_id' AND userid = '$userid'"; 
}else{
	$whereFields = "club_id = '$c_id'"; 
}
$rtn_row_editData = $sqlobj -> getSingleRow("*", "club_main", $whereFields);
 //print_r($rtn_row_editData);
  $cId = $rtn_row_editData['club_id'];
  $header_yes = $rtn_row_editData['header_yes'];
  $bottom_yes = $rtn_row_editData['bottom_yes'];
  //$p_img_overRight = $rtn_row_editData['club_logo'];
 // $p_ad_overRight = $rtn_row_editData['club_ad'];
  $img_dir = $DOCUMENT_ROOT."/clubs/images/";
}
/* Select Data for edit End */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Create New MicroSite</title>
<script type="text/javascript" src="/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script language="javascript" type="text/javascript">
function createnew(){
	var dclass = $("#club_name").val();
	if(dclass==""){
		$("#club_name").css({ "border": "Solid red 2px" });
	}else{
		postForm();
	}
}	
</script>

<!--Javascript for logo nd bannar Upload -->
<script type='text/javascript'>
function postForm(){
	document.formClub.action="new-club.php";
	document.formClub.target="";
	document.formClub.submit();
}
function uploadImg(){
	document.formClub.action="upload-img.php";
	document.formClub.target="upload_target";
	document.formClub.submit();
	document.getElementById('loading').innerHTML = '<img src="/images/indicator.gif" alt="Loading" />';
}
function stopUpload(pageHtmldata){
	document.getElementById('loading').innerHTML = "";
	document.getElementById("loadingMsg").innerHTML = pageHtmldata;
}

function uploadBnr(){
	document.formClub.action="upload-bnr.php";
	document.formClub.target="upload_target";
	document.formClub.submit();
	document.getElementById('loadingBnr').innerHTML = '<img src="/images/indicator.gif" alt="Loading" />';
}
function stopUploadBnr(pageHtmldata){
	document.getElementById('loadingBnr').innerHTML = "";
	document.getElementById("loadingBnrMsg").innerHTML = pageHtmldata;
}
</script>
<!--Javascript for logo nd bannar Upload End-->

<link href="/clubs/css/admin.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/clubs/common/top.php'); ?>
<div id="DivAdmin">
<form id="formClub" name="formClub" method="post" enctype="multipart/form-data" action="<?php echo $PHP_SELF; ?>">

<h1>Micro Site details</h1>
<? echo ($errmsg ? "<div class='redbtext'>$errmsg</div>" : ""); ?>
<table width="100%" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="200" align="right" valign="top"><span class="redbtext">*</span><strong>Micro Site  Name:</strong></td>
    <td><input name="club_name" type="text" id="club_name" size="60" value="<? echo stripslashes($rtn_row_editData['club_name']);?>" /><br />

    	 <div id="DivName" class="redbtext"></div>
    </td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong>Micro Site Nice Name:</strong></td>
    <td><input name="club_nicename" type="text" id="club_nicename" value="<?=$rtn_row_editData['club_nicename'];?>" size="40" />
    <br /><span class="graysmalltext">Nice name no space, no special characters ex (dog-club-india)</span></td>
  </tr>
  <tr>
    <td align="right" valign="top"><span class="redbtext">*</span><strong>Micro Site Logo:</strong></td>
    <td><!--<input name="club_logo" type="text" id="club_logo" size="80" value="<? echo stripallslashes($rtn_row_editData['club_logo'])?>" /> -->
<div class="required">
  <div id="loading">
        <input type="file" name="club_logo" id="club_logo" onchange="uploadImg();" />
        <br /><span class="graysmalltext">Logo (Type: jpg, jpeg, gif) (Width: 200px Height 125px)</span>
    <? if(file_exists($img_dir.$rtn_row_editData['club_logo'])){	?> <br />
  	    <input name='club_logo_file_name' type='hidden' id='club_logo_file_name' value='<? echo stripallslashes($rtn_row_editData['club_logo']) ?>' />
        <div style="margin:2px; float:left; border:solid; border-width:1px; border-color:#666; padding:2px">
        <img src="/clubs/images/<? echo stripslashes($rtn_row_editData['club_logo']);?>" alt="image">
        </div>
	<? } ?>
  </div>
 <div id="loadingMsg"><? if(!file_exists($img_dir.$rtn_row_editData['club_logo'])){	?><input name='club_logo_file_name' type='hidden' id='club_logo_file_name' value='' /><? }?></div>
 <div id="Divclear"></div>
</div>
    </td>
  </tr>
  
  <tr>
    <td align="right" valign="top"><strong>Micro Site Banner:</strong></td>
    <td><!--<input name="club_ad" type="text" id="club_ad" size="80" value="<? echo stripallslashes($rtn_row_editData['club_ad']);?>" />-->
   <div id="loadingBnr">
         <input type="file" name="club_ad" id="club_ad" onchange="uploadBnr();" /><br /><span class="graysmalltext">Banner (Type: jpg, jpeg, gif, swf) (Width: 590px Height 125px)</span>
     <? if(file_exists($img_dir.$rtn_row_editData['club_ad'])){	?> <br />
  	    <input name='club_ad_file_name' type='hidden' id='club_ad_file_name' value='<? echo stripallslashes($rtn_row_editData['club_ad']); ?>' />
        <div style="margin:2px; float:left; border:solid; border-width:1px; border-color:#666; padding:2px">
        <img src="/clubs/images/<? echo $rtn_row_editData['club_ad'];?>" alt="image">
        </div>
	<? } ?>
  </div>
  <div id="loadingBnrMsg"></div>
    </td>
  </tr>
  <tr>
  <td align="right">
<div class="dtdl">

<label>Header</label> </div></td>
<td >
  <input name="header_yes" type="radio" <? if($header_yes=='yes') {?> checked="checked"<? }?> id="header_yes" value="yes"  />
  Yes
  <input type="radio" name="header_yes" <? if($header_yes=='no') {?> checked="checked"<? }?> id="header_yes" value="no"  /> 
  No
<label for="section_header" class="error" style="display:none" >Please select Header.</label>
</td>
</tr>
<tr>
<td align="right"><div class="dtdl"><label>Footer</label> </div></td>

<td>  <input name="bottom_yes" type="radio" <? if($bottom_yes=='yes') {?> checked="checked"<? }?> id="bottom_yes" value="yes"  />
  Yes
  <input type="radio" name="bottom_yes"  <? if($bottom_yes=='no') {?> checked="checked"<? }?> id="bottom_yes" value="no"  /> 
  No
<label for="section_header" class="error" style="display:none" >Please select Bottom.</label>
</td></tr>
  
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong>Right Navigation: </strong></td>
    <td><textarea name="club_rightnavi" id="club_rightnavi" cols="45" rows="5" class="ckeditor"><? echo stripallslashes($rtn_row_editData['club_rightnavi']);?></textarea></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>
	  <? if($c_id){ ?>
	  <input name="editpost" type="hidden" id="editpost" value="editpost" />

<a href="#" onclick="javascript:createnew();" class="but">Save Changes</a>
        <input name="clubNiceName" type="hidden" id="clubNiceName" value="<? echo $rtn_row_editData['club_nicename'];?>" />
        <input name="c_id" type="hidden" id="c_id" value="<? echo"$c_id";?>" />
      <? }else{ ?>
      <input name="newpost" type="hidden" id="newpost" value="newpost" />
        <!--<input name="newpost" type="submit" class="but" id="newpost" value=" Submit " />-->
        <a href="#" onclick="javascript:createnew();" class="but"> Create New</a>
<? } ?>
    
    </td>
  </tr>
</table>
</form>

<iframe id="upload_target" name="upload_target" src="" style="width:0;height:0;border:0px solid #fff;"></iframe> 
</div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>