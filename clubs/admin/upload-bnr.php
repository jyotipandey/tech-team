<?
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/functions.php');

/*** File uploding ***/
$club_ad_file_name = $_POST["club_ad_file_name"];

//define a maxim size for the uploaded images
define ("MAX_SIZE","5000");

define ("LWIDTH","590");
define ("LHEIGHT","125");

// define the width and height for the thumbnail
// note that theese dimmensions are considered the maximum dimmension and are not fixed, 
// because we have to keep the image ratio intact or it will be deformed

// This variable is used as a flag. The value is initialized with 0 (meaning no error found) 
//and it will be changed to 1 if an errro occures. If the error occures the file will not be uploaded.
$errors = 0;
// checks if the form has been submitted

//reads the name of the file the user submitted for uploading 
$image = $_FILES['club_ad']['name'];
// if it is not empty
if($image){
// get the original name of the file from the clients machine
$filename = stripslashes($_FILES['club_ad']['name']);

// get the extension of the file in a lower case format
$extension = pathinfo($filename, PATHINFO_EXTENSION);
$extension = strtolower($extension);
// if it is not a known extension, we will suppose it is an error, print an error message and will not upload the file, otherwise we continue
if(($extension != "jpg") && ($extension != "jpeg") && ($extension != "gif") && ($extension != "swf")){
	$errorUplod = "Unknown extension! $extension ";
	$errors = 1;
}else{
	// get the size of the image in bytes
	// $_FILES[\'image\'][\'tmp_name\'] is the temporary filename of the file in which the uploaded file was stored on the server
	$size = getimagesize($_FILES['club_ad']['tmp_name']);
	$sizekb = filesize($_FILES['club_ad']['tmp_name']);

	//compare the size with the maxim size we defined and print error if bigger
	if($sizekb > MAX_SIZE*1024){
		$errorUplod = 'You have exceeded the size limit!';
		$errors = 1;
	}
}

if($errors == 0){
	//we will give an unique name, for example the time in unix time format
	$B_img = time();
	$B_img = $B_img.'.'.$extension;
	
	//the new name will be containing the full path where will be stored (images folder)
	$newname = $DOCUMENT_ROOT."/clubs/images/".$B_img;
	$copied = copy($_FILES['club_ad']['tmp_name'], $newname);
	//we verify if the image has been uploaded, and print error instead
	if(!$copied){
		$errorUplod = 'Copy unsuccessfull!';
		$errors = 1;
	}else{		
		if($club_ad_file_name){
			$DelFilePath = $DOCUMENT_ROOT."/clubs/images/".$club_ad_file_name;
			if (file_exists($DelFilePath)){ unlink ($DelFilePath); }
		}
	}
}// END if Error==0
}// if Image is not empty
?>

<input type="file" name="club_ad" id="club_ad" onchange="uploadBnr();" /><br />
<?
if($errors == 1){
	echo $errorUplod;
}else{ ?>
    <input name='club_ad_file_name' type='hidden' id='club_ad_file_name' value='<? echo $B_img; ?>' />
    <div style="margin:2px; float:left; border:solid; border-width:1px; border-color:#666; padding:2px">
    <img src="/clubs/images/<? echo $B_img;?>" title="Bannar">
    </div>
<?
 }
/*** File uploding end ***/
?>

<script language="javascript" type="text/javascript">
	var pageHtmldata = document.body.innerHTML;
	window.top.window.stopUploadBnr(pageHtmldata);
</script>