<?
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');

$session_id = session_id();
$ip = ipCheck();
$secret_key = "6fa889b61869472cb0877491fd8f83c7";	 // Your Secret Key

if(isset($_GET['DR'])) {
	 require($DOCUMENT_ROOT.'/shop/Rc43.php');
	 $DR = preg_replace("/\s/","+",$_GET['DR']);

	 $rc4 = new Crypt_RC4($secret_key);
 	 $QueryString = base64_decode($DR);
	 $rc4->decrypt($QueryString);
	 $QueryString = split('&',$QueryString);

	 $response = array();
	 foreach($QueryString as $param){
	 	$param = split('=',$param);
		$response[$param[0]] = urldecode($param[1]);
	 }
}
// Update in DB----------------------------------------------------------------------------------
if($response){
	$ResponseCode=$response["ResponseCode"];
	$ResponseMessage=$response["ResponseMessage"];
	$DateCreated=$response["DateCreated"];
	$PaymentID=$response["PaymentID"];
	$MerchantRefNo=$response["MerchantRefNo"];
	$Amount=$response["Amount"];
	$Mode=$response["Mode"];
	$IsFlagged=$response["IsFlagged"];
	$TransactionID=$response["TransactionID"];
	$mcode=explode("TRI", $MerchantRefNo);
	$online_show_dog_id=$mcode[1];
	
	$resultinsert = query_execute("UPDATE online_show_dog SET order_transaction_id = '$TransactionID', paid = '$ResponseCode', paymentid = '$PaymentID', payment_type = '$Mode' WHERE online_show_dog_id = '$online_show_dog_id'");
}
// Update in DB----------------------------------------------------------------------------------

$selDog = mysql_query("SELECT * FROM online_show_dog WHERE online_show_dog_id = '$mcode[1]'");
 if (!$selDog){  die(mysql_error()); 	}
  $rowDog = mysql_fetch_array($selDog);
    $dog_id = $rowDog["dog_id"];
	$kci_num = $rowDog["kci_num"];
	$dog_breed = $rowDog["dog_breed"];
	$dogowner_id = $rowDog["dogowner_id"];
	$breed_name = $rowDog["dog_breed_name"];
	$microchip = $rowDog['microchip'];
	$address = stripslashes($rowDog["address"]);
	
	$city = $rowDog["city"];
	$phone = $rowDog["phone"];
	$name_title = $rowDog["name_title"];
	$dog_name = stripslashes($rowDog["dog_name"]);
	
	$dog_sex = $rowDog["dog_sex"];
	$dog_color = stripslashes($rowDog["dog_color"]);
	
	$sire = $rowDog["sire"];
	$dam = $rowDog["dam"];
	$breeder = stripslashes($rowDog["breeder"]);
	
	$dob = $rowDog["dob"];
	$dognationality = $rowDog["dognationality"];
	$dog_class = $rowDog["dog_class"];
	$owner_name = $rowDog["owner_name"];
	$owner_address = $rowDog["owner_address"];
	$owner_city = $rowDog["owner_city"];
	$owner_pin = $rowDog["owner_pin"];
	$owner_mobile = $rowDog["owner_mobile"];
	$owner_email = $rowDog["owner_email"];
	
	list($year, $month, $day) = split("-", $dob);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payment Details | DogSpot</title>
</head>

<body>
<div id="pagebody">
<? if($response["ResponseCode"]==0){?>
<div style="padding:10px; background-color:#d4f797;"><strong>Your Payment has beed successfully Authorized.</strong> Thank you for your order from DogSpot.in</div>
<? }else{?>
<div style="padding:10px; background-color:#fa7c79;"><strong>Your Payment has beed unsuccessful.</strong> </div>
<? }?>
</div>
<? ob_start();?>
<p>Order No: <b><?=$response["MerchantRefNo"]?></b></p>
<p>Payment status: <span <? if($response["ResponseCode"]==0){?>style="color:#0C0"<? }else{?>style="color:red"<? }?>><?=$response["ResponseMessage"]?></span></p>
<p>Transaction ID: <?=$TransactionID?></p>
<p>Payment ID: <?=$PaymentID?></p>
<p>Amount: <?=$Amount?></p>
<p>Details:</p>
<table width="70%" border="1" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="200"><strong> Registered Name of Exhibit </strong></td>
    <td><?=$dog_name?></td>
  </tr>
  <tr>
    <td><strong> KCI Reg No. </strong></td>
    <td><?=$kci_num?></td>
  </tr>
  <tr>
    <td><strong> Microchip No. </strong></td>
    <td><?=$microchip?></td>
  </tr>
  <tr>
    <td><strong> Breed </strong></td>
    <td><?=$breed_name;?></td>
  </tr>
  <tr>
    <td><strong> Color &amp; Marking </strong></td>
    <td><?=$dog_color?></td>
  </tr>
  <tr>
    <td><strong> Sire </strong></td>
    <td><?=$sire?></td>
  </tr>
  <tr>
    <td><strong> Dam </strong></td>
    <td><?=$dam?></td>
  </tr>
  <tr>
    <td><strong> Bred in India? </strong></td>
    <td><?=$dognationality?></td>
  </tr>
  <tr>
    <td><strong> Date of Birth Of the Dog </strong></td>
    <td><? print(showdate($dob, "d M Y")); ?></td>
  </tr>
  <tr>
    <td><strong> Entered in Class </strong></td>
    <td><?=$dog_class?></td>
  </tr>
  <tr>
    <td><strong>Breeder </strong></td>
    <td><?=$breeder?></td>
  </tr>
  <tr>
    <td><strong>Owners Name </strong></td>
    <td><?=$owner_name?></td>
  </tr>
  <tr>
    <td><strong>Owners Address </strong></td>
    <td><?=$owner_address?></td>
  </tr>
  <tr>
    <td><strong>Owner City</strong></td>
    <td><?=$owner_city?></td>
  </tr>
  <tr>
    <td><strong>Owner Pin Code</strong></td>
    <td><?=$owner_pin?></td>
  </tr>
  <tr>
    <td><strong>Mobile Number </strong></td>
    <td><?=$owner_mobile?></td>
  </tr>
  <tr>
    <td><strong>Email </strong></td>
    <td><?=$owner_email?></td>
  </tr>
  
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<?
		$mailBody= ob_get_contents();
		ob_end_flush();

		$mailSubject = "Entry for FCI Thrissur ID: $online_show_dog_id";
		$toEmail = '"'.$owner_name.'" <'.$owner_email.'>';
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: DogSpot.in <contact@dogspot.in>' . "\r\n";
		$headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";
		$headers .= 'Reply-To: DogSpot.in <contact@dogspot.in>'. "\r\n";
		$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
		$headers .= "Organization: DogSpot\r\n";
	
		if(mail($toEmail, $mailSubject, $mailBody, $headers,"-f contact@dogspot.in -r mybounceemail@dogspot.in")){
		}
?>
</body>
</html>