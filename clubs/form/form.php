<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Form</title>
<script src="/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
	  $("#clubform").validate({
	});
});
</script>
<style type="text/css">
.error {
	font-size: 10px;
	color: #F00;
}
</style>
</head>
<body>
<form action="form-post.php" method="post" name="clubform" target="clubformtarg" id="clubform">
<table width="" border="0" cellspacing="2" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
  <tr>
    <td width="50" valign="top">Name:</td>
    <td valign="top"><input name="name" type="text" id="name" style="width:220px;" class="required"></td>
  </tr>
  <tr>
    <td valign="top">Mobile:</td>
    <td valign="top"><input name="mobile" type="text" id="mobile"style="width:220px;" class="required"></td>
  </tr>
  <tr>
    <td valign="top">Address:</td>
    <td valign="top"><textarea name="address" id="address" cols="20" rows="5" style="width:220px;" class="required"></textarea></td>
  </tr>
  <tr>
    <td valign="top">Quantity:</td>
    <td valign="top"><input name="qty" type="text" id="qty" style="width:220px;" class="required"></td>
  </tr>
  <tr>
    <td valign="top">Email:</td>
    <td valign="top"><input name="email" type="text" id="email" style="width:220px;" class="required email"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="post_club_form" id="post_club_form" value="Submit"></td>
  </tr>
</table>
</form>
<!-- Google Code for WMB Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1059390776;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "nP6pCMSzmgIQuIqU-QM";
var google_conversion_value = 0;
if (500) {
  google_conversion_value = 500;
}
/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1059390776/?value=500&amp;label=nP6pCMSzmgIQuIqU-QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>