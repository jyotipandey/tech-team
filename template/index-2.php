<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Dog lovers Community</title>

<link href="/css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div id="main">
<div id="head">
<div id="logo"><img src="/images/logo-new.jpg" alt="DogSpot.in" width="253" height="83" /></div>
<div id="userstat">Welcome! <span class="redbtext">Rana</span><br />
        <img src="/images/trans.gif" alt="trans" width="2" height="4" /><br />
      New User? <span class="redbtext">Register here</span><br />
    </div>
<div id="tsearch">
      <input name="textfield" type="text" class="tfield" id="textfield" value="Search" />
    <img src="/images/go.jpg" alt="Search" width="18" hspace="6" /></div> 


 </div>
<div  id="topnavi"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center"><a href="#">About DogSpot</a></td>
        <td width="1" align="center">|</td>
        <td align="center"><a href="#">Who We Are</a></td>
        <td width="1" align="center">|</td>
        <td align="center"><a href="#">Why DogSpot</a></td>
      </tr>
    </table>
  </div>
<div id="pagebody">
      <h1>About DogSpot</h1>
      <p>I have a dog. And I sometimes think of all those wonderful people who own a dog. I wish I could meet them at one place -- something like 101 Dalmatians -- a home full of dogs &amp; dog lovers, sharing experiences, tips to dog grooming, dog training, dog food, places to buy dog merchandise, anything at all under the sun. I wish I could share my experiences, the kind of mate I am looking for my dog, coz I best know his likes and dislikes. I wish I knew of the nearest vet to take my dog for his shots, and oh! His food is finishing too… sometimes, I really wish, I had DogSpot -- a Dog Lover’s community </p>
      <h3>Coming Soon… </h3>
    </div>
</div>
<div id="footer">Want to be a part of DogSpot? Email us at contact@dogspot.in</div>
</body>
</html>
