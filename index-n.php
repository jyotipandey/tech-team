<?php 
ob_start();
session_start();
$userid = $_SESSION['sessionuserid'];

if ($logoff == "logoff"){
session_destroy();
// refresh the page
header ("Location: index-t.php"); 
ob_end_flush();
exit(); 
}
$sitesection = "HOME";
$ajaxbody = "HOME";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Dog lovers Community Health Information</title>
<meta name="keywords" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<script src="/js/ajax.js" type="text/javascript"></script>
<script language="javascript">
<!--
function LoadHAjax(){
OpenAjaxPostCmd('hr-articles.php','hrarticles','','Loading....','hrarticles','2','2');
OpenAjaxPostCmd('hf-articles.php','hfarticles','','Loading....','hrarticles','2','2');
OpenAjaxPostCmd('adog.php','adog','','Loading....','adog','2','2');
OpenAjaxPostCmd('apuppy.php','apuppy','','Loading....','apuppy','2','2');
}
//-->
</script>

<?php require_once('common/top.php'); ?>
<div id="pagebody">
      <div id="OutFrame">
        <div id="InFrame">
          <div id="HWelL">
            <h4>Welcome to DogSpot.in</h4>
          If you have a pet, probably you would know. Right from vaccination, to  what food to give, you've had your doubts. Your best advisor in most  cases is your vet. But then, what if you wanted to get a second opinion?        
          
          </div>
          <div id="HWelR">
            <h4>DogSpot.in</h4>
            <ul class="Hbul">
              <li><a href="http://www.dogspot.in/#1">What is DogSpot?</a></li>
              <li><a href="http://www.dogspot.in/#1">Who We Are?</a></li>
              <li><a href="http://www.dogspot.in/#1">Why DogSpot?</a> </li>
            </ul>
          </div>
          <div id="clearall"></div>
        </div>
    </div>
<div id="HflotL">
<div id="OutFrame">
        <div id="InFrame">
          <div id="hfarticles">
          
          </div>
        </div>
    </div>
<div id="OutFrame">
        <div id="InFrame">
          <div id="adog">
          
          </div>
        </div>
    </div>
    <div id="OutFrame">
        <div id="InFrame">
          <h2>Sponsored Links</h2>
          <ul class="Hbul">
            <li><a href="#">Article 1</a></li>
            <li><a href="#">Article 2</a></li>
            <li><a href="#">Article 3</a></li>
            <li><a href="#">Article 4</a></li>
            <li><a href="#">Article 5</a></li>
          </ul>
        </div>
    </div>
     
</div>
<div id="HflotR">
<div id="OutFrame">
        <div id="InFrame">
        <div id="hrarticles">
          
          </div> 
        </div>
    </div>
    <div id="OutFrame">
        <div id="InFrame">
          <h2>Sponsored Links</h2>
          <ul class="Hbul">
            <li><a href="#">Article 1</a></li>
            <li><a href="#">Article 2</a></li>
            <li><a href="#">Article 3</a></li>
            <li><a href="#">Article 4</a></li>
            <li><a href="#">Article 5</a></li>
          </ul>
          
        </div>
    </div>
    <div id="OutFrame">
        <div id="InFrame">
          <div id="apuppy">
          
          </div>
          
        </div>
    </div>
    
</div>
<div id="clearall"></div>          
</div>
<?php require_once('common/bottom.php'); ?>
