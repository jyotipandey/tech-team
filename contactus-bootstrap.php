<?php
require_once('constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();

$title="Contact DogSpot - Online Pet Supply Store";
$keyword="Contact Dogspot, Contact Pet supplies store, get in touch with Pet Shop, Dog store.";
$desc="DogSpot.in offers pet products including dogs, cats and small pets online.";
 
    $alternate="https://www.dogspot.in/contactus.php";
	$canonical="https://www.dogspot.in/contactus.php";
	$og_url=$canonical;
	$imgURLAbs="";
	
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<script type="text/javascript">
 jQuery(document).ready(function() {
	  jQuery("#formC").validate({
		  
	});
});
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a700fcd4b401e45400c81e6/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<style>
.contact-us-section {
	margin: 25px 0px 30px;
}
.contact-us-section h1 {
	margin: 0px;
	padding: 0px;
	font-size: 26px;
	margin-bottom: 20px;
}
iframe {
	min-height: 480px !important
}
</style>
<div class="breadcrumbs">
  <div class="container">
    <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
      <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a href="/" itemprop="item"><span itemprop="name">Home</span></a>
        <meta itemprop="position" content="1" />
        </span> <span> / </span> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <span itemprop="item"> <span itemprop="name" class="active-bread">Contact Us</span></span>
        <meta itemprop="position" content="2" />
        </span> </div>
    </div>
  </div>
</div>
<section class="contact-us-section">
  <div class="container">
    <h1>Send us an Email</h1>
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6"> 
        <script type="text/javascript" src="https://assets.freshdesk.com/widget/freshwidget.js">
 </script>
        <link rel="stylesheet" type="text/css" href="https://assets.freshdesk.com/widget/freshwidget.css">
        <?php /*?><style type="text/css" media="screen, projection">
	@import url(http://assets.freshdesk.com/widget/freshwidget.css); 
</style> <?php */?>
        <iframe class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://dogspotsupport.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&screenshot=no" scrolling="yes" height="420px" width="100%" frameborder="0" > </iframe>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6">
        <?php /*?> <h3>Call Us:</h3> 
  <p>Customer Care +91- 9212196633<br /></p>
  <p> Timing: 9 AM to 6 PM - Mon to Sat <br />
  <span>(Standard Calling Charges Apply)</span>
  </p> 
   <br /><?php */?>
         <h3>Marketing Alliances:</h3> <p> marketing@dogspot.in </p>
        <br />
        <h3>Mail Us:</h3>
        <table>
          <tr>
            <td> F.F., C/o BMP eGroup Solutions Private Limited, <br />
Plot No: 37/19/22, Behind IGL CNG Pump, Kapashera,<br /> New Delhi, 110037 (India)
             </td>
          </tr>
        </table>
        <br />
        <h3>Please Note: </h3>
        <p>DogSpot.in is an open platform for information sharing and is not involved in
          the sale or purchase of puppies and dogs. The users can interact with each other
          directly for adoption, sale or purchase of Puppies and Dogs. DogSpot.in does not
          undertake any responsibility for Transactions made based on information posted
          on the website.</p>
      </div>
    </div>
  </div>
</section>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
