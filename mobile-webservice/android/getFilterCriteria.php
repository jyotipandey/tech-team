<?php
	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	//require_once($baseURL.'/session.php');

	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "getFilterCriteria.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);

  if($cat_id){
	$salinstock="item_category_id:\"$cat_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND stock_status:instock&version=2.2&rows=500&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
	$url = "http://101.53.137.39/solr/dogspotshopsolr/select/?q=$salinstock";
  }else
  {
	  $salinstock="item_brand:\"$brand_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND stock_status:instock&version=2.2&rows=500&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
	$url = "http://101.53.137.39/solr/dogspotshopsolr/select/?q=$salinstock";
  }
	
	$url = str_replace(" ","%20",$url);
	$resultsolr = get_solr_result($url);
	$totrecord = $resultsolr['TOTALHITS'];

	foreach($resultsolr['HITS'] as $rowItemall){
		$Aitem_Itemid[]=$rowItemall["item_id"];
		$Aitem_brand[]=$rowItemall["item_brand"];
		$Aitem_price[]=$rowItemall["price"];
		$Aitem_attribute[]=$rowItemall["item_attribute"];
		$Aitem_life_stage[]=$rowItemall["item_life_stage"];
		$Aitem_breed_id[]=$rowItemall["item_breed_id"];
		$Aitem_breed_type[]=$rowItemall["item_breed_type"];
		$Aitem_category[]=$rowItemall["item_cat_id"];//add code
		$Aitem_weight[]=$rowItemall["weight"];
		$A1weight_price[]=round($rowItemall["price"]/$rowItemall["weight"],3);
	}
	
	
	if($totrecord>0){
		$Aitem_brand=array_unique($Aitem_brand);
		$Aitem_price=array_unique($Aitem_price);
		$Aitem_attribute=array_unique($Aitem_attribute);
		$Aitem_life_stage=array_unique($Aitem_life_stage);
		$Aitem_breed_id=array_unique($Aitem_breed_id);
		$Aitem_breed_type=array_unique($Aitem_breed_type);
		$Aitem_category=array_unique($Aitem_category); //add code from
		//print_r($Aitem_category);//Array ( [0] => 62|10 [46] => 62|10|87|149 )
		$Aitem_weight=array_unique($Aitem_weight);
		$A1weight_price=array_unique($A1weight_price);
		//create brand array
		foreach($Aitem_brand as $abrand23){
			$qbrandname=query_execute_row("SELECT brand_name FROM shop_brand where brand_id=$abrand23");
			$Abrandname[]=strtoupper($qbrandname['brand_name'])."@@".$abrand23;
		}
		asort($Abrandname);
		foreach($Abrandname as $abrandA){ 
			$cabrandA=explode("@@",$abrandA);//$ArrayShopBrands[$abrand]		
			if($cabrandA[1]!='0' && $cabrandA[1]!='35' ){	
				$data_brand=array('brand_id' =>$cabrandA[1],'brand_name' =>$cabrandA[0]);
				$databrand1[]=$data_brand;
			}
		} 
		$databrand = array("type"=>"optional", "title"=>"BRANDS","data"=>$databrand1); 
		//close brand array
		//create price range
		foreach($Aitem_price as $ap){
			$ANitem_price[]=$ap;
		}
		asort($ANitem_price);

		$endnum = min($ANitem_price);
		$startnum = max($ANitem_price);
		$data_price=array('min' =>$endnum,'max' =>$startnum);
		$dataprice1[]=$data_price;
		$dataprice = array("type"=>"range", "title"=>"PRICE RANGE","data"=>$dataprice1); 
		
		//create attribute
		foreach($Aitem_attribute as $Aitem_attribute1){ 
			$getdata=explode('|',$Aitem_attribute1);
			foreach($getdata as $getd){
				$get=explode('A',$getd);
				$key=$get['0'];
				$keyvalue=$get['1'];
				$Asd[]=$key.'@'.$keyvalue;
				$keypoint[]=$get['0'];
			}
		}
		
		
		$Akeypoint=array_unique($keypoint);
		asort($Akeypoint);
		$Asd=array_unique($Asd);
		asort($Asd);
		
		foreach($Akeypoint as $keyname){
			if($keyname){
				if(($cat_id=='40' && $keyname=='35') || $keyname=='37' || $cat_id=='15'){}else{
					$qAttname=query_execute_row("SELECT attribute_set_name FROM shop_attribute_set where attribute_set_id='$keyname' ");
					$attr_set_name = str_replace(" ","_",$qAttname['attribute_set_name']);
					$attri='';
					foreach($Asd as $Asd1){
						$Asd2=explode('@',$Asd1);
						if($keyname==$Asd2[0]){
							$qAttvalue=query_execute_row("SELECT attribute_value_name FROM shop_attribute_value where attribute_value_id='$Asd2[1]' ");	
							$attri[]=array('value'=>$Asd2[0].'-'.$Asd2[1],'name'=>str_replace("-"," ",$qAttvalue['attribute_value_name']));	
							//$attri[]=array('value'=>'','name'=>str_replace("-"," ",$qAttvalue['attribute_value_name']));	
						}
						
					}//foreach close
					$attr = array('type'=>"optional", 'title'=>$qAttname['attribute_set_name'],'data'=>$attri);
					$mainn[$attr_set_name]=$attr;			
				}
			}
		}
		if($cat_id=="15"){
			$breed_type1 = array(
				/*array("value"=>"2","name"=>"Small Breed"),
				array("value"=>"3","name"=>"Medium Breed"),
				array("value"=>"5","name"=>"Large Breed"),
				array("value"=>"6","name"=>"Giant Breed"),
				array("value"=>"7","name"=>"All Breed")*/
				
				array("value"=>"upto 10kg","name"=>"Small Breed"),
				array("value"=>"10-25kg","name"=>"Medium Breed"),
				array("value"=>"25-45kg","name"=>"Large Breed"),
				array("value"=>"above 45kg","name"=>"Giant Breed"),
				array("value"=>"For all type","name"=>"All Breed")
				
			);
			$attr_set_breed_type = "Breed_type";  
			$breed1 = array(
				array("value"=>"11","name"=>"Beagle","image"=>"mobile-webservice/android/images/beagle.png"),
				array("value"=>"23","name"=>"Boxer","image"=>"mobile-webservice/android/images/boxer.png"),
				array("value"=>"37","name"=>"Cocker Spaniel","image"=>"mobile-webservice/android/images/cocker-spaniel.png"),
				array("value"=>"60","name"=>"German Shepherd","image"=>"mobile-webservice/android/images/german-shepherd.png"),
				array("value"=>"65","name"=>"Golden Retriever","image"=>"mobile-webservice/android/images/golden-retriever.png"),
				array("value"=>"67","name"=>"Great Dan","image"=>"mobile-webservice/android/images/great-dane.png"),
				array("value"=>"86","name"=>"Labrador Retriever","image"=>"mobile-webservice/android/images/labrador-retriever.png"),
				array("value"=>"108","name"=>"Pug","image"=>"mobile-webservice/android/images/pug-dog.png"),
				array("value"=>"112","name"=>"Rottweiler","image"=>"/mobile-webservice/android/images/rottweiler-dog.png")
				
			);
			
			$life_stages1 = array(
				array("value"=>"upto 2 Months","name"=>"Starter"),
				array("value"=>"2-24 Months","name"=>"Puppy & Junior"),
				array("value"=>"2-7 Years","name"=>"Adult"),
				array("value"=>"More than 7 Years","name"=>"Senior")
				/*array("value"=>"1","name"=>"Starter"),
				array("value"=>"2","name"=>"Puppy & Junior"),
				array("value"=>"5","name"=>"Adult"),
				array("value"=>"6","name"=>"Senior")*/
			);
			$attr_set_life_stage = "Life_Stage";
			
			$breed_type = array('type'=>"optional", 'title'=>"Breed Type", 'data'=>$breed_type1);
			$mainn[$attr_set_breed_type]=$breed_type;
			
			$breed = array('type'=>"slider", 'title'=>"Breed", 'data'=>$breed1);
			$mainn[Breed]=$breed;
			
			$life_stages = array('type'=>"optional", 'title'=>"Life Stages", 'data'=>$life_stages1);
			$mainn[$attr_set_life_stage]=$life_stages;
		}
		
		//close attribute
		//final array
		//print_r($mainn);
		//die;
		
		
		$datat1= array('price_data'=>$dataprice,'brand_data'=>$databrand);
		//$datat1=array_merge('cat_id'=>$cat_id,$datat);
		if(count($mainn)>0){
			echo json_encode(array_merge($datat1,$mainn));
			$return = appLogPhoneGap($api_url, "Response", $log_id);
		}else{
			echo json_encode($datat1);
			$return = appLogPhoneGap($api_url, "Response", $log_id);
		}
		//echo json_encode($datat1);
	}else{
		$datat1 = array(
			'status'=>"No Filter Criteria Found",
			'cat_id'=>$cat_id
		);
		echo json_encode($datat1);
		$return = appLogPhoneGap($api_url, "Response", $log_id);
	}
?>