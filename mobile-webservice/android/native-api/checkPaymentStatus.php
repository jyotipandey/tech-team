<?php

$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
require_once($baseURL.'/shop/functions.php');
require_once($baseURL.'/session.php');
require_once($baseURL.'/shop/arrays/arrays.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');
require_once('constants.php');

$api_url = "checkPaymentStatus.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);

$access_token = $_POST['access_token'];
$order_id = $_POST['order_id'];
$getMode = $_POST['mode'];
//$mode = base64_decode($getMode);

if($getMode==$app_security_mode){
	if($access_token){
		$getUserId=base64_decode($access_token);
		$pos = strpos($getUserId, '##');
		if($pos != false){	
			$vardata = explode('##',$getUserId);
			$user_id = $vardata [0];
			$device_id = $vardata [1];
			//get token and get userid from this token using algo...

			$check_userid = query_execute_row("SELECT count(*) as cc FROM users_mobile_token WHERE userid='$user_id'");
			if($check_userid[cc]>0){
				//echo "SELECT * FROM shop_order WHERE userid='$user_id' AND order_id='$order_id'";
				$check_order = query_execute_row("SELECT * FROM shop_order WHERE userid='$user_id' AND order_id='$order_id'");
				if($check_order['order_id']!=""){
					$order_method = $check_order['order_method'];
					$order_id = $check_order['order_id'];
					$data=array();
					//echo $check_order['order_status'];
					if($check_order['order_status']=="inc" || $check_order['order_status']==""){
						$close_status = "0";
						$status = "inc";
					}
					
					if(($order_method=="DC" || $order_method=="CC"||$order_method=="dc"||$order_method=="cc"||$order_method=="ccnb"||$order_method=="nb"||$order_method=="CCNB"||$order_method=="NB") && $check_order['order_transaction_amount']!="0.0000" && $check_order['order_transaction_id']!=""){
						if($check_order['order_status']=="0"){
							$close_status = "1";
							$status = "success";
						}elseif($check_order['order_status']=="1"){
							$close_status = "1";
							$status = "fail";
						}
					}elseif($order_method==""){
						$close_status = "1";
						$status = "fail";
					}
					$data=array(                     
						'status'=>$status,
						'order_id' => $order_id,
						'order_date' => $check_order['order_c_date'],
						'order_amount' => number_format($check_order['order_amount'],2),
						'payment_method'=>$AOrderMethod[$order_method],
						'close_status'=>$close_status,
						'msg' =>"Order Placed"
					);
				}	
			}
			else{
				$data=array(
					'status' =>"fail",
					'msg' =>"Not a valid User"
				);	
			}
		}
		else{
			$data=array(
				'status' =>"fail",
				'msg' =>"Access Token Not Valid!"
			);
		}
	}
	else{
		$data=array(
			'status' =>"fail",
			'msg' => "Not a valid User Authentication"
		);
	}
}else{
	$data=array(
		'status' =>"fail",
		'msg' => "Unauthorized Access"
	);
}	
$das=json_encode($data);
echo $das;
$return = app_logs($api_url, "Response", $log_id);
?>