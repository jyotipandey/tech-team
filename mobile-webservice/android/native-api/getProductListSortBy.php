<?php
//$baseURL='E:/xampp/htdocs/dogspot_new';
$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
require_once('constants.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');

// App log
$api_url = "getProductListSortBy.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);

//Request Params
$getMode = $_POST['mode'];
//$mode = base64_decode($getMode);
$mode = $getMode;
$page = $_POST['page'];
$sort = $_POST['sort'];
$cat_id = $_POST['cat_id'];
if($mode==$app_security_mode){
	//paging 
	$num_rec_per_page=10;
	if (isset($page)) { 
		$page  = $page; 
	}else { 
		$page=1; 
	} 
	$start_from = ($page-1) * $num_rec_per_page;

	if($sort=='1'){
		$st='&sort=price asc';	
	}elseif($sort=='2'){
		$st='&sort=price desc';			
	}else{
		$st='&sort=item_display_order ASC';
	}
	$datat1 = array();
	if($cat_id){
		$salinstock="item_category_id:\"$cat_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND stock_status:instock$st&version=2.2&start=$start_from&rows=$num_rec_per_page&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
		$url = "http://localhost/solr/dogspotshopsolr/select/?q=$salinstock";
	}
	if($brand_id){
		$salinstock="item_brand:\"$brand_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND stock_status:instock$st&version=2.2&start=$start_from&rows=$num_rec_per_page&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
	$url = "http://localhost/solr/dogspotshopsolr/select/?q=$salinstock";
	}
	if($url){
		
		$url = str_replace(" ","%20",$url);
		$resultsolr = get_solr_result($url);
		$totrecord = $resultsolr['TOTALHITS'];
		
		$Aitem_Itemid = array();
		$Aitem_brand = array();
		$Aitem_price = array();
		$Aitem_attribute = array();
		$Aitem_life_stage = array();
		$Aitem_breed_id = array();
		$Aitem_breed_type = array();
		$Aitem_category = array();
		$Aitem_weight = array();
		$A1weight_price = array();
		foreach($resultsolr['HITS'] as $rowItemall){
			$Aitem_Itemid[]=$rowItemall["item_id"];
			$Aitem_brand[]=$rowItemall["item_brand"];
			$Aitem_price[]=$rowItemall["price"];
			$Aitem_attribute[]=$rowItemall["item_attribute"];
			$Aitem_life_stage[]=$rowItemall["item_life_stage"];
			$Aitem_breed_id[]=$rowItemall["item_breed_id"];
			$Aitem_breed_type[]=$rowItemall["item_breed_type"];
			$Aitem_category[]=$rowItemall["item_category_id"];//add code
			$Aitem_weight[]=$rowItemall["weight"];
			$A1weight_price[]=round($rowItemall["price"]/$rowItemall["weight"],3);
			}
		
		if($totrecord>0){
		$Acount=array_unique($Aitem_Itemid);
		}
		$Aitem_ItemidP3 = array();
		foreach($resultsolr['HITS'] as $rowItemP11){
			//while($rowItemP11 = mysql_fetch_array($qItem)){
			//echo $rowItemP11['item_id'].";";
			$Aitem_ItemidP3[]=$rowItemP11['item_id'];
		}
			$unique = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
			//$totrecord=$resultsolr['TOTALHITS'];
			//echo "dfdsgsdgsd".$totrecord22=count($unique);
			$totrecord_display = count($unique);
		foreach($unique as $rowItemitem ){
			$rowItem=query_execute_row("SELECT tag,item_id,name,nice_name,description ,selling_price,item_parent_id,price,type_id,weight_price,item_brand,stock_status,created_at,weight,item_display_order from shop_items WHERE item_id='$rowItemitem'");
			$item_id=$rowItem["item_id"];
			
			$tag=$rowItem["tag"];
			$name=stripslashes($rowItem["name"]);
			$name=stripslashes($name);
			$nice_name=$rowItem["nice_name"];
			$type_id=$rowItem["type_id"];
			$price=number_format($rowItem["price"]);
			$selling_price=number_format($rowItem["selling_price"]);
			$item_parent_id=$rowItem["item_parent_id"];
			$stock_status=$rowItem["stock_status"];
			$brandd=$rowItem["item_brand"];
			$description=$rowItem["description"];
			if($item_parent_id == '0'){
			$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
			}else{
			$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
			}
			$rowdatM = mysql_fetch_array($qdataM);
			
			if($rowdatM["media_file"]){
				$src = $baseURL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
				$imageURL='/imgthumb/150x160-'.$rowdatM["media_file"];
				$dest = $baseURL.$thumb_img;
				$src = $baseURL.$img;
				createImgThumbIfnot($src,$dest,'150','160','ratiowh');
			}else{
				$src = $baseURL.'/shop/image/no-photo-t.jpg';
				$imageURL='/shop/image/no-photo-t.jpg';
			}
			
			//get today's savings
			if($rowItem["selling_price"] > $rowItem["price"]){
				$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
				$item_discount_per=number_format($item_discount_per1,0);	
			}else{
				$item_discount_per="0";
			}
			//end
			
			$data=array(
				'product_id' =>$item_id,
				'product_name' =>$name,
				'product_image' =>$site_url.$imageURL,
				'retail_price' =>$price,
				'currency' =>'INR',
				'sale_price' =>$selling_price,
				'description' =>$description,
				'saving_percentage'=>$item_discount_per
			);
			$datat1[]=$data;
		}
		$maxpages = ceil($totrecord/$num_rec_per_page);
		if($page < $maxpages){
			$next1 = $page+1;
			$next = "$next1";
		}else{
			$next = "0";	
		}
		$datat['status']="success";
		$datat['product_data']=$datat1;
		$datat['total_items']=$totrecord;
		$datat['msg']="Record Found";
		$datat['next']="$next";
	}else{
		$datat['status']="fail";
		$datat['product_data']=$datat1;
		$datat['total_items']=0;
		$datat['msg']="Record not Found";
		$datat['next']="0";
	}	
}else{
	$datat['status']="fail";
	$datat['product_data']=$datat1;
	$datat['total_items']=$totrecord;
	$datat['msg']="Unauthorized Access";
	$datat['next']="0";
}

echo json_encode($datat);
$return = app_logs($api_url, "Response", $log_id);
?>
