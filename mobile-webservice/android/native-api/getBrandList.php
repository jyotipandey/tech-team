<?php
	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once('constants.php');

	$api_url = "getBrandList.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$page = $_POST['page'];
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	
	if($mode==$app_security_mode){
		$num_rec_per_page=10;
		if (isset($page)) { 
			$page  = $page; 
		}else { 
			$page=1; 
		} 
		$start_from = ($page-1) * $num_rec_per_page;
		$SQL = "SELECT * FROM shop_brand WHERE brand_status='1' AND domain_id!='2' ORDER BY FIND_IN_SET(display_order,'1,2,3,4,5,6,7,8,9,10,0') ASC LIMIT $start_from, $num_rec_per_page";

		// This for mobile site. Because mobile site is working without paging  
		if($mobile_site){
			$SQL = "SELECT * FROM shop_brand WHERE brand_status='1' AND domain_id!='2' ORDER BY FIND_IN_SET(display_order,'1,2,3,4,5,6,7,8,9,10,0') ASC";
		}
		// End Query of Mobile site for all Brand List
		
		$qdata=query_execute($SQL);
		
		//Getting Total Record 
		$totrecord = mysql_num_rows(mysql_query("SELECT * FROM shop_brand WHERE brand_status='1' AND domain_id!='2' ORDER BY FIND_IN_SET(display_order,'1,2,3,4,5,6,7,8,9,10,0') ASC"));
		$maxpages = ceil($totrecord/$num_rec_per_page);
		if($page < $maxpages){
			$next1 = $page+1;
			$next = "$next1";
		}else{
			$next = "0";	
		}
		//End
		
		if(mysql_num_rows($qdata) > 0){
			while($tt=mysql_fetch_array($qdata)){
				if($tt['brand_logo']!=''){
						$src1 = "/imgthumb/130x54-".$tt['brand_logo'];
						$src = $baseURL."/imgthumb/130x54-".$tt['brand_logo'];
						$imageURL = "/imgthumb/30x40-".$tt['brand_logo'];
						$dest = $baseURL.$imageURL;
						createImgThumbIfnot($src,$dest,'30','40','ratiowh');
					}else{
						$src1 = "/shop/image/no_image_new.png";
						$imageURL = "/shop/image/no_image_new.png";
					}

				$data[]=array(
					'brand_id' =>$tt['brand_id'],
					'brand_name' =>$tt['brand_name'],
					'brand_logo' => $site_url.$imageURL,
					'brand_nicename' =>$tt['brand_nice_name']
					
				);
			}
			$datat = array(
				'status' => "success",
				'msg' => "Record Found",
				'data' => $data,
				'total_items'=>"$totrecord",
				'next' =>$next
			);
		}else{
			$datat = array(
				'status' => "fail",
				'msg' => "Record not Found",
				'data' => "",
				'total_items'=>"$totrecord",
				'next' =>$next
			);
		}	
	}else{
		$datat = array(
			'status' => "fail",
			'msg' => "Unauthorized Access",
			'data' => "",
			'total_items'=>"$totrecord",
			'next' =>$next
		);
	}
	echo json_encode($datat);
	$return = app_logs($api_url, "Response", $log_id);
?>