<?php
/**
* 
* @var $mode1 is a fixed parameter that is passed from two screen 
* 1- if mode=="addItemDirectlyFromList" then item is being added in wishlist from product listting screen 
* 2- if mode=="" then item is being passed from cart screen
* 
*/

$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');
//require_once($baseURL.'/session.php');

require_once('constants.php');

$api_url = "addWishlist.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);

$access_token = $_POST['access_token'];
$session_id = $_POST['session_id'];
$item_id = $_POST['item_id'];
$getMode = $_POST['mode'];
//$mode = base64_decode($getMode);
$mode = $getMode;

if($mode==$app_security_mode){
	if($access_token){
		$dataUser=base64_decode($access_token);	
		$vardata = explode('##',$dataUser);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		//get token and get userid from this token using algo...
		if($mode1!="addItemDirectlyFromList"){
			$mode1="addItemFromCart";
		}
		$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
		if($check_userid[cc]>0){
			if($item_id && $mode1=="addItemFromCart"){
				$check_item = mysql_query("SELECT wishlist_id FROM shop_wishlist WHERE item_id = '$item_id' AND userid = '$user_id'");
				$count_rec = mysql_num_rows($check_item);
				$getItemList = mysql_fetch_array($check_item);
				if($count_rec > 0){
					$qRemoveItemCart = mysql_query("DELETE FROM shop_cart WHERE userid='$user_id' AND item_id = '$item_id' AND cart_order_status = 'new'");
					$data=array(
						'status' =>"fail",
						'msg' => 'Item has been already added in wishlist',
						'wishlist_id'=>$getItemList['wishlist_id']					
					);
				}
				else{				
					$wishlist = mysql_query("INSERT INTO shop_wishlist(userid, item_id, date) VALUES ('$user_id','$item_id',NULL)");						
					$wishlist_id = mysql_insert_id();				
					if($wishlist_id){
						$qRemoveItemCart = mysql_query("DELETE FROM shop_cart WHERE userid='$user_id' AND item_id = '$item_id' AND cart_order_status = 'new'");
						$data=array(
							'status' =>"success",
							'msg' =>"Item has been added to wishlist successfully",
							'wishlist_id' =>$wishlist_id
						);					
					}
				}
			}	
			elseif($item_id && $mode1=="addItemDirectlyFromList"){
				$SQ_wishlist = mysql_query("SELECT wishlist_id FROM shop_wishlist WHERE item_id = '$item_id' AND userid = '$user_id'");
				$count = mysql_num_rows($SQ_wishlist);
				$getWishlistItem = mysql_fetch_array($SQ_wishlist);
				if($count > 0){
					$qRemoveItemCart = query_execute("DELETE FROM shop_wishlist WHERE wishlist_id='".$getWishlistItem['wishlist_id']."' AND item_id = '$item_id' AND userid='$user_id'");
					$data=array(
						'status'=>"status",
						'msg' =>"Item has been deleted from Wishlist successfully",
						'wishlist_id' =>$getWishlistItem['wishlist_id']
					);
				}
				else{				
					$wishlist = mysql_query("INSERT INTO shop_wishlist(userid,item_id,date) VALUES ('$user_id','$item_id',NULL)");						
					$wishlist_id = mysql_insert_id();				
					if($wishlist_id){
						$data=array(
							'status' =>"success",
							'msg' =>"Item has been added to Wishlist successfully",
							'wishlist_id' =>$wishlist_id
						);					
					}
				}
			}
			else{
				$data=array(
					'status' =>"fail",
					'msg' =>"Insufficient parameters",
					'wishlist_id' =>""
				);		
			}					
		}
		else{
			$data=array(
				'status' =>"fail",
				'msg' =>"Not a valid user",
				'wishlist_id' =>""
			);
		}
	}
	else{
		$data=array(
			'status' =>"fail",
			'msg' =>"Not a valid User Authentication",
			'wishlist_id' =>""
		);
	}	
}else{
	$data=array(
		'status' =>"fail",
		'msg' => "Unauthorized Access",
		'wishlist_id'=>""
	);
}
$das=json_encode($data);
echo $das;
$return = app_logs($api_url, "Response", $log_id);
?>