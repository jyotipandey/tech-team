<?php
	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once('constants.php');
	
	$api_url = "getAddToaddress.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	$accessToken = $_POST['access_token'];
	$address_id = $_POST['address_id'];
	$guest_email_id = $_POST['guest_email'];
	$address_name = $_POST['address_name'];
	$address_zip = $_POST['address_zip'];
	$mobile_number = $_POST['mobile_number'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$state = $_POST['address_id'];
	$session_id = $_POST['session_id'];
	$deviceId = $_POST['device_id'];
	
	$paymentOption = array();
	$donatebag = '';
	$saledisc = '';
	$total_order1="";
	if($mode==$app_security_mode){
		if($accessToken){
			$dataUserID=base64_decode($accessToken);	
			$vardata = explode('##',$dataUserID);
			$user_id = $vardata [0];		
		}
		if($guest_email==""){
			$guest_email = useremail($user_id);	
		}else{
			$guest_email = $guest_email_id;	
		}
		if($user_id != "Guest"){
			if($address_id){
				$more_address = query_execute_row("SELECT * FROM shop_order_more_address WHERE more_address_id = '$address_id'");
				$address_name = addslashes($more_address['address_name']);
				$mobile_number = $more_address['address_phone1'];
				$address = addslashes($more_address['address_address1']);
				$address_address2 = addslashes($more_address['address_address2']);
				$address_zip = $more_address['address_zip'];
				$city = addslashes($more_address['address_city']);
				$address_state_id = $more_address['address_state_id'];
				$state = $more_address['address_state'];
				$state1 = $address_state_id.';'.$address_state;		
			}
			// User as a Guest  and filling a new shipping addrress
			// request param is required like : name, pincode, mob, city, state, email, address, etc
			if($address_name && $address_zip && $mobile_number && $address && $city && $state && $guest_email && $session_id){
				$more_address_id = $address_id;
				// Getting City Details
				$SQ_city = query_execute_row("SELECT * FROM city WHERE city_name='$city'");
				$city_id = $SQ_city[city_id];
				$city_nicename = $SQ_city[city_nicename];
				
				// Getting State Details
				$SQ_state = query_execute_row("SELECT * FROM state WHERE state_name='$state'");
				$state_id = $SQ_state[state_id];
				$state_nicename = $SQ_state[state_nicename];
				
				$address_address = stripslashes($address);
				$getShopCart_orderID  = query_execute_row("SELECT * FROM shop_cart WHERE userid = '$user_id' AND cart_order_status='new' AND dog_show='' AND donate_bag='0' AND cart_order_id !='0' ORDER BY cart_id desc LIMIT 1");
				if($getShopCart_orderID['cart_order_id']!='0'){
					$arrayOrderData=array(
						'userid' => $user_id,
						'app_name' => "mobile-app",
						'session_id' => $session_id,
						'order_user_ip' => $deviceId,
						'u_email' => $guest_email,
						'donatebag' => $donatebag,
						'name' => addslashes($address_name)
					);	
				}else{
					$arrayOrderData=array(
						'userid' => $user_id,
						'session_id' => $session_id,
						'order_user_ip' => $deviceId,
						'u_email' => $guest_email,
						'donatebag' => $donatebag,
						'name' => addslashes($address_name)
					);
				}
				$order_id = createOrder($arrayOrderData);
				$secCart = query_execute("SELECT item_id, cart_id, item_discount FROM shop_cart WHERE cart_order_id = '".$order_id['order_id']."'");
				while($rowCartid = mysql_fetch_array($secCart)){
					$cart_id = $rowCartid['cart_id'];	
					$item_id = $rowCartid['item_id'];
					$saledisc = $rowCartid['item_discount']+$saledisc;
					$qItem = query_execute("SELECT item_id, price, selling_price, discount FROM shop_items WHERE item_id = '$item_id'");
					$rowItem = mysql_fetch_array($qItem);
					$selling_price = $rowItem['selling_price'];
					$price = $rowItem['price'];
					
					if($selling_price <= $price  ) {	
						$cartupdate = query_execute("UPDATE shop_cart SET item_discount = '0' WHERE cart_id = '$cart_id'");	
					}												
					else{							
						$cartupdate = query_execute("UPDATE shop_cart SET item_discount = $selling_price*item_qty - $price*item_qty WHERE cart_id = '$cart_id'");
					}							
				}
				$checkamount = query_execute("DELETE FROM shop_order_coupon WHERE order_id='".$order_id['order_id']."'");

				// checked for picode code available for COD
				$getcod = query_execute("SELECT courier_pincode FROM shop_couriorpin_availablity WHERE courier_pincode = '$address_zip' AND courier_method = 'cod'");
				$totcod = mysql_num_rows($getcod);
				$codno='0';
				
				$item_cod34 = query_execute_row("SELECT count(*) as codno1 FROM shop_cart as fr, shop_items as pm WHERE fr.item_id = pm.item_id AND fr.cart_order_id = '".$order_id['order_id']."' AND fr.cart_order_status = 'new' AND pm.payment_mode != 'cod'");		
				if($item_cod34['codno1'] > 0){
					$codno='1';
				}
				$donation = query_execute_row("SELECT count(*) as codno1 FROM shop_cart as fr, shop_items as pm WHERE fr.item_id = pm.item_id AND fr.cart_order_id='".$order_id['order_id']."' AND fr.cart_order_status = 'new' AND fr.donate_bag ='1'");
				//$num=mysql_num_rows($donation);
				if($donation['codno1'] > 0){
					$codno='1';
				}
				
				$donapay = query_execute_row("SELECT sum(item_totalprice) as amount FROM shop_cart WHERE cart_order_id='".$order_id['order_id']."'");
				
				$o_amout = $donapay['amount']+$shipping_charge;
				$queryAmount = query_execute("UPDATE shop_order SET order_items_amount='".$donapay['amount']."' WHERE order_id='".$order_id['order_id']."'");
				 $queryAmount2 = query_execute("UPDATE shop_order SET order_amount=order_items_amount-order_discount_amount+item_shipping_amount+order_shipment_amount WHERE order_id='".$order_id['order_id']."'");
				if($o_amout > 8000){
					$codno='1';
				}
				$check_order1 = query_execute_row("SELECT * FROM shop_order WHERE order_id='".$order_id['order_id']."'");
				
				if($check_order1['order_discount_id']!=""){
					updatediscount($order_id['order_id'], $user_id, $session_id, $check_order1['order_amount']); 
				}
				
				$total_order1 = $check_order1['order_items_amount'] + $check_order1['order_shipment_amount'] + $check_order1['item_shipping_amount']-$check_order1['order_discount_amount'];
				$getusercount = query_execute_row("SELECT user_cod_status FROM users WHERE (u_email = '$guest_email' OR userid = '$user_id')");
				if($getusercount['user_cod_status']=='no') {
					$usercod='no'; 
				}  
				else {  
					$usercod='yes';
				}

				if($usercod=='yes' && $codno!='1' ){ // user can avail COD Facilities
					if($totcod=='0'){ // pincode not valid for COD
						$COD_availability = "No";
						$paymentOption = array(
							'payment_mode1'=>"Net Banking",
							'payment_mode2'=>"",
							'payment_mode3'=>"Credit Card",
							'payment_mode4'=>"Debit Card",
							'payment_mode5'=>"Demand Draft/Cheque"
						);
						
					}
					else{
						$COD_availability = "Yes";
						$paymentOption = array(
							'payment_mode1'=>"Net Banking",
							'payment_mode2'=>"Cash On Delivery",
							'payment_mode3'=>"Credit Card",
							'payment_mode4'=>"Debit Card",
							'payment_mode5'=>"Demand Draft/Cheque"
						);
					}	
				}else{
					$COD_availability = "No";
					$paymentOption = array(
						'payment_mode1'=>"Net Banking",
						'payment_mode2'=>"",
						'payment_mode3'=>"Credit Card",
						'payment_mode4'=>"Debit Card",
						'payment_mode5'=>"Demand Draft/Cheque"
					);
				}
				$datat = array(
					'address_id'=>$more_address_id,
					'order_id'=>$order_id['order_id'],
					'cod_check' =>$COD_availability,
					'order_total'=>$total_order1
				);
				$datat1 = array(
					'status' => "success",
					'msg' => "Order Created",
					'data' => $datat,
					'payment_option'=>$paymentOption 
				);	
			}
			else{
				$datat1 = array(
					'status' => "fail",
					'msg' => "Parameter are missing",
					'data' => "",
					'payment_option'=>""
				);
			}
		}else{
			$datat1 = array(
				'status' => "fail",
				'msg' => "Unauthorized Access",
				'data' => "",
				'payment_option'=>"" 
			);
		}
		echo json_encode($datat1);
		$return = app_logs($api_url, "Response", $log_id);
?>