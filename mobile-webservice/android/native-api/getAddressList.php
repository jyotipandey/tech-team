<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	//require_once($baseURL.'/session.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once('constants.php');
	
	$api_url = "getAddressList.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	
	$access_token = $_POST['access_token'];
	$address_id = $_POST['address_id'];
	$address_name = $_POST['address_name'];
	$address_zip = $_POST['address_zip'];
	$mobile_number = $_POST['mobile_number'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$guest_email = $_POST['guest_email'];
	$showmode = $_POST['showmode'];
	//$data = array();
	if($mode==$app_security_mode){
		if($access_token){
			$getUserid=base64_decode($access_token);	
			$vardata = explode('##',$getUserid);
			$user_id = $vardata [0];
			$device_id = $vardata [1];
			$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
			if($check_userid[cc] > 0){
				// Add New Address
				if($showmode=="addAddress"){
					if($user_id != 'Guest'){
						$guest_email = useremail($user_id);
					}
					if($address_name && $address_zip && $mobile_number && $address && $city && $state && $guest_email){
						$SQ_check_pin = mysql_query("SELECT courier_pincode FROM shop_couriorpin_availablity WHERE courier_pincode = '$address_zip'");
						if(mysql_num_rows($SQ_check_pin) > 0){
							$SQ_city = query_execute_row("SELECT * FROM city WHERE city_name='$city'");
							$city_id = $SQ_city[city_id];
							$city_nicename = $SQ_city[city_nicename];
							$SQ_state = query_execute_row("SELECT * FROM state WHERE state_name='$state'");
							$state_id = $SQ_state[state_id];
							$state_nicename = $SQ_state[state_nicename];
							if($address_zip){
								$address_address = stripslashes($address);
								if($address_address){
									$resultaddress = query_execute_row("SELECT * FROM shop_order_more_address WHERE userid='$user_id' AND address_zip = '$address_zip' AND address_address1 = '".addslashes($address_address)."'");
									if($resultaddress['more_address_id']==''){
										$address_name = addslashes($address_name);
										$address_city = addslashes($city);				
										$resultinsert34 = query_execute("INSERT INTO shop_order_more_address (userid, address_name, address_email, address_address1, address_address2, address_city, address_city_id, address_city_nicename, address_state, address_state_id, address_state_nicename, address_zip, address_phone1) VALUES('$user_id','".mysql_real_escape_string($address_name)."', '$guest_email', '".mysql_real_escape_string($address)."', '', '$city', '$city_id','$city_nicename', '$state', '$state_id', '$state_nicename', '$address_zip', '$mobile_number')");
										$more_address_id = mysql_insert_id();
										$data = array(
											'status' => "success",
											'msg'=>"Your address has been added successfully",
											'address_id'=>$more_address_id
										);
									}else{
										$data = array(
											'status' => "fail",
											'msg'=>"Address Already Exists",
											'address_id'=>$resultaddress['more_address_id']
										);
									}
								}
							}
						}else{
							$data = array(
								'status' => "fail",
								'msg'=>"Sorry, We do not offer shipping facility on this pincode",
								'address_id'=>$resultaddress['more_address_id']
							);
						}
					}
				}
				
				// show address list 
				if($showmode=="showList"){
					$getaddress = mysql_query("SELECT * FROM shop_order_more_address WHERE userid='$user_id' AND address_name!='' AND address_address1 != ''  AND address_city != '' AND address_state != 'Select State' AND address_state != '' AND address_phone1 != '' ORDER BY more_address_id");

					$count = mysql_num_rows($getaddress);
					if($count>0){  
						
						while($ship_add = mysql_fetch_array($getaddress)){ 
							$address[] = array(
								'address_id' => $ship_add['more_address_id'],
								'name' => $ship_add['address_name'],
								'address_name1' => $ship_add['address_address1'],
								'address_name2' => $ship_add['address_address2'],
								'address_city' => $ship_add['address_city'],
								'address_state' => $ship_add['address_city'],
								'address_state' => $ship_add['address_state'],
								'address_zip' => $ship_add['address_zip'],
								'address_phone1' => $ship_add['address_phone1']
							);
						}
						$data = array(
							'status' => "success",
							'msg' => "Record Found",
							'address' => $address,
							'total_address' =>$count
						);
					}
					else{
						$address[] = array(
							'address_id' =>"", 
							'name' => "",
							'address_name1' => "",
							'address_name2' => "",
							'address_city' => "",
							'address_state' => "",
							'address_state' => "",
							'address_zip' => "",
							'address_phone1' => ""
						);
						$data=array(
							'status' => "fail",
							'msg' => "Record not Found",
							'address' => $address,
							'total_address' =>"0"
						);
					}  
				} 
				// Edit address and save 
				if($showmode == "updateAddress" && $address_id){
					//echo $address_id;
					if($address_name && $address_address1 && $address_city && $address_state && $address_zip && $address_phone1){
						$SQ_check_pin = mysql_query("SELECT courier_pincode FROM shop_couriorpin_availablity WHERE courier_pincode = '$address_zip'");
						if(mysql_num_rows($SQ_check_pin) > 0){
							$SQ_city = query_execute_row("SELECT * FROM city WHERE city_name='$address_city'");
							$address_city_id = $SQ_city['city_id'];
							$address_city_nicename = $SQ_city['city_nicename'];
							
							$SQ_state = query_execute_row("SELECT * FROM state WHERE state_name='$address_state'");
							$address_state_id = $SQ_state['state_id'];
							$address_state_nicename = $SQ_state['state_nicename'];	
							
							$query = "UPDATE shop_order_more_address SET address_name='$address_name', address_address1='$address_address1', address_city='$address_city', address_city_id='$address_city_id', address_city_nicename='$address_city_nicename', address_state='$address_state', address_state_id='$address_state_id', address_state_nicename='$address_state_nicename', address_zip='$address_zip', address_phone1='$address_phone1'";
							
							if($address_address2){
								$query .= ", address_address2='$address_address2'";
							}
							$query .=" WHERE more_address_id='$address_id' AND userid='$user_id'";
							//echo $query;
							
							$update_address = mysql_query($query);
							//$result = mysql_affected_rows();
							//if($result){
								$data=array(
									'status' => "success",
									'msg' =>"Address has been updated successfully",
									'address_id' =>$address_id,
									'total_address' =>""
								);
							//}else{
								
							//}
						}else{
							$data = array(
								'status' => "fail",
								'msg'=>"Sorry, We do not offer shipping facility on this pincode",
								'address_id'=>$resultaddress['more_address_id']
							);
						}
					}else{
						$data=array(
							'status' => "fail",
							'msg' =>"other parameters should not be blank",
							'address_id' =>$address_id,
							'total_address' =>""
						);
					}
				}
				
				// Delete Address
				if($showmode == "deleteAddress" && $address_id){
					$query = "DELETE FROM shop_order_more_address WHERE more_address_id='$address_id' AND userid='$user_id' LIMIT 1";
					
					$delete = mysql_query($query);
					$result = mysql_affected_rows();
					if($result){
						$data=array(
							'status' =>"success",
							'msg' =>"Deleted successfully",
							'address_id' =>$address_id,
							'total_address' =>""
						);
					}else{
						$data=array(
							'status' =>"fail",
							'msg' =>"Not Deleted",
							'address_id' =>$address_id,
							'total_address' =>""
						);
					}
				} 
				if($showmode==""){
					$data=array(
						'status' =>"fail",
						'msg' =>"Mode parameter should not be blank",
						'address_id' =>"",
						'total_address' =>""
					);
				}
			}else{
				$data=array(
					'status' =>"fail",
					'msg' =>"Not a valid User",
					'address_id' =>"",
					'total_address' =>""
				);	
			}
		}else{
			$data=array(
				'status' =>"fail",
				'msg' =>"Not a valid User Authentication",
				'address_id' =>"",
				'total_address' =>""
			);
		}
	}else{
		$data=array(
			'status' =>"fail",
			'msg' =>"Unauthorized Access",
			'address_id' =>"",
			'total_address' =>""
		);
	}
	$das=json_encode($data);
	echo $das;
	$return = app_logs($api_url, "Response", $log_id);
?>