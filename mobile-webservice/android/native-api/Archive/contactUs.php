<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Contact DogSpot - Online Pet Supply Store</title>
		<meta name="keywords" content="Contact Dogspot, Contact Pet supplies store, get in touch with Pet Shop, Dog store." />
		<meta name="description" content="Contact DogSpot for any questions regarding pets supplies, accessories & online products purchase. You can get in touch via phone calls or by filling inquiry form." />
</head>
<body>
<div class="contactDetails_wrapper">
    <h3>Call Us:</h3> 
    <p>Customer Care - +91-9212196633(10 AM to 6 PM)</p> 
    <br>
    <p> Marketing Alliances - <a href="mailto:marketing@dogspot.in"> marketing@dogspot.in </a></p>
    
    <span>(Standard Calling Charges Apply)<br>
    </span>
    <br>
    <p></p>
    <h3>Mail Us:</h3>
    <table>
        <tbody>
        <tr>
            <td> 
                Plot no - 545, S.Lal Tower<br>
                Sector - 20, Dundahera<br>
                Gurgaon 122016<br>
                Haryana, India <br>
            </td>
            <td style="padding-left:90px;">
                586, Ground Floor <br> Sector - 20, Dundahera<br> Gurgaon-122016<br>
                Haryana, India <br>
            </td>
        </tr>
        </tbody>
    </table>
    <br><h3>Please Note:  </h3>
    <p>DogSpot.in is an open platform for information sharing and is not involved in
    the sale or purchase of puppies and dogs. The users can interact with each other
    directly for adoption, sale or purchase of Puppies and Dogs. DogSpot.in does not
    undertake any responsibility for Transactions made based on information posted
    on the website.</p>
</div>
</body>
</html>