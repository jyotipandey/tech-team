<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once('constants.php');
	
	$api_url = "customer_care.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	
	if($mode==$app_security_mode){
		$care = array(
			'contact_no' => "+91-9599090487",
			'time' => "9 AM to 6 PM"
		);
		
		$marketing_alliance = "marketing@dogspot.in";
		
		$mail_us = array(
			'address1' => "Plot no - 545, S.Lal Tower",
		    'address2' => "Sector - 20, Dundahera",
		    'city' => "Gurgaon",
		    'zip' => "122016",
		    'state' => "Haryana",
		    'country' => "INDIA"
		);
		$data1 = array(
			'cus_care' =>$care,
			'mail_us' =>$mail_us,
			'marketing_email' =>$marketing_alliance
		);
		
		$data= array(
			'status' =>"success",
			'msg' =>"Record Found",
			'data' =>$data1	
		);	
	}else{
		$data= array(
			'status' =>"fail",
			'msg' =>"Unauthorized Access",
			'data' =>$data1	
		);
	}
	$das=json_encode($data);
	echo $das;
	$return = app_logs($api_url, "Response", $log_id);
?>