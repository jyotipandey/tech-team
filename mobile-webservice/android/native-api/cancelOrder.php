<?php
	
	// Created By Umesh on dated: 26-02-2015
	// access token required for cancel order

	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once('constants.php');
	
	$api_url = "cancelOrder.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$access_token = $_POST['access_token'];
	$order_id = $_POST['order_id'];
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	if($mode==$app_security_mode){
		if($access_token){
			$data=base64_decode($access_token);	
			$vardata = explode('##',$data);
			$user_id = $vardata [0];
			$device_id = $vardata [1];
			if($user_id!='Guest' && $order_id!="0"){
				$SQ_shop_order = query_execute_row("SELECT order_id FROM shop_order WHERE order_type='complete' AND (delevery_status='new' OR delevery_status='pending-dispatch') AND (order_status='0' OR order_status='1') AND order_id='$order_id' AND userid='$user_id'");
				if($SQ_shop_order['order_id']!=''){
					$cancel_reason = "";
					UpdateHardItemStatus($order_id);
					$shoporder=query_execute("SELECT delevery_status FROM shop_order WHERE order_id = '".$order_id."' ");
					$rowShopOrd = mysql_fetch_array($shoporder);
					$ordercomnt= 'Changed Delivery Status: from '.$rowShopOrd["delevery_status"]." to cancelled" ;
					
					$SQ_shop_order1 = query_execute("UPDATE shop_order SET delevery_status='canceled' WHERE order_id='$order_id' AND userid='$user_id'");
					/* For Order Cancel Mail to care@dogspot.in */
					$usermail = mysql_query("SELECT address_name, address_email from shop_order_address where order_id = '$order_id' AND address_type_id='1'");
					$result_mail = mysql_fetch_array($usermail);
					$user_mail = $result_mail['address_email'];
					$user_name1 = $result_mail['address_name'];
					$user_name2 = explode(" ",$user_name1);
					$user_name = $user_name2[0];
					
					$rec_check = mysql_query("SELECT * from shop_order where order_id='$order_id' AND order_status='0' AND order_method NOT IN('cod','dd')");
					$check_result = mysql_num_rows($rec_check);
					if($check_result > 0){
						$e="care@dogspot.in";
						$mailSubject = "$order_id Cancel Please check and refund";
						$mailbody = "$order_id Cancel Please check and refund";
						$headers  = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						$headers .= 'From: DogSpot.in <care@dogspot.in>' . "\r\n";
						$headers .= 'Bcc: DogSpot.in <contact@dogspot.in>' . "\r\n";
						$headers .= 'Cc: jyoti@dogspot.in' . "\r\n";
						$headers .= 'Cc: DogSpot <finance@dogspot.in>' . "\r\n";
						$headers .= 'Cc: DogSpot <kshitij@dogspot.in>' . "\r\n";
						$headers .= 'Cc: DogSpot <sharmila@dogspot.in>' . "\r\n";
						$headers .= 'Reply-To: DogSpot.in <care@dogspot.in>'. "\r\n";
						$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
						$headers .= "Organization: DogSpot\r\n";	
			
						//echo $e.$mailSubject.$mailbody.$headers;
						if(mail($e,$mailSubject, $mailbody, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")){
							//echo $emailMsg='Email send successfully to '.$e;
						}
						else{
							//echo $emailMsg='Error in sending email to '.$email;
						}
					}
					/* For Order Cancel Mail to care@dogspot.in ENDS */
					
					// Email to customer 
					if($user_mail){
						$SQ_shop = 	query_execute_row("SELECT mode,order_status FROM shop_order WHERE order_id='$order_id'");
						$order_mode = $SQ_shop['mode'];
						$status = $SQ_shop['status'];
						$SQ_section_review_id = query_execute_row("SELECT section_review_id FROM  section_reviews WHERE review_section_id='$order_id' AND (review_name='cancelled' OR review_name='canceled') ORDER by section_review_id DESC LIMIT 1 ");
						if($SQ_section_review_id['section_review_id']==''){
							$mailSubject_customer = "Cancellation of DogSpot Order No# $order_id.";
							$mailbody_customer = "<p>Dear $user_name,</p>
							<p>Greetings from DogSpot!</p>
							<p>We regret to inform you that your order Number: $order_id placed with us has been cancelled by us.</p><p><b>Reason For cancellation:</b></p><p><i>As per your request</i></p>
							<p>We value you as a loyal DogSpot customer and hope you understand that this was a situation that could not have been avoided in spite of our best efforts.</p>
							<p>If applicable, we will process the refund and the same will be reflecting in your account within 7-10 working days.</p>
							<p>You could write to us on <a href='mailto:care@dogspot.in'>care@dogspot.in</a> or call our Customer Support Team at +91-9599090487 (Mon to Sat from 10:00 am to 6:00 pm). </p>
							<p>Once again we apologize for the inconvenience and hope to see you again on <a href='https://www.dogspot.in'>DogSpot.in</a></p>
							<br/>
							<p>Regards,</p>
							<p>Team DogSpot</p>";
							
							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
							$headers .= 'From: DogSpot.in <care@dogspot.in>' . "\r\n";
							$headers .= 'Bcc: DogSpot.in <contact@dogspot.in>' . "\r\n";
							$headers .= 'Reply-To: DogSpot.in <care@dogspot.in>'. "\r\n";
							$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
							$headers .= "Organization: DogSpot\r\n";	
							
							if(mail($user_mail,$mailSubject_customer, $mailbody_customer, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")){
								$respond_order_cancel = send_sms_to_customer($order_id, "OrderCanxByCustomer", "", "", "", ""); 	
								//// Send auto Sms customer cancelled order.
								$resultinsert = query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body,review_name, c_date, user_ip) VALUES ('".$user_id."', 'shop-order', '".$order_id."', '".$cancel_reason.$ordercomnt."','cancelled', null, '')");
							}
						}
					}
					$data = array(
						'status' => "success",
						'msg' =>"Your order cancellation request has been processed"
					);
				}else{
					$data = array(
						'status' => "fail",
						'msg' =>"No Record Found"
					);	
				}	
			}
			else{
				$data = array(
					'status' => "fail",
					'msg' =>"Not Valid User"
				);
			}
		}else{
			$data = array(
				'status' => "fail",
				'msg' =>"Not Authonticated User"
			);
		}	
	}else{
		$data = array(
			'status' => "fail",
			'msg' =>"Unauthonticated Access"
		);
	}
	echo json_encode($data);
	$return = app_logs($api_url, "Response", $log_id);
?>