<?php

$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
require_once('constants.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');
$api_url = "myWishlist.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);

$getMode = $_POST['mode'];
//$mode = base64_decode($getMode);
$mode = $getMode;
$access_token = $_POST['access_token'];	
$session_id = $_POST['session_id'];

if($mode==$app_security_mode){
	if($access_token){
		$data=base64_decode($access_token);	
		$vardata = explode('##',$data);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		//get token and get userid from this token using algo...

		$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
		
		if($check_userid[cc]>0){
			//echo "SELECT * FROM shop_wishlist WHERE userid='$user_id'";
	    	$wishlist = mysql_query("SELECT * FROM shop_wishlist WHERE userid='$user_id'");
		    $count=mysql_num_rows($wishlist);
			$data= array();
		    if($count > 0){
	    		while($wish_data = mysql_fetch_array($wishlist)){
	                $item_id = $wish_data['item_id'];                
	                $item_table = query_execute_row("SELECT name, qty, stock_status, selling_price, price, item_parent_id FROM shop_items WHERE item_id = '$item_id'");
	                $stock_status = $item_table['stock_status'];
					
					if($item_table["selling_price"] > $item_table["price"]){
		        	    $item_discount_per1 = (($item_table["selling_price"]-$item_table["price"])*100)/$item_table["selling_price"];
		            	$item_discount_per = number_format($item_discount_per1,0);
	                }
	                else{
			            $item_discount_per="0";
	                }
	                if($item_table['item_parent_id']==0){
						$item_image = query_execute_row("SELECT media_file from shop_item_media WHERE item_id = '$item_id'"); 
					}else{
						$item_image = query_execute_row("SELECT media_file from shop_item_media WHERE item_id = '".$item_table['item_parent_id']."'"); 
					}	
					if($item_image["media_file"]){
						$img = '/shop/item-images/orignal/'.$item_image["media_file"];
						$thumb_img = '/imgthumb/150x160-'.$item_image["media_file"];
						$dest = $baseURL.$thumb_img;
						$src = $baseURL.$img;
						createImgThumbIfnot($src,$dest,'150','160','ratiowh');
				
					}else{
						$img = '/shop/image/no-photo-t.jpg';
						$thumb_img = '/shop/image/no-photo-t.jpg';
					}					
	               	$whishlist_data = array(
	                	'wishlist_id' => $wish_data['wishlist_id'],
	                	'product_id' => $item_id,
	                    'product_image' => $site_url.$thumb_img,
	                    'product_name' => $item_table['name'],
	                    'quantity' => $item_table['qty'],
	                    'item_id' => $item_id,
	                    'price' => number_format($item_table['price'],0),
						'selling_price'=>number_format($item_table['selling_price'],0),
						'saving_percentage'=>$item_discount_per,
	                    'in_stock' => $stock_status,
	                    'delivery_message' => 'Dispatch in 2-3 days'
	               );
	               $data[] = $whishlist_data;
	            } 
	            $data1['status'] = "success";
	            $data1['whishlist_data'] = $data;
	            $data1['total_record'] = $count;
	            $data1['msg'] = "Record Found";
	    	}else{
				$whishlist_data = array(
	                	'wishlist_id' => "",
	                	'product_id' => "",
	                    'product_image' => "",
	                    'product_name' => "",
	                    'quantity' => "",
	                    'item_id' => "",
	                    'price' => "",
						'selling_price'=>"",
						'saving_percentage'=>"",
	                    'in_stock' =>"",
	                    'delivery_message' =>""
	               );
  	               $data[] = $whishlist_data;
	               $data1['status'] = "fail";
	               $data1['whishlist_data'] = $data;
	 	           $data1['total_record'] = "";
	 	           $data1['msg'] = "No Record Found";
	    	}    
		}else{
			$data1=array(
				'status' =>"fail",
				'msg' =>"Invalid User"
			);	
	    }
	}else{
	    $data1=array(
	    	'status' =>"fail",
         	'msg' =>"Not a valid User Authentication"
		);
	}
}else{
	$data1=array(
		'status' =>"fail",
     	'msg' =>"Unauthorized access"
	);
}
$das=json_encode($data1);
echo $das;
$return = app_logs($api_url, "Response", $log_id);
?>