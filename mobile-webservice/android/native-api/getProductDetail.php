<?php
//$baseURL='E:/xampp/htdocs/dogspot_new';
$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
require_once('constants.php');

require_once($baseURL.'/mobile-webservice/android/functions.php');
$api_url = "getProductDetail.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);

$getMode = $_POST['mode'];
//$mode = base64_decode($getMode);
$mode = $getMode;

$product_id = $_POST['product_id'];

$discount_code = $_POST['discount_code'];
$order_id = $_POST['order_id'];
$address_id = $_POST['address_id'];
$payment_type = $_POST['payment_type'];
$similar_data = array();
if($mode==$app_security_mode){

	$rowItem=query_execute_row("SELECT * FROM shop_items WHERE item_id='$product_id' AND item_display_status!='delete'");
	$item_id=$rowItem["item_id"];
		
	$tag=$rowItem["tag"];
	$name=stripslashes($rowItem["name"]);
	$name=stripslashes($name);
	$nice_name=$rowItem["nice_name"];
	$type_id=$rowItem["type_id"];
	$price=number_format($rowItem["price"],0);
	$selling_price=number_format($rowItem["selling_price"],0);
	$item_parent_id=$rowItem["item_parent_id"];
	$stock_status=$rowItem["stock_status"];
	$brandd=$rowItem["item_brand"];
	$description=$rowItem["description"];
	$cat_id=$rowItem["child_cat"];

	// calculate savings
	if($rowItem["selling_price"] > $rowItem["price"]){
		$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
		$item_discount_per = number_format($item_discount_per1,0);
	}
	else{
		$item_discount_per="0";
	}
	//End

	if($rowItem["payment_mode"]!='cod'){
		$pmode='Not Available';
	}else{
		$pmode='Available';
	}
		
	if($item_id){
		$qdataM=query_execute("SELECT media_id, media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	}
	if(mysql_num_rows($qdataM) < 1){
		$qdataM=query_execute("SELECT media_id, media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
	}
	while($rowdatM = mysql_fetch_array($qdataM)){
		if($rowdatM["media_file"]){
			$imglink='/imgthumb/200x160-'.$rowdatM["media_file"];
			$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		}else{
			$imglink='/imgthumb/200x160-no-photo.jpg';
			$src = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
		}
		$dest = $DOCUMENT_ROOT.$imglink;
		createImgThumbIfnot($src,$dest,'200','160','ratiowh');
		$data_image[]=array(
			'image_id' =>$rowdatM["media_id"],
			'image_name' =>$rowdatM["media_file"]
		);
	}
	//similar data
	$qdata12=query_execute("SELECT * FROM shop_items  WHERE  domain_id!='2' AND child_cat='$cat_id' AND item_id!='$product_id' AND type_id!='configurable' limit 10");

	while($tt12=mysql_fetch_array($qdata12)){
		$item_id1=$tt12["item_id"];
		$tag=$tt12["tag"];
		$name1=stripslashes($tt12["name"]);
		$name1=stripslashes($name1);
		$nice_name1="http://dogspot.in/".$tt12["nice_name"];
		$type_id1=$tt12["type_id"];
		$price1=number_format($tt12["price"],0);
		$selling_price1=number_format($tt12["selling_price"],0);
		$item_parent_id1=$tt12["item_parent_id"];
		$stock_status1=$tt12["stock_status"];
		$brandd1=$tt12["item_brand"];
		$description1=$tt12["description"];
		
		// calculate savings
		if($tt12["selling_price"] > $tt12["price"]){
			$item_discount_per11 = (($tt12["selling_price"]-$tt12["price"])*100)/$tt12["selling_price"];
			$item_discount_per1 = number_format($item_discount_per11,0);
		}
		else{
			$item_discount_per1="0";
		}
		//
		
		if($item_id){
			$qdataM=query_execute("SELECT media_id, media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
		}
		if(mysql_num_rows($qdataM) < 1){
			$qdataM=query_execute("SELECT media_id, media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
		}
		$rowdatM = mysql_fetch_array($qdataM);
		
		if($rowdatM["media_file"]){
			$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
			$imageURL='/imgthumb/150x160-'.$rowdatM["media_file"];

		}else{
			$src = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
			$imageURL='/shop/image/no-photo-t.jpg';
		}
		
		$dataR=array(
			"product_id"=>$item_id1,
			"product_name"=>$name1,
			"product_nicename"=>$nice_name1,
			"product_image"=> $rowdatM["media_file"],
			"retail_price"=>$price1,
			"currency"=>"INR",
			"sale_price"=>$selling_price1,
			"description"=>$description1,
			'stock_status' =>$stock_status1,
			'saving_percentage'=>$item_discount_per1
		);
		$similar_data[]=$dataR;
	}
	$array_data=array(
		'image_data'=>$data_image,
		'product_summary' =>'',
		'product_name' =>$name,
		'product_nicename' =>"http://dogspot.in/".$nice_name,
		'discount_price' =>$price,
		'sale_price' =>$selling_price,
		'product_description' =>$description,
		'stock_status' =>$stock_status,
		'saving_percentage'=>$item_discount_per,
		'feedback' =>'',
		'dispatch_info' =>'Dispatched in 2-3 business days',
		'cash_on_delivery' =>$pmode,
		'shipping_charges' =>$rowItem[item_shipping_amount],
		'available_quantity' =>$rowItem[qty],
		'similar_data'=>$similar_data
	);
}else{
	$array_data=array(
		'image_data'=>$data_image,
		'product_summary' =>'',
		'product_name' =>$name,
		'product_nicename' =>$nice_name,
		'discount_price' =>$price,
		'sale_price' =>$selling_price,
		'product_description' =>$description,
		'stock_status' =>$stock_status,
		'saving_percentage'=>$item_discount_per,
		'feedback' =>'',
		'dispatch_info' =>'Dispatched 2-3 business days',
		'cash_on_delivery' =>$pmode,
		'shipping_charges' =>$rowItem[item_shipping_amount],
		'available_quantity' =>$rowItem[qty],
		'similar_data'=>$similar_data
	);
}
echo json_encode($array_data);
$return = app_logs($api_url, "Response", $log_id);
?>