<?php
/*
*	Collect Data of shipping and Create Order but Order still not complete will go to next step for Payment details 
* 	Created By Umesh & Brajendra Sir, Dated: 16/01/2015
* 	Required Parameters
*	1- $access_token
*	2- $payment_type
*	3- $order_id
*	4- $cod_check
*	5- $discount_code1
*/

//$baseURL='E:/xampp/htdocs/dogspot_new';
$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/shop/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
require_once($baseURL.'/shop/arrays/arrays.php');
require_once('constants.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');

$api_url = "getPaymentPayU.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);

$getMode = $_POST['mode'];
//$mode = base64_decode($getMode);
$mode = $getMode;
$access_token = $_POST['access_token'];
$order_id = $_POST['order_id'];
$session_id = $_POST['session_id'];
$payment_type = $_POST['payment_type'];
$address_id = $_POST['address_id'];
	
if($mode==$app_security_mode){	
	if($access_token){
		$getUserId=base64_decode($access_token);	
		$vardata = explode('##',$getUserId);
		$user_id = $vardata [0];
		$device_id = $vardata [1];	
	}
	else{
		$user_id = "Guest";	
	}
	if($user_id!="Guest" && $payment_type && $order_id && $session_id && $address_id){
		
		$ordchq = '';
		$SQ_users = query_execute_row("SELECT * FROM users WHERE userid='$user_id'");
		$SQ_more_address = query_execute_row("SELECT * FROM shop_order_more_address WHERE userid='$user_id' AND more_address_id='$address_id'");
		
		$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id='".$returnArr['order_id']."'");
		
		if($SQ_more_address[address_address1]==""){
			$SQ_more_address[address_address1] = $SQ_more_address[address_address2];
		}
		$displaypin = "0"; 	
		$postArr = array(
			'app_name' => "mobile-app",
			'order_id' =>$order_id,
			'u_email' =>$SQ_users[u_email],
			'us_email' =>$SQ_users[u_email],
			'pin' =>$SQ_more_address[address_zip],
			'session_id' =>$session_id,
			'name' =>$SQ_more_address[address_name],
			'address1' =>$SQ_more_address[address_address1],
			'mobile1' =>$SQ_more_address[address_phone1],
			'address_email2' =>$SQ_more_address[address_email],
			'city' =>$SQ_more_address[address_city],
			'receiver_email' =>$SQ_more_address[u_email],
			'state' =>$SQ_more_address[address_id].";".$SQ_more_address[address_state].";".$SQ_more_address[address_state_nicename],
			'fishtype' =>"",			
			'country_name' =>";INDIA;india",
			'discount_code1' =>$discount_code1,
			'donatebag' =>"",
			'displaypin' =>$displaypin,
			'order_amount' =>$SQ_shop_order[order_amount],
			'payment_type'=>$payment_type,
			'payment'=>$payment_type,
			'order_shipment_amount' =>0,
			'order_items_amount' => $SQ_shop_order[order_items_amount],
			'order_discount_amount' => $SQ_shop_order[order_discount_amount],
		);
		
		if($ordchq==''){
			$arrayOrderData=array(
				'userid'=>$user_id,
				'session_id'=>$session_id,
				'order_user_ip'=>$device_id
			);

			$result = array_merge($postArr, $arrayOrderData);
		
			$returnArr = createOrder($result);

			$quseridCOD = query_execute_row("SELECT u_id FROM shop_order WHERE order_id='".$returnArr['order_id']."'");

			$getc = "SELECT count(*) as successrate FROM shop_order Where mode!='TEST' AND delevery_status='delivered' AND (userid = '".$quseridCOD['u_id']."' OR u_id = '".$quseridCOD['u_id']."')";

			$qselectCOD = query_execute_row($getc);

			$sel_cart_items_code = mysql_query("SELECT * FROM shop_cart WHERE cart_order_id='".$returnArr['order_id']."'");

			$sel_cart_items_code2 = query_execute_row("SELECT order_amount, order_discount_id FROM shop_order WHERE order_id='".$returnArr['order_id']."'");

			while($sel_cart_items_code1 = mysql_fetch_array($sel_cart_items_code)){

				$code_items = $sel_cart_items_code1['item_id'];

				$code_item_qty = $sel_cart_items_code1['item_qty'];

				$analyticsArray[$code_items] = $code_item_qty;
			}
			// update Discount Start
			$orderStat = query_execute("SELECT * FROM shop_order WHERE order_id = '".$returnArr['order_id']."'");
			$rowOrd = mysql_fetch_array($orderStat);
			$order_discount_id = $rowOrd["order_discount_id"];
			$order_status = $rowOrd["order_status"];
			$order_transaction_source = $rowOrd["order_transaction_source"];
			$order_method = $rowOrd["order_method"];
			$orderuserid = $rowOrd["userid"];
			$delevery_status = $rowOrd["delevery_status"];
			$order_transaction_id = $rowOrd["order_transaction_id"];
			
			//updateDiscountStatus($returnArr['order_id']);
			
			$order_items_amount = number_format($rowOrd["order_items_amount"]);
			$order_shipment_amount = number_format($rowOrd["order_shipment_amount"]);
			$item_shipping_amount = number_format($rowOrd["item_shipping_amount"]);
			$order_discount_amount = number_format($rowOrd["order_discount_amount"]);
			$totAmount = number_format($rowOrd["order_amount"]);
			$totAmount2 = $rowOrd["order_items_amount"] + $rowOrd["order_shipment_amount"] - $rowOrd["order_discount_amount"] + $item_shipping_amount;
			// update Discount
			
			$checkamount=query_execute_row("SELECT SUM(amount) as total FROM shop_order_coupon WHERE order_id='".$returnArr['order_id']."'");
			
			$totAmount1=$totAmount2-$checkamount['total'];
				
			//echo "UPDATE shop_cart SET userid='$user_id' WHERE cart_order_id = '".$returnArr['order_id']."'";
			
			$GuestCartUpdate = query_execute("UPDATE shop_cart SET userid='$user_id' WHERE cart_order_id = '".$returnArr['order_id']."'");
			$sql = "UPDATE shop_order SET order_method='$payment_type' WHERE order_id = '".$returnArr['order_id']."'";

			//$GuestCartUpdate = query_execute($sql);

			$data = array(
				'status' => "success",
				'msg' => 'Proceed to payment'
			);
		}else{
			$data = array(
				'status' => "fail",
				'msg' => 'Something is missing'
			);	
		}
	}else{
		$data = array(
			'status' => "fail",
			'msg' => 'some params are missing'
		);
	}
}else{
	$data = array(
		'status' => "fail",
		'msg' => 'Unauthorized Access'
	);
}
$das = json_encode($data);
print $das;
$return = app_logs($api_url, "Response", $log_id);
?>