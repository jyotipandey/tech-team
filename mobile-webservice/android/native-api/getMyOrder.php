<?php
$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
require_once($baseURL.'/session.php');
require_once('constants.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');


/*
cancel_status = 1		// cancel will be display
cancel_status = 0		// cancel will be hide
track_status = 1		// track will be display
track_status = 0		// track will be hide
*/

$api_url = "getMyOrder.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);

$getMode = $_POST['mode'];
//$mode = base64_decode($getMode);
$mode = $getMode;
$access_token = $_POST['access_token'];

$data=array();
$cancel_status = "1";
$track_status = "1";
if($mode==$app_security_mode){
	if($access_token){
		$dataUserId=base64_decode($access_token);
		$vardata = explode('##',$dataUserId);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		
		//get token and get userid from this token using algo...
		$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
		if($check_userid[cc]>0){
			$check_order = mysql_query("SELECT so.order_id FROM shop_order as so WHERE so.userid='$user_id' AND so.mode != 'TEST' AND so.order_status != 'inc' ORDER BY so.order_c_date DESC LIMIT 15");
			$count = mysql_num_rows($check_order);
			if($count > 0){
				while($order_rec = mysql_fetch_array($check_order)){			          	
					$o_id[] = $order_rec['order_id'];
				}
				foreach($o_id as $order_id){	
					$check_items = mysql_query("SELECT cart_id,item_id,item_qty,item_price FROM shop_cart WHERE cart_order_id = '$order_id'");
					$SQ_shop_order = query_execute_row("SELECT item_shipping_amount, order_c_date, order_amount, order_discount_amount, delevery_status, order_status, order_method FROM shop_order WHERE order_id = '$order_id'");
					
					$delivery_status = $SQ_shop_order['delevery_status'];
					$order_status= $SQ_shop_order['order_status'];
					$payment_mode1= $SQ_shop_order['order_method'];
					
					if($order_status==0){
						if($delivery_status=="new" || $delivery_status=="pending-dispatch"){
							$cancel_status = "1";
							$track_status = "1";
							$order_status_text = ""; 
						}elseif($delivery_status=="canceled" || $delivery_status =="cancelled"){
							$cancel_status = "0";
							$track_status = "0";
							$order_status_text = "Cancelled";
						}else{
							$cancel_status = "0";
							$track_status = "1";
							$order_status_text = "";
						}
					}elseif($order_status==1){
						if($payment_mode1=="dd" || $payment_mode1=="DD"){
							if($delivery_status=="canceled" || $delivery_status =="cancelled"){
								$cancel_status = "0";
								$track_status = "0";
								$order_status_text = "Cancelled";
							}else{
								$cancel_status = "1";
								$track_status = "1";
								$order_status_text = "";
							}
						}else{
							$cancel_status = "0";
							$track_status = "0";
							$order_status_text = "Failed";
						}
					}
					
					
//					if(($order_status['delevery_status']=="new" || $order_status['delevery_status'] =="pending-dispatch") && ($order_status['shop_courier_id'] =="NULL" || $order_status['shop_courier_id'] =="0") && $order_status1=='0'){
//						$delivery_status = "Cancelled";
//						$cancel_status = "1";
//						$track_status = "1";
//					}elseif($delivery_status=='canceled' || $delivery_status=='cancelled'){
//						$delivery_status = "Cancelled";
//						$cancel_status = "0";
//						$track_status = "0";
//					}else{
//						
//					}
					$item_data=array();
					while($getRow = mysql_fetch_array($check_items)){	
						$item_id = $getRow['item_id']; 	          	
						$item_desc = query_execute_row("SELECT name FROM shop_items WHERE item_id = '$item_id'");
						$item_price = $getRow['item_price']*$getRow['item_qty'];
						$item_data1 = array(
							'product_id' => $item_id,
							'item_id' => $item_id,
							'item_quantity' => $getRow['item_qty'],
							'item_description' => $item_desc['name'],											
							'item_price' => "$item_price"
						);
						$item_data[] = $item_data1;
					}	
					if($SQ_shop_order['item_shipping_amount'] >0){
						$shipping_price = number_format($SQ_shop_order['item_shipping_amount'],0);
					}else{
						$shipping_price = "0";			//$shipping_charge define in the constants.php file
					}	
					if($payment_mode1=="COD" || $payment_mode1=="cod" || $payment_mode1=="Cod"){
							 $qGetpincode = query_execute_row("SELECT pincode FROM shop_order_temp_phone WHERE order_id='$order_id'");
				 				 $qGetMyCart = query_execute("SELECT * FROM shop_cart WHERE cart_order_id='$order_id'");
				  				while ($rowTP = mysql_fetch_array($qGetMyCart)) {
            $getGurgaonxItem = query_execute_row("SELECT item_id FROM shop_item_warehouse WHERE item_id = '" . $rowTP['item_id'] . "' order by item_id DESC LIMIT 1");
            if ($getGurgaonxItem['item_id'] == '') {
                $itemShip = 1;
            }
				  }
							 	$shipCharges = 0;
			            		$getGurgaonx = query_execute_row("SELECT pincode,type FROM shop_pincode_codCheck WHERE pincode = '" . $qGetpincode['pincode'] . "' AND status='1'");
            					if ($getGurgaonx['type'] == 'item') {
                				if ($itemShip == 1) {
                			    $shipCharges = 1;
                				}
           						 } elseif ($getGurgaonx['pincode'] == '') {
                				$shipCharges = 1;
            					}
            
            					if ($shipCharges == '1') {
             				    $COD_charge = 99;
           					    } else {
               					$COD_charge = 0;
            					}
						$data_order[]=array(                     
							'order_id' => $order_id,
							'order_date' => $SQ_shop_order['order_c_date'],
							'shipping_charges' => $shipping_price,
							'discount_amount' => number_format($SQ_shop_order['order_discount_amount'],0),
							'order_amount' => number_format($SQ_shop_order['order_amount'],0),
							'item_data'=>$item_data,
							'order_status'=>$order_status_text,
							'delivery_status' =>$delivery_status,
							'cancel_flag'=>$cancel_status,
							'track_flag'=>$track_status,
							'COD'=>$COD_charge
						);
						
					}else{
						$data_order[]=array(                     
							'order_id' => $order_id,
							'order_date' => $SQ_shop_order['order_c_date'],
							'shipping_charges' => $shipping_price,
							'discount_amount' => number_format($SQ_shop_order['order_discount_amount'],0),
							'order_amount' => number_format($SQ_shop_order['order_amount'],0),
							'item_data'=>$item_data,
							'order_status'=>$order_status_text,
							'delivery_status' =>$delivery_status,
							'cancel_flag'=>$cancel_status,
							'track_flag'=>$track_status,
							'COD'=>""
						);
					}	
					$data = array(
						'status'=>"success",
						'order_data'=>$data_order,
						'total_record'=>$count,
						'msg'=>"Order Found"
					);
				}	
			}else{
				$item_data[] = array(
					'product_id' => "",
					'item_id' => "",
					'item_quantity' => "",
					'item_description' => "",
					'item_price' => ""
				);
				$data_order[]=array(                     
					'order_id' => "",
					'order_date' => "",
					'shipping_charges' => "",
					'discount_amount' => "",
					'order_amount' => "",
					'item_data'=>$item_data,
					'order_status'=>"",
					'delivery_status' =>"",
					'cancel_flag'=>"",
					'track_flag'=>"",
					'COD'=>""
				);
				$data=array(
					'status'=>"fail",
					'order_data'=>$data_order,
					'total_record'=>"0",
					'msg'=>"No Order Available"
				);
			}	
		}
		else{
			$item_data[] = array(
				'product_id' => "",
				'item_id' => "",
				'item_quantity' => "",
				'item_description' => "",
				'item_price' => ""
			);
			$data_order[]=array(                     
				'order_id' => "",
				'order_date' => "",
				'shipping_charges' => "",
				'discount_amount' => "",
				'order_amount' => "",
				'item_data'=>$item_data,
				'order_status'=>"",
				'delivery_status' =>"",
				'cancel_flag'=>"",
				'track_flag'=>"",
				'COD'=>""
			);
			$data=array(
				'status'=>"fail",
				'order_data'=>$data_order,
				'total_record'=>"0",
				'msg'=>"Not a valid User"
			);
		}
	}
	else{
		$item_data[] = array(
			'product_id' => "",
			'item_id' => "",
			'item_quantity' => "",
			'item_description' => "",
			'item_price' => ""
		);
		$data_order[]=array(                     
			'order_id' => "",
			'order_date' => "",
			'shipping_charges' => "",
			'discount_amount' => "",
			'order_amount' => "",
			'item_data'=>$item_data,
			'order_status'=>"",
			'delivery_status' =>"",
			'cancel_flag'=>"",
			'track_flag'=>"",
			'COD'=>""
		);
		$data=array(
			'status'=>"fail",
			'order_data'=>$data_order,
			'total_record'=>"0",
			'msg'=>"Not a valid User Authentication"
		);
	}
}else{
	$item_data[] = array(
		'product_id' => "",
		'item_id' => "",
		'item_quantity' => "",
		'item_description' => "",
		'item_price' => ""
	);
	$data_order[]=array(                     
		'order_id' => "",
		'order_date' => "",
		'shipping_charges' => "",
		'discount_amount' => "",
		'order_amount' => "",
		'item_data'=>$item_data,
		'order_status'=>"",
		'delivery_status' =>"",
		'cancel_flag'=>"",
		'track_flag'=>"",
		'COD'=>""
	);
	$data=array(
		'status'=>"fail",
		'order_data'=>$data_order,
		'total_record'=>"0",
		'msg'=>"Unathorized Access"
	);
}
$das=json_encode($data);
echo $das;
$return = app_logs($api_url, "Response", $log_id);
?>