<?php
	/*
	*	Update Items Qty in Cart 
	*	Created By Umesh On dated: 16/3/2015 
	*/
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/constants.php');
	require_once($baseURL.'/session.php');
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/shop/arrays/shop-categorys.php');
	require_once($baseURL.'/arrays.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once('constants.php');
	
	$api_url = "updateCartItemQty.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	$accessToken = $_POST['access_token'];
	$device_id = $_POST['device_id'];
	$session_id = $_POST['session_id'];
	$cartId = $_POST['cart_id'];
	$cart_item_id = $_POST['item_id'];
	$cart_item_qty = $_POST['item_qty'];

	if($mode = $app_security_mode){
		if($accessToken){
			$getUsereId=base64_decode($accessToken);	
			$vardata = explode('##',$getUsereId);
			$user_id = $vardata [0];
			$device_id = $vardata [1];
		}
		else{
			$user_id = "Guest";	
		}
		if($cartId && $cart_item_id && $cart_item_qty && $session_id && $user_id){
				
			$qBuySel=query_execute_row("SELECT name,price, selling_price,qty,virtual_qty,item_status,item_shipping_amount FROM shop_items WHERE item_id = '$cart_item_id'");
			$selling_price=$qBuySel['selling_price'];
			$price=$qBuySel['price'];
			$item_status1=$qBuySel['item_status'];
			if($selling_price>$price){
				$saledisc=(($selling_price*$cart_item_qty)-($price*$cart_item_qty));
			}
			else{
				$saledisc='0';
			}
			//$item_shipping_amount=$qBuySel['item_shipping_amount'];
			if($user_id == 'Guest'){
				//$qGetcart = query_execute("SELECT cart_id,item_qty FROM shop_cart WHERE session_id = '$session_id' AND item_id = '$itemReal' AND cart_order_status='new'");
				$qGetMyCart13 = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND sc.donate_bag=0 AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc.item_id='1283')");
			}else{
				// $qGetcart = query_execute("SELECT cart_id,item_qty FROM shop_cart WHERE userid = '$user_id' AND item_id = '$itemReal' AND cart_order_status='new'");
				$qGetMyCart13 = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$user_id' AND sc.cart_order_status = 'new' AND sc.donate_bag=0 AND si.stock_status = 'instock' AND si.item_display_status != 'delete' AND ( sc.item_price != '0' OR sc.item_id = '1283' )");
			}
			if($item_status1=='hard'){
				if($cart_item_qty > $qBuySel['virtual_qty']){
					$cart_item_qty=$qBuySel['virtual_qty'];
					if($cart_item_qty >= '1'){
						$rowCart23 = mysql_fetch_array($qGetMyCart13);
						$cart_item_amount = $rowCart23["order_amount"];
						if(($cart_item_amount+($price*$cart_item_qty))<=20000000){
							$qGetcart = query_execute("UPDATE shop_cart SET item_qty='$cart_item_qty', item_discount=$saledisc, item_totalprice=item_price*item_qty WHERE cart_id = '$cartId'");
							$no_updated = mysql_affected_rows();
						}
						$data=array(
							'status' =>"success",
							'msg' =>"We have only ".$qBuySel['virtual_qty']." units of ".$qBuySel['name']." in stock.",
						);
					}
					
				}else{
					if($cart_item_qty >= '1'){
						$rowCart23 = mysql_fetch_array($qGetMyCart13);
						$cart_item_amount = $rowCart23["order_amount"];
						if(($cart_item_amount+($price*$cart_item_qty))<=20000000){
							$qGetcart = query_execute("UPDATE shop_cart SET item_qty='$cart_item_qty', item_discount=$saledisc, item_totalprice=item_price*item_qty WHERE cart_id = '$cartId'");
							$no_updated = mysql_affected_rows();
						}
						$data=array(
							'status' =>"success",
							'msg' =>"Item Qty Updated Successfully"
						);
					}
				}
			}else{
				if($cart_item_qty >= '1'){
					$rowCart23 = mysql_fetch_array($qGetMyCart13);
					$cart_item_amount = $rowCart23["order_amount"];
					if(($cart_item_amount+($price*$cart_item_qty))<=20000000){
						$qGetcart = query_execute("UPDATE shop_cart SET item_qty='$cart_item_qty', item_discount=$saledisc, item_totalprice=item_price*item_qty WHERE cart_id = '$cartId'");
						$no_updated = mysql_affected_rows();
					}
					$data=array(
						'status' =>"success",
						'msg' =>"Item Qty Updated Successfully"
					);
				}
			}
		}else{
			$data=array(
				'status'=>"fail",
				'msg' =>"some parameter are missing"
			);
		}
	}else{
		$data=array(
			'status'=>"fail",
			'msg' =>"Unauthorized Access"
		);
	}
	$das=json_encode($data);
	print $das;
	$return = app_logs($api_url, "Response", $log_id);
?>