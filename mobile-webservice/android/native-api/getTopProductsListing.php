<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once('constants.php');
	
	//$reuqest_time =  date('Y-m-d h:i:s:u');
	$api_url = "getTopProductsListing.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$data = array();
	$new_arrival = array();
	$accessToken = $_POST['access_token'];	
	$product_type = $_POST['product_type'];	
	$product_type_id = $_POST['product_type_id'];
	$item_id = $_POST['item_id'];
	$mode = $_POST['mode'];
	/* New arrival and all new arrivals */
	if($mode==$app_security_mode){
		$num_rec_per_page=10;
		if (isset($page)) { 
			$page  = $page; 
		}else { 
			$page=1; 
		} 
		$start_from = ($page-1) * $num_rec_per_page;
		
		if(($product_type == 'new_arrivals' && $product_type_id == '1') || ($product_type == 'all_new_arrival'  && $product_type_id == '2')){
			
			if($product_type == 'new_arrivals' && $product_type_id == '1' ){
			$limit = "LIMIT 5";
			// count row
			$SQ_shop_items = query_execute("Select DISTINCT(item_id) FROM shop_items  WHERE  item_brand!='0' AND item_brand='23' AND domain_id='1' AND visibility='visible' AND stock_status='instock' AND type_id !='configurable' AND item_display_status='active' group by child_cat ORDER BY item_id desc $limit");
			$totrecord = mysql_num_rows($SQ_shop_items);
			//end
		}
		elseif($product_type == 'all_new_arrival' && $product_type_id == '2'){
			$limit = "LIMIT $start_from, $num_rec_per_page";
			// count row
			$SQ_shop_items = query_execute("Select DISTINCT(item_id) FROM shop_items  WHERE  item_brand!='0' AND item_brand='23' AND domain_id='1' AND visibility='visible' AND stock_status='instock' AND type_id !='configurable' AND item_display_status='active' group by child_cat ORDER BY item_id desc");
			$totrecord = mysql_num_rows($SQ_shop_items);
			//end
		}
		 
		$qbrand = query_execute("SELECT DISTINCT(item_id) FROM shop_items WHERE item_brand!='0' AND item_brand='23' AND domain_id='1' AND visibility='visible' AND stock_status='instock' AND type_id !='configurable' AND item_display_status='active' GROUP BY child_cat ORDER BY item_id DESC $limit");
		while($rowBrand = mysql_fetch_array($qbrand)){
			$newbrandid[] = $rowBrand['item_id'];
		}
		$item_brand23 = implode(',',array_unique($newbrandid));
		if($item_brand23){
			$qItem = query_execute("SELECT item_id,name,nice_name,price,selling_price,item_parent_id,description FROM shop_items  WHERE item_id IN ($item_brand23) AND visibility='visible' AND stock_status='instock' AND type_id !='configurable' AND item_id!='0' AND item_id!='7870' AND item_id!='8494' AND item_display_status='active' ORDER BY created_at desc");
				while($rowItem = mysql_fetch_array($qItem)){
					$item_id = $rowItem["item_id"];
					$name = $rowItem["name"];
					$nice_name = $rowItem["nice_name"];
					$price = number_format($rowItem["price"],0);
					$selling_price = number_format($rowItem["selling_price"],0);
					$item_parent_id = $rowItem["item_parent_id"];
					$item_desc = $rowItem["description"];
					
					$qdataM = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
					$c=mysql_num_rows($qdataM);	
					if($c>0){
						$qdataM = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
						$qItemnice = query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='$item_id'");
						//$nice_name = $qItemnice["nice_name"];
					}else{
						$qdataM = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
						$qItemnice = query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='$item_parent_id'");
						//$nice_name = $qItemnice["nice_name"];
					}
					
					$rowdatM = mysql_fetch_array($qdataM);
					//get today's savings
					if($rowItem["selling_price"] > $rowItem["price"]){
						$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
						$item_discount_per=number_format($item_discount_per1,0);
					}else{
						$item_discount_per = "0";
					}
					//end
					if($rowdatM["media_file"]!=''){
						$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
						$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
						$imageURL = "/imgthumb/184x184-".$rowdatM["media_file"];
						$dest = $baseURL.$imageURL;
						createImgThumbIfnot($src,$dest,'184','184','ratiowh');
					}else{
						$src1 = "/shop/image/no_image_new.png";
						$imageURL = "/shop/image/no_image_new.png";
					}
							
					$new_arrival = array(
						'product_id' => $item_id,
						'product_name' => $name,
						'product_nicename' => "https://www.dogspot.in/".$nice_name,
						'product_image' => $site_url.$imageURL,
						'original_image' => $site_url.$src1,
						'retail_price' => $price,
						'currency' => 'INR',
						'sale_price' => $selling_price,
						'description' => $item_desc,
						'saving_percentage'=>$item_discount_per
					);
					$arrival_arr[] = $new_arrival;
				}
				$maxpages = ceil($totrecord/$num_rec_per_page);
				if($page < $maxpages){
					$next1 = $page+1;
					$next = "$next1";
				}else{
					$next = "0";	
				}
				if(count($arrival_arr) > 0){
					$data = array(
						'status'=>'success',
						'msg' => 'Record Found',
						'data' => $arrival_arr,
						'total_items' => "$totrecord",
						'tab_name' => "New Arrivals",
						'next' => $next
					);
				}else{
					$data = array(
						'status'=>'fail',
						'msg' => 'Record not Found',
						'data' => "",
						'total_items' => "$totrecord",
						'tab_name' => "New Arrivals",
						'next' => $next
					);
				}
			}	
		}
		/* New arrival and all new arrivals ENDS */
				
				
				
		/* YOU May Like (feature products) */
					
		elseif(($product_type == 'feature_products' && $product_type_id == '3') || ($product_type == 'all_feature_product' && $product_type_id == '4')){
			$countCheckSum='0';
			if($accessToken){
				$dataUserId=base64_decode($accessToken);
				//echo $dataUserId;
				$vardata = explode('##',$dataUserId);
				
				$user_id = $vardata [0];
				//echo $user_id;
				
				$check_order = mysql_query("SELECT so.order_id FROM shop_order as so WHERE so.userid='$user_id' AND so.mode != 'TEST' AND so.delevery_status = 'delivered' ORDER BY so.order_c_date DESC LIMIT 15");
				//echo "SELECT so.order_id FROM shop_order as so WHERE so.userid='$user_id' AND so.mode != 'TEST' AND so.delevery_status = 'delivered' ORDER BY so.order_c_date DESC LIMIT 15";
				$count = mysql_num_rows($check_order);
				
				
				if($count > 0){
					while($order_rec = mysql_fetch_array($check_order)){			          	
						$o_id[] = $order_rec['order_id'];
					}
					foreach($o_id as $order_id){	
						$check_items = mysql_query("SELECT cart_id, item_id, item_qty, item_price FROM shop_cart WHERE cart_order_id = '$order_id'");
						$item_data=array();
						while($getRow = mysql_fetch_array($check_items)){	
							$item_id = $getRow['item_id'];
							$qItem = query_execute("SELECT item_id,name,nice_name,price,description,selling_price,item_parent_id,stock_status FROM shop_items WHERE item_id !='1283' AND item_id !='0' AND price !='0' AND item_id='$item_id' AND (master_cat='283' OR master_cat='286')");
							if(mysql_num_rows($qItem)> 0){
								$rowItem = mysql_fetch_array($qItem);
								$item_id = $rowItem["item_id"];
								$name = $rowItem["name"];
								$nice_name = $rowItem["nice_name"];
								$item_desc = $rowItem["description"];
								$price = number_format($rowItem["price"],0);
								$selling_price = number_format($rowItem["selling_price"],0);
								$item_parent_id = $rowItem["item_parent_id"];
								$stock_status = $rowItem["stock_status"];
								if($item_id){
									$qdataM = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
									$qItemnice = query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='$item_id'");
									//$nice_name = $qItemnice["nice_name"];
								}
								if(mysql_num_rows($qdataM) < 1){
									$qdataM = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
									$qItemnice = query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_parent_id'");
									//$nice_name = $qItemnice["nice_name"];
								}
								$rowdatM = mysql_fetch_array($qdataM);
								//get today's savings
								if($rowItem["selling_price"] > $rowItem["price"]){
									$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
									$item_discount_per=number_format($item_discount_per1,0);
								}else{
									$item_discount_per = "0";				
								}
								//end
								if($rowdatM["media_file"]!=''){
									$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
									$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
									$imageURL = "/imgthumb/184x184-".$rowdatM["media_file"];
									$dest = $baseURL.$imageURL;
									createImgThumbIfnot($src,$dest,'184','184','ratiowh');
								}else{
									$src1 = "/shop/image/no_image_new.png";
									$imageURL = "/shop/image/no_image_new.png";
								}
								$recommanded_products = array(
									'product_id' => $item_id,
									'product_name' => $name,
									'product_nicename' => "https://www.dogspot.in/".$nice_name,
									'product_image' => $site_url.$imageURL,
									'original_image' => $site_url.$src1,
									'retail_price' => $price,
									'currency' => 'INR',
									'sale_price' => $selling_price,
									'description' => $item_desc,
									'saving_percentage'=>$item_discount_per
								);
								$data1[] = $recommanded_products;
							}else{
								$countCheckSum=0;
							}
						}
					}
					$totrecord12 =  count($data1);
					$countCheckSum =count($data1);
					if($product_type == 'feature_products'){
						$totrecord12 = 5;
					}
					$maxpages = ceil($totrecord12/$num_rec_per_page);
					if($page < $maxpages){
						$next1 = $page+1;
						$next = "$next1";
					}else{
						$next = "0";	
					}
					$countItme = count($data1);
					if(count($data1)>0){
						$data=array(
							'status' => "success",
							'msg' => "Record Found",
							'total_items'=>"$totrecord12",
							'data' => $data1,
							'tab_name' => "Personalized Recommendations",
							'count'=>"$countItme"
						);
					}else{
						$countCheckSum=0;
					}
				}else{
					$countCheckSum=0;
					
				}
			}
			if($countCheckSum=="0"){	
				if($product_type == 'feature_products' && $product_type_id == '3'){
					$limit = "LIMIT 5";
				}
				elseif($product_type == 'all_feature_product' && $product_type_id == '4'){
					$limit = "LIMIT $start_from, $num_rec_per_page";
				}	
				$qItem = query_execute("SELECT i.item_id,i.selling_price, i.name, i.nice_name, i.price,i.item_parent_id FROM shop_items_meta as m, shop_items as i WHERE i.visibility='visible' AND i.stock_status='instock' AND stock_status!='outofstock' AND i.type_id !='configurable' AND i.item_id=m.item_id  AND i.domain_id='1' ORDER BY updated_at DESC");		
				$totrecord = mysql_num_rows($qItem);
						
				$qItem = query_execute("SELECT i.item_id,i.selling_price, i.name, i.nice_name, i.price,i.item_parent_id FROM shop_items_meta as m, shop_items as i WHERE i.visibility='visible' AND i.stock_status='instock' AND stock_status!='outofstock' AND i.type_id !='configurable' AND i.item_id=m.item_id  AND i.domain_id='1' ORDER BY updated_at DESC $limit");	
				while($rowItem = mysql_fetch_array($qItem)){
					$item_id = $rowItem["item_id"];
					$name = $rowItem["name"];
					$nice_name = $rowItem["nice_name"];
					$item_desc = $rowItem["description"];
					$price = number_format($rowItem["price"],0);
					$selling_price = number_format($rowItem["selling_price"],0);
					$item_parent_id = $rowItem["item_parent_id"];
						
					if($item_parent_id == '0'){				
						$qCate = query_execute_row("SELECT ie.category_id,ic.category_name,ic.category_nicename FROM  shop_item_category as ie,shop_category as ic  WHERE ie.category_id=ic.category_id AND ie.item_id='$item_id' AND ic.category_parent_id!='0'");
						
						$category_name     = $qCate['category_name'];
						$category_nicename = $qCate['category_nicename'];
							
						$qdataM    = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
						
						$qItemnice = query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
						$nice_name = $qItemnice["nice_name"];
					}else{			
						$qCate = query_execute_row("SELECT ie.category_id,ic.category_name,ic.category_nicename FROM  shop_item_category as ie,shop_category as ic  WHERE ie.category_id=ic.category_id AND ie.item_id='$item_parent_id' AND ic.category_parent_id!='0'");
						
						$category_name     = $qCate['category_name'];
						$category_nicename = $qCate['category_nicename'];
						//$query = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC";	
						$qdataM    = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");						
						if(mysql_num_rows($qdataM) < 1){
							$qdataM    = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
						}
					}
					$rowdatM = mysql_fetch_array($qdataM);
					//get today's savings
					if($rowItem["selling_price"] > $rowItem["price"]){
						$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
						$item_discount_per=number_format($item_discount_per1,0);
					}else{
						$item_discount_per = "0";
					}
					//end
					if($rowdatM["media_file"]!=''){
						$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
						$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
						$imageURL = "/imgthumb/184x184-".$rowdatM["media_file"];
						$dest = $baseURL.$imageURL;
						createImgThumbIfnot($src,$dest,'184','184','ratiowh');
					}else{
						$src1 = "/shop/image/no_image_new.png";
						$imageURL = "/shop/image/no_image_new.png";
					}	
					$feature_products[] = array(
						'product_id' => $item_id,
						'product_name' => $name,
						'product_nicename' => "https://www.dogspot.in/".$nice_name,
						'product_image' => $site_url.$imageURL,
						'original_image' => $site_url.$src1,
						'retail_price' => $price,
						'currency' => 'INR',
						'sale_price' => $selling_price,
						'description' => $item_desc,
						'saving_percentage'=>$item_discount_per
		//				'query'=>$query
					);
					$maxpages = ceil($totrecord/$num_rec_per_page);
					if($page < $maxpages){
						$next1 = $page+1;
						$next = "$next1";
					}else{
						$next = "0";	
					}
					if(count($feature_products)>0){
						$data = array(
							'status'=>'success',
							'msg' => 'Record Found',
							'data' => $feature_products,
							'total_items' => "$totrecord",
							'tab_name' => "Recommended For You",
							'next'=>$next
						);						
					}else{
						$data = array(
							'status'=>'success',
							'msg' => 'Record Found',
							'data' => $feature_products,
							'total_items' => "$totrecord",
							'tab_name' => "Recommended For You",
							'next'=>$next
						);	
					}
				}
			}
		}
					
		/* YOU May Like (feature products) ENDS */
				
				
		/* Sale */
				
		elseif(($product_type == 'sale_items' && $product_type_id == '5')  || ($product_type == 'all_sale_item' && $product_type_id == '6')){		 
			if($product_type == 'sale_items' && $product_type_id == '5'){
				$limit = "LIMIT 5";
				/*$qshopItem = query_execute("SELECT item_id FROM shop_items WHERE price < selling_price  AND visibility='visible'  AND stock_status='instock' AND stock_status!='outofstock' AND (master_cat='11' OR master_cat='10' OR master_cat='56') GROUP BY item_brand $limit");*/	
				
				$qshopItem = query_execute("SELECT item_id FROM shop_items WHERE price < selling_price  AND visibility='visible'  AND stock_status='instock' AND type_id!='configurable' AND stock_status!='outofstock' AND item_id !='11138' AND item_brand='23' GROUP BY child_cat $limit");
					
				while($roworder1 = mysql_fetch_array($qshopItem)){		
					$item_item_id[] = $roworder1['item_id'];
				}   
				$item_item_id1 = array_unique($item_item_id);
	
				foreach($item_item_id1 as $item_item1){ 
					$pitem_id = $item_item1;
					if($pitem_id){
						$qItem          = query_execute("SELECT item_id,name,nice_name,price,selling_price,item_parent_id,stock_status,description FROM shop_items WHERE item_id !='1283' AND item_id !='0' AND item_id!='8494' AND price !='0' AND item_id='$pitem_id'");
						$rowItem        = mysql_fetch_array($qItem);
						$item_id        = $rowItem["item_id"];
						$name           = $rowItem["name"];
						$nice_name = $rowItem["nice_name"];
						$price          = number_format($rowItem["price"],0);
						$selling_price  = number_format($rowItem["selling_price"],0);
						$item_parent_id = $rowItem["item_parent_id"];
						$stock_status   = $rowItem["stock_status"];
						$nice_name   = $rowItem["nice_name"];
						$item_desc   = $rowItem["description"];
						$qdataM    = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
						$countImg=mysql_num_rows($qdataM);
							if($countImg>0)
							{
							$qdataM    = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	
							$qItemnice = query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
							}else{
						if($item_parent_id == '0'){
							$qdataM    = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
							$qItemnice = query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
							$nice_name = $qItemnice["nice_name"];
						}else{
							$qdataM    = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC  LIMIT 1");              
						}
							}
						$rowdatM = mysql_fetch_array($qdataM); 	
						//get today's savings
						if($rowItem["selling_price"] > $rowItem["price"]){
							$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
							$item_discount_per=number_format($item_discount_per1,0);
						}else{
							$item_discount_per = "0";
						}
						//end	
						if($rowdatM["media_file"]!=''){
							$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
							$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
							$imageURL = "/imgthumb/184x184-".$rowdatM["media_file"];
							$dest = $baseURL.$imageURL;
							createImgThumbIfnot($src,$dest,'184','184','ratiowh');
						}else{
							$src1 = "/shop/image/no_image_new.png";
							$imageURL = "/shop/image/no_image_new.png";
						}		
						$sale = array(
							'product_id' => $item_id,
							'product_name' => $name,
							'product_nicename' => "https://www.dogspot.in/".$nice_name,
							'product_image' => $site_url.$imageURL,
							'original_image' => $site_url.$src1,
							'retail_price' => $price,
							'currency' => 'INR',
							'sale_price' => $selling_price,
							'description' => $item_desc,
							'saving_percentage'=>$item_discount_per
						);
						$sale_arr[] = $sale;     			
					}
					$maxpages = ceil($totrecordinstock/$num_rec_per_page);
					if($page < $maxpages){
						$next1 = $page+1;
						$next = "$next1";
					}else{
						$next = "0";	
					}
					if(count($sale_arr) >0){
						$data = array(
							'status'=>'success',
							'msg' => 'Record Found',
							'data' => $sale_arr,
							'total_items' => "$totrecordinstock",
							'tab_name' => "Sale",
							'next'=>$next
						);
					}else{
						$data = array(
							'status'=>'fail',
							'msg' => 'Record not Found',
							'data' => "",
							'total_items' => "$totrecordinstock",
							'tab_name' => "Sale",
							'next'=>$next
						);
					}
				}
			}
			elseif($product_type == 'all_sale_item' && $product_type_id == '6'){
				$salinstock="item_sale:1 AND visibility:visible AND NOT type_id:configurable AND NOT item_display_status:delete AND NOT domain_id:2 AND stock_status:instock&version=2.2&start=$start_from&rows=$num_rec_per_page&fl=* score&qf=name^2&indent=on";
				//Check out of stock Items Data	
				$saloutofstock="item_sale:1 AND visibility:visible AND NOT type_id:configurable AND  stock_status:outofstock AND  domain_id:12 AND NOT item_display_status:delete$bsql1$bcsql1&version=2.2&start=$start_from&rows=$num_rec_per_page&fl=* score&qf=name^2&indent=on";
				
				$url = "http://localhost/solr/dogspotshopsolr/select/?q=$salinstock";
				$url = str_replace(" ","%20",$url);
				$resultsolr = get_solr_result($url);
				$totrecordinstock = $resultsolr['TOTALHITS'];
	
				//Check out of stock Items Data	
				$urloutofstock = "http://101.53.137.39/solr/dogspotshopsolr/select/?q=$saloutofstock";
				$urloutofstock = str_replace(" ","%20",$urloutofstock);
				$resultsolrout = get_solr_result($urloutofstock);
				$totrecordout = $resultsolrout['TOTALHITS'];
				$totrecord = $totrecordinstock+$totrecordout;
				foreach($resultsolr['HITS'] as $rowItemall){
					$Aitem_Itemid[]=$rowItemall["item_id"];
					$Aitem_brand[]=$rowItemall["item_brand"];
					$Aitem_price[]=$rowItemall["price"];
					$Aitem_attribute[]=$rowItemall["item_attribute"];
					$Aitem_life_stage[]=$rowItemall["item_life_stage"];
					$Aitem_breed_id[]=$rowItemall["item_breed_id"];
					$Aitem_breed_type[]=$rowItemall["item_breed_type"];
					$Aitem_category[]=$rowItemall["item_category_id"];//add code
					$Aitem_weight[]=$rowItemall["weight"];
					$A1weight_price[]=round($rowItemall["price"]/$rowItemall["weight"],3);
				}
				if($totrecord>0){
					$Acount=array_unique($Aitem_Itemid);
				}
				foreach($resultsolr['HITS'] as $rowItemP11){
					//while($rowItemP11 = mysql_fetch_array($qItem)){
					//echo $rowItemP11['item_id'].";";
					$Aitem_ItemidP3[]=$rowItemP11['item_id'];
				}
				if($category_id==86){
					$unique12 = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
					foreach($unique12 as $uu){
						$selbreedname=query_execute_row("SELECT item_id,item_display_order FROM shop_items WHERE  item_id='$uu'");
						$uniquedisplay[]=$selbreedname['item_display_order']."@@".$selbreedname['item_id'];
					}
					rsort($uniquedisplay);
					foreach($uniquedisplay as $uu1){
						$selbreedname12=explode("@@",$uu1);
						$unique[]=$selbreedname12[1];
					}
				}else{
					//category_id!=86	
					$selfindcat=query_execute_row("SELECT count(*) as cooo FROM item_display_order WHERE cat_id='$category_id'");	
					if($selfindcat['cooo']>'0'){
						
						$uniquec = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
						foreach($uniquec as $uu){
							$selbreedname=query_execute_row("SELECT item_id,display_order FROM item_display_order WHERE  item_id='$uu' AND cat_id='$category_id'");
							if($selbreedname['display_order']){$uniquedisplay1[]=$selbreedname['display_order']."@@".$selbreedname['item_id'];}else{
								$uniquedisplay1[]='0'."@@".$uu;
							}
						}
						asort($uniquedisplay1,1);
						foreach($uniquedisplay1 as $uu12){
							$selbreedname122=explode("@@",$uu12);if($selbreedname122[1]){
							$unique[]=$selbreedname122[1];}
						}
					}else{
						$unique = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
					}	
				}
	
				foreach($unique as $rowItemitem ){
					$rowItem=query_execute_row("SELECT tag,item_id,name,nice_name,description ,selling_price,item_parent_id,price,type_id,weight_price,item_brand,stock_status,created_at,weight,item_display_order from shop_items WHERE item_id='$rowItemitem'");
					$item_id=$rowItem["item_id"];
					$tag=$rowItem["tag"];
					$name=stripslashes($rowItem["name"]);
					$name=stripslashes($name);
					$nice_name=$rowItem["nice_name"];
					$type_id=$rowItem["type_id"];
					$price=number_format($rowItem["price"],0);
					$selling_price=number_format($rowItem["selling_price"],0);
					$item_parent_id=$rowItem["item_parent_id"];
					$stock_status=$rowItem["stock_status"];
					$brandd=$rowItem["item_brand"];
					$description=$rowItem["description"];
					if($item_parent_id == '0'){
						$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
					}else{
						$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
					}
					$rowdatM = mysql_fetch_array($qdataM);
					
					if($rowdatM["media_file"]){
						$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
						$imageURL='/imgthumb/150x160-'.$rowdatM["media_file"];
					}else{
						$src = $DOCUMENT_ROOT.'/shop/image/no_image_new.png';
						$imageURL='/imgthumb/150x160-no_image_new.png';
					}
					$dest = $DOCUMENT_ROOT.$imageURL;
					createImgThumbIfnot($src,$dest,'150','160','ratiowh');
	
					//get today's savings
					if($rowItem["selling_price"] > $rowItem["price"]){
						$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
						$item_discount_per=number_format($item_discount_per1,0);
					}else{
						$item_discount_per = "0";			
					}
					//end
					if($rowdatM["media_file"]!=''){
						$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
						$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
						$imageURL = "/imgthumb/150x160-".$rowdatM["media_file"];
						$dest = $baseURL.$imageURL;
						createImgThumbIfnot($src,$dest,'150','160','ratiowh');
					}else{
						$src1 = "/shop/image/no_image_new.png";
						$imageURL = "/imgthumb/150x160-no_image_new.png";
					}
					$sale = array(
						'product_id' => $item_id,
						'product_name' => $name,
						'product_nicename' => "https://www.dogspot.in/".$nice_name,
						'product_image' => $site_url.$imageURL,
						'original_image' => $site_url.$src1,
						'retail_price' => $price,
						'currency' => 'INR',
						'sale_price' => $selling_price,
						'description' => $item_desc,
						'saving_percentage'=>$item_discount_per
					);
					$sale_arr[] = $sale;
				}
				$maxpages = ceil($totrecordinstock/$num_rec_per_page);
				if($page < $maxpages){
					$next1 = $page+1;
					$next = "$next1";
				}else{
					$next = "0";	
				}
				if(count($sale_arr) >0){
					$data = array(
						'status'=>'success',
						'msg' => 'Record Found',
						'data' => $sale_arr,
						'total_items'=>"$totrecordinstock",
						'tab_name' => "Sale",
						'next'=>$next
					);
				}else{
					$data = array(
						'status'=>'fail',
						'msg' => 'Record not Found',
						'data' => "",
						'total_items'=>"$totrecordinstock",
						'tab_name' => "Sale",
						'next'=>$next
					);
				}
			}
		}
		/* Sale ENDS */	
		/* What Others Are Buying (recent) */
					
		elseif(($product_type == 'recent_items' && $product_type_id == '7') ||  ($product_type == 'all_recent_item' && $product_type_id == '8')){		
			if($product_type == 'recent_items' && $product_type_id == '7'){
				$limit = "LIMIT 5";
			}
			elseif($product_type == 'all_recent_item' && $product_type_id == '8'){
				$limit = "LIMIT 100";
			}		
							
			$qshopItem = query_execute("SELECT DISTINCT(c.item_id) as item_id FROM shop_cart as c,shop_items as i WHERE i.item_id=c.item_id AND c.item_id !='0' AND c.item_id!='8494' AND i.price!='0' AND c.mrp_price !='0' AND c.cart_order_id!='0' AND c.item_price!='0' AND i.stock_status='instock' AND stock_status!='outofstock' AND i.visibility='visible' AND i.item_display_status !='delete' AND c.cart_order_status='0' AND i.domain_id!='2' order by c.cart_id desc $limit");			
			while($roworder1 = mysql_fetch_array($qshopItem)){		
				$item_item_id[] = $roworder1['item_id'];
			}
			$item_item_id1 = implode(',',array_unique($item_item_id));
			if($product_type == 'recent_items' && $product_type_id == '7' ){
				$newlimit = "LIMIT 5";
				$limit12 = 5;
			}
			elseif($product_type == 'all_recent_item' && $product_type_id == '8'){
				$newlimit = "LIMIT $start_from, $num_rec_per_page";
				$limit12 = 100;
			} 		
			if($item_item_id1){
				$qItem = query_execute("SELECT item_id,name,nice_name,price,selling_price,item_parent_id,stock_status FROM shop_items WHERE item_id !='1283' AND item_id !='0' AND item_id!='8494' AND price !='0' AND item_id IN ($item_item_id1) $newlimit");
				while($rowItem        = mysql_fetch_array($qItem)){
					$item_id        = $rowItem["item_id"];
					$name           = $rowItem["name"];
					$nice_name 	  = $rowItem["nice_name"];
					$price          = number_format($rowItem["price"],0);
					$selling_price  = $rowItem["selling_price"];
					$item_parent_id = $rowItem["item_parent_id"];
					$stock_status   = $rowItem["stock_status"];
					$nice_name   = $rowItem["nice_name"];
					
					if($item_id){
						$qdataM=query_execute("SELECT media_id, media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
						$qItemnice = query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
						$nice_name = $qItemnice["nice_name"];
					}
					if(mysql_num_rows($qdataM) < 1){
						$qdataM=query_execute("SELECT media_id, media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
					}
					
					$rowdatM = mysql_fetch_array($qdataM); 
					//get today's savings
					if($rowItem["selling_price"] > $rowItem["price"]){
						$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
						$item_discount_per=number_format($item_discount_per1,0);
					}else{
						$item_discount_per = "0";
					}
					//end	
					if($rowdatM["media_file"]!=''){
						$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
						$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
						$imageURL = "/imgthumb/184x184-".$rowdatM["media_file"];
						$dest = $baseURL.$imageURL;
						createImgThumbIfnot($src,$dest,'184','184','ratiowh');
					}else{
						$src1 = "/shop/image/no_image_new.png";
						$imageURL = "/shop/image/no_image_new.png";
					}		
					$recent_products[] = array(
						'product_id' => $item_id,
						'product_name' => $name,
						'product_nicename' => "https://www.dogspot.in/".$nice_name,
						'product_image' => $site_url.$imageURL,
						'original_image' => $site_url.$src1,
						'retail_price' => $price,
						'currency' => 'INR',
						'sale_price' => number_format($selling_price,0),
						'description' => $item_desc,
						'saving_percentage'=>$item_discount_per
					);
					$maxpages = ceil($limit12/$num_rec_per_page);
					if($page < $maxpages){
						$next1 = $page+1;
						$next = "$next1";
					}else{
						$next = 0;	
					}
					if(count($recent_products) >0){
						$data = array(
							'status'=>'success',
							'msg' => 'Record Found',
							'data' => $recent_products,
							'total_items'=>"$limit12",
							'tab_name' => "Popular Products",
							'next'=>$next
						);
					}else{
						$data = array(
							'status'=>'fail',
							'msg' => 'Record not Found',
							'data' => "",
							'total_items'=>"$limit12",
							'tab_name' => "Popular Products",
							'next'=>$next
						);
					}
				}		
			}
		}
					
		/* What Others Are Buying (recent) ENDS */
		
		
		// Recommonded 
		elseif(($product_type == 'recommended' && $product_type_id == '9' && $item_id) || ($product_type == 'all_recommended' && $product_type_id == '10' && $item_id)){
			$sql1 = "SELECT child_cat FROM shop_items WHERE item_id='$item_id' AND child_cat!='' AND child_cat!='0'";
			$ocart = query_execute_row($sql1);
			$child_cat1 = $ocart['child_cat'];
			
			if($product_type == 'recommended' && $product_type_id == '9' && $item_id){
				$limit = "LIMIT 5";
			}
			elseif($product_type == 'all_recommended' && $product_type_id == '10' && $item_id){
				$limit = "LIMIT $start_from, $num_rec_per_page";
			}
	
			//foreach ($child_cat as $child_cat1) {	
			$sql12 = "SELECT distinct c.item_id as item_id FROM shop_cart as c,shop_items as i
	WHERE i.type_id!='configurable' AND i.item_id=c.item_id AND i.item_id !='0' AND c.item_id !='0' AND i.price!='0' AND i.item_id!='$item_id' AND i.item_id!='8494' AND c.item_price !='0' AND c.mrp_price !='0' AND c.cart_order_id!='0' AND c.item_price!='0' AND i.child_cat='$child_cat1' AND i.stock_status='instock' AND stock_status!='outofstock' AND i.visibility='visible' AND i.item_display_status !='delete' order by c.cart_id desc";
			$qshopItem112 = query_execute($sql12);
			$totrecord12 = mysql_num_rows($qshopItem112);
				
			$sql = "SELECT distinct c.item_id as item_id FROM shop_cart as c,shop_items as i
	WHERE i.type_id!='configurable' AND i.item_id=c.item_id AND i.item_id !='0' AND c.item_id !='0' AND i.price!='0' AND i.item_id!='$item_id' AND i.item_id!='8494' AND c.item_price !='0' AND c.mrp_price !='0' AND c.cart_order_id!='0' AND c.item_price!='0' AND i.child_cat='$child_cat1' AND i.stock_status='instock' AND stock_status!='outofstock' AND i.visibility='visible' AND i.item_display_status !='delete' order by c.cart_id desc $limit";		
			$qshopItem1 = query_execute($sql);
			while($qshopItem = mysql_fetch_array($qshopItem1)){
				$pitem_id = $qshopItem["item_id"];
				if ($pitem_id){
					$qItem = query_execute("SELECT item_id,name,nice_name,price,description,selling_price,item_parent_id,stock_status FROM shop_items WHERE item_id !='1283' AND item_id !='0' AND price !='0' AND item_id='$pitem_id'");
					$rowItem = mysql_fetch_array($qItem);
					$item_id = $rowItem["item_id"];
					$name = $rowItem["name"];
					$nice_name = $rowItem["nice_name"];
					$item_desc = $rowItem["description"];
					$price = number_format($rowItem["price"],0);
					$selling_price = number_format($rowItem["selling_price"],0);
					$item_parent_id = $rowItem["item_parent_id"];
					$stock_status = $rowItem["stock_status"];
					
					if($item_id){
						$qdataM = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
						$qItemnice = query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='$item_id'");
						//$nice_name = $qItemnice["nice_name"];
					}
					if(mysql_num_rows($qdataM) < 1){
						$qdataM = mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
						$qItemnice = query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_parent_id'");
						//$nice_name = $qItemnice["nice_name"];
					}
					$rowdatM = mysql_fetch_array($qdataM);
					//get today's savings
					if($rowItem["selling_price"] > $rowItem["price"]){
						$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
						$item_discount_per=number_format($item_discount_per1,0);
					}else{
						$item_discount_per = "0";				
					}
					//end
					if($rowdatM["media_file"]!=''){
						$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
						$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
						$imageURL = "/imgthumb/184x184-".$rowdatM["media_file"];
						$dest = $baseURL.$imageURL;
						createImgThumbIfnot($src,$dest,'184','184','ratiowh');
					}else{
						$src1 = "/shop/image/no_image_new.png";
						$imageURL = "/shop/image/no_image_new.png";
					}
					$recommanded_products = array(
						'product_id' => $item_id,
						'product_name' => $name,
						'product_nicename' => "https://www.dogspot.in/".$nice_name,
						'product_image' => $site_url.$imageURL,
						'original_image' => $site_url.$src1,
						'retail_price' => $price,
						'currency' => 'INR',
						'sale_price' => $selling_price,
						'description' => $item_desc,
						'saving_percentage'=>$item_discount_per
					);
				}  
				$data1[] = $recommanded_products; 
			}
			if($product_type == 'recommended'){
				$totrecord12 = 5;
			}
			$maxpages = ceil($totrecord12/$num_rec_per_page);
			if($page < $maxpages){
				$next1 = $page+1;
				$next = "$next1";
			}else{
				$next = "0";	
			}
			$countItme = count($data1);
			if(count($data1)>0){
				$data=array(
					'status' => "success",
					'msg' => "Record Found",
					'total_items'=>"$totrecord12",
					'data' => $data1,
					'tab_name' => "Recommended Products",
					'count'=>"$countItme",
					'next'=>"$next"
				);
			}else{
				$data=array(
					'status' => "fail",
					'msg' => "Record not Found",
					'data' => $data1,
					'total_items'=>"$totrecord12",
					'tab_name' => "Recommended Products",
					'count'=>"$countItme",
					'next'=>"$next"
				);
			}	
		
		}
		//End Recommonded
		else{
			$data=array(
				'status' => "fail",
				'msg' => "In sufficient Parameter",
				'data' => "",
				'total_items'=>$totrecord12
			);	
		}
	}else{
		$data=array(
			'status' => "fail",
			'msg' => "Unauthodirzed Access",
			'data' => "",
			'total_items'=>"$totrecord12"
		);
	}
	$das=json_encode($data);
	echo $das;
	$return = app_logs($api_url, "Response", $log_id);	
?>