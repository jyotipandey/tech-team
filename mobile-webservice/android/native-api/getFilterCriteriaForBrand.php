<?php
	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	require_once($baseURL . '/arrays.php');
	require_once('constants.php');

	$api_url = "getFilterCriteriaForBrand.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	$brand_id = $_POST['brand_id'];
if($mode==$app_security_mode){
	if($brand_id){
		$qItemAll=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price,i.weight_price, i.item_brand,i.stock_status,i.created_at,i.weight,j.breed_id,j.breed_type,j.life_stage,i.item_display_order FROM breed_shop_items as j, shop_items as i WHERE j.item_category_id = '15' AND i.visibility='visible' AND i.item_brand= '$brand_id' AND i.item_display_status!='delete' AND i.item_id=j.item_id");
		$totrecord = mysql_num_rows($qItemAll);
		
		$qitem_breeds=query_execute("SELECT * from breed_shop_items as s,shop_item_category as c WHERE c.item_id=s.item_id AND s.item_category_id='15'");
		
		$qItemorg=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price,i.weight_price, i.item_brand,i.stock_status,i.created_at,i.weight,j.breed_id,j.breed_type,j.life_stage,i.item_display_order FROM breed_shop_items as j, shop_items as i WHERE j.item_category_id = '15' AND i.item_brand= '$brand_id' AND i.visibility='visible' AND i.item_display_status!='delete' AND i.item_id=j.item_id");
		$totrecord123 = mysql_num_rows($qItemorg);
	}
	
	while($qitem_b = mysql_fetch_array($qitem_breeds)){
		$Abreed_id1[]=$qitem_b["breed_id"];
		$Abreed_type1[]=$qitem_b["breed_type"];
		$Alife_stage1[]=$qitem_b["life_stage"];
	}
	$Abreed_id = array_unique($Abreed_id1);
	$Abreed_type = array_unique($Abreed_type1);
	$Alife_stage = array_unique($Alife_stage1);

	while($rowItemall = mysql_fetch_array($qItemAll)){
		$Aitem_brand[]=$rowItemall["item_brand"];
		$Aitem_price[]=$rowItemall["price"];
		$Aweight[]=$rowItemall["weight"];
		$Aweight_price[]=$rowItemall["weight_price"];
		
	}
	while($rowItemsearch = mysql_fetch_array($qItemorg)){
		$A1item_brand[]=$rowItemsearch["item_brand"];
		$A1item_price[]=$rowItemsearch["price"];
		$A1weight[]=$rowItemsearch["weight"];
		$A1weight_price[]=$rowItemsearch["price"]/$rowItemsearch["weight"];
		
	}
	
	if($totrecord123>0){
		
		$A1item_price=array_unique($A1item_price);
		$A1weight=array_unique($A1weight);
		$A1weight_price=array_unique($A1weight_price);
		
		// Weight Price..........
		asort($A1weight_price);
		foreach($A1weight_price as $w1tp){
			$ANA1weight_price[]=$w1tp;
		}
		$endnum_wt_price = min($ANA1weight_price);
		$startnum_wt_price = max($ANA1weight_price);
		$data_wt_price=array('weight_min_price' =>$endnum_wt_price,'weight_max_price' =>$startnum_wt_price);
		$data_wtprice[]=$data_wt_price;
		$price1 = array("type"=>"range", "Title"=>"RATE PER KG","data"=>$dataWeight);
		
		// Weight...........
		asort($A1weight);
		foreach($A1weight as $w1t){
			$ANitem1_weight[]=$w1t;
		}
		
		$endnum_wt = min($ANitem1_weight);
		$startnum_wt = max($ANitem1_weight);
		$wt_price=array('min_weight' =>$endnum_wt,'max_weight' =>$startnum_wt);
		$dataWeight[]=$wt_price;
		$price2 = array("type"=>"range", "Title"=>"WEIGHT","data"=>$dataWeight);
		
		// Price...........
		asort($A1item_price);
		foreach($A1item_price as $a1p){
			$ANitem1_price[]=$a1p;
		}
		$endnum_item_p = min($ANitem1_price);
		$startnum_item_p = max($ANitem1_price);
		$item_price = array('min_price' =>$endnum_item_p,'max_price' =>$startnum_item_p);
		$dataprice[]=$item_price;
		$price3 = array("type"=>"range", "Title"=>"PRICE RANGE","data"=>$dataprice);
		
		foreach($Alife_stage as $l_stage){ 
			if($l_stage!='' && ($l_stage!='7' && $l_stage!='3')){
				
				$attri[] = array('value'=>$l_stage,'name'=>$Astyle_breed[$l_stage]);
			}
			$mainn[Life_Stages]=$attri;
			$mainn[type]="Optional";
		}
		
		foreach($Abreed_id as $breed_ids){ 
			if($breed_ids!='0' && ($breed_ids=='11'|| $breed_ids=='23'|| $breed_ids=='37'|| $breed_ids=='60'|| $breed_ids=='65'|| $breed_ids=='67'|| $breed_ids=='86'|| $breed_ids=='108'|| $breed_ids=='112')){
				
				$breedquery=query_execute_row("SELECT breed_name, nicename FROM dog_breeds WHERE breed_id='$breed_ids'");
				if($breedquery['breed_name']=='Cocker Spaniel (English)'){
					$breedquery['breed_name']='Cocker Spaniel';
				}
				if($breedquery['breed_name']=='German Shepherd Dog (Alsatian)'){
					$breedquery['breed_name']='German Shepherd';
				}
				$attri1[] = array('value'=>$breed_ids,'name'=>$breedquery['breed_name']);
				$mainn[Breeds]=$attri1;
				$mainn[type]="Optional";	
			}
		}
		
		foreach($Abreed_type as $breed_type){ 
			if($breed_type!=''){
				$attri2[] = array('value'=>$breed_type,'name'=>$Abreed_type_set[$breed_type]);
			}
			$mainn[Breed_Type]=$attri2;
			$mainn[type]="Optional";
		}

		$datat1= array('price_data'=>$price3, 'weight_price_data'=>$price1, 'weight_data'=>$price2);
		if(count($mainn) > 0){
			echo json_encode(array_merge($datat1,$mainn));
			$return = app_logs($api_url, "Response", $log_id);
		}else{
			echo json_encode($datat1);
			$return = app_logs($api_url, "Response", $log_id);
		}
	}else{
		$datat1 = array(
			'status'=>"no filter criteria found",
			'brand_id'=>$brand_id
		);
		echo json_encode($datat1);
		$return = app_logs($api_url, "Response", $log_id);
	}
}else{
	$datat1 = array(
		'status'=>"Unauthorized Access",
		'brand_id'=>$brand_id
	);
	echo json_encode($datat1);
	$return = app_logs($api_url, "Response", $log_id);
}
?>