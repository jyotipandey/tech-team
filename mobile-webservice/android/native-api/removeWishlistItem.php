<?php
	/*
	*	Remove Items from WishList 
	*	Created By Bharat On dated: 03/04/2015 
	*/
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/new/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once('constants.php');  // security key Pass
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	
	$api_url = "removeWishlistItem.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);
	
	$getMode = $_POST['mode'];
	//$mode = base64_decode($getMode);
	$mode = $getMode;
	$access_token = $_POST['access_token'];	
	$wishlist_id = $_POST['wishlist_id'];
	$item_id = $_POST['item_id'];
	
	if($mode==$app_security_mode){
		if($access_token){
			$getUsereId=base64_decode($access_token);	
			$vardata = explode('##',$getUsereId);
			$user_id = $vardata [0];
			$device_id = $vardata [1];
			if($user_id!=''){
				if($wishlist_id!='' && $item_id!='0' ){
					$SQ_shop_wishList = query_execute_row("SELECT * FROM shop_wishlist WHERE wishlist_id = '$wishlist_id' AND item_id='$item_id' AND userid='$user_id'");
					if($SQ_shop_wishList['wishlist_id']!=""){
						//echo "DELETE FROM shop_wishlist WHERE wishlist_id='".$SQ_shop_wishList['wishlist_id']."' AND item_id = '$item_id' AND userid='$user_id'";
						$qRemoveItemCart = query_execute("DELETE FROM shop_wishlist WHERE wishlist_id='".$SQ_shop_wishList['wishlist_id']."' AND item_id = '$item_id' AND userid='$user_id'");
						$no_deleted = mysql_affected_rows();
						$data=array(
							'status' =>"success",
							'msg'=> "Item has been deleted from Wishlist"
						);
					}else{
						$data=array(
							'status' =>"fail",
							'msg' => "Item has not found in wishlist"
						);
					}
				}else{
					$data=array(
						'status' =>"fail",
						'msg' => "Insufficient Parameters"
					);
				}
			}else{
				$data=array(
					'status' => 'fail',
					'msg' => "User ID not valid"
				);
			}
		}else{
			$data=array(
				'status' =>"fail",
				'msg' => "Access Token Not Valid"
			);
		}
	}else{
		$data=array(
			'status' =>"fail",
			'msg' => "Unauthorized Access"
		);
	}
	$das = json_encode($data);
	echo $das;
	$return = app_logs($api_url, "Response", $log_id);
?>