<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once('constants.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	
	$api_url = "getSearchResults.php"; 
	$log_id = "";
	$log_id = app_logs($api_url, "Request",$log_id);

	$ecomm_prodid = "";
	$ecomm_pagetype = 'searchresults';
	$ecomm_totalvalue = "";
	$google_conversion_label="hWRsCPrz2gIQvoe25QM";
		
	//$keyword="Dog Crate";
	$mode = $_POST['mode'];
	$q=$_POST['keyword'];	
	$page=$_POST['page'];
	//paging
	$num_rec_per_page=10;
	if (isset($page)) { 
		$page  = $page; 
	}else { 
		$page=1; 
	}
	$start_from = ($page-1) * $num_rec_per_page;
	//end paging							// request param shopquery
	
	if($mode==$app_security_mode){
		$q=str_replace("?filter=filter", "", $q);
		$ci=1;
		$ci1=1;
		$query=escapeSolrValueNew(trim($q), '');
		$maxshow='6';
		$shopmaxshow='80';
			
		$query=urlencode($query);
		if(str_word_count(trim($query)) > 1){
			//$query = '\"'.$query.'\"~1';
		}
			
		if($query){
			$url = "http://101.53.137.39/solr/dogspotshopsolr/select?q=$query&wt=xml&mm=1&indent=true&defType=dismax&qf=title^2+keyword_r1^6+keyword_r2^4+keyword_r3^3+text^1&fq=visibility:visible&fq=type_id:simple&fq=-domain_id:2&fq=-item_display_status:delete&fq=stock_status:instock&start=$start_from&rows=$num_rec_per_page";
			$url = str_replace(" ","",$url);
			if($userid=='shailesh' || $userid=='brajendra'){
				echo $url;
			}
			$result = get_solr_result($url);
			$totrecord = $result['TOTALHITS'];
			
			$maxpages = ceil($totrecord/$num_rec_per_page);
			if($page < $maxpages){
				$next = $page+1;
			}else{
				$next = 0;	
			}
			
			if($totrecord=='0'){
				$url = "http://101.53.137.39/solr/dogspotshopsolr/select?q=$query&wt=xml&mm=1&indent=true&defType=dismax&qf=title%5E5+category_name%5E3+brand_name%5E2+text%5E1&df=item_about&fq=visibility:visible&fq=type_id:simple&fq=domain_id:3&fq=-item_display_status:delete&fq=stock_status:instock&start=$start_from&rows=$num_rec_per_page";
				$result = get_solr_result($url);
				$totrecord = $result['TOTALHITS'];
			}
			if($totrecord!='0'){
							
				foreach($result['HITS'] as $rowItem){
					$item_id=$rowItem["item_id"];			// item_id returns in decimal number format like 1451.0
					
					// convert into number format like 1,451
					$item_id = number_format($item_id,0);
					// replace number 1,000 into number format like 1451
					$item_id = str_replace( ',', '', $item_id );
					
					$name=$rowItem["name"];
					$nice_name=$rowItem["nice_name"];
					$created_at=$rowItem["created_at"];
					$creat=explode("T", $created_at);
					$end_date=date("Y-m-d ");
					$thirtydays = date_create('30 days ago');
					$startdate=date_format($thirtydays, 'Y-m-d');
					//echo "required".$creat[0];
					//echo "start".$startdate."<br>"."end".$end_date;
					if($startdate <= $creat[0] && $creat[0] <= $end_date){
						$flag='1';
					}
					else{ 
						$flag='0';
					}
				
					$nice_name=$rowItem["nice_name"];
					$price=$rowItem["price"];
					$description=$rowItem["description"];
					$selling_price=$rowItem["selling_price"];
					$item_parent_id=$rowItem["item_parent_id"];
					$stock_status=$rowItem["stock_status"];
					if($item_parent_id == '0'){
						$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
					}else{
						$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
					}
					$rowdatM = mysql_fetch_array($qdataM);
					if($rowdatM["media_file"]!=''){
						$src1 = "/shop/item-images/orignal/".$rowdatM["media_file"];
						$src = $baseURL."/shop/item-images/orignal/".$rowdatM["media_file"];
						$imageURL = "/imgthumb/184x184-".$rowdatM["media_file"];
						
					}else{
						$src1 = "/shop/image/no_image_new.png";
						$src = $baseURL."/shop/image/no_image_new.png";
						$imageURL = "/shop/image/no_image_new.png";
					}
					
					$dest = $baseURL.$imageURL;
					createImgThumbIfnot($src,$dest,'184','184','ratiowh');
					
					if($rowItem["selling_price"] > $rowItem["price"]){
						$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
						$item_discount_per=number_format($item_discount_per1,0);
					}else{
						$item_discount_per = "0";				
					}		
								
					$data=array(
						'product_id' =>$item_id,
						'product_name' =>$name,
						'nice_name' =>$nice_name,
						'product_image' =>$site_url.$imageURL,
						'retail_price' =>number_format($price,0),
						'currency' =>'INR',
						'sale_price' =>number_format($selling_price,0),
						'description' =>$description,
						'saving_percentage'=>$item_discount_per
					);
					$datat[]=$data; 
				}
				$datat = array(
					'status'=>"success",
					'product_data' => $datat,
					'total_items'=>$totrecord,
					'msg'=>"Record Found",
					'next'=>$next
				);
			}
			else{
				$data[]=array(
					'product_id' =>"",
					'product_name' =>"",
					'nice_name' =>"",
					'product_image' =>"",
					'retail_price' =>"",
					'currency' =>'',
					'sale_price' =>"",
					'description' =>"",
					'saving_percentage'=>""
				);
				$datat = array(
					'status'=>"fail",
					'product_data' => $data,
					'total_items'=>$totrecord,
					'msg'=>"Record not Found",
					'next'=>"0"
				);
			}
		}
		else{
			$data[]=array(
				'product_id' =>"",
				'product_name' =>"",
				'nice_name' =>"",
				'product_image' =>"",
				'retail_price' =>"",
				'currency' =>'',
				'sale_price' =>"",
				'description' =>"",
				'saving_percentage'=>""
			);
			$datat = array(
				'status'=>"fail",
				'product_data' => $data,
				'total_items'=>$totrecord,
				'msg'=>"Keyword Not Found",
				'next'=>"0"
			);
		}	
	}else{
		$data[]=array(
			'product_id' =>"",
			'product_name' =>"",
			'nice_name' =>"",
			'product_image' =>"",
			'retail_price' =>"",
			'currency' =>'',
			'sale_price' =>"",
			'description' =>"",
			'saving_percentage'=>""
		);
		$datat = array(
			'status'=>"fail",
			'product_data' => $data,
			'total_items'=>$totrecord,
			'msg'=>"Unauthorized Access",
			'next'=>"0"
		);
	}
	$das=json_encode($datat);
	echo $das;
	$return = app_logs($api_url, "Response", $log_id);
?>