<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/session.php');
	
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "getTrackMyOrder.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);
	
	if($access_token){
		$data=base64_decode($access_token);	
		$vardata = explode('##',$data);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
	}
		
	if($user_id!="Guest"){		
		//get token and get userid from this token using algo...
		$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id' AND userid='$user_id' AND order_type='complete'");
		if($SQ_shop_order['order_id']!=""){
			// Get Courier Name and Url
			$SQ_courier = query_execute_row("SELECT * FROM shop_courier WHERE courier_id='".$SQ_shop_order['shop_courier_id']."'");
			
			// Order is in New
			if($SQ_shop_order['order_status']==1){
				$order_date = $SQ_shop_order['order_c_date'];
				$date = showdate($order_date, "d M o");
				$awb_no = $SQ_shop_order['order_tracking'];
				$courier_id = $SQ_shop_order['shop_courier_id'];
				$time = date('h:i A', strtotime($order_date));
				$sqlcomplete = $date. ' | ' .$time. '|' ."Your order has been failed";
			}else{
				$order_date = $SQ_shop_order['order_c_date'];
				$date = showdate($order_date, "d M o");
				$awb_no = $SQ_shop_order['order_tracking'];
				$courier_id = $SQ_shop_order['shop_courier_id'];
				$time = date('h:i A', strtotime($order_date));
				$sqlcomplete = $date. ' | ' .$time. '|' ."Your order has been placed";
			}
			
			
			//Order is in Dispatched-Ready
			$sqld1 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched-ready%' AND review_section_id='$order_id'");
			$sqlsdat1 = $sqld1['c_date'];
			$date2 = showdate($sqlsdat1, "d M o");
			$time2 = date('h:i A', strtotime($sqlsdat1));
			if($sqlsdat1 == ''){
				$sqlcomplete1 = "";
			}else{
				$sqlcomplete1 = $date2. ' | ' .$time2. ' | ' ."Your order has been dispatched";
			}
			
			
			// Order is in Dispatched
			$sqld2 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched%' AND review_section_id='$order_id'");
			$sqlsdat3 = $sqld2['c_date'];
			$date3 = showdate($sqlsdat3, "d M o");
			$time3 = date('h:i A', strtotime($sqlsdat3));
			if($sqlsdat3 == ''){
				$sqlcomplete2 = "";
			}else{
				$sqlcomplete2 = $date3. ' | ' .$time3. ' | ' ."Your order is Out for delivery";
			}
			
			//Order is in delivered
			$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%Delivered%' AND review_section_id='$order_id'");
			if($sqld4['c_date']== ''){
				$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%delivered%' AND review_section_id='$order_id'");
				$sqlsdat4 = $sqld4['c_date'];	
			}
			$sqlsdat4 = $sqld4['c_date'];
			$date7 = showdate($sqlsdat4, "d M o");
			$time7 = date('h:i A', strtotime($sqlsdat4));
			if($sqlsdat4 == ''){
				$sqlcomplete3 = "";
			}else{
				$sqlcomplete3 = $date7. ' | ' .$time7. ' | ' .'Your order has been delivered';
			}
			
			// Order is in Delivered
			$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%Delivered%' AND review_section_id='$order_id'");
			if($sqld4['c_date']== ''){
				$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%delivered%' AND review_section_id='$order_id'");
				$sqlsdat4 = $sqld4['c_date'];	
			}
			$sqlsdat4 = $sqld4['c_date'];
			$date7 = showdate($sqlsdat4, "d M o");
			$time7 = date('h:i A', strtotime($sqlsdat4));
			if($sqlsdat4 == ''){
				$sqlcomplete4 = "";
			}else{
				$sqlcomplete4 = $date7. ' | ' .$time7. ' | ' .'Your order has been delivered';
			}
			
			//Order is Cancelled
			$sqld5 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%cancelled%' AND review_section_id='$order_id'");
			if($sqld5['c_date'] ==''){
				$sqld5 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%canceled%' AND review_section_id='$order_id'");
			}
			$sqlsdat5 = $sqld5['c_date'];
			$datetimearray5 = explode(" ", $sqlsdat5);
			$date5 = $datetimearray5[0];
			$time5 = $datetimearray5[1];
			$reformatted_date5 = date('d-m-Y',strtotime($date5));
			$reformatted_time5 = date('Gi.s',strtotime($time5));
			if($sqlsdat5 == ''){
				$sqlcomplete5 = "";
			}else{
				$sqlcomplete5 = $date5. ' | ' .$time5. ' | ' ."Your order has been cancelled";
			}
			
			
			if($sqlcomplete1=="" && $sqlcomplete5!=""){
				$data = array(
					'status' =>'success',
					'step' =>$sqlcomplete,
					'step1' =>$sqlcomplete5
				);
			}elseif($sqlcomplete2=="" && $sqlcomplete5!=""){
				$data = array(
					'status' =>'success',
					'step' =>$sqlcomplete,
					'step1' =>$sqlcomplete1,
					'step2' =>$sqlcomplete5
				);
			}elseif($sqlcomplete3=="" && $sqlcomplete5!=""){
				$data = array(
					'status' =>'success',
					'step' =>$sqlcomplete,
					'step1' =>$sqlcomplete1,
					'step2' =>$sqlcomplete2,
					'step3' =>$sqlcomplete5
				);
			}elseif($sqlcomplete4=="" && $sqlcomplete5!=""){
				$data = array(
					'status' =>'success',
					'step' =>$sqlcomplete,
					'step1' =>$sqlcomplete1,
					'step2' =>$sqlcomplete2,
					'step3' =>$sqlcomplete3,
					'step4' =>$sqlcomplete5,
					'courier_name'=>$SQ_courier['courier_name'],
					'tracking_no' => $SQ_shop_order['order_tracking'],
					'tracking_url' => $SQ_courier['courier_tracking_url']
				);
			}else{
				$data = array(
					'status' =>'success',
					'step' =>$sqlcomplete,
					'step1' =>$sqlcomplete1,
					'step2' =>$sqlcomplete2,
					'step3' =>$sqlcomplete3,
					'step4' =>$sqlcomplete4,
					'courier_name'=>$SQ_courier['courier_name'],
					'tracking_no' => $SQ_shop_order['order_tracking'],
					'tracking_url' => $SQ_courier['courier_tracking_url']
				);
			}
		}else{
			$data = array(
				'status' =>'Record not found'
			);
		}
	}else{
		$data = array(
			'status' =>'Not a valid user ID'
		);
	}
	$das=json_encode($data);
	echo $das;
	$return = appLogPhoneGap($api_url, "Response", $log_id);
?>