<?php
	
	/*
	*	Response file is used to collect all data of an Order with Payment Details From app and redirect to Payment GetWay
	* 	Created By Umesh & Brajendra Sir, Dated: 16/01/2015
	* 	Required Parameters
	*	1- $access_token
	*	2- $payment_type
	*	3- $order_id
	*	4- $cod_check
	*	5- $discount_code1
	*/
	
	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	//require_once($baseURL.'/session.php');
	
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "checkout.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);
	
	if($access_token){
		$data=base64_decode($access_token);	
		$vardata = explode('##',$data);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		
	}
	else{
		$user_id = "Guest";	
	}
	
	if($user_id!="Guest" && $payment_type && $order_id && $session_id){

		// Payment Type is COD and Demand Draft
		if($payment_type=="cod" || $payment_type=="dd"){
			
			$StatusCheck = query_execute_row("SELECT delevery_status, cod_confirm FROM shop_order WHERE order_id = '$order_id'");
			if($StatusCheck['delevery_status']!='new') {
				header("Location: /new/shop/myorder.php?order_id=$order_id");
				exit();
			}
			$domainUpdate = query_execute("UPDATE shop_order SET domain_id = '1' WHERE order_id = '$order_id'");
			
			$querydonate = query_execute("SELECT * FROM shop_cart WHERE cart_order_id='$order_id' AND donate_bag='1' ");
			
			$cdonate = mysql_num_rows($querydonate);
			
			if($payment_type=='cod'){
				$order_status = 0;
				$check_amount = query_execute("SELECT order_shipment_amount from shop_order WHERE order_id = '$order_id'");
				$cod_amount = $check_amount['order_shipment_amount'];
				if($cod_amount == "0.0000"){
					$ship_amount = 99;
					$updateship = query_execute("UPDATE shop_order SET order_shipment_amount = '$ship_amount' WHERE order_id = '$order_id'");
				}
			}
			else{
				$order_status = 1;
			}
			
			$DateCreated=date("o m d");
			
			$resultinsert = query_execute("UPDATE shop_order SET order_status = '$order_status', mode = '$payment_type' WHERE order_id = '$order_id'");
			
			//echo "SELECT order_items_amount,order_discount_amount FROM shop_order WHERE order_id='$order_id'";
		 	
			$upOrder1 = query_execute_row("SELECT order_items_amount,order_discount_amount FROM shop_order WHERE order_id='$order_id'");
		  	
			$upOrder2 = query_execute_row("SELECT cart_id FROM shop_cart WHERE cart_order_id='$order_id' AND donate_bag='2'");
		  
		  	if($upOrder2['cart_id']==''){
			
				$check1=$upOrder1['order_items_amount']-$upOrder1['order_discount_amount'];
				//echo "check1".$check1;
			
				if($check1>=2000){
					//response($session_id,$userid,$order_id,$order_status);
				}
		  	}
		  	if($cdonate==0){
		  		response($session_id,$userid,$order_id,$order_status);
		  	}
			else{
				responsedonate($session_id,$userid,$order_id,$order_status);
		  	}
				
			$resultinsert1 = query_execute("UPDATE shop_cart SET cart_order_status = '$order_status' WHERE cart_order_id = '$order_id'");
		}
		$quseridCOD = query_execute_row("SELECT u_id FROM shop_order WHERE order_id='".$order_id."'");
	
		$getc = "SELECT count(*) as successrate FROM shop_order Where mode!='TEST' AND delevery_status='delivered' AND (userid='".				
		
		$quseridCOD['u_id']."' OR u_id='".$quseridCOD['u_id']."')";
		
		$qselectCOD = query_execute_row($getc);
		
		$sel_cart_items_code = mysql_query("SELECT * FROM shop_cart WHERE cart_order_id='$order_id'");
		
		$sel_cart_items_code2 = query_execute_row("SELECT order_amount,order_discount_id FROM shop_order WHERE order_id='$order_id'");
		while($sel_cart_items_code1 = mysql_fetch_array($sel_cart_items_code)){
			$code_items = $sel_cart_items_code1['item_id'];
			$code_item_qty = $sel_cart_items_code1['item_qty'];
			$analyticsArray[$code_items] = $code_item_qty;
		}
		
		couponcheck($order_id);
		
		if($payment_type=='cod'){
			$data = array(
				'order_id'=>$order_id,
				'payment_status'=>"Success",
				'payment_method'=>$AOrderMethod[$payment_type]
			);	
		}
		
		if($payment_type=='dd'){
			$data = array(
				'order_id'=>$order_id,
				'payment_status'=>"Success",
				'payment_method'=>$AOrderMethod[$payment_type],
				'dd_address'=>"Please send the Demand Draft/Cheque in favor of 'Radox Trading & Marketing Pvt Ltd.' payable at 'Gurgaon to the following address' Plot no - 545, S.Lal Tower, Sector - 20, Dundahera , Gurgaon, Haryana PIN: 122016 (India)"
			);
		}

		$das = json_encode($data);
		
		print $das;
		$log_id = appLogPhoneGap($api_url, "Response",$log_id);
	}	
?>
