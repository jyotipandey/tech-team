<?php
//$baseURL='E:/xampp/htdocs/dogspot_new';
$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
//require_once($baseURL.'/session.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');
$api_url = "getProductListByFilter.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);
/*
Request: 

{"access_token":"YnJhamVuZHJhIyMyOTI0Mg==","min_price":"10","max_price":"1000",”cat_id”=”1”,”brand_id”=”1”}

Response: {"product_data":[{"product_id":"9210","product_name":"Product1","product_image"="image1.jpg","retail_price"="42.00","currency"="INR","sale_price"="39.00","description"="This is a new product"},{ "product_id":"8010","product_name":"Product10", "product_image"= 
"image10.jpg","retail_price"="36.50","currency"="INR","sale_price"="29.00","description"="This is a product for testing" }]}
*/

$breed_type_arr = array(
	'upto 10kg'=>"2",
	'10-25kg'=>"3",
	'25-45kg'=>"5",
	'above 45kg'=>"6",
	'For all type'=>"7"
);
$life_stategs_arr = array(
	'upto 2 Months'=>"1",
	'2-24 Months'=>"2",
	'2-7 Years'=>"5",
	'More than 7 Years'=>"6"
);

$num_rec_per_page=10;
if (isset($page)) { 
	$page  = $page; 
}else { 
	$page=1; 
} 
$start_from = ($page-1) * $num_rec_per_page;

if($sort=='1'){
	$st='&sort=price asc';	
}elseif($sort=='2'){
	$st='&sort=price desc';			
}else{
	$st='&sort=item_display_order ASC';
}
if($cat_id!="0"){
	if($cat_id=="15"){
		if($attribute_value){
			//$attribute_value = str_replace("undefined", "", $attribute_value);
			$attribute_value = trim($attribute_value,'|');
			$sbreedt = explode('|',$attribute_value);
			if($sbreedt[0]){
				foreach($sbreedt as $sbrt1){
					if(in_array($sbrt1,$breed_type_arr)){
						
					}else{
						
					}
				}
			}
		}
		if($breedname){
			$breedname = str_replace("undefined", "", $breedname);
			$breedname=trim($breedname,'-');
			$sbreeds=explode('-',$breedname);
			if($sbreeds[0]){
			foreach($sbreeds as $sbre){
				$breedsql.=" OR j.breed_id='$sbre' ";
			$printbreed_name[]=$sbre;
			}
			$breedsql = trim($breedsql,' OR ');
			$breedsql = " AND ($breedsql)";
			}
		}
		
		if($type_breed11){
			$type_breed11 = str_replace("undefined", "", $type_breed11);
			$type_breed11=trim($type_breed11,'-');
			$sbreedt=explode('-',$type_breed11);
			if($sbreedt[0]){
			foreach($sbreedt as $sbrt){
				$breedtsql.=" OR j.breed_type='$sbrt' ";
				$printbreed_type[]=$sbrt;
			}
			$breedtsql = trim($breedtsql,' OR ');
			$breedtsql = " AND ($breedtsql)";
			}
		}
		
		if($type_life){ //undefinedmature-
			$type_life = str_replace("undefined", "", $type_life);
			$type_life=trim($type_life,'-');
			$sbreedlife=explode('-',$type_life);
			if($sbreedlife[0]){
				foreach($sbreedlife as $sblife){
					$breedlsql.=" OR j.life_stage='$sblife' ";
					$printbreed_life[]=$sblife;
				}
				$breedlsql = trim($breedlsql,' OR ');
				$breedlsql = " AND ($breedlsql)";
			}
		}
			
	}else{
		if($attribute_value){
			$attribute_value = str_replace("undefined", "", $attribute_value);
			$attribute_value = trim($attribute_value,'|');
			$sbreedt = explode('|',$attribute_value);
			if($sbreedt[0]){
				foreach($sbreedt as $sbrt1){
					$sbreedtype = explode('-',$sbrt1);
					$keyss[] = $sbreedtype[0];
					$sbreedt12[] = $sbrt1;
				}
				$keyss = array_unique($keyss);
				$breedtsql = '';
				if($sbreedt12[0]){
					foreach($keyss as $key1){
						foreach($sbreedt12 as $sbrt){
							$sbreedtypeT = explode('-',$sbrt);	
							$sbrt = str_replace('-','A',$sbrt);
							if($key1 == $sbreedtypeT[0]){
								if($c==1){
									$breedtsql.= " item_attribute:$sbrt";
									$c=0;
								}else{
									$breedtsql.= " OR item_attribute:$sbrt";}
								$printattrib_value[]=$sbrt;
							}
						}
						$c=1;
						$breedtsql= trim($breedtsql,' OR ');
						$breedtsql= "($breedtsql) AND (";
					}
					$breedtsql= trim($breedtsql,' AND ()');
					if($c==1){
						$breedtsql= " AND ($breedtsql)";
					}else{
						$breedtsql= " AND ($breedtsql))";
					}
				}
			}//(((item_attribute:1A1) AND OR item_attribute:2A8))
		}		
	}
	
	if($brand_id){
		$brand_id = str_replace("undefined", "", $brand_id);
		$brand_id=trim($brand_id,'-');
		$sbrands=explode('-',$brand_id);
		if($sbrands[0]){
			foreach($sbrands as $sb){
				$bsql.=" OR item_brand:$sb ";$printbrand_name[]=$bsql;
			}
			$bsql = trim($bsql,' OR ');
			$bsql = "&fq=($bsql)";
		}
	}
	if($min_price && $max_price){
		$sqlpp=" AND price:[$min_price TO $max_price]";
	}
	if($s_weight){
		$s_weight = str_replace("Kg", "", $s_weight);
		$s_weight = str_replace("-", "|", $s_weight);
		$s_weight=trim($s_weight,'|');
		$s_weight1=explode('|',$s_weight);
		$wt1=count($s_weight1);
		if($wt1>1){
			//$wsql1= " AND weight BETWEEN '".$s_weight1[0]."' AND '".$s_weight1[1]."'";
			$wsql1=" AND weight:[$s_weight1[0] TO $s_weight1[1]]";
		}
	}
	if($s_weightprice){
		$s_weightprice = str_replace("Rs", "", $s_weightprice);
		$s_weightprice = str_replace("-", "|", $s_weightprice);
		$s_weightprice=trim($s_weightprice,'|');
		$s_weightprice1=explode('|',$s_weightprice);
		$wtp1=count($s_weightprice1);
		if($wtp1>1){
			//$wsps1= " AND weight_price BETWEEN '".$s_weightprice1[0]."' AND '".$s_weightprice1[1]."'";
			$wsps1=" AND weight_price:[$s_weightprice1[0] TO $s_weightprice1[1]]";
		}
	}

	$salinstock="item_category_id:\"$cat_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND NOT item_id:805 AND NOT item_id:1283 AND stock_status:instock$breedtsql$bsql$bssql$bnsql$sqlpp$btsql$bcatsql$wsps1$wsps1$wsql1$sal$st&version=2.2&start=$start_from&rows=$num_rec_per_page&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
	$url = "http://101.53.137.39/solr/dogspotshopsolr/select/?q=$salinstock";
	//echo $url;

	$url = str_replace(" ","%20",$url);
	$resultsolr = get_solr_result($url);
	$totrecord = $resultsolr['TOTALHITS'];

	foreach($resultsolr['HITS'] as $rowItemall){
		$Aitem_Itemid[]=$rowItemall["item_id"];
		$Aitem_brand[]=$rowItemall["item_brand"];
		$Aitem_price[]=$rowItemall["price"];
		$Aitem_attribute[]=$rowItemall["item_attribute"];
		$Aitem_life_stage[]=$rowItemall["item_life_stage"];
		$Aitem_breed_id[]=$rowItemall["item_breed_id"];


		$Aitem_breed_type[]=$rowItemall["item_breed_type"];
		$Aitem_category[]=$rowItemall["item_category_id"];//add code
		$Aitem_weight[]=$rowItemall["weight"];
		$A1weight_price[]=round($rowItemall["price"]/$rowItemall["weight"],3);
	}

	if($totrecord>0){
		$Acount=array_unique($Aitem_Itemid);

		foreach($resultsolr['HITS'] as $rowItemP11){
			//while($rowItemP11 = mysql_fetch_array($qItem)){
			//echo $rowItemP11['item_id'].";";
			$Aitem_ItemidP3[]=$rowItemP11['item_id'];
		}
		$unique = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
		//$totrecord=$resultsolr['TOTALHITS'];
		//echo "dfdsgsdgsd".$totrecord22=count($unique);

		foreach($unique as $rowItemitem ){
			$rowItem=query_execute_row("SELECT tag,item_id,name,nice_name,description ,selling_price,item_parent_id,price,type_id,weight_price,item_brand,stock_status,created_at,weight,item_display_order from shop_items WHERE item_id='$rowItemitem'");
			$item_id=$rowItem["item_id"];

			$tag=$rowItem["tag"];
			$name=stripslashes($rowItem["name"]);
			$name=stripslashes($name);
			$nice_name=$rowItem["nice_name"];
			$type_id=$rowItem["type_id"];
			$price=number_format($rowItem["price"]);
			$selling_price=number_format($rowItem["selling_price"]);
			$item_parent_id=$rowItem["item_parent_id"];
			$stock_status=$rowItem["stock_status"];
			$brandd=$rowItem["item_brand"];
			$description=$rowItem["description"];
			if($item_parent_id == '0'){
				$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
			}else{
				$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
			}
			$rowdatM = mysql_fetch_array($qdataM);

			if($rowdatM["media_file"]){
				$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
				$imageURL='/imgthumb/150x160-'.$rowdatM["media_file"];
			}else{
				$src = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
				$imageURL='/shop/image/no-photo-t.jpg';
			}

			//get today's savings
			if($rowItem["selling_price"] > $rowItem["price"]){
				$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
				$item_discount_per=number_format($item_discount_per1,2);
			}else{
				$item_discount_per="0";
			}
			//end
			$data=array(
				'product_id' =>$item_id,
				'product_name' =>$name,
				'product_image' =>$rowdatM["media_file"],
				'retail_price' =>$price,
				'currency' =>'INR',
				'sale_price' =>$selling_price,
				'description' =>$description,
				'saving_percentage'=>$item_discount_per
			);
			$datat[]=$data;
		}
	}else{
		$datat=array("status"=>'Not Found');
		$return = app_logs($api_url, "Response", $log_id);
	}
}else{
	// Food filter only
	
	if($item_brand){
		$item_brand = str_replace("undefined", "", $item_brand);
		$item_brand=trim($item_brand,'-');
		$sbrands=explode('-',$item_brand);
		if($sbrands[0]){
			foreach($sbrands as $sb){
				$bsql.=" OR i.item_brand=$sb ";
				$printbrand_name[]=$sb;	
			}
			$bsql = trim($bsql,' OR ');
			$bsql = " AND ($bsql)";
		}
	}

	if($breedname){
		$breedname = str_replace("undefined", "", $breedname);
		$breedname=trim($breedname,'-');
		$sbreeds=explode('-',$breedname);
		if($sbreeds[0]){
			foreach($sbreeds as $sbre){
				$breedsql.=" OR j.breed_id='$sbre' ";
				$printbreed_name[]=$sbre;
			}
			$breedsql = trim($breedsql,' OR ');
			$breedsql = " AND ($breedsql)";
		}
	}

	if($type_breed11){
		$type_breed11 = str_replace("undefined", "", $type_breed11);
		$type_breed11=trim($type_breed11,'-');
		$sbreedt=explode('-',$type_breed11);
		if($sbreedt[0]){
			foreach($sbreedt as $sbrt){
				$breedtsql.=" OR j.breed_type='$sbrt' ";
				$printbreed_type[]=$sbrt;
			}
			$breedtsql = trim($breedtsql,' OR ');
			$breedtsql = " AND ($breedtsql)";
		}
	}

	if($type_life){ //undefinedmature-
		$type_life = str_replace("undefined", "", $type_life);
		$type_life=trim($type_life,'-');
		$sbreedlife=explode('-',$type_life);
		if($sbreedlife[0]){
			foreach($sbreedlife as $sblife){
				$breedlsql.=" OR j.life_stage='$sblife' ";
				$printbreed_life[]=$sblife;
			}
			$breedlsql = trim($breedlsql,' OR ');
			$breedlsql = " AND ($breedlsql)";
		}
		
	}
	if($min_price && $max_price){
		$psql= " AND i.price BETWEEN '$min_price' AND '$max_price";
	}

//	if($s_price1){
//		$s_price1 = str_replace("Rs", "", $s_price1);
//		$s_price1 = str_replace("-", "|", $s_price1);
//		$s_price1=trim($s_price1,'|');
//		$sprice1=explode('|',$s_price1);
//		$pc1=count($sprice1);
//		if($pc1>1){
//			$psql1= " AND i.price BETWEEN '".$sprice1[0]."' AND '".$sprice1[1]."'";
//		}
//	}
//
//	if($s_weight){
//		$s_weight = str_replace("Kg", "", $s_weight);
//		$s_weight = str_replace("-", "|", $s_weight);
//		$s_weight=trim($s_weight,'|');
//		$s_weight1=explode('|',$s_weight);
//		$wt1=count($s_weight1);
//		if($wt1>1){
//			$wsql1= " AND i.weight BETWEEN '".$s_weight1[0]."' AND '".$s_weight1[1]."'";
//		}
//	}
//
//	if($s_weightprice){
//		$s_weightprice = str_replace("Rs", "", $s_weightprice);
//		$s_weightprice = str_replace("-", "|", $s_weightprice);
//		$s_weightprice=trim($s_weightprice,'|');
//		$s_weightprice1=explode('|',$s_weightprice);
//		$wtp1=count($s_weightprice1);
//		if($wtp1>1){
//			$wsps1= " AND i.weight_price BETWEEN '".$s_weightprice1[0]."' AND '".$s_weightprice1[1]."'";
//		}
//	}
}

echo json_encode($datat);
$return = app_logs($api_url, "Response", $log_id);
?>