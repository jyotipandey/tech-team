<?php
	/*
	*	Update Items Qty in Cart 
	*	Created By Umesh On dated: 16/3/2015 
	*/	
	require_once($DOCUMENT_ROOT.'/constants.php');
	require_once($DOCUMENT_ROOT.'/session.php');
	require_once($DOCUMENT_ROOT.'/database.php');
	require_once($DOCUMENT_ROOT.'/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
	require_once($DOCUMENT_ROOT.'/arrays.php');
	require_once($DOCUMENT_ROOT.'/mobile-webservice/android/functions.php');
	$api_url = "updateCartItemQty.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);
	if($access_token){
		$getUsereId=base64_decode($access_token);	
		$vardata = explode('##',$getUsereId);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
	}
	else{
		$user_id = "Guest";	
	}
	if($user_id!=''){
		if($mode='updateQty' && $cart_id && $cart_item_id && $cart_item_qty && $session_id){
			
			$qBuySel=query_execute_row("SELECT name,price, selling_price,virtual_qty,item_status,item_shipping_amount FROM shop_items WHERE item_id = '$cart_item_id'");
			
			$selling_price=$qBuySel['selling_price'];
			
			$price=$qBuySel['price'];
			
			$item_status1=$qBuySel['item_status'];
			
			//$item_shipping_amount=$qBuySel['item_shipping_amount'];
			
			if($item_status1=='hard'){
				$virtual_qty=$qBuySel['virtual_qty'];
				if($cart_item_qty<=$virtual_qty){
					$status=1;
				}else{
					$cart_item_qty=$virtual_qty;
					$status=0;
				}
			}
			if($selling_price>$price){
				$saledisc=(($selling_price*$cart_item_qty)-($price*$cart_item_qty));
			}
			else{
				$saledisc='0';
			}
			if($cart_item_qty>='1'){
				if($user_id == 'Guest'){
					//$qGetcart = query_execute("SELECT cart_id,item_qty FROM shop_cart WHERE session_id = '$session_id' AND item_id = '$itemReal' AND cart_order_status='new'");
					$qGetMyCart13 = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND sc.donate_bag=0 AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc.item_id='1283')");
				}else{
		            
					// $qGetcart = query_execute("SELECT cart_id,item_qty FROM shop_cart WHERE userid = '$user_id' AND item_id = '$itemReal' AND cart_order_status='new'");
					$qGetMyCart13 = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$user_id' AND sc.cart_order_status = 'new' AND sc.donate_bag=0 AND si.stock_status = 'instock' AND si.item_display_status != 'delete' AND ( sc.item_price != '0' OR sc.item_id = '1283' )");
				}
				$rowCart23 = mysql_fetch_array($qGetMyCart13);
				$cart_item_amount = $rowCart23["order_amount"];
				if(($cart_item_amount+($price*$cart_item_qty))<=20000000){
					$select=query_execute_row("SELECT item_qty FROM shop_cart WHERE cart_id='$cart_id' AND (item_id='2838' OR item_id='2747' OR item_id='539') ");
					
					$sql = "UPDATE shop_cart SET item_qty='$cart_item_qty', item_discount=$saledisc, item_totalprice=item_price*item_qty WHERE cart_id = '$cart_id' AND item_id='$cart_item_id'";
						
					$qGetcart = query_execute($sql);
				
					$no_updated = mysql_affected_rows();
//					if($order_id!='' && $order_id!="0"){
//						$qCart=query_execute_row("SELECT SUM(item_totalprice) as order_item_amount FROM shop_cart WHERE cart_order_id= '$order_id'");
//						$totalpr=$qCart['order_item_amount'];
//						$qUpaOrder=query_execute("UPDATE shop_order SET order_items_amount='$totalpr' WHERE order_id = '$order_id'");
//						$TotalAmount = query_execute_row("SELECT order_shipment_amount FROM shop_order WHERE order_id = '$order_id'");
//					}
				}
				$data=array(
					'status' =>"Item Qty Updated Successfully",
					'query' =>"$sql"
				);
			}
		}
		else{
			$data=array(
				'status' =>"some parameter are missing"
			);
		}
	$das=json_encode($data);
	print $das;
	$return = appLogPhoneGap($api_url, "Response", $log_id);
}
?>