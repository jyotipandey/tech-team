<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	//require_once($baseURL.'/session.php');
	
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "nativeGetAddressList.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);		
	
	if($access_token){
		$getUserid=base64_decode($access_token);	
		$vardata = explode('##',$getUserid);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		//get token and get userid from this token using algo...
//		echo $user_id;
//		die;
		
		$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
		if($check_userid[cc] > 0){
			// Add New Address
			if($mode=="addAddress"){
				if($user_id != 'Guest'){
					$guest_email = useremail($user_id);
				}
				if($address_name && $address_zip && $mobile_number && $address && $city && $state && $guest_email){
					$SQ_city = query_execute_row("SELECT * FROM city WHERE city_name='$city'");
					$city_id = $SQ_city[city_id];
					$city_nicename = $SQ_city[city_nicename];
					$SQ_state = query_execute_row("SELECT * FROM state WHERE state_name='$state'");
					$state_id = $SQ_state[state_id];
					$state_nicename = $SQ_state[state_nicename];
					if($address_zip){
						$address_address = stripslashes($address);
						if($address_address){
							$resultaddress = query_execute_row("SELECT * FROM shop_order_more_address WHERE userid='$user_id' AND address_zip = '$address_zip' AND address_address1 = '".addslashes($address_address)."'");
							if($resultaddress['more_address_id']==''){
								$address_name = addslashes($address_name);
								$address_city = addslashes($city);				
								$resultinsert34 = query_execute("INSERT INTO shop_order_more_address (userid, address_name, address_email, address_address1, address_address2, address_city, address_city_id, address_city_nicename, address_state, address_state_id, address_state_nicename, address_zip, address_phone1) VALUES('$user_id','".mysql_real_escape_string($address_name)."', '$guest_email', '".mysql_real_escape_string($address)."', '', '$city', '$city_id','$city_nicename', '$state', '$state_id', '$state_nicename', '$address_zip', '$mobile_number')");
								$more_address_id = mysql_insert_id();
								$data = array(
									'status'=>"Address Added Successfully",
									'address_id'=>$more_address_id
								);
							}else{
								$data = array(
									'status'=>"Address Already Exists",
									'address_id'=>$resultaddress['more_address_id']
								);
							}
						}
					}
				}
			}
			
			// show address list 
			if($mode=="showList"){
				$getaddress = mysql_query("SELECT * FROM shop_order_more_address WHERE userid='$user_id' AND address_name!='' AND address_address1 != ''  AND address_city != '' AND address_state != 'Select State' AND address_state != '' AND address_phone1 != '' ORDER BY more_address_id");

				$count = mysql_num_rows($getaddress);
				if($count>0){  
					$data='';
					while($ship_add = mysql_fetch_array($getaddress)){ 
						$address = array(
							'address_id' => $ship_add['more_address_id'],
							'name' => $ship_add['address_name'],
							'address_name1' => $ship_add['address_address1'],
							'address_name2' => $ship_add['address_address2'],
							'address_city' => $ship_add['address_city'],
							'address_state' => $ship_add['address_city'],
							'address_state' => $ship_add['address_state'],
							'address_zip' => $ship_add['address_zip'],
							'address_phone1' => $ship_add['address_phone1']
						
						);
						$dataq[$ship_add['more_address_id']] = $address;
					}
					$data['address']= $dataq;
					$data['total_address']= $count;
				}
				else{
					$data=array(
						'status' =>"No Address Found"
					);
				}  
			} 
			
			// Edit address and save 
			if($mode == "updateAddress" && $address_id){
				//echo $address_id;
				if($address_name && $address_address1 && $address_city && $address_state && $address_zip && $address_phone1){
					$SQ_city = query_execute_row("SELECT * FROM city WHERE city_name='$address_city'");
					$address_city_id = $SQ_city['city_id'];
					$address_city_nicename = $SQ_city['city_nicename'];
					
					$SQ_state = query_execute_row("SELECT * FROM state WHERE state_name='$address_state'");
					$address_state_id = $SQ_state['state_id'];
					$address_state_nicename = $SQ_state['state_nicename'];	
					
					
					$query = "UPDATE shop_order_more_address SET address_name='$address_name', address_address1='$address_address1', address_city='$address_city', address_city_id='$address_city_id', address_city_nicename='$address_city_nicename', address_state='$address_state', address_state_id='$address_state_id', address_state_nicename='$address_state_nicename', address_zip='$address_zip', address_phone1='$address_phone1'";
					
					if($address_address2){
						$query .= ", address_address2='$address_address2'";
					}
					$query .=" WHERE more_address_id='$address_id' AND userid='$user_id'";
					//echo $query;
					
					$update_address = mysql_query($query);
					$result = mysql_affected_rows();
					if($result){
						$data=array(
							'status' =>"Address has been updated successfully",
							'address_id' => $address_id
						);
					}else{
						$data=array(
							'status' =>"Not Update",
						);
					}
				}else{
					$data=array(
							'status' =>"other parameters should not be blank",
							'address_id' => $address_id
					);
				}
			}
			
			// Delete Address
			if($mode == "deleteAddress" && $address_id){
				$query = "DELETE FROM shop_order_more_address WHERE more_address_id='$address_id' AND userid='$user_id' LIMIT 1";
				
				$delete = mysql_query($query);
				$result = mysql_affected_rows();
				if($result){
					$data=array(
						'status' =>"Deleted successfully",
						'address_id' => $address_id
					);
				}else{
					$data=array(
						'status' =>"Not Deleted",
					);
				}
				
			} 
			if($mode==""){
				$data=array(
					'status' =>"mode parameter should not be blank"
				);
			}
		}else{
			$data=array(
				'status' =>"Not a valid User"
			);	
		}
	}else{
		$data=array(
			'status' =>"Not a valid User Authentication"
		);
	}
	$das=json_encode($data);
	echo $das;
	$log_id = appLogPhoneGap($api_url, "Response",$log_id);
?>