<?php
/**
* 
* @var close_status=0 is for incomplete order status
* @var close_status=1 is for success and failed order status
* 
*/

	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/session.php');
	require_once($baseURL.'/shop/arrays/arrays.php');

	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "checkPaymentStatus.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);

if($access_token){
	$getUserId=base64_decode($access_token);
	$pos = strpos($getUserId, '##');
	if($pos != false){	
		$vardata = explode('##',$getUserId);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		//get token and get userid from this token using algo...

		$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
		if($check_userid[cc]>0){
			//echo "SELECT * FROM shop_order WHERE userid='$user_id' AND order_id='$order_id'";
			
			$check_order = query_execute_row("SELECT * FROM shop_order WHERE userid='$user_id' AND order_id='$order_id'");
		
			if($check_order['order_id']!=""){
				$order_method = $check_order['order_method'];
				$order_id = $check_order['order_id'];
				$data='';
				//echo $check_order['order_status'];
				if($check_order['order_status']=="inc" || $check_order['order_status']==""){
					$close_status = "0";
					$status = "Incomplete";
				}
				elseif($check_order['order_status']=="0" || $check_order['order_status']=="1"){
					if($order_method=="DC" || $order_method=="CC"||$order_method=="dc"||$order_method=="cc"||$order_method=="ccnb"||$order_method=="nb"||$order_method=="CCNB"||$order_method=="NB"){
						if($check_order['order_transaction_amount']!="0.0000" && $check_order['order_transaction_id']!="" && $check_order['order_status']=="0"){
							$status = "Success";
							$close_status = "1";			
						}else{
							$status = "Failed";
							$close_status = "1";
						}	
					}
				}
				$data_order=array(                     
					'status'=>$status,
					'order_id' => $order_id,
					'order_date' => $check_order['order_c_date'],
					'order_amount' => number_format($check_order['order_amount'],2),
					'payment_method'=>$AOrderMethod[$order_method],
					'close_status'=>$close_status
				);
				$data[order_data]=$data_order;
			}	
		}
		else{
			$data=array(
				'status' =>"Not a valid User"
			);	
		}
	}
	else{
		$data=array(
			'status' =>"Access Token Not Valid!"
		);
	}
}
else{
	$data=array(
		'status' =>"Not a valid User Authentication"
	);
}
$das=json_encode($data);
echo $das;
$return = appLogPhoneGap($api_url, "Response", $log_id);
?>