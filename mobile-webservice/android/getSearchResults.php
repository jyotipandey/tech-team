<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	//require_once($baseURL.'/session.php');

	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "getSearchResults.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);
	
	$ecomm_prodid = "";
	$ecomm_pagetype = 'searchresults';
	$ecomm_totalvalue = "";
	$google_conversion_label="hWRsCPrz2gIQvoe25QM";
	
	//$keyword="Dog Crate";
	$q=$keyword;								// request param shopquery
	$q=str_replace("?filter=filter", "", $q);
	$query=escapeSolrValueNew(trim($q), '');
	
	//paging
	$num_rec_per_page=10;
	if (isset($page)) { 
		$page  = $page; 
	}else { 
		$page=1; 
	}
	$start_from = ($page-1) * $num_rec_per_page;
	//end paging
	
	$query=urlencode($query);
	if($query){
		$url = "http://101.53.137.39/solr/dogspotshopsolr/select?q=$query&wt=xml&mm=1&indent=true&defType=dismax&qf=title^2+keyword_r1^6+keyword_r2^4+keyword_r3^3+text^1&fq=visibility:visible&fq=type_id:simple&fq=-domain_id:2&fq=-item_display_status:delete&fq=stock_status:instock&start=$start_from&rows=$num_rec_per_page";
		$url = str_replace(" ","",$url);
		$result = get_solr_result($url);
		$totrecord = $result['TOTALHITS'];
		
		if($totrecord=='0'){
			$url = "http://101.53.137.39/solr/dogspotshopsolr/select?q=$query&wt=xml&mm=1&indent=true&defType=dismax&qf=title%5E5+category_name%5E3+brand_name%5E2+text%5E1&df=item_about&fq=visibility:visible&fq=type_id:simple&fq=domain_id:3&fq=-item_display_status:delete&fq=stock_status:instock&start=$start_from&rows=$num_rec_per_page";
			$result = get_solr_result($url);
			$totrecord = $result['TOTALHITS'];
		}
		
		if($totrecord!='0'){			
			foreach($result['HITS'] as $rowItem){
				$item_id=$rowItem["item_id"];			
				
				// item_id returns in decimal number format like 1451.0
				// convert into number format like 1,451
				$item_id = number_format($item_id,0);
				
				// replace number 1,000 into number format like 1451
				$item_id = str_replace( ',', '', $item_id );
				
				$name=$rowItem["name"];
				$nice_name=$rowItem["nice_name"];
				$created_at=$rowItem["created_at"];
				$creat=explode("T", $created_at);
				$end_date=date("Y-m-d ");
				$thirtydays = date_create('30 days ago');
				$startdate=date_format($thirtydays, 'Y-m-d');
				$nice_name=$rowItem["nice_name"];
				$price=$rowItem["price"];
				$description=$rowItem["description"];
				$selling_price=$rowItem["selling_price"];
				$item_parent_id=$rowItem["item_parent_id"];
				$stock_status=$rowItem["stock_status"];
				
				if($item_id){
					$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
				}
				if(mysql_num_rows($qdataM) < 1){
					$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
				}
				$rowdatM = mysql_fetch_array($qdataM);
				
				if($rowItem["selling_price"] > $rowItem["price"]){
					$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
					$item_discount_per=number_format($item_discount_per1,0);
				}else{
					$item_discount_per = "0";				
				}	
				if($rowdatM["media_file"]){
					$imglink='/imgthumb/150x160-'.$rowdatM["media_file"];
					$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
				}else{
					$imglink='/imgthumb/150x160-no-photo.jpg';
					$src = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
					$rowdatM["media_file"] = "no-photo.jpg";
				}
				$dest = $DOCUMENT_ROOT.$imglink;
				createImgThumbIfnot($src,$dest,'150','160','ratiowh');	
							
				$data=array(
					'product_id' =>$item_id,
					'product_name' =>$name,
					'product_nicename' =>$nice_name,
					'product_image' =>$rowdatM["media_file"],
					'retail_price' =>number_format($price,0),
					'currency' =>'INR',
					'sale_price' =>number_format($selling_price,0),
					'description' =>$description,
					'saving_percentage'=>$item_discount_per
				);
				$datat[]=$data; 
			}
			$maxpages = ceil($totrecord/$num_rec_per_page);
			if($page < $maxpages){
				$next = $page+1;
			}else{
				$next = 0;	
			}
			if(count($datat)>0){
				$datat = array(
					'status'=>"success",
					'product_data' => $datat,
					'msg'=>"Record found",
					'next'=>$next
				);	
			}else{
				$datat = array(
					'status'=>"fail",
					'product_data' => "",
					'msg'=>"Record not found",
					'next'=>$next
				);
			}
			
		}else{
			$datat = array(
				'status'=>"fail",
				'product_data' => "",
				'msg'=>"Record not found",
				'next'=>$next
			);
		}
	}else{
		$datat = array(
			'status'=>"fali",
			'product_data' => "",
			'msg'=>"Record not found",
			'next'=>$next
		);
	}
	$das=json_encode($datat);
	echo $das;
	$return = appLogPhoneGap($api_url, "Response", $log_id);
?>