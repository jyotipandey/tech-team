<?php
//$baseURL='E:/xampp/htdocs/dogspot_new';
$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
//require_once($baseURL.'/session.php');
require_once($baseURL.'/mobile-webservice/android/functions.php');
$api_url = "getProductListByFilter.php"; 
$log_id = "";
$log_id = app_logs($api_url, "Request",$log_id);
/*
Request: 

{"access_token":"YnJhamVuZHJhIyMyOTI0Mg==","min_price":"10","max_price":"1000",”cat_id”=”1”,”brand_id”=”1”}

Response: {"product_data":[{"product_id":"9210","product_name":"Product1","product_image"="image1.jpg","retail_price"="42.00","currency"="INR","sale_price"="39.00","description"="This is a new product"},{ "product_id":"8010","product_name":"Product10", "product_image"= 
"image10.jpg","retail_price"="36.50","currency"="INR","sale_price"="29.00","description"="This is a product for testing" }]}
*/

$breed_type_arr = array(
	'upto 10kg'=>"2",
	'10-25kg'=>"3",
	'25-45kg'=>"5",
	'above 45kg'=>"6",
	'For all type'=>"7"
);
$life_stategs_arr = array(
	'upto 2 Months'=>"1",
	'2-24 Months'=>"2",
	'2-7 Years'=>"5",
	'More than 7 Years'=>"6"
);

$num_rec_per_page=10;
if (isset($page)) { 
	$page  = $page; 
}else { 
	$page=1; 
} 
$start_from = ($page-1) * $num_rec_per_page;

if($sort=='1'){
	$st='&sort=price asc';	
}elseif($sort=='2'){
	$st='&sort=price desc';			
}else{
	$st='&sort=item_display_order ASC';
}

if($cat_id!=15){
	if($attribute_value){
		$attribute_value = str_replace("undefined", "", $attribute_value);
		$attribute_value = trim($attribute_value,'|');
		$sbreedt = explode('|',$attribute_value);
		if($sbreedt[0]){
			foreach($sbreedt as $sbrt1){
				$sbreedtype = explode('-',$sbrt1);
				$keyss[] = $sbreedtype[0];
				$sbreedt12[] = $sbrt1;
			}
			$keyss = array_unique($keyss);
			$breedtsql = '';
			if($sbreedt12[0]){
				foreach($keyss as $key1){
					foreach($sbreedt12 as $sbrt){
						$sbreedtypeT = explode('-',$sbrt);	
						$sbrt = str_replace('-','A',$sbrt);
						if($key1 == $sbreedtypeT[0]){
							if($c==1){
								$breedtsql.= " item_attribute:$sbrt";
								$c=0;
							}else{
								$breedtsql.= " OR item_attribute:$sbrt";}
							$printattrib_value[]=$sbrt;
						}
					}
					$c=1;
					$breedtsql= trim($breedtsql,' OR ');
					$breedtsql= "($breedtsql) AND (";
				}
				$breedtsql= trim($breedtsql,' AND ()');
				if($c==1){
					$breedtsql= " AND ($breedtsql)";
				}else{
					$breedtsql= " AND ($breedtsql))";
				}
			}
		}//(((item_attribute:1A1) AND OR item_attribute:2A8))
		
	}		
	
	if($breedname){
		$breedname = str_replace("undefined", "", $breedname);
		$breedname=trim($breedname,'-');
		$sbreeds=explode('-',$breedname);
		if($sbreeds[0]){
		foreach($sbreeds as $sbre){
			$breedsql.=" OR j.breed_id='$sbre' ";
		$printbreed_name[]=$sbre;
		}
		$breedsql = trim($breedsql,' OR ');
		$breedsql = " AND ($breedsql)";
		}
	}
	
	if($type_breed11){
		$type_breed11 = str_replace("undefined", "", $type_breed11);
		$type_breed11=trim($type_breed11,'-');
		$sbreedt=explode('-',$type_breed11);
		if($sbreedt[0]){
		foreach($sbreedt as $sbrt){
			$breedtsql.=" OR j.breed_type='$sbrt' ";
			$printbreed_type[]=$sbrt;
		}
		$breedtsql = trim($breedtsql,' OR ');
		$breedtsql = " AND ($breedtsql)";
		}
	}
	
	if($type_life){ //undefinedmature-
		$type_life = str_replace("undefined", "", $type_life);
		$type_life=trim($type_life,'-');
		$sbreedlife=explode('-',$type_life);
		if($sbreedlife[0]){
			foreach($sbreedlife as $sblife){
				$breedlsql.=" OR j.life_stage='$sblife' ";
				$printbreed_life[]=$sblife;
			}
			$breedlsql = trim($breedlsql,' OR ');
			$breedlsql = " AND ($breedlsql)";
		}
	}
	
	if($brand_id){
		$brand_id = str_replace("undefined", "", $brand_id);
		$brand_id=trim($brand_id,'-');
		$sbrands=explode('-',$brand_id);
		if($sbrands[0]){
			foreach($sbrands as $sb){
				$bsql.=" OR item_brand:$sb ";
				$printbrand_name[]=$bsql;
			}
			$bsql = trim($bsql,' OR ');
			$bsql = "&fq=($bsql)";
		}
	}
	if($min_price && $max_price){
		$sqlpp=" AND price:[$min_price TO $max_price]";
	}
	if($s_weight){
		$s_weight = str_replace("Kg", "", $s_weight);
		$s_weight = str_replace("-", "|", $s_weight);
		$s_weight=trim($s_weight,'|');
		$s_weight1=explode('|',$s_weight);
		$wt1=count($s_weight1);
		if($wt1>1){
			//$wsql1= " AND weight BETWEEN '".$s_weight1[0]."' AND '".$s_weight1[1]."'";
			$wsql1=" AND weight:[$s_weight1[0] TO $s_weight1[1]]";
		}
	}
	
	if($s_weightprice){
		$s_weightprice = str_replace("Rs", "", $s_weightprice);
		$s_weightprice = str_replace("-", "|", $s_weightprice);
		$s_weightprice=trim($s_weightprice,'|');
		$s_weightprice1=explode('|',$s_weightprice);
		$wtp1=count($s_weightprice1);
		if($wtp1>1){
			//$wsps1= " AND weight_price BETWEEN '".$s_weightprice1[0]."' AND '".$s_weightprice1[1]."'";
			$wsps1=" AND weight_price:[$s_weightprice1[0] TO $s_weightprice1[1]]";
		}
	}

	$salinstock="item_category_id:\"$cat_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND NOT item_id:805 AND NOT item_id:1283 AND stock_status:instock$breedtsql$bsql$bssql$bnsql$sqlpp$btsql$bcatsql$wsps1$wsps1$wsql1$sal$st&version=2.2&start=$start_from&rows=$num_rec_per_page&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
	$url = "http://101.53.137.39/solr/dogspotshopsolr/select/?q=$salinstock";
	//echo $url;

	$url = str_replace(" ","%20",$url);
	$resultsolr = get_solr_result($url);
	$totrecord = $resultsolr['TOTALHITS'];

	foreach($resultsolr['HITS'] as $rowItemall){
		$Aitem_Itemid[]=$rowItemall["item_id"];
		$Aitem_brand[]=$rowItemall["item_brand"];
		$Aitem_price[]=$rowItemall["price"];
		$Aitem_attribute[]=$rowItemall["item_attribute"];
		$Aitem_life_stage[]=$rowItemall["item_life_stage"];
		$Aitem_breed_id[]=$rowItemall["item_breed_id"];


		$Aitem_breed_type[]=$rowItemall["item_breed_type"];
		$Aitem_category[]=$rowItemall["item_category_id"];//add code
		$Aitem_weight[]=$rowItemall["weight"];
		$A1weight_price[]=round($rowItemall["price"]/$rowItemall["weight"],3);
	}

	if($totrecord>0){
		$Acount=array_unique($Aitem_Itemid);

		foreach($resultsolr['HITS'] as $rowItemP11){
			//while($rowItemP11 = mysql_fetch_array($qItem)){
			//echo $rowItemP11['item_id'].";";
			$Aitem_ItemidP3[]=$rowItemP11['item_id'];
		}
		$unique = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
		//$totrecord=$resultsolr['TOTALHITS'];
		//echo "dfdsgsdgsd".$totrecord22=count($unique);

		foreach($unique as $rowItemitem ){
			$rowItem=query_execute_row("SELECT tag,item_id,name,nice_name,description ,selling_price,item_parent_id,price,type_id,weight_price,item_brand,stock_status,created_at,weight,item_display_order from shop_items WHERE item_id='$rowItemitem'");
			$item_id=$rowItem["item_id"];

			$tag=$rowItem["tag"];
			$name=stripslashes($rowItem["name"]);
			$name=stripslashes($name);
			$nice_name=$rowItem["nice_name"];
			$type_id=$rowItem["type_id"];
			$price=number_format($rowItem["price"]);
			$selling_price=number_format($rowItem["selling_price"]);
			$item_parent_id=$rowItem["item_parent_id"];
			$stock_status=$rowItem["stock_status"];
			$brandd=$rowItem["item_brand"];
			$description=$rowItem["description"];
			if($item_id){
				$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
			}
			if(mysql_num_rows($qdataM) < 1){
				$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
			}
			$rowdatM = mysql_fetch_array($qdataM);

			if($rowdatM["media_file"]){
				$imglink='/imgthumb/150x160-'.$rowdatM["media_file"];
				$src = $baseURL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
			}else{
				$imglink='/imgthumb/150x160-no-photo.jpg';
				$src = $baseURL.'/shop/image/no-photo.jpg';
				$rowdatM["media_file"] = "no-photo.jpg";
			}
			$dest = $baseURL.$imageURL;
			createImgThumbIfnot($src,$dest,'150','160','ratiowh');

			//get today's savings
			if($rowItem["selling_price"] > $rowItem["price"]){
				$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
				$item_discount_per=number_format($item_discount_per1,0);
			}else{
				$item_discount_per="0";
			}
			//end
			$data=array(
				'product_id' =>$item_id,
				'product_name' =>$name,
				'product_image' =>$rowdatM["media_file"],
				'retail_price' =>$price,
				'currency' =>'INR',
				'sale_price' =>$selling_price,
				'description' =>$description,
				'saving_percentage'=>$item_discount_per
			);
			$datat1[]=$data;
		}
		$maxpages = ceil($totrecord/$num_rec_per_page);
		if($page < $maxpages){
			$next = $page+1;
		}else{
			$next = 0;	
		}
		if(count($datat1) > 0){
			$datat=array("status"=>'success', 'product_data'=>$datat1, 'msg'=>"Record found",'next'=>$next);
		}else{
			$datat=array("status"=>'fail', 'product_data'=>"", 'msg'=>"Record not found",'next'=>$next);
		}
	}else{
		$maxpages = ceil($totrecord/$num_rec_per_page);
		if($page < $maxpages){
			$next = $page+1;
		}else{
			$next = 0;	
		}
		$datat=array("status"=>'fail', 'product_data'=>"", 'msg'=>"Record Not Found",'next'=>$next);
		$return = app_logs($api_url, "Response", $log_id);
	}
}else{
	
	// Food filter only
	if(urldecode($attribute_value)){
		//$attribute_value = str_replace("undefined", "", $attribute_value);
		$attribute_value = trim($attribute_value,'|');
		$sbreedt = explode('|',$attribute_value);
		$type_life ="";
		$type_breed11="";
		$breedname ="";
		if($sbreedt[0]){
			foreach($sbreedt as $sbrt1){
				$sbrt1 = urldecode($sbrt1);
				if(array_key_exists($sbrt1,$life_stategs_arr)){
					$type_life .= $life_stategs_arr[$sbrt1]."-";
				}elseif(array_key_exists($sbrt1,$breed_type_arr)){
					$type_breed11 .= $breed_type_arr[$sbrt1]."-";
				}else{
					$breedname .= $sbrt1."-";
				}
			}
		}
	}
	//echo json_encode(urldecode($attribute_value));
	
	/*echo json_encode($type_life);
	
	echo json_encode($type_breed11);
	echo json_encode($breedname);
	die();*/
	if($item_brand){
		$item_brand = str_replace("undefined", "", $item_brand);
		$item_brand=trim($item_brand,'-');
		$sbrands=explode('-',$item_brand);
		if($sbrands[0]){
			foreach($sbrands as $sb){
				$bsql.=" OR i.item_brand=$sb ";
				$printbrand_name[]=$sb;	
			}
			$bsql = trim($bsql,' OR ');
			$bsql = " AND ($bsql)";
		}
	}

	if($breedname){
		$breedname = str_replace("undefined", "", $breedname);
		$breedname=trim($breedname,'-');
		$sbreeds=explode('-',$breedname);
		if($sbreeds[0]){
			foreach($sbreeds as $sbre){
				$breedsql.=" OR j.breed_id='$sbre' ";
				$printbreed_name[]=$sbre;
			}
			$breedsql = trim($breedsql,' OR ');
			$breedsql = " AND ($breedsql)";
		}
	}

	if($type_breed11){
		$type_breed11 = str_replace("undefined", "", $type_breed11);
		$type_breed11=trim($type_breed11,'-');
		$sbreedt=explode('-',$type_breed11);
		if($sbreedt[0]){
			foreach($sbreedt as $sbrt){
				$breedtsql.=" OR j.breed_type='$sbrt' ";
				$printbreed_type[]=$sbrt;
			}
			$breedtsql = trim($breedtsql,' OR ');
			$breedtsql = " AND ($breedtsql)";
		}
	}

	if($type_life){ //undefinedmature-
		$type_life = str_replace("undefined", "", $type_life);
		$type_life=trim($type_life,'-');
		$sbreedlife=explode('-',$type_life);
		if($sbreedlife[0]){
			foreach($sbreedlife as $sblife){
				$breedlsql.=" OR j.life_stage='$sblife' ";
				$printbreed_life[]=$sblife;
			}
			$breedlsql = trim($breedlsql,' OR ');
			$breedlsql = " AND ($breedlsql)";
		}
		
	}
	if($min_price && $max_price){
		$psql= " AND i.price BETWEEN '$min_price' AND '$max_price";
	}

	if($s_price1){
		$s_price1 = str_replace("Rs", "", $s_price1);
		$s_price1 = str_replace("-", "|", $s_price1);
		$s_price1=trim($s_price1,'|');
		$sprice1=explode('|',$s_price1);
		$pc1=count($sprice1);
		if($pc1>1){
			$psql1= " AND i.price BETWEEN '".$sprice1[0]."' AND '".$sprice1[1]."'";
		}
	}

	if($s_weight){
		$s_weight = str_replace("Kg", "", $s_weight);
		$s_weight = str_replace("-", "|", $s_weight);
		$s_weight=trim($s_weight,'|');
		$s_weight1=explode('|',$s_weight);
		$wt1=count($s_weight1);
		if($wt1>1){
			$wsql1= " AND i.weight BETWEEN '".$s_weight1[0]."' AND '".$s_weight1[1]."'";
		}
	}

	if($s_weightprice){
		$s_weightprice = str_replace("Rs", "", $s_weightprice);
		$s_weightprice = str_replace("-", "|", $s_weightprice);
		$s_weightprice=trim($s_weightprice,'|');
		$s_weightprice1=explode('|',$s_weightprice);
		$wtp1=count($s_weightprice1);
		if($wtp1>1){
			$wsps1= " AND i.weight_price BETWEEN '".$s_weightprice1[0]."' AND '".$s_weightprice1[1]."'";
		}
	}
	if($s_sort=='price_asc'){
		$st='i.price DESC,i.item_display_order ';	
	}elseif($s_sort=='price_desc'){
		$st='i.price ASC,i.item_display_order ';
	}elseif($s_sort=='relevance'){
		$st='i.num_views DESC,i.item_display_order ';
	}
	else{
		if($breedname){
			$st='i.dry_dog_display desc ,i.item_display_order ,i.item_brand ';
		}else{
			$st='i.item_display_order ,i.item_brand ';
		}
	}
	
	if($show1==0){
		$s="AND i.stock_status='instock'";	
	}
	//echo json_encode("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_brand,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,j.breed_id,j.breed_type,j.life_stage,i.weight,i.item_display_order FROM shop_items as i,breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable' AND i.visibility='visible' AND i.item_display_status!='delete' AND j.item_id=i.item_id $breedsql $breedtsql $breedlsql $bsql $psql1 $wsps1 $wsql1 $s");
	//die();
	$qItemAll_count=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_brand,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,j.breed_id,j.breed_type,j.life_stage,i.weight,i.item_display_order FROM shop_items as i,breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable' AND i.visibility='visible' AND i.item_display_status!='delete' AND j.item_id=i.item_id $breedsql $breedtsql $breedlsql $bsql $psql1 $wsps1 $wsql1 $s");
	$totrecord = mysql_num_rows($qItemAll_count);
		
	$qItemAll=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_brand,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,j.breed_id,j.breed_type,j.life_stage,i.weight,i.item_display_order FROM shop_items as i,breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable' AND i.visibility='visible' AND i.item_display_status!='delete' AND j.item_id=i.item_id $breedsql $breedtsql $breedlsql $bsql $psql1 $wsps1 $wsql1 $s limit $start_from, $num_rec_per_page");
	//$totrecord = mysql_num_rows($qItemAll);
	
	$qItem=query_execute("SELECT i.item_id,i.item_brand, i.name, i.nice_name, i.price, i.selling_price,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,j.breed_id,j.breed_type,j.life_stage,i.weight,i.item_display_order FROM shop_items as i,breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable'  AND  i.visibility='visible' AND i.item_display_status!='delete' AND j.item_id=i.item_id AND i.stock_status='instock' $breedsql $breedtsql $breedlsql $bsql $psql1 $wsps1 $wsql1 $s  ORDER BY $st limit $start_from, $num_rec_per_page ");
	
	$qItemnavi=query_execute("SELECT i.item_id,i.item_brand, i.name, i.nice_name, i.price, i.selling_price,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,j.breed_id,j.breed_type,j.life_stage,i.weight,i.item_display_order FROM shop_items as i,breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable' AND  i.visibility='visible' AND i.item_display_status!='delete' AND j.item_id=i.item_id $breedsql $breedtsql $breedlsql $bsql $psql1 $wsps1 $wsql1 $s  ORDER BY  $st limit $start_from, $num_rec_per_page ");
	
	$qItem23=query_execute("SELECT i.item_id,i.item_brand, i.name, i.nice_name, i.price, i.selling_price,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,j.breed_id,j.breed_type,j.life_stage,i.weight,i.item_display_order FROM shop_items as i,breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable' AND i.visibility='visible' AND i.stock_status='outofstock' AND i.item_display_status!='delete' AND j.item_id=i.item_id $breedsql $breedlsql $breedtsql $bsql $psql1 $wsql1 $wsps1 $s ORDER BY $st limit $start_from, $num_rec_per_page ");
	$totrecord23 = mysql_num_rows($qItem23);
	$qItemoutstock=query_execute("SELECT i.item_id,i.item_brand, i.name, i.nice_name, i.price, i.selling_price,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,j.breed_id,j.breed_type,j.life_stage,i.weight,i.item_display_order FROM shop_items as i,breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable' AND i.visibility='visible' AND i.stock_status='outofstock' AND i.item_display_status!='delete' AND j.item_id=i.item_id $breedsql $breedlsql $breedtsql $bsql $psql1 $wsql1 $wsps1 $s ORDER BY $st limit $start_from, $num_rec_per_page ");
	$toqItemoutstock = mysql_num_rows($qItemoutstock);
	while($rowItemall = mysql_fetch_array($qItemAll)){
		$Aitem_brand[]=$rowItemall["item_brand"];
		$Aitem_price[]=$rowItemall["price"];
		$Abreed_id[]=$rowItemall["breed_id"];
		$Abreed_type[]=$rowItemall["breed_type"];
		$Alife_stage[]=$rowItemall["life_stage"];
		$Aweight[]=$rowItemall["weight"];
		$Aweight_price[]=$rowItemall["weight_price"];
	}
	if($totrecord>0){
		$Aitem_brand=array_unique($Aitem_brand);
		$Aitem_price=array_unique($Aitem_price);
		$Abreed_type=array_unique($Abreed_type);
		$Alife_stage=array_unique($Alife_stage);
		$Abreed_id=array_unique($Abreed_id);
		$Aweight=array_unique($Aweight);
		$Aweight_price=array_unique($Aweight_price);
		//print_r($Abreed_id);
		//print_r($Abreed_type);
		$breedid_count=count($Abreed_id);
		$breedtype_count=count($Abreed_type);
		$breedlife_count=count($Alife_stage);
		asort($Aweight_price);
		foreach($Aweight_price as $wtp){
			$ANAweight_price[]=$wtp;
		}
		$weightprice_count=count($ANAweight_price);
		$weightprice_count=$weightprice_count-1;
		$wtp1=$ANAweight_price[0];
		$endweightp= $ANAweight_price[$weightprice_count];
		//asort($Alife_stage);
		asort($Aweight);
		//asort($Abreed_type);
		foreach($Aweight as $wt){
			$ANitem_weight[]=$wt;
		}
		$weight_count=count($ANitem_weight);
		$weight_count=$weight_count-1;
		 $wt1=$ANitem_weight[0];
		$endweight= $ANitem_weight[$weight_count];
		
		asort($Aitem_price);
		foreach($Aitem_price as $ap){
			$ANitem_price[]=$ap;
		}
		$price_count=count($ANitem_price);
		$price_count=$price_count-1;
		$endnum = $ANitem_price[$price_count];
		$startnum = $ANitem_price[0];
		$st=$ANitem_price[0];
		$endamount= $ANitem_price[$price_count];
	
		while($rowItem43 = mysql_fetch_array($qItem)){
			$Uitem_id12[]=$rowItem43["item_id"];
		}
		$unique = array_map("unserialize", array_unique(array_map("serialize", $Uitem_id12)));
		$totrecord_final=count($unique);
		if($totrecord_final > 0){
			foreach($unique as $items){
				$rowItem=query_execute_row(" SELECT item_id,item_brand,name,nice_name,price,selling_price,weight_price, item_parent_id,stock_status,weight,item_display_order FROM shop_items WHERE item_id='$items'");
				$item_id=$rowItem["item_id"];
				$name=$rowItem["name"];
				$nice_name=$rowItem["nice_name"];
				$brand_name=$rowItem["item_brand"];
				$price=$rowItem["price"];
				$weight_item=number_format($rowItem["weight"],1);
				$selling_price=$rowItem["selling_price"];
				$item_parent_id=$rowItem["item_parent_id"];
				$stock_status=$rowItem["stock_status"];
				$item_display_order=$rowItem["item_display_order"];
				//$weight_price=number_format($rowItem["weight_price"],0);
				$weight_price=number_format($rowItem["price"]/$rowItem["weight"],0);
				$sqlmediaco1 = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
				$trecount1=mysql_fetch_array($sqlmediaco1);   
				if ($trecount1['trecd'] != '0') {
					$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
				}else{
					$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
				}
				$rowdatM = mysql_fetch_array($qdataM);
				
				if($rowdatM["media_file"]){
					$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
					$imageURL='/imgthumb/150x160-'.$rowdatM["media_file"];
				}else{
					$src = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
					$imageURL='/shop/image/no-photo-t.jpg';
				}
				
				// Get Image Width and Height, Updated date: 19-Nov-2014
				$image_info12 = getimagesize($DOCUMENT_ROOT.$imageURL);		
				$image_width12 = $image_info12[0];
				$image_height12 = $image_info12[1];
				// End
				
				$dest = $DOCUMENT_ROOT.$imageURL;
				createImgThumbIfnot($src,$dest,'150','160','ratiowh');
				
				//get today's savings
				if($rowItem["selling_price"] > $rowItem["price"]){
					$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
					$item_discount_per=number_format($item_discount_per1,0);
				}else{
					$item_discount_per="0";
				}
				//end
				$data=array(
					'product_id' =>$item_id,
					'product_name' =>$name,
					'product_image' =>$rowdatM["media_file"],
					'retail_price' =>number_format($price,0),
					'currency' =>'INR',
					'sale_price' =>number_format($selling_price,0),
					'description' =>$description,
					'saving_percentage'=>$item_discount_per
				);
				$datat1[]=$data;
			}
			$maxpages = ceil($totrecord/$num_rec_per_page);
			if($page < $maxpages){
				$next = $page+1;
			}else{
				$next = 0;	
			}
			if(count($datat1) > 0){
				$datat=array("status"=>'success', 'product_data'=>$datat1, 'msg'=>"Record found",'next'=>$next);
			}else{
				$datat=array("status"=>'fail', 'product_data'=>$datat1, 'msg'=>"Record found",'next'=>$next);
			}
		}else{
			$maxpages = ceil($totrecord/$num_rec_per_page);
			if($page < $maxpages){
				$next = $page+1;
			}else{
				$next = 0;	
			}
			$datat=array("status"=>'fail', 'product_data'=>"", 'msg'=>"Record not found",'next'=>$next);
		}
	}
}

echo json_encode($datat);
$return = app_logs($api_url, "Response", $log_id);
?>