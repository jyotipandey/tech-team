<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	//require_once($baseURL.'/session.php');
	
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "successPage.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);	
	
	$care = array(
		'contact_no' => "+91-9212196633",
		'time' => "10 AM to 6 PM"
	);
	
	$marketing_alliance = "marketing@dogspot.in";
	
	$mail_us = array(
		'address1' => "Plot no - 545, S.Lal Tower",
	    'address2' => "Sector - 20, Dundahera",
	    'city' => "Gurgaon",
	    'zip' => "122016",
	    'state' => "Haryana",
	    'country' => "INDIA"
	);
	
	if($mail_us && $care){
		$data[result] = array(
			'cus_care' =>$care,
			'mail_us' =>$mail_us,
			'marketing_email' =>$marketing_alliance	
		);
	}else{
    	$data[result] = array(
         	'status' =>"Not available"
		);
	}
	$das=json_encode($data);
	echo $das;
	$log_id = appLogPhoneGap($api_url, "Response",$log_id);
?>