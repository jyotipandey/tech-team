	<?php
	
	/*
	*	Collect Data of shipping and Create Order but Order still not complete will go to next step for Payment details 
	* 	Created By Umesh & Brajendra Sir, Dated: 16/01/2015
	* 	Required Parameters
	*	1- $access_token
	*	2- $payment_type
	*	3- $order_id
	*	4- $cod_check
	*	5- $discount_code1
	*/
	
	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/shop/arrays/arrays.php');
	require_once('constants.php');
	
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "getPaymentPayU.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);	
	
	if($access_token){
		$data=base64_decode($access_token);	
		$vardata = explode('##',$data);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		
	}
	else{
		$user_id = "Guest";	
	}
	
	if($user_id!="Guest" && $payment_type && $order_id && $session_id){
		
		$discount_code1 = '0';			// this is a required param, will pass from request
		
		$ordchq = '';
		
		$SQ_users = query_execute_row("SELECT * FROM users WHERE userid='$user_id'");
		
		$SQ_more_address = query_execute_row("SELECT * FROM shop_order_more_address WHERE userid='$user_id' AND address_email='".$SQ_users[u_email]."'");
		
		$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id='".$returnArr['order_id']."'");
		
		if($SQ_more_address[address_address1]==""){
			$SQ_more_address[address_address1] = $SQ_more_address[address_address2];
		}
		$displaypin = "0"; 	
		$postArr = array(
			'app_name' => "mobile-app",
			'order_id' =>$order_id,
			'u_email' =>$SQ_users[u_email],
			'us_email' =>$SQ_users[u_email],
			'pin' =>$SQ_more_address[address_zip],
			'session_id' =>$session_id,
			'name' =>$SQ_more_address[address_name],
			'address1' =>$SQ_more_address[address_address1],
			'mobile1' =>$SQ_more_address[address_phone1],
			'address_email2' =>$SQ_more_address[address_email],
			'city' =>$SQ_more_address[address_city],
			'receiver_email' =>$SQ_more_address[u_email],
			'state' =>$SQ_more_address[address_id].";",$SQ_more_address[address_state].";".$SQ_more_address[address_state_nicename],
			'fishtype' =>"",			
			'country_name' =>";INDIA;india",
			'discount_code1' =>$discount_code1,
			'donatebag' =>"",
			'displaypin' =>$displaypin,
			'order_amount' =>$SQ_shop_order[order_amount],
			'payment_type'=>$payment_type,
			'payment'=>$payment_type,
			'order_shipment_amount' =>$SQ_shop_order[order_shipment_amount],
			'order_items_amount' => $SQ_shop_order[order_items_amount],
			'order_discount_amount' => $SQ_shop_order[order_discount_amount],
		);
		
		if($ordchq==''){
			$arrayOrderData=array(
				'userid'=>$user_id,
				'session_id'=>$session_id,
				'order_user_ip'=>$device_id
			);

			$result = array_merge($postArr, $arrayOrderData);
		
			$returnArr = createOrder($result);
			
			if($response){
				$ResponseCode = $response["ResponseCode"];
				$ResponseMessage = $response["ResponseMessage"];
				$DateCreated = $response["DateCreated"];
				$PaymentID = $response["PaymentID"];
				$MerchantRefNo = $response["MerchantRefNo"];
				$Amount = $response["Amount"];
				$RMode = $response["Mode"];
				$IsFlagged = $response["IsFlagged"];
				$TransactionID = $response["TransactionID"];
				
				
				if($ResponseCode=='0'){
					//echo "UPDATE shop_order SET order_transaction_id = '$TransactionID', order_transaction_source='EBS', order_transaction_amount = '$Amount', order_status = '$ResponseCode', order_message = '$ResponseMessage', datecreated='$DateCreated', paymentid = '$PaymentID', mode = '$RMode', isflagged = '$IsFlagged' WHERE order_id = '$MerchantRefNo'";
					
					$resultinsert = query_execute("UPDATE shop_order SET order_transaction_id = '$TransactionID', order_transaction_source='EBS', order_transaction_amount = '$Amount', order_status = '$ResponseCode', order_message = '$ResponseMessage', datecreated='$DateCreated', paymentid = '$PaymentID', mode = '$RMode', isflagged = '$IsFlagged' WHERE order_id = '$MerchantRefNo'");
					
					$upOrder1 = query_execute_row("SELECT order_items_amount,order_discount_amount FROM shop_order WHERE order_id='$MerchantRefNo'");
					
			  		$upOrder2 = query_execute_row("SELECT cart_id FROM shop_cart WHERE cart_order_id='$MerchantRefNo' AND donate_bag='2'");
					if($upOrder2['cart_id']==''){
						$check1=$upOrder1['order_items_amount']-$upOrder1['order_discount_amount'];
						//echo "check1".$check1;
						if($check1>=2000){
							//response($session_id,$userid,$MerchantRefNo,$ResponseCode);
						}
					}
			  		//response($session_id,$userid,$MerchantRefNo,$ResponseCode);
					
					//echo "UPDATE shop_cart SET cart_order_status = '$ResponseCode' WHERE cart_order_id = '$MerchantRefNo'";
					
					$resultinsert = query_execute("UPDATE shop_cart SET cart_order_status = '$ResponseCode' WHERE cart_order_id = '$MerchantRefNo'");
					
				}
				else{
					//echo "UPDATE shop_order SET order_transaction_id = '$TransactionID', order_transaction_amount = '$Amount', order_status = '$ResponseCode', order_message = '$ResponseMessage', datecreated='$DateCreated', paymentid = '$PaymentID', isflagged = '$IsFlagged' WHERE order_id = '$MerchantRefNo'";
					
					$resultinsert = query_execute("UPDATE shop_order SET order_transaction_id = '$TransactionID', order_transaction_amount = '$Amount', order_status = '$ResponseCode', order_message = '$ResponseMessage', datecreated='$DateCreated', paymentid = '$PaymentID', isflagged = '$IsFlagged' WHERE order_id = '$MerchantRefNo'");
				}
			}
			
			// For COD and DD case 
			if($returnArr['order_id'] && ($payment_type=='cod' || $payment_type=='dd' || $payment_type=='gv')){
				
				
				
				$StatusCheck = query_execute_row("SELECT delevery_status, cod_confirm FROM shop_order WHERE order_id = '".$returnArr['order_id']."'");
				if($StatusCheck['delevery_status']!='new') {
					$order_already_placed = '1';
				}
				//echo "UPDATE shop_order SET domain_id = '1' WHERE order_id = '".$returnArr['order_id']."'";
				
				$domainUpdate = query_execute("UPDATE shop_order SET domain_id = '1' WHERE order_id = '".$returnArr['order_id']."'");
				
				$querydonate = query_execute("SELECT * FROM shop_cart WHERE cart_order_id='".$returnArr['order_id']."' AND donate_bag='1' ");
				
				$cdonate = mysql_num_rows($querydonate);
				
				if($payment_type=='cod' || $payment_type=='gv'){
					$order_status = 0;
					$check_amount = query_execute("SELECT order_shipment_amount FROM shop_order WHERE order_id = '".$returnArr['order_id']."'");	
					$cod_amount = $check_amount['order_shipment_amount'];
					if($cod_amount == "0.0000"){
						$ship_amount = 99;
						//echo "UPDATE shop_order SET order_shipment_amount = '$ship_amount' WHERE order_id = '".$returnArr['order_id']."'";
						
						$updateship = query_execute("UPDATE shop_order SET order_shipment_amount = '$ship_amount' WHERE order_id = '".$returnArr['order_id']."'");
					}
				}
				else{
					$order_status = 1;
				}
				
				$DateCreated = date("o m d");
				//echo "UPDATE shop_order SET order_status = '$order_status', mode = '$payment_type' WHERE order_id = '".$returnArr['order_id']."'";
				
				$resultinsert = query_execute("UPDATE shop_order SET order_status = '$order_status', mode = '$payment_type' WHERE order_id = '".$returnArr['order_id']."'");
				
				//echo "SELECT order_items_amount,order_discount_amount FROM shop_order WHERE order_id='".$returnArr['order_id']."'";
				
				$upOrder1 = query_execute_row("SELECT order_items_amount,order_discount_amount FROM shop_order WHERE order_id='".$returnArr['order_id']."'");
				
				$upOrder2 = query_execute_row("SELECT cart_id FROM shop_cart WHERE cart_order_id='".$returnArr['order_id']."' AND donate_bag='2'");
			  
				if($upOrder2['cart_id']==''){
				
					$check1=$upOrder1['order_items_amount']-$upOrder1['order_discount_amount'];
					//echo "check1".$check1;
				
					if($check1>=2000){
						//response($session_id,$userid,$order_id,$order_status);
					}
				}
				if($cdonate==0){
					response($session_id,$user_id,$order_id,$order_status);
				}
				else{
					responsedonate($session_id,$user_id,$order_id,$order_status);
				}
				//echo "UPDATE shop_cart SET cart_order_status = '$order_status' WHERE cart_order_id = '".$returnArr['order_id']."'";
				
				$resultinsert1 = query_execute("UPDATE shop_cart SET cart_order_status = '$order_status' WHERE cart_order_id = '".$returnArr['order_id']."'");
				
				$quseridCOD = query_execute_row("SELECT u_id FROM shop_order WHERE order_id='".$order_id."'");
		
				$getc = "SELECT count(*) as successrate FROM shop_order Where mode!='TEST' AND delevery_status='delivered' AND (userid='".$quseridCOD['u_id']."' OR u_id='".$quseridCOD['u_id']."')";
				
				$qselectCOD = query_execute_row($getc);
				
				$sel_cart_items_code = mysql_query("SELECT * FROM shop_cart WHERE cart_order_id='".$returnArr['order_id']."'");
				
				$sel_cart_items_code2 = query_execute_row("SELECT order_amount,order_discount_id FROM shop_order WHERE order_id='".$returnArr['order_id']."'");
				while($sel_cart_items_code1 = mysql_fetch_array($sel_cart_items_code)){
					$code_items = $sel_cart_items_code1['item_id'];
					$code_item_qty = $sel_cart_items_code1['item_qty'];
					$analyticsArray[$code_items] = $code_item_qty;
				}
				
				couponcheck($order_id);				// check if coupon is apllied on
				
				$x = base64_encode($returnArr['order_id']);
				
				if($payment_type=='cod' && $order_status=='0'){
					$data = array(
						'order_id' => $returnArr['order_id'],
						'cart_order_id' => $x,
						'tsid' => $session_id,
						'mode' => $payment_type,
						'order_status' => $order_status,
						'payment_status' => "Success",
						'payment_method' => $AOrderMethod[$payment_type],
					);	
				}
				
				if($payment_type=='dd' && $order_status=='1'){
					$data = array(
						'order_id' => $returnArr['order_id'],
						'cart_order_id' => $x,
						'tsid' => $session_id,
						'mode' => $payment_type,
						'order_status' => $order_status,
						'payment_status' => "Success",
						'payment_method' => $AOrderMethod[$payment_type],
						'dd_address' => "Please send the Demand Draft/Cheque in favor of 'Radox Trading & Marketing Pvt Ltd.' payable at 'Gurgaon to the following address' Plot no - 545, S.Lal Tower, Sector - 20, Dundahera , Gurgaon, Haryana PIN: 122016 (India)"
					);
				}
			}
			
			// COD and DD END..............
			
			$quseridCOD = query_execute_row("SELECT u_id FROM shop_order WHERE order_id='".$returnArr['order_id']."'");

			$getc = "SELECT count(*) as successrate FROM shop_order Where mode!='TEST' AND delevery_status='delivered' AND (userid = '".$quseridCOD['u_id']."' OR u_id = '".$quseridCOD['u_id']."')";

			$qselectCOD = query_execute_row($getc);

			$sel_cart_items_code = mysql_query("SELECT * FROM shop_cart WHERE cart_order_id='".$returnArr['order_id']."'");

			$sel_cart_items_code2 = query_execute_row("SELECT order_amount, order_discount_id FROM shop_order WHERE order_id='".$returnArr['order_id']."'");

			while($sel_cart_items_code1 = mysql_fetch_array($sel_cart_items_code)){

				$code_items = $sel_cart_items_code1['item_id'];

				$code_item_qty = $sel_cart_items_code1['item_qty'];

				$analyticsArray[$code_items] = $code_item_qty;
			}
			
			
			//FOR CC, DC and Net-Banking
			if($returnArr['order_id'] && ($payment_type=='cc' || $payment_type=='dc' || $payment_type=='nb')){
				//echo "UPDATE shop_order SET order_amount = order_items_amount + item_shipping_amount - order_discount_amount WHERE order_id = '".$returnArr['order_id']."'";
				
				$queryOrdUp12 = query_execute("UPDATE shop_order SET order_amount = order_items_amount + item_shipping_amount - order_discount_amount WHERE order_id = '".$returnArr['order_id']."'");
		
				// Update Order amount
				$updateCart = query_execute("SELECT u_id, order_amount, order_method FROM shop_order WHERE order_id='".$returnArr['order_id']."'");
				$rowOrderamt = mysql_fetch_array($updateCart);
				$order_amount = $rowOrderamt["order_amount"];
				$u_id = $rowOrderamt["u_id"];
				$order_method = $rowOrderamt["order_method"];
				
				// Update Order amount
				$getUsersBill = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '1' AND order_id = '".$returnArr['order_id']."'");
				$rowUsersBill = mysql_fetch_array($getUsersBill);
				$name = removeSpecialCharFromString($rowUsersBill["address_name"]);
				$email = $rowUsersBill["address_email"];
				$address1 = removeSpecialCharFromString($rowUsersBill["address_address1"]);
				$address2 = removeSpecialCharFromString($rowUsersBill["address_address2"]);
				$address_city = removeSpecialCharFromString($rowUsersBill["address_city"]);
				$address_state = $rowUsersBill["address_state"];
				$address_country = $rowUsersBill["address_country_code"];
				$pin = $rowUsersBill["address_zip"];
				$mobile1 = $rowUsersBill["address_phone1"];
				$phone1 = $rowUsersBill["address_phone2"];
				$fax = $rowUsersBill["address_fax"];
		
				// get details from shop_order_address table Shipping------------------------------------------
				$getUsersShip = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '1' AND order_id = '".$returnArr['order_id']."'");
				$rowUsersShip = mysql_fetch_array($getUsersShip);
				$ship_name = removeSpecialCharFromString($rowUsersShip["address_name"]);
				$ship_address1 = removeSpecialCharFromString($rowUsersShip["address_address1"]);
				$ship_address2 = removeSpecialCharFromString($rowUsersShip["address_address2"]);
				$ship_city = $rowUsersShip["address_city"];
				$ship_state = $rowUsersShip["address_state"];
				$ship_country = $rowUsersShip["address_country"];
				$ship_pin = $rowUsersShip["address_zip"];
				$ship_phone1 = $rowUsersShip["address_phone1"];
				// get details from shop_order_address table END Shipping--------------------------------------	
				$shahash = "i0JYEe|$order_id|$order_amount|PetShop|$name|$email|||||||||||gE7Ys3Ee";
				
				$postArrData = array(
					'post_url' => "https://secure.payu.in/_payment.php",
					'amount' => $order_amount,
					'key' => "i0JYEe",
					'surl' => "http://m.dogspot.in/response-payu.php?session=".$session_id,
					'Furl' => "http://m.dogspot.in/response-payu.php?session=".$session_id,
					'pg' => ucwords($order_method),
					'productinfo' => "PetShop",
					'txnid' => $returnArr['order_id'],
					'firstname' => $name,
					'address1' => $address1.' '.$address2,
					'City' => $address_city,
					'State' => $address_state,
					'Zipcode' => $pin,
					'Country' => $address_country,
					'email' => $email,
					'phone' => $mobile1,
					'user_credentials' => "i0JYEe:".$email,
					'Hash' => hash('sha512', $shahash)	
				);
				
				$name = addslashes($name);
				
				$address1 = addslashes($address1);
				
				$address2 = addslashes($address2);
				
				$ship_name = addslashes($ship_name);
				
				$ship_address1 = addslashes($ship_address1);
				
				$ship_address2 = addslashes($ship_address2);
				
				//echo "INSERT INTO shop_checkout_logs (checkout_logs_amount, checkout_logs_account_id, checkout_logs_return_url, checkout_logs_description, checkout_logs_reference_no, checkout_logs_name, checkout_logs_address, checkout_logs_city, checkout_logs_state, checkout_logs_postal_code, checkout_logs_country, checkout_logs_email, checkout_logs_phone, checkout_logs_ship_name, checkout_logs_ship_address, checkout_logs_ship_city, checkout_logs_ship_state, checkout_logs_ship_postal_code, checkout_logs_ship_country, checkout_logs_ship_phone, checkout_logs_mode) VALUES('$order_amount', '7860', 'http://www.dogspot.in/shop/response.php?DR={DR}', 'PetShop', '".$returnArr['order_id']."', '$name', '$address1 $address2', '$address_city', '$address_state', '$pin', '$address_country', '$email', '$mobile1', '$ship_name', '$ship_address1 $ship_address2', '$ship_city', '$ship_state', '$ship_pin', '$ship_country', '$ship_phone1', 'Mobile')";
				
				$resultinsert = query_execute("INSERT INTO shop_checkout_logs (checkout_logs_amount, checkout_logs_account_id, checkout_logs_return_url, checkout_logs_description, checkout_logs_reference_no, checkout_logs_name, checkout_logs_address, checkout_logs_city, checkout_logs_state, checkout_logs_postal_code, checkout_logs_country, checkout_logs_email, checkout_logs_phone, checkout_logs_ship_name, checkout_logs_ship_address, checkout_logs_ship_city, checkout_logs_ship_state, checkout_logs_ship_postal_code, checkout_logs_ship_country, checkout_logs_ship_phone, checkout_logs_mode) VALUES('$order_amount', '7860', 'http://www.dogspot.in/shop/response.php?DR={DR}', 'PetShop', '".$returnArr['order_id']."', '$name', '$address1 $address2', '$address_city', '$address_state', '$pin', '$address_country', '$email', '$mobile1', '$ship_name', '$ship_address1 $ship_address2', '$ship_city', '$ship_state', '$ship_pin', '$ship_country', '$ship_phone1', 'Mobile')");
				
				if($payment_type=='cc' || $payment_type=='dc' || $payment_type=='nb'){
					$data = array(
						'post_data'=>$postArrData,
						'payment_method'=>$AOrderMethod[$payment_type]
					);	
				}
			}
			// CC, DC and NB End............
			
			
			//Payment Method PayTM Wallet
			if($returnArr['order_id'] && $payment_type=='paytm'){
				require_once($baseURL."/shop/config_paytm.php");
				require_once($baseURL."/shop/encdec_paytm.php");
				$checkSum = "";
				$paramList = array();
				$updateCart=query_execute("SELECT u_id, order_amount,order_method FROM shop_order WHERE order_id='".$returnArr['order_id']."'");
						
				$rowOrderamt = mysql_fetch_array($updateCart);
				$order_amount = $rowOrderamt["order_amount"];
				$u_id = $rowOrderamt["u_id"];
				$order_method= $rowOrderamt["order_method"];
	
				// get details from shop_order_address table Shipping------------------------------------------
				
				$getUsersShip = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '1' AND order_id = '".$returnArr['order_id']."'");
				$rowUsersShip = mysql_fetch_array($getUsersShip);
				$ship_name = removeSpecialCharFromString($rowUsersShip["address_name"]);
				$ship_address1 = removeSpecialCharFromString($rowUsersShip["address_address1"]);
				$ship_address2 = removeSpecialCharFromString($rowUsersShip["address_address2"]);
				$email = $rowUsersShip["address_email"];
				$ship_city = $rowUsersShip["address_city"];
				$ship_state = $rowUsersShip["address_state"];
				$ship_country = $rowUsersShip["address_country_code"];
				$ship_pin = $rowUsersShip["address_zip"];
				$ship_phone1 = $rowUsersShip["address_phone1"];
				// get details from shop_order_address table END Shipping--------------------------------------	
				
				// Create an array having all required parameters for creating checksum.
				$paramList["MID"] = PAYTM_MERCHANT_MID;
				$paramList["ORDER_ID"] = $returnArr['order_id'];
				$paramList["CUST_ID"] = $u_id;
				$paramList["INDUSTRY_TYPE_ID"] = 'Retail110';
				$paramList["CHANNEL_ID"] = 'WEB';
				$paramList["TXN_AMOUNT"] = number_format($order_amount,2,',','');
				$paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
				$paramList["MOBILE_NO"] = $ship_phone1; 			//Mobile number of customer
				$paramList["EMAIL"] = $email; 					  //Email ID of customer
				//$paramList["PAYMENT_TYPE_ID"] = 'PPI';
				
				//$paramList["VERIFIED_BY"] = "EMAIL"; //
				//$paramList["IS_USER_VERIFIED"] = "YES"; //
	
				//Here checksum string will return by getChecksumFromArray() function.
				$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);

				$paramList["CHECKSUMHASH"] = $checkSum;
				
				couponcheck($order_id);
				$name=addslashes($name);
				$address1=addslashes($ship_address1);
				$address2=addslashes($ship_address2);
				$ship_name=addslashes($ship_name);
				$ship_address1=addslashes($ship_address1);
				$ship_address2=addslashes($ship_address2);

				$resultinsert = query_execute("INSERT INTO shop_checkout_logs (checkout_logs_amount, checkout_logs_account_id, checkout_logs_return_url, checkout_logs_description, checkout_logs_reference_no, checkout_logs_name, checkout_logs_address, checkout_logs_city, checkout_logs_state, checkout_logs_postal_code, checkout_logs_country, checkout_logs_email, checkout_logs_phone, checkout_logs_ship_name, checkout_logs_ship_address, checkout_logs_ship_city, checkout_logs_ship_state, checkout_logs_ship_postal_code, checkout_logs_ship_country, checkout_logs_ship_phone, checkout_logs_mode) VALUES('$order_amount', '7860', 'http://www.dogspot.in/shop/response.php?DR={DR}', 'PetShop', '$order_id', '$name', '$address1 $address2', '$address_city', '$address_state', '$pin', '$address_country', '$email', '$ship_phone1', '$ship_name', '$ship_address1 $ship_address2', '$ship_city', '$ship_state', '$ship_pin', '$ship_country', '$ship_phone1', 'LIVE')");
				
				if($payment_type=='paytm'){
					$data = array(
						'post_data'=>$paramList,
						'payment_method'=>$AOrderMethod[$payment_type]
					);	
				}

			}
			
			
			// update Discount Start
			$orderStat = query_execute("SELECT * FROM shop_order WHERE order_id = '".$returnArr['order_id']."'");
			$rowOrd = mysql_fetch_array($orderStat);
			$order_discount_id = $rowOrd["order_discount_id"];
			$order_status = $rowOrd["order_status"];
			$order_transaction_source = $rowOrd["order_transaction_source"];
			$order_method = $rowOrd["order_method"];
			$orderuserid = $rowOrd["userid"];
			$delevery_status = $rowOrd["delevery_status"];
			$order_transaction_id = $rowOrd["order_transaction_id"];
			
			//updateDiscountStatus($returnArr['order_id']);
			
			$order_items_amount = number_format($rowOrd["order_items_amount"]);
			$order_shipment_amount = number_format($rowOrd["order_shipment_amount"]);
			$item_shipping_amount = number_format($rowOrd["item_shipping_amount"]);
			$order_discount_amount = number_format($rowOrd["order_discount_amount"]);
			$totAmount = number_format($rowOrd["order_amount"]);
			$totAmount2 = $rowOrd["order_items_amount"] + $rowOrd["order_shipment_amount"] - $rowOrd["order_discount_amount"] + $item_shipping_amount;
			// update Discount
			
			$checkamount=query_execute_row("SELECT SUM(amount) as total FROM shop_order_coupon WHERE order_id='".$returnArr['order_id']."'");
			
			$totAmount1=$totAmount2-$checkamount['total'];
				
			//echo "UPDATE shop_cart SET userid='$user_id' WHERE cart_order_id = '".$returnArr['order_id']."'";
			
			$GuestCartUpdate = query_execute("UPDATE shop_cart SET userid='$user_id' WHERE cart_order_id = '".$returnArr['order_id']."'");

			// get details from shop_order_address table Shipping------------------------------------------
			$getUsersShip = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '1' AND order_id = '".$returnArr['order_id']."'");
			$rowUsersShip = mysql_fetch_array($getUsersShip);
			$ship_name = stripslashes($rowUsersShip["address_name"]);
			$ship_address1 = stripslashes($rowUsersShip["address_address1"]);
			$ship_address2 = stripslashes($rowUsersShip["address_address2"]);
			$ship_city = $rowUsersShip["address_city"];
			$ship_state = $rowUsersShip["address_state"];
			$ship_country = $rowUsersShip["address_country"];
			$ship_pin = $rowUsersShip["address_zip"];
			$ship_phone1 = $rowUsersShip["address_phone1"];
			
			if(($order_method=='nb' || $order_method=='dc' || $order_method=='cc') && $delevery_status == 'new' ){
				//echo "UPDATE shop_order SET delevery_status='pending-dispatch' WHERE order_id = '".$returnArr['order_id']."' AND delevery_status = 'new'";
				
				$prepaidcount = query_execute("UPDATE shop_order SET delevery_status='pending-dispatch' WHERE order_id = '".$returnArr['order_id']."' AND delevery_status = 'new'");
				//echo "INSERT INTO section_reviews() VALUES('', 'BackGroundJob', 'shop-order', '".$returnArr['order_id']."', 'Changed Delivery Status: pending-dispatch,', 'pending-dispatch', NULL, '$device_id')";
				
				$sectrevw = query_execute("INSERT INTO section_reviews() VALUES('', 'BackGroundJob', 'shop-order', '".$returnArr['order_id']."', 'Changed Delivery Status: pending-dispatch,', 'pending-dispatch', NULL, '$device_id')");
			}
	
			if($order_method=='cod' || $order_method=='gv'){	

				$codcount = query_execute_row("SELECT count(*) as prepaidcount FROM shop_order  WHERE userid = '$user_id' AND delevery_status='delivered' AND order_method='cod'");

				$pincodecheck = query_execute_row("SELECT count(*) as pincodechck FROM shop_couriorpin_availablity WHERE courier_pincode = '$ship_pin' AND courier_method='cod'");
				
				if($codcount['prepaidcount'] >= '2' && $pincodecheck['pincodechck'] > '0' && $order_status=='0' && $delevery_status=='new' ){			
					//echo "UPDATE shop_order SET delevery_status='pending-dispatch' WHERE order_id = '".$returnArr['order_id']."' AND delevery_status = 'new'";
				
					$orderStat = query_execute("UPDATE shop_order SET delevery_status='pending-dispatch' WHERE order_id = '".$returnArr['order_id']."' AND delevery_status = 'new'");
		
					//echo "INSERT INTO section_reviews() VALUES('', 'BackGroundJob', 'shop-order', '".$returnArr['order_id']."', 'Changed Delivery Status: pending-dispatch','pending-dispatch',NULL,'$ip')";
					
					$sectrevw = query_execute("INSERT INTO section_reviews() VALUES('', 'BackGroundJob', 'shop-order', '".$returnArr['order_id']."', 'Changed Delivery Status: pending-dispatch','pending-dispatch',NULL,'$ip')");
				}
			}
			
			$emailCart.='<br><br><p>---------------------------------------------------------------------</p>';
		
			$rowOrdUser=query_execute_row("SELECT address_name, address_email , address_phone1 FROM shop_order_address WHERE order_id = '".$returnArr['order_id']."' AND address_type_id='1'");
			
			//Added By Umesh, Dated 17-12-2014,
			$order_query_cart = mysql_query("select si.name, si.nice_name, sc.mrp_price, sc.mrp_price-sc.item_price as discount, sc.item_price, sc.item_qty, sc.item_qty*sc.item_price as subtotal from shop_cart as sc, shop_items as si where si.item_id = sc.item_id AND sc.cart_order_id='".$returnArr['order_id']."'");
			
			$order_query_ship_amt = query_execute_row("SELECT order_shipment_amount, order_amount, item_shipping_amount, mode, order_discount_amount, order_method FROM shop_order WHERE order_id='".$returnArr['order_id']."'");
			
			$SQ_coupon = query_execute("SELECT amount, coupon_id FROM shop_order_coupon WHERE order_id='".$returnArr['order_id']."'");
			$ifCodeAppy = mysql_num_rows($SQ_coupon);
			$amount_coupon = 0;
			$save_amt = 0;
			while($getCoupon = mysql_fetch_array($SQ_coupon)){
				$coupon_id = $getCoupon['coupon_id'];
				$amount_coupon = $amount_coupon + $getCoupon['amount'];
				
				$SQ_coupon_save_amt = query_execute_row("SELECT save_amount FROM shop_coupon WHERE coupon_id='$coupon_id'");
				$save_amt = $save_amt + $SQ_coupon_save_amt['save_amount'];
			}
		
			$grand_total = 0;
			$grand_subtotal = 0;
			$grand_mrp = 0;
			$emailTemplate = "";
			//$emailTemplate = array();
			$numrow = mysql_num_rows($order_query_cart);
			while($order_result_row = mysql_fetch_array($order_query_cart)){ 
				$grand_total = $grand_total + $order_result_row['subtotal'];
				$total_discount= $total_discount + ($order_result_row['discount']*$order_result_row['item_qty']);
				$grand_mrp = $grand_mrp + ($order_result_row['mrp_price']*$order_result_row['item_qty']); 
				//$grand_subtotal = $grand_subtotal+ $order_result_row['subtotal'];              
										
				$emailTemplate .= '<tr style="font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#2d2d2d">
									<td height="30"></td>
									<td height="30"><a href="http://www.dogspot.in/'.$order_result_row['nice_name'].'" target="_blank" title="'.$order_result_row['name'].'"></a>'.$order_result_row['name'].'</a></td>
									<td height="30">'.number_format($order_result_row['mrp_price'],2).'</td>
									<td height="30">'.number_format($order_result_row['discount'],2).'</td>
									<td height="30">'.number_format($order_result_row['item_price'],2).'</td>
									<td height="30">'.$order_result_row['item_qty'].'</td>                        
									<td height="30">'.number_format($order_result_row['subtotal'],2).'</td>
								</tr>';
			}

			//End Email Template.........
			if($qselectCOD['successrate']==0){
				$cod_confirm='no';
			}
			else{
				$cod_confirm='yes';
			}

			$f_name=stripslashes($rowOrdUser["address_name"]);
			$email=$rowOrdUser["address_email"];
			
			$arrayEmail['emailType']='SHOP-ORDER';
			$arrayEmail['toName']=$f_name;
			$arrayEmail['toEmail']=$email;
			$arrayEmail['ship_state']=$ship_state;
			$arrayEmail['mod']=$mode;
			$arrayEmail['cod_confirm']=$cod_confirm;
			$arrayEmail['order_id']=$cart_order_id;
			$arrayEmail['emailCart']=$emailCart;
			$arrayEmail['emailAmount']=$emailAmount;
			$arrayEmail['totAmount']=$totAmount;
			
			$arrayEmail['ship_name']=$ship_name;
			$arrayEmail['ship_address1']=$ship_address1;
			$arrayEmail['ship_address2']=$ship_address2;
			$arrayEmail['ship_city']=$ship_city;
			$arrayEmail['ship_state']=$ship_state;
			$arrayEmail['ship_pin']=$ship_pin;
			
			$arrayEmail['dogspotId']=$userid;
			$arrayEmail['authorCode']=$author_code;
			$arrayEmail['AOrderMethod']=$AOrderMethod;
			
			// Email Template
			$arrayEmail['emailTemplate']=$emailTemplate;
			$total_discount = $total_discount + $order_query_ship_amt['order_discount_amount'];
			
			$arrayEmail['grand_mrp']=number_format($grand_mrp,0);
			
			$arrayEmail['total_discount'] = number_format($order_query_ship_amt[order_discount_amount],0);
			
			$arrayEmail['grand_total'] = number_format($order_query_ship_amt[order_amount],0);
			
			$arrayEmail['order_shipment_amount']=number_format($order_query_ship_amt[order_shipment_amount],0);
			
			$arrayEmail['item_shipping_amount']=number_format($order_query_ship_amt[item_shipping_amount],0);
			
			$arrayEmail['amount_coupon'] = $amount_coupon;
			
			$arrayEmail['save_amt'] = $save_amt;
			
			//$arrayEmail['grand_subtotal']=number_format($grand_subtotal,0);
			
			$arrayEmail['order_transaction_source'] = $order_transaction_source;
			
			$query = query_execute("SELECT * FROM section_reviews WHERE review_name like '%newmailsend%' AND review_section_id='".$returnArr['order_id']."'");
			
			$c = mysql_num_rows($query);
			
			if($cdonate == 0){
				if($c=='0'){
						$emailReturn = sendShopEmail($arrayEmail);
				}
			}
			else{
				
				$emailReturn = sendDonationEmail($arrayEmail);
			}
			
			if($userid != 'tradus'){		// No email if the userid is tradus
				// Check Email Status--------------------------------------------
				$rowOrdEmail = query_execute_row("SELECT review_body FROM section_reviews WHERE review_section_id = '".$returnArr['order_id']."' AND review_section_name='shop-order' AND review_body='orderResponseEmailSend'");
				
				//echo"<hr>".$rowOrdEmail["review_body"];
				if($rowOrdEmail["review_body"] != 'orderResponseEmailSend'){
					//echo "INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body, review_name, c_date, user_ip) VALUES ('$user_id', 'shop-order', '".$returnArr['order_id']."', 'orderResponseEmailSend', 'newmailsend', NULL, '$ip')";
					$resultinsertlog = query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body, review_name, c_date, user_ip) VALUES ('$userid', 'shop-order', '".$returnArr['order_id']."', 'orderResponseEmailSend', 'newmailsend', NULL, '$ip')");
				}
			}
			//echo "INSERT INTO shop_transactions (order_id, userid, session_id, order_transaction_id, order_transaction_amount, order_status, order_message, paymentid, mode, isflagged, datecreated, order_user_ip, conversion_type, 	conversion_data, user_browser, user_platform, user_agent) VALUES ('".$returnArr['order_id']."',  '$user_id', '$session_id', '', '$totAmount', '', '', '', '$payment_type', '', NULL, '$device_id', 'Shopping with App', '$session_id', 'Mobile App', 'Mobile App', 'Mobile App')";
			
			$resultinsertlog = query_execute("INSERT INTO shop_transactions (order_id, userid, session_id, order_transaction_id, order_transaction_amount, order_status, order_message, paymentid, mode, isflagged, datecreated, order_user_ip, conversion_type, 	conversion_data, user_browser, user_platform, user_agent) VALUES ('".$returnArr['order_id']."',  '$user_id', '$session_id', '', '$totAmount', '', '', '', '$payment_type', '', NULL, '$device_id', 'Shopping with App', '$session_id', 'Mobile App', 'Mobile App', 'Mobile App')");
		}
		else{
			$data = array(
				'status' => 'Failed to place order',
				'order_id' => $returnArr['order_id']
			);	
		}
	}
	else{
		$data = array(
			'status' => 'some parameter are missing',
			'order_id' => $returnArr['order_id']
		);	
	}
	
	$das = json_encode($data);
	print $das;
	$log_id = appLogPhoneGap($api_url, "Response",$log_id);
?>
