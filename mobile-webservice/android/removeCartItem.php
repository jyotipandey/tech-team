<?php
	/*
	*	Remove Items from Cart 
	*	Created By Umesh On dated: 12/1/2015 
	*/

	//$baseURL='E:/xampp/htdocs/dogspot_new';
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/new/shop/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	//require_once($baseURL.'/session.php');
	
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "removeCartItem.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);

	if($access_token){
		$getUsereId=base64_decode($access_token);	
		$vardata = explode('##',$getUsereId);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		
	}
	else{
		$user_id = "Guest";	
	}
	if($user_id!=''){
		if($remove='remove' && $cart_id!='' && $cart_id!='0' ){
			$sel_cart_details = query_execute_row("SELECT * FROM shop_cart WHERE cart_id = '$cart_id'");
			
			$remove_item_id = $sel_cart_details['item_id'];
			
			$sel_del_item_details = query_execute_row("SELECT * FROM shop_items WHERE item_id = '$remove_item_id'");
			
			$sel_del_item_name = $sel_del_item_details["name"];
			
			$sel_del_item_cat = $sel_del_item_details["child_cat"];
			
			$sel_del_item_brand = $sel_del_item_details["item_brand"];
			
			$sel_del_item_price = $sel_del_item_details["price"];
			
			$qParentCat = query_execute("SELECT category_name FROM shop_category WHERE category_id = '$sel_del_item_cat'"); //NKS
			
			$rowParentCat = mysql_fetch_array($qParentCat); 
			
			$sel_del_item_category = $rowParentCat["category_name"];
			
			$qBrand = query_execute("SELECT brand_name FROM shop_brand WHERE brand_id = '$sel_del_item_brand'"); //NKS
			
			$qBrand1 = mysql_fetch_array($qBrand); 
			
			$sel_del_item_brand_name = $qBrand1["brand_name"];
			
			if(!$donate){
				$donate=0;	
			}
			if($user_id!='Guest'){
				$qRemoveItemCart = query_execute("DELETE FROM shop_cart WHERE cart_id='$cart_id' AND userid = '$user_id' AND cart_order_status = 'new'");
				
				$no_deleted = mysql_affected_rows();
				
				$qGetMyshowCart1 = query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status,si.item_shipping_amount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$user_id' AND sc.cart_order_status = 'new' AND si.stock_status = 'instock' AND si.item_display_status != 'delete' AND ( sc.item_price != '0' OR sc.item_id = '1283' ) AND sc.donate_bag='$donate'");
			
			}else{
				
				if($session_id!='0' && $session_id!=''){
					$qRemoveItemCart=query_execute("DELETE FROM shop_cart WHERE cart_id='$cart_id' AND session_id = '$session_id' AND cart_order_status = 'new'");	
					$no_deleted = mysql_affected_rows();
				}
				 $qGetMyshowCart1 =query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, si.item_shipping_amount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status = 'instock' AND si.item_display_status != 'delete' AND sc.cart_order_status = 'new' AND (sc.item_price != '0' OR sc.item_id = '1283') AND sc.donate_bag = '$donate'");
			
			}
			if($no_deleted > 0){
				$data=array(
					'status' =>"$no_deleted item deleted successfully"
				);	
			}
			else{
				$data=array(
					'status' =>"Item not deleted"
				);
			}
			
		}else{
			$data=array(
				'status' =>"Some parameter missing"
			);
		}
		$das=json_encode($data);
		print $das;
		$return = appLogPhoneGap($api_url, "Response", $log_id);
}
?>