<?php
/**
* 
* @var $mode is a fixed parameter that is passed from two screen 
* 1- if mode=="addItemDirectlyFromList" then item is being added in wishlist from product listting screen 
* 2- if mode=="" then item is being passed from cart screen
* 
*/

$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');
require_once($baseURL.'/webservice/android/functions.php');
//require_once($baseURL.'/session.php');

	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "addWishlist.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);

if($access_token){
	$data=base64_decode($access_token);	
	$vardata = explode('##',$data);
	$user_id = $vardata [0];
	$device_id = $vardata [1];
	//get token and get userid from this token using algo...
	if($mode!="addItemDirectlyFromList"){
		$mode="addItemFromCart";
	}
	$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
	if($check_userid[cc]>0){
		if($item_id && $mode=="addItemFromCart"){
			$check_item = mysql_query("SELECT wishlist_id FROM shop_wishlist WHERE item_id = '$item_id' AND userid = '$user_id'");
			$count_rec = mysql_num_rows($check_item);
			$getItemList = mysql_fetch_array($check_item);
			if($count_rec > 0){
				$data=array(
					'access_token' =>$access_token,
					'status' => 'Item has been already added',
					'wishlist_id'=>$getItemList['wishlist_id']					
				);
			}
			else{				
				$wishlist = mysql_query("INSERT INTO shop_wishlist(userid,item_id,date) VALUES ('$user_id','$item_id',NULL)");						
				$wishlist_id = mysql_insert_id();				
				if($wishlist_id){
					//echo "DELETE FROM shop_cart WHERE userid='$user_id' AND item_id = '$item_id' AND cart_order_status = 'new'";
					$qRemoveItemCart = mysql_query("DELETE FROM shop_cart WHERE userid='$user_id' AND item_id = '$item_id' AND cart_order_status = 'new'");
					
					$data=array(
						'access_token' =>$access_token,
						'status' =>"Item has been added successfully",
						'wishlist_id' =>$wishlist_id
					);					
				}
			}
		}	
		elseif($item_id && $mode=="addItemDirectlyFromList"){
			$SQ_wishlist = mysql_query("SELECT wishlist_id FROM shop_wishlist WHERE item_id = '$item_id' AND userid = '$user_id'");
			$count = mysql_num_rows($SQ_wishlist);
			$getWishlistItem = mysql_fetch_array($SQ_wishlist);
			if($count > 0){
				$qRemoveItemCart = query_execute("DELETE FROM shop_wishlist WHERE wishlist_id='".$getWishlistItem['wishlist_id']."' AND item_id = '$item_id' AND userid='$user_id'");
				$data=array(
					'status' =>"Item has been deleted successfully"
				);
			}
			else{				
				$wishlist = mysql_query("INSERT INTO shop_wishlist(userid,item_id,date) VALUES ('$user_id','$item_id',NULL)");						
				$wishlist_id = mysql_insert_id();				
				if($wishlist_id){
					$data=array(
						'access_token' =>$access_token,
						'status' =>"Item has been added successfully",
						'wishlist_id' =>$wishlist_id
					);					
				}
			}
		}
		else{
			$data=array(
				'status' =>"Insufficient parameters"
			);
		}					
	}
	else{
		$data=array(
			'status' =>"Not a valid User"
		);	
	}
}
else{
	$data=array(
		'status' =>"Not a valid User Authentication"
	);
}
$das=json_encode($data);
echo $das;
$log_id = appLogPhoneGap($api_url, "Response",$log_id);
?>

