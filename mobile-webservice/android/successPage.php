<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/shop/functions.php');
	require_once($baseURL.'/session.php');
	require_once('constants.php');
	require_once($baseURL.'/shop/arrays/arrays.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	
	$api_url = "successPage.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);

if($access_token){
	$getUserId=base64_decode($access_token);
	$pos = strpos($getUserId, '##');
	if($pos != false){	
		$vardata = explode('##',$getUserId);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		//get token and get userid from this token using algo...

		$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
		if($check_userid[cc]>0){
			$check_order = query_execute_row("SELECT * FROM shop_order WHERE userid='$user_id' AND order_id='$order_id'");
		
			if($check_order['order_id']!=""){
				$c_date =	substr($check_order['order_c_date'],0,10);
				$c_date = date("jS F Y", strtotime($c_date));
				$order_method = $check_order['order_method'];
				$delivery_status = $check_order['delevery_status'];
				$order_id = $check_order['order_id'];
				$discount_amount = $check_order['order_discount_amount'];
				$data='';
				$shipping_price='';
				$item_data='';
				
				// Item 
				$check_order1 = mysql_query("SELECT so.*,sc.* FROM shop_order as so, shop_cart as sc WHERE so.order_id=sc.cart_order_id AND so.order_id='$order_id' AND so.mode != 'TEST' ORDER BY so.order_c_date DESC");
				$check_items = mysql_query("SELECT cart_id,item_id,item_qty,item_price FROM shop_cart WHERE cart_order_id = '$order_id'");
				
				while($getRow = mysql_fetch_array($check_items)){	
					$item_id = $getRow['item_id']; 	          	
					$item_desc = query_execute_row("SELECT name FROM shop_items WHERE item_id = '$item_id'");
					$item_data1 = array(
						'product_id' => $item_id,
						'item_id' => $item_id,
						'item_quantity' => $getRow['item_qty'],
						'item_description' => $item_desc['name'],											
						'item_price' => $getRow['item_price']*$getRow['item_qty']
					);
					$item_data[]=$item_data1;
				}
				//End Item
					
				if($check_order['item_shipping_amount'] !="0.0000"){
					$shipping_price = number_format($check_order['item_shipping_amount'],0);
				}else{
					$shipping_price = $shipping_charge;
				}

				if($check_order['order_status']=="0"){
					$status = "Your order has been placed successfully";
				}elseif($check_order['order_status']=="1"){
					$status = "Your order has not been placed";
				}else{
					$status = "Your order has not been completed";
				}
				
				if($order_method=="DC" || $order_method=="CC"||$order_method=="dc"||$order_method=="cc"||$order_method=="ccnb"||$order_method=="nb"||$order_method=="CCNB"||$order_method=="NB"){
					if($check_order['order_transaction_amount']!="0.0000" && $check_order['order_transaction_id']!=""){
						$txn_msg = "Your transaction has been successfull";
					}else{
						$txn_msg = "Your transaction has been failed";
					}
					$data_order=array(                     
						'order_id' => $order_id,
						'item_data'=>$item_data,
						'order_date' => $c_date,
						'shipping_price'=>$shipping_price,
						'discount_amount'=>number_format($discount_amount,0),
						'order_amount' => number_format(round($check_order['order_amount']),0),
						'payment_method'=>$AOrderMethod[$order_method],
						'status'=>$txn_msg,
						'status'=>$status
					);
				}
				if($order_method=="COD" || $order_method=="cod"||$order_method=="dd"||$order_method=="DD"){
					$data_order=array(                     
						'order_id' => $order_id,
						'item_data'=>$item_data,
						'order_date' => $c_date,
						'shipping_price'=>$shipping_price,
						'discount_amount'=>number_format($discount_amount,0),
						'order_amount' => number_format(round($check_order['order_amount']),0),
						'payment_method'=>$AOrderMethod[$order_method],
						'status'=>$status
					);
					if($order_method=="COD" || $order_method=="cod"){
						$cod_charges = $COD_charge;
						$data_order[cod_charge] = $cod_charges;
					}
				}
				$data[order_data]=$data_order;
			}	
		}
		else{
			$data=array(
				'status' =>"Not a valid User"
			);	
		}
	}
	else{
		$data=array(
			'status' =>"Access Token Not Valid!"
		);
	}
}
else{
	$data=array(
		'status' =>"Not a valid User Authentication"
	);
}
$das=json_encode($data);
echo $das;
$return = appLogPhoneGap($api_url, "Response", $log_id);
?>