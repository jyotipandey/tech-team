<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/session.php');
	require_once('constants.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "getMyCart.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);

	if($access_token){
		$dataUserid=base64_decode($access_token);	
		$vardata = explode('##',$dataUserid);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		
	}else{
		$user_id = "Guest";	
	}
	/*$arr_list = array(
		'API Name'=>"getMyCart.php",
		'access_token'=>$access_token,
		'date'=>date('Y-m-d h:i:s')
	);*/
	//params_error_log($arr_list);	
	if($user_id){				
		if($user_id=='Guest'){
			$check_cart = query_execute("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price,sc.item_discount,sc.item_totalprice FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$user_id' AND sc.session_id = '$session_id' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='0'");
		}else{
			$check_cart = query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$user_id' AND sc.cart_order_status =  'new' AND si.stock_status =  'instock' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283' ) AND sc.donate_bag='0'");
		}
		
		$data1 = array();
		$data = array();
		$grand_total = 0;
		//$shipping_charge = 100;
		$total_item = mysql_num_rows($check_cart);
		if(mysql_num_rows($check_cart) > 0){
			while($cart_rec = mysql_fetch_array($check_cart)){
				
				$SQ_shop_item = query_execute_row("SELECT item_id, payment_mode, name, price, selling_price, weight, item_parent_id,item_status,stock_status,item_display_status FROM shop_items WHERE item_id='".$cart_rec['item_id']."' AND stock_status='instock' AND item_display_status!='delete'");
				
				$grand_total = $cart_rec['item_totalprice']+$grand_total;
				
				if($SQ_shop_item["payment_mode"]!='cod'){
					$pmode='Not Available';
				}else{
					$pmode='Available';
				}
				
				if($SQ_shop_item['item_parent_id']=='0'){
					$media_item_id = $SQ_shop_item['item_id'];
				}else{
					$media_item_id = $SQ_shop_item['item_parent_id']; 
				}
				$rowdatM = query_execute_row("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id' AND position!='2' AND position!='3' AND position!='4'");
				
				$data1[] = array(                     
						'cart_id' => $cart_rec['cart_id'],
						'cart_qty' => $cart_rec['item_qty'],
						'item_id' => $cart_rec['item_id'],
						'item_name' => $SQ_shop_item['name'],
						'item_img' => "shop/item-images/thumb_".$rowdatM['media_file'],
						'item_original_img' => "shop/item-images/orignal/".$rowdatM['media_file'],
						'item_price' => $cart_rec['item_price'],
						'item_selling_price' => number_format($SQ_shop_item['selling_price'],0),
						'item_totalprice' => $cart_rec['item_totalprice'],
						'delivered_text'=>'Dispatched in 2-3 Days',
						'cash_on_delivery' =>$pmode
						
				);
				
			}
			$data = array(
				'cart_item' =>$data1,
				'shipping_text'=>'SHIPPING',
				'shipping_charges'=>$shipping_charge,
				'items_total'=>$grand_total,
				'cart_total'=>$grand_total+$shipping_charge,
				'total_items'=>$total_item,
				'userid'=>$user_id
			);
		}else{
			$data=array(
				'status' =>"Cart is Empty",
				'userid'=>$user_id
			);	
		}
	}
	$das=json_encode($data);
	echo $das;
	$log_id = appLogPhoneGap($api_url, "Response",$log_id);

?>