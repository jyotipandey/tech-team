<?php
	$baseURL='/home/dogspot/public_html';
	require_once($baseURL.'/database.php');
	require_once($baseURL.'/functions.php');
	require_once($baseURL.'/webservice/android/functions.php');
	require_once($baseURL.'/session.php');
	require_once('constants.php');
	require_once($baseURL.'/mobile-webservice/android/functions.php');
	$api_url = "getMyOrder.php"; 
	$log_id = "";
	$log_id = appLogPhoneGap($api_url, "Request",$log_id);

if($access_token){
	$dataUserId=base64_decode($access_token);
	$pos = strpos($dataUserId, '##');
	if($pos != false){	
		$vardata = explode('##',$dataUserId);
		$user_id = $vardata [0];
		$device_id = $vardata [1];
		//get token and get userid from this token using algo...

		$check_userid = query_execute_row("SELECT count(*) as cc From users_mobile_token WHERE userid='$user_id'");
		if($check_userid[cc]>0){
			$check_order = mysql_query("SELECT so.order_id FROM shop_order as so WHERE so.userid='$user_id' AND so.mode != 'TEST' AND so.order_status != 'inc' ORDER BY so.order_c_date DESC limit 15");
			$count = mysql_num_rows($check_order);
			if($count > 0){
				$data='';
				$cancel_status = "1";
				$track_status = "1";
				while($order_rec = mysql_fetch_array($check_order)){			          	
					$o_id[] = $order_rec['order_id'];
				}
				//print_r($o_id);
				foreach($o_id as $order_id){
					$shipping_price='';
					$item_data='';
					//$check_order1 = mysql_query("SELECT so.*,sc.* FROM shop_order as so, shop_cart as sc WHERE so.order_id=sc.cart_order_id AND so.order_id='$order_id' AND so.mode != 'TEST' ORDER BY so.order_c_date DESC");
					$check_items = mysql_query("SELECT cart_id,item_id,item_qty,item_price FROM shop_cart WHERE cart_order_id = '$order_id'");
					
					$order_status = query_execute_row("SELECT delevery_status, order_status, order_method FROM shop_order WHERE order_id = '$order_id'");
					
					$delivery_status = $order_status['delevery_status'];
					$order_status1 = $order_status['order_status'];
					$payment_mode1= $order_status['order_method'];
					
					if($order_status1=="0"){
						$order_status = "Success";
					}elseif($order_status1 == "1"){
						$order_status = "Unpaid";
					}elseif($order_status1 == "2"){
						$order_status = "Pending";
					}
					
					if(($order_status['delevery_status']=="new" || $order_status['delevery_status'] =="pending-dispatch") && $order_status['shop_courier_id'] =="NULL" || $order_status['shop_courier_id'] =="0" && $order_status['order_status']=='0'){
					
						$cancel_status = "1";
					}elseif($delivery_status=='canceled' || $delivery_status=='cancelled'){
						$delivery_status = "Cancelled";
						$cancel_status = "0";
						$track_status = "0";
					}
					
					
					
					while($getRow = mysql_fetch_array($check_items)){	
						$item_id = $getRow['item_id']; 	          	
						$item_desc = query_execute_row("SELECT name FROM shop_items WHERE item_id = '$item_id'");
						$item_data[] = array(
							'product_id' => $item_id,
							'item_id' => $item_id,
							'item_quantity' => $getRow['item_qty'],
							'item_description' => $item_desc['name'],											
							'item_price' => $getRow['item_price']*$getRow['item_qty']
						);
					}
					
					$shop_order = query_execute_row("SELECT item_shipping_amount,order_c_date,order_amount, order_discount_amount FROM shop_order WHERE order_id = '$order_id'");	
					if($shop_order['item_shipping_amount'] !="0.0000"){
						$shipping_price = number_format($shop_order['item_shipping_amount'],0);
					}else{
						$shipping_price = $shipping_charge;			//$shipping_charge define in the constants.php file
					}	
					if($payment_mode1=="COD" || $payment_mode1=="cod" || $payment_mode1=="Cod"){
						$data_order[]=array(                     
							'order_id' => $order_id,
							'order_date' => $shop_order['order_c_date'],
							'shipping_charges' => number_format($shipping_price,0),
							'discount_amount' => number_format($shop_order['order_discount_amount'],0),
							'order_amount' => number_format($shop_order['order_amount'],0),
							'item_data'=>$item_data,
							'order_status'=>$delivery_status,
							'delivery_status' =>$delivery_status,
							'cancel_flag'=>$cancel_status,
							'track_flag'=>$track_status,
							'COD'=>number_format($COD_charge,0)
						);
						
					}else{
						$data_order[]=array(                     
							'order_id' => $order_id,
							'order_date' => $shop_order['order_c_date'],
							'shipping_charges' => number_format($shipping_price,0),
							'discount_amount' => number_format($shop_order['order_discount_amount'],0),
							'order_amount' => number_format($shop_order['order_amount'],0),
							'item_data'=>$item_data,
							'order_status'=>$delivery_status,
							'delivery_status' =>$delivery_status,
							'cancel_flag'=>$cancel_status,
							'track_flag'=>$track_status,
							'COD'=>""
						);
					}	
					$data[order_data]=$data_order;
					$data[total_record]=$count;
				}	
			}else{
				$data=array(
					'status' =>"No Order Available"
				);
				$data[total_record]=$count;
			}	
		}
		else{
			$data=array(
				'status' =>"Not a valid User"
			);	
		}
	}
	else{
		$data=array(
			'status' =>"Access Token Not Valid!"
		);
	}
}
else{
	$data=array(
		'status' =>"Not a valid User Authentication"
	);
}
$das=json_encode($data);
echo $das;
$return = appLogPhoneGap($api_url, "Response", $log_id);
?>