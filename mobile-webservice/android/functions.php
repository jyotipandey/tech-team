<?
function getencode($userid,$device_id){
	$data = $userid.'##'.$device_id;
	return base64_encode($data);
	}

function array2json2($arr) { 
    if(function_exists('json_encode')) return json_encode($arr); //Lastest versions of PHP already has this functionality.
    $parts = array(); 
    $is_list = false; 

    //Find out if the given array is a numerical array 
    $keys = array_keys($arr); 
    $max_length = count($arr)-1; 
    if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {//See if the first key is 0 and last key is length - 1
        $is_list = true; 
        for($i=0; $i<count($keys); $i++) { //See if each key correspondes to its position 
            if($i != $keys[$i]) { //A key fails at position check. 
                $is_list = false; //It is an associative array. 
                break; 
            } 
        } 
    } 

    foreach($arr as $key=>$value) { 
        if(is_array($value)) { //Custom handling for arrays 
            if($is_list) $parts[] = array2json($value); /* :RECURSION: */ 
            else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */ 
        } else { 
            $str = ''; 
            if(!$is_list) $str = '"' . $key . '":'; 

            //Custom handling for multiple data types 
            if(is_numeric($value)) $str .= $value; //Numbers 
            elseif($value === false) $str .= 'false'; //The booleans 
            elseif($value === true) $str .= 'true'; 
            else $str .= '"' . addslashes($value) . '"'; //All other things 
            // :TODO: Is there any more datatype we should be in the lookout for? (Object?) 

            $parts[] = $str; 
        } 
    } 
    $json = implode(',',$parts); 
     
    if($is_list) return '[' . $json . ']';//Return numerical JSON 
    return '{' . $json . '}';//Return associative JSON 
} 

function params_error_log($param_list){
	$filename="error_log.txt";
	$i=1;
	foreach($param_list as $getParams=>$getValue){
		$current = file_get_contents($filename);
		$current .= $i."- ".$getParams." - ".$getValue."\n";
		file_put_contents($filename, $current);
		$i++;
	}
}

function appLogPhoneGap($url, $type,$params){
	/*// Insert all reuqest parameter and api request time 
	if($type=="Request"){
		$now = DateTime::createFromFormat('U.u', microtime(true));
		$now->setTimeZone(new DateTimeZone('Asia/Kolkata'));
		$reuqest_time= $now->format("Y-m-d h:i:s:u");

		$request_params1 = $_REQUEST;
		if(count($request_params1) > 0){
			$request_params=json_encode($request_params1);
		}
		else{
			$request_params1 = array("Request Parameter NIL");
			$request_params=json_encode($request_params1);	
		}
		$url = "/android/".$url;
		$SQ_logs = query_execute("INSERT INTO dogspot_app_logs(id, app_url, request_params, request_time, type_app)  VALUES('', '$url', '$request_params', '$reuqest_time',  'phonegap')");
		return mysql_insert_id();
		
	}// End
	
	// Update api response time
	if($type=="Response" && $params!=""){
		$now = DateTime::createFromFormat('U.u', microtime(true));
		$now->setTimeZone(new DateTimeZone('Asia/Kolkata'));
		$response_time= $now->format("Y-m-d h:i:s:u");
		
		$UQ_logs = query_execute("UPDATE dogspot_app_logs SET response_time = '$response_time' WHERE id='$params'");
		return mysql_affected_rows();
	}// End
	*/	
}

// api logs 
function app_logs($api_url, $type,$params){
	// Insert all reuqest parameter and api request time 
	/*if($type=="Request"){
		$now = DateTime::createFromFormat('U.u', microtime(true));
		$now->setTimeZone(new DateTimeZone('Asia/Kolkata'));
		$reuqest_time= $now->format("Y-m-d h:i:s:u");

		$request_params1 = $_REQUEST;
		if(count($request_params1) > 0){
			$request_params=json_encode($request_params1);
		}
		else{
			$request_params1 = array("Request Parameter NIL");
			$request_params=json_encode($request_params1);	
		}
		$url = "/android/native-api/".$api_url;
		$SQ_logs = query_execute("INSERT INTO dogspot_app_logs(id, app_url, request_params, request_time, type_app)  VALUES('', '$url', '$request_params', '$reuqest_time',  'android')");
		return mysql_insert_id();
		
	}// End
	
	// Update api response time
	if($type=="Response" && $params!=""){
		$now = DateTime::createFromFormat('U.u', microtime(true));
		$now->setTimeZone(new DateTimeZone('Asia/Kolkata'));
		$response_time= $now->format("Y-m-d h:i:s:u");
		
		$UQ_logs = query_execute("UPDATE dogspot_app_logs SET response_time = '$response_time' WHERE id='$params'");
		return mysql_affected_rows();
	}// End*/	
}
?>