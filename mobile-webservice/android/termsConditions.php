<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Terms and Conditions | DogSpot.in</title>
		<meta name="keywords" content="Terms and Conditions" />
		<meta name="description" content="Please read DogSpot Terms and Conditions before ordering or using information given on Site." />
	</head>
	<body>
		<div class="cont980 pageBody">
			<h1 style="text-align:left;">Terms &amp; Conditions</h1><br />
			<p style="line-height:20px;">Welcome to DogSpot.in. The following are the rules ("Terms") that govern use of the DogSpot.in Web site ("Site"). By using, adding information, or visiting the Site, you expressly agree that you have read, understood and are bound by the terms and conditions set out herein and all applicable laws and regulations governing the Site, including but not limited to any additional or amended terms or condition as applicable from time to time, regardless of how you subscribed to or use the services.</p>
			<p style="line-height:20px;">DogSpot.in or PetsGlam Services Pvt Ltd ("DogSpot"), whose registered office is 150 Ground Floor, Pocket-22, Sector-24, Rohini, New Delhi-110085, India. Our company registration number is U74999DL2011PTC217046, Delhi, reserves the right to change these Terms at any time, effective immediately upon posting on the site.</p><br />
			<p style="line-height:20px;"><strong>Authorized Site Visits</strong><br />
				You agree that you are only authorized to visit, view, and retain a copy of pages of this Site for your own personal use, and that you shall not duplicate, download, publish, modify, scrape, or otherwise distribute the material on this Site for any commercial use, or for any purpose other than as described in these Terms.</p><br />
			<p style="line-height:20px;"><strong> Registering with DogSpot.in</strong><br />
				In order to take advantage of certain features on the Site, you may be required to register with DogSpot. For example, you may be required to register with DogSpot if you want to set up a "My Profile" on DogSpot, where you can view a record of your contributions in the form of articles, ratings, blogs, feedback, get email alerts, etc.. You may also be required to register with DogSpot if you are a business owner, or want to respond to certain comments by users, or participate in any contribution towards the Site. DogSpot's use of any personal information that you provide to DogSpot.in during any registration process is governed by DogSpot's privacy policy.
			</p><br />
			<p style="line-height:20px;"><strong>Authentication Service</strong><br /> DogSpot may permit You to register for the Website   through certain third party services, such as Facebook Connect and Google   (&ldquo;Authentication Service&rdquo;). By registering for the Website using an   Authentication Service, you agree that DogSpot may access your account   information from the Authentication Service and you agree to any and all terms   of use of the Authentication Service regarding your use of the Website via the   Authentication Service. You agree that any Authentication Service is a Reference   Site and you are solely responsible for your interactions with the   Authentication Service as a result of accessing the Website through the   Authentication Service.</p>
			<br />
			<p style="line-height:20px;"><strong>DogSpot.in</strong> only  collect your Personal Information to conduct its business and to enable itself  to deliver and improve its Services.</p>
			<p style="line-height:20px;">DogSpot will only  disclose your Personal Information in accordance with this <a href="/shop-policy.php#privacy"><font color="0000FF">Privacy Policy.</font></a></p>
			<p style="line-height:20px;">If you decline to submit personal information  to us, then we will unfortunately not be in a position to provide the Services  to you.</p>
			<p style="line-height:20px;">Any of your information which you provide when  you use DogSpot.in in  an unencrypted manner and/or to an open, public environment or forum including  (without limitation) any blog, chat room, albums, community, classifieds or  discussion board, is not confidential, does not constitute Personal  Information, and is not subject to protection under Privacy Policy.</p>
			<p style="line-height:20px;">Since such public environments are accessible  by third parties, it is possible that third parties may collect and collate and  use such information for their own purposes. You should accordingly be careful  when deciding to share any of your Personal Information in such public  environments. <br />
				DogSpot is not  liable to you or any third party for any damages that you or any third party  may suffer howsoever arising from your disclosure of Personal Information in  any public environment. You accordingly disclose information in a public  environment at your own risk.</p><br />
			<p style="line-height:20px;"><strong>DogSpot Account</strong><br />
				If you register for a DogSpot account, you will be allowed to save and  personalize DogSpot content, receive, add, and view comments/dog-blog/ratings  from existing and future features provided by the DogSpot system, and post  ratings and articles about DogSpot businesses and other user articles. By  having an account with DogSpot, you agree to take full responsibility for  maintaining the confidentiality of your account user name, password, and all  related activity that occurs under your account user name. If you violate these  Terms, DogSpot may, at its sole discretion, terminate your accounts, remove or  modify any account-related content or access (including, but not limited to,  articles, and user profile information), or take any other action that DogSpot  believes is appropriate.</p><br />
			<p style="line-height:20px;"><strong>Access and Interference</strong><br />
				You agree that you will not use any robot, spider, other automatic device, or  manual process to monitor or copy our pages or the content contained thereon or  for any other unauthorized purpose without our prior expressed written  permission. You agree that you will not use any device, software, or routine to  interfere or attempt to interfere with the proper working of the Site. You  agree that you will not copy, reproduce, alter, modify, create derivative works  from, or publicly display any content (except for your own personal,  non-commercial use) from our Web site without the prior expressed written  permission of DogSpot.</p><br />
			<p style="line-height:20px;"><strong>Unauthorized Use of the Site</strong><br />
				Illegal and/or unauthorized uses of the Site, including, but not limited to,  unauthorized framing of or linking to the Site or unauthorized use of any robot,  spider, or other automated device on the Site, will be investigated and subject  to appropriate legal action, including, without limitation, civil, criminal,  and injunctive redress.</p>  <br />
			<p style="line-height:20px;"><strong>Violation of the Terms</strong><br />
				You agree that monetary damages may not provide a sufficient remedy to DogSpot  for violations of these terms of use and you consent to injunctive or other  equitable relief for such violations.</p>  <br />
  
			<p style="line-height:20px;"><a name="cw" id="disclaimer"></a></p>  
			<p style="line-height:20px;"><strong>Disclaimers</strong><br />
				Dogspot does not promise that the site will be inoffensive, error-free or uninterrupted, or that it will provide specific information from use of the site or any content, search, or link on it. The site and its content are delivered on an "as-is" and "as-available" basis. Dogspot cannot ensure that files you download from the site will be free of viruses or contamination or destructive features. Dogspot disclaims all warranties, express or implied, including any implied warranties of merchantability and fitness for a particular purpose. Dogspot will not be liable for any damages of any kind arising from the use of this site, including, without limitation, direct, indirect, incidental, and punitive and consequential damages.<br />
				<br />
				DogSpot disclaims any and all liability for the acts, omissions, and conduct of any third-party users, DogSpot users, advertisers, and/or sponsors on the Site, in connection with the Site, or otherwise related to your use of the Site. DogSpot is not responsible for the products, services, actions, or failure to act of any third party in connection with or referenced on the Site. Without limiting the foregoing, you may report the misconduct of users and/or third-party advertisers or service and/or product providers referenced on or included in the Site to DogSpot at legal@DogSpot.in. DogSpot may investigate the claim and take appropriate action, at its sole discretion.</p><br />
   
			<p style="line-height:20px;"><strong>Limitation of Laibility</strong><br />
				In no event shall DogSpot and/or its employee, affiliates  be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of DogSpot.in sites/services, with the delay or inability to use DogSpot.in sites/services or related services, the provision of or failure to provide services, or for any information, software, products, services and related graphics obtained through DogSpot.in sites/services, or otherwise arising out of the use of DogSpot.in sites/services, whether based on contract, tort, negligence, strict liability or otherwise, even if DogSpot.in or any of employees, affiliates had been advised of the possibility of damages</p>
			<p style="line-height:20px;">Any product or promotion of a product on the Service are not sponsored, endorsed or administered by, or in any other way associated with, Facebook. Facebook has no control over Content, Contributions or other information and opinion expressed via the Service.<br />
			</p><br />
			<p style="line-height:20px;"><strong>Disputes</strong><br />
				If there is any dispute about or involving the Site, you, by using the Site, agree that the dispute will be governed by the laws of the Republic of India without regard to its conflict-of-law provisions. You agree to personal jurisdiction by and venue in New Delhi, in the state of Delhi, India.</p><br />
			<p style="line-height:20px;"><strong>Posting Profiles, Articles, and Blogs  on DogSpot.in</strong><br />
				DogSpot reserves the right, but not the obligation, to refuse to post or to remove any Profile, Comment, or Blog if it contains or features any of the following:</p><br />
			<p style="line-height:20px;"><strong>Unacceptable Content:</strong></p>
			<ul>
				<li style="line-height:20px;">Offensive, harmful and/or abusive language, including without limitation: expletives, profanities, obscenities, harassment, vulgarities, sexually explicit language and hate speech (e.g., racist/discriminatory speech.)</li>
				<li style="line-height:20px;"> Articles that do not address the pet community or articles with no qualitative value (e.g., "this kennel's pets are great!") as determined by DogSpot in its sole discretion.<br /></li>
				<li style="line-height:20px;"> Articles which comments on other users.<br /></li>
				<li style="line-height:20px;"> Content that contains personal attacks or describes physical confrontations and/or sexual harassment.<br /></li>
				<li style="line-height:20px;"> Excessive damage or death of a puppy bought from a particular kennel club and related cases caused by a business or service to person or property.<br /></li>
				<li style="line-height:20px;"> Personal information or messages including email addresses, URLs, phone  numbers and postal addresses.<br /></li>
				<li style="line-height:20px;"> Messages that are advertising or commercial in nature, or are inappropriate  based on the applicable subject matter.<br /></li>
				<li style="line-height:20px;"> Language that violates the standards of good taste or the standards of this  website, as determined by DogSpot in its sole discretion.<br /></li>
				<li style="line-height:20px;"> Content determined by DogSpot to be illegal, or to violate any central,  state, or local law or regulation or the rights of any other person or entity.<br /></li>
				<li style="line-height:20px;"> Language intended to impersonate other users (including names of other  individuals) or offensive or inappropriate user names or signatures.<br /></li>
				<li style="line-height:20px;"> Articles which scrape or re-purpose other people's content without  permission.</li>
  
				<li style="line-height:20px;">Protected by copyright or trademark and used, in any manner on Dog.Spot.in without the permission of the author or the owner;</li></ul>
			<br />
    
			<p style="line-height:20px;"> <strong>Defamatory; illegal; hateful; pornographic; or harmful</strong></p>
			<p style="line-height:20px;"><strong>Note:</strong>We are not able to police every article we  include on Dogspot, so we will make mistakes. If we've approved an article on  our site which you feel is misappropriating your content, please let us know at <strong>contact@dogspot.in.</strong><br />
				DogSpot team will exercise its discretion on such subjects based on statistics, user feedback.</p><br />
			<p style="line-height:20px;"><strong>Copyright</strong></p>
			<ol>
				<li>You may not reproduce, publish, perform, broadcast, make an adaptation of, sell, lease, offer or expose any copy of any Content in respect of which we own the copyright without our consent, or in the case of the Content of a third party author, without his or her consent.</li>
				<li>You acknowledge that we own the right, title and interest in and to the Services developed and provided by us, the system which provides the Services and all software associated with the Services, as well as all Intellectual Property Rights thereto.</li>
				<li>You will comply with all national and international laws pertaining to Intellectual Property Rights.</li>
			</ol>
			<br />	
			<p style="line-height:20px;"><strong>A. Your content</strong></p>
			<ol>
				<li>You will  retain ownership of any original content that you provide when using a Service,  including any text, data, information, images, photographs, music, sound, video  or any other material which you may upload, transmit or store when making use  of our Service.</li>
				<li>However, we own all compilations, collective works or derivative works  created by us which may incorporate your content and which are reduced to a  material form and original; and with regards to content which you may upload or  make available for inclusion on publicly accessible areas, you grant us an  irrevocable, perpetual, worldwide and royalty-free right and license to use,  publicly display, publish, publicly perform, reproduce, distribute, broadcast,  adapt, modify and promote your content on any medium.</li></ol>
			<p style="line-height:20px;"><strong>B. </strong>Should you be of the view that the  Intellectual Property Rights in any of your works uploaded on the Service have  been infringed or otherwise violated, please provide our Webmaster with the  following information:</p>
			<ul>
				<li style="line-height:20px;">A description of the work, which you claim, has been infringed;</li>
				<li style="line-height:20px;">The location of the work on the ibibo.com site;</li>
				<li style="line-height:20px;">Your contact details;</li>
				<li style="line-height:20px;">An affidavit deposed to by you stating that the work was used without your consent;</li>
				<li style="line-height:20px;">Written consent  if an agent is acting on your behalf.</li>
				<li style="line-height:20px;">The level of  attention to be afforded to your matter will lie within our discretion.</li>
			</ul>
			<p style="line-height:20px;">This clause should in no way be construed as a  guarantee that we will assist you under these circumstances.</p><br />
			<a name="cw" id="disclaime"></a>
			<p style="line-height:20px;"><strong>Promotional Code Terms & Conditions:</strong></p>
			<ul>
				<li style="line-height:20px;">	Coupon/Gift Voucher is issued at the sole discretion of PetsGlam Services Private Limited (company).</li>
				<li style="line-height:20px;">	The company retains the right to revoke, modify and/or alter them as it may deem fit.</li>
				<li style="line-height:20px;">	Only one Coupon/Gift Voucher can be used against one particular order.</li>
				<li style="line-height:20px;">	Coupon usage is restricted to certain products and/or categories and is subject to change from time to time.</li>
				<li style="line-height:20px;">	The company will issue coupons from time to time under various promotions. Members who are eligible for these coupons will be intimated about the same via email, text alert and/or on site messaging.</li>
				<li style="line-height:20px;">	All coupons will expire after a set duration and can be used as per the usage defined. This information would be communicated to members.</li>
				<li style="line-height:20px;">	Any remainder Coupon amount will lapse.</li>
				<li style="line-height:20px;">	Action(s) intended to fraudulently gain Coupons under any promotional scheme or otherwise will result in cancellation of Coupon/Order and/or termination of account.</li>
				<li style="line-height:20px;">	Any issue(s) arising with redemption of Coupons will be dealt on a case to case basis. The final decision rests with the company.</li>
				<li style="line-height:20px;">	Gift Voucher can be redeemed against purchase(s) on site in full. Any unused amount will be returned back to the account.</li>
				<li style="line-height:20px;">	This policy is subject to change from time to time without prior notices.</li>
				<li style="line-height:20px;">	In case of any query pertaining to use of Coupon and/or Gift Voucher, please email customer care on <a href="mailto:contact@dogspot.in">contact@dogspot.in</a>.</li></ul>

			<p style="line-height:20px;"><strong>Note:</strong> The terms related to promo code can be changed by Dogspot.in anytime.</p>
			<p style="line-height:20px;"><strong>Free Creature Companion - Terms and Condition</strong></p>
			<ul>
				<li style="line-height:20px;">This scheme only applies to food packs weighing 3 KG and above.</li>
				<li style="line-height:20px;"> The "food packs" refers to: dry dog food and dry cat food packets only.</li>
				<li style="line-height:20px;">These copies are sample editions only</li>
			</ul>
  
			<p style="line-height:20px;"><strong>For Promotion Code: WLCM1</strong></p>
			<ul>
				<li style="line-height:20px;">The code is only applicable for new customers only. (those who have never shopped with DogSpot earlier).</li>
				<li style="line-height:20px;">The code is not applicable on cart with already discounted items.</li>
				<li style="line-height:20px;">Code is applicable on a minimum purchase of Rs 500 and above.</li>  
			</ul>
			<p style="line-height:20px;"><strong>Release On Delivery(ROD)- Terms and Condition</strong></p>
			
			<ul>
				<li style="line-height:20px;">You will also save Rs 50, which is charged when you pay through COD.</li>
				<li style="line-height:20px;">To avail of these benefits, you would need to create a PayUPaisa account.</li>   
			</ul>
			<p style="line-height:20px;"><strong>Product Recommendation- Terms and Condition</strong></p>
			<ul>
				<li style="line-height:20px;">All recommendations by the DogSpot team are presented in good faith and believed to be correct to the best of our knowledge. DogSpot.in requests the customer to use his/her own discretion while making a purchase.</li>
			</ul>
			<p style="line-height:20px;"><strong> Additional Terms & Conditions for the Finikee Offer</strong></p>
			<ul>
				<li style="line-height:20px;">The offer is valid only on order for Dry Cat Food.</li>
				<li style="line-height:20px;">The offer is valid till stock lasts.</li>
				<li style="line-height:20px;">DogSpot doesn't promise the flavor of the product the customer will receive and will in no case replace the free product.</li> 
				<li style="line-height:20px;">A customer will get only one sample per order, irrespective of the number of Dry Cat food packs ordered.</li>
			</ul>
			<p style="line-height:20px;"><strong> Additional Terms & Conditions for the N&D Offer</strong></p>
		
			<ul>
				<li style="line-height:20px;">The offer is valid only on order of Natural & Delicious Chicken & Pomegranate variants.</li>
				<li style="line-height:20px;">The offer is valid till stock lasts.</li>
			</ul>
			<p style="line-height:20px;"><strong> Gurgaon Special Offer on all products</strong></p>
			<ul>
				<li style="line-height:20px;">This offer only applies to all products</li>
				<li style="line-height:20px;">The 24 hour delivery is only applicable for orders placed on Business Days (Monday to Friday- till 6 pm). Our Business Days exclude Saturday, Sunday and all Public Holidays.</li>
				<li style="line-height:20px;">Standard Shipping charges applicable for 24 hour delivery.</li> 
				<li style="line-height:20px;">DogSpot reserves the right to cancel any such order without any prior intimation to the customer.</li>
				<li style="line-height:20px;">DogSpot reserves the right to withdraw the offer at any given point of time.</li>
			</ul>
			<p style="line-height:20px;"><strong>Terms and Condition for Gurgaon Offer:</strong></p>
			<ul>
				<li style="line-height:20px;">This offer applies to all products except the ones which are already on discount.</li>
				<li style="line-height:20px;">24 hour delivery is applicable on orders placed on Business Days subject to product's availability.</li>
				<li style="line-height:20px;">Same Day Delivery is applicable to orders placed before 12 pm on Business Days and subject to product's availability.</li> 
				<li style="line-height:20px;">Business Day are Monday to Friday- till 6 pm excluding Saturday, Sunday and all Public Holidays.</li>
				<li style="line-height:20px;">Standard Shipping charges applicable.</li>
				<li style="line-height:20px;">DogSpot reserves the right to cancel any such order without any prior intimation to the customer.</li>
				<li style="line-height:20px;">DogSpot reserves the right to withdraw the offer at any given point of time.</li>
			</ul>

			<p style="line-height:20px;"><strong>Terms and Condition for Donation:</strong></p>
			<ul>
				<li style="line-height:20px;">Tax Rebate Policy- When you donate through our website, you shall NOT be eligible for any benefits like tax rebate..</li>
				<li style="line-height:20px;">Contribution from DogSpot.in: DogSpot will now match the quantity you choose to donate i.e if you choose 1 packet of dog food, 2 packets will be donated to the concerned person.</li>
				<li style="line-height:20px;">No schemes/discounts available- The ongoing schemes/discounts will not be applicable on donation items.</li> 
				<li style="line-height:20px;">Refund policy- If you make an error in your donation please contact us either by email at care@dogspot.in or by phone at 91-9212196633 within 24 hours  and a full refund will be made to you.</li>
				<li style="line-height:20px;">Any item which is once used by the customer, will not be replaced by the seller.</li>
			</ul>
			<p style="line-height:20px;"><strong>Terms and Conditions for Adoption:</strong></p>
			<ul>
				<li style="line-height:20px;">Spay/Neuter a must for all pets after adoption within a span of 15 days and the family is required to present us a proof of the same.</li>
				<li style="line-height:20px;">There should be someone at home to take care of the pet and he/she should not be completely left to the care of domestic help. Though this can be discussed and considered for up till 4 hours day depending on the animal up for adoption.</li>
				<li style="line-height:20px;">The adopted animal should not be kept tied up at any time for long periods of time.</li> 
				<li style="line-height:20px;">House checks are done prior to giving up pets for adoption. Even after adoption, house checks can be done anytime with our without prior notice. The adopter and his/her family's co-operation are mandatory during such visits.</li>
				<li style="line-height:20px;">Outstation adoptions are considered only in special cases where we might have a trustee said city to do house check and monitor adoption.</li>
				<li style="line-height:20px;">In case of farmhouse owners, the family should live at the farmhouse and not leave the animal in the care of employees (except for special cases of guard dog adoptions where this guideline can be reconsidered).</li>
				<li style="line-height:20px;">You need to have a backup plan when you plan to travel like pet boarding and should be willing to bare the costs involved.</li>
				<li style="line-height:20px;">At any point of time if the family is not able to continue keeping the pet. We should be notified immediately. In case we find out the animal has been abandoned/mistreated, legal action might be initiated against the family.</li>
			</ul>
			<p style="line-height:20px;"><strong><a id="tcoffer">Terms and Condition for Offer period:</a></strong></p>
			<ul>
				<li style="line-height:20px;">Any purchase made during a sale or discount period has a 'No Refund' and 'No Exchange' policy.</li>
				<li style="line-height:20px;">DogSpot is not liable for any faulty or incorrect order made by the customers in terms of size, colour or any other product specification.</li>
				<li style="line-height:20px;">Any operational errors in terms of dispatch and delivery form our end will be complied by DogSpot.in</li>
				<li style="line-height:20px;">DogSpot.in reserves all rights to change and or cancel the offer without prior intimation.</li>
				<li style="line-height:20px;">DogSpot.in reserves right to cancel order without being obliged to give any reason.</li> 
			</ul>
			<hr /> 
		</div>
	</body>
</html>