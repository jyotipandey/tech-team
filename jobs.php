<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();
 if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/jobs-bootstrap.php');
  exit;
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="canonical" href="https://www.dogspot.in/jobs.php" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Careers & Jobs at DogSpot.in</title>
<meta name="keywords" content="Careers & Jobs at DogSpot.in" />
<meta name="description" content="DogSpot is a niche organisation that breathes the culture of work hard and play hard. We’re a close knit team that is based on the pillars of trust and transparency. A bunch of young, fun loving and passionate people we support each other to continually develop new skills and knowledge. Careers & Jobs at DogSpot.in" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>

<link href="/jobs/css/default.css" rel="stylesheet" type="text/css" />
<link href="/jobs/css/nivo-slider.css" rel="stylesheet" type="text/css" />
<link href="/jobs/css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/jobs/js/jquery.nivo.slider.js"></script>
<script type="text/javascript">
$(window).load(function() {
   $('#slider').nivoSlider();
 });
</script>
<style>

.senior_developor {
    box-sizing: border-box;
    float: left;
    padding: 0px 0px 0;
    width: 100%;
}
.job_position strong {
    font-size: 18px;
    color: #585858;
    font-weight: 400;
    width: 8%;
    float: left;
    line-height: 49px;
}
.job_position .job_profile {
    line-height: 48px;
    font-size: 19px;
    width: 50%;
    color: #585858;
    font-weight: 400;
}
.job_Respons, .job_requirement {
    font-size: 14px;
    clear: both;

    width: 100%;
}
.job_Respons strong, .job_requirement strong, .job_optional strong {
    font-size: 16px;
    float: left;
    color: #585858;
    font-weight: 400;
    width: 100%;
	    margin-top: 10px;
    margin-bottom: 10px;
}
.job_Respons ul,  {
    float: left;
    width: 100%;
    padding-left: 30px;
    margin-top: 5px;
}
.job_requirement ul{ float: left;
    width: 100%;
   
    margin-top: 5px;}
.job_Respons ul li, .job_requirement ul li {
    line-height: 25px;
    list-style: square;
    text-align: left;
}
.pageBody ul li{ margin-top:5px; margin-left: 45px;}
</style>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="vs20"></div>
<div class="cont980 pageBody">

<h1 style="font-size:16px; float:left; width:100%;     margin-top: 20px;
    font-weight: normal;">CURRENT OPENINGS AT DOGSPOT</h1>

   <?php /*?><table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" >
      
     
      <tr>
        <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            <td width="50%" valign="top"><div style="border-radius:7px; border:1px solid #999; height:105px; padding:5px; text-align:left;">
            <div style="float:left;" > <h3 style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:16px; margin-bottom:5px;">Customer Relationship Associate</h3> </div> <div style="float:right; background: url(/new/pix/bg-transparent.png) repeat;width:74px;"  ><a  href="/jobs/customer-relationship-associate.php">View & Apply</a></div>
              <div class="cb"></div>
              
              <p  class="pageBody">Become the face of our organization and a primary contact for our widespread customer community range.Resolve all customer query patiently and efficiently.</p>
             </div></td>
            
            <td width="50%" valign="top"><div style="border-radius:7px; border:1px solid #999; height:105px; padding:5px; text-align:left;">
            <div style="float:left;" > <h3 style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:16px; margin-bottom:5px;">PHP Developer</h3>
            </div> <div style="float:right; background: url(/new/pix/bg-transparent.png) repeat;width:74px;"  ><a  href="/jobs/php-developer.php">View & Apply</a></div>
              <div class="cb"></div>
              
            
              <p class="pageBody">The individual should have an extensive experience of core PHP Coding & an attitude towards continual learning and growth. One who is a self-starter, takes responsibility for tasks asigned and also a quick learner. <br />
              </p>
              </div>              
            </td>
            
          </tr>
          <tr>
            <td width="50%" valign="top"><div style="border-radius:7px; border:1px solid #999; height:105px; padding:5px; text-align:left;">
           <div style="float:left;" >  <h3 style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:16px; margin-bottom:5px;">Android Developer</h3>
           </div> <div style="float:right; background: url(/new/pix/bg-transparent.png) repeat;width:74px;"  ><a  href="/jobs/Android-developer.php">View & Apply</a></div>
              <div class="cb"></div>
              
           
              <p class="pageBody">The individual should drive development using the latest mobile standards & Android Application Architecture. One who is a self-starter,takes responsibility for participate in all phases of our software development lifecycle in order to convert concepts and specifications into released Android applications and features. </p>
              
             </div></td>
             
            <td width="50%" valign="top" ><div style="border-radius:7px; border:1px solid #999; height:105px; padding:5px; text-align:left;">
             <div style="float:left;" >  <h3 style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:16px; margin-bottom:5px;">Assistant Manager - Operations</h3>
             </div> <div style="float:right; background: url(/new/pix/bg-transparent.png) repeat;width:74px;"  ><a  href="/jobs/manager.php">View & Apply</a></div>
              <div class="cb"></div>
              
              
              <p class="pageBody">Take ownership to Dispatch and Deliver the orders as received from the customers. Adhere to Delivery and Dispatch SLAs and execute smooth fulfillment of orders by coordinating with logistics partners.</p>
                         
            </div></td>
            
          </tr>
        </table></td>
      </tr>
      
    </table></td>
  </tr>
</table><?php */?>
<!--<div style="margin-top:2px; margin-bottom:2px;">Thanks for your inclination in working with us. We do not have any open positions currently.</div>-->


<!-- job position-->
<div class="senior_developor" id="senior_job">
<div class="job_position"> 
<div class="job_profile"> Position: Telesales Executive</div>
</div>
<div style="font-size:14px;">Location - Gurgaon</div>
<div style="font-size:14px; margin-top:10px;">Nature - Full Time</div>
<div style="font-size:14px; margin-top:10px;">Salary - At par with industry standards</div>
<div class="job_Respons"><strong>Responsibilities:</strong>
<ul style="float:left;">
<li>
Contact existing customers to inform them about RC products
</li>
<li>Gather information about their pet and their eating habits</li>
<li>Answer questions about products and their benefits</li>
<li>Ask questions to understand customer requirements</li>
<li>Enter and update customer information in the database</li>
<li>Take and process orders in an accurate manner</li>
<li>Handle objections or grievances if any</li>
<li>Go the "extra mile" to meet sales quota and facilitate future sales</li>
<li>Keep records of calls and sales and note useful information</li>
</ul>

</div>
<div class="job_requirement"> <strong>Requirements:</strong>
<ul>
<li>Pet owner</li>
<li>Proven experience as telesales representative or other sales role</li>
<li>Proven track record of successfully meeting sales quota preferably over the phone</li>
<li>Good knowledge of relevant computer programs (e.g. CRM software, MS Excel) and telephone systems</li>
<li>Ability to learn about products and services and describe/explain them to prospects</li>
<li>Excellent knowledge of English</li>
<li>Excellent communication and interpersonal skills</li>
<li>Cool-tempered and able to handle rejection</li>
<li>Outstanding negotiation skills with the ability to resolve issues and address complaints</li>
</ul>
</div>


<div class="apply_button" style="margin-bottom:20px; float:left;   font-size:14px; margin-top:10px;">Please email your Resume to <a href="mailto:meghna@dogspot.in">meghna@dogspot.in</a></div>
</div>
<!-- job position end-->



  <div class="vs10"></div>
  <h3 style="color:#4B3A15; font-size:16px;font-weight: normal; " >WHY DOGSPOT?</h3>
 
  <p class="pageBody " style="font-size:14px">
  DogSpot is a niche organisation that breathes the culture of work hard and play hard. We’re a close knit team that is based on the pillars of trust and transparency.  A bunch of young, fun loving and passionate people we support each other to continually develop new skills and knowledge. 
  
  </p>
  
  <p class="pageBody " style="font-size:14px">
 Our founders - Rana, Shalesh and Vizal are pet lovers that bring to us years of experience to guide us at every stage and make every moment enjoyable. DogSpot gives you the rare chance to work for your fervour for pets within an environment that helps you strive for the best and gives you endless opportunities to learn.  
  </p>
  
  <h3 style="color:#4B3A15; font-size:16px; font-weight: normal;">MEET OUR TEAM</h3>
  <div class="vs10"></div>
  <div id="photoslider" style="margin-bottom:20px;">
    <div class="slider-wrapper theme-default">
      <div id="slider" class="nivoSlider" style="height:400px">
       <img src="/jobs/images/slider-08.jpg" data-thumb="/jobs/images/slider-08.jpg"  />
      <img src="/jobs/images/slider-02.jpg" data-thumb="/jobs/images/slider-02.jpg" />
       <img src="/jobs/images/slider-03.jpg" data-thumb="/jobs/images/slider-03.jpg" />
        <img src="/jobs/images/slider-04.jpg" data-thumb="/jobs/images/slider-04.jpg" />
        <img src="/jobs/images/slider-05.jpg" data-thumb="/jobs/images/slider-05.jpg"  />
        <img src="/jobs/images/slider-06.jpg" data-thumb="/jobs/images/slider-06.jpg"  />
        <img src="/jobs/images/slider-07.jpg" data-thumb="/jobs/images/slider-07.jpg"  />
      
        <img src="/jobs/images/slider-09.jpg" data-thumb="/jobs/images/slider-09.jpg"  />
        <img src="/jobs/images/slider-10.jpg" data-thumb="/jobs/images/slider-10.jpg"  />
        <img src="/jobs/images/slider-11.jpg" data-thumb="/jobs/images/slider-11.jpg"  />
        <img src="/jobs/images/slider-12.jpg" data-thumb="/jobs/images/slider-12.jpg"  />
        
        <img src="/jobs/images/slider-14.jpg" data-thumb="/jobs/images/slider-14.jpg"  />
        
        <img src="/jobs/images/slider-16.jpg" data-thumb="/jobs/images/slider-16.jpg"  />
        <img src="/jobs/images/slider-17.jpg" data-thumb="/jobs/images/slider-17.jpg"  />
        <img src="/jobs/images/slider-18.jpg" data-thumb="/jobs/images/slider-18.jpg"  />
       
     </div>
    </div>
  </div>
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-2.php'); ?>
