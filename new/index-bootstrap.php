<?php
  // header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + 36000)); 
   require_once($DOCUMENT_ROOT.'/database.php');
   require_once($DOCUMENT_ROOT.'/functions.php');
   require_once($DOCUMENT_ROOT . '/constants.php');
   require_once($DOCUMENT_ROOT . '/session.php');
   require_once($DOCUMENT_ROOT . '/shop/functions.php');
   require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys.php');
   require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys-nice.php');

  if(!$section[0]){
   $image_session_array = "";
   $video_session_array = "";
   $BkUrl = "/";
   $BkUrlVideo = "/";
   session_register("BkUrl");
   session_register("BkUrlVideo");
   }
   if ($logoff == "logoff"){
   if(isset($_COOKIE['cookid']) && isset($_COOKIE['cookLevel']) && isset($_COOKIE['cookName'])){
      $cookieTime = time()-60*60*24*100;
      setcookie("cookid", $sessionuserid, $cookieTime, "/");
      setcookie("cookLevel", $sessionLevel, $cookieTime, "/");
      setcookie("cookName", $sessionName, $cookieTime, "/");
      setcookie("cookProfileImg", $sessionProfileImg,   $cookieTime, "/");
      setcookie("cookFacebook",   $sessionFacebook,   $cookieTime, "/");
      setcookie("cookTwitter",   $sessionTwitter,   $cookieTime, "/");
      setcookie("cookSocialaction",   $sessionSocialaction,   $cookieTime, "/");
   }
   session_regenerate_id();
   session_destroy();
   unset($_SESSION);
   session_start();
   ?>
   <script>(_bout.push(["logoutUser"]));</script><?
   removeOfflineUser($userid); //remove Offline User details
   
   // refresh the page
   header ("Location: /"); 
   ob_end_flush();
   exit(); 
   }
   $sitesection = "HOME";
   $ajaxbody = "HOME";
   
   // TOP PHP-----------------------------------------------------------
   if($sitesection != "photos"){
   $image_session_array = "";
   $BkUrl = "";
   }
    // Record user
   $display_user_Name = dispUname($userid);
   $ip = ipCheck();
   // TOP PHP------------------------------------------------------------
   if($userid!='Guest'){
   $rowusers=query_execute_row("SELECT count(*) as jee FROM users WHERE userid='$userid' AND city='Gurgaon'");
   $rowItemw=query_execute("SELECT address_zip FROM shop_order_address WHERE userid='$userid' AND address_type_id='1' order by order_address_id desc");
   while($rows = mysql_fetch_array($rowItemw)){
   	
   if($rows['address_zip']!='' || $rows['address_zip']!='0'){
   	$zipp=$rows['address_zip'];
   	$raddress = query_execute_row("SELECT count(*) as coo FROM shop_courier_pincodes WHERE pincode='$zipp' and city='GURGAON'");
   	if($raddress['coo']!=0){
   	$displaypin='0';break;
   	}
   	}
   }}
   ?>
<?
    $title="DogSpot.in-Online Pet Supplies Store | Shop for Dog,Cat,Birds Products";
	$keyword="DogSpot.in, Online Pet Supplies Store, Dog Store, Cat shop, Birds products, pet shopping India, puppies shop, cat supplies, dog shop online.";
    $desc="DogSpot.in Pet Supplies Store - India's best online shopping site for pets supplies, accessories & products for pets. Shop for Dog, Cat, Birds and small pets at great price from anywhere.";
	$alternate="https://m.dogspot.in/";
	$canonical="https://www.dogspot.in/";
	$og_url="https://www.dogspot.in/";
	$imgURLAbs="https://www.dogspot.in/new/breed_engine/images/logo-300x300.jpg";
	$page_type='index-home';
    require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');	  
   ?>
<?php /*?>   banner slider start <?php */?>

<section  id="demo">
  <div id="owl-banner-slider" class="owl-carousel">
    <? $s=0;
                     $tdate=date("Y-m-d H:i:s");
					 //echo "SELECT * FROM banners WHERE featured_on='home' AND width='1366' AND height='417' AND start_date <= '$tdate' AND end_date > '$tdate' AND status='active' ORDER BY banner_order ASC";
                     $sqlbanner=query_execute("SELECT * FROM banners WHERE featured_on='home' AND width='1366' AND height='417' AND start_date <= '$tdate' AND end_date > '$tdate' AND status='active' ORDER BY banner_order ASC");
                     while($getbanner=mysql_fetch_array($sqlbanner)){
                     	$banner_name=$getbanner['banner_name'];
                     	$img=$getbanner['img'];
                     	$banner_link=$getbanner['banner_link'];
						$banner_order=$getbanner['banner_order'];?>
    <div class="item"> <a href="https://www.dogspot.in/<?=$banner_link?>"> <img src="https://ik.imagekit.io/2345/allbanners/<?=$img?>" alt="<?=$banner_name?>" width="1366" height="417" data-transition="slideInLeft"  data-thumb="/allbanners/<?=$img?>" /></a> </div>
    <? }?>
	
  </div>
</section>
<?php /*?>   banner slider end <?php */?>

<?php /*?>free shipping sec start<?php */?>
<section class="freeshiping-container text-center">
  <div class="container">
    <div class="row">
    <? $s=0;
         $sqltag=query_execute_row("SELECT * FROM deals WHERE featured_on='exclusive' AND status='active' ORDER BY c_date DESC LIMIT 1");
      ?>
      <div class="freeshiping-text"><a href="https://www.dogspot.in/sales/" style="text-decoration: none;color: #333;"><?=$sqltag['show_txt']?> </a></div>
     <?php /*?> <div class="tmc-box">*terms and conditions apply</div><?php */?>
    </div>
  </div>
</section>
<?php /*?> free shipping sec end<?php */?>
 <?php /*?>shop by category<?php */?>
<section>
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 shop-by-cat">
      <h3>Shop by Category</h3>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/dog-food/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/dog-food.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Dog Food</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/biscuits-treats/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/dog-treats.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Dog Treats</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/dog-bowls/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/bowls-and-feeders.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Bowls & Feeders</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/health-wellness/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/health-n-wellness.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Health & Wellness</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/dog-toy/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/dog-toys.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Dog Toys</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/collars-leashes/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/collar-n-leash.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Collars & Leashes</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/dog-grooming/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/gromming.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Grooming </span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/flea-ticks/"> <img src="https://ik.imagekit.io/2345/bootstrap/images/flea-n-ticks.png" width="159" height="123" alt="" title="" class="img-responsive" /><span class="cat-name">Flea & Ticks </span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/crates-beds/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/crate-n-cage.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Crates & Cages </span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/dog-clean-up/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/cleaning.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Dog Cleaning</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/dog-training-behavior/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/traning.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Training</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
      <div class="cat-section"> <a href="/clothing-accessories/"> <span class="cat-image"> <img src="https://ik.imagekit.io/2345/bootstrap/images/dog-cloth.png" width="159" height="123" alt="" title="" class="img-responsive" /> </span> <span class="cat-name">Clothing</span> <span class="cat-discount">Upto 30% Off</span> </a></div>
    </div>
  </div>
</div>
</section>
<?php /*?>shop by category end<?php */?>
<?php /*?>hot selling product start<?php */?>
<section>
<div class="container">
  <div  class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 hot-sell-cat">
      <h3>Hot Selling Products</h3>
    </div>
    <div id="owl-product-slider" class="owl-carousel">
    <? $arrayItem=hotSelling();
				//print_r($arrayItem);
				foreach($arrayItem as $item){
				//	echo "SELECT * FROM shop_items WHERE item_id='$item'";
					$qItemq=query_execute_row("SELECT item_id,name,price,selling_price,item_parent_id,nice_name FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$name=$qItemq["name"];
					$nice_name=$qItemq["nice_name"];
					$price=$qItemq["price"];
					$selling_price=$qItemq["selling_price"];
					$discountPer=($selling_price-$price)/$selling_price*100;	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			    }
					$rowdatM = mysql_fetch_array($qdataM);
					$imglink="https://ik.imagekit.io/2345/tr:h-197,w-197/shop/item-images/orignal/".$rowdatM["media_file"];
				  ?>
                  
               
               
               
      <div class="item">
        <div class="productinfo text-center p-slide-img"> 
        <a href="/<?=$nice_name?>/" style="text-decoration:none;"> <img class="p-s-images" src="<?=$imglink?>" alt="sample image" width="197" height="197" />
               <p><?=substr($name,0,40); if(strlen($name>40)){ echo "...";}?></p></a>
        <div class="product-price-s text-center">
         <span class="p-ds-off">Rs. <?=number_format($price,0)?></span>
               <? if($selling_price>$price){?><span class="p-d-price"><del>Rs. <?=number_format($selling_price,0)?></del></span>
               <span class="p-d-off"><?=number_format($discountPer,0)?>%</span><? }?>
         </div>
        </div>
      </div>
     
     <? }?>
    </div>
    
  </div>
</div>
</section>
<?php /*?>hot selling product end<?php */?>

<?php /*?>recent view start<?php */?>

<?  if($userid=='Guest'){
	    $countuser=query_execute("SELECT  DISTINCT(si.item_id) FROM user_items_history as us,shop_items as si,shop_item_category as sic WHERE sic.item_id=si.item_id AND sic.category_id !='836' AND sic.category_id !='837' AND sic.category_id !='835'  AND si.item_id=us.item_id  AND visibility='visible' AND stock_status='instock' AND user_ip='$ip' AND type_id !='configurable' ORDER BY id DESC LIMIT 10"); 
	   }elseif($userid !='Guest')
	   {
		   $countuser=query_execute("SELECT  DISTINCT(si.item_id) FROM user_items_history as us,shop_items as si ,shop_item_category as sic WHERE sic.item_id=si.item_id AND sic.category_id !='836' AND sic.category_id !='837' AND sic.category_id !='835' AND si.item_id=us.item_id  AND visibility='visible' AND stock_status='instock' AND userid='$userid' AND type_id !='configurable' ORDER BY id DESC LIMIT 10");  
	   }
$c_count=mysql_num_rows($countuser);
if($c_count>4)
{
?>
<section>

<div class="container">
  <div  class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 hot-sell-cat">
      <h3>Recently Viewed</h3>
    </div>
    <div id="owl-recent-slider" class="owl-carousel">
    <? while($item_idS=mysql_fetch_array($countuser))
	   {
		$item_idRec[]= $item_idS['item_id']; 
	   }
			 foreach($item_idRec as $item){
					$qItemq=query_execute_row("SELECT item_id,name,price,selling_price,item_parent_id,nice_name FROM shop_items WHERE item_id='$item' AND visibility='visible' AND stock_status='instock'");
					$item_id=$qItemq["item_id"];
					$name=$qItemq["name"];
					$price=$qItemq["price"];
					$nice_name=$qItemq["nice_name"];
					$selling_price=$qItemq["selling_price"];
					$discountPer=($selling_price-$price)/$selling_price*100;	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			    }
					$rowdatM = mysql_fetch_array($qdataM);
					$imglink="https://ik.imagekit.io/2345/tr:h-197,w-197/shop/item-images/orignal/".$rowdatM["media_file"];
			        $itemE =10;
                    query_execute_row("SELECT min(price) as price,mrp_price,count(id) as c,merchant_link FROM  shop_item_affiliate WHERE item_id='$item_id' AND price !='' AND price !='0'"); 
                   $minPrice=$getdataItem['price'];
                   $mrp_price=$getdataItem['mrp_price'];
                   if($getdataItem['c']>0){
                   $itemE=$getdataItem['c'];
                   }
				?>
                             
      <div class="item">
        <div class="productinfo text-center p-slide-img"> 
        <a href="/<?=$nice_name?>/" style="text-decoration:none;"> <img class="p-s-images" src="<?=$imglink?>" alt="sample image" width="197" height="197" />
               <p><?=substr($name,0,40); if(strlen($name>40)){ echo "...";}?></p></a>
        <div class="product-price-s text-center">
         <span class="p-ds-off">Rs. <?=number_format($price,0)?></span>
               <? if($selling_price>$price){?><span class="p-d-price"><del>Rs. <?=number_format($selling_price,0)?></del></span>
               <span class="p-d-off"><?=number_format($discountPer,0)?>%</span><? }?>
         </div>
        </div>
      </div>
     
     <? }?>
    </div>
    
  </div>
</div>
</section>
<? }?>
<?php /*?>recent view end<?php */?>
<?php /*?>logn banner<?php */?>
<section class="banner-section">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12 hot-sell-cat">
      <h3> DogSpot Trusted Partners</h3>
    </div>
<div class="col-md-12">
<a href="http://www.petshopdelhi.in/" target="_blank">
<img src="https://ik.imagekit.io/2345/bootstrap/images/doggy-world-banner.jpg" alt="" class="img-responsive" title="" width="1140" height="197" />
</a>
</div>
</div>
</div>
</section>
<?php /*?>long banner end<?php */?>

<?php /*?>adoption and wag tag section<?php */?>
<section class="wagtag-n-adoption">
  <div class="container">
    <div class="row">
      <div class="col-md6 col-sm-6 col-xs-12 adopt-wagtag-btm">
        <div class="adoption-box">
          <div class="adoption-text-b">Adoption Center</div>
          <div class="adoption-text-s">Give a home to home less</div>
          <div class="adopt-btn"><a href="http://www.dogspot.in/adoption/">
            <input class="adbtn" type="button" value="#ADOPTDONTSHOP">
            </a></div>
        </div>
        <div class="adoption-banner"> <a href="http://www.dogspot.in/adoption/"><img src="https://ik.imagekit.io/2345/bootstrap/images/adoption-banner.jpg" alt="adoption" title="adoption" class="img-responsive" ></a> </div>
      </div>
      <div class="col-md6 col-sm-6 col-xs-12 adopt-wagtag-btm">
        <div class="wagtag-sec">
          <div class="wagtag-banner"><a href="https://www.dogspot.in/wagtag-info/"><img src="https://ik.imagekit.io/2345/bootstrap/images/wag-tag-banner.jpg" class="img-responsive" alt="wag tag" title="wag tag" width="556" height="309"></a></div>
          <div class="wagtag-text">Wag tag will ensure that your pet comes back to you in case of any unfortunate event in which you lose him/ her. </div>
          <div class="wagtag-btn"><a href="https://www.dogspot.in/wagtag-info/">
            <input class="wag-tag-btn" type="button" value="GET YOUR WAGTAG">
            </a></div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php /*?>adoption and wag tag section end<?php */?>
<?php /*?>internation brands<?php */?> 
<section>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-4 adopt-wagtag-btm">
        <h1 class="int-brands-txt"> <span class="int-brands-color">Exclusive</span> International 
          Pet Brands</h1>
        <div class="int-brands-small">Now Available at your doorsteps!</div>
      </div>
      <div class="col-xs-6 col-md-2"><a href="/spotty/">
        <div class="int-brands_sprite int-spoty-logo"></div>
        </a></div>
      <div class="col-xs-6 col-md-2 adopt-wagtag-btm"><a href="/furminator/">
        <div class="int-brands_sprite int-furminator-logo"></div>
        </a></div>
      <div class="col-xs-6 col-md-2"><a href="/urine-off/">
        <div class="int-brands_sprite int-urineoff-logo"></div>
        </a></div>
      <div class="col-xs-6 col-md-2 adopt-wagtag-btm"><a href="/petmate/">
        <div class="int-brands_sprite int-petmate-logo"></div>
        </a></div>
      <div class="col-md-12 col-xs-12 col-sm-12 brands-bottom" >&nbsp;</div>
    </div>
  </div>
</section>
<?php /*?>internation brands end<?php */?> 
<?php /*?> qna section<?php */?>
<section class="pet-qna-section">
			<div class="container">
            <div class="pet-qna-heading text-center">
					<h3>Latest Pet QNA</h3>
					</div>
				<div class="row">
					<!-- Content Area Bar -->
					<div class="col-md-12 col-sm-12 col-xs-12">
						
						<div class="panel-body">
                           <?
			$selectArt1=mysql_query("SELECT * FROM qna_questions WHERE publish_status ='publish' AND qna_desc !='' ORDER BY qna_cdate desc LIMIT 2");
						    $i=0;
          while($rowArt = mysql_fetch_array($selectArt1)){
			  $i=$i+1;
            $qna_id = $rowArt["qna_id"];
            $qnauser = $rowArt["userid"];
			$qnadesc = $rowArt["qna_desc"];
            $qna_question = $rowArt["qna_question"];
            $qna_name = $rowArt["qna_name"];
            $qna_tag = stripslashes($rowArt["qna_tag"]);
            $qna_catid = $rowArt["qna_catid"];
            $qna_cdate = $rowArt["qna_cdate"];
            $qnadesclen=strlen($qnadesc);
			$qnaqtnlen=strlen($qna_question);
			//echo $qnadesclen."kkkkkkkkk</br>";
			//echo $qnaqtnlen."llllllll</br>";    
           // $qna_question = makeClickableLinks($qna_question);
            $qna_question = htmlspecialchars_decode(nl2br(stripslashes($qna_question)));
            $qna_question = breakLongWords($qna_question, 30, " ");
			$qna_question=make_outlink_nofollow($qna_question);
			$qnadesc = htmlspecialchars_decode(stripallslashes(makeTeaser($qnadesc, 150)));
			$qnadesc=make_outlink_nofollow($qnadesc);
           $rowUser=query_execute_row("SELECT f_name, image FROM users WHERE userid='".$rowArt['userid']."'");

?>
                        <div class="listing-grid">
										<div class="row">
											<div class="col-md-2 col-sm-2 col-xs-12 hidden-xs">
												<a href="<? echo"/qna/$qna_name/";?>">
													<img alt="" class="img-responsive center-block" src="https://ik.imagekit.io/2345/bootstrap/images/qna-icons.jpg">
												</a>
											</div>
											<div class="col-md-10 col-sm-10  col-xs-12">
<h3><a href="<? echo"/qna/$qna_name/";?>"><?=htmlspecialchars_decode(makeTeaser(ucfirst($qna_question),120));?></a></h3>
												<div class="listing-meta"> 
                                                <span> <? print(showdate($qna_cdate, "d M o g:i a")); ?></span> <span>|</span>
                                                <span><? if(($qnauser == $userid || $sessionLevel ==1 ) && $userid!="Guest")?>By <?=$rowUser["f_name"]; ?></span> 
												</div>
											</div>
											
											<div class="col-md-10 col-sm-10  col-xs-12">
												<? if($qnadesc!=''){	?>
                                               <p> 
                                              <?=ucfirst($qnadesc)?><? if($qnadesclen > 149){ ?>.. <? } ?>
                                                </p>
												<? }?>
												</div>
											</div>
										</div>
                        
                        
                        <?
                        }
                        ?>
                        <div class="text-center clearfix">
								<a href="/qna/" class="btn view-allblog-btn">View all qna</button></a>
								</div>
                        </div>

</div>
</div>
</div>
</section>
<?php /*?>qna section end<?php */?>
<?php /*?>pet stories section<?php */?>
<section class="pet-blog-section">
  <div class="container">
  <div class="pet-blog-heading text-center">
					<h3>Pet Stories</h3>
					</div>
					
				
    <div class="row">
    <? 
$selectTqna1 = mysql_query("SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr ,wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND post_date LIKE '%2016%' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY RAND() LIMIT 3"); 
    $iid           = $iid + 1;
   while($selectTqna=mysql_fetch_array($selectTqna1)){
    $articlecat_id = $selectTqna["term_id"];
	$sel_cat=query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
	$cat_name=$sel_cat['name'];
	$cat_nice=$sel_cat['slug'];
    $article_id    = $selectTqna["ID"];
    $art_subject   = $selectTqna["post_title"];
    $art_body      = $selectTqna["post_content"];
    $c_date        = $selectTqna["post_date"];
    $artuser_wp    = $selectTqna["post_author"];
	$rowUser_id  = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$artuser_wp'");
	$artuser = $rowUser_id['user_login'];
    $art_name      = $selectTqna["post_name"];
	$imgURL    = get_first_image($art_body, MYSITE_ROOT_URL);
	$imgURLAbs = make_absolute($imgURL, 'https://www.dogspot.in');
    
    $art_subject = stripslashes($art_subject);
    $art_subject1 = substr($art_subject,0,45);
	 if(strlen($art_subject)>45)
	 {
	$art_subject1=$art_subject1.'..';	 
	 }
    	$rowUser_id  = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='" . $rowTqna['post_author'] . "'");
	$userid_artdb=$rowUser_id['user_login'];
	
	//------------------Database change
	//Check for featured image--------------------------------------------------
	$check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id' AND post_type='attachment' AND post_mime_type like '%image%' ORDER BY ID ASC");
	//--------------------ends----------
	$rowUser  = query_execute_row("SELECT f_name, image FROM users WHERE userid='$userid_artdb'");
		if (strpos($imgURLAbs,'wordpress') !== false) {
    	$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/470x287-'.$image_name;
		}else if($check_img['guid'] !=''){
		$imgURLAbs=$check_img['guid'];	
		$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/470x287-'.$image_name;
		}
		else if ($imgURLAbs!='/new/pix/dogspot-logo-beta.gif') {
		$URL=$imgURLAbs;
	   $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
	   $pos = strrpos($image_name,'/');
	   $image_name = substr($image_name,$pos+1);
		$extension = stristr($image_name,'.');
		if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
		$image_name;
		}
		$src = '/userfiles/images/'.$image_name;
		$imageURL='/imgthumb/470x287-'.$image_name;
		} 
		else if($rowUser["image"]) {
		$src ='/profile/images/'.$rowUser["image"];
		$imageURL = '/imgthumb/470x287-'.$rowUser["image"];
		} 
		else  {
        $src ='/images/noimg.gif';
		$imageURL = '/imgthumb/470x287-noimg.gif';
		}
	$imglink="https://ik.imagekit.io/2345/tr:h-287,w-470,c-at_max/".$src;
		     $image_width =470;
			 $image_height =287;

    ?>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="pet-blog-grid">
							<div class="pet-blog-image">
								<a href="/<?=$art_name?>/">
                                <img src="<?=$imglink?>" width="<?=$image_width?>" height="<?=$image_height?>" alt="<?=$art_subject1?>" title="<?=$art_subject1?>" /> </a>
							</div>
							<div class="pet-blog-content">
								<h5><a href="/<?=$art_name?>/"><?=$art_subject1?></a></h5>
								<ul class="post-meta">
                               
                                    <li><?print(showdate($c_date, "d M Y"));?></li>
    <li>By <?=dispUname($artuser); ?> 
    </li>
									
									
								</ul>
								<?php /*?><p><?=substr($art_body,0,130);?>..</p><?php */?>
							</div>
							<div class="pet-blog-footer">
								 <a href="/<?=$art_name?>/" class="more-btn pull-right"> Read more</a> 
							</div>
						</div>
					</div>
      
     <? }?>
      
      
    <div class="clearfix"></div>
    <div class="text-center clearfix section-padding-bt"> <a href="/dog-blog/" class="btn view-allblog-btn">View all pet stories</a> 
					</div>
    </div>
  </div>
</section>
<?php /*?>pet stories end<?php */?>
<script type="text/javascript" src="/bootstrap/js/owl.carousel.min.js"></script> 
<script>
    $(document).ready(function() {
      $("#owl-banner-slider").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
	  autoPlay: 4000,
      singleItem : true
	  

      });
    });
	$(document).ready(function() {
 $('#owl-product-slider').owlCarousel({
    items:4,
    lazyLoad:true,
    loop:false,
	slideSpeed : 400,
	autoPlay: false,
	paginationSpeed : 400,
	navigation:true,
    margin:10,
	rewindNav:false,
});
   
 
});
$(document).ready(function() {
 $('#owl-recent-slider').owlCarousel({
    items:4,
    lazyLoad:true,
    loop:false,
	slideSpeed : 400,
	autoPlay: false,
	paginationSpeed : 400,
	navigation:true,
    margin:10,
	rewindNav:false,
});
   
 
});

    </script>
    <? require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php'); ?>  
   