<? ob_start();
session_start();
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT.'/session.php');
include($DOCUMENT_ROOT . "/facebookconnect/facebooksettings.php");
include($DOCUMENT_ROOT . "/facebookconnect/facebookfunctions.php");
include($DOCUMENT_ROOT . '/twitterconnect/twitterfunctions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');

$refUrl = $_SESSION['refUrl'];
$session_id = session_id();
$user_ip = ipCheck();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
<title><?=$title?></title>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" />
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/checkout.css?v=14" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,900,700italic' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function show_ship_detail(divName,divNameHide) 
{ 
    var ship_d=document.getElementById(divName).style.display; 
    if(ship_d=='none' && divName !='showAddress') 
    { 
        document.getElementById(divName).style.display='inline'; 
        document.getElementById(divNameHide).style.display='none'; 
    } 
    if(divName=="showAddress" && document.getElementsByClassName('imgtxtcontwag').length !== 0) 
    { 
        document.getElementById(divName).style.display='inline'; 
        document.getElementById(divNameHide).style.display='none';     
    } 
} 

</script>

<!-- Start Visual Website Optimizer Asynchronous Code -->
<!-- End Visual Website Optimizer Asynchronous Code -->

<script>
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.shipping-dropbtn')) {

    var dropdowns = document.getElementsByClassName("shipping-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
<script type="text/javascript">
  var _bout = _bout || [];
  var _boutAKEY = "r8qocjz8bo543u8h8a4ay4tjy33oysx8m83smhrhv8", _boutPID = "33409"; 
  var d = document, f = d.getElementsByTagName("script")[0], _sc = d.createElement("script"); 
  _sc.type = "text/javascript"; _sc.async = true; _sc.src = "//d22vyp49cxb9py.cloudfront.net/jal-v2.min.js"; f.parentNode.insertBefore(_sc, f);
 </script>

<script type="text/javascript">
window.history.forward();
function noback(){
	window.history.forward();
	}
</script>
</head>

<body onLoad="noback();" onpageshow="if (event.persisted) noback();" onUnload="">
<header id="header"> 
  <!-- mobile navigation-->
  
  <div class="header-middle visible-lg visible-md">
    <div class="checkout-navbar ">
      <div class="container ">
        <div class="row">
          <div class="col-sm-3">
            <div class="logo pull-left"> <a href="https://www.dogspot.in/"><img src="https://ik.imagekit.io/2345/bootstrap/images/logo.png" style="margin-left:-3px;" alt="Dogspot"></a> </div>
          </div>
          <!-- search start--> 
          <div class="col-md-6">
           <? if($page!='Login' && $page !='confirmation'){?>      
  <ul class="checkout-steps">
              <li class="<? if($page=='shipping'){?>active<? }else{?><? }?>">
               <a href="#login-box" class="login-window"  >
                  <div class="header-sprie <? if($page=='shipping'){?> shipping-icon-active <? }else{?>shipping-icon<? }?>"></div>
                  <div class="txt">Shipping</div>
                   </a>
                  

              </li>
             
              <li class="<? if($page=='Payment'){?>active<? }else{?><? }?>">
             <a href="#">
              <div class="header-sprie <? if($page=='Payment'){?>payment-icon-active<? }else{?>payment-icon<? }?>"></div>
                <div class="txt">Payment</div>
                 </a>
              </li>
              </li>
             
              <li class="<? if($page=='confirmation'){?>active<? }else{?><? }?>">
               <div class="header-sprie <? if($page=='confirmation'){?> confirmation-icon-active <? }else{?>confirmation-icon<? }?>"></div>
                <div class="txt">Confirmation</div>
              </li>
              
               
 </ul>
 <? }?>
          </div>
          <!-- search end here-->
          
                    <?
if ($useridNew != "" && $sitesection!='login-checkout') {?>
	  <!-- checkout process-->
<div class="col-md-3 pull-right text-right">
 <div class="ds-cartbox"><i class="header-sprie user-icon"></i><? if($useridNew){echo $useridNew;}else { $userid; } ?></span>
</div></div> <?
}else{?>
          <div class="col-md-3 pull-right text-right">
            <div class="ds-cartbox">
              <? if($userid!='Guest'){?>
             <a  href="#" data-toggle="dropdown"><i class="fa fa-user-o"></i> <span><? if($sessionName)
   { echo ucwords(strtolower($sessionName)); }elseif($userid){ echo $userid;} ?></span> </a>
             <? }?>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
              </ul>
            </div>
          </div>
        <? }?></div>
      </div>
    </div>
  </div>
</header>