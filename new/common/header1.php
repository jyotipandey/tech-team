<?php
ob_start();
session_start();
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT.'/session.php');
include($DOCUMENT_ROOT . "/facebookconnect/facebooksettings.php");
include($DOCUMENT_ROOT . "/facebookconnect/facebookfunctions.php");
include($DOCUMENT_ROOT . '/twitterconnect/twitterfunctions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');

$refUrl = $_SESSION['refUrl'];
$session_id = session_id();
$user_ip = ipCheck();


header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
?>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="msvalidate.01" content="33722F73799514D1FBB0C782A3581F88"/>
<? if($sitesection!='shop' || $sitesection!='articles' || $sitesection!='add-events' || $sitesection == 'dog-business-view' ){ ?>
<meta property="og:image" content="https://www.dogspot.in/new/common/images/fb-logo.jpg" />
<? } ?>
<?php /*?><link href="/new/common/css/header-style.css?v=<?=time(); ?>" rel="stylesheet" type="text/css" />
<link href="/new/common/css/mega_menu.css?v=<?=time(); ?>" rel="stylesheet" type="text/css" /><?php */?>
<?php /*?><link href="/new/common/head_new.css" rel="stylesheet" type="text/css" /><?php */?>
 <link type="text/css" rel="stylesheet" href="/checkout/css/checkout.css?=v43" />
 

<script type="text/javascript">
  var _bout = _bout || [];
  var _boutAKEY = "r8qocjz8bo543u8h8a4ay4tjy33oysx8m83smhrhv8", _boutPID = "33409"; 
  var d = document, f = d.getElementsByTagName("script")[0], _sc = d.createElement("script"); 
  _sc.type = "text/javascript"; _sc.async = true; _sc.src = "//d22vyp49cxb9py.cloudfront.net/jal-v2.min.js"; f.parentNode.insertBefore(_sc, f);
 </script>


<!--End of Zopim Live Chat Script-->
<? /////////////////////////////////////////WIZGO////////////////////////////////////////////////?>
<?php /*?><script async src="https://tracker.wigzopush.com/wigzopush_manager.js?orgtoken=c7c2b2d9-90ab-4955-a7c0-83f9e92d89e6" type="text/javascript"></script>
<?php */?>


<? /////////////////////////////////////////WIZGO////////////////////////////////////////////////?>
 <script type="text/javascript">

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<!-- Start Visual Website Optimizer Asynchronous Code -->
<!-- End Visual Website Optimizer Asynchronous Code -->

<script>
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.shipping-dropbtn')) {

    var dropdowns = document.getElementsByClassName("shipping-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
<script type="text/javascript">
window.history.forward();
function noback(){
	window.history.forward();
	}
</script>
<body onLoad="noback();" onpageshow="if (event.persisted) noback();" onUnload="">
<!-- header start-->
<nav class="checkout-navbar">
<div class="navbar-header">
  <!-- logo--><div class="chek-out-logo"><a href="https://www.dogspot.in/"> <img src="/checkout/images/logo.png" /></a></div><!--logo end-->

     <!--forgot password starts-->     
              
    <? if($page!='Login' && $page !='confirmation'){?>      
  <ul class="checkout-steps">
              <li class="<? if($page=='shipping'){?>active<? }else{?><? }?>">
               <a href="#login-box" class="login-window"  >
                  <div class="header-sprie <? if($page=='shipping'){?> shipping-icon-active <? }else{?>shipping-icon<? }?>"></div>
                  <div class="txt">Shipping</div>
                   </a>
                  

              </li>
             
              <li class="<? if($page=='Payment'){?>active<? }else{?><? }?>">
             <a href="#">
              <div class="header-sprie <? if($page=='Payment'){?>payment-icon-active<? }else{?>payment-icon<? }?>"></div>
                <div class="txt">Payment</div>
                 </a>
              </li>
              </li>
             
              <li class="<? if($page=='confirmation'){?>active<? }else{?><? }?>">
               <div class="header-sprie <? if($page=='confirmation'){?> confirmation-icon-active <? }else{?>confirmation-icon<? }?>"></div>
                <div class="txt">Confirmation</div>
              </li>
              
               
 </ul>
 <? }?>
          
<!--forgot password ends-->  
          <?
if ($useridNew != "" && $sitesection!='login-checkout') {?>
	  <!-- checkout process-->

<ul class="nav-right">
<li><i class="header-sprie user-icon"></i><? if($useridNew){echo $useridNew;}else { $userid; } ?></li>
</ul><?
} else {
?>
<ul class="nav-right">
  <? if($userid!='Guest'){?>
  <li class="dropdown">
  <div onClick="myFunction()" class="dropbtn"><i class="header-sprie user-icon"></i><? if($sessionName)
   { echo ucwords(strtolower($sessionName)); }elseif($userid){ echo $userid;} ?></div>
          <? }?>
       <div id="myDropdown" class="dropdown-content">
    <a class="" href="/logout.php?logoff=logoff" title="Logout" ><img src="/checkout/images/logout-icons.jpg" class="log-out-image">Logout</a>
   </div>
   </li>

        <li>
       </li></ul>
          
         
        <?

		  }
?>
</div>
</nav>
          
     
        </div>
      </div>
      
    </div>


