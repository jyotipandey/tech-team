<?
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

require_once($DOCUMENT_ROOT.'/shop/functions.php');
include($DOCUMENT_ROOT . "/new/articles/catarray.php");
include($DOCUMENT_ROOT . "/new/qna/qnacat.php");
include($DOCUMENT_ROOT."/dogs/arraybreed.php");

$sitesection = "shop";

//$shopQuery="Dog Crate";
$q=$shopQuery;
$q=str_replace("?filter=filter", "", $q);
$ci=1;
$ci1=1;
$query=escapeSolrValue($q, 'OR');
$maxshow='5';
	
	$query=urlencode($query);
	$url = "http://localhost:8080/dogspotshop/select/?q=$query AND visibility:visible AND item_display_status:active&version=2.2&rows=$maxshow&fl=* score&qf=name^2&indent=on";
	$url = str_replace(" ","%20",$url);
	$result = get_solr_result($url);
	$totrecord = $result['TOTALHITS'];

/*article */

			$st='&sort=c_date desc';
			$url1 = "http://localhost:8080/dogspotarticles/select/?q=$query AND publish_status:publish$st&version=2.2&rows=$maxshow&fl=* score&qf=title_tag^2&indent=on";
			$url1 = str_replace(" ","%20",$url1);
			$artresult = get_solr_result($url1);
			$totrecordart = $artresult['TOTALHITS'];

/*close here*/	
	if($totrecordart>0){ foreach($artresult['HITS'] as $row){ $Aarticle_cat[]=$row["articlecat_id"];}}
	if($totrecordart>0){ $Aitem_categoryart=array_unique($Aarticle_cat); }
	

/*QNA */

			$qt='&sort=qna_cdate desc';
	 		$url2 = "http://localhost:8080/dogspotqna/select/?q=$query AND publish_status:publish$bsql$qt&version=2.2&rows=$maxshow&fl=* score&qf=title_tag^2&indent=on";
			$url2 = str_replace(" ","%20",$url2);
			
			$resultqna = get_solr_result($url2);
			$totrecordqna = $resultqna['TOTALHITS'];
/*close here*/	
	if($totrecordqna>0){ foreach($resultqna['HITS'] as $row1){$Qqna_cat[]=$row1["qna_catid"];}}
	if($totrecordqna>0){ $Aitem_categoryqna=array_unique($Qqna_cat); }


/*Business */
			$bt='&sort=c_date desc';
	 		$url3 = "http://localhost:8080/dogspotbusiness/select/?q=$query AND publish_status:publish$bsql$bt&version=2.2&rows=$maxshow&fl=* score&qf=title_tag^2&indent=on";
			$url3 = str_replace(" ","%20",$url3);
			$resultbus = get_solr_result($url3);
			$totrecordbus= $resultbus['TOTALHITS'];

/* Closed here */
	if($totrecordbus>0){ 
	foreach($resultbus['HITS'] as $row2){
		$Bus_cat[]=$row2["category_name1"];
		$Bus_cat[]=$row2["category_name2"];	
		$Bus_cat[]=$row2["category_name3"];
		$Bus_cat[]=$row2["category_name4"];
		$Bus_cat[]=$row2["category_name5"];
		}
	}
	if($totrecordbus>0){ $Aitem_categorybus=array_unique($Bus_cat); }
/*Dogs */

			$dt='&sort=cdate desc';
	 		$url4 = "http://localhost:8080/dogspotdog/select/?q=$query AND publish_status:publish$dt&version=2.2&rows=$maxshow&fl=* score&qf=dog_name^2&indent=on";
			$url4 = str_replace(" ","%20",$url4);
			
			$resultdogs = get_solr_result($url4);
			$totrecorddogs = $resultdogs['TOTALHITS'];
/*close here*/	
	if($totrecorddogs>0){ foreach($resultdogs['HITS'] as $row3){$Dog_cat[]=$row3["breed_nicename"];}}
	if($totrecorddogs>0){ $Aitem_categorydogs=array_unique($Dog_cat); }
/* Puppy */
			 $pt='&sort=c_date desc';
	 		 $url5 = "http://localhost:8080/dogspotpuppy/select/?q=$query AND publish_status:publish$pt&version=2.2&rows=$maxshow&fl=* score&qf=title_tag^2&indent=on";
			$url5 = str_replace(" ","%20",$url5);
			$resultpuppy = get_solr_result($url5);
			$totrecordpuppy = $resultpuppy['TOTALHITS'];
/* Closed Here */
if($totrecordpuppy>0){ foreach($resultpuppy['HITS'] as $row4){$Puppy_cat[]=$row4["breed_nicename"];}}
	if($totrecordpuppy>0){ $Aitem_categorypuppy=array_unique($Puppy_cat); }

$q=trim($q);$q=str_replace('+AND+',' ',$q);$q=ucwords($q);
if($q){
	$pageTitle=$q;
	$pageKeyword=$q;
	$pageDesc=$q;
	
}else{
	$pageTitle=$category_title;
	$pageKeyword=$category_keyword;
	$pageDesc=$category_desc;
	$cat_name=$category_name;
	$pageabout=$category_about;
}

// Create filter end --------------------------------------------------------
// Custom Variable for Google analytics
//$CustomVar[2]='Category|'.$cat_name;
// Custom Variable for Google analytics
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title><?=$pageTitle?></title>
<meta name="keywords" content="<?=$pageKeyword?>" />
<meta name="description" content="<?=$pageDesc?>" />
<? if($shopQuery){?>
<meta name="robots" content="noindex, nofollow">
<? }?>
<link href="/new/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/new/css/jquery-ui.css" />
<link href="/new/css/style1.css" rel="stylesheet" type="text/css" />

 
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?><script type="text/javascript" src="/new/js/jquery/jquery-ui.js"></script>

<style>
table { 
 
  border-collapse: collapse; 
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #eee; 
}
th { 
  background: #999; 
  color: #fff; 
  font-weight:normal;
  font-size:16px;
}
td, th { 

  padding: 5px; 
  border: 1px solid #ccc; 
  text-align: left; 
  font-weight:normal;
  font-size:14px;
}
</style>

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>    
 <!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980">
                
                    <div class="fl" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666;"> <a href="/shop/">Home</a> » <a href="/shop/">Shop</a> » <?=$q?>
                     </div>
                 
                     <div class="fr"> <a href="javascript:window.print()"><img src="/new/pix/bdcmb_printicon.gif" alt="print" title="print"  /></a>
                        
                       <a href="http://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a>
      <a href="http://twitter.com/#!/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a>                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->   
       
       <!-- main container -->
       <div class="cont980"> 
       <!-- three column approach -->
       
       	<!-- column 1 -->
             	<form id="formFilter" name="formFilter" method="post" action="">
                 <div class="filterContainer fl comment" id="comment" >
      <header>Refine Your Search</header>
       <div class="filterContainer_wrapper">
                <div style="width:220px"> 
                   <div class="filterCategory" >
          <c1>By Filter</c1>
          <div style="overflow:auto;">
          <br />
          <ul class="category" style="margin-right:35px;">
         						    <? if($totrecord>0){?>
                                    <li>
                                    <a target="_blank" href="/new/shop/product_listing.php?shopQuery=<?=$shopQuery?>">Shop</a>    
                                    </li>
                                    <? } if($totrecordart>0){?>
                                    <li>
                                    <a target="_blank" href="/dog-blog/search/<?=$shopQuery?>">Article</a>    
                                    </li><? } if($totrecordqna>0){?>
                                     <li>
                                    <a target="_blank" href="/qna/search/<?=$shopQuery?>">QNA</a>    
                                    </li><? } if($totrecordbus>0){?>
                                    <li>
                                    <a target="_blank" href="/dog-listing/blogsearch/<?=$shopQuery?>">Business Listing</a>    
                                    </li><? } if($totrecorddogs>0){?>
                                    <li>
                                    <a target="_blank" href="/dogs/dogsearch/<?=$shopQuery?>">Dogs</a>    
                                    </li><? } if($totrecordpuppy>0){?>
                                    <li>
                                    <a target="_blank" href="/puppies/puppysearch/<?=$shopQuery?>">Puppy</a>    
                                    </li><? } ?>
                                    <?php /*?><li>
                                    <a target="_blank" href="/qna/search/<?=$query?>">Shop</a>    
                                    </li><?php */?> 
                                    </ul>
          
                                      
            </div></div>        
              <div class="vs10"></div>
           <? if($totrecordart>0){ ?>
             <div class="filterCategory" >
          <c1>By Article Category</c1>
          <div style="overflow:auto;">
          <br /> 
          <ul class="category" style="margin-right:35px;">
            <? foreach($Aitem_categoryart as $acateart){
				if($acateart!='0') {
				?>
                                <li>
                                    <a target="_blank" href="/dog-blog/category/<?=$AartCatNice[$acateart]?>/"><?=$AartCat[$acateart]?></a>    
                                    </li>
                                <? } } ?> </ul>       
            </div></div>        
             
              <div class="vs10"></div>
              <? } if($totrecordqna>0){ ?>
 <div class="filterCategory" >
          <c1>By QNA Category</c1>
          <div style="overflow:auto;"> 
          <br />
          <ul class="category" style="margin-right:35px;">
            <? foreach($Aitem_categoryqna as $acateqna){
				if($acateqna!='0') {
				?><li>
                                    <a target="_blank" href="/qna/category/<?=$AqnaCatNice[$acateqna]?>/"><?=$AqnaCat[$acateqna]?></a>    
                                    </li>
                                <? } } ?>  </ul>      
            </div></div>       
                    <? } if($totrecordbus>0){ ?>   
          <div class="filterCategory" >
          <c1>By Business Category</c1>
          <div style="overflow:auto;"> 
          <br />
          <ul class="category" style="margin-right:35px;">
            <? foreach($Aitem_categorybus as $acatebus){
				if($acatebus!='') {
				?><li>
                                    <a target="_blank" href="/dog-listing/category/<?=$acatebus?>/"><?=$acatebus?></a>    
                                    </li>
                   <? } } ?> </ul>       
            </div></div>     
               <? } if($totrecorddogs>0){ ?>
                    <div class="filterCategory" >
          <c1>By Dog Breeds</c1>
          <div style="overflow:auto;"> 
          <br />
          <ul class="category" style="margin-right:35px;">
            <? foreach($Aitem_categorydogs as $acatedogs){
				if($acatedogs!='') {
				?><li>
                                    <a target="_blank" href="/dogs/breed/<?=$acatedogs?>/"><?=$ArrDogBreed[$acatedogs]?></a>    
                                    </li>
                   <? } } ?> </ul>       
            </div></div>            
            <? } if($totrecordpuppy>0){ ?>
               
              <div class="filterCategory" >
          <c1>By Puppy Breeds</c1>
          <div style="overflow:auto;"> 
          <br />
          <ul class="category" style="margin-right:35px;">
            <? foreach($Aitem_categorypuppy as $acatepuppy){
				if($acatepuppy!='') {
				?><li>
                                    <a target="_blank" href="/puppy/breed/<?=$acatepuppy?>/"><?=$ArrDogBreed[$acatepuppy]?></a>    
                                    </li>
                   <? } } ?> </ul>       
            </div></div>                     
   <? }  ?>
           
                
				</div>
                </div><!-- column 1 -->
				</div>
                <!-- column 2 -->
                <div class="displayContainer fr">
                <h1>Results For : <?=$shopQuery?> </h1>                
                <div class="cb"></div>
                <div class="doubleborderImg"></div>
                <div class="vs20"></div>
                
				
                <div id="productFilter">
                
                <? 
				if($totrecord!='0'){
				
				 foreach($result['HITS'] as $rowItem){
					$item_id=$rowItem["item_id"];
					
					$name=$rowItem["name"];
					$created_at=$rowItem["created_at"];
					$creat=explode("T", $created_at);
					$end_date=date("Y-m-d ");
					$thirtydays = date_create('30 days ago');
					$startdate=date_format($thirtydays, 'Y-m-d');
					//echo "required".$creat[0];
					//echo "start".$startdate."<br>"."end".$end_date;
					if ($startdate <= $creat[0] && $creat[0] <= $end_date) 
					 {
						$flag='1';
					}
					else { 
						$flag='0';
					}
	
					$nice_name=$rowItem["nice_name"];
					$price=$rowItem["price"];
					$selling_price=$rowItem["selling_price"];
					$item_parent_id=$rowItem["item_parent_id"];
					$stock_status=$rowItem["stock_status"];
					if($item_parent_id == '0'){
						$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
					}else{
						$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
					}
					$rowdatM = mysql_fetch_array($qdataM);
					
					if($rowdatM["media_file"]){
						$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
						$imageURL='/imgthumb/150x160-'.$rowdatM["media_file"];
					}else{
						$src = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
						$imageURL='/shop/image/no-photo-t.jpg';
					}
					
					$dest = $DOCUMENT_ROOT.$imageURL;
					createImgThumbIfnot($src,$dest,'150','160','ratiowh');
				?> 
                <? if($ci==1){?>                   
                    <ul class="slide"> 
				<? }?>                      
                <li style=" <? if($ci%4==0){?>border:0px; margin-right:0px; padding:0px;<? }?> width:150px;"><a href="/<?=$nice_name?>/" style="height:160px; display:block" ><img src="<?=$imageURL?>" alt="<?=$name?>" title="<?=$name?>"/></a><h2><a href="/<?=$nice_name?>/" style="height:80px; display:block" alt="<?=$name?>" title="<?=$name?>"><?=$name;?></a></h2><strong>Rs. <?=number_format($price)?></strong><br /><del><? if($selling_price>$price){echo 'Rs. '.number_format($selling_price); }?></del><!--<?//$rowItem["score"]?></a>-->
                 <a href="/<?=$nice_name?>/" class="link"><img src="/new/pix/buynow_btn_small.gif" width="82" height="20" alt="Buy Now" /></a>
                        </li>
                     
					<? if($ci%4==0){?>               
    				</ul>
                    <div class="cb"></div>
   					<div class="doubleborderImg"></div>
                    <ul class="slide">              
					<?  }$ci++;}//... for loop ends here ?>
                    </ul>
                    <div class="cb"></div>
                    <div class="fr" style="margin-bottom:5px;">
                    <a href="/new/shop/product_listing.php?shopQuery=<?=$shopQuery?>"><img src="/new/pix/button-browse-more.jpg"></a>
                    </div>
                    <div class="vs10"></div>
                                         <!-- content -->
                    <?php /*?><div style="text-align:center; margin: 5px 0 5px 25px;"><?php
                    require_once($DOCUMENT_ROOT . '/banner1.php');
                    addbanner600('74', '600', '1');
                    ?></div><?php */?>
                    
                    
                    
                     <!---------------------------- Article Solar -------------------------->                      
                    
                       <!-- just in -->
                        <div class="vs30"></div>
                        <div class="cb"></div>
                        <div class="hrline"></div>
                        <? } 
		
		$s = 0;
		if($totrecordart>'0'){?>
        
                        <div class="cb"></div>
                        <div class="vs10"></div>
							<h3>Related Article</h3>
                       <div class="cb"></div>
                        <div class="vs10"></div>
                        <?
				
			
		foreach($artresult['HITS'] as $rowArt){
			 $s             = $s + 1;
			$articlecat_id = $rowArt["articlecat_id"];
			$article_id    = $rowArt["article_id"];
			$art_subject   = $rowArt["art_subject"];
			$art_body      = $rowArt["art_body"];
			$c_date        = $rowArt["c_date"];
			$artuser       = $rowArt["userid"];
			$art_name      = $rowArt["art_name"];
			
			$art_subject = stripslashes($art_subject);
			$art_subject = breakLongWords($art_subject, 30, " ");
			
			// Get Post Teaser
			$art_body = stripslashes($art_body);
			$art_body = strip_tags($art_body);
			$art_body = trim($art_body);
			$art_body = substr($art_body, 0, 240);
			
			$art_body = stripslashes($art_body);
			$art_body = breakLongWords($art_body, 30, " ");
			$rowUser1 = query_execute_row("SELECT f_name, image FROM users WHERE userid='" . $rowArt['userid'] . "'");
			if ($rowUser1["image"]) {
				$profileimgurl = '/profile/images/' . $rowUser1["image"];
			} else {
				$profileimgurl = '/images/noimg.gif';
			}
			$img_name = $DOCUMENT_ROOT . $profileimgurl;
			$new_w    = 90;
			$new_h    = 70;
			$imgWH    = @WidthHeightImg($img_name, $new_w, $new_h);
		?>
					  <div class="imgtxtcont" <?
    if ($s == 6) {
        echo "style=border:0; padding:0; margin:0;";
    }
?>>
                      
                      <? // function for image for guest ...............
?>
				<?
    guestredirectimage($userid, $rowUser1['f_name'], $artuser, $profileimgurl, $imgWH[0], $imgWH[1])
    // ends here by sagar.......................................
        ;
?>
                
  <div class="txthold" style="width:544px;">
  <h2><a href="<? echo "/$art_name/"; ?>"><? echo "$art_subject"; ?></a></h2>
						   <div class="vs5"></div>
                       <p style="color:#46433d; text-align:justify"><?
    echo "$art_body...  <a href='/$art_name/' class='read'>read more</a> ";
?></p>
							 
							<div class="fl"><p class="caption1" align="left">  
							 <?
    if (($artuser == $userid || $sessionLevel == 1) && $userid != "Guest") {
        echo "<a   href='/new/articles/articles_new.php?article_id=$article_id'>Edit</a>";
?> | <a href="<?
        echo "javascript:confirmDelete('/new/articles/articles.php?article_id=$article_id&del=del&reDirURL=$refUrl')";
?>" style="text-decoration:none;">Delete</a> | <?
    }
    echo "Posted in: <b><a href='/dog-blog/category/$AartCatNice[$articlecat_id]/'>" . $AartCat[$articlecat_id] . "</a></b>";
?> | Posted by: <?
    guestredirect($userid, dispUname($artuser), $artuser);
?></div>
<div class="fr"><p class="caption1" align="right">Date:<?
    print(showdate($c_date, "d M o g:i a"));
?>  | <a href="<?
    echo "/$art_name/";
?>" style="text-decoration:none;">Comments (<?
    echo numCommentsAll($article_id);
?>)</a></div>	</div>     <!--class="txthold" style="width:622px;"-->
                       
					  <div class="cb"></div>
                         </div>  <!-- class="imgtxtcont"  above div         -->
                      <div class="cb"></div>
					  <?  }  // qna filter ...............................?>
                      <div class="fr" style="margin-bottom:5px;">
                    <a href="/dog-blog/search/<?=$shopQuery?>"><img src="/new/pix/button-browse-more.jpg"></a>
                    </div>
                    <div class="vs10"></div>
					 <!-- content --><? } ?>
                 <?php /*?>	   <div style="text-align:center; margin: 5px 0 5px 25px;"><?php
                        require_once($DOCUMENT_ROOT . '/banner1.php');
                        addbanner600('74', '600', '1');
                        ?></div><?php */?>
                        
                        
                        
                    <!---------------------------- QNA Solar -------------------------->    
                          <!-- just in -->
                        <div class="vs30"></div>
                        <div class="cb"></div>
                        <div class="hrline"></div>
  <?                      

		$i = 0; 
		if($totrecordqna>'0'){?>
 						 <div class="cb"></div>
                        <div class="vs10"></div>
							<h3>Related Questions</h3>
                       <div class="cb"></div>
                        <div class="vs10"></div>
               <div class="cb"></div>
        <?
		foreach($resultqna['HITS'] as $rowArt){
			  $i=$i+1;
            $qna_id = $rowArt["qna_id"];
            $qnauser = $rowArt["userid"];
            $qna_question = $rowArt["qna_question"];
            $qna_name = $rowArt["qna_name"];
            $qna_tag = stripslashes($rowArt["qna_tag"]);
            $qna_catid = $rowArt["qna_catid"];
            $qna_cdate = $rowArt["qna_cdate"];
                
           // $qna_question = makeClickableLinks($qna_question);
            $qna_question = nl2br(stripslashes($qna_question));
            $qna_question = breakLongWords($qna_question, 30, " ");
           $rowUser=query_execute_row("SELECT f_name, image FROM users WHERE userid='".$rowArt['userid']."'");
		if($rowUser["image"]){
			$profileimgurl='/profile/images/'.$rowUser["image"];
		}else{
			$profileimgurl='/images/noimg.gif';
		}
    	$img_name = $DOCUMENT_ROOT.$profileimgurl;
    	$new_w = 90;
    	$new_h = 70;
    	$imgWH = @WidthHeightImg($img_name,$new_w,$new_h);
					  ?>
					  <div class="imgtxtcont" <? if($i == 4){ echo "style=border:0; padding:0; margin:0;";} ?>>
                       <? // function for image with no link for guest......... 
			guestredirectimage($userid, $rowUser["f_name"], $rowArt['userid'],$profileimgurl,$imgWH[0],$imgWH[1]);	
			    // function ends here by sagar.......................?>
					
                    
                    <div class="txthold" style="width:600px;text-justify:auto"><a href="<? echo"/qna/$qna_name/";?>"><? echo"$qna_question"; ?></a></b>
     <div class="vs5"></div>
  	<div class="fl"><p class="caption1" align="left">
     <? 
		 if(($qnauser == $userid || $sessionLevel ==1 ) && $userid!="Guest"){ echo"<a href='/new/qna/new.php?qna_id=$qna_id'>Edit</a>";?> | <a href="<? echo"javascript:confirmDelete('?qna_id=$qna_id&del=del')"; ?>">Delete</a> | <? } echo"Posted in: <b><a href='/qna/category/$AqnaCatNice[$qna_catid]/'>".$AqnaCat[$qna_catid]."</a></b>"; ?> | Posted by: 
         <? guestredirect($userid, $rowUser["f_name"],$qnauser) ?></div>
 
<p class="caption1" align="right">date:<? print(showdate($qna_cdate, "d M o g:i a")); ?></a>  | <a href="<? echo"/qna/$qna_name/";?>" style="text-decoration:none;"> reply so far (<?  echo numReply($qna_id);?>)</a> |<a href="<? echo"/qna/$qna_name/";?>">Reply Now</a></p>	
   	</div>     <!--class="txthold" style="width:622px;"-->
    <div class="cb"></div>
   
          </div>  <!-- class="imgtxtcont"  above div         -->
          <div class="cb"></div>
          
						<?	 }if($totrecord == 0){ ?>
     <div id="artpost"> <strong>You have not posted any Question(s) yet.</strong> 
       <br /><br />
       <div class="buttonwrapper"> <a class="ovalbutton" href="/qna/new.php"><span>Start Now!</span></a>  </div>
     </div>
 <? }} ?>
 <div class="fr" style="margin-bottom:5px;">
                    <a href="/qna/search/<?=$shopQuery?>"><img src="/new/pix/button-browse-more.jpg"></a>
                    </div>
 
  <? // }?>
                        <div class="vs15"></div>
                    
                        
                        
                        
                    <!---------------------------- Business Solar -------------------------->   
                     
                        <!-- just in -->
                        <div class="vs30"></div>
                        <div class="cb"></div>
                        <div class="hrline"></div>
                            <?  
		$a = 0; 
		if($totrecordbus>'0'){?>
        
                        <div class="cb"></div>
                        <div class="vs10"></div>
							<h3>Related Business Listing</h3>
                       <div class="cb"></div>
                        <div class="vs10"></div>
       <?
		foreach($resultbus['HITS'] as $rowArt){
			  $a=$a+1;
			   		  $comp_org = $rowArt["comp_org"];
			   		  $bus_nicename = $rowArt["bus_nicename"];
					  $address = $rowArt["address"];
					  $address = stripslashes($address);
					  $address =  breakLongWords($address, 30, " ");
			          $city_name = $rowArt["city_name"];
					  $date = $rowArt["c_date"];
					  $category=$rowArt["category_name1"];
					  $username=$rowArt["userid"];					 
			   		  $country_name = $rowArt["country_name"];
					  $businessuserid = $rowArt["userid"]; 
			   		  $phone_main = $rowArt["phone_main"];
			   		  $website = $rowArt["website"];
					  $photo_album_id = $rowArt["photo_album_id"];
			   		  $video_album_id = $rowArt["video_album_id"];
			   		  $num_review = $rowArt["num_review"];
					  $category_name1 = $rowArt["category_name1"];
					  $cat_nice_name1 = $rowArt["cat_nice_name1"];
					  $category_name2 = $rowArt["category_name2"];
					  $cat_nice_name2 = $rowArt["cat_nice_name2"];
					  $category_name3 = $rowArt["category_name3"];
					  $cat_nice_name3 = $rowArt["cat_nice_name3"];
					  $category_name4 = $rowArt["category_name4"];
					  $cat_nice_name4 = $rowArt["cat_nice_name4"];
					  $category_name5 = $rowArt["category_name5"];
					  $cat_nice_name5 = $rowArt["cat_nice_name5"];
                
           // $qna_question = makeClickableLinks($qna_question);
  			  $rowUser1=query_execute_row("SELECT f_name, image FROM users WHERE userid='". $username."'");
		if($rowUser1["image"]){
			$profileimgurl='/profile/images/'.$rowUser1["image"];
		}else{
			$profileimgurl='/images/noimg.gif';
		}
    	$img_name = $DOCUMENT_ROOT.$profileimgurl;
    	$new_w = 90;
    	$new_h = 70;
    	$imgWH = @WidthHeightImg($img_name,$new_w,$new_h);
?>
<?	echo "<div class='imgtxtcont' id='$a'>"; ?>
				<? // function for image for guest ...............?>
				<?	guestredirectimage($userid, $rowUser1['f_name'], $username,$profileimgurl,$imgWH[0],$imgWH[1]) 
				// ends here by sagar.......................................?>

	<div class="txthold" style="width:600px; ">
    <div class="vs5"></div>
        <h2><a href='<? echo"/dog-listing/$bus_nicename/"; ?>'><? print breakLongWords($comp_org, 11, " "); ?> </a></h2>
						   <div class="vs5"></div>
                       <p style="color:#46433d; text-align:justify"><?
    echo"$address, $city_name, $country_name, $phone_main";;
?></p>
 
 <p class="caption">Posted by: <? guestredirect($userid, dispUname($username),$username) ?> | <? print(showdate($date, "d M o g:i a")); ?> | Category:

		 <a href="<? echo "/dog-listing/category/$cat_nice_name1/"; ?>" ><? echo "$category_name1"; ?></a> 
         <a href="<? echo "/dog-listing/category/$cat_nice_name2/"; ?>" ><? echo "$category_name2"; ?></a> 
         <a href="<? echo "/dog-listing/category/$cat_nice_name3/"; ?>" ><? echo "$category_name3"; ?></a> 
         <a href="<? echo "/dog-listing/category/$cat_nice_name4/"; ?>" ><? echo "$category_name4"; ?></a> 
         <a href="<? echo "/dog-listing/category/$cat_nice_name5/"; ?>" ><? echo "$category_name5"; ?></a>
      </p>  
	  </div>
               </div>
                                      <div class="cb"></div>

               <? }?>
			   <div class="fr" style="margin-bottom:5px;">
                    <a href="/dog-listing/blogsearch/<?=$shopQuery?>"><img src="/new/pix/button-browse-more.jpg"></a>
                    </div>
			   
			   
			   <? }?>
                       <div class="cb"></div>


                <!------------------------------------- Dogs Solar -------------------------------------------->
                       <div class="vs30"></div>
                        <div class="cb"></div>
                        <div class="hrline"></div>
                        
  <?  
		$d = 0; 
		if($totrecorddogs>'0'){?>
        
                        <div class="cb"></div>
                        <div class="vs10"></div>
							<h3>Related Dogs Search</h3>
                       <div class="cb"></div>
                        <div class="vs10"></div>
 <div id="puppiesContainer" style="width:700px">
  <div class="puppiesContainer_header">
    <div class="image headerText_container">Image</div>
    <div class="details headerText_container">Dogs Details</div>
    <div class="breed headerText_container">Breed</div>
    <div class="age headerText_container">Sex</div>
    <div class="location headerText_container">Location</div>
    
  </div>
  <div class="puppiesContainer_body" style="">
    
  <? 
	foreach($resultdogs['HITS'] as $rowgroup){
	$dog_id  = $rowgroup["dog_id"];
	$dog_name  = $rowgroup["dog_name"];
	$dog_color  = $rowgroup["dog_color"];
	$dog_sex = $rowgroup["dog_sex"];
	$dog_image = $rowgroup["dog_image"];
	$dog_owner = $rowgroup["userid"];
	$dog_nicename = $rowgroup["dog_nicename"];
	$breed_nicename = $rowgroup["breed_nicename"];
	$country = $rowgroup["country"];
	$state = $rowgroup["state"];
	$city = $rowgroup["city"];
	$pub_status=$rowgroup['publish_status'];
	$dog_name = stripslashes($dog_name);

$img_src = $DOCUMENT_ROOT."/dogs/images/thumb_$dog_image";

if(file_exists($img_src)){
  $new_w = 100;
  $new_h = 87;
  $imgWH = @WidthHeightImg($img_src,$new_w,$new_h);
}
if($imgWH[0]==''){
	$imgWH[0]='100';
}if($imgWH[1]==''){
	$imgWH[1]='87';
}

?>

 <div class="bodyText_row">
      <div class="image bodyText_container"><? if($dog_image){ ?>
    <a href="<? echo "/dogs/$dog_nicename/"; ?>"><img src="<? echo"/dogs/images/thumb_$dog_image";?>" border="0" alt="<? echo "$ArrDogBreed[$breed_nicename]"; print (dispUname($dog_owner));?>" title="<? echo"$ArrDogBreed[$breed_nicename] | "; print (dispUname($dog_owner));?>" width="<? echo"$imgWH[0]";?>" height="<? echo"$imgWH[1]";?>" /></a>
    <? }else{ ?>
    <a href="<? echo "/dogs/$dog_nicename/"; ?>"><img src="https://www.dogspot.in/images/no-img.jpg" alt="<? echo "$ArrDogBreed[$breed_nicename]  | "; print (dispUname($dog_owner));?>" width="100" height="87" border="0"/></a>
<? } ?></div>
      <div class="details bodyText_container">Dog name: <a href="<? echo "/dogs/$dog_nicename/"; ?>"><? echo"<b>"; print breakLongWords($dog_name, 20, " "); echo"</b>"; ?><br><br> </a>
	
	<?
	if($dog_owner){ ?> Dog Owner: <? guestredirect1($userid, dispUname($dog_owner),$dog_owner) ; }?><br /><br />
    <? if($userid=="Guest"){?>
    <img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> <a href="/login.php"> Contact  <? print (dispUname($dog_owner));?></a>
    <? }else{?>
    <img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> Contact <a href="<? echo "/scrapbook/msgview-post.php?msg_from=$dog_owner";?>" onclick="return hs.htmlExpand(this, { contentId: 'highslide-tc', objectType: 'iframe',	preserveContent: false, objectWidth: 600, objectHeight: 300} )"><? print (dispUname($dog_owner));?></a>
    <? }?>
    
    <? $dogcoment=query_execute_row("SELECT comm_body,c_date FROM `dog_comments` WHERE `dog_id` ='$dog_id' AND userid='admin' ORDER BY comment_id DESC ");
	if($sessionLevel == '1') {?><?=$pub_status; 
	echo $dogcoment['comm_body'] ." | Date: ".$dogcoment['c_date'];
	 } ?>
     <br /><br />
        <? if(($dog_owner == $userid || $sessionLevel ==1) && $userid != "Guest"){?>
    <div id="editBut">
                  <div class="buttonwrapper"><a href="<? echo"javascript:confirmDelete('/dogs/index.php?dog_id=$dog_id&del=del')";?>" class="ovalbutton"><span>Delete</span></a> <a href="<? echo"/dogs/new.php?dog_id=$dog_id";?>" class="ovalbutton"><span>Edit</span></a></div></div>
<? }?></a></span></div>
      <div class="breed bodyText_container"><a href= "<? echo"/dogs/$dog_nicename/";?>" class="imgBoder"><? echo "$ArrDogBreed[$breed_nicename]"; ?></a></div>
      <div class="age bodyText_container"><? if($dog_sex=='M' || $dog_sex=='F'  ){ echo $DogSex[$dog_sex];} else { echo "NA"; }?>
      </div>
      <div class="location bodyText_container" style="">
	  <?
	 if($city){ 	echo"$city<br>";   }
	 if($state){  echo"$state<br>";  }
	 if($country){  echo $countries[$country];  }
    ?></div>
    
    </div>
     <? } ?>
    </div>
    <? }?>
    <div class="cb"></div>
                        <div class="vs10"></div>
    <div class="fr" style="margin-bottom:5px;">
                    <a href="/dogs/dogsearch/<?=$shopQuery?>"><img src="/new/pix/button-browse-more.jpg"></a>
                    </div>


                     <!------------------------------- Puppy Solar ------------------------------>   
                     </div>
                      <div class="vs30"></div>
                        <div class="cb"></div>
                        <div class="hrline"></div>
                        
                  <?   if($totrecorddogs>'0'){?>
        
                        <div class="cb"></div>
                        <div class="vs10"></div>
							<h3>Related Puppy Search</h3>
                       <div class="cb"></div>
                        <div class="vs10"></div>                   
<div id="puppiesContainer" style="width:700px">
  <div class="puppiesContainer_header">
    <div class="image headerText_container">Image</div>
    <div class="details headerText_container">Puppies Details</div>
    <div class="breed headerText_container">Breed</div>
    <div class="age headerText_container">Age</div>
    <div class="location headerText_container">Location</div>
  </div>
           <div class="puppiesContainer_body">
     <?
	foreach($resultpuppy['HITS'] as $rowgroup){
		$puppi_id  = $rowgroup["puppi_id"];
	
		$puppy_owner  = $rowgroup["userid"];

		$puppi_breed  = $rowgroup["puppi_breed"];

		$puppi_img = $rowgroup["puppi_img"];
		$day = $rowgroup["day"];
		$month = $rowgroup["month"];
		$year = $rowgroup["year"];
		$puppy_nicename = $rowgroup["puppy_nicename"];
		$breed_nicename = $rowgroup["breed_nicename"];
		
		$country = $rowgroup["country"];
		$state = $rowgroup["state"];

		$city =$rowgroup["city"];

		$img_src = $DOCUMENT_ROOT."/puppies/images/thumb_$puppi_img";
		
		if(file_exists($img_src)){
		  $imgWH = @WidthHeightImg($img_src,'100','87');
		}
 ?> 
 
<div class="bodyText_row">
      <div class="image bodyText_container">
<? if($puppi_img){ ?>
   <a href="<? echo"/puppies/$puppy_nicename/";?>" ><img src="<? echo"/puppies/images/thumb_$puppi_img";?>" border="0" alt="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),20); ?>" title="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),20); ?>" width="<? echo"$imgWH[0]";?>" height="<? echo"$imgWH[1]";?>" /></a>
<? }else{ ?>
  <a href="<? echo"/puppies/$puppy_nicename/";?>"><img src="https://www.dogspot.in/images/no-img.jpg" alt="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),20); ?>" width="100" height="87" border="0"/></a>
<?  } ?></div>
      <div class="details bodyText_container">Owner: <? guestredirect1($userid, dispUname($puppy_owner),$puppy_owner) ; ?><br /><br />
<? if($userid == "Guest"){ ?>
<b><img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> <a href="<? echo"/login.php";?>">Contact <? print getTeaser(dispUname($puppy_owner),20); ?></a></b>
<? }else{ ?>
<img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> Contact: <a href="<? echo"/scrapbook/msgview-post.php?msg_from=$puppy_owner";?>" onclick="return hs.htmlExpand(this, { contentId: 'highslide-tc', objectType: 'iframe',	preserveContent: false, objectWidth: 600, objectHeight: 300} )"><b><? print getTeaser(dispUname($puppy_owner),20); ?></b></a>
<? } ?>
<? if(($puppy_owner == $userid || $sessionLevel == 1) && $userid != "Guest"){ ?>
    <br />
    <div id="editBut"><div class="buttonwrapper">
    <a href="<? echo"javascript:confirmDelete('/puppies/index.php?puppi_id=$puppi_id&del=del')";?>" class="ovalbutton"><span>Delete</span></a> &nbsp;<a href="<? echo"/puppies/new.php?puppi_id=$puppi_id";?>" class="ovalbutton"><span>Edit</span></a>
    </div></div>
   <? } ?></div>
      <div class="breed bodyText_container"><a href= "<? echo"/puppies/$puppy_nicename/";?>" class="imgBoder"><? echo "$ArrDogBreed[$breed_nicename]"; ?></div>
      <div class="age bodyText_container"><? print (pupDOB($day, $month, $year)); echo"<br>"; print (numPupies($puppi_id)); echo" Puppies"; ?>   </div>
      <div class="location bodyText_container"><?
if($city){
 echo"$city <br>";
}
if($state){
 echo"$state <br>";
}
if($country){
 echo $countries[$country];
}
?></div>

    </div>
    <div class="line"></div>
 <? } ?>

 <div class="vs5"></div>

</div>
<? }?>
  
</div>
   
<div class="cb"></div>
 <div class="vs10"></div>
  
    <div class="fr" style="margin-bottom:5px;">
                    <a href="/puppies/puppysearch/<?=$shopQuery?>"><img src="/new/pix/button-browse-more.jpg"></a>
                    </div>
    <div class="line"></div>

          </div>
                </form><!-- column 2 -->
                
				</div>
                <div class="cb"></div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>   