<div class="cb"></div>
<div class="vs20"></div>

 <link rel="stylesheet" type="text/css" href="/combinefiles/shop-new.css?date=1355838816" />
  
 <link href="/new/css/footer.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script type="text/javascript" src="/new/js/jquery-1.js"></script>


<script language="javascript">
function validateComment(){
	if (document.formArt.comments.value == ""){
		document.getElementById('comments').style.borderColor = "red";
		document.formArt.comments.focus();
		document.getElementById('vali').style.display="block";
		
	}
		else {
		document.getElementById('vali').style.display="none";		
		
		var feedback= document.formArt.comments.value;
		ShaAjaxJquary("/testimonial/insertcomment.php?feedback1="+feedback+"", "#mis", '', 'formArt', 'GET', '#loading', '<img src="/images/indicator.gif" />','REP');
		document.formArt.comments.value== "";	
		
		}
}
	</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>       
     
   <!-- footer -->
  <div id="footer">
  <div id="footerContent">
    <div id="footerContent_left">
      <div style="height:250px; border-bottom:1px dotted #ddd; margin:0 10px 10px 0;">
        <div class="footerContainer" style="border-right:1px dotted #ddd; margin-right:20px; width:250px; height:240px; ">
          <h3>Dog Spot</h3>
          <ul style="width:130px;">
            <li><a href="/"> Home</a></li>
            <li><a href="/shop/"> Shop</a></li>
            <li><a href="/dog-blog/"> Articles</a></li>
            <li><a href="/dog-events/"> Dog Shows</a></li>
            <li><a href="/puppies/"> Puppies Available</a></li>
            <li><a href="/dogs/"> Dog Gallery</a></li>
            <li><a href="/dog-listing/"> Business</a></li>
            <li><a href="/qna/"> Q&amp;A</a></li>
          </ul>
          <ul style="width:120px;">
            <li><a href="/about-us.php"> About Us</a></li>
            <li><a href="/contactus.php"> Contact Us</a></li>
            <li><a href="/press-release.php"> Press Release</a></li>
            <li><a href="/jobs.php"> Careers</a></li>
            <li><a href="https://www.dogspot.in/sitemap.php"> Site Map</a></li>
            <li><a href="https://www.dogspot.in/sitemap.xml"> Sitemap xml</a></li>
            <!--<li><a href="#"> Coupon Dunia</a></li>
            <li><a href="#"> Coupon Guru</a></li>
            <li><a href="#"> Sitemap</a></li>-->
          </ul>
        </div>
        <div class="footerContainer" style="border-right:1px dotted #ddd; margin-right:20px; padding-right:10px; width:130px; height:240px; ">
          <h3>Our Policies</h3>
          <ul>
            <li><a href="/shipping-delivery.php"> Shipping &amp; Delivery</a></li>
            <li><a href="/cancellation-refund.php"> Order Cancellation</a></li>
            <li><a href="/refund-return.php"> Refund &amp; Return</a></li>
            <li><a href="/replacement.php"> Replacement</a></li>
            <li><a href="#"> Pricing Information</a></li>
            <li><a href="/terms-conditions.php"> Terms &amp; Conditions</a></li>
            <li><a href="/privacy-policy.php"> Privacy Policy</a></li>
            <li><a href="/disclaimer-policy.php"> Disclaimer</a></li>
          </ul>
        </div>
        <div class="footerContainer" style="border-right:1px dotted #ddd; margin-right:20px; padding-right:10px; width:130px; height:240px;">
          <h3>Payment Option</h3>
          <ul style="width:150px;">
            <li style="background:url(images/icon-card.png) no-repeat; padding-left:35px; margin-bottom:10px;"><a >Debit Card</a></li>
            <li style="background:url(images/icon-card.png) no-repeat; padding-left:35px; margin-bottom:10px;"><a >Credit Card</a></li>
            <li style="background:url(images/icon-netbanking.png) no-repeat; padding-left:28px; margin-left:7px; margin-bottom:10px;"><a >Netbanking</a></li>
            <li style="background:url(images/icon-cheque.png) no-repeat; padding-left:35px; margin-bottom:10px;"><a >Cheque/Draft</a></li>
            <li style="background:url(images/icon-cod.png) no-repeat; padding-left:35px; margin-bottom:10px;"><a >Cash on Delivery</a></li>
          </ul>
        </div>
        <div class="footerContainer" style=" width:80px; height:250px;">
          <h3>Join Us</h3>
          <ul style="width:100px;">
            <li style="background:url(images/icon-facebook.jpg) no-repeat; padding-left:25px;"><a href="http://www.facebook.com/indogspot" target="_blank">Facebook</a></li>
            <li style="background:url(images/icon-twitter.jpg) no-repeat; padding-left:25px;"><a href="http://twitter.com/#!/indogspot" target="_blank">Twitter</a></li>
            <li style="background:url(images/icon-mail.jpg) no-repeat; padding-left:25px;"><a href="/contactus.php">Mail Us</a></li>
          </ul>
        </div>
      </div>
      <div style="height:250px; margin-right:10px;">
        <div class="footerContainer" style="border-right:1px dotted #ddd; margin-right:20px; width:250px; height:240px; ">
          <h3>Top Brands</h3>
          <ul style="width:125px;">
            <li><a href="https://www.dogspot.in/brand/"> Dog Food Brands</a></li>
            <li><a href="https://www.dogspot.in/Pedigree/"> Pedigree</a></li>
            <li><a href="https://www.dogspot.in/royal-canin/"> Royal Canin</a></li>
            <li><a href="https://www.dogspot.in/Drools/"> Drools Dog Food</a></li>
            <li><a href="https://www.dogspot.in/eukanuba/"> Eukanuba</a></li>
            <li><a href="https://www.dogspot.in/taste-of-wild/"> Taste Of Wild</a></li>
            <li><a href="https://www.dogspot.in/petdig/"> Petdig</a></li>
            <li><a href="https://www.dogspot.in/durapet/"> Durapet</a></li>
          </ul>
          <ul style="width:125px;">
            <li><a href="https://www.dogspot.in/jerhigh/"> Jerhigh</a></li>
            <li><a href="https://www.dogspot.in/FRONTLINE/"> Frontline</a></li>
            <li><a href="https://www.dogspot.in/karlie/"> Karlie</a></li>
            <li><a href="https://www.dogspot.in/kong/"> Kong</a></li>
            <li><a href="https://www.dogspot.in/forbis/"> Forbis</a></li>
            <li><a href="https://www.dogspot.in/simple-solutions/"> Simple Solution</a></li>
            <li><a href="https://www.dogspot.in/Me-O/"> Me-O</a></li>
            <?php /*?><li><a href="#"> Brand Eight</a></li><?php */?>
          </ul>
        </div>
        <div class="footerContainer" style="margin-right:20px; width:280px; height:240px; ">
          <h3>Top Categories</h3>
          <ul style="width:140px;">
            <li><a href="https://www.dogspot.in/treats-food/"> Dog Food</a></li>
            <li><a href="https://www.dogspot.in/dog-collars/"> Dog Collars</a></li>
            <li><a href="https://www.dogspot.in/dog-leashes/"> Dog Leash</a></li>
            <li><a href="https://www.dogspot.in/dog-harnesses/"> Dog Harness</a></li>
            <li><a href="https://www.dogspot.in/accessories/"> Dog Accessories</a></li>
            <li><a href="https://www.dogspot.in/clothes/"> Dog Clothes</a></li>
			<li><a href="https://www.dogspot.in/beds/"> Dog Beds</a></li>
            <li><a href="https://www.dogspot.in/dog-grooming/"> Dog Grooming</a></li>
          </ul>
          <ul style="width:140px;">
            <li><a href="https://www.dogspot.in/shampoos/"> Dog Shampoo</a></li>
            <li><a href="https://www.dogspot.in/cages/"> Dog Cage</a></li>
            <li><a href="https://www.dogspot.in/dog-biscuits/"> Dog Biscuits</a></li>
            <li><a href="https://www.dogspot.in/supplements/"> Dog Supplements</a></li>
            <li><a href="https://www.dogspot.in/crates/"> Dog Crates</a></li>
            <li><a href="https://www.dogspot.in/flea-ticks/"> Dog Ticks</a></li>
            <li><a href="https://www.dogspot.in/dog-toy/"> Dog Toys</a></li>
            <li><a href="https://www.dogspot.in/dog-meaty-treats/"> Dog Meat</a></li>
                      </ul>
        </div>
      </div>
      <div><img src="images/puppies.jpg" width="685" height="298" /></div>
    </div>
    <div id="footerContent_right">
      <div id="feedbackContainer">
        <?php require_once($DOCUMENT_ROOT.'/testimonial/testimonia-box-new.php'); ?>
        <div  class="feedback">
          <h1>Your Feedback Please</h1>
          <form id="formArt" name="formArt"  action="" >
         <?php /*?><? if($userid=="Guest"){ ?><label>Name:
              <input type="text" name="name" id="name" />
            </label>
            <br /><? } ?><?php */?>
            
           <textarea name="comments" style="width:220px;height:80px;" placeholder="Enter Your Feedback here" id="comments" ></textarea><br /> <br />
           <div id="vali"  style="display:none;"><span class="error">Please enter your feedback.</span></div>
            

  			<a href="javascript:validateComment()"><img src="images/button-submit.jpg" /></a>
  
	</form>
    <div id='loading'></div>
                          <div id='mis'></div>
        </div>
      </div>
      <div class="fb-like-box" data-href="http://www.facebook.com/indogspot" data-width="250" data-show-faces="true" data-stream="false" data-header="true"></div>
    </div>
  </div>
  <div id="footerGreen">
    <p>Views and Articles are not endorsed by DogSpot.in. DogSpot does not assume responsibility or liability for any Comment or for any claims,<br />
    damages, or losses resulting from any use of the Site or the materials contained therein. All contributions and Articles are owned by DogSpot.in.</p>
  </div>
</div>
<!-- main container -->
<!-- popup Box Start--> 
<?php require_once($DOCUMENT_ROOT.'/subscribe-popup/subscribe.php'); ?>
<!-- popup Box END-->
<script type="text/javascript">
adroll_adv_id = "MHSX72GMJNG3XDYEPAV745";
adroll_pix_id = "GKKCEQKQPNCLRPNCNXRH6F";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>
</body>
</html>
<?
$UserRequested = empty($_SERVER['REQUEST_URI']) ? false : $_SERVER['REQUEST_URI'];
recordOnlineUser($userid, $sessionName, $ip, $UserRequested);
// Record user END
//Remove User/
removeOnlineUsers();
//Remove User/END
?>