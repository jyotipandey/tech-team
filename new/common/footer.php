

<!-- footer -->
   	<div id="footer">
    	<!-- left container -->
   	  <div class="fcont300">
        	<h2>quick links</h2>
            <div class="vs10"></div>
            <ul>
           	  <li>
                <a href="/">Home</a>
                <a href="/shop/">Shop</a>
                <a href="/jobs.php">Careers</a>
                <a href="/dog-blog/">Articles</a>
                <a href="/dog-events/">Dog Shows</a>
                
                <a href="/puppies/">Puppies Available</a>
                <a href="/dogs/">Dog Gallery</a>
                
              </li>
              
              <li>
       
                <a href="/qna/">Questions & Answer</a>
                <a href="/dog-listing/">Business</a>
                <a href="/shop/contactus.php">Contact Us</a>
                 <a href="/shop/about-us.php">About Us</a>
                <a href="/shop/press-release.php">Press Release</a>
                <a href="http://www.coupondunia.in/" target="_blank">CouponDunia</a>
              </li>
            </ul>
          <span class="cl"></span>        
      </div><!-- left container -->
          
          <!-- middle container -->
          
          <div class="fcont300" >
          	<h2>shipping information</h2>
            <div class="vs10"></div>
            
            <ul>
           	  <li>
                <a href="/shop/shipping-delivery.php">Shipping & Delivery Policy</a>
                <a href="/shop/cancellation-refund.php">Order Cancellation</a>
                <a href="/shop/replacement.php">Replacement</a>
                <a href="/shop/refund-return.php">Refund & Return</a>
                
              </li>
              
              <li>
              <a href="/shop/terms-conditions.php">Terms & Conditions</a>
                <a href="/shop/disclaimer-policy.php">Disclaimer Policy</a>
                <a href="/shop/privacy-policy.php">Privacy Policy</a>
                
                
              </li>
              
            
            </ul>
            <span class="cl"></span>
            
      </div><!-- middle container -->
            
            <!-- right container -->
            
            <div class="fcont300" style="margin-right:0px; padding-right:0px; border:0px;">
             <h2>customer care</h2>
             <div class="vs10"></div>
             <p class="btn"><span><a href="javascript:void();">+91-9818809553</a></span></p>
             <div class="vs10"></div>
            <h2>payment gateway</h2>
            <div class="vs10"></div>
            <p><img src="/new/pix/mastercard.gif" alt="" title="" border="0" class="mr5" /><img src="/new/pix/maestro_icon.gif" alt="" title="" border="0" class="mr5"  /><img src="/new/pix/visa_icon.gif" alt="" title="" border="0"  /></p>
            
            
            
      </div><!-- right container -->
          
        <div class="cb"></div>
    </div><!-- footer -->
    
    <div class="vs20"></div>
    <div class="hrline"></div>
    <div class="vs20"></div>
    
    <!-- footer2 -->
    <div class="fl f2cont750">

    <p align="left">
    Views and Articles are not endorsed by DogSpot.in. DogSpot does not assume responsibility or liability for any Comment or for any claims, damages, or losses resulting from any use of the Site or the materials contained therein. All contributions and Articles are owned by DogSpot.in. </p>
    
    </div>

    <div class="fr">
    <p align="right">
    <a href="http://www.facebook.com/indogspot" target="_blank" rel="nofollow"><img src="/new/pix/fb_icon.gif" alt="Facebook" title="Facebook" border="0" rel="nofollow"  /></a><a href="http://twitter.com/#!/indogspot" target="_blank" rel="nofollow" ><img src="/new/pix/twitter_icon.gif" alt="Twitter" title="Twitter" border="0" rel="nofollow" /></a></p>
    </div><!-- footer 2-->