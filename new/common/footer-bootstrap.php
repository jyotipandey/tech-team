<section class="footer-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="footerContainer" >
          <h5>DogSpot</h5>
          <ul>
            <li><a href="/"> Home</a></li>
            <li><a href="/sales/"> Sales</a></li>
            <li><a href="/dog-blog/"> Articles</a></li>
            <li><a href="/dog-events/"> Dog Shows</a></li>
            <li><a href="/dogs/"> Wag Club</a></li>
            <li><a href="/dog-listing/"> Classifieds</a></li>
            <li><a href="/qna/"> Q&amp;A</a></li>
            <li><a href="/dog-breeds/">Dog Breeds</a></li>
          </ul>
          <ul >
            <li><a href="/about-us.php"> About Us</a></li>
            <li><a href="/contactus.php"> Contact Us</a></li>
            <li><a href="/press-release/"> Press Release</a></li>
            <li><a href="/jobs.php"> Careers</a></li>
            <li><a href="/sitemap.php"> Site Map</a></li>
            <li><a href="/sitemap.xml"> Sitemap XML</a></li>
            <li><a href="/adoption/"> Adoption</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="footerContainer" >
          <h5>Our Policies</h5>
          <ul>
            <li><a href="/shop-policy.php#shipping"> Shipping &amp; Delivery</a></li>
            <li><a href="/shop-policy.php#cancelation"> Order Cancellation</a></li>
            <li><a href="/shop-policy.php#refund"> Refund &amp; Return</a></li>
            <li><a href="/shop-policy.php#replacement"> Replacement</a></li>
            <li><a href="/shop-policy.php#pricing"> Pricing Information</a></li>
            <li><a href="/terms-conditions.php"> Terms &amp; Conditions</a></li>
            <li><a href="/shop-policy.php#privacy"> Privacy Policy</a></li>
            <li><a href="/terms-conditions.php#disclaimer"> Disclaimer</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="footerContainer" >
          <h5>Top Breeds</h5>
          <ul >
            <li><a href="https://www.dogspot.in/german-shepherd-dog-alsatian/"> German Shepherd</a></li>
            <li><a href="https://www.dogspot.in/rottweiler/">Rottweiler</a></li>
            <li><a href="https://www.dogspot.in/tibetan-mastiff/">Tibetan Mastiff</a></li>
            <li><a href="https://www.dogspot.in/golden-retriever/">Golden Retriever</a></li>
            <li><a href="https://www.dogspot.in/labrador-retriever/">Labrador Retriever</a></li>
            <li><a href="https://www.dogspot.in/great-dane/">Great Dane</a></li>
            <li><a href="https://www.dogspot.in/dobermann/">Doberman</a></li>
            <li><a href="https://www.dogspot.in/pug/"> Pug</a></li>
            <li><a href="https://www.dogspot.in/beagle/">Beagle</a></li>
          </ul>
          <ul >
            <li><a href="https://www.dogspot.in/siberian-husky/">Siberian Husky</a></li>
            <li><a href="https://www.dogspot.in/pomeranian/">Pomeranian</a></li>
            <li><a href="https://www.dogspot.in/shih-tsu/">Shih tzu</a></li>
            <li><a href="https://www.dogspot.in/dalmatian/">Dalmatian</a></li>
            <li><a href="https://www.dogspot.in/dachshund-standard-smooth-haired/">Dachshund</a></li>
            <li><a href="https://www.dogspot.in/stbernard/">St.Bernard</a></li>
            <li><a href="https://www.dogspot.in/chihuahua-long-smooth-coat/">Chihuahua</a></li>
            <li><a href="https://www.dogspot.in/afghan-hound/">Afghan Hound</a></li>
            <li><a href="https://www.dogspot.in/akita/">Akita</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="footerContainer" >
          <h5>Top Brands</h5>
          <ul>
            <li><a href="https://www.dogspot.in/dogspot/">DogSpot</a></li>
            <li><a href="https://www.dogspot.in/solid-gold/">Solid Gold</a></li>
            <li><a href="https://www.dogspot.in/urine-off/"> Urine Off</a></li>
            <li><a href="https://www.dogspot.in/pedigree/"> Pedigree</a></li>
            <li><a href="https://www.dogspot.in/royal-canin/"> Royal Canin</a></li>
            <li><a href="https://www.dogspot.in/hills/">Hill’s Science Plan</a></li>
            <li><a href="https://www.dogspot.in/jerhigh/"> Jerhigh</a></li>
            <li><a href="https://www.dogspot.in/pethead/"> PetHead</a></li>
            <li><a href="https://www.dogspot.in/furminator/">Furminator</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="bottomFoot_tb">
    <div class="container  pd-tb20">
      <div class="col-md-3 text-center">
        <label class="bottom-line-text">3000+ Products<br>
          170+Brands</label>
      </div>
      <div class="col-md-3 text-center">
        <label class="bottom-line-text">Delivery<br />
          Across India</label>
      </div>
      <div class="col-md-3 text-center">
        <label class="bottom-line-text">Free 30-day<br />
          Return</label>
      </div>
      <div class="col-md-3 text-center">
        <label class="bottom-line-text">Free Pet Expert<br />
          Service</label>
      </div>
    </div>
    <div class="container ">
      
      <div class="col-md-12 text-center"> <img src="/bootstrap/images/bank-icons.jpg" class="img-responsive"  width="424" height="44" style="margin:auto"  /> </div>
      
    </div>
  </div>
</section>

<section class="compy-right-sec">
<div class="container">
<div class="row">
<div class="col-md-10">
<p>
Views and Articles are not endorsed by DogSpot.in. DogSpot does not assume responsibility or liability for any Comment or for any claims, damages, or losses resulting from any use of the Site or the materials contained therein. 
All contributions and Articles are owned by DogSpot.in.
</p>
</div>
<div class="col-md-2">
<img src="/bootstrap/images/dcma-logo.png" alt="Dmca" title="Dmca" width="149" height="31" />
</div>
</div>
</div>
</section>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button"  data-toggle="tooltip" data-placement="left"><i class="fa fa-angle-up"></i></a> 
 <script>
  $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

});
   </script>
</body>
</html>
