<script>
$(document).ready(function(){
	
	$('ul.tabs-nav li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs-nav li').removeClass('current');
		$('.nav-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})
</script>
<?
 $tab_def=array("birds","small-pets","fish","sales","monthly-essential-packs","pet-lovers-gallery");
 $tab_name=array("birds","small-pets","fish","sales","monthly-essential-packs","pet-lovers-gallery","dog-breeds","dog-blog","qna","dog-listing","dog-events","wag_club","adoption","wagfund","wagtag-info","animal-activist","dog-names");
 $commu_arr=array("dog-breeds","dog-blog","qna","dog-listing","dog-events","wag_club","adoption","wagfund","wagtag-info","animal-activist","dog-names");
 $cat_array=array("dry-cat-food","wet-cat-food","cat-food-and-treats","cat-shampoos-and-conditioners","cat-shedding-control","cat-eye-care","cat-bath-accessories","cat-grooming-tools","cat-dry-bathing","cat-brushes-and-combs","cat-grooming","cat-stain-remover","cat-odor-remover","cat-fleas-and-ticks","cat-cleaning","cat-crates","cat-cages","cat-health-and-care","cat-litter-boxes","cat-litter-accessories","cat-litter","cat-ball-toys","cat-interactive-toys","cat-plush-toys","cat-teasers-and-wands","cat-scratcher-toys","cat-collars","cat-harness","cat-toys","cat-bowls","cat-feeders","cat-bowls-and-feeders","cat-beds","cat-furniture","cat-scratchers","cat-doors","cat-bath-accessories","cat-pet-tags","cat-magazines","cat-food-and-treats","cat-training-aids","cat-repellents","cat-training","cat-crates-and-cages");
 $tab_fish=array("fish-food", "aquarium-filter", "aquarium-co2-system", "aquarium-accessories", "aquarium-air-pumps","fish-aquarium-decoration", "fish-aquarium-water-conditioning", "aquarium-freshwater-substrate", "fish-aquarium-plant-nutrients", "aquarium-maintenance");
 $tab_smallpet=array("small-pet-food-treats", "supplement-cleaning", "habitat-accessories", "litter-bedding", "small-pet-supplement", "feeding-accessories","turtle-food", "supplement-cleaning");
$tab_birds=array("bird-food", "bird-toys", "bird-supplement", "bird-cages", "bird-feeding-accessories");
$tab_people=array("magazine", "bags", "others-dog", "tshirt");
 $keyword=str_replace('/','',$_SERVER['REQUEST_URI']);
?>

<div class="ds-header-new_cont"> 
  <!-- top nav tab start-->
  <ul class="tabs-nav">
    <li class="tab-link <? if(!in_array($keyword,$tab_def) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_people) && !in_array($keyword,$tab_birds) && !in_array($keyword,$tab_fish) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_smallpet)){echo 'current';}else{echo '';}?>" data-tab="navtab-1">Dog</li>
    <li class="tab-link <? if(in_array($keyword,$cat_array) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish)){echo 'current';}else{echo '';} ?>" data-tab="navtab-2">Cat</li>
    <li class="tab-link <? if(in_array($keyword,$tab_birds) && !in_array($keyword,$cat_array) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish) && !in_array($keyword,$tab_smallpet)){echo 'current';}else{echo '';}?>" data-tab="navtab-6">Birds</li>
    <li class="tab-link <? if(in_array($keyword,$tab_smallpet) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_birds) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish)){echo 'current';}else{echo '';}?>" data-tab="navtab-5">Small Pet</li>
    <li class="tab-link <? if(in_array($keyword,$tab_fish) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_def)){echo 'current';}else{echo '';}?>" data-tab="navtab-4">Fish</li>
    <a href="/sales/">
    <li class="tab-link <? if(str_replace('/','',$_SERVER['REQUEST_URI'])==='sales'){echo 'current';}else{echo '';}?>">Sale</li>
    </a> <a href="/monthly-essential-packs/">
    <li class="tab-link <? if(str_replace('/','',$_SERVER['REQUEST_URI'])==='monthly-essential-packs'){echo 'current';}else{echo '';}?>"> Smart Deals</li>
    </a>
    <li class="tab-link <? if(in_array($keyword,$tab_people) && !in_array($keyword,$cat_array) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish) && !in_array($keyword,$tab_smallpet)){echo 'current';}else{echo '';}?>"" data-tab="navtab-7">People</li>
    <li class="tab-link <? if(in_array($keyword,$commu_arr) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish)){echo 'current';}else{echo '';} ?>" data-tab="navtab-3"> Community </li>
  </ul>
  <!-- top nav tab start end--> 
  
  <!-- dog nav start-->
  <div id="navtab-1" class="nav-content <? if(!in_array($keyword,$tab_def) && !in_array($keyword,$tab_people) && !in_array($keyword,$tab_fish) && !in_array($keyword,$tab_birds) && !in_array($keyword,$commu_arr) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_smallpet)){echo 'current';}else{echo '';}?>">
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a href="/dog-food/" >Food</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/dry-dog-food/">Dry Dog Food</a></li>
                    <li><a  href="/canned-dog-food/">Canned Dog Food</a></li>
                    <li><a  href="/prescription-dog-food/">Prescription Dog Food</a></li>
                    <li><a  href="/weaning-food/">Weaning Food</a></li>
                    <li><a  href="/food-toppings/">Food Toppings</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                  	<li><a  href="/eukanuba_brand/">Eukanuba</a></li>
                    <li><a  href="/royal-canin/#filter=filter&record=&category=15-173-239-">Royal Canin</a></li>
                    <li><a  href="/pedigree/#filter=filter&record=&category=15-">Pedigree</a></li>
                    <li><a  href="/hills/">Hill's</a></li>
                    <li><a  href="/cibau/">Cibau</a></li>
                  </ul>
                </div>
                <div class="view-all-products"> <a href="/dog-food/">View All Dog Food Products</a> </div>
              </div>
            </li>
            <li> <a href="/biscuits-treats/">Biscuits & Treats</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/chicken-treats/">Chicken Treats</a></li>
                    <li><a  href="/other-meaty-treats/">Other Meaty Treats</a></li>
                    <li><a  href="/veg-treats/">Fruits & Vegetables Treats</a></li>
                    <li><a  href="/dog-dental-treats/">Dental Treats</a></li>
                    <li><a  href="/dog-biscuits/">Biscuits</a></li>
                    <li><a  href="/bakery-products/">Bakery Products</a></li>
                    <li><a  href="/rawhide/">Rawhide</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li> 
                    <a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=250|19|247|252|18|20|&sale=dogspot">DogSpot</a> </li>
                     <li><a  href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=250|19|247|252|18|20|&sale=dogspot">Nibbles</a></li>
                 
                     
                  </ul>
                </div>
                <div class="view-all-products"> <a href="/biscuits-treats/">View All Biscuits & Treats Products</a> </div>
              </div>
            </li>
            <li><a href="/dog-grooming/" >Grooming</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/shampoos/">Shampoos</a></li>
                    <li><a  href="/conditioners/">Conditioners</a></li>
                    <li><a  href="/brushes-combs/">Brushes & Combs</a></li>
                    <li><a  href="/grooming-tools/">Grooming Tools</a></li>
                    <li><a  href="/towel-accessories/">Towel & Accessories</a></li>
                    <li><a  href="/dry-bathing/">Dry Bathing</a></li>
                    <li><a  href="/deodorizers/">Deodorizers</a></li>
                    <li><a  href="/shedding-control/">Shedding Control</a></li>
                    <li><a  href="/grooming-tables-bath-tubs/">Grooming Tables & Bath Tubs</a></li>
                    <li><a  href="/hair-nail-trimming/">Hair & Nail Trimming</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=40|58|44|45|255|253|&sale=dogspot">DogSpot</a></li>
                    <li><a  href="/furminator/">Furminator</a></li>
                    <li><a  href="/wahl/">Wahl</a></li>
                    <li><a  href="/andis/">Andis</a></li>
                    <li><a  href="/forbis/">Forbis</a></li>
                    <li><a  href="/bio-groom/">Bio-groom</a></li>
                    <li><a  href="/beaphar/">Beaphar</a></li>
                  </ul>
                </div>
                <div class="view-all-products"><a href="/dog-grooming/">View All Grooming Products</a></div>
              </div>
            </li>
            <li><a href="/health-wellness/">Health & Wellness</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dental-care/">Dental Care</a></li>
                    <li><a href="/dewormer/">Dewormer</a></li>
                    <li><a href="/supplements/">Supplements</a></li>
                    <li><a href="/dog-puppy-care/">Puppy Care</a></li>
                    <li><a href="/health-supplies-support/">Health Supplies & Support</a></li>
                    <li><a href="/eye-care/">Eye Care</a></li>
                    <li><a href="/ear-care/">Ear Care</a></li>
                    <li><a href="/skin-coat-care/">Skin & Coat Care</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=84|86|&sale=dogspot">DogSpot</a></li>
                    <li><a href="/bayer/">Bayer</a></li>
                    <li><a href="/cipla/">Cipla</a></li>
                    <li><a href="/virbac/">Virbac</a></li>
                    <li><a href="/himalaya/">Himalaya</a></li>
                    <li><a href="/beaphar/">Beaphar</a></li>
                  </ul>
                </div>
                <div class="view-all-products"><a href="/health-wellness/">View All Health & Wellness Products</a></div>
              </div>
            </li>
            <li><a href="/dog-bowls/"> Bowls & Feeders</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/feeders/">Feeders</a></li>
                    <li><a href="/steel-bowls/">Steel & Plastic Bowls</a></li>
                    <li><a href="/adjustable-bowl/">Elevated & Adjustable Bowls</a></li>
                    <li><a href="/clamp-bowls/">Clamp Bowls</a></li>
                    <li><a href="/slow-feeding-bowl/"> Slow Feeding Bowls</a></li>
                    <li><a href="/puppy-feeders/">Puppy Feeder</a></li>
                   </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=85|266|236|232|277|&sale=dogspot">DogSpot</a></li>
                  
                  </ul>
                </div>
                <div class="view-all-products"> <a href="/dog-bowls/">View All Bowls & Feeders Products</a> </div>
              </div>
            </li>
            <li><a href="/collars-leashes/">Collars & Leashes</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dog-collars/">Collars</a></li>
                    <li><a href="/dog-leashes/">Leashes</a></li>
                    <li><a href="/dog-harnesses/">Harnesses</a></li>
                    <li><a href="/chaincollars/">Chain Collars </a></li>
                    <li><a href="/retractable-leashes/">Retractable Leashes</a></li>
                    <li><a href="/show-leashes/">Show Leashes</a></li>
                    <li><a href="/muzzles/">Muzzles</a></li>
                    <li><a href="/recovery-collars-cones/">Recovery Collars & Cones</a></li>
                    <li><a href="/pet-tag/">Pet Tag</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=241|1|2|54|&sale=dogspot">DogSpot</a></li>
                    <li><a href="/petmate/">Petmate</a></li>
                    <li><a href="/flexi/">Flexi</a></li>
                    <li><a href="/karlie/">Karlie</a></li>
                    <li><a href="/trixie/">Trixie</a></li>
                    <li><a href="/ferplast/">Ferplast</a></li>
                  </ul>
                </div>
                <div class="view-all-products"><a href="/collars-leashes/">View All Collars & Leashes Products</a></div>
              </div>
            </li>
            <li><a href="/dog-toy/">Toys</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/chew-toys/">Chew Toys</a></li>
                    <li><a href="/soft-toys/">Soft Toys</a></li>
                    <li><a href="/interactive-dog-toys/">Interactive Toys</a></li>
                    <li><a href="/ball-toys/">Ball Toys</a></li>
                    <li><a href="/squeaker-toy/">Squeaker Toys</a></li>
                     <li><a href="/leather-toys/">Leather Toys</a></li>
                    <li><a href="/bone-toys/">Bone Toys</a></li>
                    <li><a href="/rubber-toys/">Rubber Toys</a></li>
                    <li><a href="/dental-toys/">Dental Toys</a></li>
                    <li><a href="/rope-dog-toys/">Rope & Jute Toys</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=22|24|271|&sale=dogspot">DogSpot</a></li>
                   <li><a href="/gnawsome/">Gnawsome</a></li>
                   
                  </ul>
                </div>
                <div class="view-all-products"><a href="/dog-toy/">View All Toys Products</a></div>
              </div>
            </li>
            <li><a href="/flea-ticks/"> Flea & Ticks</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/powders/">Powders</a></li>
                    <li><a href="/flea-ticks-collar/">Collars</a></li>
                    <li><a href="/flea-tick-shampoos/">Shampoos</a></li>
                    <li><a href="/spot-treatments/">Spot on Treatments</a></li>
                    <li><a href="/combs-accessories/">Combs & Accessories</a></li>
                    <li><a href="/sprays/">Sprays</a></li>
                    <li><a href="/soaps/">Soaps</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/savavet/">Savavet</a></li>
                    <li><a href="/bayer/">Bayer</a></li>
                    <li><a href="/himalaya/">Himalaya</a></li>
                    <li><a href="/cipla/">Cipla</a></li>
                  </ul>
                </div>
                <div class="view-all-products"><a href="/flea-ticks/">View All Flea & Ticks</a></div>
              </div>
            </li>
            <li><a href="/dog-clean-up/">Cleaning & Potty</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/odour-remover/">Odour & Stain Remover</a></li>
                    <li><a href="/diapers/">Diapers</a></li>
                    <li><a href="/pet-hair-remover/">Pet Hair Removal</a></li>
                    <li><a href="/potty-bags-dispensers/">Potty Bags & Dispensers</a></li>
                    <li><a href="/potty-scoopers/">Potty Scoopers</a></li>
                    <li><a href="/training-pads/">Potty Training Pads & Trays</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=52|&sale=dogspot">DogSpot</a></li>
                    <li><a href="/urine-off/">Urine Off</a></li>
                    <li><a href="/spotty/">Spotty</a></li>
                    <li><a href="/out/">Out</a></li>
                    <li><a href="/bayer/">Bayer</a></li>
                    <li><a href="/karlie/">Karlie</a></li>
                  </ul>
                </div>
                <div class="view-all-products"><a href="/dog-clean-up/">View All Cleaning & Potty Products</a></div>
              </div>
            </li>
            <li><a href="/crates-beds/"> Crates, Cages & Beds</a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/crates/">Crates</a></li>
                    <li><a href="/cages/">Cages</a></li>
                    <li><a href="/dog-house/">Dog House</a></li>
                    <li><a href="/car-travel-accessories/">Car Travel Accessories</a></li>
                    <li><a href="/beds/">Beds</a></li>
                    <li><a href="/bedding/">Bedding</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=259|62|67|65|104|&sale=dogspot">DogSpot</a></li>
                    <li><a href="/super-dog/">Super Dog</a></li>
                    <li><a href="/petmate/">Petmate</a></li>
                    <li><a href="/all4pets/">All4pets</a></li>
                    <li><a href="/ferplast/">Ferplast</a></li>
                    <li><a href="/savic/">Savic</a></li>
                  </ul>
                </div>
                <div class="view-all-products"><a href="/crates-beds/">View All Crates, Cages & Beds Products</a></div>
              </div>
            </li>
            <li><a href="/dog-training-behavior/"> Training</a>
              <div class="drop-container-dog drop-nav-right">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/training-leashes/">Training Leash</a></li>
                    <li><a href="/training-aids/">Training Aids</a></li>
                    <li><a href="/training-clickers-whistles/">Training Clickers & Whistles</a></li>
                    <li><a href="/training-harness/">Training Harnesses</a></li>
                    <li><a href="/agility/">Agility Equipment</a></li>
                    <li><a href="/education/">Education</a></li>
                    <!-- <li><a href="/training-treats/">Training Treats</a></li>-->
                    <li><a href="/training-pads/">Potty Training Trays and pads</a></li>
                    <!--<li><a href="/training-collars/">Training Collars</a></li>-->
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=3|54|52|280|&sale=dogspot">DogSpot</a></li>
                    <li><a href="/spotty/">Spotty</a></li>
                    <li><a href="/andis/">Andis</a></li>
                    <li><a href="/bayer/">Bayer</a></li>
                    <li><a href="/beaphar/">Beaphar</a></li>
                  </ul>
                </div>
                <div class="view-all-products"><a href="/dog-training-behavior/">View All Training Products</a></div>
              </div>
            </li>
            <li><a href="/clothing-accessories/" > Clothing  & Accessories</a>
              <div class="drop-container-dog drop-nav-right">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dog-tshirt/">T-Shirts</a></li>
                    <li><a href="/bandana/">Bandanas</a></li>
                    <li><a href="/cooling-dog-coat/">Cooling Coats</a></li>
                    <li><a href="/paw-protection/">Shoes and Socks</a></li>
                    <li><a href="/sweaters/">Sweaters</a></li>
                    <li><a href="/coats-jackets/">Coats & Jackets</a></li>
                     <li><a href="/raincoat/">Raincoats</a></li>
                  </ul>
                </div>
                <div class="drop-list-brand">
                  <div class="shob-by-brands">Shop By Brands</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dogspot/?shopQuery=&filter=filter&category_id=&item_brand=undefined23-&s_price=Rs1-Rs92083&show1=1&record=&categoryid23=30|262|285|29|278|&sale=dogspot">DogSpot</a></li>
                    <li><a href="/huft/">Huft</a></li>
                    <li><a href="/hyperkewl/">Hyperkewl</a></li>
                    <li><a href="/trixie/">Trixie</a></li>
                  </ul>
                </div>
                <div class="view-all-products"> <a href="/clothing-accessories/">View All Clothing  & Accessories Products</a> </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- dog nav end--> 
  
  <!-- cat nav start-->
  
  <div id="navtab-2" class="nav-content <? if(in_array($keyword,$cat_array) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && $_SERVER['REQUEST_URI']!='/'){echo 'current';}else{echo '';} ?>">
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a href="/cat-food-and-treats/">Foods & Treats</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dry-cat-food/">Dry Cat Food</a></li>
                    <li><a  href="/wet-cat-food/">Wet Cat Food</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-food-and-treats/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-grooming/">Grooming</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-shampoos-and-conditioners/">Shampoos & Conditioners</a></li>
                    <li><a  href="/cat-shedding-control/">Shedding Control</a></li>
                    <li><a  href="/cat-eye-care/">Eye Care</a></li>
                    <li><a  href="/cat-bath-accessories/">Bath Accessories</a></li>
                    <li><a  href="/cat-grooming-tools/">Grooming Tools</a></li>
                    <li><a  href="/cat-dry-bathing/">Dry Bathing</a></li>
                    <li><a  href="/cat-brushes-and-combs/">Brushes & Combs</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-grooming/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-cleaning/">Cleaning</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-stain-remover/">Stain Remover</a></li>
                    <li><a  href="/cat-odor-remover/">Odor Remover</a></li>
                    <li><a  href="/cat-fleas-and-ticks/">Fleas And Ticks</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-cleaning/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-crates-and-cages/">Crates & Cages</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-crates/">Cat Crates</a></li>
                    <li><a  href="/cat-cages/">Cat Cages</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-crates-and-cages/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-health-and-care/">Healthcare</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-dewormer/">Cat Dewormer</a></li>
                    <li><a  href="/cat-supplements/">Supplements</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-health-and-care/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-litter/">Litter</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-litter/">Cat Litter</a></li>
                    <li><a  href="/cat-litter-boxes/">Cat Litter Box</a></li>
                    <li><a  href="/cat-litter-accessories/">Litter Accessories</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-litter/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-toys/">Toys</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-ball-toys/">Cat Ball Toys</a></li>
                    <li><a  href="/cat-interactive-toys/">Interactive Cat Toys</a></li>
                    <li><a  href="/cat-plush-toys/">Cat Plush Toys</a></li>
                    <li><a  href="/cat-teasers-and-wands/">Teasers & Wands</a></li>
                    <li><a  href="/cat-scratcher-toys/">Scratcher Toys</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-toys/">View All  Products</a></div>
              </div>
            </li>
            <li><a href="/cat-bowls-and-feeders/">Bowls & Feeders</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-bowls/">Bowls</a></li>
                    <li><a  href="/cat-feeders/">Feeders</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-bowls-and-feeders/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-training/">Training</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-training-aids/">Training Aids</a></li>
                    <li><a  href="/cat-repellents/">Repellents</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-training/">View All Products</a> </div>
              </div>
            </li>
            <li><a href="/cat-bath-accessories/"> Accessories</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-beds/">Beds</a></li>
                    <li><a  href="/cat-furniture/">Cat Furniture</a></li>
                    <li><a  href="/cat-scratchers/">Cat Scratchers</a></li>
                    <li><a  href="/cat-doors/">Cat Doors</a></li>
                  </ul>
                </div>
                <div class="view-all-products-cat"> <a href="/cat-bath-accessories/">View All Products</a> </div>
              </div>
            </li>
            <li><a > More</a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/cat-pet-tags/">Cat Pet Tags</a></li>
                    <li><a  href="/cat-magazines/">Magazines</a></li>
                    <li><a  href="/cat-collars/">Collars</a></li>
                    <li><a  href="/cat-harness/">Cat Harness</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="drop-nav-space-cat">&nbsp;</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- cat nav end--> 
  
  <!-- fish -->
  <div id="navtab-4" class="nav-content <? if(in_array($keyword,$tab_fish) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_def) && $_SERVER['REQUEST_URI']!='/'){echo 'current';
}else{echo '';} ?>">
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a href="/fish-food/"><span>Food</span></a></li>
            <li><a href="/aquarium-filter/"><span>Filters</span></a></li>
            <li><a href="/aquarium-co2-system/"><span> CO2 System</span></a></li>
            <li><a href="/aquarium-accessories/"><span> Accessories</span></a></li>
            <li><a href="/aquarium-air-pumps/"> <span>Air Pumps</span></a></li>
            <li><a href="/fish-aquarium-decoration/"><span>Decoration</span></a></li>
            <li><a href="/fish-aquarium-water-conditioning/"><span>Water Conditioning</span></a></li>
            <li><a href="/aquarium-freshwater-substrate/"><span>Freshwater Substrate</span></a></li>
            <li><a href="/fish-aquarium-plant-nutrients/"><span>Plant Nutrients</span></a></li>
            <li><a href="/aquarium-maintenance/"> <span>Maintenance Products</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- fish --> 
  
  <!-- small pet -->
  <div id="navtab-5" class="nav-content <? if(in_array($keyword,$tab_smallpet) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_birds) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish)){echo 'current';}else{echo '';}?>">
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a href="/small-pet-food-treats/"><span>Food & Treats</span></a></li>
            <li><a href="/small-pet-supplement/"> <span> Supplement</span></a></li>
            <li><a href="/feeding-accessories/"><span>Feeding Accessories</span></a></li>
            <li><a href="/habitat-accessories/"><span> Habitat & Accessories</span></a></li>
            <li><a href="/litter-bedding/"><span> Litter & Bedding</span></a></li>
            <li><a><span>Reptile</span></a>
              <div class="drop-container-cat">
                <div class="drop-list-cat">
                  <div class="shob-by-cat">Shop By Category</div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/turtle-food/">Turtle Food</a></li>
                    <li><a  href="/supplement-cleaning/">Supplement & Cleaning</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li style="width: 304px;">&nbsp;</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--small pet end --> 
  
  <!-- birds pet -->
  <div id="navtab-6" class="nav-content <? if(in_array($keyword,$tab_birds) && !in_array($keyword,$cat_array) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish) && !in_array($keyword,$tab_smallpet)){echo 'current';}else{echo '';}?>">
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a href="/bird-food/"><span>Food</span></a></li>
            <li><a href="/bird-toys/"> <span>Toys</span></a></li>
            <li><a href="/bird-supplement/"><span>Supplement</span></a></li>
            <li><a href="/bird-feeding-accessories/"><span>Feeding & Accessories </span></a></li>
            <li style="width: 634px;">&nbsp;</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- birds pet end --> 
  
  <!-- People pet -->
  <div id="navtab-7" class="nav-content <? if(in_array($keyword,$tab_people) && !in_array($keyword,$cat_array) && !in_array($keyword,$commu_arr) && !in_array($keyword,$tab_def) && !in_array($keyword,$tab_fish) && !in_array($keyword,$tab_smallpet)){echo 'current';}else{echo '';}?>">
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a href="/magazine/"><span>Dog Magazines</span></a></li>
            <li><a href="/bags/"><span>Dog Lovers Bag</span></a></li>
            <li><a href="/others-dog/"><span>Products for Pet Lovers </span></a></li>
            <li><a href="/tshirt/"><span>Dog Lover T-Shirts</span></a></li>
            <li style="width: 452px;">&nbsp;</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- People pet end --> 
  
  <!-- community section start-->
  
  <div id="navtab-3" class="nav-content <? if(in_array($keyword,$commu_arr) && !in_array($keyword,$cat_array) && !in_array($keyword,$tab_def) && $_SERVER['REQUEST_URI']!='/'){echo 'current';}else{echo '';} ?>">
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a href="/dog-breeds/"  id="aq11"> <span>Dog Breeds</span></a></li>
            <li><a href="/dog-blog/"> <span>Articles</span></a></li>
            <li><a href="/qna/"> <span>Q&A </span></a></li>
            <li><a href="/dog-listing/"> <span>Classifieds</span></a></li>
            <li><a href="/dog-events/"> <span>Dog Show </span></a></li>
            <li><a href="/wag_club/"> <span>Wag Club </span></a></li>
            <li><a href="/adoption/"> <span>Adoption</span></a></li>
            <li><a href="/wagfund/"> <span>Donation</span></a></li>
            <li><a href="/wagtag-info/"> <span>Wag Tag</span></a></li>
            <li><a href="/animal-activist/"> <span>Animal Activist</span></a></li>
            <li><a href="/dog-names/"> <span>Dog Names</span></a></li>
            <li class="drop-nav-space-art">&nbsp;</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
