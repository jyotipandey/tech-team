<style> .ds-nav>li>a{padding: 9px 22px 13px 0;height: auto; font-size:14px;}#ds-header-new_1{text-align:center;background:#f4eedb;width:100%;float:left;margin-top:-10px;padding-top:10px;text-transform:uppercase;font-size:small}.NavigationDiv ul{list-style:none;padding:0;margin:0;background:#f4eedb;text-align:center;width:972px;margin:0 auto;text-align:left;font-size: 13px;}.NavigationDiv ul li{display:block;float:left;background:#fff}.NavigationDiv li ul{display:none;z-index:10000;max-width:300px;background:#fff;}.NavigationDiv ul li a{display:block;padding:1em;text-decoration:none;white-space:nowrap;color:#444;font-weight:700}.NavigationDiv ul li a:hover{color:orange}.NavigationDiv li:hover>ul{display:block;position:absolute}.NavigationDiv li:hover li{float:none}.NavigationDiv li:hover a{background:fff}.NavigationDiv >ul > li{background:#f4eedb;}.NavigationDiv li:hover li a:hover{background:#fff;color:orange}.NavigationDiv .main-navigation li ul li{border-top:0;max-height: 45px;}.NavigationDiv ul ul ul{left:100%;top:0;height:100%;background:#fff}.NavigationDiv ul:before,ul:after{content:" ";display:table}.NavigationDiv ul:after{clear:both}
</style>
<div class="ds-header-new_cont">
 <div class="nav_wrap" style="margin-bottom: 39px;">
      <div id="ds-header-new_1">
      <div class="NavigationDiv">
<ul class="main-navigation" style="letter-spacing: 0px;">
  <li style="width:85px;"><a href="#">Dog Store</a>
  <ul><li><a href="#">Dog Food</a>
          <ul style="border-left: 1px solid #ddd;">
<li><a  href="/dry-dog-food/" >Dry Dog Food</a></li>
<li><a  href="/prescription-food/" >Prescription food</a></li>
<li><a  href="/frozen-dog-food/" >Frozen/Fresh food</a></li>
<li><a  href="/wet-food/" >Wet food</a></li>
<li><a  href="/weaning-food/" >Weaning food</a></li>
<li><a  href="/food-toppings/" >Food toppings</a></li>
<li><a  href="/dog-food/" >View all</a></li>
        </ul></li>
          <li><a href="#">Dog Treats</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/dog-meaty-treats/" >Dog Meaty Treats</a>
<a  href="/dog-dental-treats/" >Dental Treats</a>
<a  href="/dog-biscuits/" >Biscuits</a>
<a  href="/rawhide/" >Rawhide</a>
<a  href="/treats/" >View all</a>
        </ul></li>
        <li><a href="#">Dog Toys</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/dental-toys/">Dental toys</a>
<a  href="/interactive-dog-toys/">Interactive toys</a>
<a  href="/rubber-toys/">Rubber Toys</a>
<a  href="/rope-dog-toys/">Rope & Jute toys</a>
<a  href="/dog-toy/">View all</a>
        </ul></li>
        <li><a href="#">Health & Wellness</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/supplements/">Supplements</a>
<a  href="/dental-care/">Dental Care</a>
<a  href="/ear-care/">Ear Care</a>
<a  href="/health-wellness/">View all</a>
        </ul></li>
        <li><a href="#">Grooming</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/conditioners/" >Dog Conditioner</a>
<a  href="/shampoos/">Shampoo</a>
<a  href="/grooming-supplies/">Grooming supplies</a>
<a  href="/dog-grooming/">View all</a>
        </ul></li>
        <li><a href="#">Flea & Ticks</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/flea-ticks-collar/" >Flea Ticks Collar</a>
<a  href="/powders/">Powders</a>
<a  href="/flea-tick-shampoos/">Shampoos</a>
<a  href="/flea-ticks/">View all</a>
        </ul></li>
        <li><a href="#">Collars & Leashes</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/dog-collars/" >Dog Collars</a>
<a  href="/dog-harnesses/">Harness</a>
<a  href="/dog-collars-leashes/">Dog Collars & Leashes</a>
<a  href="/collars-leashes/">View all</a>
        </ul></li>
        <li><a href="#">Crates, Cages & Beds</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/crates/" >Crates</a>
<a  href="/beds/">Beds</a>
        </ul></li>
        <li><a href="#">Bowls & Feeders</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/steel-bowls/" >Steel & Plastic Bowls</a>
<a  href="/feeders/">Feeders</a>
<a  href="/adjustable-bowl/">Elevated & Adjustable Bowls</a>
        </ul></li>
        <li><a href="#">Cleaning & Potty</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/diapers/" >Diapers</a>
<a  href="/training-pads/">Potty Training Trays & Pads</a>
        </ul></li>
        <li><a href="#">Dog Brands</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/mutt-course/" >Mutt Of Course</a>
<a  href="/royal-canin/" >Royal Canin</a>
<a  href="/lana-paws/" >Lana Paws</a>
<a  href="/dogspot/" >DogSpot</a>
<a  href="/forfurs/" >Forfurs</a>
<a  href="/pedigree/" >Pedigree</a>
<a  href="/paws-little/" >Paws A Little</a>
<a  href="/jerhigh/" >JerHigh</a>
<a  href="/petzo/" >Petzo</a>
        </ul></li>
          </ul></li>
           <li  style="width:136px;"><a href="/bangalore-shop/">BANGALORE STORE</a></li>
  <li  style="width:80px;"><a href="#">Cat Store</a>
  <ul><li><a href="#">Cat Foods</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/dry-cat-food/" >Dry Cat Food</a>
<a  href="/wet-cat-food/" >Wet Cat Food</a>
<a  href="/cat-food-and-treats/" >View all</a>
        </ul></li>
          <li><a href="#">Cat Accessories</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/cat-furniture/" >Cat Furniture</a>
<a  href="/cat-scratchers/" >Cat Scratchers</a>
<a  href="/cat-accessories/" >View all</a>
        </ul></li>
        <li><a href="#">Cat Grooming</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/cat-shedding-control/">Cat Shedding Control</a>
<a  href="/cat-dry-bathing/">Cat Dry Bathing</a>
<a  href="/cat-grooming/">View all</a>
        </ul></li>
        <li><a href="/cat-crates/">Cat Crates</a></li>
        <li><a href="#">Cat Cleaning</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/cat-stain-remover/">Cat Stain Remover</a>
<a  href="/cat-fleas-and-ticks/">Cat Fleas & Ticks</a>
        </ul></li>
        <li><a href="#">Cat Litter And Accessories</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/cat-litter/" >Cat Litter</a>
<a  href="/cat-litter-accessories/">Cat Litter Accessories</a>
        </ul></li>
        <li><a href="#">Cat Toys</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/cat-interactive-toys/" >Interactive Cat Toys</a>
<a  href="/cat-scratcher-toys/">Cat Scratcher Toys</a>
        </ul></li>
        <li><a href="/cat-supplements/">Cat Supplements</a></li>
        <li><a href="#">Cat Brands</a>
          <ul style="border-left: 1px solid #ddd;">
<a  href="/whiskas/" >Whiskas</a>
<a  href="/catspot/">Catspot</a>
<a  href="/matisse/">Matisse</a>
<a  href="/lchic/">L chic</a>
        </ul></li>
        </ul></li>
      <li  style="width:109px;"><a href="/sales/">Today's deals</a></li>
        <li style="width:41px;"><a href="/qna/">Q&A</a></li>            
  <li style="width:91px;"><a href="#">Classifieds</a>
    <ul>
     <a  href="/dog-listing/category/veterinarian/" >Veterinarian</a>
<a  href="/dog-listing/category/services/" >Grooming</a>
<a  href="/dog-listing/category/pet-boarding/" >Boarding</a>
<a  href="/dog-listing/category/pet-shop/" >Pet Shop</a>
<a  href="/dog-listing/category/dog-trainer/" >Trainers</a>
<a  href="/dog-listing/category/kennel-owner-breeder/" >Breeders/Kennels</a>
<a  href="/dog-listing/category/kennel-club/" >Kennel Club</a>
<a  href="/dog-listing/category/others/" >Others</a>
    </ul>
  </li>
  <li style="width:79px;"><a href="/dog-blog/">Dog Blog</a></li>
  <li style="width:83px;"><a href="/adoption/">Adoption</a></li>
  <li style="width:96px;"><a href="#">Dog Breeds</a>
    <ul>
     <a  href="/labrador-retriever/" >Labrador</a>
<a  href="/golden-retriever/" >Golden Retriever</a>
<a  href="/german-shepherd-dog-alsatian/" >German Shepherd</a>
<a  href="/beagle/" >Beagle</a>
<a  href="/dog-breeds/" >Others</a>
    </ul>
  </li>
<li><a href="#">Dog Community</a>
    <ul>
<a  href="/wag-club/" data-ajax="false">Wag Club</a>
<a  href="/dog-names/" data-ajax="false">Dog Names</a>
<a  href="/photos/" data-ajax="false">Dog Images</a>
<a  href="/dog-events/" data-ajax="false">Dog Show</a>
    </ul>
  </li>
</ul>

</div>

          </div>
          </div>
      <? //if($userid!='602105702'){?>    
<?php /*?><div style="height:100px;padding-top: 10px;">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive-Head -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="6045502591"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div><?php */?>
<? // }else{?>
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>


<? if($sitesection != "HOME"){?>

<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/Adoptionmobile', [970, 90], 'div-gpt-ad-1552033085017-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/Puppies', [300, 250], 'div-gpt-ad-1552029423125-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/Newbanner', [728, 90], 'div-gpt-ad-1552045945672-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/Articlemo', [970, 90], 'div-gpt-ad-1552371695288-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/PurinaNew', [160, 600], 'div-gpt-ad-1552378600577-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
<!-- /21630298032/Adoptionmobile -->
<div id='div-gpt-ad-1552033085017-0' style='height:90px; width:970px;margin-left: 168px;padding-top: 15px;padding-bottom: 15px;;text-align: end;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552033085017-0'); });
</script>
</div>
<? }else{?>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/Desktop_home_970x90_top', [970, 90], 'div-gpt-ad-1554455715163-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/Desktop_home_970x90_bottom', [970, 90], 'div-gpt-ad-1554456576115-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<!-- /21630298032/Desktop_home_970x90_top -->
<div id='div-gpt-ad-1554455715163-0' style='height:90px; width:970px;margin-left: 168px;padding-top: 15px;padding-bottom: 15px;;text-align: end;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1554455715163-0'); });
</script>
</div>
<? }?>

</div>
<?php /*?>
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/PurinaNew', [970, 90], 'div-gpt-ad-1549777778766-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/UnitPlace', [1024, 768], 'div-gpt-ad-1549873791850-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();

  });
</script><script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/HomePage', [[1024, 768], [480, 320]], 'div-gpt-ad-1549778118233-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<?php */?>