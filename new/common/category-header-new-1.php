

<div class="ds-header-new_cont"> 
  <!-- top nav tab start-->
  
  <!-- top nav tab start end--> 
  
 
    <div class="nav_wrap">
      <div id="ds-header-new">
        <div class="top_nav_tab">
          <ul class="ds-nav clearfix animated">
            <li><a >Dog <svg width="4.7" height="8" viewBox="0 0 16 27" class="drp-svg"><path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#444" class="_3Der3h"></path></svg></a>
              <div class="drop-container-dog">
                <div class="drop-list-cat">
                  <div class="shob-by-cat"><a href="https://www.dogspot.in/dog-food/">Food<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a  href="/dry-dog-food/">Dry Dog Food</a></li>
                    <li><a  href="/canned-dog-food/">Canned Dog Food</a></li>
                    <li><a  href="/prescription-dog-food/">Prescription Dog Food</a></li>
                    <li><a  href="/weaning-food/">Weaning Food</a></li>
                    <li><a  href="/food-toppings/">Food Toppings</a></li>
                  </ul>
                  
                 <div class="shob-by-cat"><a href="https://www.dogspot.in/collars-leashes/">Collars & Leashes<span class="drp-arrow-nav"></span></a></div>
                  
                          <ul class="ds-nav-dropdown">
                    <li><a href="/dog-collars/">Collars</a></li>
                    <li><a href="/dog-leashes/">Leashes</a></li>
                    <li><a href="/dog-harnesses/">Harnesses</a></li>
                    <li><a href="/chaincollars/">Chain Collars </a></li>
                    <li><a href="/retractable-leashes/">Retractable Leashes</a></li>
                    <li><a href="/show-leashes/">Show Leashes</a></li>
                    <li><a href="/muzzles/">Muzzles</a></li>
                    <li><a href="/recovery-collars-cones/">Recovery Collars &amp; Cones</a></li>
                    <li><a href="/pet-tag/">Pet Tag</a></li>
                  </ul>  
                         
                  
                </div>
              
                <div class="drop-list-cat nev-bg-gray">
                 <div class="shob-by-cat"><a href="https://www.dogspot.in/biscuits-treats/">Biscuits & Treats<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/chicken-treats/">Chicken Treats</a></li>
                    <li><a href="/other-meaty-treats/">Other Meaty Treats</a></li>
                    <li><a href="/veg-treats/">Fruits &amp; Vegetables Treats</a></li>
                    <li><a href="/dog-dental-treats/">Dental Treats</a></li>
                    <li><a href="/dog-biscuits/">Biscuits</a></li>
                    <!--<li><a href="/bakery-products/">Bakery Products</a></li>-->
                    <li><a href="/rawhide/">Rawhide</a></li>
                  </ul>
                  
                  <div class="shob-by-cat"><a href="https://www.dogspot.in/dog-bowls/">Bowls &amp; Feeders<span class="drp-arrow-nav"></span></a></div>
                         <ul class="ds-nav-dropdown">
                    <li><a href="/feeders/">Feeders</a></li>
                    <li><a href="/steel-bowls/">Steel &amp; Plastic Bowls</a></li>
                    <li><a href="/adjustable-bowl/">Elevated &amp; Adjustable Bowls</a></li>
                    <li><a href="/clamp-bowls/">Clamp Bowls</a></li>
                    <li><a href="/slow-feeding-bowl/"> Slow Feeding Bowls</a></li>
                    <li><a href="/puppy-feeders/">Puppy Feeder</a></li>
                   </ul>
                  
                  </div>
                  
              
              
                <div class="drop-list-cat">
                 
                
                 
                 <div class="shob-by-cat"><a href="https://www.dogspot.in/health-wellness/">Health & Wellness<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/dental-care/">Dental Care</a></li>
                    <li><a href="/dewormer/">Dewormer</a></li>
                    <li><a href="/supplements/">Supplements</a></li>
                    <li><a href="/dog-puppy-care/">Puppy Care</a></li>
                    <li><a href="/health-supplies-support/">Health Supplies &amp; Support</a></li>
                    <li><a href="/eye-care/">Eye Care</a></li>
                    <li><a href="/ear-care/">Ear Care</a></li>
                    <li><a href="/skin-coat-care/">Skin &amp; Coat Care</a></li>
                  </ul>
              
                
                 <div class="shob-by-cat"><a href="/clothing-accessories/"> 
                    Clothing  &amp; Accessories<span class="drp-arrow-nav"></span></a></div>
                      <ul class="ds-nav-dropdown">
                    <li><a href="/raincoat/">Raincoats</a></li>
                    <li><a href="/dog-tshirt/">Dog T-shirt</a></li>
                    <li><a href="/bandana/">Bandana</a></li>
                    <li><a href="/cooling-dog-coat/">Cooling Coats</a></li>
                    <li><a href="/paw-protection/">Shoes & Socks</a></li>
                     <li><a href="/coats-jackets/">Coats & Jackets</a></li>
                   
                  </ul>
                  
                  </div>
               
                
               
                 <div class="drop-list-cat nev-bg-gray">
                 
                  
                    <div class="shob-by-cat"><a href="https://www.dogspot.in/dog-grooming/">Grooming<span class="drp-arrow-nav"></span></a></div>
                <ul class="ds-nav-dropdown">
                    <li><a href="/shampoos/">Shampoos</a></li>
                    <li><a href="/conditioners/">Conditioners</a></li>
                    <li><a href="/brushes-combs/">Brushes & Combs</a></li>
                    <li><a href="/grooming-tools/">Grooming Tools</a></li>
                    <li><a href="/towel-accessories/">Towel & Accessories</a></li>
                    <li><a href="/dry-bathing/">Dry Bathing</a></li>
                     <li><a href="/deodorizers/">Deodorizers</a></li>
                    <li><a href="/shedding-control/">Shedding Control</a></li>
                    <li><a href="/grooming-tables-bath-tubs/">Grooming Tables & Bath Tubs</a></li>
                  </ul>
				  
				  <div class="shob-by-cat"><a href="https://www.dogspot.in/crates-beds/">Crates, Cages & Beds<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/crates/">Crates</a></li>
                    <li><a href="/cages/">Cages</a></li>
                    <li><a href="/dog-house/">Dog House</a></li>
                    <li><a href="/car-travel-accessories/">Car Travel Accessories</a></li>
                    <li><a href="/beds/">Beds</a></li>
              
                  </ul>
                  
                 
                  </div>
                 
                
                <div class="drop-list-cat">
                 <div class="shob-by-cat"><a href="https://www.dogspot.in/dog-toy/">
                 Toys<span class="drp-arrow-nav"></span></a></div>
                   <ul class="ds-nav-dropdown">
                    <li><a href="/chew-toys/">Chew Toys</a></li>
                    <li><a href="/soft-toys/">Soft Toys</a></li>
                    <li><a href="/interactive-dog-toys/">Interactive Toys</a></li>
                    <li><a href="/ball-toys/">Ball Toys</a></li>
                    <li><a href="/squeaker-toy/">Squeaker Toys</a></li>
                     <li><a href="/leather-toys/">Leather Toys</a></li>
                    <li><a href="/bone-toys/">Bone Toys</a></li>
                    <li><a href="/rubber-toys/">Rubber Toys</a></li>
                    <li><a href="/dental-toys/">Dental Toys</a></li>
                    <li><a href="/rope-dog-toys/">Rope &amp; Jute Toys</a></li>
                  </ul>
                    
                  
                  <div class="shob-by-cat"><a href="/dog-training-behavior/">Training &amp; Behavior <span class="drp-arrow-nav"></span></a></div>
                 
                 <ul class="ds-nav-dropdown">
                    <li><a href="/training-leashes/">Training Leashes</a></li>
                    <li><a href="/training-aids/">Training Aids</a></li>
                    <li><a href="/training-clickers-whistles/">Training Clickers & Whistles</a></li>
                  
                  </ul>
                      </div>     
                
                                        
                <div class="drop-list-cat nev-bg-gray">
                     <div class="shob-by-cat"><a href="https://www.dogspot.in/flea-ticks/">
                  Flea & Ticks<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/powders/">Powders</a></li>
                    <li><a href="/flea-ticks-collar/">Collars</a></li>
                    <li><a href="/flea-tick-shampoos/">Shampoos</a></li>
                    <li><a href="/spot-treatments/">Spot on Treatments</a></li>
                    <li><a href="/combs-accessories/">Combs &amp; Accessories</a></li>
                    <li><a href="/sprays/">Sprays</a></li>
                    <li><a href="/soaps/">Soaps</a></li>
                  </ul>  
                 <div class="shob-by-cat"><a href="https://www.dogspot.in/dog-clean-up/">
                Dog Cleaning & Control<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/odour-remover/">Odour & Stain Remover</a></li>
                    <li><a href="/diapers/">Diapers</a></li>
                    <li><a href="/pet-hair-remover/">Pet Hair Removal</a></li>
                    <li><a href="/potty-bags-dispensers/">Potty Bags & Dispensers</a></li>
                    <li><a href="/potty-scoopers/">Potty Scoopers</a></li>
                    <li><a href="/training-pads/">Potty Training Pads & Trays</a></li>
                  </ul>
                  
                </div>
                
                
              </div>
            </li>
            <li> <a >Cat  <svg width="4.7" height="8" viewBox="0 0 16 27" class="drp-svg"><path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z"  class="_3Der3h"></path></svg></a>
              <div class="drop-container-dog cat-left">
                <div class="drop-list-cat">
                  <div class="shob-by-cat"><a href="https://www.dogspot.in/cat-food-and-treats/">Foods & Treats<span class="drp-arrow-nav"></span></a></div>
                <ul class="ds-nav-dropdown">
                    <li><a href="/dry-cat-food/">Dry Cat Food</a></li>
                    <li><a href="/wet-cat-food/">Wet Cat Food</a></li>
                      <li><a href="/cat-meaty-treats/">Cat Meaty Treats</a></li>
                  </ul>
                   <div class="shob-by-cat"><a href="https://www.dogspot.in/cat-cleaning/">Cleaning<span class="drp-arrow-nav"></span></a></div>
                      <ul class="ds-nav-dropdown">
                    <li><a href="/cat-stain-remover/">Stain Remover</a></li>
                    <li><a href="/cat-odor-remover/">Odor Remover</a></li>
                    <li><a href="/cat-fleas-and-ticks/">Fleas And Ticks</a></li>
                  </ul>
                  
                  
                </div>
                
                
                <div class="drop-list-cat nev-bg-gray">
                 <div class="shob-by-cat"><a href="https://www.dogspot.in/cat-grooming/">Grooming<span class="drp-arrow-nav"></span></a></div>
               <ul class="ds-nav-dropdown">
                    <li><a href="/cat-shampoos-and-conditioners/">Shampoos &amp; Conditioners</a></li>
                    <li><a href="/cat-shedding-control/">Shedding Control</a></li>
                    <li><a href="/cat-eye-care/">Eye Care</a></li>
                    <li><a href="/cat-bath-accessories/">Bath Accessories</a></li>
                    <li><a href="/cat-grooming-tools/">Grooming Tools</a></li>
                    <li><a href="/cat-dry-bathing/">Dry Bathing</a></li>
                    <li><a href="/cat-brushes-and-combs/">Brushes &amp; Combs</a></li>
                  </ul>
                  
                 
                  </div>
                <div class="drop-list-cat">
                 
                
                 
                 <div class="shob-by-cat"><a href="/cat-health-and-care/">Healthcare<span class="drp-arrow-nav"></span></a></div>
                 <ul class="ds-nav-dropdown">
                    <li><a href="/cat-dewormer/">Cat Dewormer</a></li>
                    <li><a href="/cat-supplements/">Supplements</a></li>
                  </ul>
              
                  <div class="shob-by-cat"><a href="/cat-litter/">Litter<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/cat-litter/">Cat Litter</a></li>
                    <li><a href="/cat-litter-boxes/">Cat Litter Box</a></li>
                    <li><a href="/cat-litter-accessories/">Litter Accessories</a></li>
                  </ul>
                  
                  </div>
                  
                  <div class="drop-list-cat nev-bg-gray">
                 
                  
                    <div class="shob-by-cat"><a href="https://www.dogspot.in/cat-toys/">Toys<span class="drp-arrow-nav"></span></a></div>
               <ul class="ds-nav-dropdown">
                    <li><a href="/cat-ball-toys/">Cat Ball Toys</a></li>
                    <li><a href="/cat-interactive-toys/">Interactive Cat Toys</a></li>
                    <li><a href="/cat-plush-toys/">Cat Plush Toys</a></li>
                    <li><a href="/cat-teasers-and-wands/">Teasers &amp; Wands</a></li>
                    <li><a href="/cat-scratcher-toys/">Scratcher Toys</a></li>
                  </ul>
                  <div class="shob-by-cat">
                  <a href="/cat-bowls-and-feeders/">Bowls & Feeders<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/cat-bowls/">Bowls</a></li>
                    <li><a href="/cat-feeders/">Feeders</a></li>
                  </ul>
                  </div>
                  <div class="drop-list-cat">
                 <div class="shob-by-cat">
                 <a href="/cat-training/">Training<span class="drp-arrow-nav"></span></a></div>
                   <ul class="ds-nav-dropdown">
                    <li><a href="/cat-training-aids/">Training Aids</a></li>
                    <li><a href="/cat-repellents/">Repellents</a></li>
                  </ul>
                    <div class="shob-by-cat">
                 <a href="/cat-bath-accessories/"> Accessories<span class="drp-arrow-nav"></span></a></div>
                <ul class="ds-nav-dropdown">
                    <li><a href="/cat-beds/">Beds</a></li>
                    <li><a href="/cat-furniture/">Cat Furniture</a></li>
                    <li><a href="/cat-scratchers/">Cat Scratchers</a></li>
                    <li><a href="/cat-doors/">Cat Doors</a></li>
                  </ul>
                    </div> 
                   <div class="drop-list-cat nev-bg-gray">
                <ul class="ds-nav-dropdown">
                <div class="shob-by-cat"><a href="https://www.dogspot.in/cat-crates-and-cages/">Collars & Leashes<span class="drp-arrow-nav"></span></a></div>
                  <ul class="ds-nav-dropdown">
                    <li><a href="/cat-crates/">Cat Crates</a></li>
                    <li><a href="/cat-cages/">Cat Cages</a></li>
                  </ul>
                
                    <div class="shob-by-cat"><a href="/dog-training-behavior/">Cat Pet Tags<span class="drp-arrow-nav"></span></a></div>
                   <div class="shob-by-cat"><a href="/cat-magazines/">Magazines<span class="drp-arrow-nav"></span></a></div>
                    <div class="shob-by-cat"><a href="/cat-collars/">Collars<span class="drp-arrow-nav"></span></a></div>
                    <div class="shob-by-cat"><a href="/cat-harness/">Cat Harness<span class="drp-arrow-nav"></span></a></div>
                  </ul>
				

                 
                 
                  
                </div>
                </div>
            </li>
            <li><a  >Bird  <svg width="4.7" height="8" viewBox="0 0 16 27" class="drp-svg"><path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#444" class="_3Der3h"></path></svg></a>
            
             <div class="drop-list-cat nev-bg-gray other-nav">
                <ul class="ds-nav-dropdown">
                
            <li><a href="/bird-food/"><span>Food</span></a></li>
            <li><a href="/bird-toys/"> <span>Toys</span></a></li>
            <li><a href="/bird-supplement/"><span>Supplement</span></a></li>
            <li><a href="/bird-feeding-accessories/"><span>Feeding &amp; Accessories</span></a></li>
            
          
                  </ul>
				

                 
                 
                  
                </div>
            </li>
            <li><a>Small Pet  <svg width="4.7" height="8" viewBox="0 0 16 27" class="drp-svg"><path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#444" class="_3Der3h"></path></svg></a>
               <div class="drop-list-cat nev-bg-gray other-nav">
                <ul class="ds-nav-dropdown">
                
         
            <li><a href="/small-pet-food-treats/"><span>Food &amp; Treats</span></a></li>
            <li><a href="/small-pet-supplement/"> <span> Supplement</span></a></li>
            <li><a href="/feeding-accessories/"><span>Feeding Accessories</span></a></li>
            <li><a href="/habitat-accessories/"><span> Habitat &amp; Accessories</span></a></li>
            <li><a href="/litter-bedding/"><span> Litter &amp; Bedding</span></a></li>
            
                    <li><a href="/turtle-food/">Turtle Food</a></li>
                    <li><a href="/supplement-cleaning/">Supplement &amp; Cleaning</a></li>

                  </ul>
 
                </div
            ></li>
            <li><a>Fish  <svg width="4.7" height="8" viewBox="0 0 16 27" class="drp-svg"><path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#444" class="_3Der3h"></path></svg></a>
          <div class="drop-container-dog" style="width:324px !important; ">
                
              <div class="drop-list-cat ">
                <ul class="ds-nav-dropdown">
            <li><a href="/fish-food/"><span>Food</span></a></li>
            <li><a href="/aquarium-filter/"><span>Filters</span></a></li>
            <li><a href="/aquarium-co2-system/"><span> CO2 System</span></a></li>
            <li><a href="/aquarium-accessories/"><span> Accessories</span></a></li>
            <li><a href="/aquarium-air-pumps/"> <span>Air Pumps</span></a></li>
            
            
            </ul>
          </div>
        <div class="drop-list-cat nev-bg-gray"> 
		<ul class="ds-nav-dropdown">
          <li><a href="/fish-aquarium-water-conditioning/"><span>Water Conditioning</span></a></li><li><a href="/fish-aquarium-decoration/"><span>Decoration</span></a></li>
            <li><a href="/aquarium-freshwater-substrate/"><span>Freshwater Substrate</span></a></li>
            <li><a href="/fish-aquarium-plant-nutrients/"><span>Plant Nutrients</span></a></li>
             <li><a href="/aquarium-maintenance/"> Maintenance Products</a></li>
          </ul>
                </div>
            
           </div>
            </li>
            <li><a>People <svg width="4.7" height="8" viewBox="0 0 16 27" class="drp-svg"><path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#444" class="_3Der3h"></path></svg></a>
              <div class="drop-list-cat nev-bg-gray other-nav">
                <ul class="ds-nav-dropdown">
                
      
            <li><a href="/magazine/"><span>Dog Magazines</span></a></li>
            <li><a href="/bags/"><span>Dog Lovers Bag</span></a></li>
            <li><a href="/others-dog/"><span>Products for Pet Lovers </span></a></li>
            <li><a href="/tshirt/"><span>Dog Lover T-Shirts</span></a></li>
            <li style="width: 404px;">&nbsp;</li>
          </ul>
            
 
                </div
            >
            </li>
            <li><a> Community  <svg width="4.7" height="8" viewBox="0 0 16 27" class="drp-svg"><path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#444" class="_3Der3h"></path></svg></a>
              <div class="drop-container-dog" style="width:324px !important; ">
                
              <div class="drop-list-cat ">
                <ul class="ds-nav-dropdown">
           <li><a href="/dog-breeds/" id="aq11"> <span>Dog Breeds</span></a></li>
            <li><a href="/dog-blog/"> <span>Articles</span></a></li>
            <li><a href="/qna/"> <span>Q&amp;A </span></a></li>
            <li><a href="/dog-listing/"> <span>Classifieds</span></a></li>
            <li><a href="/dog-events/"> <span>Dog Show </span></a></li>
            <li><a href="/wag_club/"> <span>Wag Club </span></a></li>
            
            </ul>
          </div>
        <div class="drop-list-cat nev-bg-gray"> 
		<ul class="ds-nav-dropdown">
         
            <li><a href="/adoption/"> <span>Adoption</span></a></li>
            <li><a href="/wagfund/"> <span>Donation</span></a></li>
            <li><a href="/wagtag-info/"> <span>Wag Tag</span></a></li>
            <li><a href="/animal-activist/"> <span>Animal Activist</span></a></li>
            <li><a href="/dog-names/"> <span>Dog Names</span></a></li> </ul>
                </div>
            
           </div>
            </li>
            <li class="smrt-deal"><a href="/monthly-essential-packs/" style=" background:#fff;"  >Smart Deals</a></li>
            <li class="offer-nav"><a href="/sales/"  > Sale</a></li>
            
            
            
          </ul>
          </div>
          </div>
          </div>
          </div>
         