<?php
$DOCUMENT_ROOT='/home/dogspot/public_html';
require_once($DOCUMENT_ROOT.'/functions.php');
// initialize ob_gzhandler to send and compress data
//ob_start('ob_gzhandler');
 
// required header info and character set
header("Content-type: text/css;charset: UTF-8");
 
// duration of cached content (24 hours)
$offset = 60 * 60 * 24 * 30;
//$offset = 0 * 0 * 0;
 
// cache control to process
header ('Cache-Control: max-age=' . $offset . ', must-revalidate');
 
// expiration header format
//$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s",time() + $offset) . " GMT";
 
// send cache expiration header to browser
header($ExpStr);
 
// initialize compress function for white-space removal
//ob_start("compress");
// Begin function compress
//function compress($buffer) {
// 
//// remove comments
//    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
// 
//// remove tabs, spaces, new lines, etc. 
//    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
// 
//// remove unnecessary spaces
//    $buffer = str_replace('{ ', '{', $buffer);
//    $buffer = str_replace(' }', '}', $buffer);
//    $buffer = str_replace('; ', ';', $buffer);
//    $buffer = str_replace(', ', ',', $buffer);
//    $buffer = str_replace(' {', '{', $buffer);
//    $buffer = str_replace('} ', '}', $buffer);
//    $buffer = str_replace(': ', ':', $buffer);
//    $buffer = str_replace(' ,', ',', $buffer);
//    $buffer = str_replace(' ;', ';', $buffer);
//return $buffer;
//}
 
/**
INCLUDE YOUR STYLE SHEETS HERE
**/
$afile=array(
'/new/css/main.css',
'/new/css/shop.css',
'/sharengain/sharengain.css',
'/js/starmatrix.css',
'/shapopup/shapopup.css',
'/new/slide/skins.css',
'/new/slide/jquery.jcarousel.css',
'/jqzoom_ev-2.3/css/jquery.jqzoom.css',
'/new/css/featured.css',
'/new/css/rcbreed.css',
'/ajax-autocomplete/jquery.autocomplete.css',
'/new/css/login-style.css',
'/new/css/style1.css',
'/css/highslide.css',
'/template/css/checkout.css',
'/new/css/tab_style.css',
'/new/css/profile.css',
'/css/paging.css',
'/new/common/css/common_css.css'
);

$buffer='';
foreach ($afile as $af){
	$buffer .= file_get_contents($DOCUMENT_ROOT.$af);
}
echo compress($buffer);
//echo $buffer;
ob_end_flush();
ob_end_clean();
?>