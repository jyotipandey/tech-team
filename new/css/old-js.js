<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

// if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
require_once($DOCUMENT_ROOT.'/functions.php');
// initialize ob_gzhandler to send and compress data
//ob_start('ob_gzhandler');
 
// required header info and character set
header("Content-type: text/css;charset: UTF-8");
 
// duration of cached content (24 hours)
$offset = 60 * 60 * 24 * 30;
//$offset = 0 * 0 * 0;
 
// cache control to process
header ('Cache-Control: max-age=' . $offset . ', must-revalidate');
 
// expiration header format
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s",time() + $offset) . " GMT";
 
// send cache expiration header to browser
header($ExpStr);
 
// initialize compress function for white-space removal
//ob_start("compress");
// Begin function compress
function compress($buffer) {
// 
//// remove comments
//    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)!', '', $buffer);
 
// remove tabs, spaces, new lines, etc. 
    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
 
// remove unnecessary spaces
    $buffer = str_replace('{ ', '{', $buffer);
    $buffer = str_replace(' }', '}', $buffer);
    $buffer = str_replace('; ', ';', $buffer);
    $buffer = str_replace(', ', ',', $buffer);
    $buffer = str_replace(' {', '{', $buffer);
    $buffer = str_replace('} ', '}', $buffer);
    $buffer = str_replace(': ', ':', $buffer);
    $buffer = str_replace(' ,', ',', $buffer);
    $buffer = str_replace(' ;', ';', $buffer);
return $buffer;
}
 
//*
//INCLUDE YOUR STYLE SHEETS HERE
//*
$afile=array(
//'/js/jquery-1.8.0.min.js',
'/new/js/jquery.min.js',
'/jquery/jquery.validate-1.9.js',
//jquery/jquery.validate.min-1.9.js',
'/js/shaajax.min.2.1.js',
'/js/shaajax.multiple.load.min-1.1.js',
'/shapopup/shapopup.min.js',
'/jqzoom_ev-2.3/js/jquery.jqzoom-core.min.js',
'/new/js/application.min.js',
//'/new/common/js/alice.js',
//'/ajax-autocomplete/jquery.autocomplete.js',
'new/slide/jquery.jcarousel.pack.js',
);

$buffer='';
foreach ($afile as $af){
	$buffer .= file_get_contents($DOCUMENT_ROOT.$af);
}
echo $buffer;
echo compress($buffer);
ob_end_flush();
ob_end_clean();
?> 
