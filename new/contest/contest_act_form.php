<?php
	ini_set("post_max_size", "40M");
	ini_set("upload_max_filesize", "15M");
	ini_set("max_input_time", "300");
	ini_set("max_execution_time", "300");
	ini_set("memory_limit", "99M");
	require_once($DOCUMENT_ROOT.'/constants.php');
	require_once($DOCUMENT_ROOT.'/session.php');
	require_once($DOCUMENT_ROOT.'/database.php');
	require_once($DOCUMENT_ROOT.'/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
	require_once($DOCUMENT_ROOT.'/arrays.php');
	//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>

<?php //require_once($DOCUMENT_ROOT . '/new/common/header-new.php');?>
<div id="show"></div>
<div class="activity_pop_wc_upload" id="activity_pop_wc_upload" style="width:600px; height:auto!important;">
	<div class="activity_pop_wc">
		<h2><span style="float:left; width:516px;">Make it simple and fun to capture and share the everyday magic in your dog's life. Give it a whirl!</span>
 		<a class="close"><span style="float:right; margin-top:9px;"> [X]</span></a></h2>
        <form id="vasPLUS_Programming_Blog_Form" method="post" enctype="multipart/form-data" action="javascript:void(0);">
            <div class="select_box_dg1"> 
				<span id="submitspan"><input name="vasPhoto_uploads" id="vasPhoto_uploads" type="file" /><div class="upload_photo_dg1">Upload Photo</div></span>
				<div class="crop_img_wc" id="crop_img2" style="display: none">
					<div id="vasPhoto_uploads_Status" width="250" height="250"></div>
				</div>
			</div>
			
            <div class="edit_info_wc">
            	<label>Tagline</label><input type="text" name="tagline" id="tagline" value="" required="required" />
            </div>
			
            <div class="edit_info_wc">
            	<label>Dog's Name</label><input name="dog_name" id="dog_name" type="text" value="" required="required" />
            </div>
			
            <div class="edit_info_wc">
            	<label>Dog's Breed</label>
                <select name="breed" id="breed" required>
                <option value="">Select Breed</option>
 				<? 
					$query=mysql_query("SELECT * FROM dog_breeds ORDER BY breed_name ASC");
	 				while($row=mysql_fetch_array($query)){
				?>
     					<option value="<?=$row['breed_id']?>"><?=$row['breed_name']?></option>
                <? }?>
                </select>
			</div>
			<div id="error" style="display:none"><font style="color:#F00">Please select image</font> </div>
			<div class="btn_is_box1">
            	<input name="" type="button" class="close" value="Cancel" onclick="cancelpic('crop_img2')">
				<input name="" type="button" value="Publish" onclick="submitdata()">
			</div>
		</form>
	</div>
</div>