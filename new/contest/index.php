<?php 
 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");
include($DOCUMENT_ROOT."/session.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot Pooch of the Week</title>
<link type="text/css" rel="stylesheet" href="/new/contest/css/contest.css?h=888" />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="contest" />
<meta property="og:url" content="https://www.dogspot.in/contest/" />
<meta property="og:title" content="Contest" /> 
<meta property="og:description" content="DogSpot presents 'Pooch of the Week' contest especially for your furry friend. Click here to participate and win exciting prizes" />
<meta name="keywords" content="Dog Contest, Pooch of the Week, DogSpot Contest Page" />

<meta name="description" content="DogSpot presents 'Pooch of the Week' contest especially for your furry friend. Click here to participate and win exciting prizes"  />



<?php
	require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
	require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<script type="text/javascript" src="/wag_club/file_uploads.js"></script>
<script type="text/javascript">
    $(".close_popup").leanModal({top : 120, overlay : 0.6, closeButton: ".modal_close" });
</script>
<script type="text/javascript" src="/wag_club/jquery.form.js"></script>

<script>
	$(document).ready(function() {
		$('a.login-window1').click(function() {
			var loginBox = $(this).attr('href');
			$(loginBox).fadeIn(300);
			var popMargTop = ($(loginBox).height() + 24) / 2;
			var popMargLeft = ($(loginBox).width() + 20) / 2;
			$(loginBox).css({
				'margin-top' : -popMargTop,
				'margin-left' : -popMargLeft
			});
			$('body').append('<div id="mask"></div>');
			$('#mask').fadeIn(300);
			return false;
		});
		$('.close, #mask').live('click', function() {
			$('#mask , .login-popup').fadeOut(300 , function() {
			$('#mask').remove();
		});
		return false;
	});
});
</script>
<script type="text/javascript" >
	$(document).ready(function() {
		$('#vasPhoto_uploads').live('change', function() {
        	$("#vasPLUS_Programming_Blog_Form").vPB({
            	beforeSubmit: function() {
                	$("#vasPhoto_uploads_Status").show();
					$("#crop_img2").show();
		            $("#vasPhoto_uploads_Status").html('');
                  	$("#vasPhoto_uploads_Status").html('<div style="" align="center"><font style="font-family: Verdana, Geneva, sans-serif; font-size:12px; color:black;">Upload</font> <img src="https://www.dogspot.in/images/indicator.gif" alt="Upload...." align="absmiddle" title="Upload...."/></div><br clear="all">');
				},
			url: '/new/contest/activity_dog_image.php',
            success: function(response) {
				$("#submitspan").hide();
		        $("#vasPhoto_uploads_Status").html($(response).fadeIn(2000));
            }
		}).submit();
	}); 
}); 

function sleep(milliseconds,act) {
	var dog_nicename=$("#dog_nicename").val();
	var user_id=$("#user_id").val();	
  	var start = new Date().getTime();
  	for (var i = 0; i < 1e7; i++) {
    	if ((new Date().getTime() - start) > milliseconds){
			break;
    	}
		window.location.href="https://www.dogspot.in/wag_club/activity/"+act+"/";
	}
}

function wagfuncount1(activity_id,dog_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/wag_club/wag_insert.php?activity_wag=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
}

function wagcount1(activity_id,dog_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/wag_club/wag_insert.php?activity_wag_delete=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
}

function validateComment1(activity_id,maxreview){
	$('#loadingdiv'+activity_id).show();                         
	var review_body1= $('#review_body'+activity_id).val();
	var review_body=$.trim(review_body1);
	$('#review_body'+activity_id).val('');
	if(review_body!='' && typeof review_body!='Undefined'){
    	ShaAjaxJquary("/wag_club/checkcomment.php?review_body="+review_body+"&activity_id="+activity_id+"", "#aftercmntq"+activity_id, '', '', 'POST', '#loading', '<img src="/images/indicator.gif" />','APE');
	}
    document.getElementById('review_body').value() = ""
}

function confirmDelete2(review,url,activity_id){
	var user=$("#user_id").val();
	var viewmore=$("#viewmore"+activity_id).text();
	if(viewmore!=''){
		var view=1;
	}else{
		var view=2;
	}
	ShaAjaxJquary("/wag_club/review_delete.php?ram=1&review_id="+review+"&reUrl="+url+"&userid="+user+"&view="+view+"", "#aftercmntq"+activity_id, '',  '', 'post', '#loading', '<img src="/images/indicator.gif" />','APE');	
}

function submitdata(){
	var dog_name=$("#dog_name").val();
	var breed=$("#breed").val();
	var tagline=$("#tagline").val();
	var inserted_id=$("#inserted_id").val();
	var image=$("#image").val();
	var user_id=$("#user_id").val();
	if(dog_name!='' && image!="" && breed!=''){
		$("#error").css("display","none");
		$("#crop_img21").css("display","none");
		ShaAjaxJquary("/new/contest/add-activity.php?acttag=1&dog_name="+dog_name+"&tagline="+tagline+"&inserted_id="+inserted_id+"&breed="+breed+"&image="+image+"", '#show', '',  '', 'post', '', '','REP');
		alert("Your Dog has been published successfully");
		window.location="https://www.dogspot.in/contest";
	}else{
		$("#error").css("display","block");
	}
}
</script>
<style>
.popupContainer{ 
	margin-top:0px !important;
}
#mask{ 
	display:none !important;
}
.select_box_dg1{
	margin: 15px 50px;
}
.crop_img_wc{
	margin-top: 0px;
}
.uploadeFileWrapper img{    
	max-width: 100px; max-height: 120px;
}
.edit_info_wc label{
	font-family: arial;
}
br{ display:none;}
.txt_dg, .bg_body_op {
    top: 176px !important;
    top: 214px !important;
}
.ad_dg_btn{margin-top: -60px !important;}
</style>
</head>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php');?>
<!-- footer-->

<!-- new header-->
<div class="bg_body_op" id="dogtop_margin"></div>
<div class="bg_body"></div>
<div class="txt_dg" >
<div class="text_box_dg">
<h1 style="margin-top:25px;">DogSpot Contest Page</h1>
<p> <span class="font174">Pooch </span>
<span class="font50">of</span>
<span class="font174">the </span> 
<span class="font174">Week!</span> 
</p>
<div class="add_btn_dg">
	<?php if($userid=="Guest" || $userid=="guest"){ ?>
		<a href="#modal" class="bharat close_popup login-window1" id="modal_trigger2"><div class="ad_dg_btn"><span style="font-size:33px; margin-left:30px;">+</span><span  style="padding: 8px;">Upload Photo</span></div></a>
    <? }else{?>
    	<a href="#login-box1" class="close_popup login-window1" id="modal_trigger2"><div class="ad_dg_btn"><span style="font-size:33px; margin-left:30px;">+</span><span  style="padding: 8px;">Upload Photo</span></div></a>
	<? } ?>
</div>  
<div id="login-box1" class="login-popup">
<?php require_once($DOCUMENT_ROOT.'/new/contest/contest_act_form.php'); ?></div>
</div>
</div>
<!-- new header end-->





<!-- fotter-->

<div class="cont980" style="padding:0px;">
 
 
    <? include($DOCUMENT_ROOT."/new/contest/contest_activity.php");?>
    
    <!-- right side bar-->
    <? if($userid=='77681'){?>
    <div class="winner_blk">
    <div class="winner_box">WINNER</div>
    <!-- winner blk-->

    <?
	$date=date('Y-m-d',strtotime("0 days"));
	//echo "SELECT * FROM dogs_activity as dc,dogs_available as da WHERE dc.publish_status='publish' AND user_activity='contest' AND winner='yes' AND da.dog_id=dc.dog_id AND dc.c_date LIKE '%$date%' ORDER BY dc.dog_id DESC";
	$sqldata=query_execute("SELECT * FROM dogs_activity as dc,dogs_available as da WHERE dc.publish_status='publish' AND user_activity='contest' AND winner='yes' AND da.dog_id=dc.dog_id ORDER BY dc.c_date DESC");
	while($winner=mysql_fetch_array($sqldata)){
		$img=$winner['image']; 
		$dog_name=$winner['dog_name'];
		$c_date=$winner['c_date']; 
		$imglink = '/dogs/images/96x96-'.$img;
		$src2 = $DOCUMENT_ROOT . '/dogs/images/'.$img;
		$dest2 = $DOCUMENT_ROOT . $imglink;
		createImgThumbIfnot($src2, $dest2, '96', '96', 'ratiowh');
?>
	<div class="today_winner_blk">
	   	<div class="today_date"><? echo date('jS F Y',strtotime($c_date));?></div>
        <div class="winner_dog_info">
            <div class="contest_img"><img src="<?=$imglink?>" /></div>
            <div class="contest_dogname"><?=$dog_name?></div>
            <div class="contest_tagline">Won Pooch of the Week</div>
        </div>
    </div>
    <? }?>
    <!-- winner blk end-->
    </div>
    <? }else{?>
    <div class="winner_blk">
    <div class="winner_box">WINNER</div>
    <!-- winner blk-->
<?php /*?><div style="text-align:center; font-weight:bold; color: #668000; font: normal 21px arial; background: #8dc059;padding: 20px; color: #fff;">Winners to be announced soon</div><?php */?>
    <?
	$date=date('Y-m-d',strtotime("0 days"));
	$sqldata=query_execute("SELECT * FROM dogs_activity as dc,dogs_available as da WHERE dc.publish_status='publish' AND user_activity='contest' AND winner='yes' AND da.dog_id=dc.dog_id ORDER BY dc.c_date DESC");
	while($winner=mysql_fetch_array($sqldata)){
		$img=$winner['image']; 
		$dog_name=$winner['dog_name']; 
		$c_date=$winner['c_date']; 
		$imglink = '/dogs/images/96x96-'.$img;
		$src2 = $DOCUMENT_ROOT . '/dogs/images/'.$img;
		$dest2 = $DOCUMENT_ROOT . $imglink;
		createImgThumbIfnot($src2, $dest2, '96', '96', 'ratiowh');
?>
	<div class="today_winner_blk">
	   	<div class="today_date"><? echo date('jS F Y',strtotime($c_date));?></div>
        <div class="winner_dog_info">
            <div class="contest_img"><img src="<?=$imglink?>" /></div>
            <div class="contest_dogname"><?=$dog_name?></div>
              <div class="contest_tagline">Won Pooch of the Week</div>
        </div>
    </div>
    <? }?>
    <!-- winner blk end-->
    </div>
    <?  }?>
 <!-- right sidebar end-->
  </div>
</div>

<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
