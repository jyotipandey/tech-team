<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if($section[0]==''){
require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";
$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");?>

<? if($_POST){
	if($acttag){
	$dog_nicename = "$dog_name";
	$dog_nicename = createSlug($dog_nicename);
	$dog_nicename = checkSlugAll('dogs_available', 'dog_nicename', $dog_nicename);
	$breed_nice1=query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$breed'");
	echo "INSERT INTO `dogs_available`(`userid`, `dog_name`, `dog_breed`, `breed_nicename`, `dog_nicename`, `dog_image`,cdate) VALUES ('$userid', '$dog_name', '".$breed_nice1['breed_name']."', '".$breed_nice1['nicename']."', '$dog_nicename', '$image',NULL)";
	query_execute("INSERT INTO `dogs_available`(`userid`, `dog_name`, `dog_breed`, `breed_nicename`, `dog_nicename`, `dog_image`,cdate,publish_status) VALUES ('$userid', '$dog_name', '".$breed_nice1['breed_name']."', '".$breed_nice1['nicename']."', '$dog_nicename', '$image',NULL,'publish')");
	$inserted_id_dog = mysql_insert_id();
	//echo  "INSERT INTO dogs_activity(dog_id,image,publish_status,activity1)values('$dog_insert_id','$image','draft','$tagline')";
	//$rt=query_execute_row("SELECT * FROM dogs_activity WHERE dog_id='$dog_insert_id' AND image='$image'");
	//if($rt['dog_id']==''){
	//query_execute("INSERT INTO dogs_activity(dog_id,image,publish_status,activity1)values('$dog_insert_id','$image','draft','$tagline')");
   	query_execute("UPDATE dogs_activity SET dog_id='$inserted_id_dog' WHERE id='$act_id'");
   	exit();
	header("Location: /contest/");
//$id=mysql_insert_id();
	//}
?>

<?	
} }?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<title>Dog Breeds | Complete Information On Dog Breeds | Wagpedia | Dog Breed Types</title>
<meta name="keywords" content=" Dog Breeds, Dog Breeds List, Dogs Breed Selector, Best Dog Breeds, Top Dog Breeds" />
<meta name="description" content="WagPedia - Complete Information On Dog Breeds, Temperament,Types, Pictures, Care, Diet,Training to help you determine which type of dog you should get." />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:title" content="Dog Breeds | Complete Information On Dog Breeds | Pictures | Types | Temperament | DogSpot.in" /> 
<meta property="og:description" content="Find Complete Information On Dog Breeds,Types, Pictures, Care, Diet. and this helps you determine which type of dog you should get at Dog Breeds Section with DogSpot.in" />
<style>
.fileUpload {
    position: relative;
    overflow: hidden;

}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.btn-primary {
  
    background-color:#d1d1d1;
    
}
.btn {
    display: block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
.contest_uploadblk{background:#f2f2f2; margin:10px; padding:15px;}
.ui-btn{    border: none !important;
    border-bottom: 1px solid #ddd;
    border-radius: 0px !important;
    padding-top: 10px !important;
    padding-bottom: 10px!important;
    color: #999;
    font-size: 14px !important;
    border-bottom: 1px solid #ccc !important;
}
.ui-input-btn{background-color:#d1d1d1 !important;  }
#image-holder{ margin-top:15px;}
.ui-select .ui-btn {
    
    border-radius: 0px !important;
}
.mrt{margin-top:15px !important;}
.dog_nameinput{background: #fff;border: none;border-bottom: 1px solid #ccc; padding-top: 10px !important;
    padding-bottom: 10px!important;}
</style>

<body>

<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<form method="post" enctype="multipart/form-data" action="javascript:void(0);">
<div class="contest_uploadblk">

<div class="fileUpload btn btn-primary">
 <span>Upload</span>
    <input id="fileUpload" class="upload" type="file" required multiple />
</div>
   <input type="hidden" name="act_id" id="act_id" value="<?=$id?>" />
    <div id="image-holder"></div>
    <div class="mrt">
<input type="text" name="dog_name" id="dog_name" required placeholder="Dog Name" class="dog_nameinput" >
</div>
    <div class="mrt">
<select name="breed" id="breed" required>
<option>Select Dog Breed</option>
 <?php $queryBreed=mysql_query("SELECT * FROM dog_breeds ORDER BY breed_name ASC");
	 while($rowBreed=mysql_fetch_array($queryBreed)){ 
	 ?>
     <option value="<?=$rowBreed['breed_id']?>"><?=$rowBreed['breed_name']?></option>
                  
                  <?php }?>
</select>
</div>
<div class="mrt">
<input type="Submit" name="acttag" id="acttag" value="Save">

</div>
<div  class="mrt">
<input type="reset" value="Reset">

</div>
</div>
</form>
<script>
	  $("#fileUpload").on('change', function () {

     //Get count of selected files
     var countFiles = $(this)[0].files.length;

     var imgPath = $(this)[0].value;
     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
     var image_holder = $("#image-holder");
     image_holder.empty();

     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
         if (typeof (FileReader) != "undefined") {

             //loop for each file selected for uploaded.
             for (var i = 0; i < countFiles; i++) {

                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $("<img />", {
                         "src": e.target.result,
                             "class": "thumb-image"
                     }).appendTo(image_holder);
                 }

                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
             }

         } else {
             alert("This browser does not support FileReader.");
         }
     } else {
         alert("Pls select only images");
     }
 });
	 </script>
     
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
