<?php 
include($DOCUMENT_ROOT."/constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT."/new/qna/qnacat.php");
$sitesection = "qna";

if($section[0]=='qna_questions'){
	header("HTTP/1.1 301 Moved Permanently"); header("Location: /qna/");
}
//Qna url in small
	$dty=strlen($section[1]);
	for($i=0;$i<$dty;$i++){
	if(strcspn($section[1], 'ABCDEFGHJIJKLMNOPQRSTUVWXYZ')==$i){
	$section[1]=strtolower($section[1]);
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/qna/$section[1]/");
	//exit();	
	}
	}
//Qna url in small Ends		
if($section[1]=="category"){ 
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/qna/$cat_nicename/");	
}

if($cat_nicename){
	$articlecat_id = $AqnaCatID[$cat_nicename];
	$pageT = $AqnaCat[$articlecat_id];
	$pageTitle=$AqnaCatTitle[$articlecat_id];
	$pageh1="$pageT - PET QUESTION & ANSWER FORUM";
	$pageKey=$AqnaCatKey[$articlecat_id];
	$pageDesc=$AqnaCatDesc[$articlecat_id];
	$pageDesc=trim(strip_tags($pageDesc));
	if(trim($articlecat_id)==''){
		header("HTTP/1.0 404 Not Found");
		require_once($DOCUMENT_ROOT.'/404.php');
		die(mysql_error());
		exit();
	}
}elseif($qnauser){
	$pageT = dispUname($qnauser);
	$pageTitle="$pageT, Dog Forum, Dogs Questions, Dogs India DogSpot.in";
	$pageh1="$pageT - PET QUESTION & ANSWER FORUM";
	$pageKey="$pageT, Dog Forum, Dogs Questions, Dogs India";
	$pageDesc="$pageT Ask Dogs Questions from Dogs Experts and give answers to the Questions Dogs India DogSpot.in";
	if(trim($pageT)==''){
		header("HTTP/1.0 404 Not Found");
		require_once($DOCUMENT_ROOT.'/404.php');
		die(mysql_error());
		exit();
	}
}else{
	$pageTitle="Pet Question & Answer Forum";
	$pageh1 = "Dogs Forum, Ask Dogs Questions from Dog Experts";
	if($filter=='recent'){
			$pageKey="Recently asked question, pet forum, pet QNA, dog forum, dog questions, dogs expert, cat forum, ask dogs questions, forum for small pet, recently answered pet questions.";
			$pageDesc="See Recently asked and answered pet questions forum allows you to ask queries from Pet Experts or help others by answering.";
		}
		elseif($filter=='popular'){
			$pageKey="Popular pet questions, pet forum, pet QNA, dog forum, dog questions, dogs expert, cat forum, ask dogs questions, small pet forum, answer pet questions.";
			$pageDesc="See popular pet questions and answers to find the solutions of your queries. Pet Forum allows you to ask queries from Pet Experts or help others by answering.";
		}else{
	$pageKey="asked question, pet forum, pet QNA, dog forum, dog questions, dogs expert, cat forum, ask dogs questions, forum for small pet, recently answered pet questions.";
	$pageDesc="asked and answered pet questions. Pet forum allows you to ask queries from Pet Experts or help others by answering.";
}}

if($del){
// check if he has rights to delete
$resultPostId = mysql_query("SELECT userid FROM qna_questions WHERE qna_id = '$qna_id'");
if(!$resultPostId){ die(sql_error()); }	
$rowPostid = mysql_fetch_array($resultPostId);
$postUserid = $rowPostid["userid"];

if($userid != 'Guest' && ($userid == $postUserid || $sessionLevel == 1)){	
}else{
   header ("Location: /");
}
// check if he has rights to delete END	
	
	$deleteReply = mysql_query ("UPDATE qna_reply SET publish_status='markdel' WHERE qna_id = '$qna_id'"); 
	
	// Update blog Count on users Table
	$selectArt = mysql_query ("SELECT userid FROM qna_questions WHERE qna_id = '$qna_id'");
	 if(!$selectArt){ die(sql_error()); }	
	$rowArt = mysql_fetch_array($selectArt);
	 $downUser = $rowArt["userid"];
	 downUserNum("users","num_qna",$downUser);
	 $resultdelete = mysql_query ("UPDATE qna_questions SET publish_status='markdel' WHERE qna_id = '$qna_id'");
	// Update blog Count on users Table END 
	//delete from friends update
	 delete_friends_update($qna_id, 'qna');
	//end of delete friends update
	 if (!$resultdelete){
		die(sql_error());
	 }else{
		// Create recent Events File
 	    //$recentevents=file_get_contents(MYSITE_URL."/backgroundjobs/create-home-recent-qna.php");
 	   // Create recent Events File
		 
		header ("Location: index.php?artuser=$userid");
	}
}

$BannerCount="1";
// List of Posts
$sqlall="SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_id DESC";
$sql="SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_id DESC LIMIT 0,12";
$sql3="SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_novisit desc LIMIT 1";
$sqlallcount="SELECT count(*) as countrecord FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_id DESC";

if($cat_nicename){
$qna_catid=$AqnaCatID[$cat_nicename];
$sqlall="SELECT * FROM qna_questions WHERE qna_catid = '$qna_catid' AND publish_status ='publish' ORDER BY qna_id DESC";
$sql="SELECT * FROM qna_questions WHERE qna_catid = '$qna_catid' AND publish_status ='publish'  ORDER BY qna_id DESC LIMIT 0,12";
$sql3="SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_novisit desc LIMIT 1";
$sqlallcount="SELECT count(*) as countrecord FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_id DESC";

}
if($qnauser){
if($qnauser == "Guest"){
$refUrl = $_SERVER['REQUEST_URI'];
header ("Location: /reg-log/?refUrl=/qna/?qnauser");
}
$sqlall="SELECT * FROM qna_questions WHERE userid = '$qnauser' AND publish_status ='publish'  ORDER BY qna_id DESC";
$sql="SELECT * FROM qna_questions WHERE userid = '$qnauser' AND publish_status ='publish'  ORDER BY qna_id DESC LIMIT 0,12";
$sql3="SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_novisit desc LIMIT 1";
$sqlallcount="SELECT count(*) as countrecord FROM qna_questions WHERE userid = '$qnauser' AND publish_status ='publish' ORDER BY qna_id DESC";

}

if ($filter){
		if ($filter=='recent') {
			$pageT='recent';
			$pageh1="Most Recents";	
			$sql="SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_cdate DESC LIMIT 0,12";
			$sqlall = "SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_cdate  DESC ";	
			$sqlallcount="SELECT count(*) as countrecord FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_cdate DESC";

			}
	else if ($filter=='popular'){
			$pageT='popular';
			$pageh1="Most Popular";	
			$sql="SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_novisit DESC LIMIT 0,12";
			$sqlall = "SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_novisit  DESC";	
			$sqlallcount="SELECT count(*) as countrecord FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_cdate DESC";

			}
	else if ($filter=='answer')	{
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	exit();

	}
	else if ($filter=='question'){
	 $pageT='question';
	$pageh1="Resolved Questions ";	
	$sql="SELECT * FROM qna_questions WHERE publish_status ='publish'  ORDER BY qna_novisit DESC LIMIT 0,12 ";
	$sqlall = "SELECT * FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_novisit  DESC";	
	$sqlallcount="SELECT count(*) as countrecord FROM qna_questions WHERE publish_status ='publish' ORDER BY qna_cdate DESC";

	}
	else if ($filter=='user2'){
	$pageh1=" Ask Questions ";	
	header("Location: /new/qna/new.php/");
	}
	else {
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	exit();
	}
}


$selectArt = mysql_query ("$sql");
$selectArtcount = query_execute_row("$sqlallcount");
 if(!$selectArt){  die(mysql_error());  }
$selectArtall = mysql_query ("$sqlall");
 if(!$selectArtall){	die(mysql_error());	}	
$selectArt3 = mysql_query ("$sql3"); 
 if(!$selectArt3){  die(mysql_error());  }
	$totrecord = mysql_num_rows($selectArt);
?>

 <?php require_once($DOCUMENT_ROOT.'/new/common/header-bootstrap.php');?> 
  <link type="text/css"  rel="stylesheet" href="/bootstrap/css/qna.css" />
  
 <!-- breadcrumb -->
 <div class="breadcrumbs" >
  <? $string = str_replace("-", " ", $section[2]);?>
   <div class="container">
  <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
    <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <a href="/qna/" itemprop="item"><span itemprop="name">Dog Q&A </span> </a>
      <meta itemprop="position" content="1" /> </span>
     <span> / </span>
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <span itemprop="item"><span itemprop="name" class="active-bread"><? if($section[1]=='filter'){ echo ucwords($section[2]); }else{?><?=$AqnaCat[$qna_catid];?><? }?></span></span>
       <meta itemprop="position" content="2" /> </span>
    </div>
    </div>
  </div>
</div>

<section class="qna-wrap" >
<div class="container">
<div class="row">
<!-- qna content section start-->
<div class="qna-content qna-margin-top">
<!-- left side start-->
<div class="col-md-8 col-xs-12">
<div class="qna-contentsec">
<div class="qna-searchsec qna-borderbotttom-gray">
<form action="/new/qna/search-bootstrap.php" method="GET">
<input type="text" placeholder="Search Questiions and Answer" name="data" id="data" />
<input type="submit" value="Search Answer" />
</form>
</div>
<!-- most recent question start-->
<div class="qna-recent-ques">
<h1 class="qna-recent-title"><? echo"$pageh1";?></h1>
<div id="txt1" style='display:none'><?= $pageT; ?></div>
<div id="txt2" style='display:none'><?= $selectArtcount['countrecord']; ?></div>
<div class="qna-bottom-border">&nbsp;</div>
</div>
<!-- questions section start-->
<div class="qna-recentques-cont" id="cont660">
<!-- recent ques start-->
       <? $iid=0;
while($rowArt = mysql_fetch_array($selectArt)){
			$iid=$iid+1;
            $qna_id = $rowArt["qna_id"];
            $qnauser = $rowArt["userid"];
			$qnadesc = $rowArt["qna_desc"];
            $qna_question =htmlspecialchars_decode( $rowArt["qna_question"]);
            $qna_name = $rowArt["qna_name"];
            $qna_tag = stripslashes($rowArt["qna_tag"]);
            $qna_catid = $rowArt["qna_catid"];
            $qna_cdate = $rowArt["qna_cdate"];
            $qnadesclen=strlen($qnadesc);
			$qnaqtnlen=strlen($qna_question);
            $qna_question = nl2br(stripslashes($qna_question));
            $qna_question = breakLongWords($qna_question, 30, " ");
			$qnadesc =htmlspecialchars_decode( stripallslashes(makeTeaser($qnadesc, 150)));
			$qnadesc=make_outlink_nofollow($qnadesc);
			$qna_question=make_outlink_nofollow($qna_question);
$rowUser=query_execute_row("SELECT f_name, image FROM users WHERE userid='".$rowArt['userid']."'");

if($rowUser["image"]){
	$profileimgurl='/profile/images/50x40-'.$rowUser["image"];
	$src = $DOCUMENT_ROOT.'/profile/images/'.$rowUser["image"];
}else{
	$profileimgurl='/imgthumb/50x40-noimg.gif';
	$src = $DOCUMENT_ROOT.'/images/noimg.gif';
	
}
$dest=$DOCUMENT_ROOT.$profileimgurl;
createImgThumbIfnot($src,$dest,'50','40','ratiowh');
list($width1, $height1) = getimagesize($DOCUMENT_ROOT .$profileimgurl);
					  ?>
                       <? // function for image with no link for guest......... 
				
			    // function ends here by sagar.......................?>
<?	echo "<div class='imgtxtcontt' id='$iid'>"; ?>                
<div class="qna-recentques-list qna-borderbotttom-gray">
<div class="qna-user-img-sec">
<img src="<?=$profileimgurl;?>" alt="<?=$rowUser['f_name'];?>" title="<?=$rowUser['f_name'];?>" width="35" height="<?=$height1;?>" />
</div>
<div class="qna-question-sec">
<div class="recent-question"><a href="<? echo"/qna/$qna_name/";?>"><?=makeTeaser(ucfirst($qna_question),100);?></a></div>
<? if($qnadesc!=''){?>
<div class="qna-description"><?=stripallslashes(htmlspecialchars_decode(ucfirst($qnadesc)));?><? if($qnadesclen > 149){ ?>.. <a class="qna-readmore" href="/qna/<?=$qna_name?>/">Read More</a><? }?></div>
<? }?>
<div class="qna-ques-ask-details">
<span><? if(($qnauser == $userid || $sessionLevel ==1 ) && $userid!="Guest"){/* echo"<a href='/new/qna/new.php?qna_id=$qna_id'>Edit</a>";?> | <a href="<? echo"javascript:confirmDelete('?qna_id=$qna_id&del=del')"; ?>">Delete</a> | <? */}?>By <? guestredirect($userid, $rowUser["f_name"],$qnauser) ?></span> <span class="ds_fz_14">·</span> <span><a href="/qna/<?=$AqnaCatNice[$qna_catid];?>/"><?=$AqnaCat[$qna_catid];?></a></span> <span class="ds_fz_14">·</span> <span> <? print(showdate($qna_cdate, "d M o g:i a")); ?></span>
<span class="ds_fz_14">·</span> <span><a href="<? echo"/qna/$qna_name/";?>">Reply Now</a></span>
</div>
</div>

</div>
<? echo"</div> "?> 
<? }?>
</div>
<!-- recent ques end-->
<!-- most recent question end-->
</div>
<!--left side end-->
</div>
<!-- side bar start-->
<aside class="col-md-4 col-xs-12">
<? include($DOCUMENT_ROOT . "/new/qna/include-left-bootstrap.php"); ?>
</aside>
<script type="text/javascript">
var cat_nice='';
 $(document).ready(function() { 
 	var cat_nice=document.getElementById('txt1').innerHTML;
	var c=0;
 	countr=document.getElementById('txt2').innerHTML;
	$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   	if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	if(c!=3){
	$.ajax({
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/new/qna/loadmore.php?lastComment="+ $(".imgtxtcontt:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#cont660").append(html);$('#loadMoreComments').hide();c=c+1;
	
	}else {
    $('#loadMoreComments').html();
    }
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
	}else{
		e12=  $(".imgtxtcontt:last").attr('id');
		$('#cval').val(e12);
		if(e12!=countr){
		$("#cont660").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>Load More</div>");
		c=0;
	}$('#loadMoreComments').hide();
	} // c condition close
	}
	});
	
	});
</script>
<script>
function get(rt){
	$('#divw'+rt).hide();
	var cat_nice=document.getElementById('txt1').innerHTML;
	countr=document.getElementById('txt2').innerHTML;
	
	c=0;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	
	if(c!=3){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/new/qna/loadmore.php?lastComment="+ $(".imgtxtcontt:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#cont660").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
	else{
		e12=  $(".imgtxtcontt:last").attr('id');
		if(e12!=countr){
		$("#cont660").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
		c=0;}
		$('#loadMoreComments').hide();
	
	} // c condition close-->
	}
	
	}

</script>
<div id='loadMoreComments' style='display:none'><img src="/new/pix/loading2.gif" /></div></div>
<!-- side bar end-->

<!--qna section end-->
</div>
</div>
</section>

<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?> 