<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<!--TOP HTML-->
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Blogs" href="http://feeds.feedburner.com/dogspotblog" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Q&A" href="http://feeds.feedburner.com/DogspotinQna" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Gallery " href="http://feeds.feedburner.com/DogspotDogs" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Puppies Available" href="http://feeds.feedburner.com/DogspotPuppies" />

<!-- main Analytics profile code -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1552614-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
<? if($_SESSION['DogSpotDevice']=='DogSpotDesktop'){?>
function loadsubscribe(){
	centerPopup();
	loadPopup();
}  <? }?>
window.onload=function(){
<? if($sitesection == "photos"){?>
	uploadPhotos();
<? }?>	
<? if($sitesection == "videos"){?>
	uploadvideos();
<? }?>
}
<!--Main Analytics Profile-->
</script>
<link rel="stylesheet" type="text/css" href="/shapopup/shapopup.css" media="all"  />
<script src="/shapopup/shapopup.js" type="text/javascript"></script>
<script src="/jquery/jquery.validate-1.9.js" type="text/javascript"></script>
<link href="/new/css/login-style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript" src="/js/highslide-html.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
    hs.outlineType = 'rounded-white';
    hs.outlineWhileAnimating = true;
    hs.objectLoadTime = 'after';
</script>
<!--TOP HTML END-->
</head>
<? if($sitesection == "friends"){?>
<body>
<? }elseif($sitesection == "msg"){?>
<body onLoad="LoadHAjax()">
<? }elseif($ajaxbody == "puppie"){?>
<body>
<? }elseif($ajaxbody == "dog"){ ?>
<body>
<? }elseif($sitesection == "dog"){ ?>
<body onLoad="ajaxload();">
<? }elseif($ajaxbody == "puppynew"){ ?>
<body>
<? }elseif($ajaxbody == "cat"){ ?>
<body >
<? }else{
echo"<body>";
}
?>

	<!-- header -->
    
    	<div class="header" style="height:40px;">
        <div class="cont460 topcc" style="width:600px;">Customer Care<br /> +91-9818809553</div>
        	<div class="vs15"></div>
            <p align="right" class="login" style="margin-top:0px;">
            <? if ($userid != "Guest" && $sessionName){  echo $sessionName.' | <a href = "/profile/">My Profile</a> | <a href = "/new/shop/wishlist/">My Wishlist</a> | <a href = "/home.php?logoff=logoff">Logout</a>'; }else{?>
            <div style="float:right;margin-left:15px;">
            <p class="login2"><a href="/login.php">Login</a> / <a href="/reg-log/register-1.php">Register</a></p>
            </div>
            <div style="float:right;margin-left:15px;"> 
			<?php include($DOCUMENT_ROOT."/googleconnect/googlebutton.php");?> 
            </div>
            <div style="float:right;margin-left:15px;">
			<?php include($DOCUMENT_ROOT."/facebookconnect/facebookbutton.php");?>
            </div>
            
            <? }?>
            </p>
            <div class="cb"></div>
        </div>
    	  
    

        
     <!-- green strip -->
	<div class="greenstrip">
		<div class="header">
			
            		<!-- logo cont -->
                    <div class="fl mt16">
            			<p align="left"><a href="https://www.dogspot.in/"><img src="/new/pix/dogspot-logo-beta.gif" alt="Dogspot | Spot for all your Dog needs " title="Dogspot | Spot for all your Dog needs"  /></a></p></div>
                     <!-- logo cont -->
                     <!-- right link cont -->
     				<div class="fr cont497">
                  
                    	<ul class="rightHeader">
                            <li  style="width:132px;"><span><a href="/shop/" class="newtab"><b>shop</b>1200 products</a></span></li>
                            <li  style="width:183px;"><span><a href="/puppies/"><b>classifieds</b>3000+ puppies available</a></span></li>
                            <li  style="width:187px; margin-right:0px; padding-right:0px; border:0;"><span><a href="/qna/"><b>answers</b>2500+ questions</a></span></li>
       
   						</ul>
                    <span class="cl"></span>
                    
                    
					<!-- search box-->
                    	
							<div  class="search" >
                            	<div style="padding:10px 5px 0px 10px;">
                    			<div class="searchBox" >

								<div id='input-search-container' style="position:relative">
                           <div id='search-cont'>
                           <form action="https://www.dogspot.in/search.php" id="cse-search-box">
                           <input type='text' id='search-query' value="Enter the Keywords" style="color:#666;" name="q" onClick="clickclear(this, 'Enter the Keywords')" onBlur="clickrecall(this,'Enter the Keywords')"/> 
                            <input type="hidden" name="cx" value="partner-pub-7080808620760591:8w0nse-xhme" />
        					<input type="hidden" name="cof" value="FORID:11" />
        					<input type="hidden" name="ie" value="ISO-8859-1" />
                               
                           </form>
                           </div>
                        </div>

                        </div>
                        <div style="float:left; width:50px;">
						<a href="#"><img src="/new/pix/search_btn.gif" alt="searh" title="search"/></a>
						</div>
						<div class="cb"></div>

                        </div>
                        </div><!-- search box-->

                   
                    
                    </div> <!-- right link cont -->
            
     		
     <div class="cb"></div>
		</div><!-- end of header -->
		
</div><!-- end of green strip -->
    
    
     
  
        <!-- navigation -->
        <div class="petSpecificSecNavBox">
    		<div class="wrap">
                 <div class="petSpecificNavigation">
                     <div class="petSpecificsecNavSide"></div>
    
                   
                        <div id="siteNav">
                        <div id="naviCont">
   		  <ul class="dropdown" style="margin:0px; padding:0px; list-style:none;">
		    <li><a href="/" class="dir" style="padding-left:12px;">home</a></li>
			  
           	<li class="linkdiv"><a href="/dog-events/" class="dir">dog shows</a>
  								<!--<ul>
                              <li >
                                    <strong>shop <em>categories</em></strong>
                                     <a href="/shop/category/treats-food">Food</a>
                                       <a href="/shop/category/puppy-care">Puppy Care</a>
                                         <a href="/shop/category/treats">Treats</a>
                                           <a href="/shop/category/collars-leashes">Collars Leashes</a>
                                             <a href="/shop/category/accessories">Toys</a>
                                                <a href="/shop/category/accessories">Accessories</a>
                                                   <a href="/shop/category/dog-grooming">Gromming</a>
                                                    <a href="/shop/" class="viewall">Find All Categories</a>
                                             
                                    
                                </li>
                                <li style="border:none; padding-right:0px; margin-left:8px;">
                               		 <strong>shop <em>brands</em></strong>
                                     <a href="/shop/brand/royal-canin">Royal Canin</a>
                                     <a href="/shop/brand/ourpet_1">Durapet</a>
                                     <a href="/shop/brand/flexi">Flexi</a>
                                     <a href="/shop/brand/kong">Kong</a>
                                    <a href="/shop/brand/karlie">Karlie</a>
                                    <a href="/shop/brand/eukanuba">Eukanuba</a>
                                    <a href="/shop/brand/isle-dogs">Isle of Dogs</a>
                                    <a href="/shop/brand" class="viewall">Find All Brands</a>
                              </li>
		    	
						</ul>-->
                

   		 </li>
         <li ><a href="/dog-blog/" class="dir">articles</a></li>
         <li><a href="/shop/" class="dir newtab">shop</a>
		 
				
		</li>
		<li class="linkdiv"><a href="/puppies/" class="dir">puppies available</a></li>
		<li class="linkdiv"><a href="/dog-listing/" class="dir">business</a>
		 
		</li>
		<li class="linkdiv" style="position:relative;"><a href="/dogs/" class="dir">dog gallery</a>
		
		</li>
		<li  style="border:none; padding-right:0px; background-image:none; position:relative"><a href="/qna/" class="dir">answers</a>
		 
		</li>
    	
</ul>

 <div class="cb"></div>
 </div></div>
                        
                        <!-- navi drop down -->
                    <div class="petSpecificsecNavSide"></div>
                </div>
            </div>
         </div><!-- navigation -->