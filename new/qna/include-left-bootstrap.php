<script>
//Ask Question Jquery Alert And Function Call By Id By Bharat
$(document).ready(function(e) {
	$("#qnaclickdet").click(function(){
		$( "#qnaopenbox" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});
$("#qtnbox").click(function(){
	$("#userpro").css("display", "none");
});
//Show Validation Errors By Bharat
$("#btsubmit").click(function(){
var qtn = $("#qtnbox").val();
var qna_catid = $("#qna_catid").val();
var cs = $("#qtnbox").val().length;	
if(qtn!=''){
	if(cs<50){
	alert('Please input minimum 50 characters.');
	return false;} }	
if(qtn=='' && qna_catid!=''){
	$("#qtnans").css("display", "block");
	$("#qtnblank").css("display", "none");
	$("#qtncat").css("display", "none");
	$("#qtncharerror").css("display", "none");
	$("#userpro").css("display", "none");
		}else if(qna_catid=='' && qtn!=''){
		$("#qtncat").css("display", "block");
		$("#qtnans").css("display", "none");
		$("#qtnblank").css("display", "none");
		$("#qtncharerror").css("display", "none");
		$("#userpro").css("display", "none");
		}else if(qna_catid=='' && qtn==''){
		$("#qtnblank").css("display", "block");
		$("#qtncat").css("display", "none");
		$("#qtnans").css("display", "none");
		$("#qtncharerror").css("display", "none");
		$("#userpro").css("display", "none");
		}
	else{
	var qna_catid=$("#qna_catid").val();
	var qtnbox=$("#qtnbox").val();
	var qnaopenbox=$("#qnaopenbox").val();
	var user_id=$("#user_id").val();
ShaAjaxJquary('/new/qna/insert-data.php?qna_catid='+qna_catid+'&qtnbox='+qtnbox+'&qnaopenbox='+qnaopenbox+'&user_id='+user_id+'', "#userpro", '', '', 'POST', "#userpro", 'checking...', 'REP');
		<? setcookie ("qnavallk", "", time() - 3600);?>
		$("#qtnblank").css("display", "none");
		$("#qtncat").css("display", "none");
		$("#qtnans").css("display", "none");
		$("#qtncharerror").css("display", "none");
		$('#qna_catid').val("");
		$('#qtnbox').val("");
		$('#qnaopenbox').val("");
	}
});
$("#qnaopenbox").click(function(){
$("#qtncharerror").css("display", "none");	 
});
});
</script>
<!--character count-->
<script >
(function($){  
	$.fn.textareaCount = function(options, fn) {   
		var defaults = {  
			maxCharacterSize: -1,  
			originalStyle: 'originalTextareaInfo',
			warningStyle: 'warningTextareaInfo',  
			warningNumber: 20,
			displayFormat: '#input characters | #words words'
		};  
		var options = $.extend(defaults, options);
		
		var container = $(this);
		var fgtr = $("#erty");
		
		$("<div class='charleft'>&nbsp;</div>").insertAfter(fgtr);
		
		//create charleft css
		var charLeftCss = {
			'width' : fgtr.width()
		};
		
		var charLeftInfo = getNextCharLeftInformation(fgtr);
		charLeftInfo.addClass(options.originalStyle);
		charLeftInfo.css(charLeftCss);
		
		var numInput = 0;
		var maxCharacters = options.maxCharacterSize;
		var numLeft = 0;
		var numWords = 0;
				
		container.bind('keyup', function(event){limitTextAreaByCharacterCount();})
				 .bind('mouseover', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);})
				 .bind('paste', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);});
		
		
		function limitTextAreaByCharacterCount(){
			charLeftInfo.html(countByCharacters());
			//function call back
			if(typeof fn != 'undefined'){
				fn.call(this, getInfo());
			}
			return true;
		}
		
		function countByCharacters(){
			var content = container.val();
			var contentLength = content.length;
			
			//Start Cut
			if(options.maxCharacterSize > 0){
				if(contentLength > 99){
							$( "#qnaopenbox" ).slideDown( "slow", function() {
    // Animation complete.
  });	
    $("#qtncharerror").css("display", "block");
  		$("#qtnblank").css("display", "none");
		$("#qtncat").css("display", "none");
		$("#qtnans").css("display", "none");		
				}else{
					$("#qnaopenbox").css("display", "none");
					$("#qtncharerror").css("display", "none");
					}
				//If copied content is already more than maxCharacterSize, chop it to maxCharacterSize.
				if(contentLength >= options.maxCharacterSize) {
					content = content.substring(0, options.maxCharacterSize); 				
				}
				
				var newlineCount = getNewlineCount(content);
				
				// newlineCount new line character. For windows, it occupies 2 characters
				var systemmaxCharacterSize = options.maxCharacterSize - newlineCount;
				if (!isWin()){
					 systemmaxCharacterSize = options.maxCharacterSize
				}
				if(contentLength > systemmaxCharacterSize){
					//avoid scroll bar moving
					var originalScrollTopPosition = this.scrollTop;
					container.val(content.substring(0, systemmaxCharacterSize));
					this.scrollTop = originalScrollTopPosition;
				}
				charLeftInfo.removeClass(options.warningStyle);
				if(systemmaxCharacterSize - contentLength <= options.warningNumber){
					charLeftInfo.addClass(options.warningStyle);
				}
				
				numInput = container.val().length + newlineCount;
				if(!isWin()){
					numInput = container.val().length;
				}
			
				numWords = countWord(getCleanedWordString(container.val()));
				
				numLeft = maxCharacters - numInput;
			} else {
				//normal count, no cut
				var newlineCount = getNewlineCount(content);
				numInput = container.val().length + newlineCount;
				if(!isWin()){
					numInput = container.val().length;
				}
				numWords = countWord(getCleanedWordString(container.val()));
			}
			
			return formatDisplayInfo();
		}
		
		function formatDisplayInfo(){
			var format = options.displayFormat;
			format = format.replace('#input', numInput);
			format = format.replace('#words', numWords);
			//When maxCharacters <= 0, #max, #left cannot be substituted.
			if(maxCharacters > 0){
				format = format.replace('#max', maxCharacters);
				format = format.replace('#left', numLeft);
			}
			return format;
		}
		
		function getInfo(){
			var info = {
				input: numInput,
				max: maxCharacters,
				left: numLeft,
				words: numWords
			};
			return info;
		}
		
		function getNextCharLeftInformation(container){
				return container.next('.charleft');
		}
		

		function isWin(){
			var strOS = navigator.appVersion;
			if (strOS.toLowerCase().indexOf('win') != -1){
				return true;
			}
			return false;
		}
		
		function getNewlineCount(content){
			var newlineCount = 0;
			for(var i=0; i<content.length;i++){
				if(content.charAt(i) == '\n'){
					newlineCount++;
				}
			}
			return newlineCount;
		}
		
		function getCleanedWordString(content){
			var fullStr = content + " ";
			var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
			var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
			var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
			var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
			var splitString = cleanedStr.split(" ");
			return splitString;
		}
		
		function countWord(cleanedWordString){
			var word_count = cleanedWordString.length-1;
			return word_count;
		}
	};  
})(jQuery); 
        </script>
		<script type="text/javascript">
			$(document).ready(function(){
				var options3 = {
						'maxCharacterSize': 100,
						'originalStyle': 'originalTextareaInfo',
						'warningStyle' : 'warningTextareaInfo',
						'warningNumber': 10,
						'displayFormat' : '#left'
				};
				$('#qtnbox').textareaCount(options3, function(data){
				});
			});
</script>
<script>
function setcookie4(){
    var text_valkl = $("#qtnbox").val();
	document.cookie = "qnavallk=" + text_valkl;
}
</script>

<?
if(isset($_COOKIE['qnavallk'])){
    $text_valkl = $_COOKIE['qnavallk'];
}
?>
<div class="row">
<!-- ask ques box start-->
<section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
<div class="qna-ask-qbox">
<h3 class="qna-ask-ques">Ask a Question</h3>
<div style="display:none" class="qna-error-box" id="qtnans">Please Enter Question.</div>
<div style="display:none" class="qna-error-box" id="qtncat">Please Select Category.</div>
<div style="display:none" class="qna-error-box" id="qtnblank">Field Should Not Be Blank</div>
<div style="display:none" class="qna-error-box" id="qtncharerror">100 Characters Limit Exceeded. Add More Details in Description.</div>
<div  id="userpro"></div>
<div class="qna-ask-text-box">
<textarea placeholder="Share Your Question" id="qtnbox" maxlength="100"><?=$text_valkl?></textarea>
</div>
<div class="qna-ask-desc-box">
<textarea placeholder="Description" id="qnaopenbox" style="display:none;"></textarea>
<div class="qna-ques-desc" id="qnaclickdet"><a><div><i class="fa fa-file-text"></i> Description</div></a></div>
<!--select category section-->
<div class="qna-category-dropdown">
<select name="qna_catid" id="qna_catid" >
<option value="">Select Category</option>
	 <? $sqqncatop=query_execute("SELECT cat_desc, cat_nicename, cat_id FROM qna_cat ORDER BY cat_desc ASC");
        while($rowCat = mysql_fetch_array($sqqncatop)){
         $cat_id = $rowCat["cat_id"];
         $cat_desc = $rowCat["cat_desc"];
     ?>
    <option value="<? echo"$cat_id";?>" <? if($cat_id==$qna_Oldcatid){ echo"selected"; } ?>><? echo"$cat_desc";?></option>
   <? } ?>
</select>
<input type="hidden" id="user_id" value="<?=$userid;?>"/>
<? if($userid!='Guest'){?>
<a class="qna-question-submit-btn" id="btsubmit"  >Submit</a>
<? }else{?>
<a href="#modal" style="float:left;" id="modal_trigger2" class="bharat qna-question-submit-btn" onclick="setcookie4();">Submit</a>
<? }?>
<div id="erty"></div>
</div>
<!-- category section end-->

</div>

</div>
</section>
<?php /*?><section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;"><?
require_once($DOCUMENT_ROOT . '/product_banner.php');
?>
</section><?php */?>

<!--<div class="qna-sidebar-banner">
<?php /*?><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- DogSpot-300x250-new -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7080808620760591"
     data-ad-slot="6096310357"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script><?php */?>
<?php
require_once($DOCUMENT_ROOT . '/banner1.php');
//addbanner300('250', '300', '1');
?>
</div>-->
<!-- ask ques box end-->
<!-- qna category list start--><section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
                                                                <!-- Widget Header -->
                                                                <header class="clearfix">
                                                                        <h4>Browse Category</h4>
                                                                        </header>
<div class="qna-category-sec">

<ul class="qna-category-list">
<? $sqqncat=query_execute("SELECT cat_desc, cat_nicename, cat_id FROM qna_cat ORDER BY cat_desc ASC");
while($gesqqcat=mysql_fetch_array($sqqncat)){
$sqlcdacat=mysql_query("SELECT COUNT(qna_id) as totrec FROM qna_questions WHERE qna_catid='".$gesqqcat['cat_id']."' AND publish_status='publish'");	
$trrecord=mysql_fetch_array($sqlcdacat)?>
<li><a href="/qna/<?=$gesqqcat['cat_nicename'];?>/"><?=$gesqqcat['cat_desc'];?> (<?=$trrecord['totrec'];?>)</a></li>
<? }?>
</ul>
</div>
</section>
<!-- qna category list end--><section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;"> 
                                                                <!-- Widget Header --> 
                                                                <header class="clearfix"> 
                                                                        <h4>Sponsored: In the Stores</h4> 
                                                                                                        </header> 
<?php
$getItems = query_execute("SELECT si.name,si.nice_name,si.item_id,sia.price,sia.mrp_price,si.item_parent_id FROM shop_items as si,shop_item_affiliate as sia WHERE sia.item_id=si.item_id AND showbanner='1' ");
while ($fetchaffiliate = mysql_fetch_array($getItems)) {
    $name           = $fetchaffiliate['name'];
    $item_id        = $fetchaffiliate['item_id'];
    $item_parent_id = $fetchaffiliate['item_parent_id'];
    $nice_name      = $fetchaffiliate['nice_name'];
    $price          = $fetchaffiliate['price'];
    $mrp_price      = $fetchaffiliate['mrp_price'];
    $rowdatM        = query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
    if (!$rowdatM['media_file']) {
        $rowdatM = query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
    }
    
    $imglink = 'https://ik.imagekit.io/2345/tr:h-100,w-100,c-at_max/shop/item-images/orignal/' . $rowdatM['media_file'];
    
    
?> 
                       <div class="gurgaon_offers">  
             
                <div class="gurgaon_offersr"> <a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><img src="<?= $imglink ?>" width="100" height="93"></a> </div> 
                <div class="gurgaon_offersl"> 
                    <div class="gurgaon_offers_pn"><a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><?= $name ?></a> </div> 
                                        <div class="gurgaon_offers_pr"> 
                                        <?php
    if ($mrp_price > $price) {
?> 
                                       <span>Rs. <del><?= $mrp_price ?></del>  
                                        </span> &nbsp; &nbsp; <?php
    }
?> 
                                       <span class="price-color"> 
                                        Rs. <?= $price ?></span> 
                                       <div class="aff-m-logo"> <a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><img src="/new/articles/img/amazon.jpg" /></a> 
                                       </div> 
                                        </div> 
                                    </div> 
               
        </div> 
     <?php
}
?> 
        </section>
<section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
                                                                <!-- Widget Header -->
                                                                <header class="clearfix">
                                                                        <h4>Related Articles</h4>
                                                                        </header>
<div class="qna-releted-article">
<div class="qna-releted-art-text"></div>
<div class="qna-bottom-border">&nbsp;</div>
<ul class="qna-releted-artsec">
<? 
//mysql_select_db("test");
$getqnablogp=mysql_query("SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wtt.term_id='386' GROUP BY wpp.ID ORDER BY wpp.views DESC LIMIT 0,5");
while($rowArt=mysql_fetch_array($getqnablogp)){
    $articlecat_id = $rowArt["term_id"];
	$article_views = $rowArt["views"];
	$sel_cat=query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
	$cat_name=$sel_cat['name'];
	$cat_nice=$sel_cat['slug'];	
    $article_id    = $rowArt["ID"];
    $post_title   = $rowArt["post_title"];
    $post_content      = $rowArt["post_content"];
    $post_date        = $rowArt["post_date"];
    $artuser_wp       = $rowArt["post_author"];
	$post_statuss       = $rowArt["post_status"];
	//mysql_select_db("test");
	$sel_cmnts=mysql_query("SELECT * FROM wp_comments WHERE comment_post_ID='$article_id'");
	$tot_cmnts=mysql_num_rows($sel_cmnts);
	$rowUser_id  = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$artuser_wp'");
	$artuser = $rowUser_id['user_login'];
    $post_name      = $rowArt["post_name"];
	$imgURL    = get_first_image($post_content, $DOCUMENT_ROOT);
	$imgURLAbs = make_absolute($imgURL, 'https://www.dogspot.in');
    $post_title = stripslashes($post_title);
    $post_title = breakLongWords($post_title, 20, " ");
    
    // Get Post Teaser
    $post_content = stripslashes($post_content);
    $post_content = strip_tags($post_content);
    $post_content = trim($post_content);
    $post_content = substr($post_content, 0, 90);
    
    $post_content = stripslashes($post_content);
    $post_content = breakLongWords($post_content, 30, " ");
		$check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id' AND post_type='attachment' AND post_mime_type like '%image%' ORDER BY ID DESC");
	//mysql_select_db("dogspot_dogspot");
    $rowUser1 = query_execute_row("SELECT f_name, image FROM users WHERE userid='$artuser'");
	    if($check_img['guid'] !=''){
		$imgURLAbs=$check_img['guid'];	
		$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/90x73-'.$image_name;
		}
		else if (strpos($imgURLAbs,'wordpress') !== false) {
    	$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/90x73-'.$image_name;
		}
   else if ($imgURLAbs!='https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
	   $URL=$imgURLAbs;
	   $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
	   $pos = strrpos($image_name,'/');
	   $image_name = substr($image_name,$pos+1);
		$extension = stristr($image_name,'.');
		if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
		$image_name;
		}
		$src = $DOCUMENT_ROOT.'/userfiles/images/'.$image_name;
		//$src = $imgURL;
		$imageURL='/imgthumb/90x73-'.$image_name;
		} 
		else if($rowUser1["image"]) {
		$src = $DOCUMENT_ROOT.'/profile/images/'.$rowUser1["image"];
		$imageURL = '/imgthumb/90x73-'.$rowUser1["image"];
		} 
		else  {
        $src = $DOCUMENT_ROOT.'/images/noimg.gif';
		$imageURL = '/imgthumb/90x73-noimg.gif';
		}
		
		$dest = $DOCUMENT_ROOT.$imageURL;
		createImgThumbIfnot($src,$dest,'90','73','ratiowh');	
	
	
	 if(file_exists($dest)){
		  }else{
		  $imageURL="/imgthumb/90x73-noimg.gif";
		  }
	?>
<li>
<div class="qna-rel-art-img">
<a href="<? echo "/$post_name/";?>">
<img src="<?=$imageURL?>" alt="<?=$post_title ?>"  title="<?=$post_title ?>" /></a></div>

<div class="qna-rel-art-headline">
<a href="/<?=$post_name?>/"><?= $post_title ?></a>
</div>
</li>
<? }?>
</ul>
</div>
</section>
<section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
<div class="qna-sidebar-banner">
<?php /*?><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- DogSpot-300x250-new -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7080808620760591"
     data-ad-slot="6096310357"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script><?php */?>
<?php
require_once($DOCUMENT_ROOT . '/banner1.php');
//addbanner300('250', '300', '1');
?>
</div>
</section>
</div>
