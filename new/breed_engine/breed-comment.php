<?php
require_once($DOCUMENT_ROOT . '/constants.php');
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT . '/arrays.php');
include($DOCUMENT_ROOT . "/new/articles/catarray.php");
include($DOCUMENT_ROOT . "/new/qna/qnacat.php");
include($DOCUMENT_ROOT."/dogs/arraybreed.php");
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = '';
?>
<?php
$breednameselect = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$breed_id'");
$breedname       = $breednameselect['breed_name'];
$be_name       = $breednameselect['be_name'];
$breednice       = $breednameselect['nicename'];
$icon            = $breednameselect['icon'];
$img             = $breednameselect['image_name'];
$ht              = $breednameselect['height'];
$wt              = $breednameselect['weight'];
$imglink         = $breednameselect['img_link'];
$img_dog_name    = $breednameselect['img_dog_name'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$be_name?> Review</title>
<meta name="keywords" content="<?=$selecttitle['keyword']?>" />
<meta name="description" content="<?=$selecttitle['description']?>" />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.dogspot.in/<?= $breednice ?>/" /> 
<meta property="og:title" content="<?=$selecttitle['title']?>" /> 
<meta property="og:description" content="<?=$selecttitle['description']?>" />
<meta property="og:image" content="https://www.dogspot.in/new/breed_engine/images/dog_images/<?= $img ?>" /> 

<meta name="twitter:card" content="photo">
<meta name="twitter:site" content="@indogspot">
<meta name="twitter:url" content="https://www.dogspot.in/<?= $breednice ?>/">
<meta name="twitter:title" content="<?=$selecttitle['title']?>">
<meta name="twitter:description" content="<?=$selecttitle['description']?>">
<meta name="twitter:image" content="https://www.dogspot.in/new/breed_engine/images/dog_images/<?= $img ?>">
<meta name="twitter:image:width" content="610">
<meta name="twitter:image:height" content="610">
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>

<link rel="stylesheet" href="/new/breed_engine/css/be_style.css?v=07-04-14" />
<link rel="stylesheet" href="/new/breed_engine/css/starmatrix1.css" />
<link href="/new/css/toll-tip.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/new/css/jquery-ui.css" />
<script type="text/javascript" src="/new/js/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<script>
$(function() {
$(".tooltip").tooltip();
});
</script>

<script type="text/javascript">
function postcomment(){
var cmnt_tit=$("#tit").val();
var cmnt=$("#cmnt").val();
if(cmnt==''){
	$("#cmnt").css("border-color","#F00");
	var error="1";
}else{
	$("#cmnt").css("border-color","#CCC");
	var error="0";
}
if(error=='0'){
var user=$("#user").val();
if(user=='Guest'){
	window.location="/login.php";
}else{
ShaAjaxJquary('/new/breed_engine/post_comment.php?breed=<?=$breed_id?>&user=<?=$userid?>&cmnt='+cmnt+'&cmnt_tit='+cmnt_tit+'', '#show_cmnt', '', '', 'GET', '#show_cmnt', 'Loading...', 'REP');
$("#tit").val("");
$("#cmnt").val("");
}
}
}
function delcmnt(cmnt_id){
ShaAjaxJquary('/new/breed_engine/post_comment.php?delete=1&breed=<?=$breed_id?>&cmnt_id='+cmnt_id+'', '#show_cmnt', '', '', 'GET', '#show_cmnt', 'Loading...', 'REP');	
}
</script>
         <style>
ul.rating li.be_one a:hover{ background-position:0 -23px !important;}
ul.rating li.be_two a:hover{ background-position:0 -46px !important;}
ul.rating li.be_three a:hover{ background-position:0 -69px !important;}
ul.rating li.be_four a:hover{ background-position:0 -92px !important;}
ul.rating li.be_five a:hover{ background-position:0 -115px !important;}

		</style> 
<?php
require_once($DOCUMENT_ROOT . '/new/common/header.php');
?>
        	<div class="breadcrumb">
            	<div class="header cont980">
                
                    <div class="fl" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#666;"> 
                    <a href="/">Home</a> » <a href="/dog-breeds/">Dog Breeds</a>  » <a href="/dog-breeds/"><?=$be_name; ?></a> » 
					<?=$be_name; ?> Review
                     </div>
   
                    </div>              
                     
                     <div class="cb"></div>
                 </div>
<? 
		//	echo "SELECT * FROM breed_rating WHERE brd_id='$breed_id' AND userid='$userid'";
        $qRATING1 = query_execute_row("SELECT * FROM breed_rating WHERE brd_id='$breed_id' AND userid='$userid'");
		$qRATING_count = mysql_query("SELECT * FROM breed_rating WHERE brd_id='$breed_id' AND userid='$userid'");
        $num_votes1=mysql_num_rows($qRATING_count);
		//echo "votes".$num_votes1;
		if($num_votes1==''){
			$num_votes1='0';
		}
		//echo "votes1".$num_votes1;
        $qbest1 = '5';
        if($num_votes1!='0') {
			$nRating1=$qRATING1['rating'];
       // $nRating1=$qRATING1['rating']/$num_votes1;
        if($nRating1 >= 0 && $nRating1 < 0.5){ $finalratng1='0'; $class_rat1="nostar";}
        if($nRating1 >= 0.5 && $nRating1 < 1.5){ $finalratng='1'; $class_rat1="onestar"; $rat_css_cmnt_more1="be_one";} 
        if($nRating1 >= 1.5 && $nRating1 < 2.5){ $finalratng='2'; $class_rat1="twostar"; $rat_css_cmnt_more1="be_two";}
        if($nRating1 >= 2.5 && $nRating1 < 3.5){ $finalratng='3'; $class_rat1="threestar"; $rat_css_cmnt_more1="be_three";}
        if($nRating1 >= 3.5 && $nRating1 < 4.5){ $finalratng='4'; $class_rat1="fourstar"; $rat_css_cmnt_more1="be_four";}
        if($nRating1 >= 4.5 && $nRating1 <= 5){ $finalratng='5'; $class_rat1="fivestar"; $rat_css_cmnt_more1="be_five";}
        }
        ?>    
<div class="cont980">
<div class="be_rvwBox" style="margin-top:10px;">
<div class="be_commentBox">
<div class="be_commentList">
<div class="be_cmntBreedimg">
<? if($img){
		$src_cmnt = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$img;
		$imageURL='/new/breed_engine/images/dog_images/100-100-'.$img;
	}else{
		$src_cmnt = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest_cmnt = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src_cmnt,$dest_cmnt,'100','100','ratiowh');
?>
<img src="/new/breed_engine/images/dog_images/100-100-<?=$img; ?>" width="100" height="100" alt="<?=$breedname?>" title="<?=$breedname?>">
</div>
<div class="be_cmntBreedimg">Review <?=$breedname ?></div>
</div>
<div class="be_commentList be_cmntNote">
Please add a rating and a written review. You will need to be logged in to submit.
</div>
<div class="be_commentList">
<div class="be_cmntLabel"> Your Rating</div>
<div class="be_cmntInput">                         
<div id="rateIt1" >

  <? if($num_votes1=='0' || $userid=='Guest'){ ?>
    <ul class="rating <?=$class_rat1 ?>">
	<li class="one" style="cursor:pointer"><a onclick="Javascript:ShaAjaxJquary('/new/breed_engine/breed_rating.php?rating=1&breed=<?=$breed_id?>', '#rateIt1', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="1 Star">1</a></li>
	<li class="two" style="cursor:pointer"><a onclick="Javascript:ShaAjaxJquary('/new/breed_engine/breed_rating.php?rating=2&breed=<?=$breed_id?>', '#rateIt1', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="2 Stars">2</a></li>
	<li class="three" style="cursor:pointer"><a onclick="Javascript:ShaAjaxJquary('/new/breed_engine/breed_rating.php?rating=3&breed=<?=$breed_id?>', '#rateIt1', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="3 Stars">3</a></li>
	<li class="four" style="cursor:pointer"><a onclick="Javascript:ShaAjaxJquary('/new/breed_engine/breed_rating.php?rating=4&breed=<?=$breed_id?>', '#rateIt1', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="4 Stars">4</a></li>
	<li class="five" style="cursor:pointer"><a onclick="Javascript:ShaAjaxJquary('/new/breed_engine/breed_rating.php?rating=5&breed=<?=$breed_id?>', '#rateIt1', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="5 Stars">5</a></li>
</ul>
<? }else{ ?>
    <ul class="rating <?=$class_rat1 ?>">
	<li class="one <?=$rat_css_cmnt_more1 ?>" style="cursor:pointer"><a title="1 Star">1</a></li>
	<li class="two <?=$rat_css_cmnt_more1 ?>" style="cursor:pointer"><a title="2 Stars">2</a></li>
	<li class="three <?=$rat_css_cmnt_more1 ?>" style="cursor:pointer"><a title="3 Stars">3</a></li>
	<li class="four <?=$rat_css_cmnt_more1 ?>" style="cursor:pointer"><a title="4 Stars">4</a></li>
	<li class="five <?=$rat_css_cmnt_more1 ?>" style="cursor:pointer"><a title="5 Stars">5</a></li>
</ul>
<? } ?>
</div>
<div id="rateLoad"></div>
</div>

<div class="be_commentList">
<div class="be_cmntLabel"> Title</div>
<div class="be_cmntInput"><input type="text" id="tit" name="tit" /></div>
</div>
<div class="be_commentList">
<div class="be_cmntLabel">Review</div>
<div class="be_cmntInput"><textarea name="cmnt" id="cmnt"></textarea></div>
</div>
<div class="be_commentList">
<div class="be_cmntLabel">&nbsp;</div>
<div class="be_cmntInput" style="cursor:pointer"><input  style="cursor:pointer" type="button" id="post_cmnt" name="post_cmnt" value="Post Review" onclick="postcomment();" /></div>
</div>
</div>
</div>
<div id="post_cmnt_div"></div>
<div class="be_commentList" id="show_cmnt">
<?
$sel_cmnts=mysql_query("SELECT * FROM breed_comments WHERE breed_id='$breed_id'");
while($sel_cmnts1=mysql_fetch_array($sel_cmnts)){
$cmnt_data=$sel_cmnts1['comment'];
$cmnt_user=$sel_cmnts1['user_id'];
$cmnt_tit=$sel_cmnts1['comment_title'];
$cmnt_date=$sel_cmnts1['c_date'];
$cmnt_id=$sel_cmnts1['cmnt_id'];
?>

<div class="be_postCmnt">
<div class="be_cmntUser">
<?
$sel_usr_img= query_execute_row("SELECT image,f_name FROM users WHERE userid = '$cmnt_user'");
if($sel_usr_img['image']){
		$src_usr_img = $DOCUMENT_ROOT.'/profile/images/'.$sel_usr_img['image'];
		$imageURL='/profile/images/50-50-'.$sel_usr_img['image'];
	}else{
		$src_usr_img = $DOCUMENT_ROOT.'/imgthumb/100x100-no-photo.jpg';
		$imageURL='/imgthumb/50-50-no-photo.jpg';
	}
	$dest_usr_img = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src_usr_img,$dest_usr_img,'50','50','ratiowh');
?>
<div class="be_cmntImg"><img src="<?=$imageURL; ?>" width="50" height="50" alt="<?=$sel_usr_img['f_name']?>" title="<?=$sel_usr_img['f_name']?>"/> </div>
<div class="be_cmntName">
<label><?=dispUname($cmnt_user) ?></label>
<?
$timediff=dateDiff(date("Y-m-d H:i:s"), $cmnt_date);
if($timediff=='1'){ ?>
<p>1 second ago</p>
<? }else{ ?>
<p><?=$timediff; ?> ago</p>
<? } ?>
</div>
</div>
<div class="be_cmntDone">
<div class="be_cmntInput">
		<? 
		//	echo "SELECT * FROM breed_rating WHERE brd_id='$breed_id' AND userid='$userid'";
        $qRATING2 = query_execute_row("SELECT * FROM breed_rating WHERE brd_id='$breed_id' AND userid='$cmnt_user'");
		$qRATING_count = mysql_query("SELECT * FROM breed_rating WHERE brd_id='$breed_id' AND userid='$cmnt_user'");
        $num_votes2=mysql_num_rows($qRATING_count);
		//echo "votes".$num_votes1;
		if($num_votes2==''){
			$num_votes2='0';
		}
		//echo "votes1".$num_votes1;
        $qbest2 = '5';
        if($num_votes2!='0') {
			$nRating2=$qRATING2['rating'];
       // $nRating1=$qRATING1['rating']/$num_votes1;
        if($nRating2 >= 0 && $nRating2 < 0.5){ $finalratng2='0'; $class_rat2="nostar"; }
        if($nRating2 >= 0.5 && $nRating2 < 1.5){ $finalratng2='1'; $class_rat2="onestar"; $rat_css_cmnt_more="be_one";}
        if($nRating2 >= 1.5 && $nRating2 < 2.5){ $finalratng2='2'; $class_rat2="twostar"; $rat_css_cmnt_more="be_two";}
        if($nRating2 >= 2.5 && $nRating2 < 3.5){ $finalratng2='3'; $class_rat2="threestar"; $rat_css_cmnt_more="be_three";}
        if($nRating2 >= 3.5 && $nRating2 < 4.5){ $finalratng2='4'; $class_rat2="fourstar"; $rat_css_cmnt_more="be_four";}
        if($nRating2 >= 4.5 && $nRating2 <= 5){ $finalratng2='5'; $class_rat2="fivestar"; $rat_css_cmnt_more="be_five";}
        }
        ?>                           
<div id="rateIt1" >
    <ul class="rating <?=$class_rat2?>">
	<li class="one <?=$rat_css_cmnt_more;?>" style="cursor:pointer"><a title="1 Star">1</a></li>
	<li class="two <?=$rat_css_cmnt_more;?>" style="cursor:pointer"><a title="2 Stars">2</a></li>
	<li class="three <?=$rat_css_cmnt_more;?>" style="cursor:pointer"><a title="3 Stars">3</a></li>
	<li class="four <?=$rat_css_cmnt_more;?>" style="cursor:pointer"><a title="4 Stars">4</a></li>
	<li class="five <?=$rat_css_cmnt_more;?>" style="cursor:pointer"><a title="5 Stars">5</a></li>
</ul>
</div>
<div id="rateLoad"></div>

<div class="be_cmntInput">
<h3><?=$cmnt_tit;?></h3><? if($userid==$cmnt_user){
?>
<a onclick="delcmnt('<?=$cmnt_id;?>')" class="be_delCmnt">X</a>
<?
}?>
</div>
<div class="be_cmntInput">
<p><?=$cmnt_data; ?></p>
</div>
</div>
</div>
</div>

<? } ?>
</div>
</div>
</div>
<input type="hidden" name="user" id="user" value="<?=$userid;?>" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom.php');
?>