<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/be_style.css" />
<title>Breed Engine</title>

<?php require_once($DOCUMENT_ROOT.'/new/common/header_new.php');?>
<div class="cont980">
<div class="be_breedBox">
<p style="color:#000"> <img src="images/save.png" width="40" height="38" alt="Save" title="Save" />
<label><span style="color:#668000">Add</span> another Dog Breed to your comparison:</label>
<input name="" type="text" />
</p>
<div class="be_back">
<div>See all dog breeds</div>
<div class="be_arw"><img src="images/arw_blck.png" width="15" height="10" alt="" /></div>
<div><a><img src="images/click.png" width="55" height="55" alt="" /></a></div>
</div>
<div class="be_breedDetail">
<div class="be_leftDetail">
<div class="be_search">
<div class="be_heding">
<h3>Compare</h3>
</div>
<div class="be_coatBox be_comBox" style="border:0px;">
<h3>Add Related Dog</h3>
<ul>
<li><input name="" type="checkbox" value=""><span>German Shepherd </span></li>
<li><input name="" type="checkbox" value=""><span>Labrador Retriever</span></li>
<li><input name="" type="checkbox" value=""><span>Beagle</span></li>
</ul>
</div>

</div>
</div>


<div class="be_rytDetail">
<ul>
<li class="be_com50">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
 <li class="be_com50">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
 <li class="be_com30">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
 <li class="be_com30">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
  <li class="be_com30">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
 
   <li class="be_com25">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
    <li class="be_com25">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
    <li class="be_com25">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
    <li class="be_com25">
 <div class="be_comImg"> <img src="images/thumb_img.jpg" width="229" height="156" alt="" /></div>
  <div class="be_comTxt"><a href="#">Boxer</a></div>
 </li>
 
 
</ul>
</div>
</div>

</div>
<div class="be_detailBox">
<div class="be_comDetails">
<h3><img src="images/plus.gif" width="14" height="14" alt="Plus" title="Plus" />
  <img src="images/minus.gif" width="14" height="14" alt="Minus" title="Minus" />Breed Info</h3>
<ul>
<li class="be_com30"><strong>Dog Breed</strong></li>
<li class="be_com30">Golden Retriever</li>
<li class="be_com30">Labrador Retriever</li>
</ul>

<ul>
<li class="be_com25"><strong>Dog Breed</strong></li>
<li class="be_com25">Golden Retriever</li>
<li class="be_com25">Labrador Retriever</li>
<li class="be_com25">Labrador Retriever</li>
</ul>

<ul>
<li class="be_com20"><strong>Dog Breed</strong></li>
<li class="be_com20">Golden Retriever</li>
<li class="be_com20">Labrador Retriever</li>
<li class="be_com20">Labrador Retriever</li>
<li class="be_com20">Labrador Retriever</li>
</ul>
</div>
</div>
</div>
</div>