<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
?>
<div class="breadcrumbs">
            	 <div class="container">
                 <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
                
                    <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a href="/dog-breeds/" itemprop="item">
                    <span itemprop="name">Dog Breeds</span></a>
                    <meta itemprop="position" content="1" /> </span>
                    <span> / </span>
                     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a href="/dog-breeds-comparison/" itemprop="item">
                    <span itemprop="name"  class="active-bread">Comparison</span></a>
                    <meta itemprop="position" content="2" /> </span> <span> / </span>

   
<input type="hidden" id="p_breed" name="p_breed" value="<?=$nicenames ?>" />

                     </div>
                 
                    
                    <?php /*?><div class="fr" style="width:80px; margin-top:-22px;">
                  	<script type="text/javascript" src="https://w.sharethis.com/widget/?tabs=web%2Cpost%2Cemail&amp;charset=utf-8&amp;style=default&amp;publisher=203f1564-d8e9-4d1a-a18a-e0a64020e484"></script>
                    </div><?php */?>
                    </div>
                     </div>
                 </div>
<section class="breeds-section more-breed-com breed-com">
<div class="container" id="ajaxbreed">
<h1 class="text-center"> You have not selected a breed to compare</h1>
<div class="col-md-12 text-center">

<p style="color:#000"> 
<div><i class="fa fa-paw"></i><span style="color:#668000">Add</span> another Dog Breed to your comparison:

</p>
<p>
<select id="breed_id_add" style="    width: 50%;
    height: 35px;
    padding: 0px 10px;" name="breed_id_add" onchange="addbreedchange(this.value);">
      <option value='0'>Select Breed</option>
        <?php
			$query_domain = query_execute("SELECT * FROM dog_breeds WHERE breed_engine='1'");
		
			while($query_domain1 = mysql_fetch_array($query_domain)){
			$breed_id12 = $query_domain1["breed_id"];
			$breed_name = $query_domain1["breed_name"];
			$be_name = $query_domain1["be_name"];
			$nicename=$query_domain1['nicename'];
			  print "<option value='$breed_id12|$nicename'";
			  if($breed_id12 == $breed_id){   echo "selected=='selected'";  }  print ">$be_name</option>";
			 }
		  ?>
    </select>
</p>

    <div id="ajaxbreed_load"></div>
</div>

</div>
 
<input type="hidden" id="prv_breed" name="prv_breed" value="<?=$breeds ?>" />
</div>
</div>
</section>
