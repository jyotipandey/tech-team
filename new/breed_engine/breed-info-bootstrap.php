<?php
require_once($DOCUMENT_ROOT . '/constants.php');
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/arrays.php');

include($DOCUMENT_ROOT."/dogs/arraybreed.php");
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
?>
<?php
$breedid         = $brd;
$breednameselect = query_execute_row("SELECT breed_name,nicename,breed_life,monthly_keeping_cost_premium,monthly_keeping_cost_standard,icon,image_name,height,weight,img_link,img_dog_name,breed_engine,be_name FROM dog_breeds WHERE breed_id='$breed_id'");
$breedname       = $breednameselect['breed_name'];
$breednice       = $breednameselect['nicename'];
$icon            = $breednameselect['icon'];
$img             = $breednameselect['image_name'];
$ht              = $breednameselect['height'];
$wt              = $breednameselect['weight'];
$imglink         = $breednameselect['img_link'];
$img_dog_name    = $breednameselect['img_dog_name'];
$breed_engine    = $breednameselect['breed_engine'];


$ant_category = $be_name         = $breednameselect['be_name'];
$lifestage         = $breednameselect['breed_life'];
$costpr          = $breednameselect['monthly_keeping_cost_premium'];
$costst          = $breednameselect['monthly_keeping_cost_standard'];
$selorigin       = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='39'");
$selorigin1      = $selorigin['value'];
$selsize2       = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='41'");
$selsize    	 = $selsize2['value'];
if($selsize=='193'){
	$selsize1='2';
}else if($selsize=='194'){
	$selsize1='3';
}else if($selsize=='195'){
	$selsize1='5';
}else{
	$selsize1='6';
}
$selorigin1name  = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_id='39' AND att_att_id='$selorigin1'");
$selgroup        = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='35'");
$selgroup1       = $selgroup['value'];
$selgroup1name   = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_id='35' AND att_att_id='$selgroup1' ");
$seltag          = query_execute_row("SELECT data FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id='1'");
$selogn          = query_execute_row("SELECT data FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id='4'");
$selrelated      = mysql_query("SELECT bev.breed_id FROM breed_engine_values as bev , dog_breeds as db WHERE bev.breed_id=db.breed_id AND bev.att_id='35' AND bev.value='$selgroup1' AND bev.breed_id !='$breed_id' AND db.breed_engine='1' LIMIT 3");
$countrelated=mysql_num_rows($selrelated);
$sel_odr_grp_name=mysql_query("SELECT * FROM breed_engine_att_att WHERE att_id='35'");
while($selrelated1=mysql_fetch_array($selrelated)){
$relatedarray[]=$selrelated1['breed_id'];
}
$selecttitle=query_execute_row("SELECT * FROM breed_engine_breed_titles WHERE breed_id='$breed_id'");

$new_ht= round($new_ht=(266/72)*$ht);
 $title=$selecttitle['title'];
	$keyword=$selecttitle['keyword'];
    $desc=$selecttitle['description'];
	$alternate="https://m.dogspot.in/$section[0]/";
	$canonical="https://www.dogspot.in/$section[0]/";
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in/new/breed_engine/images/dog_images/$img";
	$page_type='Dog Breeds';
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php'); ?>

<link type="text/css"  rel="stylesheet" href="/bootstrap/css/dog-breeds.css?v=12" />
<div class="breadcrumbs"> 
    <div class="container"> 
      <div class="row" itemscope itemtype="http://schema.org/Breadcrumb"> 
        <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
         <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> 
      <a href="/" itemprop="item"><span itemprop="name">Home</span></a> 
       <meta itemprop="position" content="1" /> </span> 
     <span> / </span> 
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">  
     <span itemprop="item"> 
     <span itemprop="name" class="active-bread"><?=$breedname?></span></span> 
      <meta itemprop="position" content="2" /> </span> 
        </div> 
      </div> 
    </div> 
  </div>

<section class="breeds-section">
<div class="container">
    <div class="row" itemscope itemtype="https://www.schema.org/Product">
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <div class="breeds-box" style="height: auto;"> <img itemprop="image" src="https://ik.imagekit.io/2345/new/breed_engine/images/dog_images/<?= $img ?>" alt="<?=$be_name?>" title="<?=$be_name; ?>" />
          <div class="breeds-box-pc">
          <? if($img_dog_name!=''){ ?>

<h4>Picture Courtesy:</h4>
  <label>
 <? if($section[0]!='indian-pariah-dog'){?> <a href="<?=$imglink?>" target="_blank" ><? }?>
<?=$img_dog_name; ?> <? if($section[0]!='indian-pariah-dog'){?></a><? }?>
<? if($section[0]=='indian-pariah-dog'){?>(http://indog.co.in/)<? }?>
  </label>
 
<? } ?>
            
            
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="breed-info" itemprop="description">
          <h1 itemprop="name"><?= $be_name ?></h1>
          <h4 class="be_tag"><?= stripcslashes(htmlspecialchars_decode($seltag['data'])); ?></h4>
         <p>
<label><strong>Origin:</strong> &nbsp;<?= stripcslashes(htmlspecialchars_decode($selorigin1name['value'])) ?></label>
</p>
<p>
<label><strong>Group:</strong> &nbsp;<a href="/<?=strtolower($selgroup1name['value'])?>-group-dog-breeds/" target="_blank" ><?= stripcslashes(htmlspecialchars_decode($selgroup1name['value'])) ?></a></label>
</p>

<p style="display:inline;"> <label><strong>Origin of Name:</strong> <?= htmlspecialchars_decode(stripallslashes($selogn['data'])); ?>  </label> </p>

        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <div class="breed-info-box">
          <div class="breed-info-box-height">
            <div class="breed-info-box-icon">
            <? if($breed_id=='636'){ ?><p class="das_wc_height">Standard : <? }else{ ?><p><? } ?><?=$ht ?> Inches<label style="cursor:pointer; float:none; margin:0;" id="This is an average height till head" title="This is an average height till head">*</label></p>
 <?
	if($icon){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/icons/'.$icon;
		$imageURL='/new/breed_engine/images/icons/new-ht-'.$icon;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
	//	createImgThumbIfnot($src,$dest,$new_ht,$new_ht,'ratiohh');
		//$compressed = compress_image($dest,$dest, 60);
?>
<img src="<?=$siteURL?>/new/breed_engine/images/icons/new-ht-<?=$icon?>" alt="<?=$breedname?>" title="<?=$breedname?>">
</div>
          </div>
          <div class="breed-info-weight-box">
          <? if($breed_id=='636'){ ?>
 <div class="breed-info-box-weight">
 <p>Miniature : 6 Inches<label style="cursor:pointer" id="This an average height for the male dog" title="This an average height for the male dog">*</label></p>
</div>
<? } ?>
  
<? if($breed_id!='636'){ ?>
<div class="breed-info-box-weight">
<p>In Kg<label style="cursor:pointer" id="Average for the breed" title="Average for the breed" >*</label><br>
<?=$wt ?></p></div>
<? } if($breed_id=='636'){ ?>
 <div class="breed-info-box-weight">
Standard
<p>In Kg<label style="cursor:pointer" id="Average for the breed" title="Average for the breed" >*</label><br>
<?=$wt ?></p></div>
 <div class="breed-info-box-weight">
Miniature
<p>In Kg<label style="cursor:pointer" id="Average for the breed" title="Average for the breed" >*</label><br>
upto 4 Kg</p></div>
<? } ?>

          </div>
        </div>
      </div>
    </div>
  </div>
   <div class="container">
    <div class="row">
      <div class="col-md-12 breed-basic-info">
        <table class="table">
          <tr>
            <th> Size </th>
            <th> Efforts </th>
            <th> Shedding </th>
            <th> Monthly keeping cost </th>
          </tr>
          <? 
$tablearry=array("41","30");
foreach($tablearry as $table_att){
$tabledata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$table_att'");
while($tabledata1=mysql_fetch_array($tabledata)){
	$tablevalue=$tabledata1['value'];
	//echo "SELECT value FROM breed_engine_att_att WHERE att_att_id='$tablevalue'"."<br>";
	$tablevaluename=query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$tablevalue'");
if($table_att!='30'){
?>
          <tr>
            <td><?=stripcslashes(htmlspecialchars_decode($tablevaluename['value'])) ?> </td>
            <? }else{?>
            <td> <?=stripcslashes(htmlspecialchars_decode($tablevaluename['value'])) ?> </td>
            <? }}} 
$minmax=query_execute_row("SELECT min_value,max_value FROM breed_engine_traits WHERE id='15'");
$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='15'");
$nRating=(($qRATING['value1']/1000)*100);
            if ($nRating <= 0 || $nRating > 5) {
                $finalratng = '0';
                $ratvalue   = "rate1";
            }
            if ($nRating > 0 && $nRating <= 1) {
                $finalratng = '1';
                $ratvalue   = "rate1";
            }
            if ($nRating > 1 && $nRating <= 2) {
                $finalratng = '2';
                $ratvalue   = "rate2";
            }
            if ($nRating > 2 && $nRating <= 3) {
                $finalratng = '3';
                $ratvalue   = "rate3";
            }
            if ($nRating > 3 && $nRating <= 4) {
                $finalratng = '4';
                $ratvalue   = "rate4";
            }
            if ($nRating > 4 && $nRating <= 5) {
                $finalratng = '5';
                $ratvalue   = "rate5";
            }
?>
            <td>
             
            <div class="meter orange nostripes"> <span style="width:<?=$nRating?>%"></span> </div>
              <span class="meter-span"><?=$minmax['min_value']; ?></span> <span class="meter-span"><?=$minmax['max_value']; ?></span></td>
            <td><table class="table">
                <tr>
                  <td><strong>Premium</strong> <a href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Average for the breed"><i class="fa fa-info-circle"></i></a></td>
                  <td><strong>Standard </strong> <a href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Average for the breed"><i class="fa fa-info-circle"></i></a></td>
                </tr>
                <tr>
                  <td><i class="fa fa-rupee"></i> <?= number_format($costpr); ?></td>
                  <td><i class="fa fa-rupee"></i> <?= number_format($costpr); ?></td>
                </tr>
              </table></td>
          </tr>
    
        </table>
      </div>
    </div>
  </div>
   <?
$selectdata = mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' ORDER BY att_id DESC");
while ($selectdata1 = mysql_fetch_array($selectdata)) {
    $att_id1[] = $selectdata1['att_id'];
    $att_value = $selectdata1['value'];
}
?>
  <div class="container">
    <div class="row">
      <div class="col-md-9"> 
        <!-- nav-->
        <div class="breed-page-nav">
          <nav class="navbar">
            <ul class="nav navbar-nav">
              <li class="current" onclick="moveToFooter('tabs-1')" style="cursor:pointer"><a>Breed Info</a></li>
              <li onclick="moveToFooter('tabs-8')" style="cursor:pointer"><a>Maintenance &amp; Effort</a></li>
              <li onclick="moveToFooter('tabs-9')" style="cursor:pointer"><a>Hair &amp; Coat</a></li>
              <li onclick="moveToFooter('tabs-10')" style="cursor:pointer"><a>Health</a></li>
              <li onclick="moveToFooter('tabs-11')" style="cursor:pointer"><a>Behavior </a></li>
              <li onclick="moveToFooter('tabs-6')" style="cursor:pointer"><a>Breeding </a></li>
              <li onclick="moveToFooter('tabs-7')" style="cursor:pointer"><a>Appearance </a></li>
            </ul>
          </nav>
        </div>
        <!-- nav end-->
        
        <div id="tabs-1" class="nav_content" name="tabs-1">
         <h2 class="headingBreed_be"><?=$be_name;?> Dog Breed Information</h2>
  
  <div class="breed-cont-info">
   
   
<div>
    <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '37' || $att_id == '3' || $att_id == '14' || $att_id == '27') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
                    <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1']/1000)*100);
         ?> 

<ul>
  <li><!--<span><img src="images/detail_icon8.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
  </li>
  </ul>

<?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
 <ul>
  <li><!--<span><img src="images/detail_icon8.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?><? if($att_id=='27'){ echo " <a href='#' data-toggle='tooltip' data-placement='bottom' title='' data-original-title='Average for the breed'><i class='fa fa-info-circle'></i></a>";} ?><? if($att_id=='37'){ echo " <a href='#' data-toggle='tooltip' data-placement='bottom' title='' data-original-title='Average for the breed'><i class='fa fa-info-circle'></i></a>";} ?></label></li>
  <li class="no_border">
  <?= $valuename['value']; 
  if($att_id=='27'){ ?>
  <? if($valuename['value']=='Economical'){ $tit_id='(upto Rs 5,000)';} else if($valuename['value']=='Pocket Friendly'){ $tit_id='(Rs 10,000 - Rs 20,000)';} else if
($valuename['value']=='Expensive'){$tit_id='(Rs 25,000 - Rs 30,000)';} else{$tit_id='(Rs 35,000 - Rs 50,000)';}?><a href="#" data-toggle="tooltip"  id="<?=$tit_id?> approximate cost,which can very according to the puppy and the location." data-placement="bottom" title="" data-original-title="<?=$tit_id?> approximate cost,which can very according to the puppy and the location."><i class="fa fa-info-circle"></i></a>
 <? }
 ?> </li>
  </ul>
  <?
        }
    }
}
?>
</div>
</div>

<?

$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (2,3,5,7,15,16)  ORDER BY  title_id ASC");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
    
    if ($articletitleid != '15' && $articletitleid != '16' && $articletitleid != '7' ) {
?>
<strong><?= stripcslashes(htmlspecialchars_decode($selecttitlename['title'])); ?></strong>
<?=htmlspecialchars_decode(stripallslashes($articlecontent)); ?>

<?
    }
?>

<?
    if ($articletitleid == '15' || $articletitleid == '16'){
?>
<?
        if ($articletitleid == '15') {
?>
<div class="be_pros" style=" border-right:0px">
<strong>Pros</strong>

<?= $articlecontent = preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', stripallslashes(htmlspecialchars_decode($articlecontent))); ?>

</div>
<?
        } else {
?>
<div class="be_pros" >
<strong>Cons</strong>
<?= $articlecontent = preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', stripallslashes(htmlspecialchars_decode($articlecontent))); ?>
</div>
<?
        }
?>
<?
    }
	if($articletitleid=='7'){ ?>
    <div class="be_detailTxt be_funBox">
 <div class="be_funImg"><img src="<?=$siteURL?>/new/breed_engine/images/mask1.png" width="66" height="66" alt="" /></div>
<?php /*?><h2>  Fun Trivia</h2>
<?php */?><h3><?=$selecttitlename['title']; ?></h3>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
</div>
	
	<? }
}
?>
 </div>   
  
  <div class="be-share-box">
                          <?  
                        $surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                       include($DOCUMENT_ROOT."/new/articles/social-bootstrap.php"); 
                       
                     ?>
                        </div>
  <?
  if($breednice=='indian-pariah-dog'  || $breednice=='jack-russell-terrier'){
	  //------------------------------------ 
	$selalb2=mysql_query("SELECT * FROM dogs_available WHERE breed_nicename='$breednice' AND publish_status='publish' AND dog_image !='' ORDER BY cdate DESC LIMIT 8");  
	  ?>
<div class="be_slideBox">
<div class="be_funImg" style="margin: -5px 10px 0 16px"><img src="<?=$siteURL?>/new/breed_engine/images/gall.png" width="68" height="57" alt="" /></div>
<h3><?=$be_name ?> Photos from our collection</h3>
  <ul>
 <? 
	while ($row = mysql_fetch_array($selalb2)) {
	$dog_id  = $row["dog_id"];
	$dog_name  = ucfirst(strtolower($row["dog_name"]));
	$dog_image = $row["dog_image"];
	$dog_nicename = $row["dog_nicename"];
	$breed_nicename = $row["breed_nicename"];
	$dog_name = stripslashes($dog_name);
 
	$img_src = $DOCUMENT_ROOT."/dogs/images/thumb_$dog_image";

	
	if($dog_image){
		$src = $DOCUMENT_ROOT.'/dogs/images/'.$dog_image;
		$imageURL='/dogs/images/150-150-'.$dog_image;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	if(file_exists($src)){
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'150','150','ratiowh');
$compressed = compress_image($dest,$dest, 60);
 ?>
  <li><a href="/dogs/<?=$dog_nicename?>/" target="_blank" ><img src="/dogs/images/150-150-<?=$dog_image?>" alt="<?=$dog_image?>" title="<?=$dog_name?>" border="0"  width="150px;"/></a>
</li>
<? } }?>
  </ul>
  </div>
	<?  //------------------------
  }else{
  $v=0;$cc=0;
 if($breed_id=='636'){
	 $query="dachshund";
 }
 else if($breed_id=='494'){
	 $query="chihuahua";
 }
 else if($breed_id=='58'){
	 $query="fox terrier weired";
 }
  else if($breed_id=='627'){
	 $query="cane";
 }  else if($breed_id=='60'){
	 $query="German Shepherd";
 }  else if($breed_id=='96'){
	 $query="Neapolitan Mastiff";
  } else if($breed_id=='104'){
	 $query="Pit Bull";
 }else if($breed_id=='93'){
	 $query="English Mastiff";
 }
 else if($breed_id=='26'){
	 $query='-"French%20Bull%20Dog"'.'"bulldog"';
 }else if($breed_id=='68'){
	 $query="spitz";
 }
 else{
  	$query=trim($breedname);
 }
	$query = stripslashes(str_replace(" ","%20AND%20",$query));//&sort=no_views desc
$url = "http://localhost/solr/dogspotphotos/select/?q=$query&wt=xml&mm=0&indent=true&defType=dismax&qf=title%5E2+text%5E1&sort=cdate+desc&start=1&rows=300";
	$result = get_solr_result($url);
	$totrecord = $result['TOTALHITS'];
   if($totrecord > 0){?>
  <div class="be_slideBox">
  <div class="be_funImg" style="margin: -5px 10px 0 16px"><img src="<?=$siteURL?>/new/breed_engine/images/gall.png" width="68" height="57" alt="" /></div>
<h3><?=$be_name ?> Photos from our collection</h3>
  <ul>
  
  <? 
	$check_ex=0;
	$count_images='0';
	$check_random_img='0';
	$img_dis='0';
	// check for exhibition id n show id images----------------------------------------------------------------
	foreach($result['HITS'] as $row){
	$selalb=mysql_query("SELECT * FROM photos_album WHERE album_link_name='event' AND album_id='$row[album_id]'");
	$count_alb=mysql_num_rows($selalb);
	if($count_alb > 0){
	$album_id[] = $row["album_id"];
	$selimg=query_execute_row("SELECT image_desc FROM photos_image WHERE image_id='$row[image_id]'");
		if((strpos($selimg['image_desc'], "ex-")!== false) && (strpos($selimg['image_desc'], "Lineup")== '0') && (strpos($selimg['image_desc'], "BIS")== '0') && (strpos($selimg['image_desc'], "BOB")== '0')){
		$Aimage[]=$row["image_id"];
		$arr[]=$row["album_id"]."@@".$row["image_id"]."@@".$row["image_nicename"]."@@".$row["image"];	
		$check_ex=1;$count_images++;
		}

	}
	}
	// if exhibition id n show id images are not found----------------------------------------------------------------
	if($check_ex=='0' || $count_images < 20){	
	foreach($result['HITS'] as $row){
	$selalb=mysql_query("SELECT * FROM photos_album WHERE album_link_name='event' AND album_id='$row[album_id]'");
	$count_alb=mysql_num_rows($selalb);
	if($count_alb > 0){
	$count_images++;
	$album_id[] = $row["album_id"];
	$Aimage[]=$row["image_id"];
	$arr[]=$row["album_id"]."@@".$row["image_id"]."@@".$row["image_nicename"]."@@".$row["image"];	
	}	
	}
	}


	$Aalbum_id=array_unique($album_id);
	asort($Aalbum_id);
	$check_album_count=count($Aalbum_id);
	if($check_album_count<4){
		if($breednice=='rampur-hound'){
		$num_images='4';
		}elseif($breednice=='dogo-argentino')
		{
			$num_images='4';
		}else{
		$num_images='8';
		}
	}elseif($check_album_count==5 && $breednice=='rajapalayam'){
		if($breednice=='rajapalayam'){
		$num_images='4';
		}
	}else{
		$num_images='2';
	}
	//print_r($arr);
	if($count_images < 20){
	$check_random_img_var='1';
	}else{
	$check_random_img_var='2';
	}
	$arr=array_unique($arr);
	foreach($Aalbum_id as $rows){
	foreach($arr as $row1){
			$row11=explode("@@",$row1);
			if($rows==$row11[0]){	
		$check_random_img++;
	//$albumname = query_execute_row("SELECT image_id,image, image_nicename FROM photos_image WHERE album_id = '$rows' AND image_desc like '%sw%'");		
	 
	 $image_id = $row11[1];
	 $image = $row11[3];
	 $image_nicename = $row11[2];
	 $img_name = $DOCUMENT_ROOT."/photos/images/thumb_$image";
     $cc++;
	 if($check_random_img==$check_random_img_var){
	 $check_random_img='0';
	 $v++;$cc++;	
	 $img_dis++; 
	if(file_exists($img_name)){
		
	$imgWH = @WidthHeightImg($img_name,'150','');
	//$src = $DOCUMENT_ROOT."/photos/images/$image";
	//$destm = $DOCUMENT_ROOT.'/event_images/thumb_'.$image; 
	//createImgThumbIfnot($src,$destm,'150','','ratiowh');
	}else{
		$imgWH[0] = 150;
		$imgWH[1] = 86;
	}
	if(($breednice=='rampur-hound' || $breednice=='mudhol-hound' || $breednice=='weimaraner' || $breednice=='chippiparai' || 
	$breednice=='border-collie' || $breednice=='alaskan-malamute') && $img_dis<='4'){
		if($image!='kanpur-dog-show-2011_156.jpg' && $image !='kanpur-dog-show-2011_155.jpg'){
	 ?>
  <li><a href="/photos/<?=$image_nicename; ?>/" target="_blank" ><img src="<? echo"/photos/images/thumb_$image";?>" title="<?=$image?>" alt="<?=$image?>" border="0"  width="<? echo"$imgWH[0]";?>"/></a>
</li>
  <? 
		}
	}
if($img_dis<='8' && ($breednice!='rampur-hound' && $breednice!='mudhol-hound' && $breednice!='weimaraner' &&  $breednice!='chippiparai' && 
$breednice!='border-collie' && $breednice!='alaskan-malamute')){
 ?>
  <li><a href="/photos/<?=$image_nicename; ?>/" target="_blank" ><img src="<? echo"/photos/images/thumb_$image";?>" title="<?=$image?>" alt="<?=$image?>" border="0"  width="<? echo"$imgWH[0]";?>"/></a>
</li>
  <?  }
			}}if($v==$num_images){
				$v='0';
				break;
			}
}
if($cc=='32'){break;}
}
if($breednice=='border-collie'){
?>	
<li><a href="/photos/bangalore-dog-show_686/" target="_blank" ><img src="/photos/images/thumb_bangalore-dog-show_686.jpg" title="bangalore-dog-show_686" alt="bangalore-dog-show_686" border="0" height="101"  width="150"/></a>
</li>
<li><a href="/photos/bangalore-dog-show_683/" target="_blank" ><img src="/photos/images/thumb_bangalore-dog-show_683.jpg" title="bangalore-dog-show_683" 
alt="bangalore-dog-show_683" border="0" height="101"  width="150"/></a>
</li>
<? }
?>
  </ul>
  </div>
    <? }}?>
  <div id="tabs-8" class="nav_content" name="tabs-2">
  <h3><?=$breedname ?> Maintenance & Effort</h3>
  <div class="breed-cont-info">
   <form name="formcomnt" id="formcomnt">

   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '22' || $att_id == '28' || $att_id == '7'  || $att_id == '34' || $att_id == '16' || $att_id == '17') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
          
?> 

 <ul>
  <li><!--<span><img src="images/detail_icon8.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
 <ul>
   <li><!--<span><img src="images/detail_icon7.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?><? if($att_id=='11'){echo "<a style='cursor:pointer' id='Average for the breed' title='Average for the breed'>*</a>";} ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
   </ul>
<?
        }
    }
}
?>

  </form>
</div>
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id ='9'");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>
<strong><?= $selecttitlename['title']; ?></strong>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>  
</div>
    <div id="tabs-9" class="nav_content" name="tabs-3">
     <h3><?=$breedname ?> Hair & Coat</h3>
     
 
  <div class="breed-cont-info">
     <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '24' || $att_id == '25' || $att_id == '9' || $att_id == '10' || $att_id == '46') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
    <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
            
?> 

 <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
  </li>
  </ul><?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
  <li class="no_border"><?= $valuename['value'] ?></li>
  </ul> <?
        }
    }
}
?>
</div>
 
  </div>
 
  <div id="tabs-10" class="nav_content" name="tabs-4">
  
  <h3><?=$breedname ?> Health & Care</h3>

  <div class="breed-cont-info">
   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '40' || $att_id == '47') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");     
?>
                <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);

?> 

   <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
  <li class="no_border"><?= $valuename['value'] ?></li>
  </ul>
<?
        }
    }
}
?>
</div>
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id ='8'");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>
<strong><?= $selecttitlename['title']; ?></strong>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?></div>
  
<div id="tabs-11" class="nav_content" name="tabs-5">
 <h3><?=$breedname ?> Behavior</h3>
 
  
<div class="breed-cont-info">
   <?
   foreach ($att_id1 as $att_id){
	   if ($att_id == '21' ||  $att_id == '36') {
		   $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
		    $qRATING = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
		   $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>

  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
   }
foreach ($att_id1 as $att_id) {
 if ($att_id == '1' || $att_id == '2'  || $att_id == '4' || $att_id == '5' || $att_id == '18' || $att_id == '33' || $att_id == '19' 
 || $att_id == '12' || $att_id == '13' || $att_id == '43' || $att_id == '44') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
           <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20' || $att_id == '43' || $att_id == '44' || $att_id == '33') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
 
?> 
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?> </label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
    }
}
?>
</div>
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (6,11,10)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>

<strong><?= $selecttitlename['title']; ?></strong>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>
 
  </div>
  <div id="tabs-6" class="nav_content" name="tabs-6">
 
 <!-- <h2>Body Features</h2>-->
  <h3><?=$breedname ?> Breeding</h3>
 <div class="breed-cont-info">
   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '26' || $att_id == '38') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
           <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating =(($qRATING['value1'] / 1000)*100);;
           
?> 

  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?> </label></li>
    <li class="no_border">
   <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
    }
}
?>
</div>
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (12,13)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>

<strong><?= $selecttitlename['title']; ?></strong>
<?=stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>
</div>
 <div id="tabs-7" class="nav_content" name="tabs-7">
 <div class="be_detailTxt">
 <!-- <h2>Body Features</h2>-->
  <h3><?=$breedname ?> Appearance</h3>
  <div class="breed-cont-info">
   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '8' || $att_id == '6' || $att_id == '29' || $att_id == '31' || $att_id == '42' || $att_id == '48' || $att_id == '23') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
           <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
           
?> 

  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?> </label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
    }
}
?>
</div>
   
    
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (14)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>
<strong><?= $selecttitlename['title']; ?></strong>
<?=stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>
</div>
</div>     
</div>

  <aside class="col-md-3">
        <div class="breed_sidebar">
          <h4>Related Breeds</h4>
          <ul>
<? foreach($relatedarray as $relbrd1){ 
$rltdbrdname = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$relbrd1'");
$rltdbreedname1      = $rltdbrdname['be_name'];
$image_name          = $rltdbrdname['image_name'];
$rltdbreednice       = $rltdbrdname['nicename'];
	//if($image_name){
	////	$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$image_name;
	//	$imageURL='/new/breed_engine/images/dog_images/35-33-'.$image_name;
	//}else{
	//	$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
	//	$imageURL='/dogs/images/no-photo-t.jpg';
//	}
	//$dest = $DOCUMENT_ROOT.$imageURL;
	
//		createImgThumbIfnot($src,$dest,'35','33','ratiowh');
//$compressed = compress_image($dest,$dest, 60);
?>
<li><a href="/<?=$rltdbreednice;?>/" target="_blank" ><label><?=$rltdbreedname1;?></label>
<span><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-<?= $image_name ?>" alt="<?=$rltdbreedname1; ?>" title="<?=$rltdbreedname1; ?>" ></span>
</a></li>
<? } ?> 
</ul>
          
        </div>
      
      <div class="breed_sidebar">
      <div class="reco-name"><a href="/dog-names/<?=$breednice;?>/">Recommended Name for <?=$be_name;?></a>
      </div></div>
      <div class="breed_sidebar">
      <h4>Compare Related Breeds</h4>
      <ul>
<? foreach($relatedarray as $relbrd){ 
$rltdbrdname = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$relbrd'");
$rltdbreedname       = $rltdbrdname['be_name'];
$rltdbreednice       = $rltdbrdname['nicename'];
$rltdimg             = $rltdbrdname['image_name'];
	if($rltdimg){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$rltdimg;
		$imageURL='/new/breed_engine/images/dog_images/80-77-'.$rltdimg;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	    $dest = $DOCUMENT_ROOT.$imageURL;
	
	//	createImgThumbIfnot($src,$dest,'80','77','ratiowh');
	//	$compressed2 = compress_image($dest,$dest, 50);
		if($img){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$img;
		$imageURL='/new/breed_engine/images/dog_images/80-77-'.$img;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;

	//	createImgThumbIfnot($src,$dest,'80','77','ratiowh');
	//		$compressed3 = compress_image($dest,$dest, 50);
$checkstring=$breednice>$rltdbreednice;
?><a <? if($checkstring>0){?>href="/<?=$rltdbreednice;?>-vs-<?=$breednice;?>-compare/"<? }else{?>href="/<?=$breednice;?>-vs-<?=$rltdbreednice;?>-compare/"<? }?> target="_blank" title="<?=$be_name ?> vs <?=$rltdbreedname ?>" >
<li>
<div class="row">
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-<?= $img ?>" alt="<?=$be_name ?>" title="<?=$be_name ?>"  /> 
<?php /*?>  <label><?=$be_name ?></label><?php */?>
</div>

<div class="col-sm-4 col-xs-4"><div class="breed-vs"> vs </div></div>
<div div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-<?= $rltdimg ?>"  alt="<?=$rltdbreedname ?>" title="<?=$rltdbreedname ?>" /> 
<?php /*?>  <label> <?=$rltdbreedname ?></label><?php */?>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">
<?=$be_name ?> vs <?=$rltdbreedname ?></div>
</div>

</div>
</li></a>
<? } ?>
<li>
<div class="breed-more-com text-center">
<a href="/compare-<?=$breednice;?>-with-related-breeds/" >More Comparisons</a>
</div>
</li>

</ul>
	

      </div>
	  <div class="breed_sidebar">
	  
<h4>Browse by Groups</h4>
<ul>
<? while($sel_odr_grp_name1=mysql_fetch_array($sel_odr_grp_name)){ ?>
<a href="/<?=strtolower($sel_odr_grp_name1['value'])?>-group-dog-breeds/" style="cursor:pointer" target="_blank" >
<li><?=$sel_odr_grp_name1['value']; ?></li></a>
<? } ?>
</ul>

</div>
	 
      <div class="breed_sidebar">
<h4>Top Breeds</h4>
<ul>
<? if($section[0]!='german-shepherd-dog-alsatian'){ ?><a href="/german-shepherd-dog-alsatian/" target="_blank" >
<li><label>German Shepherd</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-german.jpg" alt="German Shepherd" title="German Shepherd" >
</li></a>
<? } ?>
<? if($section[0]!='rottweiler'){ ?><a href="/rottweiler/" target="_blank" >
<li><label>Rottweiler</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-rott.jpg" alt="Rottweiler" title="Rottweiler" >
</li></a>
<? } ?>
<? if($section[0]!='tibetan-mastiff'){ ?>
<a href="/tibetan-mastiff/" target="_blank" ><li><label>Tibetan Mastiff</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-tibetan-mastiff.jpg" alt="Tibetan Mastiff" title="Tibetan Mastiff" >
</li></a>
<? } ?>
<? if($section[0]!='golden-retriever'){ ?>
<a href="/golden-retriever/" target="_blank" ><li><label>Golden Retriever</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-golden.jpg" alt="Golden Retriever" title="Golden Retriever" >
</li></a>
<? } ?>
<? if($section[0]!='great-dane'){ ?>
<a href="/great-dane/" target="_blank" ><li><label>Great Dane</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-great-dan.jpg" alt="Great Dane" title="Great Dane" >
</li></a>
<? } ?>

</ul>

</div>
     
    <div class="breed_sidebar">
      <h4>Top Breed Comparison</h4>
      <ul>
<a href="/german-shepherd-dog-alsatian-vs-labrador-retriever-compare/" target="_blank" title="German Shepherd Dog (Alsatian) vs Labrador Retriever" >
<li>
<div class="row">
  <div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-german.jpg" alt="German Shepherd Dog (Alsatian)" title="German Shepherd Dog (Alsatian)"  /> 
</div>
<div class="col-sm-4 col-xs-4">
  <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-lab.jpg"  alt="Labrador Retriever" title="Labrador Retriever" /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">
German Shepherd Dog (Alsatian) vs Labrador Retriever</div>
</div>

</div>
</li>
</a>
<a href="/pit-bull-terrier-american-vs-rottweiler-compare/" target="_blank" title="Rottweiler vs Pit Bull Terrier (American)" >
<li><div class="row">
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-rott.jpg" alt="Rottweiler" title="Rottweiler"  /> 
</div>
<div class="col-sm-4 col-xs-4" >
   <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pit-bull.jpg"  alt="Pit Bull Terrier (American)" title="Pit Bull Terrier (American)" /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Rottweiler vs Pit Bull Terrier (American)</div></div>
</div>
</li>
</a>
<a href="/rottweiler-vs-tibetan-mastiff-compare/" target="_blank" title="Tibetan Mastiff vs Rottweiler">
<li><div class="row">
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-tibetan-mastiff.jpg" alt="Tibetan Mastiff" title="Tibetan Mastiff"  /> 
</div>
<div class="col-sm-4 col-xs-4">
  <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-rott.jpg" alt="Rottweiler" title="Rottweiler"  /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Tibetan Mastiff vs Rottweiler</div></div>
</div>
</li>
</a>
<a href="/golden-retriever-vs-labrador-retriever-compare/" target="_blank" title="Golden Retriever vs Labrador Retriever"  >
<li><div class="row">
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-golden.jpg" alt="Golden Retriever" title="Golden Retriever"  /> 
</div>
<div class="col-sm-4 col-xs-4">
  <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-lab.jpg"  alt="Labrador Retriever" title="Labrador Retriever" /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Golden Retriever vs Labrador Retriever</div></div>
</div>
</li>
</a>
<a href="/german-shepherd-dog-alsatian-vs-great-dane-compare/" target="_blank" title="Great Dane vs German Shepherd Dog (Alsatian)">
<li>
<div class="row">
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-great-dan.jpg" alt="Great Dane" title="Great Dane"  /> 
</div>
<div class="col-sm-4 col-xs-4 ">
 <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-german.jpg" alt="German Shepherd Dog (Alsatian)" title="German Shepherd Dog (Alsatian)"  /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Great Dane vs German Shepherd Dog (Alsatian)</div></div>
</div>
</li>
</a>
<a href="/boxer-vs-dobermann-compare/" target="_blank" title="Dobermann vs Boxer" >
<li>
<div class="row">
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-doberman.jpg" alt="Dobermann" title="Dobermann"  /> 
</div>
<div class="col-sm-4 col-xs-4 ">
 <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-boxer.jpg"  alt="Boxer" title="Boxer" /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Dobermann vs Boxer</div></div>
</div>
</li>
</a>
<a href="/beagle-vs-pug-compare/" target="_blank" title="Pug vs Beagle" >
<li>
<div class="row">
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pug.jpg" alt="Pug" title="Pug"  /> 
</div>
<div class="col-sm-4 col-xs-4 ">
 <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-beagle.jpg"  alt="Beagle" title="Beagle" /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Pug vs Beagle</div></div>
</div>
</li>
</a>
<a href="/beagle-vs-labrador-retriever-compare/" target="_blank" title="Beagle vs Labrador Retriever" >
<li>
<div class="row">
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-beagle.jpg"  alt="Beagle" title="Beagle" /> 
</div>

<div class="col-sm-4 col-xs-4 ">
 <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-lab.jpg"  alt="Labrador Retriever" title="Labrador Retriever" /> 
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Beagle vs Labrador Retriever</div></div>
</div>
</li>
</a>
<a href="/pomeranian-vs-pug-compare/" target="_blank" title="Pomeranian vs Pug" >
<li>
<div class="row">
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pom.jpg" alt="Pomeranian" title="Pomeranian"  /> 
</div>
<div class="col-sm-4 col-xs-4 ">
 <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pug.jpg" alt="Pug" title="Pug"  /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Pomeranian vs Pug</div></div>
</div>
</li>
</a>
<a href="/pug-vs-shih-tsu-compare/" target="_blank" title="Shih Tzu vs Pug" >
<li><div class="row">
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-shih-tzu.jpg"  alt="Shih Tzu" title="Shih Tzu" /> 
</div>

<div class="col-sm-4 col-xs-4 ">
 <div class="breed-vs"> vs </div>
</div>
<div class="col-sm-4 col-xs-4 ">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pug.jpg" alt="Pug" title="Pug"  /> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="breed-vs-text text-center">Shih Tzu vs Pug</div></div>
</div>
</li>
</a>
</ul>
	

      </div>
      <div class="breed_sidebar">
      <h4>Articles</h4>
      <?     $article=query_execute("SELECT DISTINCT(wpp.ID), wpp.post_title,wpp.post_name,wpp.post_content,first_image_name FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_content LIKE '%$breedname%' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_date>'2014-01-01 00:00:00' AND wpp.post_status='publish' AND wpp.ID NOT IN (16616,15348,17664,4773,2125) ORDER BY wpp.ID DESC LIMIT 7");
		$count=mysql_num_rows($article);
		if($count<3){
		$breedhalfDog=explode(' ',$breedname);
		$breedhalf=	$breedhalfDog[0];
		if(!$breedhalfDog[1])
		{
		$breedhalfDog=explode('.',$breedname);
		$breedhalf=	$breedhalfDog[1];	
		}
		if($breedhalf){
	 $article=query_execute("SELECT DISTINCT(wpp.ID), wpp.post_title,wpp.post_name,wpp.post_content,first_image_name FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_content LIKE '%$breedhalf%' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_date>'2014-01-01 00:00:00' AND wpp.post_status='publish' AND wpp.ID NOT IN (16616,15348,17664,4773,2125) ORDER BY wpp.ID DESC LIMIT 7");
		}else
		{
		$article=query_execute("SELECT DISTINCT(wpp.ID), wpp.post_title,wpp.post_name,wpp.post_content,first_image_name FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE (wpp.post_content LIKE '%$breedname%' OR wpp.post_content LIKE '%cost%') AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_status='publish' AND wpp.post_date>'2014-01-01 00:00:00' AND wpp.ID NOT IN (16616,15348,17664,4773,2125) ORDER BY wpp.ID DESC LIMIT 7");
		
		}
		}?>
      <div id="owl-banner-slider" class="owl-carousel">        
               
                <?
               while($rowArt=mysql_fetch_array($article)){
				$post_name   = $rowArt["post_title"];
				$cat_name=$rowArt['post_name'];
				$first_image_name=$rowArt['first_image_name'];
				$main_fea_art_body=$rowArt['post_content'];
				$main_fea_art_id=$rowArt['ID'];
				$image=explode('/',$first_image_name);
				if(count($image)>0){
				$countI=count($image)-1;
				}else
				{
				$countI=0;	
				}
		$imageURL = '/imgthumb/200x190-'.$image[$countI];
	
		// Get Image Width and Height, Updated date: 18-Nov-2014
		$image_info02 = getimagesize($DOCUMENT_ROOT.$imageURL);		
		$image_width02 = $image_info02[0];
		$image_height02 = $image_info02[1];
		// End
		?>          
                    <div class="item">
                    	<a href="/<?=$cat_name?>/"> 
		  	<img src="<?=$imageURL?>" alt="<?=$cat_name; ?>" title="<?=$cat_name ?>" style="width:268px;height:205px;" width="<?= $image_width02 ?>" height="<?= $image_height02 ?>">
		  </a>
                        <div class="caption">
                       	  <h4><?=snippetwop($post_name,$length=25,$tail="...");?></h4>
                            
                        </div>
                    </div>
                      
                    <? } ?>                                                                                   
                
				</div>
      </div>
      <!-- qna-->
      <?
			$qt='&sort=qna_cdate desc';
			if($breed_id=="494"){
				$url2 = "http://localhost/solr/dogspotqna/select/?q=Chihuahua&fq=publish_status:publish$bsql$qt&version=2.2&rows=5&fl=* score&qf=title_tag^2&df=text&wt=xml&indent=true";
			}else{
	 		$url2 = "http://localhost/solr/dogspotqna/select/?q=$breedname&fq=publish_status:publish$bsql$qt&version=2.2&rows=5&fl=* score&qf=title_tag^2&df=text&wt=xml&indent=true";
			}
			$url2 = str_replace(" ","%20",$url2);
			
			$resultqna = get_solr_result($url2);
			$totrecordqna = $resultqna['TOTALHITS'];
			if($totrecordqna > 0){
?>
          <div class="breed_sidebar">
          <h4>Q&amp;A</h4>
          <ul>
<? 			foreach($resultqna['HITS'] as $rowArtqna){
			  $i=$i+1;
            $qna_id = $rowArtqna["qna_id"];
            $qnauser = $rowArtqna["userid"];
            $qna_question = $rowArtqna["qna_question"];
            $qna_name = $rowArtqna["qna_name"];
            $qna_tag = stripslashes($rowArtqna["qna_tag"]);
            $qna_catid = $rowArtqna["qna_catid"];
            $qna_cdate = $rowArtqna["qna_cdate"];
			$qna_question = strip_tags(stripslashes($qna_question));
			$qna_question = substr($qna_question, 0, 60);
            $qna_question = nl2br(stripslashes($qna_question));
           ?><a href="<? echo"/qna/$qna_name/";?>" target="_blank" >
<li>
<p><? echo"$qna_question"."..."; ?></p>
</li></a>
<? } ?>
<li>
<div class="breed-more-com text-center">
<a href="/qna/search/<?=$breedname?>" rel="nofollow">Browse for more</a>
</div>
</li>
</ul>
          
          </div>
      <!-- qna end-->
      
      <? } ?>
      </aside>
  </div>
        
        </div>
        </div>
</div>
</section>
<script type="text/javascript" src="/bootstrap/js/owl.carousel.min.js"></script> 
<script>
$(document).ready(function() {
      $("#owl-banner-slider").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
	  autoPlay: 4000,
	  navigation:false,
      singleItem : true,
	 pagination: false
	  

      });
    });
</script>
<script>
function moveToFooter(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top},
        'slow');
}

function showpages(cmnt_limit){
var c_limit=cmnt_limit-1;
if(c_limit >= 0){	
var c_limit=c_limit*5;	
ShaAjaxJquary('/new/breed_engine/cmnt_paging.php?breed=<?=$breed_id ?>&cmnt_limit='+c_limit+'', '#show_cmnt', '', '', 'GET', '#show_cmnt', 'Loading...', 'REP');	
}
}
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script> 
 
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php'); ?>

