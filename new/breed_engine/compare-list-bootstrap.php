<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection='dog-breeds';
$breedid = query_execute_row("SELECT * FROM dog_breeds WHERE nicename='$breed_nice' AND breed_engine='1'");
$breed_id=$breedid['breed_id'];
if(!$breed_id){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();	
	}
$breednameselect = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$breed_id' AND breed_engine='1'");
$be_name       = $breednameselect['be_name'];
$img		   = $breednameselect['image_name'];
$selgroup        = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='35'");
$selgroup1       = $selgroup['value'];
$selrelated      = mysql_query("SELECT bev.breed_id FROM breed_engine_values as bev , dog_breeds as db WHERE bev.breed_id=db.breed_id AND bev.att_id='35' AND bev.value='$selgroup1' AND bev.breed_id !='$breed_id' AND db.breed_engine='1' LIMIT 5");
	if($img){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$img;
		$imageURL='/new/breed_engine/images/dog_images/120-114-'.$img;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,'120','114','ratiowh');

?>
<?php
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
?>
<link type="text/css" rel="stylesheet" href="/bootstrap/css/dog-breeds.css">

<div class="breadcrumbs"> 
    <div class="container"> 
      <div class="row" itemscope itemtype="http://schema.org/Breadcrumb"> 
        <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
         <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> 
      <a href="/" itemprop="item"><span itemprop="name">Home</span></a> 
       <meta itemprop="position" content="1" /> </span> 
     <span> / </span> 
       <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">  
     <span itemprop="item"> 
   <a href="/dog-breeds/"  itemprop="name">Dog Breeds</a></span> 
   <meta itemprop="position" content="2" /> </span> 
       <span> / </span>
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">  
     <span itemprop="item"> 
     <span itemprop="name" class="active-bread">More Comparison</span></span> 
      <meta itemprop="position" content="3" /> </span> 
      
        </div> 
      </div> 
    </div> 
  </div>
<section class="breeds-section more-breed-com" >
  <div class="container">
    <div class="row">
    <div class="col-md-12">
    <h1>Related Comparisons for <a href="/<?=$breed_nice;?>/"><?= $be_name;?></a></h1>
    </div>
    <? while($selrelated1=mysql_fetch_array($selrelated)){
$relbrd=$selrelated1['breed_id'];
$rltdbrdname = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$relbrd' AND breed_engine='1'");
$rltdbreedname       = $rltdbrdname['be_name'];
$rltdbreednice       = $rltdbrdname['nicename'];
$rltdimg             = $rltdbrdname['image_name'];
$checkstring=$breed_nice>$rltdbreednice;
?>
     <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
    <div class="more-breedcom-box ">
            <div class="col-sm-5">
            <img src="/new/breed_engine/images/dog_images/120-114-<?= $img ?>" width="120" height="114" alt="<?=$rltdbreedname?>" title="<?=$rltdbreedname?>" /> 
            <h4><?=$be_name; ?></h4>
            </div>
                      <div class="col-sm-2 breed-vs-box" >
                     <div class="breed-vs"> vs </div>
                      </div>
           <div class="col-sm-5">
           <?
	if($rltdimg){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$rltdimg;
		$imageURL='/new/breed_engine/images/dog_images/120-114-'.$rltdimg;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,'120','114','ratiowh');
?>
                   <img src="/new/breed_engine/images/dog_images/120-114-<?= $rltdimg ?>" width="120" height="114" alt="<?=$rltdbreedname?>" title="<?=$rltdbreedname?>" />
            
            <h4><?=$rltdbreedname ?></h4>
            </div>
        
        <div class="col-sm-12">
        <a <? if($checkstring>0){?>href="/<?=$rltdbreednice;?>-vs-<?=$breed_nice;?>-compare/"<? }else{?>href="/<?=$breed_nice;?>-vs-<?=$rltdbreednice;?>-compare/"<? }?>><div class="comp-btn">Compare</div></a>
        </div>
        </div>
        
        
        </div>
        <? } ?>
    </div>
    </div>
    </section>
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');
?>