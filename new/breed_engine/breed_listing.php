<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/be_style.css" />
<title>Breed Engine</title>

<?php require_once($DOCUMENT_ROOT.'/new/common/header_new.php');?>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<?php /*?> <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script><?php */?>
    <script type="text/javascript">
        $(document).ready(function(){
            // set up hover panels
            // although this can be done without JavaScript, we've attached these events
            // because it causes the hover to be triggered when the element is tapped on a touch device
            $('.hover').hover(function(){
                $(this).addClass('flip');
            },function(){
                $(this).removeClass('flip');
            });
        });
    </script>
    
<div class="cont980">
<div class="be_breedBox">
<h1>Get to know about your <span style="color:#668000">Breed</span></h1>
<div class="be_back">
<div>Click here</div>
<div class="be_arw"><img src="images/arw_blck.png" width="15" height="10" alt="" /></div>
<div><a><img src="images/click.png" width="55" height="55" alt="" /></a></div>
</div>
<ul>
<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>

<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>

<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>

<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>

<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>

<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>

<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>

<li class="be_boxBreed">
<a href="#">
<h3>Boxer</h3>
<div class="be_thumbImg">
  <img src="images/thumb_img.jpg" width="229" height="156" alt="" />
</div>
</a>
<div class="be_hover">
<a href="#"><h3>Boxer</h3></a>
</div>
</li>
</ul>
</div>





</div>