<?php
$dater=explode('=',$_GET['breeds']);
if($dater[0]=='breeds'){
	header("HTTP/1.1 301 Moved Permanently");
	header('Location: /dog-breeds/');
    exit();
}
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

$selectdata=mysql_query("SELECT breed_id,value FROM breed_engine_values WHERE att_id='$att_id' ORDER BY value DESC");
$pageurl=explode("?",$_SERVER["REQUEST_URI"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title><?=$pageTitle?><?=$breeds?></title>
<meta name="keywords" content="<?=$pageKeyword?><?=$breeds?>" />
<meta name="description" content="<?=$pageDesc?><?=$breeds?>" />
<? if($shopQuery){?>
<meta name="robots" content="noindex, nofollow">
<? }?>
<link href="/new/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/new/css/jquery-ui.css" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script type="text/javascript" src="/new/js/iepngfix_tilebg.js"></script>
<script type="text/javascript">
function addbreed(){
	var breed_id=document.getElementById("prv_breed").value + ',' + document.getElementById("breed_id_add").value;
	window.location='/new/breed_engine/breed_engine3.php?breeds='+breed_id;
	
}
</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>    
 <!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980">
                <div id="ajaxdiv23"></div>
                    <div class="fl" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666;"> <a href="/shop/">Home</a> » <a href="/shop/">Shop</a> » <?=$category_name?>
                     </div>
                 
                     <div class="fr"> <a href="javascript:window.print()"><img src="/new/pix/bdcmb_printicon.gif" alt="print" title="print"  /></a>
                        
                       <a href="http://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a>
      <a href="http://twitter.com/#!/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a>                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb --> 
            <div class="cont980">
            <select id="breed_id_add" name="breed_id_add" onchange="addbreed();">
      <option value='0'>Select Breed</option>
        <?php
			$query_domain = query_execute("SELECT * FROM dog_breeds WHERE breed_engine='1'");
		
			while($query_domain1 = mysql_fetch_array($query_domain)){
			$breed_id12 = $query_domain1["breed_id"];
			$breed_name = $query_domain1["breed_name"];
			  print "<option value='$breed_id12'";
			  if($breed_id12 == $breed_id){   echo "selected=='selected'";  }  print ">$breed_name</option>";
			 }
		  ?>
    </select>
    <br /><br />
<table>
<tr>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
//echo "SELECT * FROM breed_engine_values WHERE breed_id='$breedid' ORDER BY att_id ASC";
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' ORDER BY att_id ASC");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];

$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	
	echo $attname['traits']." : ";
	?>
                 <? 
				$qRATING = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
								
								if($att_id <= '25') {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=$qRATING['value']/2;
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="onestar";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="twostar";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="threestar";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="fourstar";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="fivestar";}
								
								?> 
<div id="rateIt" >
                                
     <div id="rateIt" class="dn" itemprop="aggregateRating"   itemscope itemtype="http://schema.org/AggregateRating">
    <span itemprop="ratingValue"><?=$finalratng?></span>
<?php /*?>    <span itemprop="bestRating"><?=$qbest;?></span><?php */?>
<?php /*?>     <span itemprop="ratingCount"><?=$qRATING['num_votes']?></span> <?php */?>
  </div>

                                <ul class="rating <?=$ratvalue;?>">
                                
                               
	<li class="one"><a title="1 Star">1</a></li>
	<li class="two"><a title="2 Stars">2</a></li>
	<li class="three"><a title="3 Stars">3</a></li>
	<li class="four"><a title="4 Stars">4</a></li>
	<li class="five"><a title="5 Stars">5</a></li>
</ul></div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$att_value'");
echo $valuename['value']."<br>";
?>

<? }
}
}
}
echo "<td>"; ?>
<?
//echo $breedname."&nbsp;&nbsp;".$att_value;
echo "</td>";
$count++;
if($count=='4'){
	$count='0';
?>
</tr>
<tr>
<?
}?>
</tr>
<tr><td>
<input type="hidden" id="chkvalue" name="chkvalue" />
</table>
<input type="hidden" id="prv_breed" name="prv_breed" value="<?=$pageurl[1] ?>" />
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>  