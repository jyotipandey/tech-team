<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title><?=$pageTitle?></title>
<meta name="keywords" content="<?=$pageKeyword?>" />
<meta name="description" content="<?=$pageDesc?>" />
<? if($shopQuery){?>
<meta name="robots" content="noindex, nofollow">
<? }?>
<link href="/new/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/new/css/jquery-ui.css" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script type="text/javascript" src="/new/js/iepngfix_tilebg.js"></script>

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>    
 <!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980">
                <div id="ajaxdiv23"></div>
                    <div class="fl" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666;"> <a href="/shop/">Home</a> » <a href="/shop/">Shop</a> » <?=$category_name?>
                     </div>
                 
                     <div class="fr"> <a href="javascript:window.print()"><img src="/new/pix/bdcmb_printicon.gif" alt="print" title="print"  /></a>
                        
                       <a href="http://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a>
      <a href="http://twitter.com/#!/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a>                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb --> 
            <div class="cont980">
<table>
<tr>
<?php
$count=0;

$breedid=$brd;
//echo "SELECT * FROM breed_engine_values WHERE breed_id='$breedid' ORDER BY att_id ASC";
$breednameselect=query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$breed_id'");
$breedname=$breednameselect['breed_name'];
$breednice=$breednameselect['nicename'];
  ?>   <h1 align="center"><?=$breedname; ?></h1>
  <a href="/new/common/breed_engine4.php?breed_id='<?=$breed_id ?>'">Know More About Your Breed</a>
  <a href="/wag_club/">Add Your Dog</a>
  <a href="/dogs/dogsearch/<?=$breednice; ?>/">Wag Club</a>
  <a href="/dogs/dogsearch/<?=$breednice; ?>/">Related breeds</a>
  <br />
  <?
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' ORDER BY att_id ASC");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];

$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	
	echo $attname['traits']." : ";
	?>
                 <? 
				$qRATING = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
								
								if($att_id <= '25') {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=$qRATING['value']/2;
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="onestar";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="twostar";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="threestar";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="fourstar";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="fivestar";}
								
								?> 
                           
<div id="rateIt" >
                                
     <div id="rateIt" class="dn" itemprop="aggregateRating"   itemscope itemtype="http://schema.org/AggregateRating">
    <span itemprop="ratingValue"><?=$finalratng?></span>
<?php /*?>    <span itemprop="bestRating"><?=$qbest;?></span><?php */?>
<?php /*?>     <span itemprop="ratingCount"><?=$qRATING['num_votes']?></span> <?php */?>
  </div>

                                <ul class="rating <?=$ratvalue;?>">
                                
                               
	<li class="one"><a title="1 Star">1</a></li>
	<li class="two"><a title="2 Stars">2</a></li>
	<li class="three"><a title="3 Stars">3</a></li>
	<li class="four"><a title="4 Stars">4</a></li>
	<li class="five"><a title="5 Stars">5</a></li>
</ul></div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$att_value'");
echo $valuename['value']."<br>";
?>

<? }
}

echo "<td>"; ?>
<?
//echo $breedname."&nbsp;&nbsp;".$att_value;
echo "</td>";
$count++;
?>
</tr>
<tr><td>
<input type="hidden" id="chkvalue" name="chkvalue" />
<input type="button" value="Next Page" id="nxtbtn" />
</table>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>  