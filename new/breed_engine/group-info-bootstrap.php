<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

$sel_grp_name=query_execute_row("SELECT * FROM breed_engine_att_att WHERE att_id='35' AND att_att_id='$group_id'");
$sel_grp_det=query_execute_row("SELECT * FROM breed_engine_group WHERE group_id='$group_id'");
$sel_odr_grp_name=mysql_query("SELECT * FROM breed_engine_att_att WHERE att_id='35' AND att_att_id!='$group_id'");
$sel_grp_members=mysql_query("SELECT breed_id FROM breed_engine_values WHERE att_id='35' AND value='$group_id'");
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = '';
$ant_category =ucwords(str_replace("-"," ",$section[0]));

if($sel_grp_name['value']=='Gundog'){
$title="Gundog Group | Hunting Dog | Sporting Dogs";
$keyword="Gundog Dog Breeds, Gundog Group Dog Breeds, Gundog Dogs, hunting dog group, sporting dogs group.";
$desc="Gundog Group Dog Breeds consists of sporting Dogs which are specially used in hunting, including Cocker Spaniel, Golden Retriever, Irish Setter, Labrador Retriever etc.";
 }elseif($sel_grp_name['value']=='Pastoral'){ 
$title="Pastoral Group | Herding Dogs";
$keyword="Pastoral Dog Breeds, Pastoral Group Dog Breeds, Pastoral Dogs, Herding Dogs." ;
$desc="Pastoral Group consists of herding dogs which are commonly bred for a working life in the fields including Border Collie, German Shepherd Dog (Alsatian), Shetland Sheep dogs etc." ;
 }elseif($sel_grp_name['value']=='Utility'){
$title="Utility Group | Non-Sporting Dogs";
$keyword="Utility Dog Breeds, Utility Group Dog Breeds, Utility Dogs, Non-Sporting Dogs." ;
$desc="Utility group consists of miscellaneous dog breeds, mainly non sporting origin including bull dog, dalmatian, lhasa apso, shar pei etc. Find more about Utility dog breeds at DogSpot.in." ;
 }else{
$title=$sel_grp_name['value']." | ".$sel_grp_name['value']." Group | ".$sel_grp_name['value']." Dogs | DogSpot.In";
$keyword=$sel_grp_name['value']." Dog Breeds, ".$sel_grp_name['value']." Group Dog Breeds,". $sel_grp_name['value']." Dogs" ;
$desc="Find". $sel_grp_name['value']." Group Dog Breeds List and " . $sel_grp_name['value']." Dog Breeds List. Find more about ".$sel_grp_name['value']." dog breeds at DogSpot.in" ;
 }
    $alternate="https://m.dogspot.in/$section[0]/";
	$canonical="https://www.dogspot.in/$section[0]/";
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in/new/pix/$section[0].jpg";
	$page_type='Dog Breeds';

require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
?>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/dog-breeds.css?v=17" />

<div class="breadcrumbs"> 
    <div class="container"> 
      <div class="row" itemscope itemtype="http://schema.org/Breadcrumb"> 
        <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
         <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> 
      <a href="/dog-breeds/" itemprop="item"><span itemprop="name">Dog Breeds</span></a> 
       <meta itemprop="position" content="1" /> </span> 
     <span> / </span> 
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">  
     <span itemprop="item"> 
     <span itemprop="name" class="active-bread"><?=$sel_grp_name['value']; ?> Group</span></span> 
      <meta itemprop="position" content="2" /> </span> 
        </div> 
      </div> 
    </div> 
  </div>

<section class="breeds-section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
     <div class="breeds-box"> 
     <?
$sel_img=query_execute_row("SELECT db.image_name FROM dog_breeds as db ,breed_engine_values as bev WHERE bev.att_id='35' AND bev.value='$group_id' AND db.breed_id=bev.breed_id AND db.breed_engine='1'");
?>
  <img  src="/new/breed_engine/images/dog_images/<?= $sel_img['image_name']?>" alt="<?= $sel_img['image_name']?>" title="<?= $sel_img['image_name']?>" style="margin-top:40px;"> 
</div>
      </div>
            <div class="col-xs-12 col-sm-12 col-md-9">
        <div class="breed-info">
        <h1><?=$sel_grp_name['value'] ?> Group Dog Breed</h1>
<p><?=$sel_grp_det['group_desc']; ?></p>
        </div>
        </div>
      </div>
      </div>
       <div class="container">
    <div class="row">
      <div class="col-sm-9">
      <div class="be_slideBox dog-breed-sec">
  
<h2>Dog in <?=$sel_grp_name['value'] ?> Group</h2>
  <div class="row">
  
  <? while($sel_grp_members1=mysql_fetch_array($sel_grp_members)){ 
  $sel_brd_name=query_execute_row("SELECT * FROM dog_breeds WHERE breed_id=$sel_grp_members1[breed_id]");
  $brd_grp_arr[]=$sel_brd_name['breed_name']."@@".$sel_brd_name['nicename']."@@".$sel_brd_name['image_name']."@@".
  $sel_brd_name['breed_engine'];
  }
  //print_r($brd_grp_arr);
  asort($brd_grp_arr);
  foreach($brd_grp_arr as $brd_grp_arr11){
	$brd_grp_arr1=explode("@@",$brd_grp_arr11); 
  if($brd_grp_arr1[3]=='1'){
  ?>
  
<div class="col-xs-6 col-sm-6 col-md-4 text-center">
<div class="breed-group">
<a href="/<?=$brd_grp_arr1[1] ?>/" >
<img src="/new/breed_engine/images/dog_images/<?=$brd_grp_arr1[2] ?>" title="<?=$brd_grp_arr1[0] ?>" 
alt="<?=$brd_grp_arr1[0] ?>" border="0" width="150">
<?=$brd_grp_arr1[0] ?>
</a>
</div> 
</div>
<? }} ?>   
<div class="clearfix"></div>
     </div>
    <div class="be_stay text-center">Stay tuned for more breeds</div>
  </div>
      <div class="be-share-box">
                          <?  
                        $surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                       include($DOCUMENT_ROOT."/new/articles/social-bootstrap.php"); 
                       
                     ?>
                        </div>
      </div>
      <aside class="col-xs-12 col-sm-6 col-md-3">
      <div class="breed_sidebar">
	  
<h4>Browse by Groups</h4>
<ul>
<? while($sel_odr_grp_name1=mysql_fetch_array($sel_odr_grp_name)){ ?>
<a href="/<?=strtolower($sel_odr_grp_name1['value'])?>-group-dog-breeds/" style="cursor:pointer" target="_blank" >
<li><?=$sel_odr_grp_name1['value']; ?></li></a>
<? } ?>
</ul>

</div>
      <div class="breed_sidebar">
<h4>Top Breeds</h4>
<ul>
<? if($section[0]!='german-shepherd-dog-alsatian'){ ?><a href="/german-shepherd-dog-alsatian/" target="_blank" >
<li><label>German Shepherd</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-german.jpg" alt="German Shepherd" title="German Shepherd" >
</li></a>
<? } ?>
<? if($section[0]!='rottweiler'){ ?><a href="/rottweiler/" target="_blank" >
<li><label>Rottweiler</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-rott.jpg" alt="Rottweiler" title="Rottweiler" >
</li></a>
<? } ?>
<? if($section[0]!='tibetan-mastiff'){ ?>
<a href="/tibetan-mastiff/" target="_blank" ><li><label>Tibetan Mastiff</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-tibetan-mastiff.jpg" alt="Tibetan Mastiff" title="Tibetan Mastiff" >
</li></a>
<? } ?>
<? if($section[0]!='golden-retriever'){ ?>
<a href="/golden-retriever/" target="_blank" ><li><label>Golden Retriever</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-golden.jpg" alt="Golden Retriever" title="Golden Retriever" >
</li></a>
<? } ?>
<? if($section[0]!='great-dane'){ ?>
<a href="/great-dane/" target="_blank" ><li><label>Great Dane</label>
<img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-great-dan.jpg" alt="Great Dane" title="Great Dane" >
</li></a>
<? } ?>

</ul>

</div>
      </aside>
      </div>
      </div>
      </section>
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');
?>