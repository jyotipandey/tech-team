<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";
 $title="Dog Breeds | Complete Information of dogs | Dogspot.in";
	$keyword="Dog breed name, Pet breeds in India, Indian dog breeds, Dog breed info, Dog breed types";
    $desc="It will give you a comprehensive view for every individual breed; this includes the cost involved in getting a puppy home to the monthly expenditure";
	$alternate="https://m.dogspot.in/$section[0]/";
	$canonical="https://www.dogspot.in/$section[0]/";
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in/new/breed_engine/images/logo-300x300.jpg";
	$page_type='Dog Breeds';

$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");
 require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/dog-breeds.css?v=10" />
<section class="breeds-section">
<div class="container-fluid">
    <div class="row" id="knwMore" onclick="moveToFooter('breed_box')"> <img src="https://ik.imagekit.io/2345/new/breed_engine/images/dog-breed-new.jpg" alt="Dog Breeds" name="Dog Breeds" class="img-responsive" id="Dog Breeds"> </div>
  </div>
  <div class="container-fluid breeds-nav">
    <div class="container">
      <nav class="navbar">
        <ul class="nav navbar-nav">
          <li class="current"> <a href="/big-dog-breeds/">big 
            dog</a></li>
          <li><a href=" /therapy-dog-breeds/">stress 
            buster</a></li>
          <li><a href="/friendly-dog-breeds/">just a
            Friend</a></li>
          <li> <a href="/cute-dog-breeds/">cute &amp;
            furry buddy </a></li>
          <li><a href="/kid-friendly-dog-breeds/">for
            kids</a></li>
          <li> <a href="/guard-dog-breeds/">guard
            dog</a></li>
        </ul>
        
      </nav>
    </div>
  </div>
  <div class="breadcrumbs"> 
    <div class="container"> 
      <div class="row" itemscope itemtype="http://schema.org/Breadcrumb"> 
        <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
         <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> 
      <a href="/" itemprop="item"><span itemprop="name">Home</span></a> 
       <meta itemprop="position" content="1" /> </span> 
     <span> / </span> 
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">  
     <span itemprop="item"> 
     <span itemprop="name" class="active-bread">Dog Breeds</span></span> 
      <meta itemprop="position" content="2" /> </span> 
        </div> 
      </div> 
    </div> 
  </div>
  <div class="container">
  <form name="formcomnt" id="formcomnt"></form>
<div class="row" itemscope itemtype="https://www.schema.org/Organization">

<form name="formcomnt" id="formcomnt">
<div class="be_breedBox" id="breed_box" name="breed_box" >
<?
$breednameselect=mysql_query("SELECT * FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC");
while($breeddetails=mysql_fetch_array($breednameselect)){
$breedid=$breeddetails['breed_id'];
$breedname=$breeddetails['breed_name'];
$breednice=$breeddetails['nicename'];
$imgname=$breeddetails['image_name'];
$icon=$breeddetails['icon'];
$ht=$breeddetails['height'];
$wt=$breeddetails['weight'];
$be_name=$breeddetails['be_name'];
$breedadjselect=mysql_query("SELECT * FROM breed_adj WHERE breed_id='$breedid' ORDER BY adj_name ASC LIMIT 4");
while($breedadjselect1=mysql_fetch_array($breedadjselect)){
$adjname[]=$breedadjselect1['adj_name'];
}
?>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
<div class="breeds-box">
<a href="/<?=$breednice;?>/">
<? if($breedid=='111' || $breedid=='629'){ ?>
<div class="be_new"><img src="/new/breed_engine/images/new-be.png" width="85" height="85" alt="New" title="New"></div>
<? } ?>
<h3 itemprop="name"><?=$be_name; ?></h3>

<img itemprop="image" src="https://ik.imagekit.io/2345/new/breed_engine/images/dog_images/<?=$imgname; ?>" width="229" height="217" alt="<?=$breedname?>" title="<?=$breedname?>" />
<ul itemprop="description">
<? foreach($adjname as $adjname1){ ?>
<li><?= $adjname1; ?></li>
<? } ?>
</ul>

</a>
<div class="breed-hover-box">
<a href="/<?=$breednice; ?>/">
<h3><?=$be_name; ?></h3>
<div class="breed-hover-box-height">
 <div class="breed-hover-box-icon">
 <? if($breedid=='636'){ ?><p class="das_wc_height">Standard : <? }else{ ?><p><? } ?><?=$ht ?> Inches<label style="cursor:pointer; float:none; margin:0;" id="This is an average height till head" title="This is an average height till head">*</label></p>
    <?
 $new_ht= round($new_ht=(266/72)*$ht);
	if($icon){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/icons/'.$icon;
		$imageURL='/new/breed_engine/images/icons/new-ht-'.$icon;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,$new_ht,$new_ht,'ratiohh');
?>
<img src="https://ik.imagekit.io/2345/new/breed_engine/images/icons/new-ht-<?=$icon; ?>" alt="<?=$breedname?>" title="<?=$breedname?>" style="max-width: 219px;">
</div>
  </div>
  <? if($breedid=='636'){ ?>
<div class="breed-hover-box-weight">
<p class="be_das_height">Miniature : 6 Inches<label style="cursor:pointer; float:none; margin:0;" id="This is an average height till head" title="This is an average height till head">*</label>
 </p>
</div>
<? } ?>
<? if($breedid!='636'){ ?>
<div class="breed-hover-box-weight">
<p>In Kg<label style="cursor:pointer; float:none; margin:0;" id="Average for the breed" title="Average for the breed">*</label><br>
<?=$wt ?></p>
</div>

<? } if($breedid=='636'){ ?>
<?php /*?><div class="be_weight be_std_weight">
Standard
<p>In Kg<label style="cursor:pointer; float:none; margin:0;" id="Average for the breed" title="Average for the breed">*</label><br>
<?=$wt ?></p></div><?php */?>
<div class="breed-hover-box-weight">

<p>In Kg<label style="cursor:pointer; float:none; margin:0;" id="Average for the breed" title="Average for the breed">*</label><br>
upto 4 Kg</p></div>
<? } ?>
</a>
</div>

</div>

</div>
<? 
$adjname='';
} ?>
</div>
</form>
</div>

</div>
<div class="container">
<div class="row">
 <div class="col-md-12" style="margin:30px 0px 0px;">
       <?  
                        $surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                       include($DOCUMENT_ROOT."/new/articles/social-bootstrap.php"); 
                       
                     ?>
       </div>
</div>
</div>
<div class="container">
<div class="row">
 <div class="col-md-12 dog-breeds-text">
<h1>Dog Breeds</h1>
<p>
Keeping pets is not exactly a child’s play. It requires a lot of research; you cannot go and get a pup just because he or she is cute or just because you are getting it easily. Having a pet is a lot of work and at times can be tedious and time consuming as well. Getting a puppy is like bringing a child home, it is a lot of hard work with excellent rewards. So, when you decide that it is time to own a pet it is absolutely essential to know a few things about the breed.<br />
The FCI (the world canine organization) recognises 343 dog breed types in the world. That is a whopping number to select a breed from, so what do you do in such cases. We will help you resolve this issue efficiently.</p>

<h3>Recognised breeds</h3>
<p>As we mentioned earlier there are numerous dogs breeds in the world and all of them are not even recognised by the Kennel Clubs. So, how do you know that which is the pedigree breed and has been recognised by the Kennel Clubs. Simple, in our breed write ups, we have dedicated an entire section to the breed standards that is how each and every part of the breed should be or you can say that the standards that are excepted by the kennel clubs across the  world. The standards have been taken from the esteemed kennel clubs such as AKC and UK Kennel Club.<br />
This will be for both the existing as well as the prospective dog owners. So now whichever breed you want, whether  it is the handsome German Shepherd or one of the adorable Retrievers, we will help you to find the perfect pet breeds in India and throughout the world as per your requirement.</p>

<h3>Why do you want a dog?</h3>
<p>Our six broad filters will help you to select the perfect breed. Not everyone has the same requirement. Every breed will have its pros and cons; we will help you to select the perfect breed for your home or estate. Whether it is to guard your big estate or just for companionship in your home. Our aim is simple to show the dog that will be best suited for you.</p>

<h3>How will it help you?</h3>
<p>Wagpedia will give you a comprehensive view for every individual breed; this includes the cost involved in getting a puppy home to the monthly expenditure that is incurred in the upkeep of the individual breed. This will include an average of expenditure incurred for the breed’s dry dog food, grooming and monthly vet visits.<br />
The write ups have been divided into sub categories for both present and prospective dog owner to help you read specific sections as well if you want.  Our aim is to help you select the appropriate breed for you home, whatever the need maybe.<br />
We plan to add more and more breeds to make it easier for our patrons. If we have forgotten a breed please then we apologise and will make a sincere effort to include them soon. So just watch out for this space for interesting reads or dog breed info.<br />
</p>
</div>
</div>
</div>
</section>
<? require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');?>
