<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<?php
$breedid=$brd;
//echo "SELECT * FROM breed_engine_values WHERE breed_id='$breedid' ORDER BY att_id ASC";
$breednameselect=query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$breed_id'");
$breedname=$breednameselect['breed_name'];
$breednice=$breednameselect['nicename'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/be_style.css" />
<title>Breed Engine</title>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<?php require_once($DOCUMENT_ROOT.'/new/common/header_new.php');?>
<style>.container{ float:left;width: 680px;}
html ul.tabs li.active {
 border-radius:0;
background: #fff;
border:1px solid #668000;
border-bottom: 0px;
}
html ul.tabs li.active a{ border:0px; border-radius:0;background: #fff; border-top:4px solid #668000;}
html ul.tabs li{ border-radius:0;border:0px solid #668000; }
ul.tabs li a{background: #f7ffd8;font-size:14px;padding: 0px 15px;
border: 1px solid #668000; border-radius:2px 2px 0 0;}
html ul.tabs li.active a:hover, html ul.tabs a:hover {
background: #668000; color:#fff;
border-radius: 0;
border: 1px solid #666;
border-bottom: 0px;
}
html ul.tabs li.active a:hover{border:0px;}
.tab_container{border: 0;
border-top: 1px solid #668000;
border-radius: 0px;
}
</style>
<div class="cont980">
<div class="be_detailBox">
<div class="be_imgDetail">
  <img src="images/thumb_img.jpg" width="329" height="256" alt="" /> 
</div>
<div class="be_txtDetail">
<h3><?=$breedname; ?></h3>
<p>
<label><strong>Common Nicknames:</strong> King of Terriers,	Airedale</label>
<label><strong>Origin:</strong> United Kingdom</label>
<label><strong>Group (Of Breed):</strong> Terrier</label>
<label><strong>Description:</strong> The Airedale is the largest of the Terriers originating in Britain. They weigh 25-30 kilograms (55-66 lb) and have a height at the withers of 58-61 centimetres (23-24 in) for dogs, with bitches slightly smaller.</label>
</p>
</div>
</div>

<div class="container" id="tabs">
<ul class="tabs">
    <li class="active"><a href="#tabs-1">OVERVIEW</a></li>
      <li><a href="#tabs-2">POPULARITY</a></li>
       <li ><a href="#tabs-3">CHARACTERISTICS</a></li>
       <li ><a href="#tabs-4">COMPETITIVE REGISTRATION</a></li>
       <li ><a href="#tabs-5">REVIEWS</a></li>
      </ul>
  <div class="tab_container">
    <?
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' ORDER BY att_id ASC");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id1[]=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
}
?>
		
  <div id="tabs-1" class="tab_content">
  <div class="be_detailTxt">
  <h2>Overview</h2>
  <? foreach($att_id1 as $att_id){
  	if($att_id=='39' ||$att_id=='38' || $att_id=='40' || $att_id=='47' || $att_id=='52' || $att_id=='34' || $att_id=='17' || $att_id=='18' || $att_id=='27' || 
	$att_id=='9' || $att_id=='5' || $att_id=='3' || $att_id=='4' ){
		$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	
	
	?>
                 <? 
				$qRATING = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
								
								if($att_id <= '25') {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=$qRATING['value']/2;
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="onestar";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="twostar";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="threestar";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="fourstar";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="fivestar";}
								
								?> 
  <p><strong><?=$attname['traits']." : "; ?></strong></p>
  <p> <div id="rateIt" >
                                
     <div id="rateIt" class="dn" itemprop="aggregateRating"   itemscope itemtype="http://schema.org/AggregateRating">
    <span itemprop="ratingValue"><?=$finalratng?></span>
<?php /*?>    <span itemprop="bestRating"><?=$qbest;?></span><?php */?>
<?php /*?>     <span itemprop="ratingCount"><?=$qRATING['num_votes']?></span> <?php */?>
  </div>

                                <ul class="rating <?=$ratvalue;?>">
                                
                               
	<li class="one"><a title="1 Star">1</a></li>
	<li class="two"><a title="2 Stars">2</a></li>
	<li class="three"><a title="3 Stars">3</a></li>
	<li class="four"><a title="4 Stars">4</a></li>
	<li class="five"><a title="5 Stars">5</a></li>
</ul></div></p><br />
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?> <p><strong><?=$attname['traits']." : "; ?></strong></p>";<?
echo $valuename['value']."<br>";
}
}
}
?>
  </div>   
  </div>
    <div id="tabs-2" class="tab_content" style="display:none">
  <div class="be_detailTxt">
  <h2>Popularity</h2>
  <h3>AIREDALE TERRIER DETAILS</h3>
  <p><strong>Dog Size and Average Life Expectancy</strong></p>
  <p>The Airedale Terrier has a minimum height of 23 inches and a maximum height of 24 inches, which is slightly taller than the average for all Dog Breeds. The Airedale Terrier tends to weigh between 55 pounds and 66 pounds and is expected to live between 10 years and 11.5 years, which is far less than the average for all Dog Breeds. </p><br />

  <p><strong>Behavior</strong></p>
  <p>The Airedale Terrier has been known to be Aggressive, Alert, Independent, Intelligent and Loyal. The Airedale Terrier is good with children. </p><br />

  </div>  
  </div>
    <div id="tabs-3" class="tab_content" style="display:none">
 <div class="be_detailTxt">
  <h2>Characteristics</h2>
  <h3>AIREDALE TERRIER DETAILS</h3>
  <p><strong>Dog Size and Average Life Expectancy</strong></p>
  <p>The Airedale Terrier has a minimum height of 23 inches and a maximum height of 24 inches, which is slightly taller than the average for all Dog Breeds. The Airedale Terrier tends to weigh between 55 pounds and 66 pounds and is expected to live between 10 years and 11.5 years, which is far less than the average for all Dog Breeds. </p><br />

  <p><strong>Behavior</strong></p>
  <p>The Airedale Terrier has been known to be Aggressive, Alert, Independent, Intelligent and Loyal. The Airedale Terrier is good with children. </p><br />

  </div>
  </div>
  <div id="tabs-4" class="tab_content" style="display:none">
  <div class="be_detailTxt">
  <h2>Competitive Registration</h2>
  <h3>AIREDALE TERRIER DETAILS</h3>
  <p><strong>Dog Size and Average Life Expectancy</strong></p>
  <p>The Airedale Terrier has a minimum height of 23 inches and a maximum height of 24 inches, which is slightly taller than the average for all Dog Breeds. The Airedale Terrier tends to weigh between 55 pounds and 66 pounds and is expected to live between 10 years and 11.5 years, which is far less than the average for all Dog Breeds. </p><br />

  <p><strong>Behavior</strong></p>
  <p>The Airedale Terrier has been known to be Aggressive, Alert, Independent, Intelligent and Loyal. The Airedale Terrier is good with children. </p><br />

  </div>  
  </div>
    <div id="tabs-5" class="tab_content" style="display:none">
 <div class="be_detailTxt">
  <h2>Review</h2>
  <h3>AIREDALE TERRIER DETAILS</h3>
  <p><strong>Dog Size and Average Life Expectancy</strong></p>
  <p>The Airedale Terrier has a minimum height of 23 inches and a maximum height of 24 inches, which is slightly taller than the average for all Dog Breeds. The Airedale Terrier tends to weigh between 55 pounds and 66 pounds and is expected to live between 10 years and 11.5 years, which is far less than the average for all Dog Breeds. </p><br />

  <p><strong>Behavior</strong></p>
  <p>The Airedale Terrier has been known to be Aggressive, Alert, Independent, Intelligent and Loyal. The Airedale Terrier is good with children. </p><br />

  </div>
  </div>
  </div>
      
</div>

<div class="be_linkBox">
<div class="be_ad_banner">
  <img src="images/thumb_img.jpg" width="270" height="256" alt="" /> 
</div>
<div class="be_ad_banner">
<h3>Related Breeds</h3>
<ul>
<li><a href="#">German Shepherd</a></li>
<li><a href="#">Labrador Retriever</a></li>
<li><a href="#">Beagle</a></li>
</ul>
</div>
<div class="be_ad_banner">
<h3>Related Topics</h3>
<ul>
<li><a href="#">German Shepherd</a></li>
<li><a href="#">Labrador Retriever</a></li>
<li><a href="#">Beagle</a></li>
</ul>
</div>
<div class="be_ad_banner">
<h3>Give your pet It's Social Identity</h3>
<ul>
<li><a href="#">Wag Club</a></li>
<li><a href="#">Add Your Dog</a></li>
</ul>
</div>
</div>

</div>