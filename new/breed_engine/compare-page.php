<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
if($userid=='jyoti05'){
 
  require_once($DOCUMENT_ROOT . '/new/breed_engine/compare-page-bootstrap.php');
  exit;
  }
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$breednice=str_replace("-compare","",$section[0]);
$selbrd=explode("-vs-",$breednice);
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds Comparison';
$ant_page = '';
$ant_category =ucwords(str_replace("-"," ",$breednice));

$countBreedCompare = explode("-", $section[0]);
$countVS=0;
foreach($countBreedCompare as $getWordVS){
	//echo $getWordVS."<br/>";
	if($getWordVS=='vs'){
		$countVS++;	
	}	
}
	
foreach($selbrd as $selbrd1){
	$seletPupp12=mysql_query("SELECT breed_name FROM dog_breeds WHERE breed_engine='1' AND nicename='$selbrd1'");
	$rowTotal12 = mysql_num_rows($seletPupp12);
	if($rowTotal12==0){
		header("HTTP/1.0 404 Not Found");
		require_once($DOCUMENT_ROOT.'/404.php');
		die(mysql_error());
		exit();
	}
	$breednames=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$selbrd1'");
	$titbrd.=$breednames['breed_name']." vs ";
}
$pos = strrpos($titbrd, " vs ");
$subject = substr_replace($titbrd,"", $pos, strlen(" vs "));
$subject1=str_replace(" vs "," and ",$subject);

function genratecurl($breednice){
	$numbers = (explode("-vs-",$breednice));
	sort($numbers);
	$arrlength = count($numbers);
	for($x = 0; $x <  $arrlength; $x++) {
    	$rrt[]=$numbers[$x];
	}
	$redt  =    implode("-vs-",$rrt);
	if($breednice!=$redt){
		$redt=$redt."-compare";
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Location: /$redt/" );
		exit();
	}
}

genratecurl($breednice);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/new/breed_engine/css/be_style.css" />
<link rel="canonical" href="https://www.dogspot.in/<?=$section[0]?>/" />
<link rel="amphtml" href="https://www.dogspot.in/amp/<?=$section[0]?>/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/<?=$section[0]?>/" >
<title>Compare <?=$subject ?> | Difference Between <?=$subject1;?></title>
<meta name="keywords" content="Compare <?=$subject ?> , Compare <?=$subject1;?>, Difference Between <?=$subject1;?>" />
<meta name="description" content="Compare <?=$subject ?> Dog Breed and find features which are most important for you and which is the best or Suitable <?=$subject1;?> at DogSpot.in" />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.dogspot.in/<?=$section[0]?>/" /> 
<meta property="og:title" content="Comparison of <?=$subject ?> | DogSpot.in" /> 
<meta property="og:description" content="Compare the features of these dog Breeds and find the best suited for your home" />

<? $imm = $DOCUMENT_ROOT . "/new/pix/".$section[0]."jpg";
if(file_exists($imm)){?>
<meta property="og:image" content="https://www.dogspot.in/new/pix/<?=$section[0]?>.jpg" />
 <? }else{?>
 <meta property="og:image" content="https://www.dogspot.in/new/breed_engine/images/compare-dogbreeds.jpg" />
 <? }?>
<?php if($countVS > 1){ ?>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<?php }?>
<meta name="twitter:card" content="photo">
<meta name="twitter:site" content="@indogspot">
<meta name="twitter:url" content="https://www.dogspot.in/<?=$subject ?>/">
<meta name="twitter:title" content="Comparison of <?=$subject ?> | DogSpot.in">
<meta name="twitter:description" content="Compare the features of these dog Breeds and find the best suited for your home">
<meta name="twitter:image" content="https://www.dogspot.in/new/common/images/logo-300x300.jpg">
<meta name="twitter:image:width" content="610">
<meta name="twitter:image:height" content="610">

<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
//require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>

<style>
#rateIt {
margin-left: 55px;
}
</style>
<style>
ul.rating li.one a:hover,ul.rating li.two a:hover ,ul.rating li.three a:hover ,ul.rating li.four a:hover ,ul.rating li.five a:hover  { display:none;}	
</style>
<link type="text/css" rel="stylesheet" 
href="https://www.dogspot.in/new/articles/css/font-awesome.min.css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script defer="defer" type="text/javascript" src="/jquery/jquery.validate.min-1.9.js"></script>
<script type="text/javascript" src="/js/shaajax.min.2.1.js"></script>
<script type="text/javascript" src="/js/shaajax.multiple.load.min-1.1.js"></script>
<script type="text/javascript" src="/shapopup/shapopup.min.js"></script>
<script type="text/javascript" src="/jqzoom_ev-2.3/js/jquery.jqzoom-core.min.js"></script>
<script type="text/javascript" src="/new/js/application.min.js"></script>
<script type="text/javascript" src="/new/slide/jquery.jcarousel.pack.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js" ></script>
<script type="text/javascript" src="/new/js/jquery/jquery-ui.js" ></script>
<script type="text/javascript"  src="/shop-slider/js/jquery.tabSlideOut.js"></script>
<script type='text/javascript'  src='/ajax-autocomplete/jquery.autocomplete.js'></script>

<script type="text/javascript"  defer="defer" src="/social-share/social-share.min.js"></script>

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php');?>
<link href="/new/css/toll-tip.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/new/css/jquery-ui.css" />


<style>
	#top_name {
  		position: relative;
		/*  top: 0;
		   	padding-top: 19px;
		 	margin-top: 20px;
		  	border-top: 1px solid purple;
		*/
		padding-top: 0px;
		margin-top:5px;
		margin-left: 0px;
		z-index: 9999;
		display:none
	}
	#top_name.fixed {
		position: fixed;
		top: -5px;
		float:left;
	}
</style>
<style>
.ds_social_cont{width:100%; float:left; width:100%;}
.ds_social_buttons{word-wrap:normal; clear: both; margin: 0; padding: 0; font: 14px/23px sans-serif; color: #222222;}
.ds_social_buttons ul{padding:0;margin:0;list-style: none; border:1px solid #f00;}
.ds_social_buttons .ds_social_list,.ds_social_list a, .ds_social_icons { display: inline-block; vertical-align:middle;}
.ds_social_list{position: relative; margin: 2px 8px 2px 0px; list-style: none; }
.ds_social_buttons .ds_social_list a {color: #fff; text-decoration: none; font-size: 12px; padding:10px 20px;}
.ds_social_buttons .ds_twitter_share a{background-color: #1ab2e8;}
.ds_social_buttons .ds_google_share a{background-color: #dc4a38;}
.ds_social_buttons .ds_pinterest_share a{background-color: #fd1d1f;}
.ds_social_buttons .ds_facebook_share a{background-color: #5f82cd; }
.ds_social_buttons .ds_linkedin_share a{background-color: #0077b5;}
.fa{ font-size:16px !important;}
.ds_social_buttons .ds_social_list a:hover{ background:#8dc059;}
.ds_social_number{color: #333; font-size:14px; margin-top:2px; line-height: 16px;}
.ds_social_number_cont_num{font-size:20px;}
</style>
<script>

$(function () {
  
  var msie6 = $.browser == 'msie' && $.browser.version < 7;
  
  if (!msie6) {
    var top = $('#top_name').offset().top - parseFloat($('#top_name').css('margin-top').replace(/auto/, 0));
    $(window).scroll(function (event) {
      // what the y position of the scroll is
      var y = $(this).scrollTop();
	  if(y<=200){
	  y=-5;
	  }
      // whether that's below the form
      if (y > top) {
        // if so, ad the fixed class
		$('#top_name').css("display","block");
        $('#top_name').addClass('fixed');
      } 
	  if (y == top)
	   {
        // otherwise remove it
        $('#top_name').removeClass('fixed');
		$('#top_name').css("display","none");
      }
    });
  }  
});

</script>
<script type="text/javascript">
function addbreedchange(vaa){
	var prvbrdnn1=vaa.split("|");
	var breed=prvbrdnn1[0];
	var nice=prvbrdnn1[1];
	
	window.history.pushState('', '', 'https://www.dogspot.in/'+nice+'-compare/');
	 var pushss='https://www.dogspot.in/'+nice+'-compare/';
	ShaAjaxJquary('/new/breed_engine/compare-page-ajax.php?breeds='+breed+'&pushss='+pushss+'', '#ajaxbreed', '', '', 'GET', '#ajaxbreed_load', 'Loading...', 'REP');
	}
function addbreed(dropid12){
	 var drop_value=document.getElementById(dropid12).value;
	 var de_value=document.getElementById("p_breed").value;
	 var de_value12=de_value.split(",");
	 var comm='';
	 for(var c=0;c<=de_value12.length;c++){
		 if(de_value12[c]!='undefined'){
		 comm=de_value12[c]+'-vs-'+ comm ;}
		 }
	//undefined-vs--vs-labrador-retriever-vs-golden-retriever-vs-
	 //undefined-vs--vs-akita-vs-labrador-retriever-vs-golden-retriever-vs-
	 var comm = comm.replace('undefined-vs--vs-','');
	  //alert(comm);
	 var drop_value=document.getElementById(dropid12).value;
	 var prvbrdnn=drop_value.split("|");
	 var dropid=prvbrdnn[0];
	 var nicename=prvbrdnn[1];
	 //alert(comm+nicename);
	 //alert(dropid12);
	var totbrd=document.getElementById("prv_breed").value;
    var prvbrd=totbrd.split(",");
	var countbreeds=prvbrd.length;
	if(countbreeds > 4){

	$('#messagebox1').css("display","block");
	$('#messagebox1').text("You can add maximum of four breeds");
	}else{
	$('#messagebox1').css("display","none");
	$("#compare_none").css("display","none");
	$("#ajaxbreed").css("display","block");
	document.getElementById("ajaxbreed").style.display="block";
	var breed_id=document.getElementById("prv_breed").value + ',' + dropid;
	window.history.pushState('', '', 'https://www.dogspot.in/'+comm+nicename+'-compare/');
	 var pushss='https://www.dogspot.in/'+comm+nicename+'-compare/';
	ShaAjaxJquary('/new/breed_engine/compare-page-ajax.php?breeds='+breed_id+'&pushss='+pushss+'', '#ajaxbreed', '', '', 'GET', '#ajaxbreed_load', 'Loading...', 'REP');
	}
}

function removebreed(rmvid){//bea
	 var prvbrdnn=rmvid.split("|");
	 var dropid=prvbrdnn[0];
	 var nicename=prvbrdnn[1];
//alert(nicename);
	 var breed_id1=document.getElementById("prv_breed").value;
//alert(breed_id1);
	 var removebrdid=","+dropid;
	 var res = breed_id1.replace(removebrdid,'');
	 var de_value=document.getElementById("p_breed").value;
	 var de_value12=de_value.split(",");
	 var comm='';
	 for(var c=0;c<=de_value12.length;c++){
		 if(de_value12[c]!='undefined'){
			 if(de_value12[c]!=nicename){
		 comm=de_value12[c]+'-vs-'+ comm ;}}
		 }
	//beagle-vs-akita-vs-labrador-retriever-vs-
	 //beagle-vs-labrador-retriever-vs-akita-vs-
	 var comm = comm.replace('undefined-vs--vs-','');
	 var dd=comm.lastIndexOf('-vs-');
	 var ss=comm.substring(0,dd);
	 var pushss='https://www.dogspot.in/'+ss+'-compare/';
	 //alert(ss);
if(res!='1'){
	window.history.pushState('', '', 'https://www.dogspot.in/'+ss+'-compare/');
	//window.location.href = 'https://www.dogspot.in/'+res+'/';
ShaAjaxJquary('/new/breed_engine/compare-page-ajax.php?breeds='+res+'&pushss='+pushss+'', '#ajaxbreed', '', '', 'GET', '#ajaxbreed_load', 'Loading...', 'REP');
}else{
//	window.location.href = 'https://www.dogspot.in/<?=$breednice?>-vs-'+document.getElementById(dropid).value+'-compare/';
ShaAjaxJquary('/new/breed_engine/compare-page-ajax-none.php?breeds='+res+'&pushss='+pushss+'', '#ajaxbreed', '', '', 'GET', '#ajaxbreed_load', 'Loading...', 'REP');
}
}
</script>
<div id="ajaxbreed">
<div id="compare_none" style="display:none"></div>
 <!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980" itemscope itemtype="http://schema.org/Breadcrumb">
                
                    <div class="fl" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#666;" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a href="/" itemprop="item">
                    <span itemprop="name">Home</span></a>
                    <meta itemprop="position" content="1" /> </span>
                    / 
                    
<? 
	$brdcmcount='0';
	$breeds_compare_brdcrm=explode(",","$breeds");
	foreach($breeds_compare_brdcrm as $brd_brdcm){
		if($brd_brdcm!='1'){
			$breedcmid1=$brd_brdcm;
			$selectdatacm=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedcmid1'");
			while($selectdatacm1=mysql_fetch_array($selectdatacm)){
				$brdnamecm=$selectdatacm1['be_name'];$nname=$selectdatacm1['nicename'];
				$numbers = (explode("-vs-",$breednice));
				sort($numbers);
				$arrlength = count($numbers);
				if($brdcmcount==1){
					echo " / ";
				}
				if($brdcmcount==2 && $arrlength=='3'){
					echo " & ";
				}
				if($brdcmcount==2 && $arrlength!='3'){
					echo " , ";
				}
				if($brdcmcount==3){
					echo " & ";
				}
				$nicenames.=$nname."";
			?> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <? 
				if($brdcmcount==0){?>
                    <a href="/<?=$nicenames?>/" itemprop="item">
                    <span itemprop="name">
				<? echo ""; }else{?>
                 <span itemprop="name" class="brd_font_bold"><? } echo $brdnamecm; ?></span>
				<? if($brdcmcount==0){ ?></a>
				<meta itemprop="position" content="2" /></span> <? }else{?><meta itemprop="position" content="2" /></span> 
				<? }?>
				
	<? $brdcmcount++; } } } ?> 
  	<input type="hidden" id="p_breed" name="p_breed" value="<?=$nicenames ?>" />                   
</div>
                 
                
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb --> 

<? 
$breeds_compare_count=explode(",","$breeds");
$countbreed='0';
foreach($breeds_compare_count as $breeds_compare_count1){
	$countbreed++;
}
if($countbreed <= 5){
?>  
<div class="cont980">
<?

require_once($DOCUMENT_ROOT . '/banner1.php');
$displaypinBanglore=1;
			   $displaypinGUrgaon=1;
     if($userid !='Guest'){
// $rowusers1=query_execute_row("SELECT count(*) as jee FROM users WHERE userid='$userid' AND (city='Banglore' OR city='Bangalore')");
   $rowItemw1=query_execute("SELECT address_zip FROM shop_order_address WHERE userid='$userid' AND address_type_id='1' order by order_address_id desc");
   $displaypinGUrgaon=1;
   while($rows1 = mysql_fetch_array($rowItemw1)){
   	
   if($rows1['address_zip']!='' || $rows1['address_zip']!='0'){
   	$zipp1=$rows1['address_zip'];
   $raddressBanglore = query_execute_row("SELECT count(*) as coo FROM shop_pincode_codCheck WHERE pincode='$zipp1'   AND type='simple'");
  if($raddressBanglore['coo']!=0){
	 // echo 'hiu';
		$displaypinGUrgaon='0';break;
		}
   	}
   }
   }
   
 if($displaypinBanglore==0 || $sessionUserCity=='Bangalore' || $sessionUserCity=='Bengaluru'){
	 //   addbannerallBangsite();
   }elseif($displaypinGUrgaon==0 || $sessionUserCity=='Gurgaon')
	{
	//addbannerallGursite('700', '131', '1');	
	}else{
//addbannerallsite('972', '90', '1');
   }?>
<h1 class="be_comheading"><span style="color:#668000">Compare </span><? 
$brdcmcount='0';
$breeds_compare_brdcrm=explode(",","$breeds");
foreach($breeds_compare_brdcrm as $brd_brdcm){
if($brd_brdcm!='1'){
$breedcmid1=$brd_brdcm;
$selectdatacm=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedcmid1'");
while($selectdatacm1=mysql_fetch_array($selectdatacm)){
	$brdnamecm=$selectdatacm1['be_name'];
	if($brdcmcount>0){
	echo " vs ";
	}
	echo $brdnamecm;
	$brdcmcount++;
}
}
}
?> </h1>
<div class="be_compareBHeading">
 <ul id="top_name" style="display:none">
<?
$count_tit=0;
$countbreeds_tit=0;
$breeds_compare1_tit=explode(",","$breeds");
foreach($breeds_compare1_tit as $brd1_tit){
if($brd1_tit!='1'){
	$countbreeds_tit++;
$breedid1_tit=$brd1_tit;
$selectdata_tit=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedid1_tit'");
while($selectdata1_tit=mysql_fetch_array($selectdata_tit)){
	$brdname_tit=$selectdata1_tit['breed_name'];
	$imgname_tit=$selectdata1_tit['image_name'];
	$be_name_tit=$selectdata1_tit['be_name'];
	$nicename_tit=$selectdata1_tit['nicename'];
	$imgname=$selectdata1_tit['image_name'];
	}
if($count_tit=='0'){
?>
<li style="width:167px;">
<label>You can compare only four breeds at a given time</label>
</li>
<? } 
	if($imgname){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$imgname;
		$imageURL='/new/breed_engine/images/dog_images/50-50-'.$imgname;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'50','','ratiowh');
?>

<li><div class="be_breedthumbimg"><a href="/<?=$nicename_tit; ?>/"><img src="/new/breed_engine/images/dog_images/50-50-<?= $imgname ?>" 
alt="<?=$brdname_tit?>"  title="<?=$brdname_tit?>" width="50">
 <label><?=$be_name_tit; ?></label></a></div>
</li>
<?
$count_tit='1';
}
}
if($countbreeds_tit !='4'){
$dropvalue_tit=1;
while($countbreeds_tit < 4){ 
?>
<li>

</li>
<? 
$countbreeds_tit++;
$dropvalue_tit++;
}
}
?>
</ul>
</div>
<div class="be_breedBox">

<?php /*?>            <select id="breed_id_add" class="be_selectBox" name="breed_id_add" onchange="addbreed();">
      <option value='0'>Select Breed</option>
        <?php
			$query_domain = query_execute("SELECT * FROM dog_breeds WHERE breed_engine='1'");
		
			while($query_domain1 = mysql_fetch_array($query_domain)){
			$breed_id12 = $query_domain1["breed_id"];
			$breed_name = $query_domain1["breed_name"];
			$be_name=$query_domain1["breed_name"];
			  print "<option value='$breed_id12'";
			  if($breed_id12 == $breed_id){   echo "selected=='selected'";  }  print ">$breed_name</option>";
			 }
		  ?>
    </select><?php */?>
    <div id="ajaxbreed_load"></div>

<?php /*?><div class="be_back">
<div>See all dog breeds</div>
<div class="be_arw"><img src="images/arw_blck.png" width="15" height="10" alt="" /></div>
<div><a href="/new/breed_engine/"><img src="images/click.png" width="55" height="55" alt="" /></a></div>
</div><?php */?>
</div>
 <label id="messagebox1" class="be_msg" style="display:none"></label>
<div class="be_compare-box">
<ul>
<?
$count=0;
$countbreeds=0;
$breeds_compare1=explode(",","$breeds");
foreach($breeds_compare1 as $brd1){
if($brd1!='1'){
	$countbreeds++;
$breedid1=$brd1;
$selectdata=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedid1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$brdname=$selectdata1['breed_name'];
	$imgname=$selectdata1['image_name'];
	$be_name=$selectdata1['be_name'];
	$nicename=$selectdata1['nicename'];
}
if($count=='0'){
	
?>
<li style="width:167px;">
<label>You can compare only four breeds at a given time</label>
</li>
<? } ?>
<li>
 <div class="be_cls"><img src="/new/breed_engine/images/cls.png" onclick="removebreed('<?=$breedid1."|".$nicename;?>')" style="cursor:pointer" width="26" height="26" alt="<?=$brdname?>" title="<?=$brdname?>" /></div>
<div class="be_breedname"><h2 style="font-size:14px;"><?=$be_name; ?></h2></div>
<div class="be_breedimg">
<? if($countbreed==3){?>
<a href="/<?=$nicename; ?>/">
<? }?>
<img src="/new/breed_engine/images/dog_images/<?=$imgname ?>" alt="<?=$brdname?>" title="<?=$brdname?>" height="200" width="190" />
<? if($countbreed==3){?></a><? }?></div>
</li>
<?
$count='1';
}
}
if($countbreeds !='4'){
$dropvalue=1;
while($countbreeds < 4){ 
?>
<li>
<label>Add a breed to compare</label>
      <select id="breed_id_add<?=$dropvalue?>" class="be_selectBox" name="breed_id_add" onchange="addbreed('breed_id_add<?=$dropvalue?>');">
      <option value='0'>Select Breed</option>
        <?php
			$query_domain = query_execute("SELECT * FROM dog_breeds WHERE breed_engine='1' ORDER BY be_name ASC");
		
			while($query_domain1 = mysql_fetch_array($query_domain)){
			$breed_id12 = $query_domain1["breed_id"];
			$breed_name = $query_domain1["breed_name"];
			$be_name=$query_domain1["be_name"];
			$brdnice=$query_domain1["nicename"];
			$pos1 = strpos($breeds,$breed_id12);
			if($pos1=='0'){
			  print "<option value='$breed_id12|$brdnice'";
			  if($breed_id12 == $breed_id){   echo "selected=='selected'";  }  print ">$be_name</option>";
			 }
			}
		  ?>
    </select>
</li>
<? 
$countbreeds++;
$dropvalue++;
}
}
?>
</ul>
</div>
<!--------------------------------------------------------------- 1st heading --------------------------------------------------->
<h3 class="be_comhead">Breed Info</h3>
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("37","27","41","3","1");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<li class="be_detailhead"><?=$attname['traits'] ?><? if($att_id=='27'){ echo "<a style='cursor:pointer' id='This is an average cost of getting this breed of puppy home, it may vary from breeder to breeder. though, if you want then you can always adopt a dog.' title='Average for the breed' class='tooltip'>*</a>";} ?></li>
<? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<li>
<div class="be_breeddata">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="fl"><?=$attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
</div>
</li>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<li>
<div class="be_breeddata"><?= $valuename['value'];
   if($att_id=='27'){ ?>
  <? if($valuename['value']=='Economical'){ $tit_id='(upto Rs 5,000)';} else if($valuename['value']=='Pocket Friendly'){ $tit_id='(Rs 10,000 - Rs 20,000)';} else if
($valuename['value']=='Expensive'){$tit_id='(Rs 25,000 - Rs 30,000)';} else{$tit_id='(Rs 35,000 - Rs 50,000)';}?><a style="cursor:pointer" class='tooltip' id="<?=$tit_id?> approximate cost,which can very according to the puppy and the location." title="<?=$tit_id?> approximate cost,which can very according to the puppy and the location.">*</a> <? }
 ?></div>
</li>
<? 
}
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>

</ul>
</div>

<!--------------------------------------------------------------- 2nd heading --------------------------------------------------->
<h3 class="be_comhead">Maintenance & Effort</h3>
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("22","28","30","32","34","1000","1001","15","7","16","17");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
if($att_id1!='1000' && $att_id1!='1001'){
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
}
if($att_id1=='1000'){
$selectdata=mysql_query("SELECT monthly_keeping_cost_premium FROM dog_breeds WHERE breed_id='$breedid'");
}
if($att_id1=='1001'){
$selectdata=mysql_query("SELECT monthly_keeping_cost_standard FROM dog_breeds WHERE breed_id='$breedid'");
}
while($selectdata1=mysql_fetch_array($selectdata)){
	if($att_id1!='1000' && $att_id1!='1001'){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
	}
	if($att_id1=='1000'){
	$att_value_pre=$selectdata1['monthly_keeping_cost_premium'];
	}
	if($att_id1=='1001'){
	$att_value_std=$selectdata1['monthly_keeping_cost_standard'];
	}
if($att_id1!='1000' && $att_id1!='1001'){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
}
if($count=='0'){
?>
<? if($att_id1!='1000' && $att_id1!='1001'){ ?>
<li class="be_detailhead"><?=$attname['traits'] ?><? if($att_id=='32' || $att_id=='11'){ echo "<a style='cursor:pointer' id='Average for the breed' title='Average for the breed' class='tooltip'>*</a>";} ?></li>
<? }if($att_id1=='1000'){ ?>
<li class="be_detailhead">Monthly keeping cost (Premium)<a style='cursor:pointer' class='tooltip' id='This is an average monthly expense for keeping this breed. This includes an approximation cost of premium dry dog food, grooming expenses and the vet bills incurred in a month.' title='Average for the breed'>*</a></li>
<? }if($att_id1=='1001'){ ?>
<li class="be_detailhead">Monthly keeping cost (Standard)<a style='cursor:pointer' class='tooltip' id='This is an average monthly expense for keeping this breed. This includes an approximation cost of regular dry dog food, grooming expenses and the vet bills incurred in a month.' title='Average for the breed'>*</a></li>
<? }
 }
								if(($att_id <= '20'  || $att_id == '43' || $att_id == '44' ) && ($att_id1 != '1000' && $att_id1 != '1001')) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<li>
<div class="be_breeddata">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="fl"><?=$attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
</div>
</li>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<li>
<div class="be_breeddata"><? if($att_id1!='1000' && $att_id1!='1001'){ ?><?= $valuename['value'] ?><? }if($att_id1=='1000'){
echo "<img src='/new/breed_engine/images/be_rupee.png' alt='Rs icons' title='Rs icons' width='10' height='10' />". number_format($att_value_pre)."";
}if($att_id1=='1001'){
echo "<img src='/new/breed_engine/images/be_rupee.png' alt='Rs icons' title='Rs icons' width='10' height='10' />".number_format($att_value_std)."";
} ?>
</div>
</li>
<? 
}
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>

</ul>
</div>

<!--------------------------------------------------------------- 3rd heading --------------------------------------------------->
<h3 class="be_comhead">Hair & Coat</h3>
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("24","23","36","46","9","10");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<li class="be_detailhead"><?=$attname['traits'] ?></li>
<? }
								if($att_id <= '20'  || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<li>
<div class="be_breeddata">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div> 
    <span class="fl"><?=$attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
</div>
</li>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<li>
<div class="be_breeddata"><?= $valuename['value'] ?></div>
</li>
<? 
}
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>

</ul>
</div>

<!--------------------------------------------------------------- 4th heading --------------------------------------------------->
<h3 class="be_comhead">Health</h3>
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("40","47");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<li class="be_detailhead"><?=$attname['traits'] ?></li>
<? }
								if($att_id <= '20'  || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<li>
<div class="be_breeddata">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?=$attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
</div>
</li>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<li>
<div class="be_breeddata"><?= $valuename['value'] ?></div>
</li>
<? 
}
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>

</ul>
</div>

<!--------------------------------------------------------------- 5th heading --------------------------------------------------->
<h3 class="be_comhead">Behavior</h3>
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("21","36","33","1","2","43","4","5","18","19","44","12","13");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<li class="be_detailhead"><?=$attname['traits'] ?></li>
<? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' || $att_id == '33') {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<li>
<div class="be_breeddata">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="fl"><?=$attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
</div>
</li>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<li>
<div class="be_breeddata"><?= $valuename['value'] ?></div>
</li>
<? 
}
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>

</ul>
</div>
<!--------------------------------------------------------------- 6th heading --------------------------------------------------->
<h3 class="be_comhead">Breeding</h3>
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("26","38");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<li class="be_detailhead"><?=$attname['traits'] ?></li>
<? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<li>
<div class="be_breeddata">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="fl"><?=$attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
</div>
</li>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<li>
<div class="be_breeddata"><?= $valuename['value'] ?></div>
</li>
<? 
}
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>

</ul>
</div>
<!--------------------------------------------------------------- 7th heading --------------------------------------------------->
<h3 class="be_comhead">Appearance</h3>
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("29","31","42","48","23","8","6","1234");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
if($att_id1!="1234"){
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
}
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<li class="be_detailhead"><?=$attname['traits'] ?></li>
<? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<li>
<div class="be_breeddata">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  
    <span class="fl"><?=$attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>

</div>
</li>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<li>
<div class="be_breeddata"><?= $valuename['value'] ?></div>
</li>
<? }
$count='1';
}
}
 }
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>
</ul>
</div>

<!--------------------------------------------------------------------Know More------------------------------------------------------------------->
<div class="be_compare-box be_detail">
<ul>
<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("1234");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT nicename FROM dog_breeds WHERE breed_id='$breedid'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$nicename1=$selectdata1['nicename'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
if($count=='0'){
?>
<li class="be_detailhead"></li>
<? }?>
<?php /*?><li>
<div class="">
 <div class="be_knw_more"> <a href="/<?=$nicename1?>/">Know More</a></div>

</div></li><?php */?>
<? 
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be_compare-box be_detail">
<ul>
<?
} ?>

</ul>
</div>
<div class="cont980">
<?
 $titlewag="Comparison of $subject | DogSpot.in";
 $summarywag="Compare the features of these dog Breeds and find the best suited for your home";
 $urlwag="https://www.dogspot.in/".$section[0];
 $imagewag="https://www.dogspot.in/new/breed_engine/images/logo-300x300.jpg";
?>

<input type="hidden" id="prv_breed" name="prv_breed" value="<?=$breeds ?>" />

</div>
<br />
<div class="cont980">
<div class="be_share" style="margin-top:0px;">
<form action="" method="post" name="formcomnt" id="formcomnt">
	<div class="be_shareCon"><img src="/new/breed_engine/images/like.jpg" width="231" height="110" alt=""/></div>
</form>


<!--<ul>
<li><div id="fb-root"></div>
<a onClick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[title]=<?=$titlewag;?>&amp;p[summary]=<?=$summarywag;?>&amp;p[url]=<?=$urlwag; ?>&amp;p[images][0]=<?=$imagewag;?>','sharer','toolbar=0,status=0,width=548,height=325,top=100,left=250');" href="javascript: void(0)"><img src="/new/breed_engine/images/facebook.png" width="50" height="50" alt="" /></a></li>
<li><a onClick="window.open('https://www.linkedin.com/cws/share?url=<?=$urlwag;?>&original_referer=<?=$urlwag; ?>&token=&isFramed=false&lang=en_US&_ts=1396479204932.1626','sharer','toolbar=0,status=0,width=548,height=325,top=100,left=250');" href="javascript: void(0)"><img src="/new/breed_engine/images/linkedin.png" width="50" height="50" alt="" /></a></li>
<li>
<? $urlwag=str_replace("/","%2F",$urlwag); $urlwag=str_replace(":","%3A",$urlwag); ?>
<a onClick="window.open('https://twitter.com/intent/tweet?original_referer=<?=$urlwag;?>&text=<?="Compare ".trim($subject);?>&tw_p=tweetbutton&url=<?=$urlwag; ?>&via=indogspot','sharer','toolbar=0,status=0,width=548,height=325,top=100,left=250');" href="javascript: void(0)"><img src="/new/breed_engine/images/twitter.png" width="50" height="50" alt="" /></a></li>
<li><a href="https://plus.google.com/share?url=<?=$urlwag;?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=325,width=548');return false;"><img src="/new/breed_engine/images/google.png" width="50" height="50" alt="" /></a></li>
</ul>-->
</div>
<div style="padding:7px 0 3px 0; border-top:1px solid #f2f2f2;  margin-top:15px;">
              	<div class="fl" style="margin-top:10px; margin-bottom:15px;">
                <?
					$sharecheck=1;
					$surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
					include($DOCUMENT_ROOT."/Adoption1/social1_article.php"); 
				?>
                <div id="fb-root"></div>
              </div>
            </div>
</div>
</div>
</div>




<? }else{ ?>
  <div class="cont980"><label id="messagebox5" class="be_msg">You cannot compare more then 4 breeds</label></div>
<? } ?>
<!-------------------------------------------------Top Breed Comparison--------------------------------------------------------->

<div class="cont980">
<div class="fl breedComName_be">
<h3>Top Breed Comparison</h3>
<div class="be_comBrdNames fl">
<ul>
<li><a href="/german-shepherd-dog-alsatian-vs-labrador-retriever-compare/" >German Shepherd Dog (Alsatian) vs Labrador Retriever</a></li>
<li><a href="/pit-bull-terrier-american-vs-rottweiler-compare/" >Rottweiler vs Pit Bull Terrier (American)</a></li>
<li><a href="/rottweiler-vs-tibetan-mastiff-compare/" >Tibetan Mastiff vs Rottweiler</a></li>
<li><a href="/golden-retriever-vs-labrador-retriever-compare/" >Golden Retriever vs Labrador Retriever</a></li>
</ul>
</div>
<div class="be_comBrdNames fl">
<ul>
<li><a href="/german-shepherd-dog-alsatian-vs-great-dane-compare/" >Great Dane vs German Shepherd Dog (Alsatian)</a></li>
<li><a href="/boxer-vs-dobermann-compare/" >Dobermann vs Boxer</a></li>
<li><a href="/beagle-vs-pug-compare/" >Pug vs Beagle</a></li>
<li><a href="/beagle-vs-labrador-retriever-compare/" >Beagle vs Labrador Retriever</a></li>
</ul>
</div>
<div class="be_comBrdNames fl">
<ul>
<li><a href="/pomeranian-vs-pug-compare/" >Pomeranian vs Pug</a></li>
<li><a href="/pug-vs-shih-tzu-compare/" >Shih Tzu vs Pug</a></li>
<li><a href="/dalmatian-vs-labrador-retriever-compare/" >Dalmatian vs Labrador Retriever</a></li>
<li><a href="/dachshund-standard-smooth-haired-vs-pug-compare/" >Dachshund Standard ( Smooth Haired) vs Pug</a></li>
</ul>
</div>
<div class="be_comBrdNames fl">
<ul>
<li><a href="/stbernard-vs-tibetan-mastiff-compare/" >St.Bernard vs Tibetan Mastiff</a></li>
<li><a href="/afghan-hound-vs-grey-hound-compare/" >Afghan Hound vs Grey Hound</a></li>
<li><a href="/akita-vs-german-shepherd-dog-alsatian-compare/" >Akita vs German Shepherd Dog (Alsatian)</a></li>
</ul>
</div>
</div>
</div>
<!---------------------------------------------------------------Ends Here----------------------------------------------->
<script type="text/javascript">
jQuery(document).ready(function($){
	var valueurl="<?=$surl?>";
	ShaAjaxJquary("/share-update-des.php?surl="+valueurl+"", "#mis", '', 'formcomnt', 'GET', '#loading', '','REP');
});
</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php');?>