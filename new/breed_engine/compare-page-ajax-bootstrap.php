<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
$actual_link = "https://$_SERVER[HTTP_HOST]";
$sitesection='dog-breeds';
?>

<div id="ajaxbreed">
<div id="compare_none" style="display:none"></div>

            <!-- breadcrumb --> 
            <div class="breadcrumbs">
            	 <div class="container">
                 <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
                
                    <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a href="/dog-breeds/" itemprop="item">
                    <span itemprop="name">Dog Breeds</span></a>
                    <meta itemprop="position" content="1" /> </span>
                    <span> / </span>
                     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a href="/dog-breeds-comparison/" itemprop="item">
                    <span itemprop="name">Comparison</span></a>
                    <meta itemprop="position" content="2" /> </span> <span> / </span>
                    <input type="hidden" id="prv_breed" name="prv_breed" value="<?=$breeds ?>" />
<? 

$brdcmcount='0';
$breeds_compare_brdcrm=explode(",","$breeds");
foreach($breeds_compare_brdcrm as $brd_brdcm){
if($brd_brdcm!='1'){
$breedcmid1=$brd_brdcm;
$selectdatacm=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedcmid1'");
while($selectdatacm1=mysql_fetch_array($selectdatacm)){
	$brdnamecm=$selectdatacm1['be_name'];
	$brdnamecm23=$selectdatacm1['be_name'];
	
	$nname=$selectdatacm1['nicename'];
	$numbers1 = (explode(",",$breeds));
    sort($numbers1);
    $arrlength1 = count($numbers1);
    if($brdcmcount==1){
	echo " <span> / </span> ";
	
	}if($brdcmcount==2 && $arrlength1=='4'){
	echo " & ";
	
	}if($brdcmcount==2 && $arrlength1!='4'){
	echo " , ";
	
	}
	if($brdcmcount==3){
	echo " & ";
	
	}
	$nicenames.=$nname.",";
	$nicenamesshare.=$nname."-vs-";
	$brdnamecmshare.= $brdnamecm23." vs ";
	?>
    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <? 
				if($brdcmcount==0){?>
                    <a href="/compare-<?=$nname?>-with-other-breeds/" itemprop="item">
                    <span itemprop="name">
				<? echo "Compare "; }else{?>
                 <span itemprop="name" class="active-bread"><? } echo $brdnamecm; ?></span>
				<? if($brdcmcount==0){ ?></a></span>
				<meta itemprop="position" content="3" /> <? }else{?><meta itemprop="position" content="3" /></span> <? }?>
				
<?
	$brdcmcount++;
}
}
}?> 
<input type="hidden" id="p_breed" name="p_breed" value="<?=$nicenames ?>" />

                     </div>
                 
                   
          

                    <div class="cb"></div>
                    
                    <?php /*?><div class="fr" style="width:80px; margin-top:-22px;">
                  	<script type="text/javascript" src="https://w.sharethis.com/widget/?tabs=web%2Cpost%2Cemail&amp;charset=utf-8&amp;style=default&amp;publisher=203f1564-d8e9-4d1a-a18a-e0a64020e484"></script>
                    </div><?php */?>
                    </div>
                     </div>
                 </div>

<? 
$breeds_compare_count=explode(",","$breeds");
$countbreed='0';
foreach($breeds_compare_count as $breeds_compare_count1){
	$countbreed++;
}
if($countbreed <= 5){
?>  
<section class="breeds-section more-breed-com breed-com">
  <div class="container">
   <h1><i style="color:#6c9d06">Compare </i><? 
$brdcmcount='0';
$breeds_compare_brdcrm=explode(",","$breeds");
foreach($breeds_compare_brdcrm as $brd_brdcm){
if($brd_brdcm!='1'){
$breedcmid1=$brd_brdcm;
$selectdatacm=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedcmid1'");
while($selectdatacm1=mysql_fetch_array($selectdatacm)){
	$brdnamecm=$selectdatacm1['be_name'];
	if($brdcmcount>0){
	echo " vs ";
	}
	echo $brdnamecm;
	$brdcmcount++;
}
}
}
?> </h1>
  

<div class="be-compare-fix-top">
 <div id="top_name" style="display:none">
 <?
$count_tit=0;
$countbreeds_tit=0;
$breeds_compare1_tit=explode(",","$breeds");
foreach($breeds_compare1_tit as $brd1_tit){
if($brd1_tit!='1'){
	$countbreeds_tit++;
$breedid1_tit=$brd1_tit;
$selectdata_tit=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedid1_tit'");
while($selectdata1_tit=mysql_fetch_array($selectdata_tit)){
	$brdname_tit=$selectdata1_tit['breed_name'];
	$imgname_tit=$selectdata1_tit['image_name'];
	$be_name_tit=$selectdata1_tit['be_name'];
	$nicename_tit=$selectdata1_tit['nicename'];
	$imgname=$selectdata1_tit['image_name'];
	}
if($count_tit=='0'){
?>
<div class="col-sm-6 col-sm-5cols">
<label>You can compare only four breeds at a given time</label>
</div>
<? } 
	if($imgname){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$imgname;
		$imageURL='/new/breed_engine/images/dog_images/50-50-'.$imgname;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'50','','ratiowh');
?>
<div class="col-sm-6 col-sm-5cols"><div class="be_breedthumbimg">
<a href="/<?=$nicename_tit; ?>/"><img src="/new/breed_engine/images/dog_images/50-50-<?= $imgname ?>" 
alt="<?=$brdname_tit?>"  title="<?=$brdname_tit?>" width="50">
 <label><?=$be_name_tit; ?></label></a></div>
</div>
<?
$count_tit='1';
}
}
if($countbreeds_tit !='4'){
$dropvalue_tit=1;
while($countbreeds_tit < 4){ 
?>
<div class="col-sm-6 col-sm-5cols">
</div>
<? 
$countbreeds_tit++;
$dropvalue_tit++;
}
}
?>
</div>
</div>
<div class="row">
<div id="ajaxbreed_load"></div>
  </div>
   <label id="messagebox1" class="be_msg" style="display:none"></label>
  <div class="be-compare-box">
  <?
$count=0;
$countbreeds=0;
$breeds_compare1=explode(",","$breeds");
foreach($breeds_compare1 as $brd1){
if($brd1!='1'){
	$countbreeds++;
$breedid1=$brd1;
$selectdata=mysql_query("SELECT * FROM dog_breeds WHERE breed_id='$breedid1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$brdname=$selectdata1['breed_name'];
	$imgname=$selectdata1['image_name'];
	$be_name=$selectdata1['be_name'];
	$nicename=$selectdata1['nicename'];
}
if($count=='0'){
?>
   <div class="col-sm-6 col-sm-5cols">
<div class="p-d-t"><label>You can compare only four breeds at a given time</label></div>		
 </div>
 <? } ?>
 
<div class="col-sm-6 col-sm-5cols">
 
<div class="be_breedname">
<div class="text-right close-box">
<a onclick="removebreed('<?=$breedid1."|".$nicename;?>')" alt="<?=$brdname?>" title="<?=$brdname?>" ><i class="fa fa-times-circle-o"></i></a>
</div>
<h2><a href="/<?=$nicename; ?>/"><?=$be_name; ?></a></h2></div>
<div class="be_breedimg"><a href="/<?=$nicename; ?>/"><img src="/new/breed_engine/images/dog_images/<?=$imgname ?>" alt="<?=$brdname?>" title="<?=$brdname?>" height="200" width="190" /></a></div>
</div>
<?
$count='1';
}
}
if($countbreeds !='4'){
$dropvalue=1;
while($countbreeds < 4){ 
?>

<div class="col-sm-6 col-sm-5cols">
<div class="p-d-t">
<select id="breed_id_add<?=$dropvalue?>" class="be_selectBox" name="breed_id_add" onchange="addbreed('breed_id_add<?=$dropvalue?>');">
      <option value='0'>Select Breed</option>
        <?php
			$query_domain = query_execute("SELECT * FROM dog_breeds WHERE breed_engine='1' ORDER BY be_name ASC");
		
			while($query_domain1 = mysql_fetch_array($query_domain)){
			$breed_id12 = $query_domain1["breed_id"];
			$breed_name = $query_domain1["breed_name"];
			$be_name=$query_domain1["be_name"];
			$brdnice=$query_domain1["nicename"];
			$pos1 = strpos($breeds,$breed_id12);
			if($pos1=='0'){
			  print "<option value='$breed_id12|$brdnice'";
			  if($breed_id12 == $breed_id){   echo "selected=='selected'";  }  print ">$be_name</option>";
			 }
			}
		  ?>
    </select>
</div>
</div>
<? 
$countbreeds++;
$dropvalue++;
}
}
?>

   </div>
   <h3>Breed Info</h3>
   <div class="be-compare-details">
   <?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("37","27","41","3","1");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
   <div class="col-sm-6 col-sm-5cols">
   <label>
   <?=$attname['traits'] ?><? if($att_id=='27'){ echo "<a style='cursor:pointer' id='This is an average cost of getting this breed of puppy home, it may vary from breeder to breeder. though, if you want then you can always adopt a dog.' title='Average for the breed' class='tooltip'>*</a>";} ?>
  </label>
  
   </div>
   <? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>

<div class="col-sm-6 col-sm-5cols">
<label>
<div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="meter-span"><?=$attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
</div>
</label>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<div class="col-sm-6 col-sm-5cols"><label><?= $valuename['value'];
   if($att_id=='27'){ ?>
  <? if($valuename['value']=='Economical'){ $tit_id='(upto Rs 5,000)';} else if($valuename['value']=='Pocket Friendly'){ $tit_id='(Rs 10,000 - Rs 20,000)';} else if
($valuename['value']=='Expensive'){$tit_id='(Rs 25,000 - Rs 30,000)';} else{$tit_id='(Rs 35,000 - Rs 50,000)';}?><a style="cursor:pointer" class='tooltip' id="<?=$tit_id?> approximate cost,which can very according to the puppy and the location." title="<?=$tit_id?> approximate cost,which can very according to the puppy and the location.">*</a> <? }
 ?></label>
 </div>

<? 
}
$count='1';
}
}
}
?>
  </div>

  <div class="be-compare-details">

<?
} ?>


</div>
<h3>Maintenance & Effort</h3>
<div class="be-compare-details">

<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("22","28","30","32","34","1000","1001","15","7","16","17");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
if($att_id1!='1000' && $att_id1!='1001'){
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
}
if($att_id1=='1000'){
$selectdata=mysql_query("SELECT monthly_keeping_cost_premium FROM dog_breeds WHERE breed_id='$breedid'");
}
if($att_id1=='1001'){
$selectdata=mysql_query("SELECT monthly_keeping_cost_standard FROM dog_breeds WHERE breed_id='$breedid'");
}
while($selectdata1=mysql_fetch_array($selectdata)){
	if($att_id1!='1000' && $att_id1!='1001'){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
	}
	if($att_id1=='1000'){
	$att_value_pre=$selectdata1['monthly_keeping_cost_premium'];
	}
	if($att_id1=='1001'){
	$att_value_std=$selectdata1['monthly_keeping_cost_standard'];
	}
if($att_id1!='1000' && $att_id1!='1001'){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
}
if($count=='0'){
?>
<? if($att_id1!='1000' && $att_id1!='1001'){ ?>
<div class="col-sm-6 col-sm-5cols">
<label><?=$attname['traits'] ?><? if($att_id=='32' || $att_id=='11'){ echo "<a style='cursor:pointer' id='Average for the breed' title='Average for the breed' class='tooltip'>*</a>";} ?>
</label></div>
<? }if($att_id1=='1000'){ ?>
<div class="col-sm-6 col-sm-5cols">
<label>Monthly keeping cost (Premium)<a style='cursor:pointer' class='tooltip' id='This is an average monthly expense for keeping this breed. This includes an approximation cost of premium dry dog food, grooming expenses and the vet bills incurred in a month.' title='Average for the breed'>*</a>
</label></div>
<? }if($att_id1=='1001'){ ?>
<div class="col-sm-6 col-sm-5cols">
<label>Monthly keeping cost (Standard)<a style='cursor:pointer' class='tooltip' id='This is an average monthly expense for keeping this breed. This includes an approximation cost of regular dry dog food, grooming expenses and the vet bills incurred in a month.' title='Average for the breed'>*</a>
</label></div>
<? }
 }
								if(($att_id <= '20'  || $att_id == '43' || $att_id == '44' ) && ($att_id1 != '1000' && $att_id1 != '1001')) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<div class="col-sm-6 col-sm-5cols">
<label>
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="meter-span"><?=$attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
</label>
</div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<div class="col-sm-6 col-sm-5cols">
<label><? if($att_id1!='1000' && $att_id1!='1001'){ ?><?= $valuename['value'] ?><? }if($att_id1=='1000'){
echo "<img src='/new/breed_engine/images/be_rupee.png' width='10' height='10' />". number_format($att_value_pre)."";
}if($att_id1=='1001'){
echo "<img src='/new/breed_engine/images/be_rupee.png' width='10' height='10' />".number_format($att_value_std)."";
} ?>
</label>
</div>
<? 
}
$count='1';
}
}
}
?>
</div>
<div class="be-compare-details">

<?
} ?>

</div>

<h3>Hair & Coat</h3>
<div class="be-compare-details">

<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("24","23","36","46","9","10");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<div class="col-sm-6 col-sm-5cols"><label><?=$attname['traits'] ?></label></div>
<? }
								if($att_id <= '20'  || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<div class="col-sm-6 col-sm-5cols">
<label>
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div> 
    <span class="meter-span"><?=$attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
</label>
</div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<div class="col-sm-6 col-sm-5cols">
<label><?= $valuename['value'] ?></label>
</div>
<? 
}
$count='1';
}
}
}
?>
</ul>
</div>
<div class="be-compare-details">

<?
} ?>


</div>

<h3>Health</h3>
<div class="be-compare-details">

<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("40","47");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<div class="col-sm-6 col-sm-5cols"><label><?=$attname['traits'] ?></label></div>
<? }
								if($att_id <= '20'  || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<div class="col-sm-6 col-sm-5cols">
<label>
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?=$attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
</label>
</div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<div class="col-sm-6 col-sm-5cols">
<label><?= $valuename['value'] ?></label>
</div>
<? 
}
$count='1';
}
}
}
?>
</div>
<div class="be-compare-details">

<?
} ?>

</div>


<h3>Behavior</h3>
<div class="be-compare-details">

<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("21","36","33","1","2","43","4","5","18","19","44","12","13");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<div class="col-sm-6 col-sm-5cols"><label><?=$attname['traits'] ?></label></div>
<? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' || $att_id == '33') {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<div class="col-sm-6 col-sm-5cols">
<label>
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="meter-span"><?=$attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
</label>
</div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<div class="col-sm-6 col-sm-5cols">
<label><?= $valuename['value'] ?></label>
</div>
<? 
}
$count='1';
}
}
}
?>

</div>
<div class="be-compare-details">

<?
} ?>


</div>
  
  <h3>Breeding</h3>
<div class="be-compare-details">

<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("26","38");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<div class="col-sm-6 col-sm-5cols"><label><?=$attname['traits'] ?></label></div>
<? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<div class="col-sm-6 col-sm-5cols">
<label>
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
<span class="meter-span"><?=$attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>
</label>
</div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<div class="col-sm-6 col-sm-5cols">
<label><?= $valuename['value'] ?></label>
</div>
<? 
}
$count='1';
}
}
}
?>
</div>
<div class="be-compare-details">

<?
} ?>

</div>
<h3>Appearance</h3>
<div class="be-compare-details">

<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("29","31","42","48","23","8","6","1234");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
if($att_id1!="1234"){
$selectdata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id1'");
}
while($selectdata1=mysql_fetch_array($selectdata)){
	$att_id=$selectdata1['att_id'];
	$att_value=$selectdata1['value'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
$attname=query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");	

$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breedid' AND att_id='$att_id'");
if($count=='0'){
?>
<div class="col-sm-6 col-sm-5cols"><label><?=$attname['traits'] ?></label></div>
<? }
								if($att_id <= '20' || $att_id == '43' || $att_id == '44' ) {
								//if($qRATING['value']!='0' && $qRATING['value']<='9') {
								$nRating=(($qRATING['value1']/1000)*100);
								if($nRating <= 0 || $nRating > 5){ $finalratng='0'; $ratvalue="zerostar";}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1'; $ratvalue="rate1";}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2'; $ratvalue="rate2";}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3'; $ratvalue="rate3";}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4'; $ratvalue="rate4";}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5'; $ratvalue="rate5";}
								
								?>
<div class="col-sm-6 col-sm-5cols">
<label>
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  
    <span class="meter-span"><?=$attname['min_value'] ?></span>
<span class="meter-span"><?=$attname['max_value'] ?></span>

</label>
</div>
<? } else { 
$valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
<div class="col-sm-6 col-sm-5cols">
<label><?= $valuename['value'] ?></label>
</div>
<? }
$count='1';
}
}
 }
?>

</div>
<div class="be-compare-details">

<?
} ?>

</div>
<div class="be-compare-details">

<?php
$count=0;
$breeds_compare=explode(",","$breeds");
$attary=array("1234");
foreach($attary as $att_id1){
	$count='0';
foreach($breeds_compare as $brd){
if($brd!='1'){
$breedid=$brd;
$selectdata=mysql_query("SELECT nicename FROM dog_breeds WHERE breed_id='$breedid'");
while($selectdata1=mysql_fetch_array($selectdata)){
	$nicename1=$selectdata1['nicename'];
 // 	if($att_id=='37' || $att_id=='3' || $att_id=='14' || $att_id=='27' || $att_id=='41' ){
if($count=='0'){
?>
<div class="col-sm-6 col-sm-5cols"></div>
<? }?>
<div class="col-sm-6 col-sm-5cols">
<label>
 <div class="be_knw_more"> 
 <a href="/<?=$nicename1?>/">Know More</a>
</div>
</label>
</div>
<? 
$count='1';
}
}
}
?>

</div>
<div class="be-compare-details" style="border-bottom:0px;">

<?
} ?>


</div>

<div class="container">
    <div class="row">
    
 <div class="be-share-box">
                          <?  
                        $surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                       include($DOCUMENT_ROOT."/new/articles/social-bootstrap.php"); 
                       
                     ?>
                        </div>

</div>
</div>
<? }else{ ?>
  <div class="container">
    <div class="row"><label id="messagebox5" class="be_msg">You cannot compare more then 4 breeds</label></div></div>
<? } ?>	