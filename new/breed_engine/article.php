<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/be_style.css" />
<title>Breed Engine</title>
<?php require_once($DOCUMENT_ROOT.'/new/common/header_new.php');?>
<style>.container{ float:left;width: 680px;}</style>
<div class="cont980">
<div class="be_detailBox">
<div class="be_imgDetail">
  <img src="images/thumb_img.jpg" width="329" height="256" alt="" /> 
</div>
<div class="be_txtDetail">
<h3>Breed Name</h3>
<p>
<label><strong>Common Nicknames:</strong> King of Terriers,	Airedale</label>
<label><strong>Origin:</strong> United Kingdom</label>
<label><strong>Group (Of Breed):</strong> Terrier</label>
<label><strong>Description:</strong> The Airedale is the largest of the Terriers originating in Britain. They weigh 25-30 kilograms (55-66 lb) and have a height at the withers of 58-61 centimetres (23-24 in) for dogs, with bitches slightly smaller.</label>
</p>
</div>
</div>

<div class="container" id="tabs">
<div class="be_detailTxt">
  <h2>Overview</h2>
  <h3>AIREDALE TERRIER DETAILS</h3>
  <p><strong>Dog Size and Average Life Expectancy</strong></p>
  <p>The Airedale Terrier has a minimum height of 23 inches and a maximum height of 24 inches, which is slightly taller than the average for all Dog Breeds. The Airedale Terrier tends to weigh between 55 pounds and 66 pounds and is expected to live between 10 years and 11.5 years, which is far less than the average for all Dog Breeds. </p><br />

  <p><strong>Behavior</strong></p>
  <p>The Airedale Terrier has been known to be Aggressive, Alert, Independent, Intelligent and Loyal. The Airedale Terrier is good with children. </p><br />
  <p><strong>Dog Size and Average Life Expectancy</strong></p>
  <p>The Airedale Terrier has a minimum height of 23 inches and a maximum height of 24 inches, which is slightly taller than the average for all Dog Breeds. The Airedale Terrier tends to weigh between 55 pounds and 66 pounds and is expected to live between 10 years and 11.5 years, which is far less than the average for all Dog Breeds. </p><br />

  <p><strong>Behavior</strong></p>
  <p>The Airedale Terrier has been known to be Aggressive, Alert, Independent, Intelligent and Loyal. The Airedale Terrier is good with children. </p><br />

  </div>
  </div>
  
  <div class="be_linkBox">
<div class="be_ad_banner">
  <img src="images/thumb_img.jpg" width="270" height="256" alt="" /> 
</div>
<div class="be_ad_banner">
<h3>Related Breeds</h3>
<ul>
<li><a href="#">German Shepherd</a></li>
<li><a href="#">Labrador Retriever</a></li>
<li><a href="#">Beagle</a></li>
</ul>
</div>
<div class="be_ad_banner">
<h3>Related Topics</h3>
<ul>
<li><a href="#">German Shepherd</a></li>
<li><a href="#">Labrador Retriever</a></li>
<li><a href="#">Beagle</a></li>
</ul>
</div>
<div class="be_ad_banner">
<h3>Give your pet It's Social Identity</h3>
<ul>
<li><a href="#">Wag Club</a></li>
<li><a href="#">Add Your Dog</a></li>
</ul>
</div>
</div>

</div>