
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/be_style.css" />
<title>Breed Engine</title>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php');?>
<style>.container{ float:left;width: 680px;}
html ul.tabs li.active {
 border-radius:0;
background: #fff;
border:1px solid #668000;
border-bottom: 0px;
}
html ul.tabs li.active a{ border:0px; border-radius:0;background: #fff; border-top:4px solid #668000;}
html ul.tabs li{ border-radius:0;border:0px solid #668000; margin-right:2px; }
.be_tab:hover{ overflow:visible;}
html ul.tabs li>ul{float: right;
width: 131px;
top: 36px;
right: 0px;
position: absolute;}
html ul.tabs li>ul>li{ float:left; width:100%; overflow:visible; list-style:none;}
ul.tabs li a {
background: #f7ffd8;
font-size: 15px;
padding: 0px 8px;
border: 1px solid #668000;
border-radius: 2px 2px 0 0;
text-transform: uppercase;
}
html ul.tabs li.active a:hover, html ul.tabs a:hover {
background: #668000; color:#fff;
border-radius: 0;
border: 1px solid #666;
border-bottom: 0px;
}
html ul.tabs li.active a:hover{border:0px;}
.tab_container{border: 0;
border-top: 1px solid #668000;
border-radius: 0px;
}
.tab_content p { color:#333;}
#rateIt {
height: 15px;
overflow: hidden;
}
ul.rating li{ border:0px;}
.be_groupBox.be_txtDetail1{ width:972px;}
.be_groupBox.be_txtDetail1 h1, .be_groupBox.be_txtDetail1 p{ padding-left:0px;}
</style>


<div class="cont980">

<div class="be_detailBox1">


<div class="be_groupBox be_txtDetail1">
<h1>Breed Name</h1>
<p>The name of the group is self-explanatory; it belongs to all the toy breeds. These dogs were not bred to do something; 
most of them were developed to be companions to humans.  The dogs in this group do not have specific skills that set them apart.
Some of the famous names in this category include the Chihuahua, Pug and the Pomeranian.</p>

<p>Most of the dogs in this group were bred centuries ago but their prime focus and employment have been as human companions.
They were even used as lap dogs may a times by the royalty. As of today they have taken the place of adorable pets in
the household as they do not need much room to move around in the house.</p>

<p>This group can be snappy and mostly not considered to be the ideal pets with young children.
There are a few breeds that can be fierce watchdogs and even do well with training. There are times when some breeds
are referred as a teacup size but this is not a category and is not even recognized by most of the prestigious and
renowned kennels clubs over the world. 
</p>
</div>

</div>

<div class="be_groupTag">
<div class="be_linkBox">
<div class="be_ad_banner">
<h3>Browse by Groups</h3>
<ul>
<li class="be_rldBreed"><a href="/beagle/"><label>Terrier</label>
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-beagle.jpg" alt="Beagle" title="Beagle"></span>
</a></li>
<li class="be_rldBreed"><a href="/beagle/"><label>Terrier</label>
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-beagle.jpg" alt="Beagle" title="Beagle"></span>
</a></li>
<li class="be_rldBreed"><a href="/beagle/"><label>Terrier</label>
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-beagle.jpg" alt="Beagle" title="Beagle"></span>
</a></li>

</ul>
</div>




<div class="be_ad_banner">
<h3>Top Breeds</h3>
<ul>
<li><a href="/german-shepherd-dog-alsatian/">German Shepherd
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-german.jpg" alt="German Shepherd" title="German Shepherd"></span>
</a></li>
<li><a href="/rottweiler/">Rottweiler
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-rott.jpg" alt="Rottweiler" title="Rottweiler"></span>
</a></li>
<li><a href="/tibetan-mastiff/">Tibetan Mastiff
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-tibetan-mastiff.jpg" alt="Tibetan Mastiff" title="Tibetan Mastiff"></span>
</a></li>
<li><a href="/golden-retriever/">Golden Retriever
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-golden.jpg" alt="Golden Retriever" title="Golden Retriever"></span>
</a></li>
<li><a href="/great-dane/">Great Dane
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-great-dan.jpg" alt="Great Dane" title="Great Dane"></span>
</a></li>
<li><a href="/dobermann/">Doberman
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-doberman.jpg" alt="Doberman" title="Doberman"></span>
</a></li>
<li><a href="/pug/">Pug
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-pug.jpg" alt="Pug" title="Pug"></span>
</a></li>
<li><a href="/beagle/">Beagle
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-beagle.jpg" alt="Beagle" title="Beagle"></span>
</a></li>
<li><a href="/labrador-retriever/">Labrador Retriever
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-lab.jpg" alt="Labrador Retriever" title="Labrador Retriever"></span>
</a></li>
<li><a href="/siberian-husky/">Siberian Husky
<span class="be_relimg"><img src="/new/breed_engine/images/dog_images/35-33-Siberian_husky.jpg" alt="Siberian Husky" title="Siberian Husky"></span>
</a></li>
</ul>
</div>

</div>

<div class="container">
<div class="be_slideBox">
  
<h3>Check rest of the breeds</h3>
  <ul>
  
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Pit Bull Terrier (American)</div>
</a>
</li>
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Neapolitan Mastiff (Italian Mastiff)</div>
</a>
</li>
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Beagle</div>
</a>
</li>    
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Beagle</div>
</a>
</li>    
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Beagle</div>
</a>
</li>    
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Beagle</div>
</a>
</li>    
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Beagle</div>
</a>
</li>    
<li><a href="/photos/2nd-bis_1" target="_blank">
<img src="/new/breed_engine/images/dog_images/Afghan.jpg" title="2nd-bis_1.jpg" alt="2nd-bis_1.jpg" border="0" width="150">
<div class="be_grpNam">Beagle</div>
</a>
</li>    
    </ul>
  </div>
</div>
</div>
</div>
