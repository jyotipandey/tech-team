<?php
require_once($DOCUMENT_ROOT.'/session.php');
include($DOCUMENT_ROOT . "/constants.php");
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');

if($section[0] == 'shop'){
	if($section[1] == 'category'){
	//check category
		if($section[2]){
			$rowcaturl=query_execute_row("SELECT category_nicename FROM shop_category WHERE old_category_nicename = '$section[2]'");
			if($rowcaturl['category_nicename']!=''){
				$requestedUrl = str_replace("shop/category/$section[2]","shop/category/".$rowcaturl['category_nicename'], $requestedUrl);
				
				}
		}
		//close category
		
		$newredirect = str_replace('shop/category/', '', $requestedUrl);
	}elseif($section[1] == 'brand'){
		if($section[2]){
			if(strpos('s'.$section[2], '?') !=0){
				$newredirect = str_replace('shop/', '', $requestedUrl);
			}else{
				$newredirect = str_replace('shop/brand/', '', $requestedUrl);
			}
		}else{
			$newredirect = str_replace('shop/', '', $requestedUrl);
		}
	}elseif($section[0] == 'shop'){
		$newredirect = str_replace('shop/', '', $requestedUrl);
	}
}

if($newredirect){
	header( "HTTP/1.1 301 Moved Permanently" );
	header( "location: $newredirect"); 
	die(mysql_error());
	exit();
}

makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[1],'');
// Articles---------------------------------------------------------------------- Start
// Shop---------------------------------------------------------------------- Start
$rowPageCat=query_execute_row("SELECT category_id FROM shop_category WHERE category_nicename='$section[0]'");
$category_id = $rowPageCat["category_id"];
if($category_id){
	include 'new/shop/category-chq.php';
	exit();
}
// Shop---------------------------------------------------------------------- END

$rowPageArt=query_execute_row("SELECT article_id,domain_id FROM articles WHERE art_name = '$section[0]'");
$article_id = $rowPageArt["article_id"];
$domain_article_id = $rowPageArt["domain_id"];
if($article_id){
	if($domain_article_id=='2'){
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: http://www.fishspot.in/$section[0]/");
	exit();
	}
	else{
	include 'new/articles/articles_insidepage.php';
	exit();
}}

// Articles---------------------------------------------------------------------- End
$rowPagebreed=query_execute_row("SELECT breed_id,nicename FROM dog_breeds WHERE nicename = '$section[0]'");
$breed_id = $rowPagebreed["breed_id"];
$breed_nicename = $rowPagebreed["nicename"];
if($breed_nicename){
include 'new/breed_engine/breed-info.php';
exit();	
}


// Item---------------------------------------------------------------------- Start
$rowPageItem=query_execute_row("SELECT item_id FROM shop_items WHERE nice_name='$section[0]'");
$item_id = $rowPageItem["item_id"];

if($item_id){
	if($userid=='abhi2290'){
	include 'new/shop/product-individual-new.php';
	exit();}else{
	include 'new/shop/product-individual.php';
	exit();

	}
	
	
}
// Item ends here---------------------------------------------------------------------- END

// Search section---------------------------------------------------------------------- Start
//[Abhijeet Mudgal]
$rowPageItemsearch=query_execute_row("SELECT nice_name,trend_id FROM section_nice_name WHERE nice_name='$section[0]'");
$nice_name = $rowPageItemsearch["nice_name"];
$trend_id1 = $rowPageItemsearch["trend_id"];

if($trend_id1=='13' || $trend_id1=='14' || $trend_id1=='15'){
include 'search_listing/search_listing.php';
exit();
}
else{
if($nice_name){
include 'new_search.php';
exit();
}
}
// Search section---------------------------------------------------------------------- END

// Search section---------------------------------------------------------------------- Start
$rowPageItemsearch=query_execute_row("SELECT trend_nice_name  FROM section_search_trend WHERE trend_nice_name='$section[0]'");
$trend_nice_name = $rowPageItemsearch["trend_nice_name"];

if($trend_nice_name){
	include 'sitemap-more.php';
	exit();
	}
// Search section---------------------------------------------------------------------- END
 



// Search Brred section---------------------------------------------------------------------- Start
$emsearch=query_execute_row("SELECT breed_url  FROM section_nice_name WHERE breed_url='$section[0]'");
$breed_url = $emsearch["breed_url"];

if($breed_url){
	include 'sitemap-more-ajax.php';
	exit();
	}
// Search section---------------------------------------------------------------------- END

// Search Show section---------------------------------------------------------------------- Start
$rowPageh=query_execute_row("SELECT show_nicename FROM show_description WHERE show_nicename='$section[0]'");

$nice_name = $rowPageh["show_nicename"];

if($nice_name){
	include 'show-results/show-results.php';
	exit();
	}
// Search show  section---------------------------------------------------------------------- END



// Brand---------------------------------------------------------------------- Start
$rowBrand=query_execute_row("SELECT brand_id FROM shop_brand WHERE brand_nice_name='$section[0]'");
$brand_id = $rowBrand["brand_id"];
if($brand_id){
	include 'new/shop/brand.php';
	exit();
}
// Brand---------------------------------------------------------------------- END


header("HTTP/1.0 404 Not Found");
require_once($DOCUMENT_ROOT.'/404.php');
exit();
?>