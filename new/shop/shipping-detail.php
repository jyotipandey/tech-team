<?
 
	/*error_reporting(E_ALL);
	ini_set("display_errors", 1);*/
	$url2 ='/home/dogspot/public_html';
	//$url2='D:/xampp/htdocs/dogspot';
	require_once($url2.'/database.php');
	require_once($url2.'/functions.php');
	require_once($url2.'/shop/functions.php');
	$getCod=query_execute_row("SELECT valNo FROM shop_charges_settings WHERE name='cod'");
	$codCharges=$getCod['valNo'];
$getShipping=query_execute_row("SELECT  `valNo`, `condition` FROM `shop_charges_settings` WHERE `name`='shipping'");
	$ShipCharges=$getShipping['valNo'];
$ShipconditionCharges=$getShipping['condition'];?>
    <style>
    .login-popup{ padding:0px; border:none;}
	.btn-sec{width: 30%;float: left;}
	.shp-top{ float: left;width: 100%;border-bottom: 1px solid #ddd;}
	.shp-l{width: 70%;text-align: left;float: left;}
	.shp-l h3{color:#333;padding:15px 20px;text-transform: uppercase;font-size: 16px;font-family: lato,sans-serif;letter-spacing: 0.6px;}
	.btn-sec a{font-weight: bold;text-decoration: none;}
	.btn_close{background: #f1f1f1;color: #999;float: right;font-size: 15px;position: relative;z-index: 1;text-align: center;line-height: 16px;border-radius: 50%;width: 35px;height: 35px;top: 5px;right: 10px;padding: 10px;box-sizing: border-box;}
	.sh-text-m{ float: left;width: 100%;margin-top: 10px;}
	.sh-text-m  h2{    margin-bottom: 5px;
    color: #333;
    padding: 5px 20px 5px 20px;
    font-size: 16px;
    font-weight: normal;
    font-family: lato, sans-serif;
    text-transform: uppercase;}
	.text-shp{    text-align: left;
    font-size: 13px;
    padding: 0px 20px;
    color: #555;
    line-height: 18px; margin-bottom:3px;}
    </style>
    <div class="main-cont1" style="width: 972px; margin-top:10px; bottom: 120px; left: 406.5px; display: block; font-family:lato,sans-serif; letter-spacing:0.5px; float:left; padding-bottom:15px " >
    
    
    <div class="shp-top">
    <div class="shp-l">
    <h3>
    Shipping and Delivery details for Products</h3></div>
    
    <div class="btn-sec">
	<a href="#" class="close">
    <div class="btn_close" title="Close Window" alt="Close" style="" rel="nofollow">X<p></p></div></a>
</div>
  </div>
  
    
    

<div class="sh-text-m">
	
	<h2>We ship only in India. </h2>
    
	<div class="text-shp">For domestic (India) buyers, orders are shipped through registered domestic courier companies and speed post. Orders are shipped within 2-3 Business days from the day of placing the order. The delivery of the shipment is subject to Courier Company / post office norms.Radox Trading & Marketing Pvt Ltd (DogSpot.in) is not liable for any delay in delivery by the courier company / postal authorities and only guarantees to hand over the consignment to the courier company or postal authorities within 2-3 Business days from the date of the order and payment. All orders will be delivered to the registered address of the buyer as per the credit/debit card records (Unless specified at the time of Order). Radox Trading & Marketing Pvt Ltd (DogSpot.in) is in no way responsible for any damage to the order while in transit to the buyer.</div>
   <div style=" text-align:left;"> 
    <h2 style="color:#333; margin-top:10px; margin-bottom:5px; ">Shipping Charges</h2>
    
     <div class="text-shp">Free Shipping on all orders above Rs. <?=$ShipconditionCharges?>*</div>
          
<h2 style="color:#333; margin-top:10px; margin-bottom:5px;"> *Terms and Conditions</h2>

 <div class="text-shp">1. Dog food products would be charged itemized shipping charge of <strong>Rs. 75/quantity</strong></div>

 <div class="text-shp">2. If the order value is more than or equal to Rs. <?=$ShipconditionCharges?> with no dog food and cat litter then shipping  would be <strong>FREE</strong>
 </div>

 <div class="text-shp">
 3. If the order value is less then Rs. <?=$ShipconditionCharges?> with no dog food or cat litter in the order  then shipping would be <strong>Rs. <?=$ShipCharges?></strong></div>

 <div class="text-shp">4. If  the order has pet food or cat litter + any other items then the itemized shipping will be  applicable <strong>Rs. 75/quantity</strong> of pet food and cat litter</div>
</div>
   
 <div class="text-shp">Rs. <?=$codCharges?> applicable for Cash On Delivery (COD) payment. </div>
<!--<h2>The Octroi charge is payable by the recipient at the time of delivery.</h2>--> 
<!--<p style="font-size:12px;text-align:justify">The courier company will collect the OCTROI amount from the recipient at the time of delivery.</p>-->
<h2> When is an item Out of Stock? </h2>

<div class="text-shp">Currently we do not have these products available for sale. Please use the "Notify Me" feature and we will let you know via Email and SMS once it is available for sale at DogSpot.in</div>

<h2>How is the delivery done?</h2>
 <div class="text-shp">We process all deliveries through reliable couriers companies that match our delivery service standards, depending on the items ordered and delivery location.</div>
</div>
</div>
