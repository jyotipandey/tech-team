<?php
ini_set("post_max_size", "40M");
ini_set("upload_max_filesize", "15M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "99M");
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<title>Wag Club</title>

<?php //require_once($DOCUMENT_ROOT . '/new/common/header-new.php');?>
<div id="show"></div>
<div class="activity_pop_wc_upload" id="activity_pop_wc_upload">
<div class="activity_pop_wc">
<h2>
 <span style="float:left; width:516px;">Upload image here</span>
 <a class="close"><span style="float:right; margin-top:9px;"> [X]</span></a>
</h2>
 <form id="vasPLUS_Programming_Blog_Form" method="post" enctype="multipart/form-data" action="javascript:void(0);">

<div>
  <select name="status_type" id="status_type" style="margin-left:40%" required>
    <option value="0">Select</option>
    <option value="product">Product</option>
    <option value="rto">RTO</option>
    <option value="reverse-pickup">Reverse Pickup</option>
  </select>
</div>

<div class="select_box_dg1"> 
<span id="submitspan">
    <input name="vasPhoto_uploads" id="vasPhoto_uploads" type="file" required />
     <div class="upload_photo_dg1">Upload Photo</div>
     </span>
				  <div class="crop_img_wc" id="crop_img2" style="display:none" >
				 <div id="vasPhoto_uploads_Status" width="250" height="250"></div>
				 </div>
</div>

<div id="error" style="display:none"><font style="color:#F00">Please select image</font> </div>
<div class="btn_is_box1">
<input name="" type="button" value="Cancel" style="cursor:pointer" onclick="cancelpic('crop_img2')">
<input name="" type="button" value="Upload" style="cursor:pointer" onclick="submitdata()">
</div>
</form>
</div>
<div id="show" style="display:none"></div>
</div>
