<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>My Cart</title>
<meta name="keywords" content="Cart | DogSpot" />
<meta name="description" content="Cart | DogSpot" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->

<? $sitesection='shop-cart'; 
if($back==1){?>
<?php require_once($DOCUMENT_ROOT.'/new/common/header1.php'); 
}else{
require_once($DOCUMENT_ROOT.'/new/common/header.php'); 
} 
require_once($DOCUMENT_ROOT."/new/shop/cat.php"); 
if($back!=1){
require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); }?>