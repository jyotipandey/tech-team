<?php
// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$sitesection = "shop";
$session_id = session_id();
$ip = ipCheck();

if($tsid!=$session_id){
	header("Location: /new/shop/myorder.php?order_id=$cart_order_id");
	exit();
}

if($status=='success' || $status=='SUCCESS'){
	$order_status=0;
}elseif($status=='failure' || $status=='FAILURE'){
	header("Location: /shop/checkout.php?order_id=$txnid");
	exit();
	$order_status=1;
}elseif($status=='pending' || $status=='PENDING'){
	$order_status=2;
}
$ResponseMessage=$status.' '.$unmappedstatus;
if($txnid==''){
	header("Location: /");  
	exit();
}
$StatusCheck = query_execute_row("SELECT delevery_status FROM shop_order WHERE order_id = '$txnid'");
if($StatusCheck['delevery_status']!='new') {
	header("Location: /new/shop/myorder.php?order_id=$txnid"); 
	exit();
}
$domainUpdate = query_execute("UPDATE shop_order SET domain_id = '1' WHERE order_id = '$txnid'");
//update date in shop order
$orderdate_update = query_execute("UPDATE shop_order SET order_c_date=NULL WHERE order_id = '$cart_order_id'");
	//close update shop order
// Update in DB----------------------------------------------------------------------------------
if($key=='i0JYEe'){
	if($status=='success' || $status=='SUCCESS'){
		$resultinsert = query_execute("UPDATE shop_order SET order_transaction_id = '$mihpayid', order_transaction_source='PayU', order_transaction_amount = '$amount', order_status = '$order_status', mode = '$mode', order_message = '$ResponseMessage' WHERE order_id = '$txnid'");
		$resultinsert11 = query_execute("UPDATE shop_cart SET cart_order_status = '$order_status' WHERE cart_order_id = '$txnid'");
	}else{
		$resultinsert11 = query_execute("UPDATE shop_order SET order_transaction_id = '$mihpayid', order_transaction_amount = '$amount', order_status = '$order_status', order_message = '$ResponseMessage' WHERE order_id = '$txnid'");
	}

// Update in DB----------------------------------------------------------------------------------
}
if($mode=='cod' || $mode=='dd'){
	if($mode=='cod'){
		$order_status=0;
	}else{
		$order_status=1;
	}
	$DateCreated=date("o m d");
	$resultinsert = query_execute("UPDATE shop_order SET order_status = '$order_status', mode = '$mode' WHERE order_id = '$txnid'");
	$resultinsert = query_execute("UPDATE shop_cart SET cart_order_status = '$order_status' WHERE cart_order_id = '$txnid'");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Payment Details | DogSpot</title>
<meta name="keywords" content="Payment Details | DogSpot" />
<meta name="description" content="Payment Details | DogSpot" />
<link href="/new/css/main.css" rel="stylesheet" type="text/css" />
<link href="/new/css/shop.css" rel="stylesheet" type="text/css" />
<link href="/template/css/checkout.css" rel="stylesheet" type="text/css" />
<link href="/new/css/thank_you.css" rel="stylesheet" type="text/css" />
<link href="/new/css/headfoot.css" rel="stylesheet" type="text/css" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
$sitesectionShop="response";
?>
<!--[if lt IE 7]>
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="cont980">
<div class="thank_you_box">
<div class="vs20"></div>
<? if($status=='success' || $status=='SUCCESS'){?>
<h3><img src="images/ryt.png" width="26" height="21" alt="" style="float:left;" /><span class="ty_head">Thank you! Your order has been placed successfully.</span>
  <img src="images/print.png" width="16" height="15" alt="" style="float:right;" /> </h3>
<? }else{?><span class="ty_head">
<strong>Your Payment has been unsuccessful.</strong> </span>
<? }?>
<?
	$orderStat = query_execute("SELECT * FROM shop_order WHERE order_id = '$cart_order_id'");
	$rowOrd = mysql_fetch_array($orderStat);
	$order_discount_id = $rowOrd["order_discount_id"];
	$order_status = $rowOrd["order_status"];
	$order_method = $rowOrd["order_method"];
	$orderuserid = $rowOrd["userid"];
	$delevery_status=$rowOrd["delevery_status"];
	// update Discount Start
	updateDiscountStatus($cart_order_id);
	$order_items_amount=number_format($rowOrd["order_items_amount"]);
	$order_shipment_amount=number_format($rowOrd["order_shipment_amount"]);
	$order_discount_amount=number_format($rowOrd["order_discount_amount"]);
	$totAmount = number_format($rowOrd["order_amount"]);
	
	$GuestCartUpdate = query_execute("UPDATE shop_cart SET userid='$orderuserid' WHERE cart_order_id = '$cart_order_id'");
	// get details from shop_order_address table Shipping------------------------------------------
	$getUsersShip = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '1' AND order_id = '$cart_order_id'");
	$rowUsersShip = mysql_fetch_array($getUsersShip);
	$ship_name = stripslashes($rowUsersShip["address_name"]);
	$ship_address1 = stripslashes($rowUsersShip["address_address1"]);
		$address_email = $rowUsersBill["address_email"];
	$ship_address2 = stripslashes($rowUsersShip["address_address2"]);
	$ship_city = $rowUsersShip["address_city"];
	$ship_state = $rowUsersShip["address_state"];
	$ship_country = $rowUsersShip["address_country"];
	$ship_pin = $rowUsersShip["address_zip"];
	$ship_phone1 = $rowUsersShip["address_phone1"];
	// get details from shop_order_address table END Shipping--------------------------------------		
	
		
// automate shifting of  prepaid and cod  orders start's here....	
	if(($order_method=='ccb' || $order_method=='ccnb' || $order_method=='nb' || $order_method=='dc' || $order_method=='DC') && $order_status=='0' && $delevery_status=='new' ){
		$prepaidcount = query_execute("UPDATE shop_order SET delevery_status='pending-dispatch' WHERE order_id = '$cart_order_id' AND delevery_status = 'new'");
		$sectrevw=query_execute("INSERT INTO section_reviews() VALUES('','BackGroundJob','shop-order','$cart_order_id','Changed Delivery Status: pending-dispatch,','pending-dispatch',NULL,'$ip');");
	}
	if($order_method=='cod'){	
		$codcount=query_execute_row("SELECT count(*) as prepaidcount FROM shop_order  WHERE userid = '$orderuserid' AND delevery_status='delivered'  AND order_method='cod' ");
		$pincodecheck=query_execute_row("SELECT count(*) as pincodechck FROM shop_couriorpin_availablity  WHERE courier_pincode = '$ship_pin' AND courier_method='cod'");
		
		if($codcount['prepaidcount'] >= '2' && $pincodecheck['pincodechck'] > '0' && $order_status=='0' && $delevery_status=='new'){			
		$orderStat = query_execute("UPDATE shop_order SET delevery_status='pending-dispatch' WHERE order_id = '$cart_order_id'  AND delevery_status = 'new'");
		$sectrevw=query_execute("INSERT INTO section_reviews() VALUES('','BackGroundJob','shop-order','$cart_order_id','Changed Delivery Status: pending-dispatch','pending-dispatch',NULL,'$ip');");
		}
	}
	// updation for prepaid and cod order ends here................................................	
?>  
<div class="left_ty_content">
<div class="order_detail">
<h4 class="order_hd">Your order number is:<span> <?=$cart_order_id?></span></h4>
<p class="ty_para">We will soon send you an email confirmation at <?=$address_email?> for this order.<br />
Please quote this order number in all your references while corresponding with us.
</p>
</div>
<div class="order_detail ty_box">
<? if($order_method=='nb'){?>
<h4 class="order_hd">Paymode: Net Banking</h4>
<p class="ty_para">
You have successfully paid and amount of Rs. <?=$totAmount?> using  Netbanking.
</p>
<? }elseif($order_method=='cc' || $order_method=='dc'){?>
<h4 class="order_hd">Paymode: Credit Card / Debit Card</h4>
<p class="ty_para">
You have paid and amount of Rs. <?=$totAmount?> using the credit card / debit card 
</p>
<? }?>
</div>
<h4 class="order_ship">Shipping details:</h4>
<div class="order_detail ty_box1">
<div class="ship_address">
<?=$ship_name?><br/>
<?=$ship_address1?><br/>
<?=$ship_city?><br/>
<?=$ship_state.'-'.$ship_pin?><br/>
</div>
<div class="ship_days">
Your order will be shipped at the mentioned <br/>
address in next 5 - 7 business days
</div>
</div>
<!--<div style="float:right; width:300px; border-color:#CCC; border-style:solid; border-width:1px; margin-top:10px;">
<div style="padding:5px; background-color:#CCC"><strong>Shipping Address</strong></div>
<div style="padding:5px;"><?=$response["DeliveryName"]?><br /><?=$response["DeliveryAddress"]?><br /><?=$response["DeliveryCity"]?> <?=$response["DeliveryState"]?><br /><?=$response["DeliveryPostalCode"]?>
</div></div>-->

<div style="clear:both"></div>     
<?

if($mode!='cod' && $mode != 'dd'){
	$cart_order_id=$txnid;
}
if($cart_order_id){
$qGetMyCart=query_execute("SELECT * FROM shop_cart WHERE cart_order_id='$cart_order_id'");
$totrecord = mysql_num_rows($qGetMyCart);

if($totrecord == 0){
	echo 'Sorry no item in your Cart...';
}else{
?>  
<h4 class="order_ship">Order details:</h4>
<div class="order_detail ty_box1 ty_box2">
<div class="shop_cart ty_shop_cart">
  <div class="shop_head">
        <ul class="cart_ty">
        
        <li>Item</li>
        <li class="name_width">Name</li>
        <li>Quality</li>
        <li>Unit Price</li>
        <li>Total</li>
        </ul>
        </div>
    <?
	while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		$item_id = $rowMyCart["item_id"];
		$cart_id=$rowMyCart["cart_id"];
		$qdata=query_execute("SELECT name, weight, item_parent_id FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		$item_parent_id=$rowdat["item_parent_id"];
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id'");
		$rowdatM = mysql_fetch_array($qdataM);
		
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		if(file_exists($imagepath)){
			$new_w = 100;
			$new_h = 100;
			$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
		}
		
		// Get Tital option
		/*$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];*/
		
		// END
		$emailCart.=$rowdat["name"].$optitle.' (Unit Price Rs. '.$rowMyCart["item_price"].') ('.$rowMyCart["item_qty"].' qty) (Rs. '.$rowMyCart["item_totalprice"].')<br>';
	?>
    <div class="item_info">
        <ul class="ty_item">
          <li> <img src="/shop/item-images/thumb_<?=$rowdatM["media_file"]?>" alt="<?=$rowdatM["label"]?>" width="100" height="100" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/> <? if($itemdomain=='2') {?>
             <div style="border:2px solid #48BAF0; margin:7px 15px 0 15px"></div>
              <? }else{?><div style="border:2px solid #b2da1d; margin:7px 15px 0 15px"></div><? }?></li>
      <li class="name_width">
        <p><span><?=$rowdat["name"];?> <? if($optitle){echo '('.$optitle.')';}?><br /><br/>
         </p>
        <span style="color:#aaa">(Item no: <?=$item_id;?>)</span>
        </li>
       
       <li>       
        <span class="total_pro"> <?=$rowMyCart["item_qty"]?> </span>        
        </li>
        <li class="cost_color"><strike style="color:#000;"><img src="/new/pix/checkout/rupee_icon.png"><?=$rowMyCart["item_price"]?></strike><br />
        <span><img src="/new/shop/images/img/rupee_icon1.png" /><?=$rowMyCart["item_price"]?> x <?=$rowMyCart["item_qty"]?> </span>
        
        </li>
        <li class="cost_color"><strike style="color:#000;"><img src="/new/pix/checkout/rupee_icon.png"> <?=$rowMyCart["item_totalprice"]?></strike><br />
        <span><img src="/new/shop/images/img/rupee_icon1.png" /> <?=$rowMyCart["item_totalprice"]?></span>
        </li>
        </ul>
        </div>
    <? 
	$arrayItemAnaCart[]=$item_id.'|'.$rowdat["name"].' '.$optitle.'|'.$rowMyCart["item_price"].'|'.$rowMyCart["item_qty"].'|'.$rowdat["item_parent_id"];
	
} }?>
    
<div style="clear:both"></div>
<? }?>
 
</div>

<?php /*?><div style="width:300px; float:left;"> <h3 style="margin-bottom:10px;">Shipping</h3>
<table width="100%" align="center" cellpadding="0" cellspacing="0">
  <tbody>
                  <tr>
                     <td width="40%" style="border-bottom:1px solid #ccc; border-right:1px dotted #333; padding:5px;">Name</td>
			         <td width="60%" style="border-bottom:1px solid #ccc; padding:5px;"><?=$ship_name;?></td>
                  </tr>
                  
				  <tr>
                       <td style="border-bottom:1px solid #ccc; border-right:1px dotted #333; padding:5px;">Address:</td>
                      <td style="border-bottom:1px solid #ccc; padding:5px;"><?=$ship_address1;?><br>
               <?=$ship_address2;?><br></td>
                 </tr>
				<tr>
                  <td style="border-bottom:1px solid #ccc; border-right:1px dotted #333; padding:5px;">City:</td>
			      <td style="border-bottom:1px solid #ccc; padding:5px;"><?=$ship_city;?></td>
               </tr>
				<tr>
                <td style="border-bottom:1px solid #ccc; border-right:1px dotted #333; padding:5px;">State:</td>
			    <td style="border-bottom:1px solid #ccc; padding:5px;"><?=$ship_state;?></td>
          </tr>	
		  <tr>
            <td style="border-bottom:1px solid #ccc; border-right:1px dotted #333; padding:5px;">Pin Code:</td>
			<td style="border-bottom:1px solid #ccc; padding:5px;"><?=$ship_pin;?></td>
         </tr>
		  <tr>
             <td style="border-bottom:1px solid #ccc; border-right:1px dotted #333; padding:5px;">Country:</td>
			  <td style="border-bottom:1px solid #ccc; padding:5px;"><?=$ship_country;?></td>
        </tr>	
		<tr>
            <td style="border-bottom:1px solid #ccc; border-right:1px dotted #333; padding:5px;">Phone:</td>
			<td style="border-bottom:1px solid #ccc; padding:5px;"><?=$ship_phone1;?></td>
       </tr></tbody>
   </table>
</div><?php */?>
 <div style="clear:both"></div>
<?php /*?><div style="width:968px; margin-top:10px;"> <div style="float:right;"><a href="javascript:void();" onClick="javascript:window.print()" title="Print this page" style="text-decoration:none">Print this page <img src="/shop/image/print.jpg" width="29" height="29" border="0" style="vertical-align:middle"/></a></div>
<div style="clear:both"></div>
<? }?>
</div><?php */?>
</div>
 <?php //echo 'cew';
 //include("/new/shop/invite.php");
 include_once($DOCUMENT_ROOT.'/new/shop/invite.php'); ?>
<? 	$emailCart.='<br><br><p>---------------------------------------------------------------------</p>';
		
		$rowOrdUser=query_execute_row("SELECT address_name, address_email , address_phone1 FROM shop_order_address WHERE order_id = '$cart_order_id'");
		
		
		//Added By Umesh, Dated 17-12-2014,
		$order_query_cart = mysql_query("select si.name, si.nice_name, sc.mrp_price, sc.mrp_price-sc.item_price as discount, sc.item_price, sc.item_qty, sc.item_qty*sc.item_price as subtotal from shop_cart as sc, shop_items as si where si.item_id = sc.item_id AND sc.cart_order_id='$cart_order_id'");
		
		$order_query_ship_amt = query_execute_row("SELECT order_shipment_amount, item_shipping_amount,mode,order_discount_amount, order_method, order_amount FROM shop_order WHERE order_id='$cart_order_id'");
		
		
		
		$grand_total = 0;
		$grand_subtotal = 0;
		$grand_mrp = 0;
		$emailTemplate = "";
		//$emailTemplate = array();
		$numrow = mysql_num_rows($order_query_cart);
		while($order_result_row = mysql_fetch_array($order_query_cart)){ 
			$grand_total = $grand_total + $order_result_row['subtotal'];
		    $total_discount= $total_discount + ($order_result_row['discount']*$order_result_row['item_qty']);
			$grand_mrp = $grand_mrp + ($order_result_row['mrp_price']*$order_result_row['item_qty']); 
			//$grand_subtotal = $grand_subtotal+ $order_result_row['subtotal'];              
					                
			$emailTemplate .= '<tr style="font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#2d2d2d">
					                    <td height="30"></td>
					                    <td height="30"><a href="https://www.dogspot.in/'.$order_result_row['nice_name'].'" target="_blank" title="'.$order_result_row['name'].'"></a>'.$order_result_row['name'].'</a></td>
					                    <td height="30">'.number_format($order_result_row['mrp_price'],2).'</td>
					                    <td height="30">'.number_format($order_result_row['discount'],2).'</td>
					                    <td height="30">'.number_format($order_result_row['item_price'],2).'</td>
					                    <td height="30">'.$order_result_row['item_qty'].'</td>                        
					                    <td height="30">'.number_format($order_result_row['subtotal'],2).'</td>
					                </tr>';
		}
		
		$f_name=stripslashes($rowOrdUser["address_name"]);
		$email=$rowOrdUser["address_email"];
		
		$arrayEmail['emailType']='SHOP-ORDER';
		$arrayEmail['toName']=$f_name;
		$arrayEmail['toEmail']=$email;
		$arrayEmail['ship_state']=$ship_state;
		$arrayEmail['mod']=$mode;
		$arrayEmail['order_status']=$order_status;
		$arrayEmail['cod_confirm']=$cod_confirm;
		$arrayEmail['order_id']=$cart_order_id;
		$arrayEmail['emailCart']=$emailCart;
		$arrayEmail['emailAmount']=$emailAmount;
		$arrayEmail['totAmount']=$totAmount;
		
		$arrayEmail['ship_name']=$ship_name;
		$arrayEmail['ship_address1']=$ship_address1;
		$arrayEmail['ship_address2']=$ship_address2;
		$arrayEmail['ship_city']=$ship_city;
		$arrayEmail['ship_state']=$ship_state;
		$arrayEmail['ship_pin']=$ship_pin;
		
		$arrayEmail['dogspotId']=$userid;
		$arrayEmail['authorCode']=$author_code;
		$arrayEmail['AOrderMethod']=$AOrderMethod;
		
		
		// Email Template
		$arrayEmail['emailTemplate']=$emailTemplate;
		
		$arrayEmail['grand_mrp']=number_format($grand_mrp,0);
		$total_discount = $total_discount + $order_query_ship_amt['order_discount_amount'];
		
		$arrayEmail['total_discount'] = number_format($order_query_ship_amt[order_discount_amount],0);
		
		$arrayEmail['grand_total'] = number_format($order_query_ship_amt[order_amount],0);
		
		$arrayEmail['order_shipment_amount']=number_format($order_query_ship_amt[order_shipment_amount],0);
		
		$arrayEmail['item_shipping_amount']=number_format($order_query_ship_amt[item_shipping_amount],0);
		//$arrayEmail['grand_subtotal']=number_format($grand_subtotal,0);
		
		
		$emailReturn = sendShopEmail($arrayEmail);
		$registerFlag=$emailReturn['errorFlag'];
		$errortext3=$emailReturn['errorFlag'];
		
/*		if($mode == 'dd'){
			$mailBody ='<p>Dear '.$f_name.'</p>
			Thank you for your order, '.$f_name.'<br>
			<p>You selected to pay for the order via Draft / Cheque.</p>
<p>Please send the Demand Draft / Cheque in favour of <b>PetsGlam Services Pvt. Ltd.</b> payable at <b>Gurgaon</b> to the following address: <br><b>Plot no - 545, S.Lal Tower, Sector - 20, Dundahera , Gurgaon, Haryana PIN: 122016 (India) </b></p>
<p>We will process your order (ID: '.$cart_order_id.') as soon as we receive your Demand Draft/Cheque.</p>
<p><strong>Here is the summary:</strong><br>
  Items Ordered:<br>
'.$emailCart.'</p>'.$emailAmount.'
<p>Expected time of delivery: 10 business days from the time we receive the Payment</p>
'.$mailOctroi.'
<p><strong>Shipping Address:</strong><br>
'.$ship_name.'<br />'.$ship_address1.' '.$ship_address2.'<br />'.$ship_city.' '.$ship_state.'<br />'.$ship_pin.'
<p>To view your order status you can always visit:<a href="http://www.DogSpot.in/new/shop/myorders.php"> My Orders</a><br>
</p>
<p>---------------------------------------------------------------------<br>
  DogSpot.in<br>
  Spot for all your Dog needs. The Store That Delivers at Your Door.<br>
   ---------------------------------------------------------------------</p>
';

//NKS
		$msgSMS = 'Thank you for your order, We will process your order ID:'.$cart_order_id.' as soon as we receive your Demand Draft/Cheque. Please check your email for order details. Thank You! www.DogSpot.in';
		//NKS
		}else{*/
		
		
		
		//NKS
		$msgSMS = 'We have received your Order ID:'.$cart_order_id.') of amount Rs. '.$totAmount.'. Payment method: '.$AOrderMethod[$mode].'. Please check your email for order details. Thank You! www.DogSpot.in';
		//NKS
		//}
		
	
		if($userid != 'tradus'){// No email if the userid is tradus
		// Check Email Status--------------------------------------------
		$rowOrdEmail=query_execute_row("SELECT review_body FROM section_reviews WHERE review_section_id = '$cart_order_id' AND review_section_name='shop-order' AND review_body='orderResponseEmailSend'");
		//echo"<hr>".$rowOrdEmail["review_body"];
		if($rowOrdEmail["review_body"] != 'orderResponseEmailSend'){
		$resultinsertlog = query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body, c_date, user_ip) 
										VALUES ('$userid', 'shop-order', '$cart_order_id', 'orderResponseEmailSend', NULL,'$ip')");
		// Check Email Status--------------------------------------------
		
//echo $mailBody;

// function to check item status of hard items./////
CheckHardItemStatus($cart_order_id);
// ends here by sagar..........................

//NKS-START for SMS
		$mobileno = isValidMobile($mobile1);
		if($mobileno != 0){
			$msg=urlencode($msgSMS);
			$url = "http://sms.admaxitsolutions.com/sendurlcomma.aspx?user=20055248&pwd=rk26pb&senderid=DOGSPOT&mobileno=$mobileno&msgtext=$msg";
			fetch_url($url);
		}}}// No email if the userid is tradus
//NKS-END for SMS
//Insert the order details-------------------------------------------------------------------------------------------
	if($_COOKIE['bh_conv']){
		$conversion_type='CruzeConnect';
	}
	$ua=getBrowser();
	$user_browser=$ua['name'].' '.$ua['version'];
	$user_platform=$ua['platform'];
	$user_agent=$ua['userAgent'];
	$resultinsertlog = query_execute("INSERT INTO shop_transactions (order_id, userid, session_id, order_transaction_id, order_transaction_amount, order_status, order_message, paymentid, mode, isflagged, datecreated, order_user_ip, conversion_type, 	conversion_data, user_browser, user_platform, user_agent) 
										VALUES ('$cart_order_id', '$userid','$session_id','','$totAmount','','','','$mode','', NULL,'$ip', '$conversion_type', '".$_COOKIE['bh_conv']."', '$user_browser', '$user_platform', '$user_agent')");

	postdatatocruzeconnect($Amount, $cart_order_id, $conversion_type);
//Insert the order details END----------------------------------------------------------------------------------------- 

?>
<?
$rorder_amount=round($rowOrd["order_amount"]);
?>
<!-- Google Code for Sales/purchase Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1018004414;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "egMHCMLV2QIQvoe25QM";
var google_conversion_value = <?=$rorder_amount?>;

<?php /*?>if (<?=$rorder_amount?>) {
  google_conversion_value = <?=$rorder_amount?>;
}<?php */?>
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1018004414/?value=<?=$rorder_amount?>&amp;label=egMHCMLV2QIQvoe25QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1018004414/?value=1375&amp;label=egMHCMLV2QIQvoe25QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?
if($order_status == 0){ 
?>
<script type="text/javascript">
	var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1552614-5']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_addTrans',
    '<?=$cart_order_id?>',           // order ID - required
    'DogSpot',  // affiliation or store name
    '<?=$rowOrd["order_amount"]?>',          // total - required
    '',           // tax
    '',              // shipping
    '<?=$ship_city?>',       // city
    '<?=$ship_state?>',     // state or province
    'India'             // country
  ]);
<?
foreach($arrayItemAnaCart as $ItemAna){
	$ia=explode('|', $ItemAna);
?>  
  _gaq.push(['_addItem',
   '<?=$cart_order_id?>',           // order ID - necessary to associate item with transaction
   '<?=$ia[0]?>',           // SKU/code - required
   '<?=$ia[1]?>',        // product name
      <? // get item category
   if($ia[4]==0){
	   $rowCat=query_execute_row("SELECT category_name FROM shop_category as c, shop_item_category as i WHERE c.category_id = i.category_id AND i.item_id='".$ia[0]."' AND i.category_id != '0'");
   }else{
	   $rowCat=query_execute_row("SELECT category_name FROM shop_category as c, shop_item_category as i WHERE c.category_id = i.category_id AND i.item_id='".$ia[4]."' AND i.category_id != '0'");
   }
   ?>
   '<?=$rowCat["category_name"]?>',   // category or variation
   '<?=$ia[2]?>',          // unit price - required
   '<?=$ia[3]?>'               // quantity - required
  ]);
<? }?>  
  _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<? $rowCatOrder=query_execute_row("SELECT item_id FROM `shop_cart` WHERE cart_order_id = '$cart_order_id' ORDER BY item_price desc");
$cart_item_id=$rowCatOrder['item_id'];
$rowitemdetail=query_execute_row("SELECT name,nice_name,short_description,price,item_parent_id FROM `shop_items` WHERE item_id = '$cart_item_id'");
$item_parnt=$rowitemdetail['item_parent_id'];
$rowitemimage=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id = '$cart_item_id' ORDER BY position desc");
if($rowitemimage['media_file']=='') {
$rowitemimage=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id = '$item_parnt' ORDER BY position desc");
$media_file=$rowitemimage['media_file'];
}
else {
	$media_file=$rowitemimage['media_file'];
}
$rowdiscouncode=query_execute_row("SELECT discount_code FROM shop_discount_main WHERE discount_id = '$order_discount_id' ")?>

<?php /*?><!-- ViralMint Meta Tag -->
<meta property="vm:title" content="<?=$rowitemdetail['name']?>" />
<meta property="vm:description" content="<?=$rowitemdetail['short_description']?>" />
<meta property="vm:link" content="<?="https://www.dogspot.in/".$rowitemdetail['nice_name']?>/" />
<meta property="vm:image" content="<?="https://www.dogspot.in/shop/item-images/orignal/".$media_file?>" />
<meta property="vm:uid" content="<?=$rowOrd['userid']?>" />
<!-- ViralMint Meta Tag -->

<noscript><img src="http://tracker.viralmint.com/sales/1365080648/<?=$rowitemdetail['price']?>/<?=$cart_order_id?>?coupon_code=<?=$rowdiscouncode['discount_code']?>"/></noscript><script src="http://tracker.viralmint.com/sales/1365080648/<?=$rowitemdetail['price']?>/<?=$cart_order_id?>?<?=$rowdiscouncode['discount_code']?>"></script><?php */?>
<? }?>
<? $sgUrl = '/'.$rowitemdetail['nice_name'].'/';?> 
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>