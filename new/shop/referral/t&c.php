<style>

.black_overlay{

display: none;

position: absolute;

top: 0%;

left: 0%;

width: 100%;

height: 100%;

background-color: black;

z-index:1001;

-moz-opacity: 0.6;

opacity:.80;

filter: alpha(opacity=90);

}

.white_content {

display: none;

position: absolute;

top: 10%;

left: 10%;

width: 80%;

height:80%;

padding: 16px;


border: px solid grey;

background-color: white;


z-index:1002;

overflow: auto;

}

.cross
{
margin-right:33px;
margin-top:20px;

float:right;
}
.input-bts
{
margin-top:148px;
}
img#share_button {cursor: pointer;}
</style>
<script type="text/javascript" language="JavaScript"><!--
function HideContent(d) {
document.getElementById(d).style.display = "none";
}
function ShowContent(d) {
document.getElementById(d).style.display = "block";
}
function ReverseDisplay(d) {
if(document.getElementById(d).style.display == "none") { document.getElementById(d).style.display = "block"; }
else { document.getElementById(d).style.display = "none"; }
}
//--></script>
<!-------------------- T & C POPUP---------------->
<script type="text/javascript">

function ld()

{

document.getElementById('fade').style.display='block'

document.getElementById('light').style.display='block'

}

</script>



<!------------pop up on submit--------------->
<div id="fade" class="black_overlay"></div>
<div id="fade1" class="black_overlay"></div>
<div id="light" class="white_content">
<div class="cross">

<input type="image" src="/new/pix/new/close.jpg" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" title="close" name="close"  id="close" alt="close"/>
</div>
<center>



<table>
<h1 align="left">Terms and Conditions for DogSpot referral program</h1>

<p align="left"> 
Following are the terms of the referral program by DogSpot.com, a property of PetsGlam ServicesPvt. Ltd. (hereinafter the "Company" or "DogSpot"</p>
<h1 align="left">Valid referrals</h1>

          <p align="left">  A valid referral is defined as follows:<br/><br/>
            -	The referred user is a new member of DogSpot <br/></br/>
            -	The order address (placed by the referred user) should be a new address. Orders to one shipping address will count for only one valid referral<br/></br>
            -	The referred user is not a member of DogSpot via a different email address<br/></br>
            -	The referral is a 1-on-1 personal referral to a person known to the referee. Public sharing of coupons on deal sites and other media would not be counted as referrals. (Affiliate program is the appropriate channel for people who wish to make a commercial gain through sales of our products)<br/></br></p>
        <p style="background-color:#9FF"align="left">     -	 Each customer would be eligible to refer upto 10 friends – any extension beyond 10 referrals would need to be requested for and applied for by email at info@DogSpot.com <br/></br> </p>
         <p align="left">   - Referral coupon codes are not site wide. They may not be applicable on selected categories or products.<br/></br></p>
            
<p align="left" >In Brief, the referral to us means a new customer who is personally introduced to our company by an existing customer. Any violation of this concept would not be considered for the reward points & Incentives.</p>

<h1 align="left"><strong>Program Termination</strong></h1>

<p align="left">DogSpot reserves the right in its sole discretion, to terminate the Program without giving any notice to this effect by means of a posting on the homepage of www.DogSpot.com and/or via another method of notice, as deemed appropriate by Company in its sole discretion. Termination can be for any reason. In such event, unless otherwise stated in the notice, the Program will terminate at 11:59 PM IST on the 3rd calendar day following the day on which such notice of termination was given. Any Credits/Points previously earned but not used or redeemed prior to the effective termination date will be forfeited</p>
<h1 align="left"><strong>Program Changes</strong>.</h1>
<p align="left">You understand that your continued membership to DogSpot via the DogSpot Account Agreement and/or your participation in this Program constitutes your consent to agreement to abide by the most current version of these terms and conditions (the "Program Terms"). Company may at any time revise these terms by updating the Program Terms. You agree to be bound by subsequent revisions and agree to review the Program Terms periodically for changes to the terms. The most updated version of the Program Terms will always be available for your review on <a style="background:#00F" href="www.DogSpot.in"> www.DogSpot.in </a><p align="left" style="background-color:#CCF">Credits/Points cannot be redeemed other than DogSpot eShop</p> 
<h1 align="left"><strong>Restrictions on redemption of credits; Forfeiture</strong></h1>
<p align="left">Upon the effective date of termination of the Program, all unused Credits/Points will be forfeited and no compensation will be given. Additionally, each credit obtained via the Referral Program will expire within 6 months. All unused Credits/Points are then forfeited and no compensation will be given. Credits/Points issued to you cannot be transferred to another member's account or pooled with Credits/Points in another member's account. Credits/Points may only be used for orders by the registrant for whom the account was created. While every reasonable effort will be made to maintain and accurately reflect the transactions in your account (including but not limited to your balance of Credits/Points), Company is not responsible for technical errors which may cause incorrect information to be reflected.</p><p align="left" style="background-color:#CCF">DogSpot solely has the right to stop this program without any prior intimation.</p>
<p align="left">The sale, trading, transfer, assignment, combining, or barter of Credits/Points or use of Credits/Points for any purpose other than as set forth in these Program Terms is prohibited.</p>
<p align="left">Points have no cash value, and cannot be purchased or redeemed for cash or credit. Credits likewise cannot be redeemed for cash, but can only be applied to purchases from the Company through otherwise valid and accepted orders.</p>
<p align="left">Company reserves the right to reject any order deemed, in its sole discretion, to violate these Program Terms in letter or in spirit, and reserves the right to investigate and verify the legitimacy of any use of Credits/Points. You are advised to print out a copy of your registration information, the order confirmation screens, the conversion request confirmation screens (if applicable), the order status screen and account information screens for your records. In all matters relating to the interpretation and application of these Program Terms, the decisions of Company shall be final and binding in all respects.</p>
<h1 align="left"><strong>Disqualification</strong>.</h1>
<p align="left">Company reserves the right, in its sole discretion, to disqualify any individual it finds, in its sole discretion, to be tampering with the operation of the Program (including but not limited to the submission of an order or the acquisition of Credits/Points), or www.DogSpot.com or, to be in violation of the Account Agreement, the Terms of Use of www.DogSpot.com or these Program Terms; or to be acting in a non-sportsmanlike or disruptive manner, or with the intent to annoy, abuse, threaten or harass any other person.</p>
<p align="left">Any person identified to be sharing the referral coupon on a public forum or deal site would be disqualified and the coupon would not be admitted.</p>
<p align="left">qualified for any reason whatsoever that participant's Credits/Points will also be forfeited. Any person who attempts to defraud Company in any way will also be subject to prosecution as per the law.</p> 

</p>

</table>


</center>

</div>