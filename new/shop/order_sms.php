<?php
require_once('../../constants.php');
/*
* API for sending order recieved and disptach message to customer
* Date: 24 Nov- 2014
* By : Umesh
*/ 
	
	require_once($DOCUMENT_ROOT.'/session.php');
	require_once($DOCUMENT_ROOT.'/database.php');
	require_once($DOCUMENT_ROOT.'/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/functions.php');
	require_once($DOCUMENT_ROOT.'/arrays.php');
	
	// API details 
	$url = "http://www.teleshoppe.com/serv/BulkPush/index1.php"; 
	$api_username = "dogspot";
	$api_pwd = "12345678";
	$errCode = "1"; // for fails and '0' for success
	 
	// Sent SMS When Order Received 
	
	$orderId = "137091";
	$mode = "OrderDispatched";
	
	if($orderId!=''){
		
		if($mode=="OrderReceived"){
			$customer_details = query_execute_row("SELECT COUNT(*) AS num, soa.address_name, soa.address_phone1, soa.address_phone2, sc.item_id, si.name FROM shop_order_address as soa JOIN shop_cart as sc ON sc.cart_order_id=soa.order_id JOIN shop_items as si ON si.item_id=sc.item_id WHERE soa.order_id='$orderId' AND soa.address_type_id='1' LIMIT 1");
		
			$customerName = $customer_details['address_name'];
			$customerPhone1 = $customer_details['address_phone1'];
			$customerPhone2 = $customer_details['address_phone2'];
			$num_reocrd = $customer_details['num'];
			$itemName = $customer_details['name'];	
			
			$message = "Dear $customerName, Your order (ID: $orderId) has been received. Total no of item $num_reocrd";
			$error = "0";
		} // End OrderReceived
		
		if($mode=="OrderDispatched"){
			
			$message = "Dear $customer_Name, Your order (ID: $orderId) has been Dispatched. You can track your Order at <a href='https://www.dogspot.in/'>dogspot.in</a> your Order Tracking No. is $order_Tracking.";
			
			$errCode = "0";
			$customerPhone1 = "9818511886";
			$customerPhone2 = "9958871177";
		} //End OrderDispatched
		
		if($errCode=="0"){
			// SMS Details
		
			$type = "text";
			$sender = "DOGSPT";
			
			if($customerPhone1!='' && $customerPhone2!=''){
				$param = "user=$api_username&pass=$api_pwd&message=$message&msisdn=$customerPhone1,$customerPhone2&sender=$sender&type=$type";	
			}
			elseif($customerPhone1!='' && $customerPhone2==''){
				$param = "user=$api_username&pass=$api_pwd&message=$message&msisdn=$customerPhone1&sender=$sender&type=$type";
			}
			else{
				$param = "user=$api_username&pass=$api_pwd&message=$message&msisdn=$customerPhone2&sender=$sender&type=$type";
			}
			

			if($customerPhone1!='' || $customerPhone2!=''){
				$response1 = get_web_page12($url,$param);
				$response = explode(",",$response1);
				$resp = $response[0];
			}
			
		}//End ErrCode		
		
	}// End Order Id
	
		
	function get_web_page12($url, $curl_data){
    	$options = array(
	        CURLOPT_RETURNTRANSFER => true,         // return web page
	        CURLOPT_HEADER         => false,        // don't return headers
	        CURLOPT_FOLLOWLOCATION => true,         // follow redirects
	        CURLOPT_ENCODING       => "",           // handle all encodings
	       	// CURLOPT_USERAGENT      => "spider",     // who am i
	        CURLOPT_AUTOREFERER    => true,         // set referer on redirect
	        CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
	        CURLOPT_TIMEOUT        => 120,          // timeout on response
	        CURLOPT_MAXREDIRS      => 10,           // stop after 10 redirects
	        CURLOPT_POST           => 1,            // i am sending post data
	        CURLOPT_POSTFIELDS     => $curl_data,    // this are my post vars
	        CURLOPT_SSL_VERIFYHOST => 0,            // don't verify ssl
	        CURLOPT_SSL_VERIFYPEER => false,        //
	        CURLOPT_VERBOSE        => 1                //
    	);

	    $ch      = curl_init($url);
    	curl_setopt_array($ch,$options);
    	$content = curl_exec($ch);
    	$err     = curl_errno($ch);
    	$errmsg  = curl_error($ch) ;
    	//$header  = curl_getinfo($ch);
    	curl_close($ch);

		//$header['errno']   = $err;
 		// $header['errmsg']  = $errmsg;
  		//$header['content'] = $content;
    	return $content.",".$errmsg;
    }
	
?>