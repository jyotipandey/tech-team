<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$sitesection = "shop";
echo $session_id = session_id();
if($userid != 'Guest'){
	//$resultinsert = query_execute("UPDATE shop_cart SET userid = '$userid' WHERE session_id = '$session_id'");
	header("Location: /new/shop/billingshipping.php");
	ob_end_flush();
	exit();
}
// Google Connect Settings--------------------------------
require_once ($DOCUMENT_ROOT.'/googleconnect/openid.php');
$openid = new LightOpenID("https://www.dogspot.in");
$openid->identity = 'https://www.google.com/accounts/o8/id';
$openid->required = array(
	'namePerson/first',
	'namePerson/last',
	'contact/email',
	'birthDate',
	'person/gender', 
 	'contact/postalCode/home',
	'contact/country/home',
	'pref/language',  
	'pref/timezone',
);
$openid->returnUrl = 'https://www.dogspot.in/googleconnect/glogin.php?refUrl=/new/shop/login-checkout.php';
// Google Connect Settings--------------------------------
?>

<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign in to your account | DogSpot</title>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<?php /*?><link rel="stylesheet" type="text/css" href="/new/css/main.css" media="all"  />
<link rel="stylesheet" type="text/css" href="/new/css/headfoot.css" media="all"  />
<link rel="stylesheet" type="text/css" href="/new/css/shop.css" media="all"  />
<link rel="stylesheet" type="text/css" href="/new/css/dropdown.vertical.css" media="all"  /><?php */?>
<style type="text/css">
.socialimgBox{
	width: 180px;
	margin-right: auto;
	margin-left: auto;
	padding-top: 5px;
	padding-right: 7px;
	padding-bottom: 7px;
	padding-left: 5px;
}
.brdark{
	border: 1px solid #aaaaaa;
	padding-top: 7px;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 6px;
}
#dlbox{
	margin-top: 10px;
	padding: 10px;
	display: none;
}

</style>
<?php /*?><script src="/jquery/jquery-latest.js" type="text/javascript"></script>
<script src="/jquery/jquery.validate-1.9.js" type="text/javascript"></script>
<script src="/js/shaajax2.1.js" type="text/javascript"></script>
<script src="/new/js/application.js" type="text/javascript"></script><?php */?>
<script type="text/javascript">
 $(document).ready(function() {
	  jQuery("#formGuest").validate({
		  
	});
	
$('#fbbox img').hover(
       function(){ $('#fbbox').removeClass('shadowall').addClass('brdark') }
)
$('#fbbox img').mouseout(
		function(){ $('#fbbox').removeClass('brdark').addClass('shadowall') }
)
$('#gbox img').hover(
       function(){ $('#gbox').removeClass('shadowall').addClass('brdark') }
)
$('#gbox img').mouseout(
		function(){ $('#gbox').removeClass('brdark').addClass('shadowall') }
)
$('#dbox img').hover(
       function(){ $('#dbox').removeClass('shadowall').addClass('brdark') }
)
$('#dbox img').mouseout(
		function(){ $('#dbox').removeClass('brdark').addClass('shadowall') }
)
$('#dbox').click(function(){	$('#dlbox').slideDown()})
$('#dlboxc').click(function(){$('#dlbox').slideUp()})	
});

</script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

	<!-- greenstrip 2 -->  
    	<div class="categoryGreenStrip">
        	<div class="cont980">
              <h1><a href="#" class="checkOut">Begin Checkout</a></h1>
              <div class="vs20"></div>
                <!-- left container -->
             
                <div class="cont300" style="margin-right:12px; width:343px;">
                	<div class="beginCheckOut">
                    	<h4><a href="#">Login to DogSpot with </a></h4>
                        
                      <!--<p>Sign in to view saved items for faster checkout !</p>-->
                      <div class="socialimgBox shadowall radall bs" id="fbbox"><a href="#" onClick="fblogin();return false;"><img src="/images/facebook-logo-large.png" width="180" height="61" alt="Facebook" /></a></div>
                      <div class="socialimgBox shadowall radall bs" id="gbox" style="margin-top:10px;"><a href="<?php echo $openid->authUrl() ?>"><img src="/images/google-logo-large.png" width="180" height="61" alt="Facebook" /></a></div>
                      <div class="socialimgBox shadowall radall bs" id="dbox" style="margin-top:10px;"><a href="javascript:voide()"><img src="/images/dogspot-logo-large.png" width="180" height="61" alt="Facebook" /></a></div>
                      <div class="radall shadowall bs" id="dlbox">
                      <div class="vs15" style=" text-align:right"><a href="javascript:void()" id="dlboxc">x</a></div>
                           <form id="form1" name="form1" method="post" action="/login.php">
                            	<label>Dogspot ID / Email ID</label><br />
                                <input name="useridNew" type="text" id="useridNew" style="width:220px;"/>
                                
                                <label>Password</label><br />
                                <input name="password" type="password" id="password" style="width:220px;"/>
                                
                                <p><a href="/forgot_password.php" onclick="return hs.htmlExpand(this, { contentId: 'highslide-tc', objectType: 'iframe',	preserveContent: false, objectWidth: 600, objectHeight: 300} )">Forgot your password?</a></p>
                                
                                <input name="Login" type="image" id="Login" style="border:0px;" src="/new/pix/sigin_btn.gif" alt="Sign in"/>
                                <input name="Login" type="hidden" id="Login" value="     Sign in     " />
								<input name="refUrl" type="hidden" value="/new/shop/login-checkout.php" />
                                
                      </form>
                      </div>
                    </div>
                </div><!-- left container -->
                
                <!-- right container -->
                <div class="cont660 beginCheckOut" style="margin-right:0px; width:580px; height:280px;">
                	<h4><a href="#">New Customer! </a> Express to proceed</h4>
                   
                  <!--<p>New to our family of sites? It’s easy to register below and you’ll be ready to shop. </p>-->
                  <div class="vs15"></div>
                    <form id="formGuest" name="formGuest" method="post" action="/new/shop/billingshipping.php">
                            	<label>Email Address</label><br />
                                <input type="text" name="u_email" id="u_email" class="required email" style="width:220px;"/>
                                
                                
                                <p>
                                <input name="Checkout" type="image" id="Checkout" style="border:0px;" src="/new/pix/checkout_btn.gif" alt="Checkout"/>
                                </p>
                  </form>
                   
                </div><!-- right container -->
                <div class="cb"></div>
       		</div>
        </div><!-- greenstrip 2 -->
     
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
