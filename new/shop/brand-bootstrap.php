<?php
if(!$DOCUMENT_ROOT){require_once('../../constants.php');}
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT . '/arrays.php');
$sitesection = "shop";
$session_id  = session_id();
if($section[0]=='Pedigree' || $section[0]=='Scoobee' || $section[0]=='Farmina' || $section[0]=='Drools'  || $section[0]=='Whiskas'  || $section[0]=='Bayer'  || $section[0]=='Petcare' || $section[0]=='Pethead'){
	$section[0]=strtolower($section[0]);
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/$section[0]/");
	}
	$dty=strlen($section[0]);
	for($i=0;$i<$dty;$i++){
	if(strcspn($section[0], 'ABCDEFGHJIJKLMNOPQRSTUVWXYZ')==$i){
	$section[0]=strtolower($section[0]);
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/$section[0]/");
	exit();	
	}
	}
$section[0]=strtolower($section[0]);	
$qdata = query_execute("SELECT * FROM shop_brand WHERE BINARY brand_nice_name='$section[0]'");
//NKS FOR 404
$rowTotal = mysql_num_rows($qdata);
if ($rowTotal == 0) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();
} ////END NKS FOR 404
$rowdat   = mysql_fetch_array($qdata);
$domain_id=$rowdat['domain_id'];
// function to redirect according to Domain....
//updatedomain($domain_id,$section[0]);


// function to redirect according to Domain ENDS HERE....
$brand_id = $rowdat["brand_id"];
$brand_status = $rowdat["brand_status"];
if($brand_status=='0') {
	header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();
	
}
// END
//------------Show
$maxshow  = 400;
if (empty($show)) {
    $show = 0;
} else {
    $show = $show - 1;
}
$showRecord   = $show * $maxshow;
$nextShow     = $showRecord + $maxshow;
//-------------Show
// Custom Variable for Google analytics
$CustomVar[2] = 'Category|Brand';
$CustomVar[3] = 'Sub-Category|' . $rowdat["brand_name"];
$ant_section='Brand';
$ant_sectionCheck='Brand';
$ant_category=$rowdat["brand_name"];
$ant_page="";
$title=$rowdat["brand_title"]." $brand_id | DogSpot.in";
$keyword=$rowdat["brand_keyword"]." $brand_id , DogSpot Brand" ;
$desc=$rowdat["brand_description"] ." DogSpot Brand DogSpot.in";
 
    $alternate="http://m.dogspot.in/$section[0]/";
	$canonical="https://www.dogspot.in/$section[0]/";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='shop';

require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
?>

 <?
 $imglink="https://ik.imagekit.io/2345/tr:h-130,w-360/shop/item-images/orignal/". $rowdat["brand_logo"];
?>
<section class="product-list-sec" itemscope itemtype="http://schema.org/Product">
 <div class="about-cat-sec" style="background: #f4f4f4;padding: 15px 0px;">
<div class="container">
<h1 class="about-cat-headline"><?= $rowdat["brand_name"] ?></h1>
<div class="row" >
<div class="col-xs-12 col-sm-2 col-md-2 " id="ShopBrandImgBox" style="padding-bottom:10px"><img src="<?= $imglink; ?>" border="0" alt="<?= $rowdat["brand_name"] ?>" title="<?= $rowdat["brand_name"] ?>" align="middle" style="max-width:140px; height:auto;"/></div>
<div class="col-xs-12 col-sm-10 col-md-10" id="demo">
 <article style="overflow: hidden;"><? if($rowdat["brand_about"]) { ?><div class="about-cat-text"><?=preg_replace('/[^(\x20-\x7F)\x0A\x0D]*/','',stripallslashes(htmlspecialchars_decode($rowdat["brand_about"])))?></div> 
 <? }?></article>
</div>
</div>
</div>

</div>   
<div class="breadcrumbs" style="margin-bottom:30px;">
    <div class="container">
  <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
  <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList">
   <span itemscope itemtype="http://schema.org/ListItem">
      <a href="/dog-store/" itemprop="item"><span itemprop="name">Home</span>
      <meta itemprop="position" content="1" />
      </a>
      </span> <span> / </span>
      <span itemscope itemtype="http://schema.org/ListItem"><a href="/brand/" itemprop="item">
      <span itemprop="name">Brands</span>
      <meta itemprop="position" content="2" />
      </a></span>
      <span> / </span> 
      <span  itemscope itemtype="http://schema.org/ListItem">
          <span itemprop="name" class="active-bread"><?=$rowdat["brand_name"];?></span>
      <meta itemprop="position" content="3" />
      </span> 
  </div>
</div>
  </div>
</div>

    <div class="container">
  <div class="row">
<?
$qItem12 = query_execute("SELECT i.item_id FROM shop_items as i WHERE i.item_brand = '$brand_id' AND i.visibility='visible' AND stock_status='instock' AND i.item_display_status!='delete' AND i.type_id !='configurable' AND item_id!='805' AND item_id!='1283' and selling_price!=0 and price!=0 limit 100");
while ($rowItemP11 = mysql_fetch_array($qItem12)) {
	$Aitem_ItemidP3[]=$rowItemP11['item_id'];
}
$qItem13 = query_execute("SELECT i.item_id FROM shop_items as i,shop_item_affiliate as sia WHERE sia.item_id=i.item_id AND i.item_brand = '$brand_id' AND i.item_display_status!='delete' AND i.type_id !='configurable' AND i.item_id!='805' AND i.item_id!='1283' limit 100");
while ($rowItemP12 = mysql_fetch_array($qItem13)) {
	$Aitem_ItemidP3[]=$rowItemP12['item_id'];
	$Aitem_Not=$Aitem_Not.$rowItemP12['item_id'].',';
}

$notitem=rtrim($Aitem_Not,',');

$notCheck[]='';
	$uniquec = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
		foreach($uniquec as $uu){
			$getItemValue=query_execute_row("SELECT item_id,type_id FROM shop_items WHERE  item_id='$uu'");
			$getallitem[]=$getItemValue['item_id'];
			$selbreedname=query_execute_row("SELECT item_id,display_order FROM item_display_order WHERE  item_id='$uu' AND brand_id='$brand_id'");
			if($selbreedname['item_id']){
			$uniquedisplay[$selbreedname['item_id']]=$selbreedname['display_order'];
			$notCheck[]=$selbreedname['item_id'];
			}
			asort($uniquedisplay);
		}
		   $getremain=array_diff($getallitem,$notCheck);
	
		foreach($getremain as $getItem)
		{	
		$itemall.=intval($getItem).',';
		}
		$getItema=rtrim($itemall,',');
		$currentDate=date('Y-m-d');
		$backDate = date('Y-m-d', strtotime('-60 days'));
		if($getItema){
		$getItemget=query_execute("SELECT SUM(item_qty) as qty,sc.item_id FROM shop_cart as sc,shop_order as so WHERE sc.cart_order_id=so.order_id AND order_status='0' AND order_type='complete' AND delevery_status!='canceled' AND delevery_status!='cancelled' AND mode !='TEST' AND sc.item_id IN ($getItema) AND order_c_date>'$backDate' AND order_c_date<'$currentDate' group by item_id order by qty DESC");
		while($dataitem=mysql_fetch_array($getItemget))
		{
			$uniquedisplay[$dataitem['item_id']]=$dataitem['qty'];
			$notCheck[]=$dataitem['item_id'];
		}
		}
		foreach($uniquedisplay as $item=>$val){
			$unique[]=$item;
		}  
	$getremainaginI=array_diff($getallitem,$notCheck);
	foreach($getremainaginI as $getItemI)
		{
		$itemallI.=intval($getItemI).',';
		}
		$getItemaI=rtrim($itemallI,',');
		if($getItemaI){
		$getItemgetI=query_execute("SELECT SUM(item_qty) as qty,sc.item_id FROM shop_cart as sc,shop_order as so WHERE sc.cart_order_id=so.order_id AND sc.item_id IN ($getItemaI) AND order_c_date>'$backDate' AND order_c_date<'$currentDate' group by item_id order by qty DESC");
		while($dataitemI=mysql_fetch_array($getItemgetI))
		{
			$uniquedisplayI[$dataitemI['item_id']]=$dataitemI['qty'];
			$notCheck[]=$dataitemI['item_id'];
		}
		}
		foreach($uniquedisplayI as $item=>$val){
			$unique[]=$item;
		}
		 $getremainaginIA=array_diff($getallitem,$notCheck);
	   foreach($getremainaginIA as $getItemIA)
		{
		$itemallIA.=intval($getItemIA).',';
		}
		$getItemaIA=rtrim($itemallIA,',');
		if($getItemaIA){
		$getItemgetIA=query_execute("SELECT SUM(item_qty) as qty,sc.item_id FROM shop_cart as sc,shop_order as so WHERE sc.cart_order_id=so.order_id AND sc.item_id IN ($getItemaIA) AND order_c_date>'$backDate' AND order_c_date<'$currentDate' group by item_id order by qty DESC");
		while($dataitemIA=mysql_fetch_array($getItemgetIA))
		{
			$uniquedisplayIA[$dataitemIA['item_id']]=$dataitemIA['qty'];
			$notCheck[]=$dataitemIA['item_id'];
		}
		}
		foreach($uniquedisplayIA as $item=>$val){
			$unique[]=$item;
		}
		 $getremainaginIAW=array_diff($getallitem,$notCheck);
		foreach($getremainaginIAW as $item){
			$unique[]=$item;
		}
		

$qItemAll = query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id,i.selling_price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND i.visibility='visible' AND stock_status='instock' AND i.item_display_status!='delete' AND i.type_id !='configurable' AND item_id!='805' AND item_id!='1283' and selling_price!=0 and price!=0");
$totrecord = mysql_num_rows($qItemAll);
$totrecordInstock=$totrecord;
$uniqueL=array_unique($unique);
foreach($uniqueL as $items){
//echo "SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id,i.selling_price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND item_id='$items' AND i.item_display_status!='delete' AND i.type_id !='configurable' AND item_id!='805' AND item_id!='1283' and selling_price!=0 and price!=0 limit 100";
$qItem12 = query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id,i.selling_price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND item_id='$items' AND i.item_display_status!='delete' AND i.type_id !='configurable' AND item_id!='805' AND item_id!='1283' and selling_price!=0 and price!=0 limit 100");
while ($rowItem = mysql_fetch_array($qItem12)) {
    $item_id        = $rowItem["item_id"];
    $name           = $rowItem["name"];
    $nice_name      = $rowItem["nice_name"];
    $price          = $rowItem["price"];
    $item_parent_id = $rowItem["item_parent_id"];
	$selling_price = $rowItem["selling_price"];
$sqlmediaco = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
$trecount=mysql_fetch_array($sqlmediaco);   
    if ($trecount['trecd'] != '0') {
		$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1";
    } else {
        $sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1";
    }
    
    $qdataM = query_execute("$sqlmedia");
	$rowdatM=mysql_fetch_array($qdataM);
   $imglink="https://ik.imagekit.io/2345/tr:h-160,w-200,c-at_max/shop/item-images/orignal/". $rowdatM["media_file"];
$itemE =10;
$getdataItem=query_execute_row("SELECT min(price) as price,mrp_price,count(id) as c,merchant_link FROM  shop_item_affiliate WHERE item_id='$item_id' AND price !='' AND price !='0'"); 
$minPrice=$getdataItem['price'];
$mrp_price=$getdataItem['mrp_price'];
if($getdataItem['c']>0){
$itemE=$getdataItem['c'];
}

?>
<div class="col-sm-6 col-md-3 box-product-outer" style="min-height:280px;">      
<div class="ds-product-image">
<? if($itemE ==10){
 if($selling_price>$price){?><div class="discount-sticker" > 
<?=number_format((($selling_price-$price)/$selling_price)*100); ?>% </div>
<? } }else{
 if($mrp_price>$minPrice){?><div class="discount-sticker" > 
<?=number_format((($mrp_price-$minPrice)/$mrp_price)*100); ?>
%</div>
<? } }?>
<a href="/<?=$nice_name?>/" style="height:160px; display:block" class="item<?=$item_id?>" >
<img src="<?=$imglink?>" alt="<?=$name?>" title="<?=$name?>" class="img-responsive" border="0" width="" height=""/></a></div>
<div class="ds-product-name"><a href="/<?=$nice_name?>/" style="height:50px; display:block" alt="<?=$name?>" title="<?=$name?>" itemprop="name"><?=$name;?></a></div>
<? if($itemE !=10 && $minPrice!='0' && $itemE>0){?>
  <div class="ds-product-price"><span><? echo 'Rs. '.number_format($minPrice); ?></span>
  <? if($mrp_price>$minPrice){ ?>
       <span class="ds-product-mrp"> Rs. <del><?=number_format($mrp_price,0)?></del></span><? }?>
</div>
 

<? }else{?>
                        <div class="ds-product-price"><span>Rs. <?=number_format($price)?></span>
                        <? if($selling_price>$price){$sa=$selling_price-$price;?>
                      <span class="ds-product-mrp"><del><? echo 'Rs. '.number_format($selling_price); ?></del></span>
                       <? }?>
                       </div>
                        <? }?>
<input type="hidden" name="userid" id="userid" value="<?=$userid?>" />
<? if($type_id=='configurable') { ?>
                       <div  class="ds_buy_now_cont"><a href="/<?=$nice_name?>/" class="ds_buy_now_btn">Buy Now</a></div>
                         <? }else{ 
?><? if($itemE !=10 && $minPrice!='0' && $itemE>0){?>
<div class="ds_buy_now_cont">
<a href="/<?=$nice_name?>/" class="read_more_btn " style="text-decoration:none; ">Read More</a>
  </div>
                     
<? }else{?>
	                   <div class="ds_buy_now_cont"> 
                        <a href="/<?=$nice_name?>/" onclick="addW('<?=$item_id?>')"  class="ds_buy_now_btn">Buy Now</a>
</div><? }?>
<? } ?>
</div>
<?
}
}
?>
</div></div>
</div>


<?php /*?><div class="wait_section lazyload_lodingbrand"><img class="gif_image" src="https://www.dogspot.in/images/indicator.gif" />
<div class="loding_more_items">Lodaing More Items for <span class="loding_more_catname"><?= $rowdat["brand_name"] ?></span>( <span id="remaining"><?=$totrecord?> remaining</span>)</div></div><?php */?><br />

<? if($notitem){
 $qItem = query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id, i.selling_price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND i.visibility='visible' AND item_id NOT IN ($notitem) AND stock_status='outofstock' AND i.item_display_status!='delete' AND i.type_id='simple' ");
$qItemAll  = query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id, i.selling_price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND i.visibility='visible' AND item_id NOT IN ($notitem) AND stock_status='outofstock' AND i.item_display_status!='delete' AND i.type_id='simple'");
}else{
	 $qItem = query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id, i.selling_price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND i.visibility='visible'  AND stock_status='outofstock' AND i.item_display_status!='delete' AND i.type_id='simple' ");
$qItemAll  = query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id, i.selling_price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND i.visibility='visible' AND stock_status='outofstock' AND i.item_display_status!='delete' AND i.type_id='simple'");

}$totrecord = mysql_num_rows($qItemAll);
$totalOutOfRecord=$totrecord;
if ($totrecord > 0) {
?>
 <div class="container">
   <div class="row">
<div class="col-md-12">
   <div class="ds_product_sold_out">
     <h3>Sold Out Products</h3></div>
    </div>
   
<?
    while ($rowItem = mysql_fetch_array($qItem)) {
        $item_id        = $rowItem["item_id"];
        $name           = $rowItem["name"];
        $nice_name      = $rowItem["nice_name"];
        $price          = $rowItem["price"];
        $item_parent_id = $rowItem["item_parent_id"];
		$selling_price = $rowItem["selling_price"];
        
$sqlmediaco = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
$trecount=mysql_fetch_array($sqlmediaco);   
    if ($trecount['trecd'] != '0') {
		$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC";
    } else {
        $sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC";
    }
        
        $qdataM = query_execute("$sqlmedia");
        
        $rowdatM = mysql_fetch_array($qdataM);
        $imglink="https://ik.imagekit.io/2345/tr:h-160,w-200,c-at_max/shop/item-images/orignal/". $rowdatM["media_file"];
        
?>
<div class="col-sm-6 col-md-3 box-product-outer" style="min-height:280px;">      
<div class="ds-product-image">
<? if($itemE ==10){
 if($selling_price>$price){?><div class="discount-sticker" > 
<?=number_format((($selling_price-$price)/$selling_price)*100); ?>% </div>
<? } }else{
 if($mrp_price>$minPrice){?><div class="discount-sticker" > 
<?=number_format((($mrp_price-$minPrice)/$mrp_price)*100); ?>
%</div>
<? } }?>
<a href="/<?=$nice_name?>/" style="height:160px; display:block" class="item<?=$item_id?>" >
<img src="<?=$imglinkm?>" alt="<?=$name?>" title="<?=$name?>" class="img-responsive" border="0" width="" height=""/></a></div>
<div class="ds-product-name"><a href="/<?=$nice_name?>/" style="height:50px; display:block" alt="<?=$name?>" title="<?=$name?>" itemprop="name"><?=$name;?></a></div>
<? if($itemE !=10 && $minPrice!='0' && $itemE>0){?>
  <div class="ds-product-price"><span><? echo 'Rs. '.number_format($minPrice); ?></span>
  <? if($mrp_price>$minPrice){ ?>
       <span class="ds-product-mrp"> Rs. <del><?=number_format($mrp_price,0)?></del></span><? }?>
</div>
 

<? }else{?>
                        <div class="ds-product-price"><span>Rs. <?=number_format($price)?></span>
                        <? if($selling_price>$price){$sa=$selling_price-$price;?>
                      <span class="ds-product-mrp"><del><? echo 'Rs. '.number_format($selling_price); ?></del></span>
                       <? }?>
                       </div>
                        <? }?>
<input type="hidden" name="userid" id="userid" value="<?=$userid?>" />
<? if($type_id=='configurable') { ?>
             <div class="ds_notify_btn_cont">
                      <a href="/<?=$nice_name?>/" class="link ds_notify_btn">Notify Me</a></div>
 </div>
                         <? }else{ 
?><? if($itemE !=10 && $minPrice!='0' && $itemE>0){?>
<div class="ds_buy_now_cont">
<a href="/<?=$nice_name?>/" class="read_more_btn " style="text-decoration:none;">Read More</a>
  </div>
                     
<? }else{?>
<div class="ds_notify_btn_cont">
                      <a href="/<?=$nice_name?>/" class="link ds_notify_btn">Notify Me</a></div>
 <? }?>
<? } ?>
</div>
<?
    }
}
?>
</div>
</div>
<script src="/bootstrap/js/readmore.min.js"></script>
<script>
    $('#info').readmore({
      moreLink: '<a href="#">Usage, examples, and options</a>',
      collapsedHeight: 384,
      afterToggle: function(trigger, element, expanded) {
        if(! expanded) { // The "Close" link was clicked
          $('html, body').animate({scrollTop: element.offset().top}, {duration: 100});
        }
      }
    });

    $('article').readmore({speed: 500});
  </script>
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');
?>
