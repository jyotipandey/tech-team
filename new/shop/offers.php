<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Offers</title>
<meta name="keywords" content="Offers" />
<meta name="description" content="Offers" />
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<link rel="stylesheet" type="text/css" href="/new/shop/css/offer-page.css" />
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="offer_page_container">
<!-- top banner start -->
<div class="offer_page_section">
<!-- banner left-->
<div class="offer_page_banner_left">
	 <? $tdate=date("Y-m-d H:i:s");
$getbannercat=query_execute_row("SELECT * FROM banners WHERE featured_on='deal-page' AND width='636' AND height='318' AND start_date <= '$tdate' AND end_date > '$tdate' AND status='active'");
	$banner_namecat=$getbannercat['banner_name'];
	$imgcat=$getbannercat['img'];
	$banner_linkcat=$getbannercat['banner_link']; ?>
<a href="https://www.dogspot.in/<?=$banner_linkcat?>"><img src="/allbanners/<?=$imgcat?>" width="636" height="318" alt="<?=$banner_namecat;?>" title="<?=$banner_namecat;?>"/></a>
<? ?></div>
<!-- banner left end-->
<!-- banner right-->
<div class="offer_page_banner_right">
<? $sqlbannercat3=query_execute_row("SELECT * FROM banners WHERE featured_on='deal-page' AND width='306' AND height='153' AND start_date <= '$tdate' AND end_date > '$tdate' AND status='active'"); 
	$banner_name3=$sqlbannercat3['banner_name'];
	$img3=$sqlbannercat3['img'];
	$banner_link3=$sqlbannercat3['banner_link'];
?>
<div class="offer_page_small_bannr"><a href="https://www.dogspot.in/<?=$banner_link3?>"><img src="/allbanners/<?=$img3?>" width="306" height="152"  alt="<?=$banner_name3;?>" title="<?=$banner_name3;?>"/></a></div>
<? $sqlbannercat4=query_execute_row("SELECT * FROM banners WHERE featured_on='deal-page' AND width='306' AND height='152' AND start_date <= '$tdate' AND end_date > '$tdate' AND status='active'"); 
	$banner_name4=$sqlbannercat4['banner_name'];
	$img4=$sqlbannercat4['img'];
	$banner_link4=$sqlbannercat4['banner_link'];?>
<div class="offer_page_small_bannr offer_mt13"><a href="https://www.dogspot.in/<?=$banner_link4?>"><img src="/allbanners/<?=$img4?>" width="306" height="152"  alt="<?=$banner_name4;?>" title="<?=$banner_name4;?>"/></a></div>
</div>
<!-- banner right end -->
</div>
<!-- top banner sec end -->
<!-- other top banner start-->
<div class="top_offer_section">
<div class="other_top_offer"><h1>Other top offers</h1></div>
<div class="other_offer_products">
<ul>
<?
$ci=1;
$sqlbannerca1t2=query_execute("SELECT * FROM banners WHERE featured_on='offer-page' AND width='223' AND height='223' AND start_date <= '$tdate' AND end_date > '$tdate' AND status='active'");
$coutrcat2=mysql_num_rows($sqlbannercat2);
while($getbannercat2=mysql_fetch_array($sqlbannerca1t2)){
	$banner_namecat2=$getbannercat1['banner_name'];
	$imgcat2=$getbannercat2['img'];
	$banner_linkcat2=$getbannercat2['banner_link']; ?>
<li><a href="https://www.dogspot.in/<?=$banner_linkcat2?>"><img src="/allbanners/<?=$imgcat2?>" width="223" height="223"  alt="<?=$banner_namecat2;?>" title="<?=$banner_namecat2;?>" /></a></li>
<? if($ci%4==0){?>               
    				</ul> <ul>           
<?  } $ci++; }?>
        </ul>

</div>
</div>
<!-- other top banner end-->

</div>
</body>
</html>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
