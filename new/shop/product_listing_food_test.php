<?
if(!$DOCUMENT_ROOT){require_once('../../constants.php');}
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-ArrayShopAttributeValue.php');

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection = "shop";
//------------Show
$maxshow = 200;
if(empty($show)) {
	$show = 0;
}else{
 $show = $show - 1;
}
 $showRecord = $show * $maxshow;
 $nextShow = $showRecord + $maxshow;
//-------------Show
$q=$shopQuery;
$ci=1;
$ci1=1;
if($q){	
	//echo $q;
	$query=escapeSolrValue($q, 'AND');
	$query=urlencode($query);
	
	$url = "http://198.154.232.170:8080/dogspotshop/select/?q=$query AND item_parent_id:0 AND visibility:visible AND stock_status:instock&version=2.2&rows=$maxshow&fl=* score&qf=name^2&indent=on";
	//	$url = "http://198.154.232.170:8080/dogspotshop/select/?q=name:$query^2 item_about:$query keyword:$query AND item_parent_id:0 AND visibility:visible AND stock_status:instock&version=2.2&rows=$maxshow&indent=on";
	$url = str_replace(" ","%20",$url);
	
	//echo $url;
	$result = get_solr_result($url);
	$totrecord = $result['TOTALHITS'];
	
	// search data  are saved in table search_records///
	$sqlinsert=query_execute("INSERT INTO search_results(search_section,search_term,search_results,search_date) values('shop','".addslashes($q)."',$totrecord,NULL)");		
	//================  
	
	if($totrecord>0){
		foreach($result['HITS'] as $row){
			$Aitem_brand[]=$row["item_brand"];
			$Aitem_price[]=$row["price"];
		}
	}
	
}else{
	// start here------------------------------------------------
	if($type_life){ //undefinedmature-
	$type_life1 = str_replace("undefined", "", $type_life);
	$type_life1=trim($type_life1,'-');
	$sbreedlife1=explode('-',$type_life1);
	if($sbreedlife1[0]){
	foreach($sbreedlife1 as $sblife1){
		$breedlsql1.=" OR j.life_stage='$sblife1' ";
		$printbreed_life1=$printbreed_life1.",".$sblife1;
	}
	$breedlsql1 = trim($breedlsql1,' OR ');
	$breedlsql1 = " AND ($breedlsql1)";
	}
	
}
if($item_brand){
	$item_brandfilter = str_replace("undefined", "", $item_brand);
	$item_brandfilter=trim($item_brandfilter,'-');
	$sbrandsfilter=explode('-',$item_brandfilter);
	if($sbrandsfilter[0]){
	foreach($sbrandsfilter as $sb1){
		$bsql1.=" OR i.item_brand=$sb1 ";
	$printbrand_name1=$printbrand_name1.",".$sb1;	
	}
	$bsql1 = trim($bsql1,' OR ');
	$bsql1 = " AND ($bsql1)";
	}
}

if($breedname){
	$breednamef = str_replace("undefined", "", $breedname);
	$breednamef=trim($breednamef,'-');
	$sbreedsf=explode('-',$breednamef);
	if($sbreedsf[0]){
	foreach($sbreedsf as $sbref){
		$breedsqlf.=" OR j.breed_id='$sbref' ";
	$printbreed_name1=$printbreed_name1.",".$sbref;
	}
	$breedsqlf = trim($breedsqlf,' OR ');
	$breedsqlf = " AND ($breedsqlf)";
	}
}

if($type_breed11){
	$type_breed11f = str_replace("undefined", "", $type_breed11);
	$type_breed11f=trim($type_breed11f,'-');
	$sbreedtf=explode('-',$type_breed11f);
	if($sbreedtf[0]){
	foreach($sbreedtf as $sbrtf){
		$breedtsqlf.=" OR j.breed_type='$sbrtf' ";
		$printbreed_type1=$printbreed_type1.",".$sbrtf;
	}
	$breedtsqlf = trim($breedtsqlf,' OR ');
	$breedtsqlf = " AND ($breedtsqlf)";
	}
}
if($s_price1){
	$s_price112 = str_replace("Rs", "", $s_price1);
	$s_price112 = str_replace("-", "|", $s_price112);
	$s_price112=trim($s_price112,'|');
	$sprice112=explode('|',$s_price112);
	$pc112=count($sprice112);
	if($pc112>1){
	$psql112= " AND i.price BETWEEN '".$sprice112[0]."' AND '".$sprice112[1]."'";
	}
}
if($s_weight){
	$s_weight12 = str_replace("Kg", "", $s_weight);
	$s_weight12 = str_replace("-", "|", $s_weight12);
	$s_weight12=trim($s_weight12,'|');
	$s_weight112=explode('|',$s_weight12);
	$wt112=count($s_weight112);
	if($wt112>1){
	$wsql112= " AND i.weight BETWEEN '".$s_weight112[0]."' AND '".$s_weight112[1]."'";
	}
}	
if($s_weightprice){
	$s_weightprice12 = str_replace("Rs", "", $s_weightprice);
	$s_weightprice12 = str_replace("-", "|", $s_weightprice12);
	$s_weightprice12=trim($s_weightprice12,'|');
	$s_weightprice112=explode('|',$s_weightprice12);
	$wtp112=count($s_weightprice112);
	if($wtp112>1){
	$wsps112= " AND i.weight_price BETWEEN '".$s_weightprice112[0]."' AND '".$s_weightprice112[1]."'";
	}
}
if($s_sort=='price_asc'){
	$stsort='i.price DESC,i.item_display_order ';
	$so='price_asc';
}elseif($s_sort=='price_desc'){
	$stsort='i.price ASC,i.item_display_order ';
	$so='price_desc';
}elseif($s_sort=='relevance'){
	$stsort='i.num_views DESC,i.item_display_order ';
	$so='relevance';
}
else{
	$stsort='i.item_display_order ,i.item_brand ';
}

if($show1==0){
	$sshow="AND i.stock_status='instock'";	
}


$qItemAll=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price,i.weight_price, i.item_brand,i.stock_status,i.created_at,i.weight,j.breed_id,j.breed_type,j.life_stage,i.item_display_order FROM breed_shop_items as j, shop_items as i WHERE j.item_category_id = '$category_id' AND i.visibility='visible' AND i.item_display_status!='delete' AND i.item_id=j.item_id $breedlsql1 $bsql1 $breedsqlf $breedtsqlf $psql112 $wsql112 $wsps112 $sshow");
$totrecord = mysql_num_rows($qItemAll);

$qItem=query_execute("SELECT i.item_id,i.item_brand, i.name, i.nice_name, i.price, i.selling_price,i.weight_price, i.item_parent_id,i.stock_status,i.created_at,i.weight,j.breed_id,j.breed_type,j.life_stage,i.item_display_order FROM breed_shop_items as j, shop_items as i WHERE j.item_category_id = '$category_id' AND i.visibility='visible'  AND i.item_id=j.item_id AND i.item_display_status!='delete' AND i.type_id='simple' $breedlsql1 $bsql1 $breedsqlf $breedtsqlf $psql112 $wsql112 $wsps112 ORDER BY $stsort ");

$qItemorg=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price,i.weight_price, i.item_brand,i.stock_status,i.created_at,i.weight,j.breed_id,j.breed_type,j.life_stage,i.item_display_order FROM breed_shop_items as j, shop_items as i WHERE j.item_category_id = '$category_id' AND i.visibility='visible' AND i.item_display_status!='delete' AND i.item_id=j.item_id");
$totrecord123 = mysql_num_rows($qItemorg);
//------------------------end here----------------------------
$qitem_breeds=query_execute("SELECT * from breed_shop_items as s,shop_item_category as c WHERE c.item_id=s.item_id AND s.item_category_id='$category_id'");
while($qitem_b = mysql_fetch_array($qitem_breeds)){
	$Abreed_id[]=$qitem_b["breed_id"];
	$Abreed_type[]=$qitem_b["breed_type"];
	$Alife_stage[]=$qitem_b["life_stage"];
	}
	$Abreed_type=array_unique($Abreed_type);
	$Alife_stage=array_unique($Alife_stage);
	$Abreed_id=array_unique($Abreed_id);
	asort($Abreed_type);
	asort($Alife_stage);
	asort($Abreed_id);
	//print_r($ANBbreed_type);
	/*asort($Abreed_id);
	asort($Alife_stage);
	asort($Abreed_type);*/	
//--------------------------------------------------------
while($rowItemall = mysql_fetch_array($qItemAll)){
	$Aitem_brand[]=$rowItemall["item_brand"];
	$Aitem_price[]=$rowItemall["price"];
	$Aweight[]=$rowItemall["weight"];
	$Aweight_price[]=$rowItemall["weight_price"];
	
}

while($rowItemsearch = mysql_fetch_array($qItemorg)){
	$A1item_brand[]=$rowItemsearch["item_brand"];
	$A1item_price[]=$rowItemsearch["price"];
	$A1weight[]=$rowItemsearch["weight"];
	$A1weight_price[]=$rowItemsearch["weight_price"];
	
}
if($totrecord123>0){
	$A1item_price=array_unique($A1item_price);
$A1weight=array_unique($A1weight);
$A1weight_price=array_unique($A1weight_price);
asort($A1weight_price);
foreach($A1weight_price as $w1tp){
	$ANA1weight_price[]=$w1tp;
}
$weightprice1_count=count($ANA1weight_price);
$weightprice1_count=$weightprice1_count-1;
$w1tp1=$ANA1weight_price[0];
$end1weightp= $ANA1weight_price[$weightprice1_count];
asort($A1weight);
foreach($A1weight as $w1t){
	$ANitem1_weight[]=$w1t;
}
$weight1_count=count($ANitem1_weight);
$weight1_count=$weight1_count-1;
$w1t1=$ANitem1_weight[0];
$end1weight= $ANitem1_weight[$weight1_count];
asort($A1item_price);
foreach($A1item_price as $a1p){
	$ANitem1_price[]=$a1p;
}
$price1_count=count($ANitem1_price);
$price1_count=$price1_count-1;
$end1num = $ANitem1_price[$price1_count];
$start1num = $ANitem1_price[0];
$s1t=$ANitem1_price[0];
$end1amount= $ANitem1_price[$price1_count];
	}
// Create filter------------------------------------------------------------
if($totrecord>0){
$Aitem_brand=array_unique($Aitem_brand);
$Aitem_price=array_unique($Aitem_price);
$Aweight=array_unique($Aweight);
$Aweight_price=array_unique($Aweight_price);
asort($Aweight_price);
foreach($Aweight_price as $wtp){
	$ANAweight_price[]=$wtp;
}
$weightprice_count=count($ANAweight_price);
$weightprice_count=$weightprice_count-1;
$wtp1=$ANAweight_price[0];
$endweightp= $ANAweight_price[$weightprice_count];

asort($Aweight);
foreach($Aweight as $wt){
	$ANitem_weight[]=$wt;
}
$weight_count=count($ANitem_weight);
$weight_count=$weight_count-1;
$wt1=$ANitem_weight[0];
$endweight= $ANitem_weight[$weight_count];

asort($Aitem_price);
foreach($Aitem_price as $ap){
	$ANitem_price[]=$ap;
}
$price_count=count($ANitem_price);
$price_count=$price_count-1;
$endnum = $ANitem_price[$price_count];
$startnum = $ANitem_price[0];
$st=$ANitem_price[0];
$endamount= $ANitem_price[$price_count];
/*if($startnum>=0){$startnum=0;}
elseif($startnum>=100){$startnum=100;}
elseif($startnum>=1000){$startnum=1000;}
elseif($startnum>=2000){$startnum=2000;}
elseif($startnum>=5000){$startnum=5000;}
elseif($startnum>=10000){$startnum=10000;}

if($endnum<=100){$rangee=20;$endnum=100;}
elseif($endnum<=1000){$rangee=200;$endnum=1000;}
elseif($endnum<=2000){$rangee=500;$endnum=2000;}
elseif($endnum<=5000){$rangee=1000;$endnum=5000;}
elseif($endnum<=10000){$rangee=2000;$endnum=10000;}
else{$rangee=10000;}

if($endnum){
	$ApriceItem=range($startnum,$endnum, $rangee);
}*/
}
if($category_about){
	$pageTitle=$category_title;
	$pageKeyword=$category_keyword;
	$pageDesc=$category_desc;
	$cat_name=$category_name;
	$pageabout=$category_about;
}else{
	$pageTitle=$q;
	$pageKeyword=$q;
	$pageDesc=$q;
}

// Create filter end --------------------------------------------------------
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$pageTitle?> </title>
<meta name="keywords" content="<?=$pageKeyword?> " />
<meta name="description" content="<?=$pageDesc?> " />
<? if($shopQuery){?>
<meta name="robots" content="noindex, nofollow">
<? }?>
<link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />
<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/shop.css" media="all" />
<link href="/new/css/dropdown.vertical.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/new/css/skin.css" rel="stylesheet" type="text/css" />
<!--used for display quick view-->
<link type="text/css" rel="stylesheet" media="screen" href="/new/css/solr_default.css" />
<link type="text/css" rel="stylesheet" media="screen" href="/new/css/solr_style.css" />
<!--close for display quick view-->
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<SCRIPT src="/new/js/myscript.js" type="text/javascript"></SCRIPT>
<!--<script type="text/javascript" src="/new/js/jquery-1.js"></script>-->
<script type="text/javascript" src="/js/shaajax2.1.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<link href="/new/css/toll-tip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/iepngfix_tilebg.js"></script>
 <link rel="stylesheet" href="/new/css/jquery-ui.css" />
<script type="text/javascript" src="/new/js/jquery/jquery-1.9.0.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="/new/js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="/new/js/jquery.jcarousel.min.js"></script>
<link href="/new/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/slide.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>

<script>
$(function() {
$(document).tooltip();
});

function buy_show(num){
	//alert('hello');
	$('#buy_nowbutton'+num).show();
	}
	function buy_show1(num){
	//alert('hello');
	$('#buy_nowbutton'+num).hide();
	}
</script>

<script type="text/javascript">

var query = location.href.split('#');
if(query[1]){
	window.location = query[0]+'?'+query[1];
}

jQuery(document).ready(function() {
    jQuery('#topstores').jcarousel();
});
jQuery(document).ready(function() {
    jQuery('#popularstores').jcarousel();
});
jQuery(document).ready(function() {
    jQuery('#lateststores').jcarousel();
});
</script>
<!--<style>#comment.fixed {
  position: fixed;
  top: 0;
}</style><script>
$(function () {
  
  var msie6 = $.browser == 'msie' && $.browser.version < 7;
  
  if (!msie6) {
    var top = $('#comment').offset().top - parseFloat($('#comment').css('margin-top').replace(/auto/, 0));
    $(window).scroll(function (event) {
      // what the y position of the scroll is
      var y = $(this).scrollTop();
      
      // whether that's below the form
      if (y >= top) {
        // if so, ad the fixed class
        $('#comment').addClass('fixed');
      } else {
        // otherwise remove it
        $('#comment').removeClass('fixed');
      }
    });
  }  
});
</script>-->


    <script>
	
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: <?=$s1t; ?>,
            max: <?=$end1amount;?>,
            values: [ <?= $st ?>, <?=$endamount?> ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "Rs" + ui.values[ 0 ] + "-Rs" + ui.values[ 1 ] );
				$( "#pmin" ).val( "Rs. " + ui.values[ 0 ]);
				$( "#pmax" ).val( "Rs. " + ui.values[ 1 ]);
            },
			change: function (event, ui) {
				
				searchFilter();
			}
        });
        $( "#amount" ).val( "Rs" + $( "#slider-range" ).slider( "values", 0 ) +
            "-Rs" + $( "#slider-range" ).slider( "values", 1 ) );
			//alert($( "#slider-range" ).slider( "values", 0 ));
			$( "#pmin" ).val( "Rs. " + $( "#slider-range" ).slider( "values", 0 ));
		$( "#pmax" ).val( "Rs. " + $( "#slider-range" ).slider( "values", 1 ));	
		
			
		});
		//----------------weight range------------------------
		$(function() {
        $( "#weight-range" ).slider({
            range: true,
            min: <?=$w1t1; ?>,
            max: <?=$end1weight;?>,
            values: [ <?= $wt1 ?>, <?=$endweight?> ],
            slide: function( event, ui ) {
                $( "#aweight" ).val( "Kg" + ui.values[ 0 ] + "-Kg" + ui.values[ 1 ] );
				$( "#wmin" ).val( ui.values[ 0 ]+" Kg");
				$( "#wmax" ).val( ui.values[ 1 ]+" Kg");
            },
			change: function (event, ui) {
				searchFilter();
			}
        });
        $( "#aweight" ).val( "Kg" + $( "#weight-range" ).slider( "values", 0 ) +
            "-Kg" + $( "#weight-range" ).slider( "values", 1 ) );
			//alert($( "#weight-range" ).slider( "values", 0 ));
		$( "#wmin" ).val($( "#weight-range" ).slider( "values", 0 )+" Kg");
		$( "#wmax" ).val($( "#weight-range" ).slider( "values", 1 )+" Kg");	
			
		});
		
		<!--weight per kg-->
		//----------------weight range------------------------
		$(function() {
        $( "#weightp-range" ).slider({
            range: true,
            min: <?=$w1tp1; //if($w1tp1==$wtp1){ echo $wtp1;}else{echo $w1tp1;} ?>,
            max: <?=$end1weightp; //if($end1weightp==$endweightp){ echo $endweightp;}else{echo $end1weightp;} ?>,
            values: [ <?= $wtp1 ?>, <?=$endweightp?> ],
            slide: function( event, ui ) {
                $( "#aweightp" ).val( "Rs" + ui.values[ 0 ] + "-Rs" + ui.values[ 1 ] );
				$( "#wminp" ).val( "Rs. " + ui.values[ 0 ]);
				$( "#wmaxp" ).val( "Rs. " + ui.values[ 1 ]);
            },
			change: function (event, ui) {
				searchFilter();
			}
        });
        $( "#aweightp" ).val( "Rs" + $( "#weightp-range" ).slider( "values", 0 ) +
            " - Rs" + $( "#weightp-range" ).slider( "values", 1 ) );
			//alert($( "#weight-range" ).slider( "values", 0 ));
		$( "#wminp" ).val( "Rs. " + $( "#weightp-range" ).slider( "values", 0 ));
		$( "#wmaxp" ).val( "Rs. " + $( "#weightp-range" ).slider( "values", 1 ));	
			
		});
		<!--end here weight per kg-->
		
		$(document).ready(function(){
		 $("a[name=some_name]").click(function(){
			
         var sort12 = $(this).attr("id");
		 $("#sort").val(sort12);
         //event.preventDefault();
			searchFilter();
       }); 
	  $("#resetdiv").hide();
	   });
	 $(document).ready(function(){ 
	   //--------------------------life stage---------------------
		var search_brands_life12 = document.getElementById('search_brands_life').value;
		var input_obj_lifebreed_test = document.getElementsByName('typestage');
		var x,fields;
		for (x in search_brands_life12){		
		fields = search_brands_life12[x];
			if(fields!=','){		
			for (i = 0; i < input_obj_lifebreed_test.length; i++) {
				//alert(input_obj_lifebreed_test[i].value);
			if(input_obj_lifebreed_test[i].value===fields){
			input_obj_lifebreed_test[i].checked = true;
					}
				}
			}
		}
	//-------------------------close for life stage ------------------------
	 //--------------------------breed---------------------
		var search_brands_breed12 = document.getElementsByName('search_brands_breed');
		var input_obj_breed_test = document.getElementsByName('namebreed');
		var x1,fields1,f1,fi;
		for (x1=0;x1<search_brands_breed12.length;x1++){		
		f1 = search_brands_breed12[x1].value;
		}
		fi=f1.split(/,/);
			for(var v=0;v<fi.length;v++){
				fields1=fi[v];
				//alert(fields1);			
			for (i = 0; i < input_obj_breed_test.length; i++) {
				//alert(input_obj_breed_test[i].value);
			if(input_obj_breed_test[i].value===fields1){
			input_obj_breed_test[i].checked = true;
					}
			}
		}
	//-------------------------close for breed ------------------------
	
	//--------------------------breed type---------------------
		var search_brands_btype12 = document.getElementById('search_brands_btype').value;
		var input_obj_breedtype_test = document.getElementsByName('typebreed');
		var x2,fields2;
		for (x2 in search_brands_btype12){		
		fields2 = search_brands_btype12[x2];
			if(fields2!=','){		
			for (i = 0; i < input_obj_breedtype_test.length; i++) {
				//alert(input_obj_lifebreed_test[i].value);
			if(input_obj_breedtype_test[i].value===fields2){
			input_obj_breedtype_test[i].checked = true;
					}
				}
			}
		}
	//-------------------------close for breed type ------------------------
	//--------------------------brand---------------------
		var search_brands_brand12 = document.getElementsByName('search_brands_brand');
		var input_obj_brand_test = document.getElementsByName('sbrands');
		var x3,fields3,fbrand,fb1;
		for (x3=0;x3<search_brands_brand12.length;x3++){		
		fb1 = search_brands_brand12[x3].value;
		}
			fbrand=fb1.split(/,/);
			
			for(var v1=0;v1<fbrand.length;v1++){
				fields3=fbrand[v1];
				
				
			for (i = 0; i < input_obj_brand_test.length; i++) {
				//alert(input_obj_lifebreed_test[i].value);
			if(input_obj_brand_test[i].value===fields3){
			input_obj_brand_test[i].checked = true;
					
				}
			}
		}
	//-------------------------close for brand ------------------------
		var s_sort123=$('#so').val();
		
		if(s_sort123=='relevance'){s_sort123='relevance';
		$("#a2").removeClass("displayContainer_rightTabactive");
		$("#a3").removeClass("displayContainer_rightTabactive");
		$("#a1").addClass("displayContainer_rightTabactive");
		$("#a2").addClass("displayContainer_rightTab");
		$("#a3").addClass("displayContainer_rightTab");
	}//add class $("#ship_name").addClass("required");
	if(s_sort123=='price_asc'){
		s_sort123='price_asc'; 
		$("#a1").removeClass("displayContainer_rightTabactive");
		$("#a3").removeClass("displayContainer_rightTabactive");
		$("#a2").addClass("displayContainer_rightTabactive");
		$("#a1").addClass("displayContainer_rightTab");
		$("#a3").addClass("displayContainer_rightTab");
		
	  }
	if(s_sort123=='price_desc'){
		s_sort123='price_desc'; 
		$("#a1").removeClass("displayContainer_rightTabactive");
		$("#a2").removeClass("displayContainer_rightTabactive");
		$("#a3").addClass("displayContainer_rightTabactive");
		$("#a1").addClass("displayContainer_rightTab");
		$("#a2").addClass("displayContainer_rightTab");
		}
		
	    });
	   
 
		
    </script>
    <script>
	function addWItemToCart(qty,itemid,price)
{
	
ShaAjaxJquary("/new/shop/cart.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "", '', '', 'POST', 'loading', '<img src="/images/loading-round.gif" />','REP');
$("#topItemCount").load("/new/shop/cart.php #cartItemCount");
$("#topCartTotal").load("/new/shop/cart.php #cartTotal");
	
}
	
		function callsort(){
		var sort12=$("a[name=some_name1]").attr("id");	
		 
		if(sort12!=''){sort12='';
		$("#a1").removeClass("displayContainer_rightTabactive");
		$("#a2").removeClass("displayContainer_rightTabactive");
		$("#a3").removeClass("displayContainer_rightTabactive");
		}
		 $("#sort").val(sort12);
         //event.preventDefault();
			searchFilter();
       
		}
		function callbrand12(brs){
		
			$("#some_brands").val(brs);	
			searchFilter();
			}
		
		function callsortbreed(bid1){
			$("#some_breed_name").val(bid1);	
			searchFilter();
			}
		function callsortbreedtype(printbreed){
			<!--var some_breed123=$("a[name=some_breed_type]").attr("id");-->
			$("#some_breed_name1").val(printbreed);	
			searchFilter();
			}
		function callsortbreedstyle(printbreed_life1){
			$("#some_breed_name2").val(printbreed_life1);	
			searchFilter();
			}	
			
</script>

<script>
IEPNGFix.blankImg = 'blank.gif'; </script>
    <link href="/new/css/all.css" rel="stylesheet" type="text/css">
    <script>
    /*$(function() {
        $( "#logo" ).buttonset();
    });*/
    </script>
    
<script>
 
function searchFilter(){
	//alert($( "#slider-range" ).slider( "values", 0 ));
	var vurl='';
	var items_brand='';
	var input_obj = document.getElementsByName('sbrands');
	var br_brand=$("#some_brands").val();
	
	for (i = 0; i < input_obj.length; i++) {
		if(br_brand==input_obj[i].value){input_obj[i].checked = false;
		
		
		}
		if (input_obj[i].checked === true) {
			
			if(br_brand!=input_obj[i].value){
			items_brand = items_brand + input_obj[i].value + '-';
		}else { input_obj[i].checked = false;}
	}
	}
	if(items_brand!=""){vurl=vurl + '&item_brand='+items_brand; }
	
	var s_price;
	var input_obj_price = document.getElementsByName('sprice');
	for (i = 0; i < input_obj_price.length; i++) {
		if (input_obj_price[i].checked === true) {
			s_price = s_price + input_obj_price[i].value + '-';
		}
	}
	
	var s_breed1;
	var br_breed=$("#some_breed_name").val();
	var input_obj_breed = document.getElementsByName('namebreed');
	for (i = 0; i < input_obj_breed.length; i++) {
		if (input_obj_breed[i].checked === true) {
			if(br_breed!=input_obj_breed[i].value){
				
				s_breed1 = s_breed1 + input_obj_breed[i].value + '-';
		}else { input_obj_breed[i].checked = false}
		}
	}
	var str2="'"+s_breed1+"'";
	var changestr=str2.replace('undefined',"");
	if(changestr!="''"){
		changestr=changestr.replace("'","");
		changestr=changestr.replace("'","");
		vurl =vurl + '&breedname='+changestr; }
	
	var type_breed;
	var br_breed1=$("#some_breed_name1").val();
	var input_obj_typebreed = document.getElementsByName('typebreed');
	for (i = 0; i < input_obj_typebreed.length; i++) {
		if (input_obj_typebreed[i].checked === true) {
			if(br_breed1!=input_obj_typebreed[i].value){
			type_breed = type_breed + input_obj_typebreed[i].value + '-';
				}else { input_obj_typebreed[i].checked = false}

		}
	}
	var str21="'"+type_breed+"'";
	var changestr1=str21.replace('undefined',"");
	if(changestr1!="''"){
		changestr1=changestr1.replace("'","");
		changestr1=changestr1.replace("'","");
		vurl =vurl + '&type_breed11='+changestr1; }
	
	
	
	var type_life;
	var br_breed2=$("#some_breed_name2").val();
	var input_obj_lifebreed = document.getElementsByName('typestage');
	for (i = 0; i < input_obj_lifebreed.length; i++) {
		if (input_obj_lifebreed[i].checked === true) {
			if(br_breed2!=input_obj_lifebreed[i].value){
			type_life = type_life + input_obj_lifebreed[i].value + '-';
		}else { input_obj_lifebreed[i].checked = false}
		}
	}
	var str22="'"+type_life+"'";
	var changestr3=str22.replace('undefined',"");
	if(changestr3!="''"){
		changestr3=changestr3.replace("'","");changestr3=changestr3.replace("'","");
		vurl =vurl + '&type_life='+changestr3; }
	
	
	if($("#aa").is(':checked')){
		var value=0;
		}else
		{var value=1;}
	var s_sort=$('#sort').val();
	if(s_sort==''){s_sort='other';}
	if(s_sort=='relevance'){s_sort='relevance';
		$("#a2").removeClass("displayContainer_rightTabactive");
		$("#a3").removeClass("displayContainer_rightTabactive");
		$("#a1").addClass("displayContainer_rightTabactive");
		$("#a2").addClass("displayContainer_rightTab");
		$("#a3").addClass("displayContainer_rightTab");
	}//add class $("#ship_name").addClass("required");
	if(s_sort=='price_asc'){
		s_sort='price_asc'; 
		$("#a1").removeClass("displayContainer_rightTabactive");
		$("#a3").removeClass("displayContainer_rightTabactive");
		$("#a2").addClass("displayContainer_rightTabactive");
		$("#a1").addClass("displayContainer_rightTab");
		$("#a3").addClass("displayContainer_rightTab");
		
	  }
	if(s_sort=='price_desc'){
		s_sort='price_desc'; 
		$("#a1").removeClass("displayContainer_rightTabactive");
		$("#a2").removeClass("displayContainer_rightTabactive");
		$("#a3").addClass("displayContainer_rightTabactive");
		$("#a1").addClass("displayContainer_rightTab");
		$("#a2").addClass("displayContainer_rightTab");
		}
	var s_price1=$('#amount').val();
	var minprice=$('#pmin').val();
	var maxprice=$('#pmax').val();
	minprice=minprice.replace('Rs. ',"");
	maxprice=maxprice.replace('Rs. ',"");
	var m1=<?=$s1t; ?>;var m2=<?=$end1amount;?>;
	if(m1!=minprice || m2!=maxprice){vurl =vurl + '&s_price1='+s_price1; }
	
	var s_weight=$('#aweight').val();
	var minwprice=$('#wmin').val();
	var maxwprice=$('#wmax').val();
	minwprice=minwprice.replace('Kg',"");
	maxwprice=maxwprice.replace('Kg',"");
	var mw1=<?=$w1t1; ?>;var mw2=<?=$end1weight;?>;
	if(mw1!=minwprice || mw2!=maxwprice){vurl =vurl + '&s_weight='+s_weight; }
	
	var w_price=$('#aweightp').val();
	var minw1price=$('#wminp').val();
	var maxw1price=$('#wmaxp').val();
	minw1price=minw1price.replace('Rs. ',"");
	maxw1price=maxw1price.replace('Rs. ',"");
	var mw11=<?=$w1tp1; ?>;var mw12=<?=$end1weightp;?>;
	if(mw11!=minw1price || mw12!=maxw1price){vurl =vurl + '&s_weightprice='+w_price; }
		
	//alert(vurl);
	//alert(s_price1);s_weightprice
	var breedname=s_breed1;
	var catid=<?=$category_id?>;
	
	
	
	
	
	var record=  vurl + 
	/*'&item_brand='+items_brand+*/
	/*'&s_price='+s_price+*/
	'&show1='+value+
	/*'&type_breed11='+type_breed+
	'&breedname='+breedname+*/
	/*'&s_price1='+s_price1+*/
	/*'&s_weight='+s_weight+*/
	/*'&type_life='+type_life+*/
	'&s_sort='+s_sort;
	/*'&s_weightprice='+w_price;*/
	//alert(record);
	
	
	
	
	
	_gaq.push(['_setCustomVar', 1, 'Section', 'Shop', 3]);
	_gaq.push(['_setCustomVar', 2, 'Category', '<?=$category_name?>', 3]);
	_gaq.push(['_trackPageview','/<?=$section[0]?>/?filter=filter&category_name=<?=$category_name?>&category_id='+catid+'&record='+record+'']);
	window.location.hash='filter=filter&category_id='+catid+'&record='+record+'';
	ShaAjaxJquary('/new/shop/leftpanal_shop.php?filter=filter&category_name=<?=$category_name?>&category_id='+catid+'&record='+record+'', '#productleftpanal', '', '', 'POST', '', '<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />', 'REP');
	alert('this file for test');
	ShaAjaxJquary('/new/shop/product_listing_search1.php?filter=filter&category_name=<?=$category_name?>&category_id='+catid+'&record='+record+'', '#product', '', '', 'POST', '.productListing542Load', '<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />', 'REP');
	//$("#product").removeAttr('style');
	var sd='';
	$("#some_breed_name2").val(sd);
	$("#some_breed_name1").val(sd);
	$("#some_breed_name").val(sd);
	$("#some_brands").val(sd);
	$("#resetdiv").show();
	
}



</script>
<script>

function validateForm(itemid)
 {
	
 		var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 		if(document.getElementById('notify_email'+itemid).value.length<1)
 		{		
 			document.getElementById('invalidformat1'+itemid).style.display='block';
			document.getElementById('invalidformat'+itemid).style.display='none';
 			return false;
 		}
 		else if(!regex.test(document.getElementById('notify_email'+itemid).value))
 		{
 			alert("Invalid email address format");
 			return false;
 		}
		if(document.getElementById('notify_mobile'+itemid).value.length<1)
 		{
 			document.getElementById('invalidformat'+itemid).style.display='block';
			document.getElementById('invalidformat1'+itemid).style.display='none';
 			return false;
 		}
 		document.getElementById('invalidformat1'+itemid).style.display='none';
		document.getElementById('invalidformat'+itemid).style.display='none';
 		notifymesagar(itemid);
 }


</script>
<script>
IEPNGFix.blankImg = 'blank.gif'; </script>
<script type="text/javascript">


function check(notify_item) {
//alert("sasa");	
document.getElementById('notime'+notify_item).style.display='block';
document.getElementById('notifybutton'+notify_item).style.display='none';

}


function notifymesagar(itemid) {
	
	var emailid=document.getElementById('notify_email'+itemid).value;
	
	var phoneno=document.getElementById('notify_mobile'+itemid).value;
	
ShaAjaxJquary("/new/shop/notifyme.php?item_id="+itemid+"&emaiid="+emailid+"&phnno="+phoneno+"", "#notifyme"+itemid, '', '', 'GET', "#notifyme"+itemid, '<img 	src="/images/indicator.gif" />','REP');
//document.getElementById('validation'+itemid).style.display='none';	
document.getElementById('notime'+itemid).style.display='none';
//document.getElementById('notime1'+itemid).style.display='block';	
	
}

</script>
<style>
div.alert {
    background: none repeat scroll 0 0 #81A00C;
    border-radius: 0 0 3px 3px;
    color: #ECECEC;
    font-size: 12px;
    font-weight: bold;
    left: 50%;
    margin-left: -220px;
    padding: 10px 50px;
    position: fixed;
    text-align: center;
    top: 0;
    width: 233px;
    z-index: 10030;
}
</style>
<!--<style type="text/css">

.myclass .hidden {
	display: none;
	width:105px;
	text-align:center;
	border-radius:7px;
}
.myclass:hover .hidden {
	display: block;
	width: 105px;
	text-align:center;
	z-index:76px;
}

.quick-view {
	background: url("http://www.askforpets.com/skin/frontend/default/afpv2/images/btn_overlay.png") repeat-x 0px 0px rgb(151, 210, 201); padding: 0px 16px; border-radius: 3px; left: 30%; top: 14%; height: 42px; text-align: center; color: rgb(255, 255, 255); text-transform: lowercase; line-height: 42px; font-size: 14px; font-weight: 300; display: none; white-space: nowrap; position: absolute; z-index: 2; -webkit-border-radius: 3px; -moz-border-radius: 3px;
}
.quick-view:hover {
	color: rgb(255, 255, 255); text-decoration: none; background-color: rgb(57, 52, 49);
}
</style>-->
<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>    
  <!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header">
                    <div class="fl">
                        <p align="left"><a href="/">Home</a> > <a href="/shop/">Shop</a> > <?=$category_name?></p>
                     </div>
                 
                     <div class="fr">
                        <p align="right">
                        
                        <a href="javascript:window.print()"><img src="/new/pix/bdcmb_printicon.gif" alt="print" title="print"  /></a>
                        
                        <a href="http://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a>
      <a href="http://twitter.com/#!/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a></p>
                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->
       
       <!-- main container -->
       <div class="cont980"> 
       <!-- three column approach -->
      	<!-- column 1 -->
        	
            	<form id="formFilter" name="formFilter" method="post" action="">
                
                <div class="filterContainer fl comment" id="comment" >
      <header>Refine Your Search</header>
      <div class="filterContainer_wrapper">
                
            		<div style="width:220px"><? if($q){echo ': '.$q;}?>
                      <input name="q" type="hidden" id="q" value="<?=$q?>" />
            		  <input name="aq" type="hidden" id="aq" value="<?=$q?>" />
           		    <span><!--(0 activies)--></span></div>
          <div id="productleft">
          <div class="productListing542Load1" id="productListing542Load1"></div> 
          <div class="filterCategory" id="resetdiv" >
          <div style="overflow:auto;"><a href="/shop/category/dry-dog-food/">Reset all filters</a>
          </div> </div>
          <div class="filterCategory" >
          <c1>by brands</c1>
          <div style="overflow:auto;">
               <? if($totrecord>0){?>
   <label> <input type="checkbox"  name="sbrands" value="24" alt="Royal Canin" title="Royal Canin" onchange="Javascript:searchFilter();"/>Royal Canin</label><br />
   <label><input type="checkbox"  name="sbrands" value="79" alt="Drools" title="Drools" onchange="Javascript:searchFilter();"/>Drools</label><br />
               <label> <input type="checkbox"  name="sbrands" value="103" alt="Taste of Wild" title="Taste of Wild" onchange="Javascript:searchFilter();"/>Taste of Wild</label><br />
      <label><input type="checkbox"  name="sbrands" value="57" alt="Eukanuba" title="Eukanuba" onchange="Javascript:searchFilter();"/>Eukanuba</label><br />
      <label><input type="checkbox"  name="sbrands" value="80" alt="Pedigree" title="Pedigree" onchange="Javascript:searchFilter();"/>Pedigree</label><br />
      <label><input type="checkbox"  name="sbrands" value="101" alt="Farmina" title="Farmina" onchange="Javascript:searchFilter();"/>Cibau</label><br />
                                <? } ?>
                </div></div> 
          <div class="filterCategory" >
          <c1>lifestages of dog</c1>
          <div style="overflow:auto;">
               <? if($totrecord>0){foreach($Alife_stage as $l_stage){ if($l_stage!='null'){?>
                <label>  <input name="typestage" type="checkbox" id="typestage"  value="<?=$l_stage?>" onchange="Javascript:searchFilter();" alt="<?=$Astyle_tips[$l_stage]?>"/><?=$Astyle_breed[$l_stage]?>
                <a style="cursor:pointer" id="<?=$Astyle_tips[$l_stage].$Astyle_filter[$l_stage] ?>" title="<?=$Astyle_tips[$l_stage].$Astyle_filter[$l_stage]?>">(?)</a></label><br />
                                <? }}} ?>
                </div></div>        
          <div class="filterCategory">
          <c1>breeds</c1>
          <div >
           <? if($totrecord>0){foreach($Abreed_id as $breed_ids){ if($breed_ids!='0'){
		   $breedquery=query_execute_row("SELECT breed_name,nicename FROM dog_breeds WHERE breed_id='$breed_ids'");
		   if($breedquery['breed_name']=='Cocker Spaniel (English)'){$breedquery['breed_name']='Cocker Spaniel';}
		   if($breedquery['breed_name']=='German Shepherd Dog (Alsatian)'){$breedquery['breed_name']='German Shepherd';}
		   ?>
           <label><input name="namebreed" type="checkbox" id="namebreed" value="<?=$breed_ids?>" onchange="Javascript:searchFilter();"/><?=$breedquery['breed_name']?></label><br />
                                <? }}} ?>
          </div></div> 
                               
      <div class="filterCategory">
          <c1>breed type </c1>
          <div style="overflow:auto;">
               <? 
				
			    if($totrecord>0){foreach($Abreed_type as $breed_type){ if($breed_type!=''){?>
                <label>  <input name="typebreed" type="checkbox" id="typebreed" value="<?=$breed_type?>" onchange="Javascript:searchFilter();"/><?=$Abreed_type_set[$breed_type]?>
                <a style="cursor:pointer" id="<?=$Abreed_type_set[$breed_type].$Abreed_type_filter[$breed_type] ?>" title="<?=$Abreed_type_set[$breed_type].$Abreed_type_filter[$breed_type]?>">(?)</a></label><br />
                                <? }}} ?>
                </div></div> 
                  
                </div><!--div product-->
                <div class="filterCategory">
          <c1>rate per kgs</c1>
          <div>
         <input type="hidden" id="aweightp" name="aweightp" style="border: 0; color: #f6931f; background:#F0F0F0" readonly="readonly" />
          <div id="weightp-range"></div>
           <input id="wminp" name="wminp" style="border: 0; color: #f6931f; width:150px; background:#F0F0F0;float:left " readonly="readonly"/>
           <input id="wmaxp" name="wmaxp" style="border: 0; color: #f6931f; width:50px; background:#F0F0F0;" readonly="readonly" />
                </div></div>
                
                <div class="filterCategory">
          <c1>WEIGHT</c1>
          <div>
         <input type="hidden" id="aweight" name="aweight" style="border: 0; color: #f6931f; background:#F0F0F0" readonly="readonly" />
           <div id="weight-range"></div>
            <input id="wmin" name="wmin" style="border: 0; color: #f6931f; width:153px; background:#F0F0F0;float:left " readonly="readonly"/>
           <input id="wmax" name="wmax" style="border: 0; color: #f6931f; width:47px; background:#F0F0F0;" readonly="readonly" />
                </div></div>
                <div class="filterCategory">
          <c1>price range </c1>
          <div>
		   <input type="hidden" id="amount" name="amount" style="border: 0; color: #f6931f; width:100px; background:#F0F0F0" readonly="readonly" />
           <div id="slider-range"></div> 
           <input id="pmin" name="pmin" style="border: 0; color: #f6931f; width:147px; background:#F0F0F0;float:left " readonly="readonly"/>
           <input id="pmax" name="pmax" style="border: 0; color: #f6931f; width:53px; background:#F0F0F0;" readonly="readonly" />
           </div></div>
           
					
                </div><!-- column 1 -->
                <!-- column 2 -->
                </div>
                <div class="displayContainer fr" style="margin-left:1px;">
                <? if($pageabout) { ?>
<div class="container">
	  <div class="wrap" style="width:99.5%">
			<div style="width:99.5%">
				
				<h1><?=$cat_name?></h1>
              <div style="text-align:justify"><?=$pageabout ?></div>
		</div>
	  </div>
		<div class="read-more"></div>
	</div>
    <? } ?><div class="cb"></div>
    
  <?php /*?><div id="logo" style="margin-top:10px;">
        <input type="checkbox" id="24" name="sbrands" value="24" onchange="Javascript:searchFilter();"/>
        <label for="24"><img src="/new/pix/rc.png" width="105" height="70"  alt="Royal Canin" title="Royal Canin" ></label>
        <input type="checkbox" id="79" name="sbrands" value="79" onchange="Javascript:searchFilter();"/>
        <label for="79" ><img src="/new/pix/drool.png" width="105" height="70" title="Drools"></label>
        <input type="checkbox" id="103" name="sbrands" value="103" onchange="Javascript:searchFilter();"/>
        <label for="103"><img src="/new/pix/testeofwild.png" width="105" height="70" title="Test of Wild"></label>
      <input type="checkbox" id="57" name="sbrands" value="57" onchange="Javascript:searchFilter();"/>
      <label for="57"><img src="/new/pix/eukanuba.png" width="105" height="70" title="Eukanuba"></label>
      <input type="checkbox" id="80" name="sbrands" value="80" onchange="Javascript:searchFilter();"/>
      <label for="80" ><img src="/new/pix/pedigree.png" width="105" height="70" title="Pedigree"></label>
      <input type="checkbox" id="101" name="sbrands" value="101" onchange="Javascript:searchFilter();"/>
      <label for="101"><img src="/new/pix/farmina.png" width="105" height="70" title="Farmina"></label>
    </div><?php */?>
    <br />
<div class="mB20 displayContainer_header" style="margin-top:auto;">
<div class="displayContainer_left" style="margin-top: 10px;font-size:14px;"><h2>
<div id="productleftpanal">
<? if($totrecord){
	 while($rowItem43 = mysql_fetch_array($qItem)){
	$Uitem_id12[]=$rowItem43["item_id"];
	}
	$unique = array_map("unserialize", array_unique(array_map("serialize", $Uitem_id12)));
	$totrecord=count($unique); }?>
<?=$totrecord?> results found
</div></h2>
          
        </div>
        <div class="displayContainer_right">

<div class="displayContainer_rightLabel">
In Stock :<input type="checkbox" name="aa" id="aa" value="0"  <? if($show1=='0'){echo'checked="checked"';}?>onchange="Javascript:searchFilter();"/>
 </div><div class="displayContainer_rightTab" id="a1" style=" margin-right:3px;">
 <a name="some_name" id="relevance" style="cursor:pointer;" >Popularity</a>
 <input type="hidden" name="sort" id="sort" value="<?=$some_name?>"/>
 <input type="hidden" name="so" id="so" value="<?=$so?>"/>
 <input type="hidden" name="some_breed_name" id="some_breed_name" value="<?=$some_breedname2?>"/>
 <input type="hidden" name="some_breed_name1" id="some_breed_name1" value=""/>
 <input type="hidden" name="some_breed_name2" id="some_breed_name2" value=""/>
 <input type="hidden" name="some_brands" id="some_brands" value=""/>
 <input type="hidden" name="search_brands_life" id="search_brands_life" value="<?=$printbreed_life1;?>"/>
  <input type="hidden" name="search_brands_breed" id="search_brands_breed" value="<?=$printbreed_name1;?>"/>
 <input type="hidden" name="search_brands_btype" id="search_brands_btype" value="<?=$printbreed_type1;?>"/>
 <input type="hidden" name="search_brands_brand" id="search_brands_brand" value="<?=$printbrand_name1;?>"/>

 </div>
 <div class="displayContainer_rightTab" id="a2">
 <a  name="some_name" id="price_asc" style="cursor:pointer;">Price: High to Low</a></div>
 <div class="displayContainer_rightTab" id="a3">
 <a  name="some_name" id="price_desc" style="cursor:pointer;">Price: Low to High</a></div>
</div></div>
 
    
<div class="cb"></div>
      
      
                <div id="product">
                  <?php require_once('product_listing_search1.php'); ?>
                  <div class="cb"></div></div>
                </form><!-- column 2 -->
                <div class="cb"></div>
                </div>
                <?php //require_once($DOCUMENT_ROOT.'/new/common/shop-cart.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>   	
