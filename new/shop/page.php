<?php
//ini_set("display_errors", 1);
//error_reporting(E_ALL);
ini_set("memory_limit",-1);
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-ArrayShopAttributeValue.php');
require_once($DOCUMENT_ROOT.'/banner1.php');

$sitesection = "shop";
$ecomm_pagetype = 'product';

$ant_section = 'Shop';
$ant_page = 'Product';
$google_conversion_label="hWRsCPrz2gIQvoe25QM";

$session_id = session_id();
//echo "SELECT item_id,domain_id, type_id,item_status, name, item_about, title, keyword, description, price, weight, stock_status, num_views, item_brand, item_parent_id,virtual_qty, selling_price, visibility, short_description,payment_mode,item_shipping_amount FROM shop_items_vender WHERE nice_name='$section[0]' AND item_display_status!='delete'";
$qdata=query_execute("SELECT item_id,item_parent_id,domain_id,nice_name, type_id,item_status, name, item_about, title, keyword, description, price, weight, stock_status, num_views, item_brand, item_parent_id,virtual_qty, selling_price, visibility, short_description,payment_mode,item_shipping_amount FROM shop_items WHERE item_id='$item_id' AND item_display_status!='delete'");

$rowTotal = mysql_num_rows($qdata);
if($rowTotal==0){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();
}


$rowdat = mysql_fetch_array($qdata);
$domain_id=$rowdat['domain_id'];
// function to redirect according to Domain....
//updatedomain($domain_id,$section[0]);

// function to redirect according to Domain ENDS HERE....
$name1 = $rowdat["name"];
$name1=stripslashes($name1);
$title1 = $rowdat["title"];
$desc1 = $rowdat["description"];
$item_id = $rowdat["item_id"];
$type_id = $rowdat["type_id"];
$price = $rowdat["price"];
$weight = $rowdat["weight"];
$brand_id = $rowdat["item_brand"];
$stock_status = $rowdat["stock_status"];
$selling_price=$rowdat["selling_price"];
$visibility=$rowdat["visibility"];
$item_status=$rowdat["item_status"];
$virtual_qty=$rowdat["virtual_qty"];
$payment_mode=$rowdat["payment_mode"];
$item_shipping_amount=$rowdat["item_shipping_amount"];

$ecomm_prodid = "$item_id";
	if($type_id=='configurable'){
	$selectchild=mysql_query("SELECT nice_name FROM shop_items WHERE item_parent_id='$item_id' AND stock_status='instock' AND item_display_status='active' AND visibility='visible' LIMIT 1");
	$countchild=mysql_num_rows($selectchild);
	if($countchild > '0'){
while($rowchildname=mysql_fetch_array($selectchild)){
$nice=$rowchildname['nice_name'];
}
	header("HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/".$nice."/");
	}else{
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/shop/");
	}}
$ecomm_totalvalue = number_format($price,2);
$upView=query_execute("UPDATE shop_items SET num_views = num_views+1 WHERE item_id='$item_id'");

$qCat=query_execute("SELECT category_id FROM shop_item_category WHERE item_id='$item_id' AND category_id!=0");
$rowCat = mysql_fetch_array($qCat);
$category_id = $rowCat["category_id"];
// Custom Variable for Google analytics
$CustomVar[3]='Sub-Category|'.$ArrayShopCategorys[$category_id];
// Custom Variable for Google analytics

$qParentCat=query_execute("SELECT category_parent_id, category_nicename, category_name FROM shop_category WHERE category_id='".$category_id."'"); //NKS
$rowParentCat = mysql_fetch_array($qParentCat); //NKS
$category_parent_id = $rowParentCat["category_parent_id"];
$category_nicename = $rowParentCat["category_nicename"];
$category = $rowParentCat["category_name"];
$ant_category = "$category";

// Custom Variable for Google analytics
$CustomVar[2]='Category|'.$ArrayShopCategorys[$category_parent_id];
// Custom Variable for Google analytics

$qParentCatNice=query_execute("SELECT category_nicename FROM shop_category WHERE category_id='".$category_parent_id."'"); //NKS
$rowParentCatNice = mysql_fetch_array($qParentCatNice); //NKS
$catParentNiceName = $rowParentCatNice["category_nicename"];
//echo 'id:'.$category_id.' pid:'.$category_parent_id.''.$item_id; //NKS


$qBrand=query_execute("SELECT brand_name FROM shop_brand WHERE brand_id='$brand_id'");
$rowBrand = mysql_fetch_array($qBrand);
$brandName=$rowBrand["brand_name"];
// Custom Variable for Google analytics
$CustomVar[4]='Brand|'.$brandName;
// Custom Variable for Google analytics
// Get all images
if($rowdat["item_parent_id"] == '0'){
	$sqlmedia="SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC";
}else{
	$sqlmedia="SELECT media_file, label FROM shop_item_media WHERE item_id='".$rowdat["item_parent_id"]."' ORDER BY position ASC";
}

$qdataM=query_execute("$sqlmedia");
while($rowdatM = mysql_fetch_array($qdataM)){
	$ArrayAllImg[]=$rowdatM["media_file"].';'.$rowdatM["label"];
}
$imgFLBase=explode(';',$ArrayAllImg[0]);
// END

$catParentName=$ArrayShopCategorys[$rowParentCat["category_parent_id"]]; //NKS

$catName=$ArrayShopCategorys[$rowCat["category_id"]];
$pageHone=$rowdat["name"];
$pageHone=stripslashes($pageHone);

$pageTitle=$pageHone.', '.$catName.', '.$brandName.', Online Pet Shop, Buy Dog Products, Pet Stores India | DogSpot.in';
$pageKeyword=$pageHone.', '.$catName.', '.$brandName.', Online Pet Shop, Buy Dog Products, Pet Stores, Online Pet Shop India, DogSpot.in';
//$pageDesc=$pageHone.' '.$catName.' '.$art_body.' Online Pet Shop, Buy Dog Products, Pet Stores India DogSpot.in';
$pageDesc=$rowdat["short_description"];
$pageDesc=stripslashes($pageDesc);

/*$imgURL=get_first_image($desc1, $DOCUMENT_ROOT);
$imgURLAbs = make_absolute($imgURL, 'https://www.dogspot.in');*/
$fbprice=(int)$price;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title><?=$pageTitle?></title>
<meta name="keywords" content="<?=$pageKeyword?>" />
<meta name="description" content="<? echo "$pageDesc". " "."$catName"." "." $pageHone"; ?>" />

<?
if($imgFLBase[0]){
		$imglink='/imgthumb/300x280-'.$imgFLBase[0];
		$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$imgFLBase[0];
	}else{
		$imglink='/imgthumb/300x280-no-photo.jpg';
		$src = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
	}
		$dest = $DOCUMENT_ROOT.$imglink;
		createImgThumbIfnot($src,$dest,'300','280','ratiowh');
?>
<?
$sharengainTitle=$pageTitle;
$sharengainDesc="$pageDesc". " "."$catName"." "." $pageHone";
$sharengainImage='https://www.dogspot.in/'.$imglink;
?>

    <meta property="fb:app_id" content="119973928016834" />
    <meta property="og:site_name" content="indogspot"/>
	<meta property="og:title" content="<?=$name1?>" />
    <meta property="og:description" content="<?=$pageDesc." ".$catName?>" />
    <meta property="og:type" content="product" />
    <meta property="og:url" content="https://www.dogspot.in/<?=$section[0]?>/" />
    <meta property="og:site_name" content="DogSpot Store" />
    <meta property="og:price:amount" content="<?=$price?>" />
    <meta property="og:price:currency" content="INR" />
    <meta property="og:availability" content="<?=$stock_status?>" />
    <meta property="og:image" content="<? if($imgFLBase[0]) { echo 'https://www.dogspot.in/shop/item-images/orignal/'.$imgFLBase[0];} else { echo 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif'; } ?>" />

<?php /*?><meta property="og:title" ="<?=$name1?>" />
    <meta property="og:type" ="dogspot:item" />
    <meta property="og:url" ="https://www.dogspot.in/<?=$section[0]?>/" />
    <meta property="og:image" ="<? if($imgFLBase[0]) { echo 'https://www.dogspot.in/shop/item-images/orignal/'.$imgFLBase[0];} else { echo 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif'; } ?>" />
    <meta property="og:description" ="<?=$pageDesc." ".$catName?>" />
    <meta property="dogspot:shop" ="https://www.dogspot.in/shop/" />

        <meta property="dogspot:price_value" ="<?=$fbprice?>" />
    <meta property="dogspot:currency_code" ="INR" />
    <meta property="dogspot:currency_symbol" ="Rs." />
    <meta property="dogspot:price" ="Rs<?=$price?> INR" /><?php */?>

<?php /*?><meta property="og:type" ="gifts_product" />
<meta property="og:url" ="/" />
<meta property="og:title" ="<?=$name1?>" />
<meta property="og:description" ="<?=$pageDesc." ".$catName?>" />
<meta property="og:image" ="<? if($imgFLBase[0]) { echo 'https://www.dogspot.in/shop/item-images/orignal/'.$imgFLBase[0];} else { echo 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif'; } ?>"/>
<meta property="gifts_product:price" ="<?=$fbprice?>" /><?php */?>

<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@indogspot">
<meta name="twitter:creator" content="@creator_username">
<meta name="twitter:title" content="<?=$name1?>">
<meta name="twitter:description" content="<?=$pageDesc?>">
<meta name="twitter:image:src" content="<? if($imgFLBase[0]) { echo 'https://www.dogspot.in/'.$imglink;} else { echo 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif'; } ?>">
<meta name="twitter:data1" content="<?=$fbprice?>">
<meta name="twitter:label1" content="Price">
<meta name="twitter:data2" content="Cash on Delivery">

<meta name="twitter:domain" content="https://www.dogspot.in/">

<?php /*?><meta property="og:title" ="<?=$name1?>" />
<meta property="og:description" ="<?=$pageDesc." ".$catName?>" />
<meta property="og:type" ="product" />
<meta property="og:url" ="https://www.dogspot.in/<?=$section[0]?>" />
<meta property="og:site_name" ="DogSpot" />
<meta property="og:price:amount" ="<?=$fbprice?>" />
<meta property="og:brand" ="<?=$brandName?>" />
<meta property="og:price:currency" ="INR" />
<meta property="og:availability" ="<?=$stock_status?>" /><?php */?>

<?php /*?><script src="/js/shaajax2.1.js" type="text/javascript"></script>
<script type="text/javascript" src="/new/js/jquery-1.js"></script><?php */?>

<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<style type"text/css">
label.error {
	display: block;
	margin-left: 0px;
	width: auto;
}
select.error {
	border: 1px solid red;
}
.clearfix:after{clear:both;:".";display:block;font-size:0;height:0;line-height:0;visibility:hidden;}
.clearfix{display:block;zoom:1}
.jqzoom{text-decoration:none;width:770px; height:370px;}

</style>
<?php /*?><link href="/template/css/checkout.css" rel="stylesheet" type="text/css" /><?php */?>

<?php /*?><link type="text/css" rel="stylesheet" href="/new/css/tab_style.css" media="all" /><?php */?>
<?php /*?><link href="/new/css/login-style.css" rel="stylesheet" type="text/css"><?php */?>
<script type="text/javascript">

$(document).ready(function() {
$('a.login-window').click(function() {

// Getting the variable's value from a link
var loginBox = $(this).attr('href');

//Fade in the Popup and add close button
$(loginBox).fadeIn(300);

//Set the center alignment padding + border
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;

$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});

// Add the mask to body
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);

return false;
});

// When clicking on the button close or the mask layer the popup closed
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
//Default Action
	$(".tab_").hide(); //Hide all 
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_:first").show(); //Show first tab 
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_").hide(); //Hide all tab 
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + 
		$(activeTab).fadeIn(); //Fade in the active 
		return false;
	});	
});
</script>
<script type="text/javascript">
$(document).ready(function() {
//	 $(document).bind("contextmenu",function(e){
//    	return false;
//	});
	
	
	$('.jqzoom').jqzoom({
            zoomType: 'standard',
            lens:true,
            preloadImages: false,
            alwaysOn:false,
			zoomWidth: 660,
			zoomHeight: 370
    });
	
	$("#formShopItem").validate({
		  submitHandler: function (form) {
			
			   var shopqty1=$('#item_qty1').val();
			   if(shopqty1>'1')
			   {
			 var shopqty= $('#item_qty').val(shopqty1);
			   }else
			   {
				var shopqty=$('#item_qty').val();
			   }
			  if(shopqty>='1') {
			  ShaAjaxJquary('/new/shop/cart-rander.php', '#cartDataa', '', 'formShopItem', 'POST', '#cartDataa', '...', 'REP');
			  callLoadCart();
			  $('#divqty').hide();
   		  }
		  else {
			  $('#divqty').show();
		  }
		 }
		  });$('#addtoItemCart').removeAttr('disabled');
});
function updatePrice(optprice, childatt){
//	$('#addtoItemCart').attr('disabled', 'true');
	if(optprice!=""){
		if(childatt==1){
		var attval=optprice.split("-");
		ShaAjaxJquary('/shop/ajax/load-attribute.php?item_id='+attval[3]+'&att_id='+attval[2]+'', '#att_2', '', '', 'GET', '#att_2', 'Loding...', 'REP');
		}
		optprice=parseFloat(optprice);
		//if(optprice!=0){
			var itembprice=$('#itembasceprice').val();
			itembprice=parseFloat(itembprice);
			if(optprice==''){
				var newprice = itembprice;
			}else if(optprice == -1){
				var newprice = itembprice;
			}else{
				var newprice = optprice;
			}
			$('#ItemPriceOnly').html(newprice);
			$('#item_price').val(newprice);
			if(attval[4]){
				$('.sp').html('Rs. '+attval[4]);
			}
		//}
	}	
}
function productcheck(p_item,p_price){
	var totalp=Number($("#pro_price").text());
	if($("#price_"+p_item).is(':checked')){
		nprice=Number(totalp) + Number(p_price);
		$("#pro_price").text(nprice);
		}else{
		nprice=Number(totalp) - Number(p_price);
		$("#pro_price").text(nprice);
		}
	}
function addWItemToCart()
{
var qty=1;	
$('input[name="price_product"]:checked').each(function() {
   fb1=this.value;
   fb=fb1.split(/@/);
   itemid=fb[0];
   price=fb[1];
  // alert(itemid+'---'+price);
   ShaAjaxJquary("/new/shop/cart-rander.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "", '', '', 'POST', '#ttt', '<img src="/images/loading-round.gif" />','REP');
});

setTimeout("callLoadCart()",500);

//ShaAjaxJquary("/new/shop/cart.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "#ttt", '', '', 'POST', '#ttt', '<img src="/images/loading-round.gif" />','REP');
//ShaAjaxJquary("/new/shop/cart.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "", '', '', 'POST', 'loading', '<img src="/images/loading-round.gif" />','REP');
//$("#topItemCount").load("/new/shop/cart.php #cartItemCount");
//$("#topCartTotal").load("/new/shop/cart.php #cartTotal");
	
}
function updatevalue1(val23)
{
	//alert(val23);
document.getElementById('updatevalue').value=val23;	
}

function callLoadCart(){
	centerPopup();
	loadPopup();
	setTimeout("loadPopupBox()",1000);	
}
function loadPopupBox(){
	var destDiv = ['#popupContact', '#topCartTotal', '#topItemCount', '#cartTotal', '#popupItemCount','#cartshippingTotal1'];
	var sourceDiv = ['.popUp710', '#cartTotal1', '#cartItemCount', '#cartTotal1', '#cartItemCount','#cartshippingTotal2'];
	shaajaxLoadMultiple('/new/shop/cart-rander.php', 'POST', sourceDiv, destDiv);
}


function validateForm()
 {
	
 		var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 		if(document.getElementById('notify_email').value.length<1)
 		{		
 			document.getElementById('invalidformat1').style.display='block';
			document.getElementById('invalidformat').style.display='none';
 			return false;
 		}
 		else if(!regex.test(document.getElementById('notify_email').value))
 		{
 			alert("Invalid email address format");
 			return false;
 		}
		if(document.getElementById('notify_mobile').value.length<1)
 		{
 			document.getElementById('invalidformat').style.display='block';
			document.getElementById('invalidformat1').style.display='none';
 			return false;
 		}
 		document.getElementById('invalidformat1').style.display='none';
		document.getElementById('invalidformat').style.display='none';
 		notifymesagar();
 }

function notifymesagar() {
	
	var emailid=document.getElementById('notify_email').value;
	
	var phoneno=document.getElementById('notify_mobile').value;
	var itemid=document.getElementById('notify_item').value;
	ShaAjaxJquary("/new/shop/notifyme.php?item_id="+itemid+"&emaiid="+emailid+"&phnno="+phoneno+"", "#notifyme1", '', '', 'GET', "#notifyme1", '<img 	src="/images/indicator.gif" />','REP');

document.getElementById('notime').style.display='none';	
	
}
// function to add item in wishlist table.................
function check(wishlist_item) {
	
	ShaAjaxJquary("/new/shop/wishlist/wishlist_new.php?item_id="+wishlist_item+"", "#wishlist12", '', '', 'GET', "#wishlist12", '<img 	src="/images/indicator.gif" />','REP');
document.getElementById('frstwish').style.display='block';	
document.getElementById('mainwish').style.display='none';	

}




//  for quick add to cart Sagar starts here......................

 <?php /*?>function addWItemToCart(itmeid)
  {
 alert(itmeid);

 ShaAjaxJquary("/new/shop/quickadd.php?qna_id="+itmeid+"", "#cartDataa", '', 'formquickadd', 'POST', '', '<img src="/images/indicator.gif" />','REP');
 //ShaAjaxJquary('/new/shop/quickadd.php', '#cartDataa', '', 'formquickadd', 'POST', '#cartDataa', '...', 'REP');
	
 	callLoadCart1(itmeid);
	}
	
function callLoadCart1(){
	centerPopup();
	loadPopup();
	setTimeout("loadPopupBox1()",1000);	
}
function loadPopupBox1(){
	var destDiv = ['#popupContact', '#topCartTotal', '#topItemCount', '#cartTotal', '#popupItemCount'];
	var sourceDiv = ['.popUp710', '#cartTotal', '#topItemCount', '#cartTotal', '#cartItemCount'];
	shaajaxLoadMultiple('/new/shop/quickadd.php', 'POST', sourceDiv, destDiv);
}<?php */?>
// Ends here for quick add to cart......................


</script>

<script>
function changecartbox(id)
{
	
	document.getElementById('showqty_'+id).style.display='none';
	document.getElementById('update_'+id).style.display='inline';
	document.getElementById('save'+id).style.display='inline';
	document.getElementById('change'+id).style.display='none';
}
function saveupdate(cart_id,cart_item_id){
	var cart_item=$('#update_'+cart_id).val();
	 $('#show_'+cart_id).val(cart_item);
	var cart_qwe=$('#qqty_'+cart_id).val();
	if(cart_qwe==1){
		var cart_item_qty=$('#show_'+cart_id).val();
	
	}else	{
		var cart_item1=$('#q'+cart_id).val();
		
		var cart_item_qty2=$('#show_'+cart_id).val();
		
		if(cart_item_qty2 > cart_item1)
		{
		
		var cart_item_qty=cart_item1;
		//document.getElementById('divcartqtyerror'+cart_id).style.display='inline';
		var t=1;
		 $('#divcartqtyerror'+cart_id).css("display","inline");
		//document.getElementById('divcartqtyerror'+cart_id).style.display='inline';
			
		}else
		{
			var cart_item_qty=$('#show_'+cart_id).val();
		}
	}
	
   
	
	var check_qty1=cart_item_qty.charAt(0);
    var check_qty2=cart_item_qty.charAt(1);
	var check_qty3=cart_item_qty.charAt(2);
	 if(check_qty1==0)
    {
	if(check_qty2==0 && check_qty3!='0' )
  {
	  
	   var cart_item_qty=check_qty3;
  }else if( check_qty2!=0)
  {
	  var cart_item_qty=check_qty2.concat(check_qty3);
 }
	  
  }
	
	if(cart_item_qty<='1') {
		cart_item_qty=1;
	}
	if(isNaN(cart_item_qty))
	{
		
		cart_item_qty=1;
	}
	if(cart_item_qty>='1') {
	
	updateCart='updateCart';
	ShaAjaxJquary('/new/shop/cart-randers.php?cat_id1='+cart_id+'&cart_item_qty1='+cart_item_qty+'&cart_item_id1='+cart_item_id+'&updateCart='+updateCart+'', '#cartUpdate23', '', '', 'POST', '#cartUpdateitem_'+cart_id, '<img 	src="/images/indicator.gif" />', 'REP');
	if(t==1)
	{
	$('#divcartqtyerror'+cart_id).css("display","inline");
	setTimeout('update_final()',4000);
	}else
	{
	setTimeout('update_final()',500);
	}
	
		}
		else
		{
			
		$('#divcartqty'+cart_id).show();	
		}
	
}
function update_final()
{
	
var destDiv = ['#boxCart', '#topCartTotal1', '#topItemCount', '#cartTotal','#cartDiscount', '#cartItemCount','#cartshippingTotal1'];
	var sourceDiv = ['#boxCart', '#cartTotal1', '#cartItemCount', '#cartTotal1','#cartDiscount', '#cartItemCount','#cartshippingTotal2'];
	shaajaxLoadMultiple('/new/shop/cart-rander.php', 'POST', sourceDiv, destDiv);	
}

function changevalue(value,cat_id)
{
	$('#show_'+cat_id).val(value);
	ShaAjaxJquary("/new/shop/qtycheck.php?cat_id="+cat_id+"&value="+value+"", "#catchange", '', '', 'GET', "#catchange", '','REP');
	
}
function UPDATEItemToCart()
	{
 var qty=$('#item_qty1').val();
 var show=$('#show').val();
 var itemid=$('#item_id').val();
 var pet_name=$('#pet_name').val();
 var pet_phone=$('#pet_phone').val();
  var cart_value=$('#cart_value').val();
// alert(cart_value);
// alert(itemid); alert(pet_phone);
// alert(qty);
// var color=$('#color').val();
  ShaAjaxJquary("/new/shop/cart-rander.php?item_id="+itemid+"&item_qty="+qty+"&pet_name="+pet_name+"&pet_phone="+pet_phone+"&update=update&cart_value="+cart_value+"&show="+show+"", "", '', '', 'POST', '#ttt', '<img src="/images/loading-round.gif" />','REP');
  setTimeout("callLoadCart()",500);
	}
</script>

<script type="text/JavaScript">
function valid(f) {
	
	f.value = f.value.replace(/[a-zA-Z\/!@#$%^&*()-+.:;,'"\><\s]/ig,'');
} 
function valid1(f) {
	
	f.value = f.value.replace(/[0-9\/!@#$%^&*()-+.:;,'"\><]/ig,'');
} 
</script>
<div id="fb-root"></div>
<?php /*?><script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> <?php */?> 
    
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<!-- end of green strip -->
    
     
               <!-- left navigation -->
              
        	<!-- left navigation -->
                <!-- left container -->
                
        	<!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980">
                
                    <div class="fl">
                        <p align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666;"><a href="/">Home</a> » <a href="/shop/">Shop</a> » <a href="/<?=$catParentNiceName?>/"><?=$ArrayShopCategorys[$category_parent_id]?></a> » <a href="/<?=$category_nicename;?>/"><?=$ArrayShopCategorys[$category_id]?></a></p>
                     </div>

               <div class="fr" style="width:180px">
                        <p align="left" style="width:70px; float:left">                                           
 <a href="https://twitter.com/indogspot" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @indogspot</a>
<?php /*?><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script><?php */?>
<script type="text/javascript">var switchTo5x=false;</script>
<?php /*?><script>
$(document).ready( function( ) {
$.getScript('http://w.sharethis.com/button/buttons.js');
$.getScript('http://platform.twitter.com/widgets.js');
});
</script><?php */?>

                    
   <div class="fb-follow" data-href="https://www.facebook.com/indogspot" data-layout="button_count" data-show-faces="false" data-width="150" style="float:left"></div>                 
                       </p>
                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->
      <!-- main  --->
      <div class="cont980" itemscope itemtype="http://schema.org/Product">
           
                    
                    	<div class="shopimgholdSlide">
                        
                                              
 <p>                        
<? 
if($ArrayAllImg){

	$imgFL=explode(';',$ArrayAllImg);
	if($imgFL[0]){
		$imglink='/imgthumb/64x64-'.$imgFL[0];
		$imglinkm='/imgthumb/300x280-'.$imgFL[0];
		
		$src=$DOCUMENT_ROOT.'/shop/item-images/orignal/'.$imgFL[0];
	}else{
		$imglink='/imgthumb/64x64-no-photo.jpg';
		$imglinkm='/imgthumb/300x280-no-photo.jpg';
		
		$src=$DOCUMENT_ROOT.'/imgthumb/no-photo.jpg';
	}
		$dest = $DOCUMENT_ROOT.$imglink;
		$destm = $DOCUMENT_ROOT.$imglinkm;
		createImgThumbIfnot($src,$dest,'64','64','ratiowh');
		createImgThumbIfnot($src,$destm,'300','280','ratiowh');
?>
<a href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: '<?=$imglinkm?>',largeimage: '/shop/item-images/orignal/<?=$imgFL[0]?>'}">
<img src="<?=$imglink?>" alt="<?=$pageHone?>" title="<?=$pageHone?>" class="mr5" border="0" width="64" height="59" />
<img src="/shop/item-images/orignal/<?=$imgFL[0]?>" width="1" height="1" alt="<?=$pageHone?>" title="<?=$pageHone?>" border="0" />
</a>                    
<? }?>
                        </p>
                        
                        
                    
                    </div>
                    <div class="fl">
                    <!-- text hold -->
                    <div class="shoptextholdSlide">
                    	<h1 itemprop="name"><?=stripslashes($pageHone);?></h1>
                        <div class="vs5"></div>
                       <meta  itemprop="url" content="https://www.dogspot.in/<?=$section[0]?>/" />
                        <p itemprop="description"><?=snippetwop(stripslashes($pageDesc),$length=140,$tail="...")?></p>
                        <div class="vs5"></div>
                        <!-- left cont -->
                        <? // echo "SELECT * FROM shop_items_vender WHERE master_item_id='$item_id'"; 
						$query1=query_execute_row("SELECT * FROM shop_items_vender WHERE master_item_id='$item_id' AND stock_status='instock' order by price ASC ");?>
                        	         
                          <div class="vs20"></div>
                          
  <!--Marketplace Section-->
  
 <div class="ds-marketplace_mp bordernone ds-mp7">
<div class="ds-seller-section ds-paddingtpbt10">
<div class="ds-mp-l ds-font-bold">More sellers selling this product on DogSpot [?]
</div>
<div class="ds-mp-r ds-font-bold"><a href="/new/shop/item_detail_vendor.php?item_id=<?=$item_id?>"> View all Sellers (3) »</a>
</div>
<div class="ds-marketplace_mp ds-margintp10">

<table><?
//echo "SELECT * FROM shop_items_vender WHERE master_item_id='$item_id'";
  $query23=query_execute("SELECT * FROM shop_items_vender WHERE master_item_id='$item_id' AND stock_status='instock' ORDER by price ASC LIMIT 2");
	while($item_vendor1=mysql_fetch_array($query23))
	{
		$item_vendor134=$item_vendor1['vendor_id'];
		$market_price1=$item_vendor1['price'];
		$stock_status1=$item_vendor1['stock_status'];
		?>
        
   
<tr class="r0">
<td class="ds-width20 borderbtm">
<span class="ds-sellers-price ds-font-bold">Rs. <?=$market_price1?>
</span><br />
<div class="ds-font-small" id="margintp5">Free home delivery </div>
</td>

<td class="ds-width20 borderbtm"> 
<span class="ds-font-bold ds-stock-status"><?=$stock_status1?></span><br />
<div class="ds-font-small" id="margintp5">Standard delivery in 4 to 5 business days.</div>
</td>
<td class="ds-width20 borderbtm">
<div class="ds-font-small" id="marginbt5">Seller: tquest</div>

<div class="ds-font-small" id="marginbt5 "  >71% positive feedback [?]
</div>
<div class="ds-font-small" id="marginbt5">(120 ratings)</div>

<div class="ds-font-small" id="marginbt5">10 Day Replacement Guarantee [?]</div>
</td>
<td class="ds-width20 borderbtm">
<span  id="ds-buy-nowbtn" >
 <input name="addtoItemCart" type="image" id="addtoItemCart" value="<?=$item_vendor134?>" onmouseover="updatevalue1('<?=$item_vendor134?>');" style="border:0px; margin-top:10px;" src="/new/pix/buynow_btn.gif" alt="Submit"/>
</span>
</td>
</tr>


		<?
	}?></table>
</div></div>
  <!--Marketplace Section-->
                          
  <!--suggest product-->
   <?
	             if($type_id=='simple'){ 
				 $item_id23=$item_id;	  
				 } else {$qItemq23=query_execute_row("SELECT item_id FROM shop_items  WHERE item_parent_id='$item_id23' ORDER BY item_id  LIMIT 1");
		$item_id23=$qItemq23['item_id'];	 
		}
		
		$qItem233=query_execute_row("SELECT count(*) as boughtcount FROM shop_item_bought WHERE item_id='$item_id23'  ");			  		
		if($qItem233['boughtcount']>=4){  //echo "SELECT count(*) as boughtcount FROM shop_item_bought WHERE item_id='$item_id23'  "; ?>                        
 <div class="cont980 combo_box">
<!--FREQUENTLY BOUGHT TOGETHER-->
<?php require_once($DOCUMENT_ROOT.'/new/shop/suggest_product.php'); ?>
<!--FREQUENTLY BOUGHT TOGETHER-->
 
</div>  <?  }?>  
  <!--close suggest product-->           
<div class="productSummary769 pageBody">
             
 <div class="vs10 cb"> </div>
              <?
			  $item_id=$item_id23;
             	if($rowdat["item_parent_id"]!='0'){
				  $rowPabout=query_execute_row("SELECT item_about FROM shop_items WHERE item_id='$item_id'");
				  if($rowPabout['item_about']){
					  echo $rowPabout["item_about"];
				  }
				 else
				 {
				    $rowAabout=query_execute_row("SELECT item_about FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
					echo $rowAabout['item_about'];	
				 }

			  }else{
				  echo $rowdat["item_about"];
			  }
			    //display attribute........
			$selatt_id=mysql_query("SELECT * FROM shop_item_attribute WHERE item_id='$item_id' AND attribute_set_id !='0'");
			$check=mysql_num_rows($selatt_id);
			if($check > '0'){
			while($selectatt=mysql_fetch_array($selatt_id)){
			$att_id=$selectatt['attribute_set_id'];
			$att_value=$selectatt['attribute_value_id'];
			$selatt_name=query_execute_row("SELECT attribute_set_name FROM shop_attribute_set WHERE attribute_set_id='$att_id'");
			$selval_name=query_execute_row("SELECT attribute_value_name FROM shop_attribute_value WHERE attribute_value_id='$att_value'");
			echo $selatt_name['attribute_set_name']." : ".$selval_name['attribute_value_name']."<br>";
			}
			}
			  ?>
              
            
           
              
<!--Comment system-->
<div style="margin-top:20px; background-color:#fafafa; padding:10px;">        
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'dogspot'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>
</div>
<!--Comment system--> 


 </div>
 <!-- related products -->
 
<div class="cont183" style="margin-right:0px; float: right;  width: 153px;margin-top: 15px;">
                	
        <div align="center">
        <?php addbanner160('600','160','1'); ?></div> <div id="Divclear"></div>
        
</div> 
</div>              
                       <!-- related products -->
     </form>                      
<!-- popup Box Start--> 
<div id="popupContact" style="min-height:330px;" class="popUp710">
<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />
</div>  
<div id="backgroundPopup" style="z-index:9999"></div>  

<!-- popup Box END-->
<div id="login-box" class="login-popup">
<?php require_once($DOCUMENT_ROOT.'/new/shop/shipping-detail.php'); ?></div>  
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>