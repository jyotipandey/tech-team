<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

$sitesection = "shop";
$session_id = session_id();

$order_user_desc=query_execute_row("SELECT address_name,address_city,address_address1,address_address2,address_state,address_zip,address_phone1,userid FROM shop_order_address WHERE order_id='$order_id'");


//Track Order Code Starts
$qGetMyCart34=query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id'");
$ostatus=$qGetMyCart34['delevery_status'];
if($qGetMyCart34['order_type']=='complete'){
$qGetMyCart=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '$order_id' ");

$sqlodate=query_execute_row("SELECT order_c_date, order_tracking, shop_courier_id FROM shop_order WHERE order_id='$order_id'");
$sqlorderdate=$sqlodate['order_c_date'];
$date = showdate($sqlorderdate, "d M o");
$waybill=$sqlodate['order_tracking'];
$shop_courier_id=$sqlodate['shop_courier_id'];
$time = date('h:i A', strtotime($sqlorderdate));
$sqlcomplete=$date. ' | ' .$time. ' | ' ."Payment Approved";
$sqld=query_execute_row("SELECT c_date FROM section_reviews WHERE review_body LIKE '%pending-dispatch%' AND review_section_id='$order_id'");
$sqlsdat=$sqld['c_date'];
$datetimearray1 = explode(" ", $sqlsdat);
$date1 = $datetimearray1[0];
$time1 = $datetimearray1[1];
$reformatted_date1 = date('d-m-Y',strtotime($date1));
$reformatted_time1 = date('Gi.s',strtotime($time1));
if($sqlsdat==''){
$sqlstate1="Order yet to confirm";
}else{
$sqlstate1="Your order has been cofigured";
$sqlcomplete1=$date1. ' | ' .$time1. ' | ' ."Your order has been cofigured";
}
$sqld1Canceled=query_execute_row("SELECT c_date FROM section_reviews WHERE (review_body LIKE '%canceled%' OR review_body LIKE '%cancelled%') AND review_section_id='$order_id'");
$sqlsdatCanceled=$sqld1Canceled['c_date'];
$datetimearray11 = explode(" ", $sqlsdatCanceled);
$canceldate1 = $datetimearray11[0];
$canceltime1 = $datetimearray11[1];
$Canceledreformatted_date1 = date('d-m-Y',strtotime($canceldate1));

$sqld1=query_execute_row("SELECT c_date FROM section_reviews WHERE review_name='dispatched-ready' AND review_section_id='$order_id'");
$sqlsdat1=$sqld1['c_date'];
$datetimearray2 = explode(" ", $sqlsdat1);
$date2 = $datetimearray2[0];
$time2 = $datetimearray2[1];
$reformatted_date2 = date('d-m-Y',strtotime($date2));
$reformatted_time2 = date('Gi.s',strtotime($time2));
if($sqlsdat1==''){
$sqlstate2="Order yet to be packed";
}else{
$sqlstate2="Your order has been packed";
$sqlcomplete2=$date2. ' | ' .$time2. ' | ' .$sqlstate2;
}
$sqld2=query_execute_row("SELECT c_date FROM section_reviews WHERE review_name='dispatched' AND review_section_id='$order_id'");
$sqlsdat3=$sqld2['c_date'];
$datetimearray3 = explode(" ", $sqlsdat3);
$date3 = $datetimearray3[0];
$time3 = $datetimearray3[1];
$reformatted_date3 = date('d-m-Y',strtotime($date3));
$reformatted_time3 = date('Gi.s',strtotime($time3));
if($sqlsdat3==''){
$sqlstate3="Order yet to dispatch from warehouse";
}else{
if($shop_courier_id=='6'){
$cou_name="Delhivery";}
elseif($shop_courier_id=='30'){
$cou_name="SCM";}elseif($shop_courier_id=='31'){
$cou_name="Wizard";}elseif($shop_courier_id=='34'){
$cou_name="Delhivery Surface";}elseif($shop_courier_id=='33'){
$cou_name="Pickrr";}elseif($shop_courier_id=='3'){
$cou_name="Self";}else{ $cou_name="";}
if($cou_name){
$mystatuscour="Your Order is dispatched through <b>$cou_name</b> Order tracking Number is <b>".$waybill.'</b>';

}else
{
$mystatuscour='';	
}
$sqlstate3="Your order has been dispatched from warehouse";
$sqlcomplete3=$date3. ' | ' .$time3. ' | ' .$mystatuscour;
}
$sqld4=query_execute_row("SELECT c_date FROM section_reviews WHERE review_body LIKE '%Delivered%' AND review_section_id='$order_id'");
$sqlsdat4=$sqld4['c_date'];
$date7 = showdate($sqlsdat4, "d M o");
$time7 = date('h:i A', strtotime($sqlsdat4));
$sqlcomplete7=$date7. ' | ' .$time7. ' | ' .'Delivered';
if($sqlsdat4=='')
{
$sqld4=query_execute_row("SELECT c_date FROM section_reviews WHERE review_body LIKE '%delivered%' AND review_section_id='$order_id'");
$sqlsdat4=$sqld4['c_date'];	
}
if($shop_courier_id=='6' || $shop_courier_id=='29'){
$xml = simplexml_load_file( 'https://track.delhivery.com/api/packages/xml/?token=3bd37634f1d9e1aadfe32e05562fefa70b8df31a&waybill='.$waybill );
$addate=$xml->Shipment->Status->StatusDateTime;
$aDelDate=explode("T",$xml->Shipment->Status->StatusDateTime);
$c_date = $aDelDate[0].' 00:00:00';
$time4=date('h:i A', strtotime($addate));
$date4 = showdate($c_date, "d M o");
if($date4=='' || $date4=='30 Nov 1999'){
$sqlstate4="Shipment yet to be delivered";
}else{
$sqlstate4=$xml->Shipment->Status->Instructions;
$sqlcomplete5=$date4. ' | ' .$time4. ' | ' .$sqlstate4;
}
}
if($shop_courier_id=='34'){
$xml = simplexml_load_file( 'https://track.delhivery.com/api/packages/xml/?token=489f42a229e4bc4339a553df97fc13352e49a3bf&waybill='.$waybill );
$addate=$xml->Shipment->Status->StatusDateTime;
$aDelDate=explode("T",$xml->Shipment->Status->StatusDateTime);
$c_date = $aDelDate[0].' 00:00:00';
$time4=date('h:i A', strtotime($addate));
$date4 = showdate($c_date, "d M o");
if($date4=='' || $date4=='30 Nov 1999'){
$sqlstate4="Shipment yet to be delivered";
}else{
$sqlstate4=$xml->Shipment->Status->Instructions;
$sqlcomplete5=$date4. ' | ' .$time4. ' | ' .$sqlstate4;
}
}
if($shop_courier_id=='30'){
$xml = simplexml_load_file( 'http://www.rachnaerp.com/Rachna/webservice/v1/MultipleDocketTracking?secureKey=F3ABF21929E441BA97E27D074D834E1F&docketNumber='.$waybill );
$addate=$xml->Shipment->Status->StatusDateTime;
$aDelDate=explode("T",$xml->Shipment->Status->StatusDateTime);
$c_date = $aDelDate[0].' 00:00:00';
$time4=date('h:i A', strtotime($addate));
$date4 = showdate($c_date, "d M o");
if($date4=='' || $date4=='30 Nov 1999'){
$sqlstate4="Shipment yet to be delivered";
}else{
$sqlstate4=$xml->Shipment->Status->Instructions;
$sqlcomplete5=$date4. ' | ' .$time4. ' | ' .$sqlstate4;
}
}
}
if($ostatus=='rto'){
	$sqlstate5="Return to origin";
	}
	elseif($ostatus=='delivered'){
		$sqlstate5="Shipment delivered";
		if($sqlsdat4!='' &&  $date4=='' || $date4=='30 Nov 1999'){
	$date4=$sqlsdat4;
	$time4=date('h:i A', strtotime($date4));
	$sqlcomplete6=$date4. ' | ' .$time4. ' | ' .$sqlstate5;
	}
		}else{
			$sqlstate5="Shipment yet to delivered";
			}
$title="Order Details | Shop DogSpot";
$keyword="My Order Detail | DogSpot";
$desc="My Order Detail | DogSpot";
 
    $alternate="https://www.dogspot.in/new/shop/myorder.php";
	$canonical="https://www.dogspot.in/new/shop/myorder.php";
	$og_url=$canonical;
	$imgURLAbs="";
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/myorders.css?c=1" />
<script type="text/javascript" src="/new/shop/testjs.js"></script>
<? if($userid=='Guest'){?>
	<script src="/new/js/leanModal-modified-for-onload.js" type="text/javascript"></script>
  <script> $(function() {
            $('.bharat').leanModal({ top : 80, closeButton: ".modal_close" });
            $("#modal_trigger1").click();        
        });
</script>
<?  }?>

<div class="breadcrumbs">
  <div class="container">
    <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
      <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a href="/" itemprop="item"><span itemprop="name">Home</span></a>
        <meta itemprop="position" content="1" />
        </span> <span> / </span> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <span itemprop="item"> <span itemprop="name" class="active-bread">My Orders</span></span>
        <meta itemprop="position" content="2" />
        </span> </div>
    </div>
  </div>
</div>
<section class="myorder-sec">
<div class="container">
<div class="order-detail-wrp">
<div class="row">

<div class="col-sm-12"> 
 <h1 class="clearfix">Order Details</h1>
     </div>

 <div class="col-md-6 col-sm-6">
        <div class="order-unit order_info">
          <ul>
           <li> <label class="order_id_text">Order ID:</label> <span class="order_id"><?=$order_id?></span> <span class="order_item">(<?=$totrecord;?> Item)</span></li>
            <li> <label class="order_id_text">Order Date:</label> <span class="order_item"><?=showdate($qGetMyCart34["order_c_date"], "d M y h:m a");?></span></li>
            <li><label class="order_id_text">Amount Paid:</label> <span class="order_id"><i class="fa fa-inr"></i> <?=number_format($qGetMyCart34['order_amount'],0);?></span> <span> through <?=$AOrderMethod[$qGetMyCart34['order_method']]?></span></li>
          </ul>
        </div>
        
      </div> 
      <div class="col-md-6 col-sm-6">
      <div class="order-unit order_address">
        <ul>
        <li>
         <span class="order_id"><?=$order_user_desc['address_name'];?></span>
       <span><?=$order_user_desc['address_phone1'];?></span>
        </li>
        <li>
       <?=$order_user_desc['address_address1'];?> <?=$order_user_desc['address_address2'];?>
        </li>
        <li><?=$order_user_desc['address_city'];?>, <?=$order_user_desc['address_state'];?> - <?=$order_user_desc['address_zip'];?></li>
        </ul>
        </div>
      </div>
      </div>
     
         
 

</div>

<div class="order-details-manage" id="manage_div">
    <div class="header">Manage Order</div>
  <div class="row">
   <div class="col-md-6 col-sm-6 text-center border-style">
    <a onclick="printDiv()"><div class="order-details-sprits print-icon"></div>
    <div class="manage_order_text">Print Order</div></a>
        </div>
              <div class="col-md-6 col-sm-6 text-center">
                <a href="/contactus.php">
                <div class="order-details-sprits contact-icon"></div>
                <div class="manage_order_text">Contact Us</div> </a>
                </div>
  </div>
    </div>

    <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="trackorder">
                            <div class="order">
                                <div  class="trackorder_header">Track Order</div>
                                    <div class="shipment">
                                        <div class="ordertrack-centered">
                                        <div class="ordertrack-entry">
                                                <div class="ordertrack-entry-inner">

                                                    <div class="ordertrack-icon <? if($sqlsdat){?>bg-success<? }?>" >
                                                        <i class="entypo-feather"></i>
                                                    </div>

                                                    <div class="ordertrack-label">
                                                        <h2>APPROVAL</h2>
                                                        <p><?=$sqlsdat?></p>
                                                    </div>
                                                </div>
                                            </div><? if($canceldate1){?>
                                            <div class="ordertrack-entry">
                                                <div class="ordertrack-entry-inner">

                                                    <div class="ordertrack-icon <? if($canceldate1){?>red-bg-success<? }?>">
                                                        <i class="entypo-feather"></i>
                                                    </div>

                                                    <div class="ordertrack-label">
                                                        <h2>CANCELED</h2>
                                                        <p><?=$canceldate1?></p>
                                                    </div>
                                                </div>
                                            </div>
                                                   <? }else{?>
                                            <div class="ordertrack-entry">
                                                <div class="ordertrack-entry-inner">

                                                    <div class="ordertrack-icon <? if($sqlsdat1==''){ ?>inactive<? }elseif($sqlsdat1){?>bg-success<? }?>">
                                                        <i class="entypo-feather"></i>
                                                    </div>

                                                    <div class="ordertrack-label">
                                                        <h2>PROCESSING</h2>
                                                        <p><?=$sqlsdat1?></p>
                                                    </div>
                                                </div>
                                            </div>
											
											<div class="ordertrack-entry">
                                                <div class="ordertrack-entry-inner">

                                                    <div class="ordertrack-icon <? if($sqlsdat3==''){ ?>inactive<? }else{?>bg-success<? }?>">
                                                        <i class="entypo-feather"></i>
                                                    </div>

                                                    <div class="ordertrack-label">
                                                        <h2>DISPATCHED</h2>
                                                        <p><?=$sqlsdat3?></p>
                                                    </div>
                                                </div>
                                            </div>
                                                                                                                                                                      <div class="timeline-entry">
                                            	
											<div class="ordertrack-entry">
                                                <div class="ordertrack-entry-inner">

                                                    <div class="ordertrack-icon<? if($ostatus=='delivered' || $ostatus=='rto'){?> bg-success<? }elseif($date4=='' || $date4=='30 Nov 1999'){ ?>inactive<? }else{?>processed-continous  <? }?>">
                                                        <i class="entypo-feather"></i>
                                                    </div>

                                                    <div class="ordertrack-label">
                                                        <h2><? if($ostatus=='rto'){?> RTO<? }else{?>DELIVERED<? }?></h2>
                                                        <p><?=$date4?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <? }?>

	</div>
                                                                                    </div>
                                    </div>
                               
                            </div>
                        </div>
                    </div>
                </div>            
</div>
	<div class="container">										
<div class="order-details-list" >
    <div class="header">
    <div class="row">
    
     <div class="col-xs-7 col-sm-8 col-md-7 border-style-p">
    <label> PRODUCT DETAILS</label>
     </div>
       <div class="col-xs-2 col-sm-2 col-md-2 border-style-p">
    <label> QTY</label>
     </div>
     <div class="col-xs-3 col-sm-3 col-md-3">
     <label> PRICE</label>
     </div>
     
     </div>
    </div>
    <? 
$totalsub='';
while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		$item_id = $rowMyCart["item_id"];
		$cart_id=$rowMyCart["cart_id"];
		$cart_order_id=$rowMyCart["cart_order_id"];
		$orderDetail=query_execute_row("SELECT item_shipping_amount,order_amount FROM shop_order WHERE order_id='$cart_order_id'");
		$qdata=query_execute("SELECT name, price, weight, item_parent_id,item_status,stock_status,item_display_status,nice_name FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		$ItemGtotal=$rowMyCart["item_totalprice"]+$ItemGtotal;
		$item_parent_id=$rowdat["item_parent_id"];
		$nice_name=$rowdat["nice_name"];
		$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];
		$totalsub=($rowMyCart["mrp_price"]*$rowMyCart["item_qty"])+$totalsub; 
		$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 $count=mysql_num_rows($qdataM1);
	 if($count>0)
	 {
	  $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 }else{
	if($item_parent_id == '0'){
		 $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	}else{		
		$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
	}
	 }
		$rowdatM = mysql_fetch_array($qdataM);
		
		$imagekit="https://ik.imagekit.io/2345/tr:h-100,w-100/shop/item-images/orignal/".$rowdatM["media_file"];
			
	?>
  <div class="order-details-list-sec">
  <div class="row">
    <div class="col-xs-7 col-sm-8 col-md-7">
    <div class="row">
    <div class="col-xs-4 col-sm-4 col-md-4">
    <div class="product_imgDiv"><img class="img-responsive" src="<?=$imagekit?>" alt="<?=$rowdatM["label"]?>" width="100" height="100" border="0" align="middle" title="<?=$rowdatM["label"]?>"></div>
    </div>
    <a href="/<?=$nice_name?>/"> <div class="col-xs-8 col-sm-8 col-md-7">
     <p><?=$rowdat["name"];?></p>
<p>Item No:  <?=$item_id;?></p>
    </div></a>
    </div>
        </div>
           <div class="col-xs-2 col-sm-2 col-md-2">
    <p><?=number_format($rowMyCart["item_qty"],0)?></p>
        </div>
         <div class="col-xs-3 col-sm-3 col-md-3">
    <p><i class="fa fa-inr"></i>   <?=($rowMyCart["item_price"]*$rowMyCart["item_qty"])?></p>
        </div>    
  </div>
  </div>
    <?  }?>
      <div class="order-details-list-total">
      <div class="row">
      <div class="col-sm-12">
      <ul class="order-detail1 ">
<li>
<div>
Total MRP:</div> <div> <i class="fa fa-inr"></i><?=$ItemGtotal?></div>
</li>
<li>
<div>
Shipping Charges:</div> <div> <i class="fa fa-inr"></i><?=number_format($orderDetail['item_shipping_amount'],0)?></div>
</li>
<li>
</li>
<li>
</li>
<li>
<div>Total</div> <div><i class="fa fa-inr"></i><?=$ItemGtotal+$orderDetail['item_shipping_amount'];?></div>
</li>
</ul>
      </div>
      </div>
      </div>
    </div>	
</div></section>																				
<script type="text/javascript">
$(document).ready(function () {
    var hoverHTMLDemoBasic = '<p>' +
	    '<div class="grn-color">Your order has been placed</div>' +
        '<div><?=$sqlcomplete;?></div></p>';

    $("#demo-basic").hovercard({
        detailsHTML: hoverHTMLDemoBasic,
        width: 260,
		
    });
});
$(document).ready(function () {
    var hoverHTMLDemoBasic = '<p>' +
	    '<div class="grn-color"><?=$sqlstate1;?> </div>' +
        '<div><?=$sqlcomplete1;?></div></p>';

    $("#demo-basic1").hovercard({
        detailsHTML: hoverHTMLDemoBasic,
        width: 260,
    });
});
$(document).ready(function () {
    var hoverHTMLDemoBasic = '<p>' +
	    '<div class="grn-color"><?=$sqlstate2;?> </div>' +
        '<div><?=$sqlcomplete2;?></div><p>';

    $("#demo-basic2").hovercard({
        detailsHTML: hoverHTMLDemoBasic,
        width: 260,
    });
});
$(document).ready(function () {
    var hoverHTMLDemoBasic = '<p>' +
	    '<div class="grn-color"><?=$sqlstate3;?> </div>' +
        '<div><?=$sqlcomplete3;?></div></p>';

    $("#demo-basic3").hovercard({
        detailsHTML: hoverHTMLDemoBasic,
        width: 260,
    });
});
$(document).ready(function () {
    var hoverHTMLDemoBasic = '<p>' +
	    '<div class="grn-color"><?=$sqlstate4;?> </div>' +
        '<div><?=$sqlcomplete5;?></div></p>';

    $("#demo-basic4").hovercard({
        detailsHTML: hoverHTMLDemoBasic,
        width: 260,
    });
});
$(document).ready(function () {
    var hoverHTMLDemoBasic = '<p>' +
	    '<div class="grn-color"><?=$sqlstate5;?> </div>' +
        '<div><?=$sqlcomplete6;?></div></p>';

    $("#demo-basic5").hovercard({
        detailsHTML: hoverHTMLDemoBasic,
        width:260,
    });
});
</script>
<!--Code For print order-->
<script>
function printDiv() {
        var toPrint = document.getElementById('to_print_div');
        var popupWin = window.open('', '_blank', 'width=1020,height=1050,location=no,left=20px');
        popupWin.document.write('<html><title>::Preview::</title><link rel="stylesheet" type="text/css" href="/new/shop/css/myorders.css" /></head><body onload="window.print()">')
		var div_1 = document.getElementById('manage_div');
		div_1.style.display = 'none';
		var div_2 = document.getElementById('track_div');
		div_2.style.display = 'none';
        popupWin.document.write(toPrint.innerHTML);
        popupWin.document.write('</html>');
       
		var div_1 = document.getElementById('manage_div');
		div_1.style.display = 'block';
		var div_2 = document.getElementById('track_div');
		div_2.style.display = 'block';
}
</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>