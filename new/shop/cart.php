<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT . '/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

if ($userid == 'Guest') {
    $qGetMyCartcheck = query_execute("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price,sc.item_discount,sc.item_totalprice FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='0' ");
    
    $qGetMyCartcheck_donate = query_execute("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price,sc.item_discount,sc.item_totalprice FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='1' ");
    
    $qGetMyCartcheck_count = query_execute("SELECT count(sc.item_id) as totoosc FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='outofstock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='0' ");
    
    $qGetMyCartcheck_count_donate = query_execute("SELECT count(sc.item_id) as totoosc FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='outofstock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='1' ");
} else {
    $qGetMyCartcheck = query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid =  '$userid'
AND sc.cart_order_status =  'new' AND si.stock_status =  'instock' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283'
) AND sc.donate_bag='0'");
    
    $qGetMyCartcheck_donate = query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid =  '$userid'
AND sc.cart_order_status =  'new' AND si.stock_status =  'instock' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283'
) AND sc.donate_bag='1'");
    
    $qGetMyCartcheck_count = query_execute("SELECT count(sc.item_id) as totoosc
FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid =  '$userid' AND sc.cart_order_status =  'new' AND si.stock_status =  'outofstock' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283') AND sc.donate_bag='0'");
    
    $qGetMyCartcheck_count_donate = query_execute("SELECT count(sc.item_id) as totoosc
FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid =  '$userid' AND sc.cart_order_status =  'new' AND si.stock_status =  'outofstock' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283') AND sc.donate_bag='1'");
}
while ($rowMyCartcheck = mysql_fetch_array($qGetMyCartcheck)) {
    $item_idcheck         = $rowMyCartcheck["item_id"];
    $cart_idcheck         = $rowMyCartcheck["cart_id"];
    $item_qtycheck        = $rowMyCartcheck["item_qty"];
    $qdatacheck           = query_execute("SELECT name,domain_id, price,tag,feature, weight,nice_name,selling_price, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount,sku FROM shop_items WHERE item_id='$item_idcheck'");
    $rowdatcheck          = mysql_fetch_array($qdatacheck);
    $payment_mode         = $rowdatcheck['payment_mode'];
    $tag                  = $rowdatcheck['tag'];
    $feature              = $rowdatcheck['feature'];
    $cemantaname1[]       = $rowdatcheck['sku'];
    $cemantitemid[]       = $rowMyCartcheck['item_id'];
    $cemantcartid[]       = $rowMyCartcheck['cart_id'];
    $nice_name            = $rowdatcheck['nice_name'];
    $item_shipping_amount = $rowdatcheck['item_shipping_amount'];
    $item_updated_name    = $rowdatcheck['name'];
    if ($rowdatcheck['stock_status'] == 'instock' && $rowdatcheck['item_display_status'] != 'delete') {
        if ($rowMyCartcheck['item_price'] != $rowdatcheck['price'] || $rowMyCartcheck['mrp_price'] != $rowdatcheck['selling_price']) {
            $c_price = round($rowMyCartcheck['item_price']);
            $i_price = round($rowdatcheck['price']);
            if ($c_price < $i_price) {
                $item_updated_text = "The " . $item_updated_name . " has increased from " . $c_price . " to " . $i_price . "";
            } elseif ($c_price > $i_price) {
                $item_updated_text = "The " . $item_updated_name . " has decreased from " . $c_price . " to " . $i_price . "";
            }
            $qGetMyCartchupdate = query_execute("UPDATE shop_cart SET item_price='" . $rowdatcheck['price'] . "', mrp_price='" . $rowdatcheck['selling_price'] . "',
item_totalprice='" . $rowdatcheck['price'] * $item_qtycheck . "' WHERE cart_id='" . $cart_idcheck . "'");
        }
    }
}
$session_id = session_id();
$ip         = ipCheck();
// Check for Hunnypot
if ($newname) {
    header("Location: /error.php?errortext=Sorry Error!");
    exit();
}
?>
<link href="https://www.dogspot.in/template/css/checkout.css" rel="stylesheet" type="text/css" />
<link href="/new/shop/css/cart-page.css" rel="stylesheet" type="text/css" />
<script src="/js/popup.js" type="text/javascript"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<!-----------------------------------------------------------new analytics code------------------------------------------->
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-1552614-5', 'auto');
ga('require', 'ec');
<?
foreach ($cemantcartid as $itemm1) {
    $value_ant_cart  = query_execute_row("SELECT item_id,item_qty FROM shop_cart WHERE cart_id='$itemm1'");
    $value_antly     = query_execute_row("SELECT item_id,name,price,item_brand,child_cat FROM shop_items WHERE item_id='" . $value_ant_cart['item_id'] . "'");
    $cat_analytics   = query_execute_row("SELECT category_name FROM shop_category WHERE category_id='" . $value_antly['child_cat'] . "'");
    $brand_analytics = query_execute_row("SELECT brand_name FROM shop_brand WHERE brand_id='" . $value_antly['item_brand'] . "'");
?>
ga('ec:addProduct', {
'id': '<?= mysql_real_escape_string($value_antly['item_id']); ?>',
'name': '<?= mysql_real_escape_string($value_antly['name']); ?>',
'category': '<?= mysql_real_escape_string($cat_analytics['category_name']); ?>',
'brand': '<?= mysql_real_escape_string($brand_analytics['brand_name']); ?>',
'variant': '',
'price': '<?= mysql_real_escape_string($value_antly['price']); ?>',
'quantity': '<?= stripslashes($value_ant_cart['item_qty']); ?>'
});
<?
}
?>
</script>
<?
if ($donate == "donate") {
?>
<script>
$(document).ready(function(){
$("#donate_summery").css("display", "block");
$("#order_summery").css("display", "none");
$("div#tab-2").addClass('current');
$("div#tab-1").removeClass('current');
$("li#order_items").removeClass('current');
$("li#donate_items").addClass('current');
  });
</script>
<?
}
?>
<!--------------------------------------------------------------ends here------------------------------------------------->
<script type="text/javascript">
//Regular cart empty for remove
$(document).ready(function(){
$('#bhsave').live('click',function(){
var rowCount = $('.clbharat').length;
if(rowCount==1){
$("#order_summery").load(location.href + " #order_summery");
$("#leftcont").load(location.href + " #leftcont");
}else{
return false;
   }
});
//Regular cart empty for wishlist
//$('#bhsave_wis').live('click',function(){
//var rowCount = $('.clbharat').length;
//if(rowCount==1){
//$("#donate_items").load(location.href + " #donate_items");
////$("#leftcont").load(location.href + " #leftcont");
////var destDiv = ['#ds_order_summary_box','#order_summery'];
////var sourceDiv = ['#ds_order_summary_box','#order_summery'];
////shaajaxLoadMultiple('/new/shop/new-cart.php', 'POST', sourceDiv, destDiv);
//   }
//});

$('#d_remove').live('click',function(){
var rowCount = $('.d_rcla').length;
if(rowCount==1){
$("#donate_summery").css("display", "none");
$("#order_summery").css("display", "block");
$("#leftcont").load(location.href + " #leftcont");
   }
});
//Donate cart empty
$('#donate_items').live('click',function(){
var rowCount = $('.d_rcla').length;
if(rowCount==0){
$("#ds_donate_discount").css("display", "none");
   }
$("#flag_1").val('flag_2');
$("#donate_summery").css("display", "block");
$("#order_summery").css("display", "none");
$("#check_hardstk").css("display", "none");
$("div#tab-2").addClass('current');
$("div#tab-1").removeClass('current');
$("li#order_items").removeClass('current');
$("li#donate_items").addClass('current');
   })
$('#order_items').live('click',function(){
$("#flag_1").val('flag_1');
$("#donate_summery").css("display", "none");
$("#order_summery").css("display", "block");
$("div#tab-1").addClass('current');
$("div#tab-2").removeClass('current');
$("li#donate_items").removeClass('current');
$("li#order_items").addClass('current');
  })
})
</script>
<script>
function changecartbox(id)
 {
document.getElementById('showqty_'+id).style.display='none';
document.getElementById('update_'+id).style.display='inline';
document.getElementById('save'+id).style.display='inline';
document.getElementById('change'+id).style.display='none';
 }
function saveupdate(cart_id,cart_item_id){
var cart_item=$('#update_'+cart_id).val();
var flag_name=$('#flag_1').val();
$('#show_'+cart_id).val(cart_item);
var cart_qwe=$('#qqty_'+cart_id).val();
if(cart_qwe==1){
var cart_item_qty=$('#show_'+cart_id).val();
}else{
var cart_item1=$('#q'+cart_id).val();
var cart_item_qty2=$('#show_'+cart_id).val();
if(cart_item_qty2 > cart_item1)
{
var cart_item_qty=cart_item1;
var t=1;
$('#divcartqtyerror'+cart_id).css("display","inline");
}else
{
var cart_item_qty=$('#show_'+cart_id).val();
}
}	
var check_qty1=cart_item_qty.charAt(0);
var check_qty2=cart_item_qty.charAt(1);
var check_qty3=cart_item_qty.charAt(2);
if(check_qty1==0)
{
if(check_qty2==0 && check_qty3!='0' )
{
var cart_item_qty=check_qty3;
}else if( check_qty2!=0)
{
var cart_item_qty=check_qty2.concat(check_qty3);
}
}
if(cart_item_qty<='1') {
cart_item_qty=1;
}
if(isNaN(cart_item_qty))
{
cart_item_qty=1;
}
if(cart_item_qty>='1') {
updateCart='updateCart';
ShaAjaxJquary('/new/shop/carts.php?cat_id1='+cart_id+'&cart_item_qty1='+cart_item_qty+'&cart_item_id1='+cart_item_id+'&updateCart='+updateCart+'', '#cartUpdate23', '', '', 'POST', '#cartUpdateitem_'+cart_id, '<img 	src="/images/indicator.gif" />', 'REP');
if(t==1)
{
$('#divcartqtyerror'+cart_id).css("display","inline");
setTimeout('update_final()',4000);
}else
{
setTimeout('update_final()',500);
 }
}
else
{	
$('#divcartqty'+cart_id).show();	
 }	
}
$('#d_remove').live('click',function(){
$("#donate_summery").css("display", "block");
$("#order_summery").css("display", "none");
$("#tab-2").addClass('current');
$("#tab-1").removeClass('current');
$("#order_items").removeClass('current');
$("#donate_items").addClass('current');	
	});
function update_final()
{
var flage_name=$('#flag_1').val();
var destDiv = ['#tab-2','#tab-1','#cartItemCount1','#order_summery', '#topCartTotal1', '#topItemCount', '#cartTotal','#cartDiscount', '#cartItemCount','#cartshippingTotal1','#donate_summery'];
var sourceDiv = ['#tab-2','#tab-1','#cartItemCount1','#order_summery', '#cartTotal1', '#cartItemCount', '#cartTotal1','#cartDiscount', '#cartItemCount','#cartshippingTotal2','#donate_summery'];
shaajaxLoadMultiple('/new/shop/carts.php', 'POST', sourceDiv, destDiv);
if(flage_name==='flag_2'){
$("#donate_summery").css("display", "block");
$("#order_summery").css("display", "none");
$("#tab-2").addClass('current');
$("#tab-1").removeClass('current');
$("#order_items").removeClass('current');
$("#donate_items").addClass('current');
 }
}
function changevalue(value,cat_id)
{
var cart_item=$('#update_'+cat_id).val();
var flag_name=$('#flag_1').val();
$('#show_'+cat_id).val(cart_item);
var cart_qwe=$('#qqty_'+cat_id).val();
if(cart_qwe==1){
var cart_item_qty=$('#show_'+cat_id).val();
}else{
var cart_item1=$('#q'+cat_id).val();
var cart_item_qty2=$('#show_'+cat_id).val();
if(cart_item_qty2 > cart_item1)
{
var cart_item_qty=cart_item1;
var t=1;
$('#divcartqtyerror'+cat_id).css("display","inline");
}else
{
var cart_item_qty=$('#show_'+cat_id).val();
}
}
$('#show_'+cat_id).val(value);
ShaAjaxJquary("/new/shop/qtycheck.php?cat_id="+cat_id+"&value="+cart_item_qty+"", '#catchange', '', '', 'GET', '#catchange', '','REP');
$('#save'+cat_id).css("display", "block");
}
function changevalue_wr(value,cat_id)
{
$('#save'+cat_id).css("display", "block");
}
</script>
<script>
function check(wishlist_item,wish_cart_id) {	
	ShaAjaxJquary("/new/shop/wishlist/wishlist_new.php?item_id="+wishlist_item+"", "#wishlist12", '', '', 'GET', "#wishlist12", '<img 	src="/images/indicator.gif" />','REP');	
}
</script>
<script type="text/JavaScript">
function valid(f) {	
	f.value = f.value.replace(/[a-zA-Z\/!@#$%^&*()-+.:;,'"\><\s]/ig,'');
} 
</script>
<?
// Check for Hunnypot
// Update Cart------------------------------------------------------------
if ($updateCart) {
    $cat_id               = $cat_id1;
    $cart_item_qty        = $cart_item_qty1;
    $cart_item_id         = $cart_item_id1;
    $qBuySel              = query_execute_row("SELECT name,price, selling_price,virtual_qty,item_status,item_shipping_amount FROM shop_items WHERE item_id = '$cart_item_id'");
    $selling_price        = $qBuySel['selling_price'];
    $price                = $qBuySel['price'];
    $item_status          = $qBuySel['item_status'];
    $item_shipping_amount = $qBuySel['item_shipping_amount'] * $cart_item_qty;
    if ($item_status == 'hard') {
        $virtual_qty = $qBuySel['virtual_qty'];
        if ($cart_item_qty <= $virtual_qty) {
            $status = 1;
        } else {
            $cart_item_qty = $virtual_qty;
            $status        = 0;
        }
    }
    if ($selling_price > $price) {
        $saledisc = (($selling_price * $cart_item_qty) - ($price * $cart_item_qty));
    } else {
        $saledisc = '0';
    }
    if ($cart_item_qty >= '1') {
        if ($userid == 'Guest') {
            $qGetMyCart13 = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc.item_id='1283') AND sc.donate_bag='0'");
        } else {
            $qGetMyCart13 = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$userid' AND sc.cart_order_status = 'new' AND si.stock_status = 'instock' AND si.item_display_status != 'delete' AND ( sc.item_price != '0' OR sc.item_id = '1283' AND sc.donate_bag='0' )");
        }
        $rowCart23        = mysql_fetch_array($qGetMyCart13);
        $cart_item_amount = $rowCart23["order_amount"];
        if (($cart_item_amount + ($price * $cart_item_qty)) <= 20000000) {
            $sqlcatq     = query_execute_row("SELECT item_qty,cart_order_id FROM shop_cart WHERE cart_id = '$cat_id'");
            $salectOrder = query_execute_row("SELECT order_status,delevery_status FROM shop_order WHERE (order_id = '" . $sqlcatq['cart_order_id'] . "' AND order_id!='' AND order_id!='0')");
            if (($salectOrder['order_status'] == 'inc' && $salectOrder['delevery_status'] == 'new') || $sqlcatq['cart_order_id'] == ' ' || $sqlcatq['cart_order_id'] == 0) {
                $qGetcart = query_execute("UPDATE shop_cart SET item_qty='$cart_item_qty',item_discount=$saledisc, item_totalprice=item_price*item_qty WHERE cart_id = '$cat_id'");
                if ($order_id) {
                    $qCart       = query_execute_row("SELECT SUM(item_totalprice) as order_item_amount FROM shop_cart WHERE cart_order_id= '$order_id'");
                    $totalpr     = $qCart['order_item_amount'];
                    $qUpaOrder   = query_execute("UPDATE shop_order SET order_items_amount='$totalpr' WHERE order_id = '$order_id'");
                    $TotalAmount = query_execute_row("SELECT order_shipment_amount FROM shop_order WHERE order_id = '$order_id'");
                }
?>
<script type="text/javascript">
setTimeout('update_final()',1000);
</script>
<?
            }
        }
    }
}
if ($cart_value) {
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$pet_name' WHERE cart_id='$cart_value'  AND cart_meta_name='NAME'");
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$pet_phone' WHERE cart_id='$cart_value'  AND cart_meta_name='PHONE'");
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$alpha' WHERE cart_id='$cart_value'  AND cart_meta_name='feature'");
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$product_d' WHERE cart_id='$cart_value'  AND cart_meta_name='Design'");
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$printeddate' WHERE cart_id='$cart_value'  AND cart_meta_name='date'");
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$product_image' WHERE cart_id='$cart_value'  AND cart_meta_name='Image'");
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$flavour' WHERE cart_id='$cart_value'  AND cart_meta_name='flavour'");
    $update = query_execute("UPDATE shop_cart_meta SET cart_meta_value='$shape' WHERE cart_id='$cart_value'  AND cart_meta_name='shape'");
    $update = query_execute("UPDATE shop_cart SET item_qty='$item_qty' WHERE cart_id='$cart_value'");
}
//echo $userid."ghghhghhghghgh";		
// Update Cart------------------------------------------------------------ END
if ($itemchq == '') {
    $tagitem = query_execute_row("SELECT tag FROM shop_items WHERE item_id='$item_id' ");
    if ($tagitem['tag'] == '1') {
        if ($product_img) {
            $product_image = $dpis . '_' . $product_img;
        }
        addItemToCartItem($item_id, $type_id, $ItemOption, $item_qty, $item_price, $userid, $session_id, $ip, $pet_name, $pet_phone, $alpha, $product_image, $product_d, $printeddate, $flavour, $shape);
        if ($_POST) {
            header("Location: /new/shop/new-cart.php");
        }
    } else if ($donatebag == 1) {
        addItemToCart($item_id, $type_id, $ItemOption, $item_qty, $item_price, $userid, $session_id, $ip, $donatebag);
        if ($_POST) {
            header("Location: /new/shop/new-cart.php");
        }
    } else {
        if ($item_id != '8494') {
            addItemToCart($item_id, $type_id, $ItemOption, $item_qty, $item_price, $userid, $session_id, $ip, 0);
            if ($_POST) {
                header("Location: /new/shop/new-cart.php");
            }
        }
    }
}
?>
<div class="vs20"></div>
<?
if ($userid == 'Guest') {
    $qGetMyCart = query_execute("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price,sc.item_discount,sc.item_totalprice FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='0' ");
    
    $qGetMyCart1 = query_execute_row("SELECT SUM(sc.item_totalprice) as st FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc.item_id='1283') AND sc.donate_bag='0' ");
    
    $qGetMyCartdonate = query_execute("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price,sc.item_discount,sc.item_totalprice FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='1'");
    
    $qGetMyCart1donate = query_execute_row("SELECT SUM(sc.item_totalprice) as st FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc.item_id='1283') AND sc.donate_bag='1'");
} else {
    
    $qGetMyCart1 = query_execute_row("SELECT SUM(sc.item_totalprice) as st FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id
WHERE sc.userid =  '$userid' AND sc.cart_order_status =  'new' AND si.item_display_status !=  'delete' AND si.stock_status =  'instock' AND (
sc.item_price !=  '0' OR sc.item_id =  '1283') AND sc.donate_bag='0'");
    
    $qGetMyCart = query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid =  '$userid'
AND sc.cart_order_status =  'new' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283') AND sc.donate_bag='0'");
    
    $qGetMyCart1donate = query_execute_row("SELECT SUM(sc.item_totalprice) as st FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id
WHERE sc.userid =  '$userid' AND sc.cart_order_status =  'new' AND si.item_display_status !=  'delete' AND (sc.item_price !=  '0' OR sc.item_id =  '1283'
) AND sc.donate_bag='1'");
    
    $qGetMyCartdonate = query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid =  '$userid'
AND sc.cart_order_status =  'new' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283') AND sc.donate_bag='1'");
}
$totrecord        = mysql_num_rows($qGetMyCart);
$totrecord_donate = mysql_num_rows($qGetMyCartdonate);
$total_free       = $qGetMyCart1['st'];

$outofstock_count_data_donate = mysql_fetch_array($qGetMyCartcheck_count_donate);
$outofstock_count_donate      = $outofstock_count_data_donate['totoosc'];
?>
<form id="formCart" name="formCart" method="post" action="">
  <div class="cont980"> 
    <!--tab start-->
    <div class="ds_shopping_cont" id="boxCart"> 
      <!-- cart page left strt-->
      <div class="ds_shopping_contleft" id="leftcont">
        <ul class="dstabs" id="ds_tab_change">
          <li class="tab-link current" data-tab="tab-1" id="order_items">Your Shopping Cart (<?
echo $totrecord;
?>)</li>
          <li class="tab-link" data-tab="tab-2" id="donate_items">Your Donation Cart  (<?
echo $totrecord_donate;
?>)</li>
        </ul>
        <?
if ($totrecord == 0) {
?>
        <!--tab start-->
        <div id="tab-1" class="dstab-content current"> 
          <!-- tab contnet start-->
          <div class="dstab-content_cont"> 
            <!-- empty cart-->
            <div class="empty-cart_cont">
              <div class="">There are no items in this cart.</div>
              <div><a href="/">
                <input class="empty-cart_contshpbtn" type="button" value="Continue Shopping" />
                </a></div>
            </div>
            <div class="empty_cart_catlist">
              <div class="empty_cart_text">Browse Categories</div>
              <div class="empty_cart_sce">
                <ul class="cart_catlist">
                  <li><span class="cart_aero">�</span><a href="/dry-dog-food/">Dry Dog Food</a></li>
                  <li><span class="cart_aero">�</span><a href="/dog-biscuits/">Dog Biscuits</a></li>
                  <li><span class="cart_aero">�</span><a href="/treats/">Dog Treats</a></li>
                  <li><span class="cart_aero">�</span><a href="/bakery-products/">Bakery Products</a></li>
                </ul>
              </div>
              <div class="empty_cart_sce">
                <ul class="cart_catlist">
                  <li><span class="cart_aero">�</span><a href="/shampoos/">Dog Shampoos</a></li>
                  <li><span class="cart_aero">�</span><a href="/conditioners/">Dog Conditioners</a></li>
                  <li><span class="cart_aero">�</span><a href="/dental-care/">Dental Care</a></li>
                  <li><span class="cart_aero">�</span><a href="/grooming-tools/">Grooming Tools</a></li>
                </ul>
              </div>
              <div class="empty_cart_sce">
                <ul class="cart_catlist">
                  <li><span class="cart_aero">�</span><a href="/dog-collars/">Dog Collars</a></li>
                  <li><span class="cart_aero">�</span><a href="/dog-leashes/">Dog Leashes</a></li>
                  <li><span class="cart_aero">�</span><a href="/chaincollars/">Chain Collars</a></li>
                  <li><span class="cart_aero">�</span><a href="/dog-harnesses/">Dog Harnesses</a></li>
                </ul>
              </div>
              <div class="empty_cart_sce">
                <ul class="cart_catlist">
                  <li><span class="cart_aero">�</span><a href="/flea-ticks/">Flea Ticks</a></li>
                  <li><span class="cart_aero">�</span><a href="/odour-remover/">Odour Remover</a></li>
                  <li><span class="cart_aero">�</span><a href="/pet-hair-remover/">Hair Remover</a></li>
                  <li><span class="cart_aero">�</span><a href="/crates/">Dog Crates</a></li>
                </ul>
              </div>
            </div>
            <!-- empty cart--> 
          </div>
          <!-- tab content end--> 
        </div>
        <!-- tab end--><!-- footer-->
        <?
} else {
?>
        <div id="catchange"></div>
        <div id="tab-1" class="dstab-content current">
          <?
    $outofstock_count_data = mysql_fetch_array($qGetMyCartcheck_count);
    $outofstock_count      = $outofstock_count_data['totoosc'];
    
    if ($outofstock_count > 0) {
?>
          <div id="outof_stock" class="out_of_stock_error_text">Some of the items in your cart are Out of stock. To proceed to payment remove or add them to your wishlist</div>
          <?
    }
    if ($item_updated_text != '') {
?>
          <div id="outof_stock" class="out_of_stock_error_text">
            <?= $item_updated_text; ?>
          </div>
          <?
    }
?>
          <!-- tab contnet start-->
          <div class="dstab-content_cont"> 
            <!--header-->
            <table>
              <tr id="clconbha">
                <th class="ds_product_item_cell">ITEM</th>
                <th class="ds_product_name_cell">NAME</th>
                <th class="ds_product_qty_cell">QTY</th>
                <th class="ds_product_unit_cell">UNIT MRP</th>
                <th class="ds_product_unitprice_cell">UNIT PRICE</th>
                <th class="ds_product_seving_cell">SAVING</th>
                <th class="ds_product_subtotal_cell">SUB TOTAL</th>
              </tr>
              <?
    gift("$total_free", "$session_id", "$userid", 'cart');
    $totalitem    = '0';
    $cat_id       = '';
    $cemantaname1 = '';
    while ($rowMyCart = mysql_fetch_array($qGetMyCart)) {
        $item_id              = $rowMyCart["item_id"];
        $cart_id              = $rowMyCart["cart_id"];
        $item_qty             = $rowMyCart["item_qty"];
        $qdata                = query_execute("SELECT name,domain_id, price,tag,feature, weight,nice_name, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount,sku FROM shop_items WHERE item_id='$item_id'");
        $rowdat               = mysql_fetch_array($qdata);
        $payment_mode         = $rowdat['payment_mode'];
        $tag                  = $rowdat['tag'];
        $feature              = $rowdat['feature'];
        $cemantaname1[]       = $rowdat['sku'];
        $cemantitemid[]       = $rowMyCart['item_id'];
        $nice_name            = $rowdat['nice_name'];
        $item_shipping_amount = $rowdat['item_shipping_amount'];
        if ($rowdat['item_display_status'] != 'delete') {
            $ItemGtotal          = $qGetMyCart1["st"];
            $item_shipping_total = ($rowdat["item_shipping_amount"] * $item_qty) + $item_shipping_total;
            $item_parent_id      = $rowdat["item_parent_id"];
            $itemdomain          = $rowdat['domain_id'];
            $totalitem           = $totalitem + 1;
            // Get Tital option
            $qOptionID           = query_execute("SELECT * FROM shop_item_attribute WHERE item_id='$item_id'");
            while ($rowOptionID = mysql_fetch_array($qOptionID)) {
                $attribute_set_id   = $rowOptionID["attribute_set_id"];
                $attribute_value_id = $rowOptionID["attribute_value_id"];
                $arrayAttOp[]       = "$attribute_set_id-$attribute_value_id";
            }
            if ($item_parent_id == '0') {
                $media_item_id = $item_id;
            } else {
                $media_item_id = $item_parent_id;
            }
            $qdataM  = query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id' AND position!='2' AND position!='3' AND position!='4'");
            $rowdatM = mysql_fetch_array($qdataM);
            
            $imagepath = $DOCUMENT_ROOT . '/shop/item-images/thumb_' . $rowdatM["media_file"];
            if (file_exists($imagepath)) {
                $new_w = 100;
                $new_h = 100;
                $imgWH = WidthHeightImg($imagepath, $new_w, $new_h);
            }
?>
              <tr class="spaceUnder clbharat" id="cartitem_<?= $rowMyCart["cart_id"]; ?>">
                <td class="ds_product_item_cell ds_product_item_top" id="cartitem_<?= $rowMyCart["cart_id"]; ?>"><div> <a href="/<?= $nice_name; ?>/"> <img src="https://www.dogspot.in/shop/item-images/thumb_<?= $rowdatM["media_file"] ?>" alt="<?= $rowdatM["label"] ?>" title="<?= $rowdatM["label"] ?>" width="85" height="79" /></a> </div>
                  <?
            if ($rowdat['stock_status'] == 'outofstock') {
?>
                  <div class="cart_out_of_stock">Out Of Stock</div>
                  <?
            }
?></td>
                <td class="ds_product_name_cell ds_product_item_top"><div class="ds_product_name_desc"><a href="/<?= $nice_name; ?>/">
                    <?= $rowdat["name"]; ?>
                    </a></div>
                  <div  class="ds_product_remove ">
                    <?
            if ($userid != 'Guest') {
?>
                    <span><a id="bhsave_wis" onclick="Javascript:ShaAjaxJquary('/shop/remove-cart.php?remove=remove&cart_id=<?= $rowMyCart["cart_id"]; ?>&userid=<?= $userid; ?>&cartnew=new', '#loadingMsg_<?= $rowMyCart["cart_id"]; ?>', '', '', 'GET', '#loadingMsg_<?= $rowMyCart["cart_id"]; ?>', 'update...', 'REP');" 
href="javascript:check('<?= $item_id ?>','<?= $rowMyCart["cart_id"] ?>')">Add to Wishlist</a></span> <span> | </span>
                    <?
            }
?>
                    <span><a href="Javascript:void();"  id="bhsave" onclick="Javascript:ShaAjaxJquary('/shop/remove-cart.php?remove=remove&cart_id=<?= $rowMyCart["cart_id"]; ?>&userid=<?= $userid; ?>&session_id=<?= $session_id ?>&cartnew=new&cartnew=new&type_cart=normal', '#loadingMsg_<?= $rowMyCart["cart_id"]; ?>', '', '', 'GET', '#loadingMsg_<?= $rowMyCart["cart_id"]; ?>', 'update...', 'REP');">Remove</a></span></div>
                  <div  class="ds_product_remove ">
                    <input type="hidden" id="cart_id" name="cart_id" value="<?= $rowMyCart["cart_id"]; ?>" />
                    <div id="loadingMsg_<?= $rowMyCart["cart_id"]; ?>"></div>
                  </div>
                  <div class="ds_product_cod">Cash on Delivery: <span class="ds_product_ava">
                    <?
            if ($payment_mode != 'cod') {
?>
                    Not Available
                    <?
            } else {
?>
                    Available
                    <?
            }
?>
                    </span></div></td>
                <?
            $val      = $item_shipping_amount * $rowMyCart["item_qty"];
            $qGetcart = query_execute("UPDATE shop_cart SET item_shipping_amount=$val WHERE cart_id = '" . $rowMyCart["cart_id"] . "'");
?>
                <input type="hidden" name="qty_<?= $rowMyCart["cart_id"] ?>" id="qty_<?= $rowMyCart["cart_id"] ?>" value="<?= $rowMyCart["item_qty"] ?>"/>
                <?
            if ($rowdat['stock_status'] != 'outofstock') {
?>
                <td class="ds_product_qty_cell"><div id="showqty_<?= $rowMyCart["cart_id"] ?>">
                    <input class="ds_product_qty_text" type="text" maxlength="3" name="update_<?= $rowMyCart["cart_id"] ?>"  id="update_<?= $rowMyCart["cart_id"] ?>" value="<?= $rowMyCart["item_qty"] ?>" onclick="javascript:valid(this);changevalue_wr(this.value,<?= $rowMyCart["cart_id"] ?>);"/>
                  </div>
                  <input type="hidden" maxlength="3" name="show_<?= $rowMyCart["cart_id"] ?>" id="show_<?= $rowMyCart["cart_id"] ?>" value="" 
style="display:none;width:30px;margin-left: 44px;margin-top: -2px;"/>
                  <div class="ds_product_save" style="display:none;" id="save<?= $rowMyCart["cart_id"] ?>"><a href="Javascript:void();" 
onclick="saveupdate(<?= $rowMyCart["cart_id"] ?>,<?= $rowMyCart["item_id"] ?>), changevalue(this.value,<?= $rowMyCart["cart_id"] ?>);">Save</a></div></td>
                <?
                $cat_id .= $rowMyCart["cart_id"] . ',';
?>
                <input name="cart_item_qty<?= $rowMyCart["cart_id"]; ?>" type="hidden" id="cart_item_qty<?= $rowMyCart["cart_id"]; ?>"
value="" />
                <input name="cart_item_id<?= $rowMyCart["cart_id"]; ?>" type="hidden" id="cart_item_id<?= $rowMyCart["cart_id"]; ?>" 
value="<?= $item_id; ?>" />
                <td class="ds_product_unitprice_cell"><span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                  <?= number_format($rowMyCart["mrp_price"], 0) ?>
                  </span></td>
                <td class="ds_product_seving_cell"><span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                  <?= number_format($rowMyCart["item_price"], 0) ?>
                  </span></td>
                <td class="ds_product_seving_cell"><?
                $ttsaving = (round($rowMyCart["mrp_price"]) - round($rowMyCart["item_price"]));
?>
                  <span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                  <?= number_format($rowMyCart["item_qty"] * $ttsaving, 0) ?>
                  </span></td>
                <td class="ds_product_subtotal_cell"><span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                  <?= number_format($rowMyCart['item_price'] * $rowMyCart["item_qty"], 0) ?>
                  </span></td>
                <?
            }
?>
              </tr>
              <?
        }
    }
?>
            </table>
            <!--header-->
            <div id="bannerbottom">
              <?
    $get = $ItemGtotal + 49;
    freemsg("$session_id", "$userid", 'bootm');
?>
            </div>
            <!-- shiiping charges details-->
            <div class="shipping_container" id="stre">
              <div class="shipping_containerleft pad_l5">&nbsp;</div>
              <div class="shipping_containerright">Sub Total: <span><img src="/new/shop/images/Indian_Rupee_symbol2.png" /></span> <span>
                <?= number_format($ItemGtotal) ?>
                </span></div>
            </div>
            <!-- shipping charges details--><!-- shiping continue -->
            <div class="ds_shipping_continue" id="stre1">
              <div class="ds_shopping_continue"> <a href="/">
                <input class="ds_shopping_continuebtn" type="button" value="Continue Shopping" />
                </a> </div>
              <div class="ds_place_order">
                <?
    if ($outofstock_count > 0) {
?>
                <a href="#outof_stock" class="cart_opct">
                <input class="ds_place_orderbtn"  type="button" value="Place Order" />
                </a>
                <?
    } else {
?>
                <a href="/new/shop/login-check.php">
                <input class="ds_place_orderbtn"  type="button" value="Place Order" />
                </a>
                <?
    }
?>
              </div>
            </div>
            <!-- shiping contine end--> 
          </div>
          <!-- tab content end--> 
        </div>
        <?
}
?>
        <?
if ($totrecord_donate == '0') {
?>
        <!--tab start-->
        <div id="tab-2" class="dstab-content"> 
          <!-- tab contnet start-->
          <div class="dstab-content_cont"> 
            <!-- empty cart-->
            <div class="empty-cart_cont">
              <div class="">There are no items in this cart.</div>
              <div><a href="/wagfund/">
                <input class="empty-cart_contshpbtn" type="button" value="Continue Donating" />
                </a></div>
            </div>
            <div class="empty_cart_catlist">

            </div>
            <!-- empty cart--> 
          </div>
          <!-- tab content end--> 
        </div>
        <!-- tab end--><!-- footer-->
        <?
} else {
?>
        <!-- tab content 2-->
        <div id="tab-2" class="dstab-content" id="leftcont_donate">
       <?
    if ($outofstock_count_donate > 0) {
?>
          <div id="outof_stock" class="out_of_stock_error_text">Some of the items in your cart are Out of stock. To proceed to payment remove them</div>
       <?
    }
?>
        <!-- tab contnet start-->
        <div class="dstab-content_cont" id="dcount_discount"> 
          <!--header-->
          <table>
            <tr >
              <th class="ds_product_item_cell">ITEM</th>
              <th class="ds_product_name_cell">NAME</th>
              <th class="ds_product_qty_cell">QTY</th>
              <th class="ds_product_unit_cell">UNIT MRP</th>
              <th class="ds_product_unitprice_cell">UNIT PRICE</th>
              <th class="ds_product_seving_cell">SAVING</th>
              <th class="ds_product_subtotal_cell">SUB TOTAL</th>
            </tr>
            <?
    gift("$total_free", "$session_id", "$userid", 'cart');
    $totalitem_donate    = '0';
    $cat_id_donate       = '';
    $cemantaname1_donate = '';
    while ($rowMyCartdonate = mysql_fetch_array($qGetMyCartdonate)) {
        $item_id_donate        = $rowMyCartdonate["item_id"];
        $cart_id_donate        = $rowMyCartdonate["cart_id"];
        $item_qty              = $rowMyCartdonate["item_qty"];
        $qdata_donate          = query_execute("SELECT name,domain_id, price,tag,feature, weight,nice_name, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount,sku FROM shop_items WHERE item_id='$item_id_donate'");
        $rowdat_donate         = mysql_fetch_array($qdata_donate);
        $payment_mode          = $rowdat_donate['payment_mode'];
        $tag                   = $rowdat_donate['tag'];
        $feature               = $rowdat_donate['feature'];
        $cemantaname1_donate[] = $rowdat_donate['sku'];
        $cemantitemid[]        = $rowMyCartdonate['item_id'];
        $nice_name             = $rowdat_donate['nice_name'];
        $item_shipping_amount  = $rowdat_donate['item_shipping_amount'];
        if ($rowdat_donate['item_display_status'] != 'delete') {
            $ItemGtotal_donate   = $qGetMyCart1donate["st"];
            $item_shipping_total = ($rowdat_donate["item_shipping_amount"] * $item_qty) + $item_shipping_total;
            $item_parent_id      = $rowdat_donate["item_parent_id"];
            $itemdomain          = $rowdat_donate['domain_id'];
            $totalitem_donate    = $totalitem_donate + 1;
            // Get Tital option
            $qOptionID           = query_execute("SELECT * FROM shop_item_attribute WHERE item_id='$item_id_donate'");
            while ($rowOptionID = mysql_fetch_array($qOptionID)) {
                $attribute_set_id   = $rowOptionID["attribute_set_id"];
                $attribute_value_id = $rowOptionID["attribute_value_id"];
                $arrayAttOp[]       = "$attribute_set_id-$attribute_value_id";
            }
            if ($item_parent_id == '0') {
                $media_item_id = $item_id_donate;
            } else {
                $media_item_id = $item_parent_id;
            }
            $qdataM  = query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id' AND position!='2' AND position!='3' AND position!='4'");
            $rowdatM = mysql_fetch_array($qdataM);
            
            $imagepath = $DOCUMENT_ROOT . '/shop/item-images/thumb_' . $rowdatM["media_file"];
            if (file_exists($imagepath)) {
                $new_w = 100;
                $new_h = 100;
                $imgWH = WidthHeightImg($imagepath, $new_w, $new_h);
            }
?>
            <tr class="spaceUnder d_rcla" id="cartitem_<?= $rowMyCartdonate["cart_id"]; ?>">
              <td class="ds_product_item_cell ds_product_item_top" id="cartitem_<?= $rowMyCartdonate["cart_id"]; ?>"><div> <a href="/<?= $nice_name; ?>/"> <img src="https://www.dogspot.in/shop/item-images/thumb_<?= $rowdatM["media_file"] ?>" alt="<?= $rowdatM["label"] ?>" title="<?= $rowdatM["label"] ?>" width="85" height="79" /></a> </div>
                <?
            if ($rowdat_donate['stock_status'] == 'outofstock') {
?>
                <div class="cart_out_of_stock">Out Of Stock</div>
                <?
            }
?></td>
              <td class="ds_product_name_cell ds_product_item_top"><div class="ds_product_name_desc"><a href="/<?= $nice_name; ?>/">
                  <?= $rowdat_donate["name"]; ?>
                  </a></div>
                <div  class="ds_product_remove "> <span> <a href="Javascript:void();" id="d_remove"  onclick="Javascript:ShaAjaxJquary('/shop/remove-cart.php?remove=remove&cart_id=<?= $rowMyCartdonate["cart_id"]; ?>&userid=<?= $userid; ?>&session_id=<?= $session_id ?>&cartnew=new&type_cart=donate', '#loadingMsg_<?= $rowMyCartdonate["cart_id"]; ?>', '', '', 'GET', '#loadingMsg_<?= $rowMyCartdonate["cart_id"]; ?>', 'update...', 'REP');">Remove</a></span> </div>
                <div  class="ds_product_remove ">
                  <input type="hidden" id="cart_id" name="cart_id" value="<?= $rowMyCartdonate["cart_id"]; ?>" />
                  <div id="loadingMsg_<?= $rowMyCartdonate["cart_id"]; ?>"></div>
                </div>
                <div class="ds_product_cod">Cash on Delivery: <span class="ds_product_ava">
                  <?
            if ($payment_mode != 'cod') {
?>
                  Not Available
                  <?
            } else {
?>
                  Available
                  <?
            }
?>
                  </span></div></td>
              <?
            $val      = $item_shipping_amount * $rowMyCartdonate["item_qty"];
            $qGetcart = query_execute("UPDATE shop_cart SET item_shipping_amount=$val WHERE cart_id = '" . $rowMyCartdonate["cart_id"] . "'");
?>
              <input type="hidden" name="qty_<?= $rowMyCartdonate["cart_id"] ?>" id="qty_<?= $rowMyCartdonate["cart_id"] ?>" value="<?= $rowMyCartdonate["item_qty"] ?>"/>
              <?
            if ($rowdat_donate['stock_status'] != 'outofstock') {
?>
              <td class="ds_product_qty_cell"><div id="showqty_<?= $rowMyCartdonate["cart_id"] ?>">
                  <input class="ds_product_qty_text" type="text" maxlength="3" name="update_<?= $rowMyCartdonate["cart_id"] ?>"  id="update_<?= $rowMyCartdonate["cart_id"] ?>" value="<?= $rowMyCartdonate["item_qty"] ?>" onclick="javascript:valid(this);changevalue_wr(this.value,<?= $rowMyCartdonate["cart_id"] ?>);"/>
                </div>
                <input type="hidden" maxlength="3" name="show_<?= $rowMyCartdonate["cart_id"] ?>" id="show_<?= $rowMyCartdonate["cart_id"] ?>" value="" 
style="display:none;width:30px;margin-left: 44px;margin-top: -2px;"/>
                <div class="ds_product_save" style="display:none;" id="save<?= $rowMyCartdonate["cart_id"] ?>"><a href="Javascript:void();" 
onclick="saveupdate(<?= $rowMyCartdonate["cart_id"] ?>,<?= $rowMyCartdonate["item_id"] ?>), changevalue(this.value,<?= $rowMyCartdonate["cart_id"] ?>);">Save</a> </div>
                <input type="hidden" value="flag_1" id="flag_1" name="flag_1"/></td>
              <?
                $cat_id_donate .= $rowMyCartdonate["cart_id"] . ',';
?>
              <input name="cart_item_qty<?= $rowMyCartdonate["cart_id"]; ?>" type="hidden" id="cart_item_qty<?= $rowMyCartdonate["cart_id"]; ?>"
value="" />
              <input name="cart_item_id<?= $rowMyCartdonate["cart_id"]; ?>" type="hidden" id="cart_item_id<?= $rowMyCartdonate["cart_id"]; ?>" 
value="<?= $item_id_donate; ?>" />
              <td class="ds_product_unitprice_cell"><span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                <?= number_format($rowMyCartdonate["mrp_price"], 0) ?>
                </span></td>
              <td class="ds_product_seving_cell"><span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                <?= number_format($rowMyCartdonate["item_price"], 0) ?>
                </span></td>
              <td class="ds_product_seving_cell"><?
                $ttsaving = (round($rowMyCartdonate["mrp_price"]) - round($rowMyCartdonate["item_price"]));
?>
                <span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                <?= number_format($rowMyCartdonate["item_qty"] * $ttsaving, 0) ?>
                </span></td>
              <td class="ds_product_subtotal_cell"><span class="ruppe_icon"><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span><span class="ds_product_price">
                <?= number_format($rowMyCartdonate['item_price'] * $rowMyCartdonate["item_qty"], 0) ?>
                </span></td>
              <?
            }
?>
            </tr>
            <?
        }
    }
?>
          </table>
          <!--header--><!-- shiiping charges details-->
          <div id="bannerbottom">
            <?
    $get = $ItemGtotal_donate + 49;
    freemsg("$session_id", "$userid", 'bootm');
?>
          </div>
          <!-- shiiping charges details-->
          <div class="shipping_container">
            <div class="shipping_containerleft pad_l5">&nbsp;</div>
            <div class="shipping_containerright">Sub Total: <span><img src="/new/shop/images/Indian_Rupee_symbol2.png" /></span> <span>
              <?= number_format($ItemGtotal_donate) ?>
              </span></div>
          </div>
          <!-- shipping charges details--><!-- shiping continue -->
          <div class="ds_shipping_continue">
            <div class="ds_shopping_continue"> <a href="/wagfund/">
              <input class="ds_shopping_continuebtn" type="button" value="Continue Donating" />
              </a> </div>
            <div class="ds_place_order">
              <?
    if ($outofstock_count_donate > 0) {
?>
              <a href="#outof_stock" class="cart_opct">
              <input class="ds_place_orderbtn"  type="button" value="Place Order" />
              </a>
              <?
    } else {
        if ($userid == 'Guest') {
?>
              <a href="/new/shop/login-check.php?donatebag=1&shipping_address=address&address_id=67826">
              <input class="ds_place_orderbtn"  type="button" value="Place Order" />
              </a>
              <?
        } else {
?>
              <a href="/new/shop/new-payment.php?donatebag=1&shipping_address=address&address_id=67826" >
              <input class="ds_place_orderbtn"  type="button" value="Place Order" />
              </a>
              <?
        }
    }
?>
            </div>
          </div>
          <!-- shiping contine end--> 
        </div>
        <!-- tab content end--> 
      </div>
      <!-- tab content 2 end-->
      <?
}
?>
    </div>
    <!-- cart page left end-->
    <?
if ($totrecord != '0') {
?>
    <!--cart page right start-->
    <div class="ds_shopping_contright" id="order_summery" > 
      <!-- order summery box-->
      <div class="ds_order_summary_box">
        <div class="summery"> Order Summary </div>
        <!-- order details box-->
        <div class="ds_order_details">
          <ul id="boxCart">
            <li>
              <div class="ds_order_details_left">Sub Total:</div>
              <div class="ds_order_details_right"><span><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span> <span id="cartTotal1">
                <?= number_format($ItemGtotal) ?>
                </span></div>
            </li>
            <li>
              <div class="ds_order_details_left">Shipping Charges:</div>
              <div class="ds_order_details_right"><span><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span>49</div>
            </li>
            <li>
              <div class="ds_order_details_left">COD Charges:</div>
              <div class="ds_order_details_right"><span><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span> <span>0</span></div>
            </li>
            <li class="ds_order_total">
              <div class="ds_order_details_left"> <span class="place_order_bld">Order Total:</span> </div>
              <div class="ds_order_details_right"><span><img src="/new/shop/images/Indian_Rupee_symbol.png" /> </span> <span class="place_order_bld">
                <?= number_format($ItemGtotal + 49) ?>
                </span></div>
            </li>
          </ul>
        </div>
        <div class="place_order">
          <?
    if ($outofstock_count > 0) {
?>
          <a href="#outof_stock" class="cart_opct">
          <input type="button"  class="place_order_btn" value="Place Order" />
          </a>
          <?
    } else {
?>
          <a href="/new/shop/login-check.php">
          <input type="button"  class="place_order_btn" value="Place Order" />
          </a>
          <?
    }
?>
        </div>
        <!-- order detals box--> 
      </div>
    </div>
    <!--cart page right end-->
    <?
}
?>
    <?
if ($totrecord_donate != '0') {
?>
    <!--cart page right start DONATION-->
    <div class="ds_shopping_contright" style="display:none;" id="donate_summery" > 
      <!-- order summery box-->
      <div class="ds_order_summary_box" id="ds_donate_discount">
        <div class="summery"> Order Summary </div>
        <!-- order details box-->
        <div class="ds_order_details">
          <ul id="boxCart">
            <li>
              <div class="ds_order_details_left">Sub Total:</div>
              <div class="ds_order_details_right"><span><img src="/new/shop/images/Indian_Rupee_symbol.png" /></span> <span id="cartTotal1">
                <?= number_format($ItemGtotal_donate) ?>
                </span></div>
            </li>
            <li>
              <div class="ds_order_details_left">Shipping Charges:</div>
              <div class="ds_order_details_right"><span>Free</span></div>
            </li>
            <li class="ds_order_total">
              <div class="ds_order_details_left"> <span class="place_order_bld">Order Total:</span> </div>
              <div class="ds_order_details_right"><span><img src="/new/shop/images/Indian_Rupee_symbol.png" /> </span> <span class="place_order_bld">
                <?= number_format($ItemGtotal_donate) ?>
                </span></div>
            </li>
          </ul>
        </div>
        <div class="place_order">
          <?
    if ($outofstock_count_donate > 0) {
?>
          <a href="#outof_stock" class="cart_opct">
          <input class="place_order_btn"  type="button" value="Place Order" />
          </a>
          <?
    } else {
        if ($userid == 'Guest') {
?>
          <a href="/new/shop/login-check.php?donatebag=1&shipping_address=address&address_id=67826">
          <input class="place_order_btn"  type="button" value="Place Order" />
          </a>
          <?
        } else {
?>
          <a href="/new/shop/new-payment.php?donatebag=1&shipping_address=address&address_id=67826">
          <input class="place_order_btn"  type="button" value="Place Order" />
          </a>
          <?
        }
    }
?>
        </div>
        <!-- order detals box--> 
      </div>
    </div>
    <!--cart page right end DONATION-->
    <?
}
?>
  </div>
  <!-- tab end-->
  </div>
  <input name="fishtype" type="hidden" id="fishtype" value="<?= $fishtype; ?>" />
  <input name="cat_id" type="hidden" id="cat_id" value="<?= $cat_id; ?>" />
  <input name="updateCart" type="hidden" id="updateCart" value="updateCart" />
  <input name="checkoutTotal" type="hidden" id="checkoutTotal" value="<?= $ItemGtotal ?>" />
</form>
