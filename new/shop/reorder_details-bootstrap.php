<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

if($userid=='Guest'){
	header("Location: /login.php?refUrl=/shop/reorder_details.php");
	ob_end_flush();
	exit(); 
}
$sitesection = "shop";
$session_id = session_id();
$title="Re-Orders | DogSpot";
$keyword="Dogspot Re-Orders";
$desc="Dogspot Re-Orders";
 
    $alternate="https://www.dogspot.in/brand/";
	$canonical="http://m.dogspot.in/all-brands.php";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='Brand';
require_once($DOCUMENT_ROOT.'/new/common/header-bootstrap.php'); ?>

<style>
.table th{ background:#f8f8f8}
</style>
<div class="container">
<div class="row">
<!--<div style="float:right; margin-left:5px; width:780px; margin-top:5px; ">
--><?
//echo "SELECT * FROM shop_order WHERE userid = '$userid' AND mode != 'TEST' AND order_status != 'inc' ORDER BY order_c_date DESC";
$qGetMyCart=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND order_status != 'inc'  ORDER BY order_c_date DESC");
$totrecord = mysql_num_rows($qGetMyCart);
?>
<div class="col-sm-12"><h1>Wish to Reorder  <span style="font-size:13px; margin-left:3px;">
(Total Orders: <?=$totrecord;?>)</span></h1></div>
<?
if($totrecord == 0){
	echo '<div class="vs10"></div>';
	echo '<h2>Sorry no Order...</h2>';?>
	
	<div ><a href="/shop/" class="redButton cornerRoundAll">Continue Shopping</a></div>
	
	<? ;
}else{
?>
 <table class="table">
<thead>
<tr>
      <th width="18%">Order Date</th>
      <th width="18%">Order Id</th>
      <th width="18%">Order  Amount</th>
      <th width="18%">Payment Method</th>
      <th width="10%">Qty</th>
      <th width="18%">Want To Reorder</th>
      
    </tr></thead><tbody>
	<?
	while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart["order_id"]."' ORDER BY order_c_date ");
		$totalord=$partialdetail['ord'];
		$partilord=explode(',',$totalord);
	?>
    
   <tr>
   <td><?=showdate($rowMyCart["order_c_date"], "d M o")?></td>
      <td><?=$rowMyCart["order_id"];?></td>
     <td>Rs. <? if($rowMyCart["order_status"]=='0'){$paidamu=$paidamu+$rowMyCart["order_amount"];}else{$unpaidamu=$unpaidamu+$rowMyCart["order_amount"];}?><?=number_format($rowMyCart["order_amount"],0);?></td>
      <? 
	   $qGetMyCartCount=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart["order_id"]."'");
	   $totitem = mysql_num_rows($qGetMyCartCount);
	 ?>
      <td><?=$AOrderMethod[$rowMyCart["order_method"]];?></td>
     <td><?=$totitem;?></td>      
      <td><a href="reorders.php?order_id=<?=$rowMyCart["order_id"];?>" style="text-decoration:underline;">Reorder</a></td>
     
    </tr>
    
<? }?> 
</tbody></table>
<? }?>
<div>
<div id="clearall"></div>
</div>
</div>
</div>

<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>