<?php
function microtime_float(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
$time_start = microtime_float();

ob_start();
session_start();
//require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/function-session.php');
if($sitesection != "add-events"  ){
include($DOCUMENT_ROOT . "/facebookconnect/facebooksettings.php");
include($DOCUMENT_ROOT . "/facebookconnect/facebookfunctions.php");
include($DOCUMENT_ROOT . '/twitterconnect/twitterfunctions.php');
}
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
if($sitesection!='shop'){
	header('Cache-Control: max-age=28800, must-revalidate');
}

header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
//header('Content-Type: text/html; charset=ISO-8859-1');

//$refUrl = $_SERVER['REQUEST_URI'];
/*if($utm_source=='VCommission'){
	session_register("utm");
	setcookie("utm", $utm_source, time()+3600*24*30, "/"); 
}*/
//rememeber me----------------
if(isset($_POST['remembermenew'])) {
if(isset($_COOKIE['pwd']) && !empty($_COOKIE['pwd'])){
    $pwd = $_COOKIE['pwd'];
}else{
    $pwd = '';
}
//same for unm 

if(isset($_COOKIE['unm']) && !empty($_COOKIE['unm'])){
    $unm = $_COOKIE['unm'];
}else{
    $unm = '';
}
}
//rememeber me ends-----------------------


if($utm_source=='Vcommission' && $utm_campaign=='VcommissionCPV'){
    session_register("utmcpv");
    setcookie("utmcpv", $utm_campaign, time()+3600*24*30, "/");/* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
	//unset($_COOKIE["utmcps"]);
	//unset($_COOKIE["utmkomli"]);
	//unset($_COOKIE["utmiccps"]);
	//unset($_COOKIE["utmiccpv"]);	
	//unset($_COOKIE["utmcouponrani"]);
	//unset($_COOKIE["utmpayoom"]);
}
if($utm_source=='Vcommission' && $utm_campaign=='VcommissionCPS'){
    session_register("utmcps");
    setcookie("utmcps", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}
if($utm_source=='Komli' && $utm_campaign=='DSKMCPS'){
    session_register("utmkomli");
    setcookie("utmkomli", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}
if($utm_source=='icubes' && $utm_campaign=='ICCPS'){
    session_register("utmiccps");
    setcookie("utmiccps", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}
if($utm_source=='icubes' && $utm_campaign=='ICCPV'){
    session_register("utmiccpv");
    setcookie("utmiccpv", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}
if($utm_source=='Couponrani' && $utm_campaign=='Couponranisalebanner'){
    session_register("utmcouponrani");
    setcookie("utmcouponrani", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}
if($utm_source=='payoom' && $utm_campaign=='payoomCPS'){
    session_register("utmpayoom");
    setcookie("utmpayoom", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}
if($utm_source=='Upto75' && $utm_campaign=='Upto75CPS'){
    session_register("utmupto");
    setcookie("utmupto", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}
if($utm_source=='OMG' && $utm_campaign=='OMGCPS'){
    session_register("utmomg");
    setcookie("utmomg", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmiccps', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
}
if($utm_source=='pointific' && $utm_campaign=='dspfcps'){
    session_register("utmpointific");
    setcookie("utmpointific", $utm_campaign, time()+3600*24*30, "/"); /* expire in 30 Days */
	setcookie('utmcps', null, -1, '/');
	setcookie('utmkomli', null, -1, '/');
	setcookie('utmcpv', null, -1, '/');
	setcookie('utmiccpv', null, -1, '/');
	setcookie('utmcouponrani', null, -1, '/');
	setcookie('utmpayoom', null, -1, '/');
	setcookie('utmupto', null, -1, '/');
	setcookie('utmomg', null, -1, '/');
}

if($refUrl == '/petfed/'){  $refUrl='/pet-fed/select-shows.php';}

if($refurl_wag!=''){
	$refUrl=$refurl_wag;
}elseif($refUrl==''){
$refUrl = $_SESSION['refUrl'];}
$session_id = session_id();
//if()
//$refUrl = $_SERVER['HTTP_REFERER'];
$user_ip = ipCheck();
//echo 'ded'.$login_btn;
//$login_btn_new=$_POST['login_btn_new'];
if ($_POST['login_btn_new']) { 

ob_start();
session_start();
    //echo 'chufdxvgdfvdfdfsd';
    $cookieTime = time() + 60 * 60 * 24 * 100;
	//$login_email_new=mysql_real_escape_string($login_email_new);
	//$passwordnew=mysql_real_escape_string($login_email_new);
	$login_email_new=$_POST['login_email_new'];
	$login_email_new=$_POST['login_email_new'];
    $checkuser  = mysql_query("SELECT ID, userid, u_email, userlevel, f_name, gender, image, auth FROM users WHERE (userid = '$login_email_new' OR u_email = '$login_email_new') AND u_password = '$passwordnew' AND user_status != '0'");
    if ($sessionid = mysql_fetch_array($checkuser)) {
        $sessUserEmail = $sessionid["u_email"];
        $sessionDPid   = $sessionid["ID"];
        $sessionuserid = $sessionid["userid"];
        
        if ($sessionid["f_name"]) {
            $sessionName = $sessionid["f_name"];
        } else {
            $sessionName = $sessUserEmail;
        }
        if ($sessionid["userid"]) {
            $sessionuserid = $sessionid["userid"];
        } else {
            $sessionuserid = $sessUserEmail;
        }
        $sessionLevel      = $sessionid["userlevel"];
        $sessionProfileImg = $sessionid["image"];
        $gender            = $sessionid["gender"];
        $auth              = $sessionid["auth"];
        if ($auth == 0) {
            header("Location: /reg-log/verify-email.php?userid=$sessionuserid&reauth=1");
            exit();
        }
        
        $selectUserEmail = mysql_query("SELECT userid, u_email FROM users WHERE u_email = '$sessUserEmail'");
        $useridNew       = $selectUserEmail['userid'];
        $numUserEmail    = mysql_num_rows($selectUserEmail);
        
        if ($GetfbAccTokan && $Getfb_userid) {
            $fbkeyarray = fbUser_Data_array($Getfb_userid, $GetfbAccTokan);
            
            if ($fbkeyarray[fb_userid]) {
                foreach ($fbkeyarray as $fbUserDataKey => $fbUserDataValue) {
                    $sqlInsertFBuser = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$useridNew', '" . $fbUserDataKey . "', '" . $fbUserDataValue . "')");
                    if (!$sqlInsertFBuser) {
                        die(mysql_error());
                    }
                    // echo"$fbUserDataKey => $fbUserDataValue <br>";
                }
            }
        } //if GetfbAccTokan && Getfb_userid End
        //if facebook user then, Get user info from facebook..End....#########################
        
        // facebook Id 
        $selectfb = mysql_query("SELECT meta_key, meta_value FROM users_meta WHERE userid = '$useridNew' AND (meta_key = 'fb_userid' OR meta_key = 'fb_token')");
        if (!$selectfb) {
            die(mysql_error());
        }
        $totfb = mysql_num_rows($selectfb);
        if ($totfb > 0) {
            while ($rowfb = mysql_fetch_array($selectfb)) {
                $meta_key              = $rowfb["meta_key"];
                $meta_value            = $rowfb["meta_value"];
                $fbkeyarray[$meta_key] = "$meta_value";
            }
            $sessionFacebook = $fbkeyarray["fb_userid"] . "-##-" . $fbkeyarray["fb_token"];
            session_register("sessionFacebook");
            setcookie("cookFacebook", $sessionFacebook, $cookieTime, "/");
        }
        // facebook Id END
        // Twitter details
        //require_once('common/twitter-session.php');
        // end Twitter details
        //	Social Action
        //require_once('common/social_action.php');
        //	Social Action END
        
        $AllFriends = $numProfile = totalRows(friends_id, friends, "(created_id ='$sessionuserid' OR friend_id ='$sessionuserid') AND status = 'A'");
        $numProfile = totalRows(profile_id, user_profile, "userid = '$sessionuserid'");
        $numBreed   = totalRows(breed, users_fav_breed, "userid = '$sessionuserid'");
        
        // create session
		$_SESSION['sessionDPid'] = $sessionDPid;
		$_SESSION['sessionuserid'] = $sessionuserid;
		$_SESSION['sessUserEmail'] = $sessUserEmail;
		$_SESSION['sessionName'] = $sessionName;
		$_SESSION['sessionLevel'] = $sessionLevel;
		$_SESSION['sessionProfileImg'] = $sessionProfileImg;
		
		echo $sessionDPid;
		// Encript the userid 
		echo $DogSpotData = encrypt($sessionDPid, SALTDATA);
		// Create Cookie
		$cookieTime = time()+60*60*24*100;
		setcookie("DogSpotData", $DogSpotData, $cookieTime, '/','dogspot.in', false, true);
		echo $sessionName;
		exit();
		
		
/*        session_register("sessionDPid");
        session_register("sessionuserid");
       session_register("sessUserEmail");
        session_register("sessionName");
       session_register("sessionLevel");
        session_register("sessionProfileImg");*/
        // Create Cookie
        //if($remembermenew){
/*      setcookie("cookid", $sessionuserid, $cookieTime, "/");
        setcookie("DPid", $sessionDPid, $cookieTime, "/");
        setcookie("cookEmail", $sessUserEmail, $cookieTime, "/");
        setcookie("cookName", $sessionName, $cookieTime, "/");
        setcookie("cookLevel", $sessionLevel, $cookieTime, "/");
        setcookie("cookProfileImg", $sessionProfileImg, $cookieTime, "/");*/
        //}
        
        lastVisit($sessionuserid);
        if ($sessionLevel=='7') {
            header("Location: /market-place/awb-upload.php");
            exit();
        }
        if($refUrl){
		        header("Location: $refUrl");
        exit();
			
        }elseif($numUserEmail > 1 ){
        header("Location: /user-concanate.php");
        exit();
        }elseif(!$gender || $numProfile == 0 || $numBreed == 0){
        header("Location: /reg-log/more.php");
        exit();
        }elseif($AllFriends < 10){
        header("Location: /profile/invite-friends.php");
        exit();
        }else{
			
        header("Location: /profile/");
        exit();
        }
       
    } else {
        $errorFlag = 1;
		// header("Location: /login.php?errorFlag = 1");
		//}
    } //Ens if sessionid
    
} //End if Login

//......................................................................................................................//..................... 

// if account create is selected.......
if ($account_btn_new) {
	//echo "abcd";
	if(!$robotest){
    $chars       = '0123456789ABCDEFGHIJabchefghijklmnopqrstuvwyzKLMNOPQRSTUVWXYZ';
    $length      = 8;
    $max_i       = strlen($chars) - 1;
    $author_code = '';
    for ($i = 0; $i < $length; $i++) {
        $author_code .= $chars{mt_rand(0, $max_i)};
    }
    //generate random varchar data.............................End.................
    
    // record inserted on Members table
    if ($fb_email) {
        $auth = 1;
    } else {
        $auth = 0;
    }
    $cust_email_new    = trim($cust_email_new);
    $fullnamenew      = trim($fullnamenew);
    $cust_password_new = trim($cust_password_new);
	
	$fullnamenew1     = trim($fullnamenew1);
    $fullname1     = explode(' ', $fullnamenew);
    $fullname      = $fullname1[0];
    $lastname      = $fullname1[1];
	
	// echo "SELECT count(*) as user4 FROM users WHERE (userid = '$cust_email_new' OR u_email='$cust_email_new') ";
	 $checkregister  = query_execute_row("SELECT count(*) as user4 FROM users WHERE (userid = '$cust_email_new' OR u_email='$cust_email_new') ");
		
              if ($checkregister['user4']=='0') {   
			 // echo "INSERT INTO users (userid, u_password, u_email, f_name,l_name, updatenews, c_date, current_visit, last_visit, a_code, auth, user_ip)VALUES('', '$cust_password_new', '$cust_email_new', '$fullname','$lastname', 'Y', NULL, NULL, NULL, '$author_code', '$auth', '$user_ip')";  
    $resultinsert  = mysql_query("INSERT INTO users (userid, u_password, u_email, f_name,l_name, updatenews, c_date, current_visit, last_visit, a_code, auth, user_ip, dog_breed, mobile1)VALUES('', '$cust_password_new', '$cust_email_new', '$f_name','$l_name', 'Y', NULL, NULL, NULL, '$author_code', '$auth', '$user_ip', '$dg_breed', '$mobile1')");
    $get_id        = mysql_insert_id();
    $refuserid = query_execute("Update users SET userid='$get_id' where u_email='$cust_email_new' ");
    $userid    = $get_id;
    $refid12   = $userid . "REF" . $get_id;
    $ref       = query_execute("Update users SET ref_userid='$refid12' where userid='$userid' ");
    
    // end referal 
     if($cust_email_new=='jyotimmmec05@gmail.com')
		{
			echo 'jyoti';
		}
    
    $userpro = explode(";", $youarenew);
    $insert  = mysql_query("INSERT INTO user_profile (userid, profile_id, profile_name) Values('$get_id', '$userpro[0]', '$userpro[1]')");
	if($dg_name!=''){
	$dog_nicename = createSlug($dg_name);
	$dog_nicename = checkSlugAll('dogs_available', 'dog_nicename', $dog_nicename);
	$sqlsebe=query_execute_row("SELECT nicename FROM dog_breeds WHERE breed_name='$dg_breed'");
	if($dg_name!=''){
	$insertdgname  = mysql_query("INSERT INTO dogs_available (userid, dog_name, dog_nicename, dog_breed, breed_nicename, cdate) Values('$get_id', '$dg_name', '$dog_nicename', '$dg_breed', '".$sqlsebe['nicename']."', NULL)");
	}
	}
    if (!$insert) {
        die(mysql_error());
    } else {
        $errorFlag = 0;
    }
    
    if (!$resultinsert || $errorFlag != 0) {
        die(mysql_error());
    } else {
         if($cust_email_new=='jyotimmmec05@gmail.com')
		{
			echo $userpro;
		}
        $user_imap_email = "$get_id$get_id@m.dogspot.in";
        
        $updateImap = mysql_query("UPDATE users SET imapmail = '$user_imap_email' WHERE userid = '$get_id'"); //For update user imap Email
        if (!$updateImap) {
            die(mysql_error());
        }
        
        // update Facebook Details
        if ($fb_userid) {
            $insertFBuser = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$get_id', 'fb_userid', '$fb_userid')");
            if (!$insertFBuser) {
                die(mysql_error());
            }
            
            $insertFBtoken = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$get_id', 'fb_token', '$fb_token')");
            if (!$insertFBtoken) {
                die(mysql_error());
            }
            
            $insertFBemail = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$get_id', 'fb_email', '$fb_email')");
            if (!$insertFBemail) {
                die(mysql_error());
            }
            
            if ($fb_name) {
                $insertFBname = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$get_id', 'fb_name', '$fb_name')");
                if (!$insertFBname) {
                    die(mysql_error());
                }
            }
			 
            if ($fb_dob) {
                $insertFBdob = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$get_id', 'fb_dob', '$fb_dob')");
                if (!$insertFBdob) {
                    die(mysql_error());
                }
            }
            
            if ($fb_link) {
                $insertFBlink = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$get_id', 'fb_link', '$fb_link')");
                if (!$insertFBlink) {
                    die(mysql_error());
                }
            }
            
            if ($fb_website) {
                $insertFBweb = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) VALUES ('$get_id', 'fb_website', '$fb_website')");
                if (!$insertFBweb) {
                    die(mysql_error());
                }
            }
        }
        // update Facebook Details EnD
        if($cust_email_new=='jyotimmmec05@gmail.com')
		{
		//	echo 'de';
		}
        
        //----------------------------------------------------------------------------			
        //Send mail.................................................
		$arrayEmail['emailType']='REGISTRATION';
		$arrayEmail['toName']=$f_name;
		$arrayEmail['toEmail']=$cust_email_new;
		$arrayEmail['dogspotId']=$get_id;
		$arrayEmail['dogspotPassword']=$cust_password_new;
		$arrayEmail['authorCode']=$author_code;
		$emailReturn = sendShopEmail($arrayEmail);
		 if($cust_email_new=='jyotimmmec05@gmail.com')
		{
			echo $emailReturn['errorFlag'];
		}
		$registerFlag=$emailReturn['errorFlag'];
		$errortext3=$emailReturn['errorFlag'];
		//Send mail................................................. END
        
        
    //}
} //end verification user


if ($registerFlag == 2) {
    //....................Dogspot Team add as friend AND send wellcome scrap of new user................................
    $userTeamId   = 588; //set Dogspot Team ID and  display fullname
    $TeamInfo     = getSingleRow("f_name, userid", "users", "ID = '$userTeamId'");
    $display_name = $TeamInfo['f_name'];
    $TeamId       = $TeamInfo['userid'];
    
    
    $msg_body = '<p>Hi Welcome to  Dogspot.</p>
<p>On dogspot, you can publish blogs in dog blogs. Ask question and give  answers in Q&amp;A. Make your dog web page and it will appear in dog gallery.  In photos you can maintain your photo album.</p>
<p>  We are constantly working to improve DogSpot. Please do not hesitate to give  your suggestions and feedbacks.</p>';
    
    $insertFriend1 = mysql_query("INSERT INTO friends (userid, created_id, friend_id, status) 
									VALUES ('$TeamId', '$TeamId', '$get_id', 'A')");
    if (!$insertFriend1) {
        die(mysql_error());
    }
    
    $friends_id = mysql_insert_id();
    
    $sendScrap = mysql_query("INSERT INTO scrapbook (userid, msg_to, msg_from, msg_body, cdate) 
									VALUES ('$TeamId', '$get_id', '$TeamId', '$msg_body', NULL)");
    $newMsgid  = mysql_insert_id();
    
    if (!$sendScrap) {
        die(sql_error());
    }
    //...................Dogspot Team add as friend AND send wellcome scrap of new use End...............................
    if ($fb_email) {
        header("Location: /login.php");
    } else {
        header("Location: /reg-log/verify-email.php?userid=$get_id");
    }
    
} //End if errorFlag = 2

//if get redirected from the login page with FB ID
if ($fb_userid && $fb_token) {
    $user = json_decode(file_get_contents('https://graph.facebook.com/' . $fb_userid . '?access_token=' . $fb_token . ''));
    
    $fb_userid  = $fb_userid;
    $fb_token   = $accTokan;
    $fb_email   = $user->email;
    $fb_name    = $user->name;
    $fb_dob     = $user->birthday;
    $fb_link    = $user->link;
    $fb_website = $user->website;
    
    $name       = $user->name;
    $cust_email_new = $user->email;
}
// if get redirected from the login page with FB ID END...................................................................................
}
} // close  robotest
}
if($userid=='Guest'){
$sel_cat_items=mysql_query("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price,sc.item_discount,sc.item_totalprice FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='0' ");
}else{
$sel_cat_items=mysql_query("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status
FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id
WHERE sc.userid =  '$userid'
AND sc.cart_order_status =  'new'
AND si.stock_status =  'instock'
AND si.item_display_status !=  'delete'
AND (
sc.item_price !=  '0'
OR sc.item_id =  '1283'
) AND sc.donate_bag='0'");
}
$total_cart_items=mysql_num_rows($sel_cat_items);
?>

<!-- Begin BidVertiser code -->
<SCRIPT LANGUAGE="JavaScript1.1" SRC="http://bdv.bidvertiser.com/BidVertiser.dbm?pid=652956&bid=1709528" type="text/javascript"></SCRIPT>
<!-- End BidVertiser code -->

<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="msvalidate.01" content="0665BC887BAD14D71FDE0CFE204AE648"/>
<meta name="alexaVerifyID" content="Jsm1u5BIbORxpUFCf7kGODiQNB8"/>

<? if($sitesection!='shop' && $sitesection!='articles' && $sitesection!='add-events' && $sitesection == 'dog-business-view' ){ ?>
<meta property="og:image" content="/new/common/images/fb-logo.jpg" />
<? } ?>
<?php /*?><link href="/new/common/css/header-style.css?v=<?=time(); ?>" rel="stylesheet" type="text/css" />
<link href="/new/common/css/mega_menu.css?v=<?=time(); ?>" rel="stylesheet" type="text/css" /><?php */
//$sessionid["userid"];
//$selectuset = query_execute_row("select meta_value from users_meta where meta_key='google_userid' AND userid='".$userid."'");
//if($selectuset['meta_value']){


?>
<!-- MICROFORMATS FOR GPLUS -->
<link href="https://plus.google.com/+indogspot/" rel="publisher"/>
<? //}?>

<?php /*?><link rel="stylesheet" type="text/css" href="/ajax-autocomplete/jquery.autocomplete.css" /><?php */?>
<!----- aLL jS fILES---->
<?php /*?><script type="text/javascript" src="/new/common/js/alice.js?v=<?=time(); ?>"></script>
<script type='text/javascript' src='/ajax-autocomplete/jquery.autocomplete.js'></script><?php */?>
<?php /*?><script src="/js/shaajax2.1.js" type="text/javascript"></script><?php */?>
<?php /*?><link rel="stylesheet" type="text/css" href="/sharengain/sharengain.css" media="all"  /><?php */?>
<!----- aLL jS fILES ENDS HERE---->
<?  require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php');
// require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php');?>
<script type='text/javascript' src='/ajax-autocomplete/jquery.autocomplete.js'></script>




<!-----------------------------------------location finder starts-------------------------------------------------------->
<head>
<link rel="stylesheet" href="/new/common/css/headerpage-style4.min.css?h=8" />

<?php /*?><script>
$(window).bind("load", function(){
$("#nav_section").load("/new/common/header-category-new-home.php");
//$("#nav_section1").css('display','none');
});
</script>
<? }else{ ?>
<script>
$(window).bind("load", function(){
$("#nav_section").load("/new/common/header-category-new-home1.php");
//$("#nav_section1").css('display','none');
});
</script><?php */?>

<?php /*?><link rel="stylesheet" type="text/css" href="/ajax-autocomplete/jquery.autocomplete.css" /><?php */?>
<script type="text/javascript">
function callsharengain(){
	centerPopup('#popupContactSnG');
	loadPopup('#popupContactSnG');	
}
</script>
<script type="text/javascript">
 $(document).ready(function() {
/*	 $("#searchbox").focus(function(e) {
		$("#display").text(""); 
        $("#display").css("display","block");
    });
	$("#searchbox_div").blur(function(e) {
        $("#display").css("display","none");
    });*/
	 $("#qa-my-acc-link-new").click(function(e) {
		$("#my_account_new").toggle();
    });
	 <? if($sitesectionShop == 'response'){?>
	// callsharengain();
<? }?>

      $("#loginFormnew").validate({
        
    });
	$("#resigterFormnew").validate({
        
    });
	 $("#form-validate-create-new").validate({
        
    });



	//$('#drop-menu_width_home').mouseleave(function(){
	//$('.cat-content').hide();
	//$('.category_a').removeClass('active_Menu_hp');
	//})
	$("#forgt_pass_cancel_btn").click(function(e) {
        $(".main_div").css("display","none");
		$("#login_div").css("display","block");
    });
});
</script>
<?php /*?><script> 
$(document).ready(function(){
<?php /*?><?
if($_SESSION["banner_status"]!="no"){
?>
   $("#topbanner").slideDown("fast");
	$("#closetopbanner").click(function(e) {

        $("#topbanner").slideUp("slow");
		$("#downarrowdiv").slideDown("slow");
		$("#dogtop_margin").css("top","123px");
    });
		$("#showtopbanner").click(function(e) {

        $("#topbanner").slideDown("slow");
		$("#downarrowdiv").slideUp("slow");
		$("#dogtop_margin").css("top","159px");
    });
});
<? } ?>
</script><?php */?>
<!-- UNBXD -->

<!-- Barilliance Pixel Integration ENDS-->

<script>
$(function() {
    var button = $('#searchbox');
    var box = $('#display');
    var form = $('#search_result_div');
    button.removeAttr('href');
    button.mouseup(function(resigter) {
        box.toggle();
        button.toggleClass('active');
    });
    form.mouseup(function() { 
        return false;
    });
    $(this).mouseup(function(login) {
        if(!($(login.target).parent('#searchbox').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });
});
</script>

<script>
$(function() {
    var button = $('#qa-my-acc-link-new');
    var box = $('#my_account_new');
    var form = $('#reguser');
    button.removeAttr('href');
    button.mouseup(function(login) {
        box.toggle();
        button.toggleClass('active');
    });
    form.mouseup(function() { 
       return false;
    });
    $(this).mouseup(function(login) {
        if(!($(login.target).parent('#qa-my-acc-link-new').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });
});
</script>

<script type="text/javascript">
function show(){
    if(document.getElementById('show_pass_new').checked){
    var w=document.getElementById('cust_password_new').value;
    document.getElementById('cust_password_new').type='text';
    document.getElementById('cust_password_new').value=w;
}else{
    document.getElementById('cust_password_new').type='password';
    }
}
	function show_main_divs(divid)
	{
		if(divid=='forgot_div'){
$(".check_logbox").toggle();
		}
		$(".main_div").css("display","none");
		$("#"+divid).css("display","block");
	}
	
	function cancel_header_form(){
	$(".main_div").css("display","none");	
	$('#mask , .login-popup').fadeOut(300 , function() {
	$('#mask').remove();
	});
	}
    </script>


<!----validation for login panel start here	--->
<script type="text/javascript">
function checkpassword()
{
var pin=document.getElementById('login_email_new').value;
var password=document.getElementById('passwordnew').value;
var login='login';
ShaAjaxJquary('/new/common/checkuser1.php?email='+pin+'&password='+password+'&emailcheck='+login+'', '#login_error1', '', 'loginFormnew', 'POST', 
'#login_error1', '', 'REP');


}
</script>
<!----validation for login panel ends here	--->
<script>
function checkthis(){
var pin=document.getElementById('cust_email_new').value;
var mobile1=document.getElementById('mobile1').value;
if(pin!='' && pin!='Email*'){
var emailcheck='emailcheck';
ShaAjaxJquary('/new/common/checkuser1.php?email='+pin+'&emailcheck='+emailcheck+'&mobile1='+mobile1+'', '#customer_error1_new', '', '', 'POST', '#customer_error1_new', '', 'REP');
		 }
}

function validpassword(){
		var email =document.getElementById('RegistrationFormnew').value;
		if(email !=''){
		$("#RegistrationFormnew").css("border-color","#999");
		document.getElementById('password_error1new').style.display='none';
		 ShaAjaxJquary('/new/common/forgetpassword.php?emailid='+email+'', '#DivStateLoading', '', '', 'GET', '#DivStateLoading', 'Loading...', 'REP');
		 //document.getElementById('reset_passwordnew').style.display='none';
		}else{
		$("#RegistrationFormnew").css("border-color","#F00");
		}
}
function closed() {
	document.getElementById('DivStateLoading').innerHTML = '';
	document.getElementById('reset_passwordnew').style.display='block';
	document.getElementById('RegistrationFormnew').value='';
	document.getElementById('forgot_password_new').style.display='none';
	
}
function regsrclos() {	
	document.getElementById('new_customer_form').style.display='none';	
}
function resetpasd() {	
	document.getElementById('forgot_password_new').style.display='none';	
}
function showNewfeedbackForm() {	
	document.getElementById('loginBoxnew').style.display='none';
	document.getElementById('forgot_password_new').style.display='block';	
}
function clickclear() {
	document.getElementById('search-query-new').style.backgroundColor = "white";
}

function filevalidate(){
	if(document.getElementById('search-query-new-unbox').value=='' || document.getElementById('search-query-new-unbox').value=='Search e.g. Dog Food'){
		document.searchshopform.shopQuery.focus();		
		return false;
	}else{
		searchshopform.submit();
	 }
}
<!----- Validation for login and register ends here here---->
var countpasswd='0';
function changetype(){

	if(countpasswd=='0'){
document.getElementById("cust_password_new").type="text";
//document.getElementById("pwdtemp1").type="password";
countpasswd='1';

	}
		else{
document.getElementById("cust_password_new").type="password";
//document.getElementById("pwdtemp1").type="text";
countpasswd='0';
	}
	
}
</script>
<script>

function loginclose()
{
	//alert('few');
	$("#loginBoxnew").css("display","none");
}
function registerclose()
{
	//alert('few');
	$("#resigterBoxnew").css("display","none");
}
function customerclose()
{
	//alert('few');
	$("#customernew").css("display","none");
}
function forgotclose()
{
	//alert('few');
	
	document.getElementById("form-validate-create-new").reset();
	$("#forgot_password_new").css("display","none");
	document.getElementById('password_error1new').style.display='none';
	document.getElementById('reset_password').style.display='none';
	document.getElementById('reset_passwordnew').style.display='block';
	document.getElementById('DivStateLoading').style.display='none';
}
/*function accdivclose()
{
	//alert('few');
	$("#my_account_new").css("display","none");
}

$(document).not("#qa-my-acc-link-new").click(function(e) {
    $("#my_account_new").css("display","none");
});*/
<?php /*?>function filevalidate(){
	if(document.getElementById('search-query-new').value=='' || document.getElementById('search-query-new').value=='Search e.g. Dog Food'){
		document.searchshopform.shopQuery.focus();
		
		return false;
			}
		else{
			
		//document.getElementById('search-query-new').style.background = "white";
		searchshopform.submit();
	 }
	 searchshopform.submit();
}<?php */?>
</script>
<?php /*?><script type="text/javascript">

$(document).ready(function(){
$(".search").keyup(function() 
{
var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;
if(searchbox=='')
{}
else
{
$.ajax({
type: "POST",
url: "/new/common/getall1.php",
data: dataString,
cache: false,
success: function(html)
{
$("#display").html(html).show();
}
});
}return false; 
});
});

/*function showw(vall){
	window.location.href="http://dogspot.in/"+vall+"/";
	}*/
	
</script>
<!----------------------------------------------------old search code--------------------------------------------------------->

<script type="text/javascript">
 var aliceImageHost = '#';
        var aliceStaticHost = '#';
        var globalConfig = {};   
$().ready(function() {
		$("#search-query-new").autocomplete("/new/shop/getall.php", {
		width: 390,
		matchContains: true,
		selectFirst: false
	});
	 
	 $(".slidingDiv").hide();
        $(".show_hide").show();
 
    $('.show_hide').click(function(){
    $(".slidingDiv").slideToggle();
    });
});
</script>
<script>
function disablePopup2(){
document.getElementById('modal').style.display="none";
var currentDate = new Date(new Date().getTime() + 20 * 60 * 60 * 1000);	
document.cookie="hpop=yes; expires="+currentDate+";path=/";
	}
	</script>
    <script>
jQuery(function () {
    $("#account_btn_new").click(function () {
        var VAL = $('#cust_email_new').val();
        var email = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (!email.test(VAL)) {
			//alert('1');
            $("#error231").css("display", "block");
			return false;
        }else{
			$("#error231").css("display", "none");
			}
		var VAL1 = $('#f_name').val();
        if (VAL1.length < 2) {
			//alert('3');
            $("#error23451").css("display", "block");
			return false;
        }else{
			$("#error23451").css("display", "none");
			}
        var VAL = $('#mobile1').val();
        if (VAL.length < 10) {
			//alert('2');
            $("#error2341").css("display", "block");
			return false;
        }else{
			$("#error2341").css("display", "none");
			}
		        var VAL2 = $('#cust_password_new').val();
        if (VAL2.length < 2) {
			//alert('3');
            $("#error23461").css("display", "block");
			return false;
        }else{
			$("#error23461").css("display", "none");
			}
});
    });
</script>
<style>
[placeholder]:focus::-webkit-input-placeholder {
  color: #CCC;
}</style>
<!-------------------------------------------------------ends here------------------------------------------------------------->

<?php /* ?>
<!-- Google Analytics Content Experiment code -->
<? if ($sitesection == "HOME") {?>
<script>function utmx_section(){}function utmx(){}(function(){var
k='5485365-1',d=document,l=d.location,c=d.cookie;
if(l.search.indexOf('utm_expid='+k)>0)return;
function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
'" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
</script><script>utmx('url','A/B');</script>
<!-- End of Google Analytics Content Experiment code -->
<?php */ ?>
<? // }?>
<!-----------------------------------------------------------new analytics code------------------------------------------->
<? if($ant_page !='Response'){?>
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
 
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1552614-5', 'auto');
  ga('require', 'ec');
  <?php if($shopProductArray){ ?>
  ga('ec:addImpression', {               // Provide product details in a productFieldObject.
  'id': '<?=stripslashes($shopProductArray['id']) ?>',                   // Product ID (string).
  'name': '<?=stripslashes($shopProductArray['name']) ?>', // Product name (string).
  'category': '<?=stripslashes($shopProductArray['category']) ?>',            // Product category (string).
  'brand': '<?=stripslashes($shopProductArray['brand']) ?>',                // Product brand (string).
  'variant': '<?=stripslashes($shopProductArray['variant']) ?>',               // Product variant (string).
  'position': '<?=stripslashes($shopProductArray['position']) ?>',                    // Product position (number).
  'dimension1': '<?=stripslashes($shopProductArray['dimension1']) ?>'           // Custom dimension (string).
});
ga('ec:setAction', 'detail');

  <? } if($shopResponseArray){?>
  
  <? } ?>

<!-- Analytics code for every section starts here -->
<?php
if ($sitesection == "articles1") {
?>
ga('set', 'contentGroup1', 'Articles');
<?php
} elseif ($sitesection == "articles") {
?>
ga('set', 'contentGroup1', 'Articles');
<?php if($article_category!=''){ ?>
ga('set', 'contentGroup2', '<?=$article_category?>');
<? }?>  
<?php
} elseif ($sitesection == "puppie") {
?>
ga('set', 'contentGroup1', 'Adoption');
<?php
} elseif ($sitesection == "puppies") {
?>
ga('set', 'contentGroup1', 'Adoption');
ga('set', 'contentGroup4', '<?=$puppi_breed;?>');
<?php 
} elseif ($sitesection == "qna") {
?>
ga('set', 'contentGroup1', 'qna');
<?php
} elseif ($sitesection == "qnaview") {
?>
ga('set', 'contentGroup1', 'qna');
ga('set', 'contentGroup2', '<?=$AqnaCat[$qna_catid]?>');
<?php  
}elseif ($sitesection == "dog-breeds") {
?>
ga('set', 'contentGroup1', '<?=$ant_section?>');
<?php if($ant_category!=''){ ?>
ga('set', 'contentGroup4', '<?=$ant_category?>');
<? }?>
<?php
} elseif ($sitesection == "dog-business") {
?>
ga('set', 'contentGroup1', 'dog-business');
<?php   
} elseif ($sitesection == "dog-business-view") {
?>
ga('set', 'contentGroup1', 'dog-business');
ga('set', 'contentGroup2', '<?=$category_nameText?>');
<?php
} elseif ($sitesection == "dog-business-listing") {
?>
ga('set', 'contentGroup1', 'dog-business');
ga('set', 'contentGroup2', '<?=$category_name?>');
<?php  
} elseif ($sitesection == "dog") {
?>
ga('set', 'contentGroup1', 'dog');
<?php
    
} elseif ($sitesection == "dogview") {
?>
ga('set', 'contentGroup1', 'wagclub');
<?php if($Dogbreed!='' || $sqlbreed_gp!=''){ ?>
ga('set', 'contentGroup4', '<?=$sqldog.''.$Dogbreed.''.$sqlbreed_gp?>');
<? }?> 
<?php
} elseif ($sitesection == "dog-events") {
?>
ga('set', 'contentGroup1', 'dog-events');
<?php 
} elseif ($sitesection == "dog-events-view") {
?>
ga('set', 'contentGroup1', 'dog-events');
ga('set', 'contentGroup2', '<?=$venue?>');
<?php  
} elseif ($sitesection == "photos-album") {
?>
ga('set', 'contentGroup1', 'photos-album');
ga('set', 'contentGroup2', '<?= $album_name1 ?>'); 
<?php  
} elseif ($sitesection == "photos") {
?>
ga('set', 'contentGroup1', 'photos');
<?php
} elseif ($sitesection == "photos-view") {
?>
ga('set', 'contentGroup1', 'photos-view');
ga('set', 'contentGroup2', '<?=$rowdogID[dog_breed_name];?>');
<?php
    
} elseif ($sitesection == "show-result") {
?>
ga('set', 'contentGroup1', 'show-result');
ga('set', 'contentGroup2', '<?=$show_name; ?>');
<?php   
} elseif ($sitesection == "club") {
?>
ga('set', 'contentGroup1', 'show');
ga('set', 'contentGroup2', '<?=$club_Name;?>');
ga('set', 'contentGroup7', '<?=stripslashes($rtn_row_Data['section_title_tag'])?>');
<?php  
}elseif ($sitesection == "shop" && $ant_page !='Response') {
?>
ga('set', 'contentGroup1', 'Shop')
ga('set', 'contentGroup2', '<?=$ant_category?>');
<?php if($cat_name_bread!=''){ ?>
ga('set', 'contentGroup3', '<?=$cat_name_bread?>');
<?php if($shopProductArray['brand']!=''){?>
ga('set', 'contentGroup5', '<?=stripslashes($shopProductArray['brand']) ?>');
<? }?>
<?php }?>
<?php   
}elseif ($sitesection == "shop-cart") {
?>
ga('set', 'dimension1', '<?=$ant_section?>');
ga('set', 'dimension4', '<?=$ant_page?>');
<?php   
}elseif ($sitesection == "donation") {
?>
ga('set', 'contentGroup1', 'Donation');
<?php   
}elseif ($sitesection == "wag-tag") {
?>
ga('set', 'contentGroup1', 'Wag Tag');
<?php   
}elseif ($sitesection == "animal-activist") {
?>
ga('set', 'contentGroup1', 'Animal Activist');
<?php   
}elseif ($sitesection == "dog-names") {
?>
ga('set', 'contentGroup1', 'Dog Names');
<?php if($sex_bred!='' || $breed){?>
ga('set', 'contentGroup4', '<?=$breed.''.$sex_bred?>');
<? }?>
<?php   
}
?>
ga('require', 'displayfeatures');
ga('send', 'pageview');
<!-- Analytics code for every section ends here -->
</script>
<? }?>
<!-- ViralMint CSS -->
<style type="text/css">
	#vm-container, #vm-close{ top: 35px !important;}
</style>
<!-- ViralMint CSS -->
<?php /*?>
<!-- Share and Mozo starts here -->
<script id="dezynaScript" type="text/javascript">
      var _daq = _daq || [];
      _daq.push(['_setKey' , 'af7b9ce46856b20aeb43f813abacc3ba']);
      (function() {
        var e = document.createElement('script'); e.type = 'text/javascript'; e.async = true;
        e.src = "https://d9uqz2w57ab4i.cloudfront.net/js/api.js"
        var s = document.getElementById('dezynaScript'); s.parentNode.insertBefore(e, s);
      })();
    </script>
<!-- are and Mozo ends  here-->
<!--CEMANTIKA script date:23/04/2014-->
<script type="text/javascript">
(function() {
var d=document,
h=d.getElementsByTagName('head')[0]
s=d.createElement('script');
s.type='text/javascript';
s.src="//api.in10do.com/static/1.0.0/cemretail.js";
h.appendChild(s);
}());
</script>
<!--CEMANTIKA script close here-->
<?php */?>

<?php /*?><link href="/new/common/css/common_css.css" rel="stylesheet" type="text/css" /><?php */?>
<?php /*?><script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1423135197964769']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script><?php */?>

<!-----------------------------------------------code for pop-up------------------------------------------------------------->
 <script>
	$(document).ready(function(){
    $("#modal_trigger1").click(function(){
        $("#nnn1").addClass("tab-link current");
		$("#nnn2").removeClass("tab-link current");
		$("#tab-1").addClass("current");
        $("#tab-2").removeClass("current");
		$("#cust_email_new").focus();
		$("#login_email_new").blur();
    });
		    $("#modal_trigger2").click(function(){
			$("#nnn1").removeClass("tab-link current");
			$("#nnn2").addClass("tab-link current");
			$("#tab-1").removeClass("current");
            $("#tab-2").addClass("current");
			$("#login_email_new").focus();
			$("#cust_email_new").blur();
    });
});
</script>
<script>
$(document).ready(function(){
	
	$('ul.poptabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.poptabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})

   $(function() {
    var images = ['pop-img.jpg','pop-img1.jpg', 'pop-img2.jpg','pop-img3.jpg','pop-img4.jpg'];
    $('.imagerotate').css({'background-image': 'url(/new/images/' + images[Math.floor(Math.random() * images.length)] + ')'});
   });
  </script>
  
 
<script type="text/javascript" defer="defer" src="/shop-slider/js/jquery.tabSlideOut.js"></script>
<script>
$(function(){
		   $('.icon').hover( 
				function(){$(this).stop().animate({left:'-85px'},'slow');
			}, 
				function(){$(this).stop().animate({left:'0px'},'fast');
		   });
			
		   $('.slide-out-div').tabSlideOut({
			 tabHandle: '.handle',                              
			 pathToTabImage: '/new/pix/support.png',         
			 imageHeight: '155px',                               
			 imageWidth: '28px',                              
			 tabLocation: 'right',                               
			 speed: 400,                                        
			 action: 'click',                                   
			 topPos: '480px',                                  
			 fixedPosition: true                               
		  });	    
});
</script>

<!-------------------------------------ends here------------------------------------------------------------------------------>

<?php /*?><noscript><img height="1" width="1" alt="" rel="nofollow" style="display:none" src="http://www.facebook.com/tr?id=1423135197964769&am
p;ev=NoScript" /></noscript>
<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("d3e4404614078977d89de47809097cdf");</script><!-- end Mixpanel --><?php */?>

</head>
<body>
<!-- Google Tag Manager -->
<?php /*?><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5MCZR9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5MCZR9');</script><?php */?>
<!-- End Google Tag Manager -->
<!--<link rel="stylesheet" href="/new/common/css/head_new.css" />
<link rel="stylesheet" href="/new/common/css/header-style-home.css?a=1" />
-->

<div class="siteBanner fl" id="topbanner">
<div class="cont980">
<div class="spot_li_hp fl">

<ul>
<li><a href="/dog-breeds/">WAGpedia</a></li>
<li><a href="/dog-blog/">Articles</a></li>
<li><a href="https://play.google.com/store/apps/details?id=com.dogspot.dog&hl=en" target="_blank" style="color:#ff5a00;text-decoration: none;">Download App <img src="/new/common/images/logo-google-play.png" style="float: right; margin-left: 4px; margin-top:-2px;"></a></li>
</ul>

<?php /*?><ul>
<li><img src="/new/common/images/headImg/we-are-hiring.gif"></li>
<li style="color:#f00;border-right:none;">Diwali Sale! Flat 20% OFF use Code DEEP20,Only today</li></ul><?php */?>
<?php /*?
<ul><li style="color:#f00;border-right:none;">ID Mubarak! Today our dispatch will remain close.</li></ul>

<li class="no_border_hp"><a href="http://www.fishspot.in/">FishSpot</a></li>
<li><a href="#">CatSpot</a></li>
<li class="no_border_hp"><a href="#">BirdSpot</a></li>
</ul><?php */?>

</div>
<div class="rytTop_hp fr">
        <div class="fb_dogSpot" id="fb_like_head">  
          <div class="fb-like" data-href="http://www.facebook.com/indogspot" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
        
<div class="spot_li_hp fr">
<ul><!--<li>Call our Pet Helpline  <span itemprop="telephone">+91-9212196633</span></li>-->
<li><a href="/contactus.php">Customer Support</a></li>
<li><? if($userid!='Guest'){?><a href="/new/shop/myorders.php">Track Order</a><? }else{?><a href="#modal" id="modal_trigger2" class="bharat">Track Order</a><? }?>
</li>
<? if($userid=='Guest'){ ?>
<? if($mob_user_jyoti=='1'){ ?>
<li><a href="/reg-log/register-1.php" class="reg-window-header-mob">Signup</a></li>
<li class="no_border_hp"><a href="/login.php" class="login-window-header-mob">Login</a></li>
<? }else{ ?>
<li><a href="#modal" class="bharat" id="modal_trigger1">Signup</a></li>
<li class="no_border_hp"><a href="#modal" id="modal_trigger2" class="bharat">Login</a></li>
<? } ?>
<? }else{ ?>
<li class="no_border_hp"><a id="qa-my-acc-link-new" class="arrow" style="cursor:pointer;"><?=snippetwop(ucwords(strtolower($sessionName)),$length=17,$tail="...");?></a></li>
 <!--registered user starts--> 
  <form id="reguser" name="reguser" action="" method="POST">
<div class="logIN_box fl" style="width:150px;display:none" id="my_account_new">
<span class="index_arrow-bottom" title="down-arrow"></span>
<div class="logDetail_box fl" style="width: 153px;">
<?php /*?><span class="fr"><a class="tooltip_close" onClick="accdivclose()">[x]</a></span><?php */?>
<ul>
<li><a href="/new/shop/myorders.php" title="My Orders" rel="nofollow">My Orders</a></li>
<li><a href="/new/shop/reorder_details.php?utm_source=DogSpot-Reorder&utm_medium=Reorder&utm_campaign=DogSpot-Reorder" title="Reorder" rel="nofollow">Reorder</a></li>
<li><a href="/profile/" title="My Wishlist" rel="nofollow">My Wishlist</a></li>
<li><a href="/profile/" title="My Profile" rel="nofollow">My Profile</a></li>
<li><a href="/dogs/owner/<?=$userid ?>/" title="My Dogs" rel="nofollow">My Dogs</a></li>
<li><a href="/wag_club/add_dog.php" title="Add Your Dog">Add Your Dog</a></li>
<!--<li><a >Change Password</a></li>-->
</ul>
<div class="log_newHead"><a href="/logout.php?logoff=logoff" title="Logout">Logout</a></div>
</div>
</div>
</form>
 <!--registered user ends--> 

<? } ?>
</ul>
</div>
</div>
</div>
</div>
<div class="" id="search_bar_div" style="display:block">
<div class="siteBox_new fl">
<div class="cont980">
<div class="logo_new fl" alt="DogSpot.in - Top Online Pet Supply Store, Gurgaon, Haryana, Dog, Cat and small pet product shop" title="DogSpot.in - Top Online Pet Supply Store"><a href="https://www.dogspot.in/" >
  <div class="hp_ds_logonew"></div></a>
</div>
<div class="search_new fl srch_hp" id="searchbox_div">
<?php /*?>  <input name="shopQuery" onKeyUp="clickclear()" id="search-query-new" class="search-query-new" value="<?=$shopQuery?>" type="text" placeholder="Search e.g. Dog Food" />
 
<input type="text" class="search" id="searchbox" value="" placeholder="Search e.g. Dog Food" autocomplete="off" />
  <img src="/new/common/images/headImg/srch.png" width="41" onClick=" return filevalidate();" height="33" alt="Search" style="cursor:pointer" />
  <form id="search_result_div" name="search_result_div">
  <div id="display"  class="srchRslt_new" style="display:none; cursor:pointer"> </div>
</form><?php */?> 
<form action="/new/common/all-search.php" id="cse-search-box" onSubmit="return filevalidate();" name="searchshopform">
  <input name="q" id="search-query-new-unbox" value="<?=$shopQuery?>" type="text" placeholder="Search Category, Brand, Products"  autofocus="autofocus" unbxdattr="sq" />
  <div class="hp_search_iconnew"  onClick="return filevalidate();" unbxdattr="sq_bt"></div>
  </form>
</div>
<div class="cart_hp fr">

<div class="imgCart_hp fl"><div class="hp_carr_iconnew"></div></div>
<a href="https://www.dogspot.in/new/shop/new-cart.php" style="cursor:pointer"><div class="labelCart_hp fl">Cart </div></a>
<?php /*?><? if($total_cart_items !='0'){ ?><?php */?>
<div class="item_hp" id="cartItemCountheader"><?=$total_cart_items ?></div>
<? // } ?>
</div>

</div>
</div>

</div>

<?
/*if($userid !='Guest'){
$resultitemofcart1 = query_execute("SELECT item_id,item_qty FROM shop_cart WHERE session_id = '$session_id' AND userid='Guest'");
	while($item123=mysql_fetch_array($resultitemofcart1))
	{
		$item_id3=$item123['item_id'];
		//echo $item_id1;
		 $qty21=$item123['item_qty'];
		//echo "SELECT item_id,item_qty,cart_id FROM shop_cart WHERE userid = '$sessionuserid'";
		$useritemofcart1 = query_execute("SELECT item_id,item_qty,cart_id FROM shop_cart WHERE userid = '$userid' AND cart_order_status='new' AND item_id='$item_id3'");
		$itemop=mysql_num_rows($useritemofcart1);
		if($itemop>0){
		$item_user1=mysql_fetch_array($useritemofcart1);
		
			 $item_id22=$item_user1['item_id'];
			// echo $item_id2;
			$cart_id1=$item_user1['cart_id'];
			if($item_id3==$item_id22)
			{
				//echo 'match';
				//echo "UPDATE shop_cart SET item_qty=item_qty+'$qty1' WHERE cart_id = '$cart_id'";
				$resultinsert = query_execute("UPDATE shop_cart SET item_qty=item_qty+$qty21,item_totalprice=item_price*item_qty WHERE cart_id = '$cart_id1'");
				//echo "DELETE FROM shop_cart WHERE session_id = '$session_id' AND item_id='$item_id3' AND userid='Guset'";
				$resultdel = query_execute("DELETE FROM shop_cart WHERE session_id = '$session_id' AND item_id='$item_id3' AND userid='Guset'");
			}else
			{
				//echo "UPDATE shop_cart SET userid = '$userid' WHERE session_id = '$session_id'";
				$resultinsert = query_execute("UPDATE shop_cart SET userid = '$userid' WHERE session_id = '$session_id'");
			}
		
	}else
	{
		$resultinsert = query_execute("UPDATE shop_cart SET userid = '$userid' WHERE session_id = '$session_id' AND item_id='$item_id3'");
	}
	}
}*/?>

<?php /*?><div class="scrollHead_new" style="display:none" id="search_bar_div">
<div class="cont980">
<div class="siteBox_new fl">
<div class="fl">
<div class="logo_new fl"><a href="https://www.dogspot.in/" >
  <img src="/new/common/images/headImg/logo.jpg" width="184" height="45" alt="Dogspot" title="Dogspot"></a>
</div>
<div class="search_new fl srch_hp">
<form action="/new/common/all-search.php" id="cse-search-box" onSubmit="return filevalidate();" name="searchshopform">
  <input name="shopQuery" onKeyUp="clickclear()" id="search-query-new" class="search-query-new" value="<?=$shopQuery?>" type="text" placeholder="Search e.g. Dog Food" />
  <img src="/new/common/images/headImg/srch.png" width="41" onClick=" return filevalidate();" height="33" alt="Search" style="cursor:pointer" />
  </form>
</div>
<div class="cart_hp fr">
<div class="imgCart_hp fl"><img src="/new/common/images/cart_hp.png" width="26" height="21" alt="Cart" title="Cart"></div>
<a href="https://www.dogspot.in/new/shop/new-cart.php" style="cursor:pointer"><div class="labelCart_hp fl">Cart </div></a>
<? if($total_cart_items !='0'){ ?>
<div class="item_hp"><?=$total_cart_items ?></div>
<? } ?>
</div>

</div>
</div>


</div>
</div>

<div id="nav_section">
<div id="navContainer" align="left">

  <div class="navContainer_inside">
    <div class="navContainer_left">
      <div id="nav-wrapper">
        <div class="navigation">
          <ul id="navigation" class="fleft">
            <li id="shopnavbut" class="selected"><a onMouseOver="drop();" href="/shop/" id="shoplink_home" class="activeNav_new">
            <div class="navicon"></div>
            <span class="selected">shop
            </span></a>
              
              
            </li>

          </ul>
          
          <div class="rightNav_new navigation">
    <ul>
    <li id="head_nav_btn"><span class="bg-nav"></span><a onMouseOver="drop_head_cat('head_cat_drop','dog-brd-link','new_head_guide_arrow');" id="dog-brd-link" style="width:84px;cursor:pointer">Pet Guide 
    <span class="nav_arrowbottom" title="down-arrow" id="new_head_guide_arrow"></span></a>

    </li>
    
    <li id="head_nav_btn2"><span class="bg-nav"></span><a onMouseOver="drop_head_cat('head_cat_drop2','wag_club_link','new_head_guide_arrow2'); drop2();" id="wag_club_link" href="/wag_club/" style="width:84px;cursor:pointer">WAG CLUB
    <span class="nav_arrowbottomwag" title="down-arrow" id="new_head_guide_arrow"></span>
    
    </a><span class="bg-nav fr"></span>
    
    </li>
    <li id="head_nav_btn3">
    <a style="width:100px; cursor:pointer"  onmouseover="drop_head_cat('head_cat_drop3','pet_welfare_link','new_head_guide_arrow3'); drop2();" id="pet_welfare_link">PET WELFARE
    <span class="nav_arrowbottomwag" title="down-arrow" id="new_head_guide_arrow3"></span>
    
    </a><span class="bg-nav fr"></span>
    
    </li>
     <li id="head_nav_btn4"><a id="" href="https://www.dogspot.in/dog-events/" target="_blank"> DOG SHOWS
  
    
    </a><span class="bg-nav fr"></span>
    
    </li>
        <li id="head_nav_btn5"><a id="" href="https://www.fishspot.in/" target="_blank"> FISHSPOT
  
    

    </a><span class="bg-nav fr"></span>
    
    </li>
    
    </ul>
    </div>
        </div>
      </div>
    </div>
  </div>
  
</div>

</div><?php */?>
<?php
if($sitesection=='HOME'){
 include($DOCUMENT_ROOT . "/new/common/header-category-new6.php");}
 else{
	 if($userid=='brajendra'){
	  include($DOCUMENT_ROOT . "/new/common/header-category-new6.php");
	 }else{
	  //include($DOCUMENT_ROOT . "/new/common/header-category-new.php");
	  include($DOCUMENT_ROOT . "/new/common/header-category-new6.php");

	 }
 }
?>