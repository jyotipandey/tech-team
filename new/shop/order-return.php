<?php
require_once('../../constants.php');
	require_once($DOCUMENT_ROOT.'/session.php');
	require_once($DOCUMENT_ROOT.'/database.php');
	require_once($DOCUMENT_ROOT.'/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
	require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
	require_once($DOCUMENT_ROOT.'/arrays.php');
	if($userid=='Guest'){
		header("Location: /login.php?refUrl=/shop/myorders.php");
		ob_end_flush();
		exit(); 
	}
	$sitesection = "shop";
	$session_id = session_id();
	$ip = ipCheck(); 
	
	if($order_id && $item_id){
		$SQ_cart = query_execute_row("SELECT * FROM shop_cart WHERE cart_order_id='$order_id' AND item_id='$item_id' AND userid='$userid'");
		$cart_id = $SQ_cart['cart_id'];
		$item_id = $SQ_cart['item_id'];
		$item_qty = $SQ_cart['item_qty'];
		$item_price = $SQ_cart['item_price'];
			
		// GET Order Method
		$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id='$order_id'");
		$mode_of_payment = $SQ_shop_order['order_method'];
		$order_c_date = $SQ_shop_order['order_c_date'];
		$return_before_date = date('Y-m-d H:i:s', strtotime($order_c_date . " +30 days"));
		
		// GET Item Details
		$SQ_shop_items = query_execute_row("SELECT * FROM shop_items WHERE item_id='$item_id'");
		$item_name = $SQ_shop_items['name'];
		$item_nicename = $SQ_shop_items['nicename'];
		
		$SQ_shop_item_media = query_execute_row("SELECT * FROM shop_item_media WHERE item_id='$item_id' AND media_type='image'");
		$item_media_file = $SQ_shop_item_media['media_file'];
		$media_id = $SQ_shop_item_media['media_id'];
		
		//GET User Address 
		$SQ_shop_more_address = query_execute("SELECT * FROM shop_order_more_address WHERE userid='$userid'");	
	}	
		
	if($_POST['submit']){
		
		// GET Cart Details of the Order
		$SQ_cart = query_execute_row("SELECT * FROM shop_cart WHERE cart_order_id='$order_id' AND item_id='$item_id' AND userid='$userid'");
		$cart_id = $SQ_cart['cart_id'];
		$item_id = $SQ_cart['item_id'];
		$item_supplier_detail = $SQ_cart['item_supplier_detail'];
			
		// GET Order Method
		$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id='$order_id'");
		$mode_of_payment = $SQ_shop_order['order_method'];
			
		// INSERT Return Item values		
		$IQ_shop_returns = query_execute("INSERT INTO shop_reverse_order_returns (cart_id, item_id, order_id, return_qty, return_reason, additional_remark, return_ship_type, courier_id, tracking_id, pickup_address_id, mode_of_payment, mode_of_refund, gift_id, gift_code, item_supplier_detail_id, date) VALUES('$cart_id', '$item_id', '$order_id', '$return_qty', '$return_reason', '$additional_remark', '$ship_type', '', '', '$pickup_address', '$mode_of_payment', '$mode_refund', '$gift_id', '$gift_code', '$item_supplier_detail', null)");

		//die("Hello");
		
		header('location: myorders.php');
	}
	
	$payment_mode = array("dd"=>"Demand Draft", "DD"=>"Demand Draft", "ccpaytm"=>"PayTm Wallet", "cod"=>"Cash On Delivery","nb"=>"Net Banking","COD"=>"Cash On Delivery", "NB"=>"Net Banking");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Return Order/Item</title>
		<link type="text/css" href="css/order-return.css" rel="stylesheet" />
		<script> 
			function showvar(var1){
				// alert(var1);
	 
				if(var1=='view1')
				{
					$('#view1').css("display","block");
					$('#view11').addClass("selected");
					$('#view31').removeClass("selected");
					$('#view3').css("display","none");
				}else
				{
					$('#view31').addClass("selected");
					$('#view11').removeClass("selected");
					$('#view3').css("display","block");
					$('#view1').css("display","none");
					loadtags();
	 
				}
			}
			function closePopup()
			{
				//alert('dsfs');
				var str=document.getElementById('ordercancel').value;
				//	alert(str);
				$('#'+str).css('display','none');
				$('#mask').css('display','none');	 
			}
			</script>
		<?
			require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
			require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
		?>
		<script type="text/javascript" src="jquery/jRating.jquery.js"></script>
		<link rel="stylesheet" href="jquery/jRating.jquery.css" type="text/css" />
		<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
		<script type="text/javascript">
			var cat_nice='';
			function loadtags() { 
				//alert('233');
				cat_nice=document.getElementById('txt1').innerHTML;
				var view31=$('#view3').css("display");
				//var view3=$('#view3').css("display");
				//alert(view31);
				if(view31=='block'){
					var c=0;
					//alert('j');
					countr=document.getElementById('txt2').innerHTML;
					//alert(2);
					//alert ( $(".imgtxtcontwag:last").attr('id'));
					$(window).data('ajaxready', true).scroll(function(e) {
							if ($(window).data('ajaxready') == false) return;
		
							if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
								$('#loadMoreComments').show();
								$(window).data('ajaxready', false);
		
								if(c!=2){
									$.ajax({
											cache: false,
											dataType : "html" ,
											contentType : "application/x-www-form-urlencoded" ,
											url: "/new/shop/loadmore1.php?lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
											data: {cat_nicename:cat_nice} ,
											success: function(html) {
												if(html){		
													$("#rytPost_list").append(html);
													$('#loadMoreComments').hide();c=c+1;
												}else {
													$('#loadMoreComments').html();
												}
												$(window).data('ajaxready', true);
		
											}
										}); // ajex close
								}
								else{e12=  $(".imgtxtcontwag:last").attr('id');
									if(e12 <countr){
										$("#rytPost_list").append("<a href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore'  >See More </div></a>");
			
										c=0;}$('#loadMoreComments').hide();
								} // c condition close
							}

						});
				}
			};
		</script>
		<script>
			function get(rt){
				$('#divw'+rt).hide();
				c=0;

		
				if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
					$('#loadMoreComments').show();
					$(window).data('ajaxready', false);
					<!--
					if(c!=3){
		
						$.ajax({
								cache: false,
								dataType : "html" ,
								contentType : "application/x-www-form-urlencoded" ,
								url: "/new/shop/loadmore1.php?lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
								data: {cat_nicename:cat_nice} ,
								success: function(html) {
									if(html){		
										$("#rytPost_list").append(html);
										$('#loadMoreComments').hide();
										c=c+1; 
									}else {
										$('#loadMoreComments').html();
									}
									$(window).data('ajaxready', true);
		
								}
							}); // ajax close
					}
					else{e12=  $(".imgtxtcontwag:last").attr('id');
						if(e12!=countr){
							$("#rytPost_list").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
							c=0;}$('#loadMoreComments').hide();
		
					} // c condition close-->
				}
		
			}

		</script>

		<script>
			function success()
			{
				//alert('4');
				$('#datasSent').css("display","block");
				$('.serverResponse').css("display","block");
				var query=$('#insert').text();
				//alert(query);
				var user=query.split('@@');
				//alert(user);
				var targetDiv = $('.jDisabled').attr('id');
				var itemfd=targetDiv.split('@@');
				//alert(itemfd);
				//alert('#');
				var itemew=itemfd[0]+user[0];
				//alert(itemew);
				$('#Staritem'+itemew).attr("value","Your Rating");
				//var targetDiv1 = $('.jDisabled').parent('id');
				//var item_id=$('.login-popup').css('display', 'block').attr('id');
				//alert('#serverResponse_'+targetDiv);

				ShaAjaxJquary('/new/shop/update.php?query="'+query+'"&vendor='+targetDiv+'', '#notice', '', '', 'POST', '#notice', '...', 'REP');	

			}
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
		
					$('.basic').jRating();

					$(".exemple6").jRating({
							length:5,
							decimalLength:1,
							showRateInfo:false
						});
		
					$('a.login-window').click(function() {

							var loginBox = $(this).attr('href');
							$(loginBox).fadeIn(300);
							$('#ordercancel').val(loginBox);
							$(loginBox).css('display','block');
							$('#mask').css('display','block');

							var popMargTop = ($(loginBox).height() + 24) / 2;
							var popMargLeft = ($(loginBox).width() + 24) / 2;
							$(loginBox).css({
									'margin-top' : -popMargTop,
									'margin-left' : -popMargLeft
								});
							$('body').append('<div id="mask"></div>');
							$('#mask').fadeIn(300);
							return false;
						});
					$('a.close, #mask').live('click', function() {
							$('#mask , .login-popup').fadeOut(300 , function() {
									$('#mask').remove();
								});
							return false;
						});
				});

		</script>

		<script>
			$(document).ready(function() {
					$('a.login-window3').click(function() {
							var loginBox = $(this).attr('href');
							//alert(loginBox);
							$('#ordercancel').val(loginBox);
							$(loginBox).fadeIn(300);
							var popMargTop = ($(loginBox).height() + 24) / 2;
							var popMargLeft = ($(loginBox).width() + 20) / 2;
							$(loginBox).css({
									'margin-top' : -popMargTop,
									'margin-left' : -popMargLeft
								});
							$('body').append('<div id="mask"></div>');
							$('#mask').fadeIn(300);
							return false;
						});
					$('a.close, #mask').live('click', function() {
							$('#mask , .login-popup').fadeOut(300 , function() {
									$('#mask').remove();
								});
							return false;
						});
				});
		</script> 
		<script>
			function closethis1(items,vender){
				//alert($('#serverResponse_'+items+'@@'+vender).val());
				$('.serverResponse').css('display','none');
				//$('#serverResponse_'+items+'@@'+vender).css('display','none');	
			}
			$(document).ready(function() {
					$( "div" ).removeClass( "jDisabled" );
					$('a.login-window2').click(function() {
							var loginBox = $(this).attr('href');
							var box=loginBox.split('#');
							$("div").removeClass('jDisabled');
							$('#ordercancel').val(box[1]);
							$(loginBox).fadeIn(300);

							var popMargTop = ($(loginBox).height() + 24) / 2;
							var popMargLeft = ($(loginBox).width() + 20) / 2;
							$(loginBox).css({
									'margin-top' : -popMargTop,
									'margin-left' : -popMargLeft
								});
							$('body').append('<div id="mask"></div>');
							$('#mask').fadeIn(300);
							return false;
						});
					$('a.close, #mask').live('click', function() {
							$('#mask , .login-popup').fadeOut(300 , function() {
									$('#mask').remove();
								});
							return false;
						});
				});
		</script>

		<script type="text/javascript">

			function showhideorder(order_id)
			{
				var satus=$('#order'+order_id).css("display");
				//alert(satus);
				if(satus=='block')
				{
					$("#showhidepage"+order_id).css("background-image","url(/new/shop/img/toggle-details_1.png)");	
					//alert('#amountafter'+order_id);
					$('#amountafter'+order_id).css("display","block");
					$('#amountafterbefore'+order_id).css("display","none");
					$('#order'+order_id).css("display","none");	
				}else{
					$("#showhidepage"+order_id).css("background-image", "url(/new/shop/img/toggle-details.png)")
					$('#amountafter'+order_id).css("display","none");
					$('#amountafterbefore'+order_id).css("display","block");
					$('#order'+order_id).css("display","block");	
				}
			}

		</script> 
	</head>
	<body>
		<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
		<div class="cont980">
			<div class="order_return_cont">
				<!-- returb order -->
				<div class="order_return">

					<div class="order_return_left">Return Order</div>
					<div class="order_return_right"><?=$userid?></div>
				</div>
				<!-- return order end-->
				<!--order return inside-->
				<div class="order_return_details">

					<div class="order_return_back"><a href="myorder.php"><img src="images/back.png" /></a></div>
					<div class="order_return_order_id"><strong>Order No.</strong> <a href="myorder.php?order_id=<?=$order_id?>"><?=$order_id?></a></div>
					<div class="order_return_payment_mode"><strong>Payment mode:</strong> <?=$payment_mode[$mode_of_payment]?></div>
				</div>
				<!--order return inside end-->
				<!-- order return section-->
				<div class="order_return_section">
					<!-- left section --->
					<div class="order_return_section_left">
						<!-- product details page-->
						<div class="order_return_product">
							<div class="order_return_product_img"><a href="/<?=$item_nicename?>/"><img src="/imgthumb/150x160-<?=$item_media_file?>" /></a></div>
							<div class="order_return_product_details">
								<div class="order_return_product_name"><a href="/<?=$item_nicename?>/"><?=$item_name?></a></div>
								<div class="order_return_product_name">Qty: <?=$item_qty?></div>
								<div class="order_return_product_name">Rs: <?=number_format($item_price*$item_qty,0)?></div>
							</div>
						</div>
						<!-- product details page-->
						<!-- resion sectionn-->

						<!-- form-->
						<form name="submitForm" id="submitForm" action="order-return.php" method="POST">
							<input type="hidden" name="order_id" value="<?=$order_id?>"/>
							<input type="hidden" name="item_id" value="<?=$item_id?>"/>
							<div class="order_return_resion">
								<div class="order_return_resion_text">Why do you want to return this item?</div>
								<div class="order_return_resion_form">Reason for return *</div>
								<div>
                                	<select name="return_reason" class="order_return_resion_drop" required="">
										<option value="">Select Reason</option>
                                        <option value="WIS">Wrong Item Shipped</option>
                                        <option value="FI">I dont like the fit</option>
                                        <option value="PLS">Sizing issue - Too large</option>
                                        <option value="PNW">Product not as displayed on website</option>
                                        <option value="PSS">Sizing issue - Too small</option>
                                        <option value="ISS">Wrong size shipped</option>
                                        <option value="DNL">Did not like the product</option>
                                        <option value="PQM">Product quality issue - material</option>
                                    </select>
                                </div>
                                <div class="order_return_resion_form">Return Qty *</div>
								<div>
                                	<select name="return_qty" class="order_return_resion_drop" required="">
										<option value="">Select Return Qty</option>
										<?php for($i=1; $i<=$item_qty;$i++){?>
											<option value="<?=$i?>"><?=$i?></option>	
										<?php } ?>
                                    </select>
                                </div>
								<div class="order_return_resion_remarks">Additional remarks</div>
								<div>
									<textarea name="additional_remark" class="order_return_resion_textarea" rows="6" cols="35"  maxlength="300"></textarea>
								</div>
							</div>
							<div class="order_return_resion">
								<div class="order_return_resion_text">How do you want to return this item?</div>
								<div class="order_return_radio">
									<input type="radio" value="self" name="ship_type" id="selfShip"/>
									<label>Ship it yourself (You will be credited Rs.100 cashback)</label>
								</div>
								<div class="order_return_radio">
									<input type="radio" name="ship_type" value="courier" id="courierShip"/>
									<label>Arrange for pickup (where serviceable)</label>
								</div>
								<!-- address-->
								<div class="address_sel box">
									<?php 
										if(mysql_num_rows($SQ_shop_more_address) > 0){
											echo "<div>Arrange for pickup Address</div>";
											while($getRow = mysql_fetch_array($SQ_shop_more_address)){
									?>
											<div class="ship_add" id="addressID_<?=$getRow[more_address_id]?>" onclick="selectAddress('<?=$getRow[more_address_id]?>');">
												<ul>
													<li style="border-bottom: 1px solid #bfbfbf;padding-bottom: 4px;margin-bottom: 3px;">
														<strong><?=$getRow['address_name']?></strong>
													</li>
													<li><?=$getRow['address_address1']?>,</li>
													<li><?=$getRow['address_city']?></li>
													<li><?=$getRow['address_state']?> - <?=$getRow['address_zip']?></li>
													<li><?=$getRow['address_phone1']?></li>
												</ul>
												<div class="click_sel">Click to Select</div>
											</div>		
									<?php } } else{ ?>									
										<div>No Address Available! Please <a href="">Add New Pickup address</a></div>
									<? } ?>
                                    <input type='hidden' name='pickup_address' id='pickup_address' value=''>
								</div>
								<!-- address end-->
							</div>
							<div class="order_return_resion">
								<div class="order_return_resion_text">Choose mode of refund</div>
								<div class="order_return_radio">
								<!--<input type="radio" value="" name="nb" /><label>Cashback</label>-->
								<select name="mode_refund" required="">
									<option value="">Select Refund Mode</option>
									<? if($mode_of_payment=="nb" || $mode_of_payment=="ccpaytm"){?>
										<option value="">Online Refund</option>
									<?php } ?>
									<option value="GV">Gift Voucher</option>
								</select>	
								</div>
							</div>
							<div class="order_return_resion">
								<div class="order_return_resion_form">
									<input type="checkbox" name="agree" id="agree" value="1" required="required"/><label>I confirm that the product is unused and with original tags intact.</label></div>
								<div class="order_return_btn">
									<input class="order_return_cancelbtn" type="reset"  value="Reset" />
									<input type="submit" class="order_return_nextbtn" name="submit" id="submit" value="Next" />
								</div>
							</div>
						</form>
						<!--form end-->
						<!-- resion sectionn end-->
					</div>
					<!-- left section end --->
					<!-- right section-->
					<div class="order_return_section_right">

						<div class="order_return_valid_before">Valid for return before</div>
						<div class="order_return_valid_date"><?=$return_before_date?></div>
						<div class="order_return_valid_before">Refund amount</div>
						<div class="order_return_valid_date">Rs. <?=number_format($item_price*$item_qty,0)?></div>
						<div class="order_return_valid_before">For more information, please view <a href="#">DogSpot.in return policy</a></div>

						<div class="order_return_valid_before">Need help? Call us on</div>
						<div class="order_return_valid_date">+91-9212196633</div>
					</div>



					<!-- right section end-->
				</div>
				<!-- order return section end-->
			</div>
		</div>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$('input[type="radio"]').click(function(){
					if($(this).attr("value")=="courier"){
						$(".address_sel").show();
					}
					else{
						$(".box").hide(); 
					}
				});
			});
			
			function selectAddress(addressId){
				$("#pickup_address").val(addressId);
				$(".ship_add").css("background-color","");
				$("#addressID_"+addressId).css("background-color","yellow");
			}
			
		</script>
		<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
	</body>
</html>
