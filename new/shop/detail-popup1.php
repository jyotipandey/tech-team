<div class="what_escrow" id="show_escrow" style="margin-top:-245px;">
<h4>What is Release on Delivery?</h4><a  href="#" class="close"> <span class="close_box">close <span style="color:#aaa">[x]</span></span></a>
<ul class="escrow_detail">
<li>Release on delivery is a payment method which gives you control of your money like COD with the convenience of paying online.</li>
</ul>
<div class="how_work"> How it works : </div>
<div class="escrow_process">
<ul class="escrow_pro">
<li>
<img src="/new/shop/images/img/pro1.jpg" width="69" height="65" alt="" />
<label class="tag_lbl"><strong>Pay online on release on Delivery using Credit Card, Debit Card or Net Banking</strong></label>
</li>
<li>
<img src="/new/shop/images/img/pro2.jpg" width="69" height="65" alt="" />
<label class="tag_lbl"><strong>Your money is parked with PayUPaisa</strong><br />
(money is not transferred to the merchant)</label>
<li>
<img src="/new/shop/images/img/pro3.jpg" width="101" height="68" alt="" />
<label class="tag_lbl"><strong>You receive the product</strong><br />
you also save Rs. 50 of the COD charges</label>
<li>
<img src="/new/shop/images/img/pro4.jpg" width="69" height="65" alt="" />
<label class="tag_lbl"><strong>You request PayUPaisa to release your money to the merchant</strong></label>
<li style="width:140px;">
<img src="/new/shop/images/img/pro5.jpg" width="69" height="65" alt="" />
<label class="tag_lbl"><strong>Merchant gets Paid</strong></label>
</li>
</ul>

<div class="benefits_box">
<h3>Benefits</h3>
<ul>
<li><img src="/new/shop/images/img/ben1.jpg" width="46" height="60" alt="" />
<label> <strong>You get 3 extra days to pay</strong><br/>
You can release money to the seller after 3 days of receiving your product.
</label>
</li>
<?php /*?><li><img src="/new/shop/images/img/ben2.jpg" width="72" height="75" alt="" />
<label><strong>Extra Discount</strong><br/>
discount of  Rs. 50 or 5% whichever is lesser.
<span style="color:#6D6D6D; float:left;"><span style="color:red">*</span>Terms & Conditions Apply.</span></label>
</li><?php */?>
<li class="no_ryt_mar"><img src="/new/shop/images/img/ben3.jpg" width="54" height="57" alt="" />
<label><strong>Faster Refunds</strong><br/>
Get your money back within 2 days. COD refunds take 2 Weeks.</label>
</li>
</ul>
</div>
</div>
</div>