<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

if($userid=='Guest'){
	header("Location: /login.php?refUrl=/new/shop/myorders.php");
	ob_end_flush();
	exit(); 
}
$sitesection = "shop";
$session_id = session_id();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Order Details | Shop DogSpot</title>
<meta name="keywords" content="My Order Detail | DogSpot" />
<meta name="description" content="My Order Detail | DogSpot" />
<link href="/new/shop/css/myorders.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<style>
.reorder_blk h1{ margin-top:15px;  font-size:16px; font-weight:normal; float:left; width:100%;}
.reorder_newbtn{background: #FF6709;color: #FFF!important;display: inline!important;font-size: 14px;padding: 8px 60px; border-radius: 3px;text-transform: uppercase;    font-family: arial;cursor: pointer;text-decoration: none;border: none;}
.reorder_newbtn_blk{ text-align:right;}
</style>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<script>
$(function(){
	// coding for dispatch ready to dispathced
  $('#btnClick').click(function(){
    var val = [];
	
    $(':checkbox:checked').each(function(i){
      val[i] = $(this).val();
    });
	var x;
		for (x in val){		
		//if(val[x]!=1){
		var fb = val[x].split(/@/);
		var itemid=fb[0];
   		var qty=fb[1];
		var price=fb[2];
  // alert(itemid+'---'+price);
 ShaAjaxJquary("/new/shop/carts.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "#ttt", '', '', 'POST', '#ttt', '<img src="/images/loading-round.gif" />','REP');
setTimeout(function() {
window.location.href = '/new/shop/new-cart.php';
},600);
	}//}
	if(val!=1){
	callLoadCart();
	}else{alert("Please Select CheckBox");}
	});
});

//function callLoadCart(){
//	centerPopup();
//	loadPopup();
//	setTimeout("loadPopupBox()",1000);	
//}
//function loadPopupBox(){
//	var destDiv = ['#popupContact', '#topCartTotal', '#topItemCount', '#cartTotal', '#popupItemCount'];
//	var sourceDiv = ['.popUp710', '#cartTotal1', '#cartItemCount', '#cartTotal1', '#cartItemCount'];
//	shaajaxLoadMultiple('/new/shop/new-cart.php', 'POST', sourceDiv, destDiv);
//}
function updateCartItem(cart_id,unit1)
{
	if(unit1=='sub'){
	var cart_item_id=$('#cart_item_id'+cart_id).val();	
	var showqty=document.getElementById('showqty_'+cart_id).textContent;
	var qtyw2=Number(showqty);
	var qtyw1=qtyw2-1;
	
		if(qtyw1>='1') {
		
	document.getElementById('qty_'+cart_id).value=qtyw1;
	document.getElementById('cart_item_qty'+cart_id).value=qtyw1;
	var cart_qty=document.getElementById('cart_item_qty'+cart_id).value;
	document.getElementById('showqty_'+cart_id).innerHTML='';
	document.getElementById('showqty_'+cart_id).innerHTML=qtyw1;
	
		updatecart(cart_id,cart_item_id,cart_qty);
		}
	}else	{
		//check unit1
		var cart_item_id=document.getElementById('cart_item_id'+cart_id).value;
		//alert(cart_item_id);
		var showqty=document.getElementById('showqty_'+cart_id).textContent;
		//alert(showqty);
		var qtyw2=Number(showqty);
		var qtyw1=qtyw2+1;
		//alert('showqty_'+cart_id);
		
		//alert(qtyw1);
		if(qtyw1>='1') {
			document.getElementById('qty_'+cart_id).value=qtyw1;
		document.getElementById('cart_item_qty'+cart_id).value=qtyw1;
		document.getElementById('showqty_'+cart_id).innerHTML='';
		//alert('showqty_'+cart_id);
		document.getElementById('showqty_'+cart_id).innerHTML=qtyw1;
		var cart_qty=document.getElementById('cart_item_qty'+cart_id).value;
	//alert(qtyw1);
		
		//alert('hy');
		//alert(cart_id+' |'+cart_item_id+'|'+cart_qty);
		updatecart(cart_id,cart_item_id,cart_qty);
		}
	}
}
function updatecart(cat_id,cart_item_id,cart_item_qty){
	
	if(cart_item_qty>='1') {
	$('#divcartqty'+cat_id).hide();
	updateCart='updateCart';
	//alert(cat_id+' |'+cart_item_id+'|'+cart_item_qty);
	ShaAjaxJquary('/new/shop/new-cart.php?cat_id1='+cat_id+'&cart_item_qty1='+cart_item_qty+'&updateCart='+updateCart+'&cart_item_id1='+cart_item_id+'', '#cartUpdate'+cat_id, '', '', 'POST', '#cartUpdateitem_'+cat_id, '<img 	src="/images/indicator.gif" />', 'REP');
	
	var destDiv = ['#boxCart', '#topCartTotal1', '#topItemCount', '#cartTotal','#cartDiscount', '#cartItemCount'];
	var sourceDiv = ['#boxCart', '#cartTotal1', '#cartItemCount', '#cartTotal1','#cartDiscount', '#cartItemCount'];
	shaajaxLoadMultiple('/new/shop/new-cart.php', 'POST', sourceDiv, destDiv);	
   }
 }
</script>
<script>
function changecartbox(id)
{
	document.getElementById('showqty_'+id).style.display='none';
	document.getElementById('update_'+id).style.display='inline';
	document.getElementById('save'+id).style.display='inline';
	document.getElementById('change'+id).style.display='none';
}
function saveupdate(cart_id,cart_item_id){
	var cart_item=$('#update_'+cart_id).val();
	 $('#show_'+cart_id).val(cart_item);
	var cart_qwe=$('#qqty_'+cart_id).val();
	if(cart_qwe==1){
		var cart_item_qty=$('#show_'+cart_id).val();
	
	}else	{
		var cart_item1=$('#q'+cart_id).val();
		
		var cart_item_qty2=$('#show_'+cart_id).val();
		
		if(cart_item_qty2 > cart_item1)
		{
		
		var cart_item_qty=cart_item1;
		//document.getElementById('divcartqtyerror'+cart_id).style.display='inline';
		var t=1;
		 $('#divcartqtyerror'+cart_id).css("display","inline");
		//document.getElementById('divcartqtyerror'+cart_id).style.display='inline';
			
		}else
		{
			var cart_item_qty=$('#show_'+cart_id).val();
		}
	}
	var check_qty1=cart_item_qty.charAt(0);
    var check_qty2=cart_item_qty.charAt(1);
	var check_qty3=cart_item_qty.charAt(2);
	 if(check_qty1==0)
    {
	if(check_qty2==0 && check_qty3!='0' )
  {
	  
	   var cart_item_qty=check_qty3;
  }else if( check_qty2!=0)
  {
	  var cart_item_qty=check_qty2.concat(check_qty3);
 }
	  
  }
	
	if(cart_item_qty<='1') {
		cart_item_qty=1;
	}
	if(isNaN(cart_item_qty))
	{
		
		cart_item_qty=1;
	}
	if(cart_item_qty>='1') {
		
	updateCart='updateCart';
	ShaAjaxJquary('/new/shop/new-cart.php?cat_id1='+cart_id+'&cart_item_qty1='+cart_item_qty+'&cart_item_id1='+cart_item_id+'&updateCart='+updateCart+'', '#cartDataa3'+cart_id, '', '', 'POST', '#cartDataa'+cart_id, '<img 	src="/images/indicator.gif" />', 'REP');
	
	if(t==1)
	{
	$('#divcartqtyerror'+cart_id).css("display","inline");
	setTimeout('update_final()',4000);
	}else
	{
	setTimeout('update_final()',500);
	}
	
		}
		else
		{
			
		$('#divcartqty'+cart_id).show();	
		}
	
}
function update_final()
{
	
var destDiv = ['#boxCart', '#topCartTotal1', '#topItemCount', '#cartTotal', '#cartItemCount'];
	var sourceDiv = ['#boxCart', '#cartTotal1', '#topItemCount', '#cartTotal1', '#cartItemCount'];
	shaajaxLoadMultiple('/new/shop/new-cart.php', 'POST', sourceDiv, destDiv);	
}

function changevalue(value,cat_id)
{
	$('#show_'+cat_id).val(value);
	ShaAjaxJquary("/new/shop/qtycheck.php?cat_id="+cat_id+"&value="+value+"", "#catchange", '', '', 'GET', "#catchange", '','REP');
	
}
</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div class="cont980">
<!-- popup Box Start--> 
<div id="popupContact" style="height:430px;" class="popUp710">
<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />
</div>  
<div id="backgroundPopup" style="z-index:9999"></div>  

<!-- popup Box END-->

<div class="cont980 reorder_blk">
<h1>Order Details</h1>
<?
if($order_id==''){
	header("Location: /shop/myorders.php");
	exit();
}
$qGetMyCart=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '$order_id' ");
$qGetMyCart_count=query_execute_row("SELECT SUM(item_totalprice) as ttcart FROM shop_cart WHERE cart_order_id = '$order_id' ");

$totrecord = mysql_num_rows($qGetMyCart);
if($totrecord == 0){
	echo '<h1 style="margin-top:50px;">Sorry no item in your cart or invald order id ...</h1>';
	?>
    <a href="/new/shop/myorders.php">My Orders</a>
<?
}else{
	$qGetMyCarto=query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id' AND userid = '$userid'");
	$courier_id=$qGetMyCarto["shop_courier_id"];
	
	$qGetMyCou=query_execute_row("SELECT courier_tracking_url FROM shop_courier WHERE courier_id = '$courier_id'");
	$tracking=$qGetMyCou['courier_tracking_url'];
	?>
<div class="trackOrder">
<table cellpadding="0" cellspacing="0" style="">
<tbody>
<tr id="trackOrderHead">
<td class="tdWidth35">PRODUCT DETAILS</td>
<td>
<div class="OrderProductHead">QTY</div>
<div class="OrderProductHead">UNIT PRICE</div>
<div class="OrderProductHead">SUB TOTAL</div>
<div class="OrderProductHead">STATUS</div>
<div class="OrderProductHead" style="border-right:0px;">ADD TO CART</div>

</td>
</tr>
    <?
	while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		$item_id = $rowMyCart["item_id"];
		$cart_id=$rowMyCart["cart_id"];
		$qdata=query_execute("SELECT name, price, weight, item_parent_id,item_status,stock_status,item_display_status,nice_name FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart["item_totalprice"]+$ItemGtotal;
		$item_parent_id=$rowdat["item_parent_id"];
		// Get Tital option
		$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];
		
		// END
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		$sqlmediaco = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
$trecount=mysql_fetch_array($sqlmediaco);   
    if ($trecount['trecd'] != '0') {
		//echo "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC";
	$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
}else{
	$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY 
	position ASC");
}			$rowdatM = mysql_fetch_array($qdataM);
		
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		if(file_exists($imagepath)){
			$new_w = 100;
			$new_h = 100;
			$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
		}
		if($rowdat["item_parent_id"]!=0){
			$rownicename=query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
			$nice_name=$rownicename["nice_name"];
		}else{
			$nice_name=$rowdat["nice_name"];
		}?>
<tr>
<td class="tdWidth35 vtop pdtb" style="border-right:0px;"><div class="product_imgDiv"><img src="/shop/item-images/thumb_<?=$rowdatM["media_file"]?>" alt="<?=$rowdatM["label"]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/></div>
<div class="product_DescDiv">
<div class="trackOrderProduct_Name"><a href="/shop/<?=$nice_name?>" target="_blank"><?=$rowdat["name"];?></a></a></div>
<div class="trackOrder_itnno"><span><?=$item_id;?></span></div>
</div>
</td>
<td style=" vertical-align:top; padding-top:15px;">
<div class="OrderProductDesc"><?=$rowMyCart["item_qty"]?></div>
<div class="OrderProductDesc">₹  <?=$rowMyCart["item_price"]?></div>
<div class="OrderProductDesc">₹  <?=$rowMyCart["item_totalprice"]?></div>
      <? if($rowdat['stock_status']=='instock'){?>
    <div class="OrderProductDesc"> Instock </div><? }else{?><div class="OrderProductDesc"> Outstock </div>
	<? }
	 ?>
  <? if($rowdat['stock_status']=='instock'){?>
    <div class="OrderProductDesc">
     <input type="checkbox" name="selector[]" 
id="selector" value="<?=$item_id."@".$rowMyCart["item_qty"]."@".$rowMyCart["item_totalprice"];?>" checked="checked" />
</div>
<? }?>

</td>
</tr>
    <? } }?>
 <tr>
<td colspan="2">
<ul class="order-detail1 ">
<li>
<div>
Subtotal:</div> <div><i class="fa fa-inr"> </i> <?=number_format($qGetMyCart_count['ttcart'], 2);?></div>
</li>
<li>
<div>
Shipping Charges:</div> <div> <i class="fa fa-inr"> </i><?=number_format($qGetMyCarto['item_shipping_amount'], 2);?></div>
</li>
<li>
<div>
COD Charges:</div> <div> <i class="fa fa-inr"> </i><?=number_format($qGetMyCarto['order_shipment_amount'], 2);?></div>
</li>
<li>
</li>
<li>
</li>
<li>
<div>Total</div> <div><? $vartto=($qGetMyCart_count['ttcart']+$qGetMyCarto['item_shipping_amount']+$qGetMyCarto['order_shipment_amount']); echo number_format($vartto, 2);?></div>
</li>
</ul>
</td>
</tr>

</tbody></table> 
<div class="reorder_newbtn_blk"><input type="button" value="Reorder" class="reorder_newbtn" id="btnClick" name="btnClick"></div>
</div> 
</div>
</div>  
 <? }?>  

<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>