<ul id="nav" class="dropdown2 dropdown-vertical" style="width:183px; padding-top:1px">
<li><a href="/treats-food/" class="dir">Foods &amp; Treats</a>

                  <ul style="margin-top:-6px; margin-left:2px;">
                     <li><a href="/dry-dog-food/">Dry Dog Food </a></li>
                        <li><a href="/canned-dog-food/">Canned Dog Food </a></li>
                            <li><a href="/dog-meaty-treats/">Dog Meaty Treats </a></li>

                                                   <li><a href="/dog-dental-treats/">Dog Dental Treats </a></li>

                                                       <li><a href="/dog-biscuits/">Dog Biscuits </a></li>

                                                           <li><a href="/veg-treats/">Dog Veg. Treats </a></li>
      </ul>
                </li>

                                                                                                                                                                 

    

                <li><a href="/dog-puppy-care/" class="dir">Puppy Care</a>

                    <ul style="margin-top:-6px; margin-left:2px;">
                      <li><a href="/new-born/">New Born </a></li>

                         <li><a href="/milk-replacers/">Milk Replacer </a></li>
                  </ul>
                </li>

                <li><a  href="/collars-leashes/" class="dir">Collars & Leashes </a>

                                <ul style="margin-top:-6px; margin-left:2px;">

                                 <li><a href="/dog-collars/">Dog Collars </a></li>

                                    <li><a href="/dog-leashes/">Dog Leashes </a></li>

                                       <li><a href="/show-leashes/">Show Leashes </a></li>

                                         <li><a href="/retractable-leashes/">Retractable Leashes </a></li>

                                             <li><a href="/dog-harnesses/">Dog Harness </a></li>
                  </ul>
                </li>

                                              

                               

                

                <li><a href="/dog-toy/" class="dir">Dog Toys</a>

                                    <ul style="margin-top:-6px; margin-left:2px;">

                                        <li><a href="/kong-toys/">Kong Toys </a></li>

                                            <li><a href="/ball-toys/">Ball Toys </a></li>

                                                <li><a href="/squeaker-toy/">Squeaker Toy </a></li>

                                                     <li><a href="/interactive-dog-toys/">Interactive Dog Toys </a></li>

                                                          <li><a href="/rope-dog-toys/">Rope Dog Toys </a></li>
                                                             <li><a href="/soft-toys/">Dog Soft Toys </a></li>
                                                               <li><a href="/bone-toys/">Bone Toys  </a></li>
                                                   <li> <a href="/jw-pet-toys/">JW Pet Toys</a></li>                                                    <li><a href="/pet-brand-toys/">Pet Brand Toys  </a></li>
                                                   <li><a href="/dogit/">Dogit  </a></li>

                                                

                                </ul>

                </li>

    <li><a href="/accessories/" class="dir">Accessories </a>

                                <ul style="margin-top:-6px; margin-left:2px;">

                                                <li><a href="/collar-pendant/">Collar Pendant </a></li>

                                                <li><a href="/bandana/">Bandana </a></li>

                                                <li><a href="/muzzles/">Muzzles </a></li>

                                                <li><a href="/cutique/">Cutique </a></li>

                                                <li><a href="/utility/">Utility</a></li>
                                                <li><a href="/clothes/">Clothes </a></li>

                                                <li><a href="/beds/">Beds </a></li>
                                                <li><a  href="/dog-magazines/">Books & Magazines</a></li>
                                                <li><a  href="/cooling-dog-coat/">Cooling Dog Coats</a></li>
  </ul>

                </li>

     <li><a href="/flea-ticks/" class="dir">Flea & Ticks</a>

                               

                </li>

    <li><a href="/dog-bowls/" class="dir">Bowls & Feeders </a>

                                <ul style="margin-top:-6px; margin-left:2px;">

                                                <li><a href="/tip-bowls/">Tip Bowls </a></li>

                                                <li><a href="/non-tip-bowls/">Non Tip Bowls </a></li>

                                                <li><a href="/adjustable-bowl/">Adjustable Bowl Stands </a></li>

                                                <li><a href="/clamp-bowls/">Clamp Bowls </a></li>

                                                <li><a href="/feeders">Feeders </a></li>

                                         

                                </ul>

                </li>

      <li><a href="/dog-clean-up/"  class="dir">Cleaning</a>

                                <ul style="margin-top:-6px; margin-left:2px;">

                                                <li><a href="/odour-remover/">Odour Remover </a></li>

                                                <li><a href="/pet-hair-remover/">Pet Hair Remover </a></li>

                                                <li><a href="/waste-management/">Waste Management </a></li>

                                       

                                                <li><a href="/stain-remover/">Stain Remover </a></li>
                                                <li><a href="/diapers/">Diapers  </a></li>

                                              

                                </ul>

                </li>
                
                <li><a href="/dog-grooming/" class="dir">Grooming</a>

                                <ul style="margin-top:-275px; margin-left:2px;">

                                                <li><a href="/bath-accessories/">Bath Accessories </a></li>
                                                <li><a href="/shampoos/">Shampoos  </a>
                                                <li><a href="/conditioners/">Conditioners </a></li>
                                                <li><a href="/brushes-combs/">Brushes & Combs </a></li>

                                              

                                                <li><a href="/grooming-tools/">Grooming Tools  </a></li>
                                                <li><a href="/deodorizers/">Deodorizers  </a></li>

                                                 <li><a href="/shedding-control/">Shedding Control </a></li>
                                                 <li><a href="/ear-care/">Ear Care</a></li>

                                                   <li><a href="/eye-care/">Eye Care  </a></li>
                                                   <li><a href="/advanced-grooming/">Advanced Grooming </a></li>
                                                 <li><a href="/dry-bathing/">Dry Bathing </a></li>

                                                   <li><a href="/dental-care/">Dental care </a></li>
                  </ul>
                </li>
                                                        
                                                        
                                               <li><a href="/dog-training-behavior/"  class="dir">Training</a>

                                <ul style="margin-top:-204px; margin-left:2px;">

                                                <li><a href="/education/">Education</a></li>
                                                <li><a href="/training-products/">Training Products</a></li>
                                                <li><a href="/training-aids/">Training Aids </a></li>

                                                <li><a href="/training-pads/">Training Pads </a></li>

                                                <li><a href="/training-treats/">Training Treats </a></li>

                                                <li><a href="/training-leashes/">Training Leashes</a></li>

                                                <li><a href="/barking-control/">Barking Control  </a></li>
                                                <li><a href="/training-harness/">Training Harness </a></li>
                                                <li><a href="/training-collars">Training Collars  </a></li>

                                              

                                </ul>

                </li>         
                                                        


                          <li><a href="/crates-beds/"  class="dir">Crates &amp; Cages</a>

                                <ul style="margin-top:-59px; margin-left:2px;">
                                                <li><a href="/crates/">Crates </a></li>

                                                <li><a href="/cages/">Cages  </a></li>
                                                <li><a href="/dog-house/">Dog House</a></li>

                                </ul>

                </li>         

            <li><a  href="/cats-birds/"  class="dir">Cats, Birds & Small Animal</a>
            <ul style="margin-top:-44px; margin-left:2px;">
                                                <li><a href="/birds/">Birds </a></li>
                                                <li><a href="/cats/">Cats  </a></li>
                                                <li><a href="/small-pets/">Small Pets</a></li>

                                </ul>
            </li>
            
            <li style="position:relative;"><a  href="/fish/"  class="dir" >Fish</a>
            <ul style="margin-top:-180px; margin-left:2px;">
                                                <li><a href="/aquarium-additives/">Additives</a></li>
                                                <li><a href="/aquarium-substrate-gravel/">Substrate & Gravel</a></li>
                                                <li><a href="/aquarium-co2-system/">Co2</a></li>
                                                
                                                <li><a href="/aquarium-air-pumps/">Air Pumps</a></li>
                                                <li><a href="/aquarium-filter-media/">Filter Media</a></li>
                                                <li><a href="/aquarium-heater/">Heating</a></li>
                                                
                                                <li><a href="/fish-aquariums/">Aquarium</a></li>
                                                <li><a href="/aquarium-filter/">Filters</a></li>
                                                <li><a href="/aquarium-accessories/">Accessories</a></li>

                                </ul>
            </li>
            
            <li><a  href="/turtle/"  class="dir">Turtle</a>
            <ul style="margin-top:-34px; margin-left:2px;">
                                                <li><a href="/food/">Food </a></li>
                                                <?php /*?><li><a href="/feeding-accessories/">Feeding Accessories  </a></li><?php */?>
                                                <li><a href="/habitats/">Habitats </a></li>
                                               <?php /*?> <li><a href="/substrate/">Substrate</a></li>
                                                <li><a href="/heating-lighting/">Heating and Lighting </a></li>
                                                <li><a href="/dcor/">Dcor  </a></li>
                                                <li><a href="/health-wellness/">Health and Wellness  </a></li><?php */?>
                                             

                                </ul>
            </li>
            
            <li style="position:relative;"><a  href="/health-care/"  class="dir" >Health Care</a>
            <ul style="margin-top:-10px; margin-left:2px;">
                                                <?php /*?><li><a href="/dewormer/">Dewormer</a></li><?php */?>
                                                <li><a href="/supplements/">Supplements </a></li>
                                                

                                </ul>
            </li>
            
            
            
            <li ><a  href="/dog-shows/"  class="dir">Dog Shows</a></li>
            
              </ul>