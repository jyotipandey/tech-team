<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Shop Address</title>
<meta name="robots" content="noindex, follow">
<link href="/new/css/checkout.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<script src="/jquery/jquery-latest.js" type="text/javascript"></script>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
   
<meta http-equiv="X-UA-Compatible" content="IE=100" >
<style type="text/css">
	label.error{ color: #F00;}
	input.error{border: 1px solid #F00;}
</style>
<script type="text/javascript">
 $(document).ready(function() {
	  $("#formshop_add").validate({  
	});
});



</script>

<script language="javascript">
function checkthis(pin){
	ShaAjaxJquary('/new/shop/chechpincode.php?pin='+pin+'', '#citystate', '', '', 'POST', '#citystate', '...', 'REP');
		//setInterval('check()',100);
		}

function check()
{   
	var citystate=document.getElementById('citystate').textContent;
	if(citystate!='') {
	var a = citystate.split(",") // Delimiter is a string
    var city=a[0];
	var state=a[1];
	document.getElementById('city').value=city;
	document.getElementById('state').value=state;
	 city = city.trim();
	 state = state.trim();
	 document.getElementById('city').readOnly = false;
	document.getElementById('state').readOnly = false;
	if(city == '' && state==''){
		document.getElementById('city').readOnly = false;
	document.getElementById('state').readOnly = false;
		 
		document.getElementById("city").focus();	
		//document.getElementById("state").focus();
	
	}else{
		document.getElementById("city").readOnly = true;
		document.getElementById("state").readOnly = true;
	}
	
	}
}

</script>
<script type="text/javascript">
function count()
{
	var pin=document.getElementById('pin').value;
	var mobile=document.getElementById('mobile1').value;
	if(document.getElementById('name').value=='') {
		alert('Please enter valid Name');
		document.getElementById('name').focus();
		return false;
	}
	else if(pin=='') {
		alert('Please enter pincode.');
		document.getElementById('pin').focus();
		return false;
	}
	else if(pin.length!='6') {
			alert('Please enter valid Pincode.');
		document.getElementById('pin').focus();
		return false;
}
	
	else if(document.getElementById('city').value=='') {
		alert('Address City can not be blank');
		document.getElementById('city').focus();
		return false;
	}
	else if(document.getElementById('state').value=='') {
		alert('Address State can not be blank');
		document.getElementById('state').focus();
		return false;
	}
		
	else if(document.getElementById('address1').value=='') {
		alert('Shipping Address can not be blank');
		document.getElementById('address1').focus();
		return false;
	}
	else if(mobile=='') {
		alert('Please enter valid mobile no.');
		document.getElementById('mobile1').focus();
		return false;
	}
	else if(mobile.length!='10') {
		
		alert('Please enter valid mobile no.');
		document.getElementById('mobile1').focus();
		return false;
	}
	return true;
	
}
</script>
<script type="text/javascript">
function pincode()
{
	var pin=document.getElementById('pin').value;
	if(pin.length!='6') {
			alert('Please enter valid Pincode.');
		document.getElementById('pin').focus();
		return false;
}
return true;
}

</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div id="wrapper">


<!--header close-->
<div id="mainmidbody">
<div class="cont980">
<?php /*?><div class="centerbodycont">
<div class="centerbodycont"><?php */?>
<div id="breadcrumbs"><img class="imgmar" src="/new/pix/checkout/shipping_add.gif" alt="Shipping Address" /></div>
<!--breadcrumbs close-->
<div id="mainframe_123">
<div id="boxframe_more_123">

<div class="loginheader">Shipping Address</div>
<div style="background-color:#dcdbdb">
<form id="formshop_add" name="formshop_add" method="post" onsubmit="return count();" action="/new/shop/billingshipping.php" >
<table width="100%" border="0" cellspacing="1" cellpadding="4" class="tbldata">
    <tr>
    <td width="32%" bgcolor="#FFFFFF">Email ID</td>
    <td width="68%" bgcolor="#FFFFFF"><input name="u_email" type="text"  readonly="readonly" id="u_email" value="<?=$sessUserEmail;?>"/></td>
  </tr>
  <tr>
    <td width="32%" bgcolor="#FFFFFF">Name</td>
    <td width="68%" bgcolor="#FFFFFF"><input name="name" type="text" id="name" required="required" /></td>
  </tr>
  
  <tr>
    <td bgcolor="#f2f2f2">Pin code</td>
    <td bgcolor="#f2f2f2"><div>
    <div align="left" style="float:left">
    <input name="pin" type="text" class="required checkOut" id="pin" maxlength="6" minlength="6" required="required" autocomplete="off" onkeyup="checkthis(this.value); if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" onblur="check(); return pincode();" /></div>
    <div name='citystate'  id='citystate' align="right" style="display:inline;visibility:hidden" ></div></div>    
    </tr>
   <tr>
   
   
    <td bgcolor="#ffffff">City</td>
    <td bgcolor="#FFFFFF"><input name="city" type="text" id="city" value="<?= $city ?>" required="required"/>
      <br/></td>
    </tr>
  
  
  <tr  bgcolor="#f2f2f2">
  	 
    <td>State</td>
    <td><input name="state" type="text" id="state"  value="<?= $state ?>" required="required"/></td>
  </tr>
  <tr bgcolor="#ffffff">

    <td height="25">Country</td>
    <td><strong>India</strong> (Service available only in India)</td>
  </tr>
    		 
  <tr>
    <td bgcolor="#f2f2f2">Address</td>
    <td bgcolor="#f2f2f2"><textarea class="required checkOut" name="address1" id="address1" required="required" cols="45" rows="5" ></textarea></td>
  </tr>

  <tr bgcolor="#ffffff">
    <td>Landmark</td>
    <td><input name="address2" type="text" id="address2" required="required"/></td>
  </tr>
  <tr>
    <td bgcolor="#f2f2f2">Mobile No.</td>
    <td bgcolor="#f2f2f2"><input value="+91" readonly="readonly" style="float:left;width:30px;padding-right:3px" />&nbsp;
    <input name="mobile1" type="text" id="mobile1" style="width: 120px; padding-left:5px" maxlength="10" minlength="10"  onblur="return count();" required="required" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"/></td>
  </tr>
  <tr>
    <td bgcolor="#f2f2f2">Phone No.</td>
    <td bgcolor="#f2f2f2">
    <input name="phone1" type="text" id="phone1" style="width: 120px; padding-left:5px" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"/></td>
  </tr>
  <tr>
    <td height="30" colspan="2" align="right" bgcolor="#FFFFFF">
    <input type="hidden" id="addUpdate" name="addUpdate" value="addUpdate"  />
        <input type="submit" class="savencont_btn" value="" border="0" style=" border:none; cursor:pointer" id="submit" 
         name="submit" />
    </td>
    </tr>
</table>
</form>

</div>


</div>
<!--boxframe_more_1 close-->
</div>
<!--mainframe_123 close-->

<!--centerbodycont close-->
</div>
<br/>
<!-- centerbodycont close-->
</div>
<!--mainmidbody close-->
<div id="footer">
<div id="footernav">
<a href="#"><img class="verticalail" src="/new/pix/checkout/secure_pay.png"  />&nbsp; Secure Payment</a>
<a href="#"><img class="verticalail" src="/new/pix/checkout/delivery.png"  />&nbsp; Fast and Onlilne Delivery</a>
<a href="#"><img class="verticalail" src="/new/pix/checkout/replace_icon.png"  />&nbsp; Easy Return &amp; Replacement</a>
<a href="#"><img class="verticalail" src="/new/pix/checkout/product.png"  />&nbsp; Genuine Products</a></div>
</div>
<!--footer close-->
</div>
<!-- wrapper close-->
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?> 