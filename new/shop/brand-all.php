<?php
//require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
$sitesection = "shop";
if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/new/shop/brand-all-bootstrap.php');
  exit;
  }
$session_id = session_id();
$ant_section = 'Brand';
$ant_page = '';
$ant_category ='';
// redirect it with 301 redirect to the page with /
makeProperURL($requestedUrl, $requested);
// redirect it with 301 redirect to the page with /
$qdata=query_execute("SELECT * FROM shop_brand WHERE brand_status='1'");
$totrecord = mysql_num_rows($qdata);
// END
// Custom Variable for Google analytics
$CustomVar[2]='Category|Brand';
// Custom Variable for Google analytics
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="canonical" href="https://www.dogspot.in/brand/" />
<link rel="amphtml" href="https://www.dogspot.in/amp/brand/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.dogspot.in/brand/" />
<title> Pet Products by Brands | DogSpot.in</title>
<meta name="keywords" content=" Dog Food Brands, pet products brands, Best Dog Food Brand, Cat supplements Brands, Pet accessories Brands, Pet Food Brands In India." />
<meta name="description" content="Buy Branded Pet Products Online from DogSpot.in, India's Largest Online Pet Shop. Get complete range of pet food brands including Pedigree, Drools, Royal Canin and much more." />
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.dogspot.in/all-brands.php" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link href="/new/css/style.css?v=5" rel="stylesheet" type="text/css" />

<style>
#ShopItemBox_new{padding: 5px;float: left;text-align: center;margin: 10px 0px 15px 11px; font-family: Arial, Helvetica, sans-serif; height:100px; width:141px;  border-bottom:1px solid #ddd;  padding-bottom:10px; }
.ds_brands_head{text-transform: uppercase;font-weight: bold;font-size: 17px;font-family: Arial;color: #555;}
</style>

<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="breadcrumb">
<div class="cont980">
<div class="breadcrumb_cont"><span><a href="/">Online Pet Store</a></span>  <span>> </span>   <span class="brd_font_bold">Brands</span></div>
</div>
</div>
<div class="cont980">
<h1 class="ds_brands_head">Brands for Pet Products</h1>
<?
while($rowItem = mysql_fetch_array($qdata)){
	
	if($rowItem["brand_logo"]){
	$imagepath = $DOCUMENT_ROOT.'/shop/item-images/'.$rowItem["brand_logo"];
	if(file_exists($imagepath)){
		$imglink='/imgthumb/130x54-'.$rowItem["brand_logo"];
		$imglink1='/imgthumb/30x40-'.$rowItem["brand_logo"];
		$src = $DOCUMENT_ROOT.'/shop/item-images/'.$rowItem["brand_logo"];
	}else{
		$imglink='/imgthumb/130x54-'.$rowItem["brand_logo"];
		$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowItem["brand_logo"];
		$imglink1='/imgthumb/30x40-'.$rowItem["brand_logo"];
	}
		
		
	}else{
		$imglink='/imgthumb/130x54-no-photo.jpg';
		$src = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
	}
		$dest = $DOCUMENT_ROOT.$imglink;
		$dest1 = $DOCUMENT_ROOT.$imglink1;
	createImgThumbIfnot($src,$dest,'130','54','ratiowh');
	createImgThumbIfnot($src,$dest1,'30','40','ratiowh');
?>
<div id="ShopItemBox_new" >
<div id="ShopItemImage" style=" height:70px;"><a href="/<?=$rowItem["brand_nice_name"]?>/"><img src="<?=$imglink?>" alt="<?=$rowItem["brand_name"]?>"  border="0"  align="middle" title="<?=$rowItem["brand_name"]?>"/></a></div>
<div id="ShopItemDet"><a href="/<?=$rowItem["brand_nice_name"]?>/" title="<?=$rowItem["brand_name"]?>" alt="<?=$rowItem["brand_name"]?>"><?=$rowItem["brand_name"];?></a></div>
</div>
<? }?>
<div style="clear:both"></div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>