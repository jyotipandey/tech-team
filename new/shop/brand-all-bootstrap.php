<?php
//require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
$sitesection = "shop";

$session_id = session_id();
$ant_section = 'Brand';
$ant_page = '';
$ant_category ='';
// redirect it with 301 redirect to the page with /
makeProperURL($requestedUrl, $requested);
// redirect it with 301 redirect to the page with /
$qdata=query_execute("SELECT * FROM shop_brand WHERE (domain_id = '1' OR domain_id='2') AND brand_status='1'");
$totrecord = mysql_num_rows($qdata);
// END
// Custom Variable for Google analytics
$CustomVar[2]='Category|Brand';
// Custom Variable for Google analytics

$title="Pet Products by Brands | Dog Food Brands | Accessories | Supplements | Feeders";
$keyword="Dog Food Brands, pet products brands, Best Dog Food Brand, Cat supplements Brands, Pet accessories Brands, Pet Food Brands In India.";
$desc="Buy Branded Pet Products Online from DogSpot.in, India's Largest Online Pet Shop. Get complete range of pet food brands including Pedigree, Drools, Royal Canin and much more.";
 
    $alternate="https://www.dogspot.in/brand/";
	$canonical="http://m.dogspot.in/all-brands.php";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='Brand';
require_once($DOCUMENT_ROOT.'/new/common/header-bootstrap.php'); ?>



<style>
#ShopItemBox_new{padding: 5px;text-align: center;margin: 15px 0px;  min-height: 126px;   border-bottom:1px solid #ddd;  padding-bottom:10px; }
.ds_brands_head{text-transform: uppercase;;font-size: 22px;}
#ShopItemBox_new a{ text-decoration:none; color:#333;}
</style>




<div class="breadcrumbs">
    <div class="container">
  <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
  <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList">
      <span itemscope itemtype="http://schema.org/ListItem">
      <a href="/" itemprop="item"><span itemprop="name">Home</span>
      <meta itemprop="position" content="1" />
      </a>
      </span> 
      <span> / </span> 
      <span  itemscope itemtype="http://schema.org/ListItem">
          <span itemprop="name" class="active-bread">Brands</span>
      <meta itemprop="position" content="2" />
      </span> 
  </div>
</div>
  </div>
</div>

 <div class="container">
 <h1 class="ds_brands_head">Brands for Pet Products</h1>
  <div class="row">

<?
while($rowItem = mysql_fetch_array($qdata)){
	
	if($rowItem["brand_logo"]){
	$imagepath = $DOCUMENT_ROOT.'/shop/item-images/'.$rowItem["brand_logo"];
	if(file_exists($imagepath)){
		$imglink='/imgthumb/130x54-'.$rowItem["brand_logo"];
		$imglink1='/imgthumb/30x40-'.$rowItem["brand_logo"];
		$src = $DOCUMENT_ROOT.'/shop/item-images/'.$rowItem["brand_logo"];
	}else{
		$imglink='/imgthumb/130x54-'.$rowItem["brand_logo"];
		$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowItem["brand_logo"];
	}
		
		
	}else{
		$imglink='/imgthumb/130x54-no-photo.jpg';
		$src = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
	}
		$dest = $DOCUMENT_ROOT.$imglink;
		$dest1 = $DOCUMENT_ROOT.$imglink1;
	createImgThumbIfnot($src,$dest,'130','54','ratiowh');
	createImgThumbIfnot($src,$dest1,'30','40','ratiowh');
?><div  class="col-xs-4 col-sm-3 col-md-2" >
<div id="ShopItemBox_new">
<div id="ShopItemImage" style=" height:70px;"><a href="/<?=$rowItem["brand_nice_name"]?>/"><img src="<?=$imglink?>" alt="<?=$rowItem["brand_name"]?>"  border="0"  align="middle" title="<?=$rowItem["brand_name"]?>"/></a></div>
<div id="ShopItemDet"><a href="/<?=$rowItem["brand_nice_name"]?>/" title="<?=$rowItem["brand_name"]?>" alt="<?=$rowItem["brand_name"]?>"><?=$rowItem["brand_name"];?></a></div>
</div>
</div>
<? }?></div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>