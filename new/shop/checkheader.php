<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

if($userid=='Guest'){
	header("Location: /login.php?refUrl=/new/shop/myorders.php");
	ob_end_flush();
	exit(); 
}
$sitesection = "shop";
$session_id = session_id();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Order Details | Shop DogSpot</title>
<meta name="keywords" content="My Order Detail | DogSpot" />
<meta name="description" content="My Order Detail | DogSpot" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link href="/css/linkbutton.css" rel="stylesheet" type="text/css" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div class="cont980">
<div style="width:100%; margin:auto;">
<br />
<h1>Order Details</h1>
<?
if($order_id==''){
	header("Location: /shop/myorders.php");
	exit();
}
$qGetMyCart=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '$order_id' ");

//$qGetMyCart=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '$order_id' AND userid = '$userid' ");
$totrecord = mysql_num_rows($qGetMyCart);
if($totrecord == 0){
	echo '<h1 style="margin-top:50px;">Sorry no item in your cart or invald order id ...</h1>';
	?>
    <a href="/new/shop/myorders.php">My Orders</a>
<?
}else{
	$qGetMyCarto=query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id' AND userid = '$userid'");
	$courier_id=$qGetMyCarto["shop_courier_id"];
	
	$qGetMyCou=query_execute_row("SELECT courier_tracking_url FROM shop_courier WHERE courier_id = '$courier_id'");
	$tracking=$qGetMyCou['courier_tracking_url'];
	?>
<div style="float:left; width:400px; margin:5px;">
<p style="margin:3px;"><strong>Order ID:</strong> <?=$order_id?></p>
<p style="margin:3px;"><strong>Order Date:</strong> <?=showdate($qGetMyCarto["order_c_date"], "d M o, g:i a")?></p>
<? if($AorderStatusUser[$qGetMyCarto["order_status"]] !=''){?>
<p style="margin:3px;"><strong>Payment Status:</strong> <?=$AorderStatusUser[$qGetMyCarto["order_status"]];?></p>
<? } ?>
<? if($AdeleveryStatusDogSpot[$qGetMyCarto["delevery_status"]] !=''){?>
<p style="margin:3px;"><strong>Shipping Status:</strong> <?=$AdeleveryStatusDogSpot[$qGetMyCarto["delevery_status"]];?></p>
<? } ?>
<? if($AOrderMethod[$qGetMyCarto["order_method"]] !=''){?>
<p style="margin:3px;"><strong>Payment Method:</strong> <?=$AOrderMethod[$qGetMyCarto["order_method"]];?></p>
<? } ?>
<? if($ACourier[$qGetMyCarto["shop_courier_id"]] !=''){?>
<p style="margin:3px;"><strong>Courier Name:</strong> <?=$ACourier[$qGetMyCarto["shop_courier_id"]];?></p>
<? } ?>
<? if($qGetMyCarto["order_tracking"] !=''){?>
<p style="margin:3px;"><strong> AWB Number: </strong><a href="<?=$tracking?>"> <?=$qGetMyCarto["order_tracking"];?></a></p>
<? } ?>
</div> 
<div style="float:right; width:300px; margin:5px; text-align:right">
<p style="margin:3px;"><strong>Customer Care:</strong> +91-9212196633 <br />
  Mon- Fri (9 AM - 7 PM)</p>
<p style="margin:3px;"><a href="/contactus.php"><strong>Email US/Feedback</strong></a></p>
</div>
<? if($userid=='brajendra'){
	if($qGetMyCarto["delevery_status"]=='new' || $qGetMyCarto["delevery_status"]=='pending-dispatch'){?>
<div class="cancel_oc"><a href="#login-box" class="login-window">Cancel</a></div>
<? } }?>
 <div style="clear:both"></div> 
<div id="table">
  <ul class="header" style="width:759px;">
  <li style="width:100px;">Image</li>
      <li style="width:30px;">Qty</li>
      <li style="width:400px; text-align:left">Description</li>
      <li style="width:80px;">Price Each</li>
      <li style="width:100px;">Sub Total</li>
    </ul>
    <?
	
	while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		$item_id = $rowMyCart["item_id"];
		$cart_id=$rowMyCart["cart_id"];
		$qdata=query_execute("SELECT name, price, weight, item_parent_id,item_status,stock_status,item_display_status FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		//if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart["item_totalprice"]+$ItemGtotal;
		$item_parent_id=$rowdat["item_parent_id"];
		// Get Tital option
		$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];
		
		// END
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id'");
		$rowdatM = mysql_fetch_array($qdataM);
		
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		if(file_exists($imagepath)){
			$new_w = 100;
			$new_h = 100;
			$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
		}
		if($rowdat["item_parent_id"]!=0){
			$rownicename=query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
			$nice_name=$rownicename["nice_name"];
		}else{
			$nice_name=$rowdat["nice_name"];
		}
		
	?>
    <ul style="height:110px;">
     <li style="width:100px; text-align:center; height:102px;" class="cartsmall"><img src="/shop/item-images/thumb_<?=$rowdatM["media_file"]?>" alt="<?=$rowdatM["label"]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/></li>
      <li style="width:30px;height:102px;"><?=$rowMyCart["item_qty"]?></li>
      <li style="width:400px; text-align:left;height:102px;"><a href="/shop/<?=$nice_name?>" target="_blank"><?=$rowdat["name"];?></a><br /><span class="cartsmall">Item No: <?=$item_id;?></span> </li>
       <li style="width:80px;height:102px;" class="cartsmall">Rs. <?=$rowMyCart["item_price"]?></li>
      <li style="width:100px;height:102px;" class="cartsmall">Rs. <?=$rowMyCart["item_totalprice"]?></li>

    </ul>
    <? } ?>
    
    <div style="width:300px; float:right; margin-top:10px;">
    <div style="margin:5px;"><span>Subtotal:</span> <span style="float:right">Rs. <?=$ItemGtotal?></span></div>
    <? if($rowMyCarto["order_method"]=='cod'){ ?>
    <div style="margin:5px;"><span>COD Charges:</span><span style="float:right">Rs. <?=number_format($qGetMyCarto["order_shipment_amount"],0);?></span></div>
    <? } ?>
     <div style="margin:5px;"><span><?=shippingText($rowMyCarto["order_method"]);?>:</span><span style="float:right">Rs. <?=number_format($qGetMyCarto["item_shipping_amount"],0);?></span></div>
    
    <div style="padding:5px; font-weight:bold; border-top:solid; border-top-color:#000; border-top-width:1px;"><span>Total:</span><span style="float:right">Rs.  <?=number_format($qGetMyCarto["order_amount"],0);?></span>
    
    </div>
    <? if($qGetMyCarto["order_status"]!=0){?>
    <div style="float:right; margin-top:16px;"><a href="paynow.php?order_id=<?=$order_id?>" class="blueButton cornerRoundAll">Pay Now</a></div>
    <? }?>
    </div>
    
  </div>
 <? }?>  
 <div style="clear:both; margin-bottom:20px; border-bottom-color:#000; border-bottom-style:dotted; border-bottom-width:1px; height:20px;"></div>

<? if($qGetMyCarto["shop_courier_id"] == '1'){?>
<div>
<h2>Bluedart Status</h2>
<iframe src="<?=$aOrderTrackingURL[$rowMyCarto["shop_courier_id"]].$rowMyCarto["order_tracking"]?>" width="100%" height="440"></iframe>
</div>

<br />  
<?
		$getUsersBill = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '2' AND order_id = '$order_id'");
		$rowUsersBill = mysql_fetch_array($getUsersBill);
		$name = $rowUsersBill["address_name"];
		$address1 = $rowUsersBill["address_address1"];
		$address2 = $rowUsersBill["address_address2"];
		$address_city = $rowUsersBill["address_city"];
		$address_state = $rowUsersBill["address_state"];
		$address_country = $rowUsersBill["address_country"];
		$pin = $rowUsersBill["address_zip"];
		$mobile1 = $rowUsersBill["address_phone1"];
		$phone1 = $rowUsersBill["address_phone2"];
		$fax = $rowUsersBill["address_fax"];
		
// get details from shop_order_address table Shipping------------------------------------------
	$getUsersShip = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '1' AND order_id = '$order_id'");
	$rowUsersShip = mysql_fetch_array($getUsersShip);
	$ship_name = $rowUsersShip["address_name"];
	$ship_address1 = $rowUsersShip["address_address1"];
	$ship_address2 = $rowUsersShip["address_address2"];
	$ship_city = $rowUsersShip["address_city"];
	$ship_state = $rowUsersShip["address_state"];
	$ship_country = $rowUsersShip["address_country"];
	$ship_pin = $rowUsersShip["address_zip"];
	$ship_phone1 = $rowUsersShip["address_phone1"];
	// get details from shop_order_address table END Shipping--------------------------------------		
?>  


<div style="width:360px; float:left;">
<h1>Billing </h1>
<table width="100%">
  <tbody>

        <tr>
            <td style="width: 100px;"> Name: </td>
            <td><?=$name;?></td>
       </tr>
		<tr>
		  <td > Address: </td>
		  <td><?=$address1;?><br>
		    <?=$address2;?></td>
		  </tr>
		<tr>
            <td >City: </td>
            <td><?=$address_city;?>
            </td>
        </tr>
		<tr>
        <td>State: </td>
		<td><?=$address_state;?>
		</td>
     </tr>	
     <tr>
        <td >Pin Code: </td>
		<td><?=$pin;?></td>
    </tr>
    <tr>
        <td>Country: </td>
        <td> <?=$address_country?>
        </td>
    </tr>	
	<tr>
        <td > Phone: </td>
		<td><?=$mobile1;?></td>
   </tr>
</tbody>
</table></div>
<div style="width:300px; float:right;">
<h1>Shipping </h1>
<table width="100%" align="left">
  <tbody>
                  <tr>
                     <td><span style="width: 130px;"> Name</span>: </td>
			         <td><?=$ship_name;?></td>
                  </tr>
                  
				  <tr>
                      <td valign="top"> Address: </td>
                      <td><?=$ship_address1;?><br>
               <?=$ship_address2;?><br></td>
                 </tr>
				<tr>
                  <td>City: </td>
			      <td><?=$ship_city;?></td>
               </tr>
				<tr>
                <td>State: </td>
			    <td><?=$ship_state;?></td>
          </tr>	
		  <tr>
            <td >Pin Code: </td>
			<td><?=$ship_pin;?></td>
         </tr>
		  <tr>
              <td >Country: </td>
			  <td><?=$ship_country;?></td>
        </tr>	
		<tr>
            <td> Phone: </td>
			<td><?=$ship_phone1;?></td>
       </tr></tbody>
 </table>
</div>
<div style="clear:both"></div>
<? }?>
</div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>