<div class="main-cont1" style="width: 750px; bottom: 166px; left: 406.5px; display: block; font-family:sans-serif; font-size:13px;" >
<div style="float:right;">
	<a href="#" class="close" style="font-weight: bold;text-decoration: none;">
    <p class="btn_close" title="Close Window" alt="Close" style="background: #ddd;border-radius: 2px;padding: 5px;">X</p></a>
</div>
<div>

	
    
	
   <div style=" text-align:left;"> 
    <h2 style="color:#333; margin-top:10px; margin-bottom:5px; ">Shipping Charges</h2>
    
     <p>Free Shipping on all orders above Rs. 500*</p>
          
<h2 style="color:#333; margin-top:10px; margin-bottom:5px;"> *Terms and Conditions</h2>

<p>1. Dog food products would be charged itemized shipping charge of <strong>Rs. 75/quantity</strong></p>

<p>2. If the order value is more than or equal to Rs. 500 with no dog food and cat litter then shipping  would be <strong>FREE</strong></p>

<p>3. If the order value is less then Rs. 500 with no dog food or cat litter in the order  then shipping would be <strong>Rs. 49</strong></p>

<p>4. If  the order has dog food or cat litter + any other items then the itemized shipping will be  applicable <strong>Rs. 75/quantity</strong> of dog food and cat litter</p>
</div>
   
</div>