<?php
//ini_set("display_errors", 1);
//error_reporting(E_ALL);
ini_set("memory_limit",-1);
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once('constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-ArrayShopAttributeValue.php');
require_once($DOCUMENT_ROOT.'/banner1.php');

$sitesection = "shop";
$ecomm_pagetype = 'product';

$ant_section = 'Shop';
$ant_page = 'Product';
$google_conversion_label="hWRsCPrz2gIQvoe25QM";

$session_id = session_id();

$qdata=query_execute("SELECT item_id,domain_id, type_id,item_status, name, item_about, title, keyword, description, price, weight, stock_status, num_views, item_brand, item_parent_id,virtual_qty, selling_price, visibility, short_description,payment_mode FROM shop_items WHERE nice_name='$section[0]' AND item_display_status!='delete'");

$rowTotal = mysql_num_rows($qdata);
if($rowTotal==0){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();
}


$rowdat = mysql_fetch_array($qdata);
$domain_id=$rowdat['domain_id'];
// function to redirect according to Domain....
//updatedomain($domain_id,$section[0]);
if($domain_id=='2'){
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: http://www.fishspot.in/$section[0]/");
	}

// function to redirect according to Domain ENDS HERE....
$name1 = $rowdat["name"];
$name1=stripslashes($name1);
$title1 = $rowdat["title"];
$desc1 = $rowdat["description"];
$item_id = $rowdat["item_id"];
$type_id = $rowdat["type_id"];
$price = $rowdat["price"];
$weight = $rowdat["weight"];
$brand_id = $rowdat["item_brand"];
$stock_status = $rowdat["stock_status"];
$selling_price=$rowdat["selling_price"];
$visibility=$rowdat["visibility"];
$item_status=$rowdat["item_status"];
$virtual_qty=$rowdat["virtual_qty"];
$payment_mode=$rowdat["payment_mode"];

$ecomm_prodid = "$item_id";
	if($type_id=='configurable'){
	$selectchild=mysql_query("SELECT nice_name FROM shop_items WHERE item_parent_id='$item_id' AND stock_status='instock' AND item_display_status='active' AND visibility='visible' LIMIT 1");
	$countchild=mysql_num_rows($selectchild);
	if($countchild > '0'){
while($rowchildname=mysql_fetch_array($selectchild)){
$nice=$rowchildname['nice_name'];
}
	header("HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/".$nice."/");
	}else{
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/shop/");
	}}
$ecomm_totalvalue = number_format($price,2);
$upView=query_execute("UPDATE shop_items SET num_views = num_views+1 WHERE item_id='$item_id'");

$qCat=query_execute("SELECT category_id FROM shop_item_category WHERE item_id='$item_id' AND category_id!=0");
$rowCat = mysql_fetch_array($qCat);
$category_id = $rowCat["category_id"];
// Custom Variable for Google analytics
$CustomVar[3]='Sub-Category|'.$ArrayShopCategorys[$category_id];
// Custom Variable for Google analytics

$qParentCat=query_execute("SELECT category_parent_id, category_nicename, category_name FROM shop_category WHERE category_id='".$category_id."'"); //NKS
$rowParentCat = mysql_fetch_array($qParentCat); //NKS
$category_parent_id = $rowParentCat["category_parent_id"];
$category_nicename = $rowParentCat["category_nicename"];
$category = $rowParentCat["category_name"];
$ant_category = "$category";

// Custom Variable for Google analytics
$CustomVar[2]='Category|'.$ArrayShopCategorys[$category_parent_id];
// Custom Variable for Google analytics

$qParentCatNice=query_execute("SELECT category_nicename FROM shop_category WHERE category_id='".$category_parent_id."'"); //NKS
$rowParentCatNice = mysql_fetch_array($qParentCatNice); //NKS
$catParentNiceName = $rowParentCatNice["category_nicename"];
//echo 'id:'.$category_id.' pid:'.$category_parent_id.''.$item_id; //NKS


$qBrand=query_execute("SELECT brand_name FROM shop_brand WHERE brand_id='$brand_id'");
$rowBrand = mysql_fetch_array($qBrand);
$brandName=$rowBrand["brand_name"];
// Custom Variable for Google analytics
$CustomVar[4]='Brand|'.$brandName;
// Custom Variable for Google analytics
// Get all images
if($rowdat["item_parent_id"] == '0'){
	$sqlmedia="SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC";
}else{
	$sqlmedia="SELECT media_file, label FROM shop_item_media WHERE item_id='".$rowdat["item_parent_id"]."' ORDER BY position ASC";
}

$qdataM=query_execute("$sqlmedia");
while($rowdatM = mysql_fetch_array($qdataM)){
	$ArrayAllImg[]=$rowdatM["media_file"].';'.$rowdatM["label"];
}
$imgFLBase=explode(';',$ArrayAllImg[0]);
// END

$catParentName=$ArrayShopCategorys[$rowParentCat["category_parent_id"]]; //NKS

$catName=$ArrayShopCategorys[$rowCat["category_id"]];
$pageHone=$rowdat["name"];
$pageHone=stripslashes($pageHone);

$pageTitle=$pageHone.', '.$catName.', '.$brandName.', Online Pet Shop, Buy Dog Products, Pet Stores India | DogSpot.in';
$pageKeyword=$pageHone.', '.$catName.', '.$brandName.', Online Pet Shop, Buy Dog Products, Pet Stores, Online Pet Shop India, DogSpot.in';
//$pageDesc=$pageHone.' '.$catName.' '.$art_body.' Online Pet Shop, Buy Dog Products, Pet Stores India DogSpot.in';
$pageDesc=$rowdat["short_description"];
$pageDesc=stripslashes($pageDesc);

/*$imgURL=get_first_image($desc1, $DOCUMENT_ROOT);
$imgURLAbs = make_absolute($imgURL, 'https://www.dogspot.in');*/
$fbprice=(int)$price;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="-Type" ="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title><?=$pageTitle?></title>
<meta name="keywords" ="<?=$pageKeyword?>" />
<meta name="description" ="<? echo "$pageDesc". " "."$catName"." "." $pageHone"; ?>" />

<?
if($imgFLBase[0]){
		$imglink='/imgthumb/300x280-'.$imgFLBase[0];
		$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$imgFLBase[0];
	}else{
		$imglink='/imgthumb/300x280-no-photo.jpg';
		$src = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
	}
		$dest = $DOCUMENT_ROOT.$imglink;
		createImgThumbIfnot($src,$dest,'300','280','ratiowh');
?>
<?
$sharengainTitle=$pageTitle;
$sharengainDesc="$pageDesc". " "."$catName"." "." $pageHone";
$sharengainImage='https://www.dogspot.in/'.$imglink;
?>

    <meta property="fb:app_id" ="119973928016834" />
    <meta property="og:site_name" ="indogspot"/>
	<meta property="og:title" ="<?=$name1?>" />
    <meta property="og:description" ="<?=$pageDesc." ".$catName?>" />
    <meta property="og:type" ="product" />
    <meta property="og:url" ="https://www.dogspot.in/<?=$section[0]?>/" />
    <meta property="og:site_name" ="DogSpot Store" />
    <meta property="og:price:amount" ="<?=$price?>" />
    <meta property="og:price:currency" ="INR" />
    <meta property="og:availability" ="<?=$stock_status?>" />

<?php /*?><meta property="og:title" ="<?=$name1?>" />
    <meta property="og:type" ="dogspot:item" />
    <meta property="og:url" ="https://www.dogspot.in/<?=$section[0]?>/" />
    <meta property="og:image" ="<? if($imgFLBase[0]) { echo 'https://www.dogspot.in/shop/item-images/orignal/'.$imgFLBase[0];} else { echo 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif'; } ?>" />
    <meta property="og:description" ="<?=$pageDesc." ".$catName?>" />
    <meta property="dogspot:shop" ="https://www.dogspot.in/shop/" />

        <meta property="dogspot:price_value" ="<?=$fbprice?>" />
    <meta property="dogspot:currency_code" ="INR" />
    <meta property="dogspot:currency_symbol" ="Rs." />
    <meta property="dogspot:price" ="Rs<?=$price?> INR" /><?php */?>

<?php /*?><meta property="og:type" ="gifts_product" />
<meta property="og:url" ="/" />
<meta property="og:title" ="<?=$name1?>" />
<meta property="og:description" ="<?=$pageDesc." ".$catName?>" />
<meta property="og:image" ="<? if($imgFLBase[0]) { echo 'https://www.dogspot.in/shop/item-images/orignal/'.$imgFLBase[0];} else { echo 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif'; } ?>"/>
<meta property="gifts_product:price" ="<?=$fbprice?>" /><?php */?>

<meta name="twitter:card" ="product">
<meta name="twitter:site" ="@indogspot">
<meta name="twitter:creator" ="@creator_username">
<meta name="twitter:title" ="<?=$name1?>">
<meta name="twitter:description" ="<?=$pageDesc?>">
<meta name="twitter:image:src" ="<? if($imgFLBase[0]) { echo 'https://www.dogspot.in/'.$imglink;} else { echo 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif'; } ?>">
<meta name="twitter:data1" ="<?=$fbprice?>">
<meta name="twitter:label1" ="Price">
<meta name="twitter:data2" ="Cash on Delivery">
<meta name="twitter:label2" ="Free Shipping">
<meta name="twitter:domain" ="https://www.dogspot.in/">

<?php /*?><meta property="og:title" ="<?=$name1?>" />
<meta property="og:description" ="<?=$pageDesc." ".$catName?>" />
<meta property="og:type" ="product" />
<meta property="og:url" ="https://www.dogspot.in/<?=$section[0]?>" />
<meta property="og:site_name" ="DogSpot" />
<meta property="og:price:amount" ="<?=$fbprice?>" />
<meta property="og:brand" ="<?=$brandName?>" />
<meta property="og:price:currency" ="INR" />
<meta property="og:availability" ="<?=$stock_status?>" /><?php */?>

<?php /*?><script src="/js/shaajax2.1.js" type="text/javascript"></script>
<script type="text/javascript" src="/new/js/jquery-1.js"></script><?php */?>

<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<style type"text/css">
label.error {
	display: block;
	margin-left: 0px;
	width: auto;
}
select.error {
	border: 1px solid red;
}
.clearfix:after{clear:both;:".";display:block;font-size:0;height:0;line-height:0;visibility:hidden;}
.clearfix{display:block;zoom:1}
.jqzoom{text-decoration:none;width:770px; height:370px;}

</style>
<?php /*?><link href="/template/css/checkout.css" rel="stylesheet" type="text/css" /><?php */?>

<?php /*?><link type="text/css" rel="stylesheet" href="/new/css/tab_style.css" media="all" /><?php */?>
<?php /*?><link href="/new/css/login-style.css" rel="stylesheet" type="text/css"><?php */?>
<script type="text/javascript">
$(document).ready(function() {
$('a.login-window').click(function() {

// Getting the variable's value from a link
var loginBox = $(this).attr('href');

//Fade in the Popup and add close button
$(loginBox).fadeIn(300);

//Set the center alignment padding + border
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;

$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});

// Add the mask to body
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);

return false;
});

// When clicking on the button close or the mask layer the popup closed
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
//Default Action
	$(".tab_").hide(); //Hide all 
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_:first").show(); //Show first tab 
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_").hide(); //Hide all tab 
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + 
		$(activeTab).fadeIn(); //Fade in the active 
		return false;
	});	
});
</script>
<script type="text/javascript">
$(document).ready(function() {
//	 $(document).bind("contextmenu",function(e){
//    	return false;
//	});
	
	
	$('.jqzoom').jqzoom({
            zoomType: 'standard',
            lens:true,
            preloadImages: false,
            alwaysOn:false,
			zoomWidth: 660,
			zoomHeight: 370
    });
	
	$("#formShopItem").validate({
		  submitHandler: function (form) {
			  _gaq.push(['_trackPageview', 'https://www.dogspot.in/<?=$section[0]?>/']);
			  _gaq.push(['_trackEvent', 'Shop', 'addToCart', '<?=addslashes($pageHone)?>']);
			  var shopqty=$('#item_qty').val();
			  if(shopqty>='1') {
			  ShaAjaxJquary('/new/shop/cart-randers.php', '#cartDataa', '', 'formShopItem', 'POST', '#cartDataa', '...', 'REP');
			  callLoadCart();
			  $('#divqty').hide();
   		  }
		  else {
			  $('#divqty').show();
		  }
		 }
		  });$('#addtoItemCart').removeAttr('disabled');
});
function updatePrice(optprice, childatt){
//	$('#addtoItemCart').attr('disabled', 'true');
	if(optprice!=""){
		if(childatt==1){
		var attval=optprice.split("-");
		ShaAjaxJquary('/shop/ajax/load-attribute.php?item_id='+attval[3]+'&att_id='+attval[2]+'', '#att_2', '', '', 'GET', '#att_2', 'Loding...', 'REP');
		}
		optprice=parseFloat(optprice);
		//if(optprice!=0){
			var itembprice=$('#itembasceprice').val();
			itembprice=parseFloat(itembprice);
			if(optprice==''){
				var newprice = itembprice;
			}else if(optprice == -1){
				var newprice = itembprice;
			}else{
				var newprice = optprice;
			}
			$('#ItemPriceOnly').html(newprice);
			$('#item_price').val(newprice);
			if(attval[4]){
				$('.sp').html('Rs. '+attval[4]);
			}
		//}
	}	
}
function productcheck(p_item,p_price){
	var totalp=Number($("#pro_price").text());
	if($("#price_"+p_item).is(':checked')){
		nprice=Number(totalp) + Number(p_price);
		$("#pro_price").text(nprice);
		}else{
		nprice=Number(totalp) - Number(p_price);
		$("#pro_price").text(nprice);
		}
	}
function addWItemToCart()
{
var qty=1;	
$('input[name="price_product"]:checked').each(function() {
   fb1=this.value;
   fb=fb1.split(/@/);
   itemid=fb[0];
   price=fb[1];
  // alert(itemid+'---'+price);
   ShaAjaxJquary("/new/shop/cart-randers.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "", '', '', 'POST', '#ttt', '<img src="/images/loading-round.gif" />','REP');
});

setTimeout("callLoadCart()",500);

//ShaAjaxJquary("/new/shop/cart.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "#ttt", '', '', 'POST', '#ttt', '<img src="/images/loading-round.gif" />','REP');
//ShaAjaxJquary("/new/shop/cart.php?item_id="+itemid+"&item_qty="+qty+"&item_price="+price+"", "", '', '', 'POST', 'loading', '<img src="/images/loading-round.gif" />','REP');
//$("#topItemCount").load("/new/shop/cart.php #cartItemCount");
//$("#topCartTotal").load("/new/shop/cart.php #cartTotal");
	
}


function callLoadCart(){
	centerPopup();
	loadPopup();
	setTimeout("loadPopupBox()",1000);	
}
function loadPopupBox(){
	var destDiv = ['#popupContact', '#topCartTotal', '#topItemCount', '#cartTotal', '#popupItemCount'];
	var sourceDiv = ['.popUp710', '#cartTotal1', '#cartItemCount', '#cartTotal1', '#cartItemCount'];
	shaajaxLoadMultiple('/new/shop/cart-randers.php', 'POST', sourceDiv, destDiv);
}


function validateForm()
 {
	
 		var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 		if(document.getElementById('notify_email').value.length<1)
 		{		
 			document.getElementById('invalidformat1').style.display='block';
			document.getElementById('invalidformat').style.display='none';
 			return false;
 		}
 		else if(!regex.test(document.getElementById('notify_email').value))
 		{
 			alert("Invalid email address format");
 			return false;
 		}
		if(document.getElementById('notify_mobile').value.length<1)
 		{
 			document.getElementById('invalidformat').style.display='block';
			document.getElementById('invalidformat1').style.display='none';
 			return false;
 		}
 		document.getElementById('invalidformat1').style.display='none';
		document.getElementById('invalidformat').style.display='none';
 		notifymesagar();
 }

function notifymesagar() {
	
	var emailid=document.getElementById('notify_email').value;
	
	var phoneno=document.getElementById('notify_mobile').value;
	var itemid=document.getElementById('notify_item').value;
	ShaAjaxJquary("/new/shop/notifyme.php?item_id="+itemid+"&emaiid="+emailid+"&phnno="+phoneno+"", "#notifyme1", '', '', 'GET', "#notifyme1", '<img 	src="/images/indicator.gif" />','REP');

document.getElementById('notime').style.display='none';	
	
}
// function to add item in wishlist table.................
function check(wishlist_item) {
	
	ShaAjaxJquary("/new/shop/wishlist/wishlist_new.php?item_id="+wishlist_item+"", "#wishlist12", '', '', 'GET', "#wishlist12", '<img 	src="/images/indicator.gif" />','REP');
document.getElementById('frstwish').style.display='block';	
document.getElementById('mainwish').style.display='none';	

}


//  for quick add to cart Sagar starts here......................

 <?php /*?>function addWItemToCart(itmeid)
  {
 alert(itmeid);

 ShaAjaxJquary("/new/shop/quickadd.php?qna_id="+itmeid+"", "#cartDataa", '', 'formquickadd', 'POST', '', '<img src="/images/indicator.gif" />','REP');
 //ShaAjaxJquary('/new/shop/quickadd.php', '#cartDataa', '', 'formquickadd', 'POST', '#cartDataa', '...', 'REP');
	
 	callLoadCart1(itmeid);
	}
	
function callLoadCart1(){
	centerPopup();
	loadPopup();
	setTimeout("loadPopupBox1()",1000);	
}
function loadPopupBox1(){
	var destDiv = ['#popupContact', '#topCartTotal', '#topItemCount', '#cartTotal', '#popupItemCount'];
	var sourceDiv = ['.popUp710', '#cartTotal', '#topItemCount', '#cartTotal', '#cartItemCount'];
	shaajaxLoadMultiple('/new/shop/quickadd.php', 'POST', sourceDiv, destDiv);
}<?php */?>
// Ends here for quick add to cart......................


</script>

<script>
function changecartbox(id)
{
	
	document.getElementById('showqty_'+id).style.display='none';
	document.getElementById('update_'+id).style.display='inline';
	document.getElementById('save'+id).style.display='inline';
	document.getElementById('change'+id).style.display='none';
}
function saveupdate(cart_id,cart_item_id){
	var cart_item=$('#update_'+cart_id).val();
	 $('#show_'+cart_id).val(cart_item);
	var cart_qwe=$('#qqty_'+cart_id).val();
	if(cart_qwe==1){
		var cart_item_qty=$('#show_'+cart_id).val();
	
	}else	{
		var cart_item1=$('#q'+cart_id).val();
		
		var cart_item_qty2=$('#show_'+cart_id).val();
		
		if(cart_item_qty2 > cart_item1)
		{
		
		var cart_item_qty=cart_item1;
		//document.getElementById('divcartqtyerror'+cart_id).style.display='inline';
		var t=1;
		 $('#divcartqtyerror'+cart_id).css("display","inline");
		//document.getElementById('divcartqtyerror'+cart_id).style.display='inline';
			
		}else
		{
			var cart_item_qty=$('#show_'+cart_id).val();
		}
	}
	
   
	
	var check_qty1=cart_item_qty.charAt(0);
    var check_qty2=cart_item_qty.charAt(1);
	var check_qty3=cart_item_qty.charAt(2);
	 if(check_qty1==0)
    {
	if(check_qty2==0 && check_qty3!='0' )
  {
	  
	   var cart_item_qty=check_qty3;
  }else if( check_qty2!=0)
  {
	  var cart_item_qty=check_qty2.concat(check_qty3);
 }
	  
  }
	
	if(cart_item_qty<='1') {
		cart_item_qty=1;
	}
	if(isNaN(cart_item_qty))
	{
		
		cart_item_qty=1;
	}
	if(cart_item_qty>='1') {
	
	updateCart='updateCart';
	ShaAjaxJquary('/new/shop/cart-randers.php?cat_id1='+cart_id+'&cart_item_qty1='+cart_item_qty+'&cart_item_id1='+cart_item_id+'&updateCart='+updateCart+'', '#cartUpdate23', '', '', 'POST', '#cartUpdateitem_'+cart_id, '<img 	src="/images/indicator.gif" />', 'REP');
	if(t==1)
	{
	$('#divcartqtyerror'+cart_id).css("display","inline");
	setTimeout('update_final()',4000);
	}else
	{
	setTimeout('update_final()',500);
	}
	
		}
		else
		{
			
		$('#divcartqty'+cart_id).show();	
		}
	
}
function update_final()
{
	
var destDiv = ['#boxCart', '#topCartTotal1', '#topItemCount', '#cartTotal','#cartDiscount', '#cartItemCount'];
	var sourceDiv = ['#boxCart', '#cartTotal1', '#cartItemCount', '#cartTotal1','#cartDiscount', '#cartItemCount'];
	shaajaxLoadMultiple('/new/shop/cart-randers.php', 'POST', sourceDiv, destDiv);	
}

function changevalue(value,cat_id)
{
	$('#show_'+cat_id).val(value);
	ShaAjaxJquary("/new/shop/qtycheck.php?cat_id="+cat_id+"&value="+value+"", "#catchange", '', '', 'GET', "#catchange", '','REP');
	
}
</script>

<script type="text/JavaScript">
function valid(f) {
	
	f.value = f.value.replace(/[a-zA-Z\/!@#$%^&*()-+.:;,'"\><\s]/ig,'');
} 
</script>
<div id="fb-root"></div>
<?php /*?><script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> <?php */?> 
    
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<!-- end of green strip -->
    
     
               <!-- left navigation -->
              
        	<!-- left navigation -->
                <!-- left container -->
                
        	<!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980">
                
                    <div class="fl">
                        <p align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666;"><a href="/">Home</a> » <a href="/shop/">Shop</a> » <a href="/<?=$catParentNiceName?>/"><?=$ArrayShopCategorys[$category_parent_id]?></a> » <a href="/<?=$category_nicename;?>/"><?=$ArrayShopCategorys[$category_id]?></a></p>
                     </div>

               <div class="fr" style="width:180px">
                        <p align="left" style="width:70px; float:left">                                           
 <a href="https://twitter.com/indogspot" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @indogspot</a>
<?php /*?><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script><?php */?>
<script type="text/javascript">var switchTo5x=false;</script>
<?php /*?><script>
$(document).ready( function( ) {
$.getScript('http://w.sharethis.com/button/buttons.js');
$.getScript('http://platform.twitter.com/widgets.js');
});
</script><?php */?>

                    
   <div class="fb-follow" data-href="https://www.facebook.com/indogspot" data-layout="button_count" data-show-faces="false" data-width="150" style="float:left"></div>                 
                       </p>
                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->
      <!-- main  --->
      <div class="cont980" itemscope itemtype="http://schema.org/Product">
           
                    
                    	<div class="shopimgholdSlide">
                        <p> <a href="/shop/item-images/orignal/<?=$imgFLBase[0]?>" class="jqzoom slide " rel='gal1'   title="<?=$pageHone?>" style="width:300px; height:280px; display:block;">
  <img itemprop="image" src="<?=$imglink;?>"  border="0" align="middle" alt="<?=$pageHone?>" title="<?=$pageHone?>" height="280" width="300"/></a></p>
                         <div class="vs10 cb"></div>
                         
 <p>                        
<? 
if($ArrayAllImg){
foreach($ArrayAllImg as $imgFileLabel){
	$imgFL=explode(';',$imgFileLabel);
	if($imgFL[0]){
		$imglink='/imgthumb/64x64-'.$imgFL[0];
		$imglinkm='/imgthumb/300x280-'.$imgFL[0];
		
		$src=$DOCUMENT_ROOT.'/shop/item-images/orignal/'.$imgFL[0];
	}else{
		$imglink='/imgthumb/64x64-no-photo.jpg';
		$imglinkm='/imgthumb/300x280-no-photo.jpg';
		
		$src=$DOCUMENT_ROOT.'/imgthumb/no-photo.jpg';
	}
		$dest = $DOCUMENT_ROOT.$imglink;
		$destm = $DOCUMENT_ROOT.$imglinkm;
		createImgThumbIfnot($src,$dest,'64','64','ratiowh');
		createImgThumbIfnot($src,$destm,'300','280','ratiowh');
?>
<a href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: '<?=$imglinkm?>',largeimage: '/shop/item-images/orignal/<?=$imgFL[0]?>'}">
<img src="<?=$imglink?>" alt="<?=$pageHone?>" title="<?=$pageHone?>" class="mr5" border="0" width="64" height="59" />
<img src="/shop/item-images/orignal/<?=$imgFL[0]?>" width="1" height="1" alt="<?=$pageHone?>" title="<?=$pageHone?>" border="0" />
</a>                    
<? }}?>
                        </p>
                        
                        
                    
                    </div>
                    <div class="fl">
                    <!-- text hold -->
                    <div class="shoptextholdSlide">
                    	<h1 itemprop="name"><?=stripslashes($pageHone);?></h1>
                        <div class="vs5"></div>
                       <meta  itemprop="url" content="https://www.dogspot.in/<?=$section[0]?>/" />
                        <p itemprop="description"><?=snippetwop(stripslashes($pageDesc),$length=140,$tail="...")?></p>
                        <div class="vs5"></div>
                        <!-- left cont -->
                        	<div class="cont210" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <p><b>Rs.</b> <b itemprop="price" id="ItemPriceOnly"><?=number_format($price)?></b><br /><span><del class="sp"><? if($selling_price>$price){echo 'Rs. '.number_format($selling_price); }?></del></span><br /><? if($selling_price>$price){$sa=$selling_price-$price; echo '<span itemprop="price"  style="color:#F00; font-size:14px">Today\'s savings: '.number_format(($sa/$selling_price)*100).'% </span><br> Offer valid till stocks last.<br />';}?>(Prices are inclusive of all taxes)</p>
                            <meta itemprop="priceCurrency" content="INR"/>
                            
                            </div><!-- left cont -->
                        <!-- right cont -->
                            <div class="cont195">
                                <div class="imgtxtcont" style="margin:0; padding:0; border:0;">
                                  <div class="txthold">
                                    <h2 class="stock"><?=$AShopStock[$rowdat["stock_status"]]?></h2>
                                    <? if($sessionLevel == 2){?><a href="/shop/adminshop/add-item.php?action=edit&item_id=<?=$item_id?>">Edit</a><? }?>
                                    <p>Dispatched in 2-3 business days. </p>
                                    <p><a href="#login-box" class="viewall login-window"><strong>View Details</strong></a></p>
                                    </div>
                                    <div class="cb"></div>
                                </div>
                            </div>
                            <!-- right cont -->
                            <div class="cb"></div>
                            
                            <div class="vs5"></div>
                    
                            <div class="dashbor" style="width:440px; height:112px;">
                            	<form id="formShopItem" name="formShopItem" method="post" action="/new/shop/new-cart.php">
                                <? if($stock_status=='instock'){?>
                                <div class="fl">
                            		<p align="left"><b>Shop now!</b></p>
                                </div>
                                <div class="fr">
                            		<p align="right"><span style="float:left;"><font size='2'>Cash On Delivery: <? if($payment_mode!='cod'){?><font color="#FF0000">Not Available</font><? }else{?><font color="#33CC33">Available<? }?>
</font></font>
     </span></p>
                                </div>
                                <div class="cb"></div>
                                <?php /*?><div class="vs10"></div><?php */?>
                                
                                
                                <? }
if($type_id=='simple'){
	$qAtt=query_execute("SELECT attribute_set_id, attribute_value_id FROM shop_item_attribute WHERE item_id = '$item_id'");
		while($rowAtt = mysql_fetch_array($qAtt)){
			$attribute_set_id=$rowAtt["attribute_set_id"];
			$attribute_value_id=$rowAtt["attribute_value_id"];
			if($attribute_set_id){
				if($Ashop_attribute_set[$attribute_set_id]!=''){
				echo $Ashop_attribute_set[$attribute_set_id].': '.$Ashop_attribute_value[$attribute_value_id].'<br>';
				}
			}
		}
}else{
	$qChild=query_execute("SELECT item_id, price, selling_price FROM shop_items WHERE item_parent_id='$item_id' AND stock_status='instock'");
	while($rowChild = mysql_fetch_array($qChild)){
			$Childitem_id=$rowChild["item_id"];
			$Childprice=$rowChild["price"];
			if($rowChild["selling_price"]>$Childprice){
				$Childselling_price=number_format($rowChild["selling_price"]);
			}		
			
	
	$qAtt=query_execute("SELECT DISTINCT i.attribute_set_id, i.attribute_value_id FROM shop_item_attribute as i WHERE i.item_id='$Childitem_id' AND i.attribute_set_id != 0 ORDER BY i.attribute_set_id ASC");
		while($rowAtt = mysql_fetch_array($qAtt)){
			
			$attribute_set_id = $rowAtt["attribute_set_id"];
			$attribute_value_id = $rowAtt["attribute_value_id"];
			
			if($attribute_set_id != 0){
				$qAttOrder=query_execute("SELECT attribute_value_order FROM shop_attribute_value WHERE attribute_value_id='$attribute_value_id'");
				$rowAtOrdert = mysql_fetch_array($qAttOrder);
				$attribute_value_order = $rowAtOrdert["attribute_value_order"];
				$Aattribut[] = "$attribute_set_id$attribute_value_order"."-$attribute_set_id-$attribute_value_id-$Childprice-$Childselling_price";
			}			
		}
	}
	if($Aattribut){
	$Aattribut = array_unique($Aattribut);
	asort($Aattribut);
//	print_r($Aattribut);
	$keyold=0;
	foreach($Aattribut as $aatt){
		$attkey=explode('-', $aatt);
		if($attkey[1]==$keyold){		
			echo '<option value="'.$attkey[3].'-'.$attkey[1].'-'.$attkey[2].'-'.$item_id.'-'.$attkey[4].'">'.$Ashop_attribute_value[$attkey[2]].''; if($attkey[3]){echo' Rs '.number_format($attkey[3]).'';} echo'</option>';
		}else{
			if($selstart==1){ echo'</select></div>';}
			?>
            <div class="selectbox" id="att_<?=$attkey[1]?>">
            <? if($stock_status=='instock'){
				if($Ashop_attribute_set[$attkey[1]]!=''){
			echo '<b>Choose a '.$Ashop_attribute_set[$attkey[1]].':</b> <br>';
				}
			?>
            
            <select name="ItemOption[<?=$attkey[1]?>]" id="select<?=$attkey[1]?>" onchange="Javascript:updatePrice($(this).val(), '<?=$attkey[1]?>');" title="" class="required"<? if($keyold!=0){echo'disabled="disabled"';}?>>
            <option value="">Select</option>
            <?
			echo '<option value="'.$attkey[3].'-'.$attkey[1].'-'.$attkey[2].'-'.$item_id.'-'.$attkey[4].'">'.$Ashop_attribute_value[$attkey[2]]; if($attkey[3]){echo ' Rs '.number_format($attkey[3]).'';} echo '</option>';		
		}
		$selstart=1;
		$keyold=$attkey[1];
	}}

	?></select> <? //} ?>
    
    </div><?
	}
}
?>

<div>
</div>
<div id="ShopQtyBox">
<input name="itembasceprice" type="hidden" id="itembasceprice" value="<?=$price?>" />
<input name="item_id" type="hidden" id="item_id" value="<?=$item_id;?>" />
<input name="item_price" type="hidden" id="item_price" value="<?=$price?>" />
<input name="type_id" type="hidden" id="type_id" value="<?=$type_id?>" />

</div>

     <div id="catchange"></div>                             
                                
                                
                                <? if($stock_status=='instock'){?>
                                <div class="selectbox"><span>Qty</span> <br  />
                                <? if($item_status=='soft') { ?>
                                <input name="item_qty" type="text" class="ShopQty required" maxlength="3" onkeyup="javascript:valid(this);" style="text-align:center; width:20px;" id="item_qty" value="1" />
                                <div id="divqty" style="display:none"><h2><font color="#FF0000">Invalid Qty.</font></h2>
                                </div>
                                 <? } else { ?>
                                <select id="item_qty" name="item_qty" style="width:40px;" class="ShopQty required">
                                <? for($vit=1;$vit<=$virtual_qty;$vit++) { ?>
                                <option value="<?=$vit?>"><?=$vit?></option>
                                
                                <? } ?></select> <? } ?>
                                
        <?   
		  if($type_id=='simple' && $userid!='Guest')  { 
		  $strecount = query_execute_row("SELECT count(*) as referal from shop_wishlist where userid='$userid' AND item_id='$item_id'");
		  if($strecount['referal']=='0' ) {   ?>
        <input name="itemchq" type="text" id="itemchq" value="" class="dn"/>
           <div id="mainwish" style="margin-top:7px;">
             <img src="/new/pix/wishlist.png" style="vertical-align: middle; margin-top:-5px;" ><? if($userid!='Guest') { ?>&nbsp; <a  href="javascript:check('<?=$item_id?>')">Add to My Wishlist</a><? } else { ?><a  href="/login.php"> Add to My Wishlist</a><? } ?></div>
         <div id="frstwish" style="display:none; vertical-align: middle; margin-top:7px;"><img src="/new/pix/wishlist.png" style="vertical-align: middle; margin-top:-5px;">&nbsp; Added to Wishlist. <a href="/new/shop/wishlist/"><strong>View Your Wishlist</strong></a></div>
              <? } else { ?>
               <div style="margin-top:7px;"><img src="/new/pix/wishlist.png" style="vertical-align: middle; margin-top:-5px;" >&nbsp; Already in Wishlist. <a href="/new/shop/wishlist/"><strong>View Wishlist.</strong></a></div>
                                <?  }  // if aleady wishlisted ends here
								 }   // type id simple ends here ?>                      
                                </div> 
                                <div class="fr">
                                
                                <? if($price!='0' || $item_id=='1283'){?>
                                                               
                                <input name="addtoItemCart" type="image" id="addtoItemCart" style="border:0px; margin-top:10px;" src="/new/pix/buynow_btn.gif" alt="Submit"/>
                                
                                <?php /*?><span style="color:#F00; font-size:18px">Sold Out</span><?php */?>
								<? }?>
                                </div>
                               
                              <div class="cb"></div>
                              </form>
                            <? } else { 
							?>
							<div class="cb"></div>
                              </form>                            <div  style="float:left">
								  <div id="notime" >
                               <p  align="left">Notify me when this product is in Stock.</p>
                                    
                                    <table width="100%">
                                    <tr >
                          <td><strong>Email id</strong>:&nbsp;<input name="notify_email" id="notify_email" value="<?=useremail($userid)?>" type="email" /> </td>
                         <td><strong>Mobile no</strong>:&nbsp;<input name="notify_mobile" id="notify_mobile" value="<?=userphonenumbr($userid)?>" type="text" maxlength="10" />
                         
                         </td></tr><tr>
                         <td><div id="invalidformat1" style="display:none">
                          <h2><font color="#FF0000">This field is required.</font></h2>
                          </div></td>
                          <td><div id="invalidformat" style="display:none">
                         <h2><font color="#FF0000">This field is required.</font></h2>
                         </div>
                          </td></tr>
                                    <input type="hidden" id="notify_item" name="notify_item" value="<?=$item_id?>" />
                                     
                                    <tr align="right"><td></td>
                                    <td>
                                     
             <input name="notify" type="image" id="notify" style="border:0px; margin-top:10px;" onclick="validateForm()" src="/new/pix/butt_notifyme.jpg" />                                                      
                                    </td></tr>
                              </table> </div>
                                    <div id="notifyme1"></div>                     
                                </div> 
                                
                                <div class="fr">
                                
                               
                                </div>
                               
                              <div class="cb"></div>
                            
                            
                              
                              <? }?>
                            </div>
                            
                 
                    </div><!-- text hold -->
                    
                    

       
       			
             <!-- first column -->
             
             <!--second column -->
          
             	<div class="shopcontRight194">
                   <!-- box 1-->
                	<div class="shopIndMid">
                    	<div style=" padding:15px 20px 0px 26px;">
                    	<!--  -->
                        	<ul>
                            	<li><span class="left">Item No:</span><span class="right"><strong><?=$item_id?></strong></span><span class="cb"></span></li>
                                <?  $Brand = query_execute_row("SELECT brand_nice_name from shop_brand where brand_id='$brand_id'");?>
                                <li style="height:35px;"><span class="left">Brand:</span><span class="right"><a href="https://www.dogspot.in/<?=$Brand['brand_nice_name'];?>/" alt="<?=$ArrayShopBrands["$brand_id"]?>" title="<?=$ArrayShopBrands["$brand_id"]?>"><strong><span style="color:#86a70e;"><?=$ArrayShopBrands["$brand_id"]?></span></strong></a></span><span class="cb"></span></li>
                                
                                <li style="height:35px;"><span class="left">Rating:</span><span class="right">
                                <? 
				$qRATING = query_execute_row("SELECT * FROM section_rating WHERE section_id='$item_id' and site_section='item'");
								
								$qbest = '5';
								if($qRATING['num_votes']!='0') {
								$nRating=$qRATING['rating']/$qRATING['num_votes'];
								if($nRating <= 0){ $finalratng='0';}
								if($nRating > 0 && $nRating <= 1){ $finalratng='1';}
								if($nRating > 1 && $nRating <= 2){ $finalratng='2';}
								if($nRating > 2 && $nRating <= 3){ $finalratng='3';}
								if($nRating > 3 && $nRating <= 4){ $finalratng='4';}
								if($nRating > 4 && $nRating <= 5){ $finalratng='5';}
								}
								?> <div id="rateIt" >
                                
     <div id="rateIt" class="dn" itemprop="aggregateRating"   itemscope itemtype="http://schema.org/AggregateRating">
    <span itemprop="ratingValue"><?=$finalratng?></span>
    <span itemprop="bestRating"><?=$qbest;?></span>
     <span itemprop="ratingCount"><?=$qRATING['num_votes']?></span> 
  </div>
                                <ul class="rating <? print(getRating("SELECT rating, num_votes FROM section_rating WHERE section_id = '$item_id' AND site_section = 'item'"));?>">
	<li class="one"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=1&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="1 Star">1</a></li>
	<li class="two"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=2&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="2 Stars">2</a></li>
	<li class="three"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=3&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="3 Stars">3</a></li>
	<li class="four"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=4&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="4 Stars">4</a></li>
	<li class="five"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=5&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="5 Stars">5</a></li>
</ul></div>
<div id="rateLoad"></div>
</span><span class="cb"></span></li>
                            
                            <!--<li><span class="left">Reviews:</span><span class="right"><strong><span style="color:#86a70e;">34</span></strong></span></span><span class="cb"></span></li>-->
                            <li class="last"><span  >Viewed: </span> <span class="right">&nbsp;&nbsp;<strong><?=$rowdat["num_views"]?> times</strong></span></span><span class="cb"></span></li>
                                 <!--<li class="last"><span>Have you used this product? <strong>Write a Review</strong></span></li>-->
                            </ul>
                            
                        
                        </div>
                        <!--  -->
                    	
                     </div>
                     <div class="shopIndBtm"></div>
                     <!-- box 1-->
                     
                     <div class="vs10"></div>
                     <!-- second box -->
                    	 <div class="shopIndrtcont">
                             <div style=" padding:15px 15px 5px 26px; " >
                             <div style="float:left;padding-right:10px">
                             <div class="fb-like" data-href="https://www.dogspot.in/<?=$section[0]?>/" data-layout="button_count" data-width="100" data-show-faces="false" style="float:left"></div>
                             	<div id="fb-root" style="float:left"></div></div><div style="float:left;width:60px">
<?php /*?><script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script><?php */?>
                                                                 
                                <!-- Place this tag where you want the +1 button to render -->
<g:plusone size="tall" annotation="none" width="50px" ></g:plusone>

<!-- Place this render call where appropriate -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script></div><div class="cb"></div>
<div class="vs10"></div>

<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
             <div style="float:left;width:50px"> <a target="_blank" href="http://pinterest.com/pin/create/button/?url=https://www.dogspot.in/<?=$section[0]?>/&media=https://www.dogspot.in/shop/item-images/orignal/<?=$imgFLBase[0]?>&description=<?=$pageHone?>" class="pin-it-button" count-layout="horizontal" ><img border="0" src="//assets.pinterest.com/images/PinExt.png" width="43" height="21" title="Pin It" /></a>                  
                                </div>
                                
                 <div style="float:left;width:50px" ><a target="_blank" href="https://twitter.com/share" class="twitter-share-button" >Tweet</a>
<?php /*?><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script><?php */?>
<script type="text/javascript">var switchTo5x=false;</script>
<script>
$(document).ready( function( ) {
$.getScript('http://w.sharethis.com/button/buttons.js');
$.getScript('http://platform.twitter.com/widgets.js');
});
</script>
</div>
                               <!--  -->
                              
                                <span id="dezyna-share-btn" style="float:left;width:70px;margin:33px 0 0 -99px;height:22px;" data-key="af7b9ce46856b20aeb43f813abacc3ba"
             data-price="<?=$price?>"
             data-landingUrl="https://www.dogspot.in/<?=$section[0]?>/"
             data-imageUrl="https://www.dogspot.in/shop/item-images/orignal/<?=$imgFLBase[0]?>"
             data-description="<?=$pageHone?>" >
      <script type="text/javascript">
        (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = "https://d9uqz2w57ab4i.cloudfront.net/js/shareitbutton.js"
            document.getElementById('dezyna-share-btn').appendChild(e);
        }());
      </script>
    </span>
                             </div>
                             <div class="cb"></div>
                             <div class="shopIndBtm"></div>
                         </div>
                         <div class="cb"></div>
                         <!-- second box --> 
                         
                </div><!--second column -->
                <div class="cb"></div>
                <div class="vs5"></div>
                <ul class="shoptabs" style="margin-left:10px;">
                    <li style="width:190px;"><span><b>Secure Payments</b>Credit/Debit Card & Net Banking</span></li>
                    <li style="width:194px;"><span><b>All India Free Shipping</b>Most Reliable Logistics Partners</span></li>
                    <li style="width:240px;margin-right:0px;"><span><b>Customer Care</b>+91-9212196633, Mon- Fri (10 am - 6 pm)</span></li>
                </ul>                        
                </div>
                          <div class="vs20"></div>
  <!--suggest product-->
   <?
	             if($type_id=='simple'){ 
				 $item_id23=$item_id;	  
				 } else {$qItemq23=query_execute_row("SELECT item_id FROM shop_items  WHERE item_parent_id='$item_id23' ORDER BY item_id  LIMIT 1");
		$item_id23=$qItemq23['item_id'];	 
		}
		
		$qItem233=query_execute_row("SELECT count(*) as boughtcount FROM shop_item_bought WHERE item_id='$item_id23'  ");			  		
		if($qItem233['boughtcount']>=4){  //echo "SELECT count(*) as boughtcount FROM shop_item_bought WHERE item_id='$item_id23'  "; ?>                        
 <div class="cont980 combo_box">
<!--FREQUENTLY BOUGHT TOGETHER-->
<?php require_once($DOCUMENT_ROOT.'/new/shop/suggest_product.php'); ?>
<!--FREQUENTLY BOUGHT TOGETHER-->
 
</div>  <?  }?>  
  <!--close suggest product-->           
<div class="productSummary769 pageBody">
             
 <div class="vs10 cb"> </div>
              <?
			  $item_id=$item_id23;
             	if($rowdat["item_parent_id"]!='0'){
				  $rowPabout=query_execute_row("SELECT item_about FROM shop_items WHERE item_id='$item_id'");
				  if($rowPabout['item_about']){
					  echo $rowPabout["item_about"];
				  }
				 else
				 {
				    $rowAabout=query_execute_row("SELECT item_about FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
					echo $rowAabout['item_about'];	
				 }

			  }else{
				  echo $rowdat["item_about"];
			  }
			  ?>
              
            
           
              
<!--Comment system-->
<div style="margin-top:20px; background-color:#fafafa; padding:10px;">        
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'dogspot'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>
</div>
<!--Comment system--> 


 </div>
 <!-- related products -->
 
<div class="cont183" style="margin-right:0px; float: right;  width: 153px;margin-top: 15px;">
                	
        <div align="center">
        <?php addbanner160('600','160','1'); ?></div> <div id="Divclear"></div>
        
</div> 
</div>              
                       <!-- related products -->
                           
<!-- popup Box Start--> 
<div id="popupContact" style="min-height:330px;" class="popUp710">
<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />
</div>  
<div id="backgroundPopup" style="z-index:9999"></div>  

<!-- popup Box END-->
<div id="login-box" class="login-popup">
<?php require_once($DOCUMENT_ROOT.'/new/shop/shipping-detail.php'); ?></div>  
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>