<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
if($userid=='Guest'){
	header("Location: /login.php?refUrl=/shop/myorders.php");
	ob_end_flush();
	exit(); 
}
$sitesection = "shop";
$session_id = session_id();
?>
<!DOCTYPE html>
<html>
<head>
    <title>My Order Details</title>
    <script src="js/myorderpage.js" type="text/javascript"></script>
 <style>
 

ul.myorderpage_tabs
{
    padding: 7px 0;
    font-size: 0;
    margin:0;
    list-style-type: none;
    text-align: left; /*set to left, center, or right to align the myorderpage_tabs as desired*/
}
        
ul.myorderpage_tabs li
{
    display: inline;
    margin: 0;
    margin-right:3px; /*distance between myorderpage_tabs*/
}
        
ul.myorderpage_tabs li a
{
    font: normal 12px Verdana;
    text-decoration: none;
    position: relative;
    padding: 7px 16px;
    border: 1px solid #CCC;
    border-bottom-color:#B7B7B7;
    color: #000;
    background: #F0F0F0 url(tabbg.gif) 0 0 repeat-x;
    border-radius: 3px 3px 0 0;
    outline:none;
}
        
ul.myorderpage_tabs li a:visited
{
    color: #000;
}
        
ul.myorderpage_tabs li a:hover
{
    border: 1px solid #B7B7B7;
  
}
        
ul.myorderpage_tabs li.selected a, ul.myorderpage_tabs li.selected a:hover
{
    position: relative;
    top: 0px;
    font-weight:bold;
    background: white;
    border: 1px solid #B7B7B7;
    border-bottom-color: white;
}
        
        
ul.myorderpage_tabs li.selected a:hover
{
    text-decoration: none;
}
     

div.tabcontents
{
    border: 1px solid #B7B7B7; padding:2% 2% 4% ;
    background-color:#FFF;
    border-radius: 0 3px 3px 3px;
	font-size:14px;
}

.recentorder_tab
{
display:block;
width:100%:
}
.tabsection_1
{
width:15%;
}
.tabsection_2
{
width:20%;
}
.tabsection_3
{
width:25%;
}
.tabsection_4
{
width:40%;
}
.tab_unit
{
float:left;

margin-bottom:10px;
}
.myorederpage_orderbtn
{
background:#a1cc01;

text-align:center;
border-radius:3px;
box-shadow: 0px 0px 4px #D3CBB8;

}
#order-section {
color: #565656;
}
#order-section .order {
    margin: 0px 4px 20px;
    background-color: #FFF;
    border: 1px solid #CCC;
    box-shadow: 0px 0px 4px #D3CBB8;
}
#order-section .order-expanded {
    background-color: #F9F9F9;
    border-bottom: 1px solid #E6E6E6;
    padding: 12px 15px;
	height: 40px;
}
.size1of1{
	float:none
}

.size1of2{
	width:50%
}

.size1of3{
	width:33.3333%
}

.size2of3{
	width:66.6667%
}

.size1of4{
	width:25%
}

.size3of4{
	width:75%
}

.size1of5{
	width:20%
}

.size2of5{
	width:40%
}

.size3of5{
	width:60%
}

.size4of5{
	width:80%
}

.size1of6{
	width:16.6667%
}

.size5of6{
	width:83.3333%
}

.size1of7{
	width:14.2857%
}

.size2of7{
	width:28.5714%
}

.size1of8{
	width:12.5%
}

.size3of8{
	width:37.5%
}

.size1of9{
	width:11.1111%
}

.unit {
    float: left;
}
#order-section .orderIdBtn {
    width: 196px;
}
.btn.btn-blue {
    border: 1px solid #a4ce01;
    background: #a4ce01;
}
.btn.btn-medium {
    padding: 8px 12px;
    font-size: 13px;
}
.btn {
    background-color: #CCC;
    border: 1px solid #CCC;
    display: inline-block;
    line-height: 1.3;
    color: #F9F9F9;
    text-transform: uppercase;
    cursor: pointer;
    text-align: center;
    border-radius: 2px;
}
a {
    color: #666;
    text-decoration: none;
    cursor: pointer;
}

#order-section .order-expanded .deliveryInfo {
    width: 46%;
}
#order-section .order-expanded .ds-alert-user {
    margin: 0px 0px 0px 20px;
}
.ds-inline-block {
    display: inline-block;
}
.ds-alert-user {
    padding: 9px 10px;
    background-color: #f7ffd8;
    border: 1px solid #ddd;
    margin:  0px;
	font-size:12px;
}
#order-section .truck {
    width: 31px;
    height: 17px;
}
.rmargin10 {
    margin-right: 10px;
}
#order-section .truck {
    display: inline-block;
    vertical-align: middle;
    height: 13px;
    width: 17px;
    
}
.text_right {
    text-align: right;
}
.line, .lastUnit {
    overflow: hidden;
}
#order-section .order-item {
    margin: 0px 15px;
    padding: 10px 0px;
    border-bottom: 1px solid #CCC;
}

#order-section .smallText {
    color: #848484;
    font-size: 11px;
}
.ds-text-center {
    text-align: center;
}
.lpadding10 {
    padding-left: 10px;
}
.bmargin10 {
    margin-bottom: 10px;
}
p{margin:0px;}
#order-section .button-bar {
    text-align: center;
    display: inline-block;
    border-radius: 2px;
    border: 1px solid #C9C9C9;
    background: linear-gradient(to bottom, #F9F9F9 1%, #FFF 3%, #F9F9F9 7%, #F2F2F2 100%) repeat scroll 0% 0% transparent;
}
#order-section .button-bar a {
    display: inline-block;
    white-space: nowrap;
    padding: 6px 0px;
    width: 100px;
    text-align: center;
    text-decoration: none;
}
#order-section .order-total {
    margin: 10px 15px;
}

.rmargin20 {
    margin-right: 20px;
}
.tmargin20 {
    margin-top: 20px;
}

 </style>
 <script>
 function show(var1){
	 alert(var1);
	 if(var1=='view1')
	 {
		 $('#view1').css("display","block");
		  $('#view3').css("display","none");
	 }else
	 {
	$('#view3').css("display","block");
		  $('#view1').css("display","none");	 
	 }
 }</script>
</head>
<script type="text/javascript" src="jquery/jquery.js"></script>
	<script type="text/javascript" src="jquery/jRating.jquery.js"></script>
<link rel="stylesheet" href="jquery/jRating.jquery.css" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<script>function success()
{
	//alert('4');
	var query=$('#insert').text();
	//alert(query);
var targetDiv = $('.jDisabled').attr('id');

 ShaAjaxJquary('/new/shop/update.php?query="'+query+'"&vendor='+targetDiv+'', '#notice', '', '', 'POST', '#notice', '...', 'REP');	
}</script>
<script type="text/javascript">
$(document).ready(function() {
<?php /*?>	
$('.basic').jRating();
			$(".exemple6").jRating({
			  length:5,
			  decimalLength:1,
			  showRateInfo:false
			});<?php */?>
	
$('a.login-window').click(function() {
	
var loginBox = $(this).attr('href');
$(loginBox).fadeIn(300);
$(loginBox).css('display','block');
$('#mask').css('display','block');
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
});
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});

</script>
<script type="text/javascript">
function disablePopup(){
$('#login-box').css('display','none');
$('#mask').css('display','none');
<?php /*?>_gaq.push(['_trackPageview', 'https://www.dogspot.in/<?=$section[0]?>/']);
_gaq.push(['_trackEvent', 'product recommendation', 'no', 'dry-dog-food']);
jQuery('#ajaxdiv').fadeOut("slow");
jQuery('#mask').css({
			"display": "none"
		});
		
$("#cookDSPopSubscribe").val('No');	
ShaAjaxJquary("/Recommend-Product/session_store.php?s=1", "#ajaxdiv23", '', '', 'GET', '', '<img src="/images/indicator.gif" />','REP');<?php */?>
}
</script>

<?
//echo "SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND order_status != 'inc'  ORDER BY order_c_date DESC";
$qGetMyCart=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND order_status != 'inc' AND order_c_date BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) AND NOW() ORDER BY order_c_date DESC");
$totrecord = mysql_num_rows($qGetMyCart);
//echo "SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND order_status != 'inc' AND order_c_date < DATE_SUB(NOW(), INTERVAL 60 day); ORDER BY order_c_date DESC";
$qGetMyCart10=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND order_status != 'inc' AND order_c_date < DATE_SUB(NOW(), INTERVAL 60 day) ORDER BY order_c_date DESC LIMIT 0,12");
//$totrecord10 = mysql_num_rows($qGetMyCart10);
?>
<body style="background:#F6F9FC; font-family:Arial;">
    <div style="width: 972px; margin: 0 auto; ">
        <ul class="myorderpage_tabs" data-persist="true">
            <li onClick="show('view1')"><a href="#view1" >RECENT ORDERS (Last 2 Month)</a></li>
            <li onClick="show('view3')"><a href="#view3">PAST ORDERS</a></li>
        
        </ul>
        <div class="tabcontents">
            <div id="view1">
			<!-- order-section-->
			
			<div id="order-section">
            <?
if($totrecord == 0){
	echo '<div class="vs10"></div>';
	echo '<h2>Sorry no Order...</h2>';?>
	<div class="vs30"></div>
	<div ><a href="/shop/" class="redButton cornerRoundAll">Continue Shopping</a></div>
	<div id="clearall"></div>
	<?
}else{
?>
	<?
	$s=1;
	while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart["order_id"]."' ORDER BY order_c_date ");
		$totalord=$partialdetail['ord'];
		$partilord=explode(',',$totalord);
	?>
			<div class="order">
			<!-- order-section header-->
			<div class="line order-expanded">
                  <div class="unit size2of5">
                      <a class="orderIdBtn btn btn-medium btn-blue" target="_blank" href="#"><?=$rowMyCart["order_id"];?></a>
                  </div>
                  <div class="unit size1of2 deliveryInfo">
                                                <?php /*?><p class="ds-alert-user ds-inline-block">
                              <i class="truck rmargin10"><img src="img/truck.jpg"></i> &nbsp; &nbsp;Your complete order will be delivered by Tue, 19th Aug'14                          </p><?php */?>
                                        </div>
                  <div class="lastUnit text_right">
                                          <? 
	if($rowMyCart["delevery_status"]=='new' || $rowMyCart["delevery_status"]=='pending-dispatch'){?>                                                                                                                                                  <div class="button-bar" title="Cancel this item">
                                                        <a class="cancel action" data-pagename="myorders_newdesign" orderstatus="MyOrders_on_hold" orderid="OD40812242391" unitid="90934877" itemid="90820171"><i></i><a href="#login-box<?=$rowMyCart["order_id"]?>" class="login-window">Cancel</a></a>
                                                     </div>
                                                     <? }?>
                                                                                 </div>
             </div>

<!-- order-section header-->
<!-- order product details-->
<div class="line">
    <?
	//echo "SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart["order_id"]."' ";
$qGetMyCart1=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart["order_id"]."' ");	
	while($rowMyCart1 = mysql_fetch_array($qGetMyCart1)){
		 $item_id = $rowMyCart1["item_id"];
		$cart_id=$rowMyCart1["cart_id"];
		
		$qdata=query_execute("SELECT name, price, weight, item_parent_id,item_status,stock_status,item_display_status FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		//if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart1["item_totalprice"]+$ItemGtotal;
		$item_parent_id=$rowdat["item_parent_id"];
		// Get Tital option
		$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];
		
		// END
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		//echo "SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id'";
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id'");
		$rowdatM = mysql_fetch_array($qdataM);
		
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		if(file_exists($imagepath)){
			$new_w = 100;
			$new_h = 100;
			$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
		}
		if($rowdat["item_parent_id"]!=0){
			$rownicename=query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
			$nice_name=$rownicename["nice_name"];
		}else{
			$nice_name=$rowdat["nice_name"];
		}
		
	?>

                                      <div class="line order-item ">
                         <div class="line order-item-inner">
                                                                                           <div class="unit size1of8 ds-text-center product-image">
                                    <a href="#" target="__blank">
                                        <img src="/shop/item-images/thumb_<?=$rowdatM["media_file"]?>" alt="<?=$rowdatM["label"]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/>
                                    </a>
                                </div>
                                <div class="unit size2of7">
                                                    <p class="bmargin10">                                                          
																											  
                                           <a href="/shop/<?=$nice_name?>" target="_blank"><?=$rowdat["name"];?></a>                                       </p>
                                                                                                                                                    <p class="smallText tmargin10 ">
                                                                                     Size: 3.0 Kg                                          Qty: <?=$rowMyCart1["item_qty"]?>                                        </p>
                                                                    </div>
                                <div class="unit size1of6">
                                    <div class="lpadding10">
                                                                                    Rs. <?=$rowMyCart1["item_price"]?>                                                                                                                  </div>

                                </div>
                                <div class="unit size2of7">
                                                                                                                                                    <?php /*?><p class="greyText bmargin10">
                                                                                            Delivery expected by Tue, 19th Aug'14                                                                                    </p><?php */?>
                                                                                                                <p class="greyText bmargin10">Your order has been confirmed</p>
                                                                                                    </div>
                                <div class="lastUnit text_right"><div class="exemple">
	<div class="exemple6" data-average="1" id="<?=$item_vendor?>" data-id="6"></div>
    <div class="notice" style="display:none">
<pre>
</pre>
</div>



<div class="datasSent" style="display:none">
	Datas sent to the server :
	<p></p>
</div>
<div class="serverResponse" style="display:none">
	Server response :
	<p></p>
</div>
</div>
                                                                                                                                                                                             
                                                                                 </div>
                                                      </div>
                                                      
                     </div>
<? }?>
					 
					 <!-- oreder total-->
					 
                                  <div class="line order-total">
                    <div class="line">
                        <div class="unit size2of5">
                                                            <span class="smallText">Seller:</span> <span class="rmargin20">Dogspot</span>
                                                        <span class="smallText ds-inline-block">Date:</span> <?=$rowMyCart["order_c_date"];?>                        </div>
                                                        <div class="lastUnit text_right">
                            <span class="smallText">Shipping Charges:</span> <strong>49</strong>
                        </div>
                        <div class="lastUnit text_right">
                            <span class="smallText">Order Total:</span> <strong><? if($rowMyCart["order_status"]=='0'){$paidamu=$paidamu+$rowMyCart["order_amount"];}else{$unpaidamu=$unpaidamu+$rowMyCart["order_amount"];}?><?=number_format($rowMyCart["order_amount"],0);?></strong>
                        </div>
                    </div>
                 </div>
				 					 <!-- oreder total-->
									 
             </div>
			 <!-- order section-->
			 </div>
             <div id="login-box<?=$rowMyCart["order_id"]?>" class="login-popup" style="box-shadow:none; border:8px solid #bfbfbf;">
					<?php require_once($DOCUMENT_ROOT.'/new/shop/order-cancel.php'); ?></div>
             <? }}?>
			 <!-- order cancel-->
			 
			 <!-- order cancel-->
<!-- orderproduct details-->
</div>
</div>
                
            
             <div id="view3">
				
			<div id="order-section">
			<?
	$s=1;
	while($rowMyCart10 = mysql_fetch_array($qGetMyCart10)){
		$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart10["order_id"]."' ORDER BY order_c_date ");
		$totalord=$partialdetail['ord'];
		$partilord=explode(',',$totalord);
	?>
			<div class="order">
			<!-- order-section header-->
			<div class="line order-expanded">
                  <div class="unit size2of5">
                      <a class="orderIdBtn btn btn-medium btn-blue" target="_blank" href="#"><?=$rowMyCart10["order_id"];?></a>
                  </div>
                  <div class="unit size1of2 deliveryInfo">
                                                <?php /*?><p class="ds-alert-user ds-inline-block">
                              <i class="truck rmargin10"><img src="img/truck.jpg"></i> &nbsp; &nbsp;Your complete order will be delivered by Tue, 19th Aug'14                          </p><?php */?>
                                        </div>
                  <div class="lastUnit text_right">
                                             <? 
	if($rowMyCart10["delevery_status"]=='new' || $rowMyCart10["delevery_status"]=='pending-dispatch'){?>                                                                                                                                                 <div class="button-bar" title="Cancel this item">
                                                        <a class="cancel action" data-pagename="myorders_newdesign" orderstatus="MyOrders_on_hold" orderid="OD40812242391" unitid="90934877" itemid="90820171"><i></i><a href="#login-box" class="login-window">Cancel</a></a>
                                                     </div>
                                                     <? }?>
                                                                                 </div>
             </div>

<!-- order-section header-->
<!-- order product details-->
<div class="line">
    <?
	//echo "SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart["order_id"]."' ";
$qGetMyCart1=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart10["order_id"]."' ");	
	while($rowMyCart1 = mysql_fetch_array($qGetMyCart1)){
		 $item_id = $rowMyCart1["item_id"];
		$cart_id=$rowMyCart1["cart_id"];
		
		$qdata=query_execute("SELECT name, price, weight, item_parent_id,item_status,stock_status,item_display_status FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		//if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart1["item_totalprice"]+$ItemGtotal;
		$item_parent_id=$rowdat["item_parent_id"];
		// Get Tital option
		$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];
		
		// END
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		//echo "SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id'";
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id'");
		$rowdatM = mysql_fetch_array($qdataM);
		
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		if(file_exists($imagepath)){
			$new_w = 100;
			$new_h = 100;
			$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
		}
		if($rowdat["item_parent_id"]!=0){
			$rownicename=query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
			$nice_name=$rownicename["nice_name"];
		}else{
			$nice_name=$rowdat["nice_name"];
		}
		
	?>

                                      <div class="line order-item ">
                         <div class="line order-item-inner">
                                                                                           <div class="unit size1of8 ds-text-center product-image">
                                    <a href="#" target="__blank">
                                        <img src="/shop/item-images/thumb_<?=$rowdatM["media_file"]?>" alt="<?=$rowdatM["label"]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/>
                                    </a>
                                </div>
                                <div class="unit size2of7">
                                                    <p class="bmargin10">                                                          
																											  
                                           <a href="/shop/<?=$nice_name?>" target="_blank"><?=$rowdat["name"];?></a>                                       </p>
                                                                                                                                                    <p class="smallText tmargin10 ">
                                                                                     Size: 3.0 Kg                                          Qty: <?=$rowMyCart1["item_qty"]?>                                        </p>
                                                                    </div>
                                <div class="unit size1of6">
                                    <div class="lpadding10">
                                                                                    Rs. <?=$rowMyCart1["item_price"]?>                                                                                                                  </div>

                                </div>
                                <div class="unit size2of7">
                                                                                                                                                    <?php /*?><p class="greyText bmargin10">
                                                                                            Delivery expected by Tue, 19th Aug'14                                                                                    </p><?php */?>
                                                                                                                <p class="greyText bmargin10">Your order has been confirmed</p>
                                                                                                    </div>
                                <div class="lastUnit text_right">
                                                                                                                                                                                             
                                                                                 </div>
                                                      </div>
                                                      
                     </div>
<? }?>
					 
					 <!-- oreder total-->
					 
                                  <div class="line order-total">
                    <div class="line">
                        <div class="unit size2of5">
                                                            <span class="smallText">Seller:</span> <span class="rmargin20">Dogspot</span>
                                                        <span class="smallText ds-inline-block">Date:</span> <?=$rowMyCart10["order_c_date"];?>                        </div>
                                                        <div class="lastUnit text_right">
                            <span class="smallText">Shipping Charges:</span> <strong>49</strong>
                        </div>
                        <div class="lastUnit text_right">
                            <span class="smallText">Order Total:</span> <strong><? if($rowMyCart10["order_status"]=='0'){$paidamu=$paidamu+$rowMyCart10["order_amount"];}else{$unpaidamu=$unpaidamu+$rowMyCart10["order_amount"];}?><?=number_format($rowMyCart10["order_amount"],0);?></strong>
                        </div>
                    </div>
                 </div>
				 					 <!-- oreder total-->
									 
             </div>
			 <!-- order section-->
			 </div>
             <? }?>
		
<!-- orderproduct details-->
</div>
	</div>
</body>
</html>
