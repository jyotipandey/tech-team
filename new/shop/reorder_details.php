<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/new/shop/reorder_details-bootstrap.php');
  exit;
  }
if($userid=='Guest'){
	header("Location: /login.php?refUrl=/shop/reorder_details.php");
	ob_end_flush();
	exit(); 
}
$sitesection = "shop";
$session_id = session_id();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Re-Orders | DogSpot</title>
<meta name="keywords" content="Re-Orders | DogSpot" />
<meta name="description" content="Re-Orders | DogSpot" />

<link href="/css/linkbutton.css" rel="stylesheet" type="text/css" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<script>
$(document).ready(function(){
  $("tr:odd").css("background-color","#fff");
});
</script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  margin-top:15px;
}


th { 
  background-color: #f2f2f2;
  font-weight:normal;
  font-size:16px;
  color:#333;

}

td { 
  
  width:10%;
  
}
td, th { 
  padding: 10px; 
  border-bottom: 1px solid #ccc; 
  text-align: left; 
  font-weight:normal;
  font-size:12px;
 
}
.reorder_blk{ width:100%; float:left; font-family:Arial, Helvetica, sans-serif; }
.reorder_blk h1{font-size: 16px; margin-top: 20px; margin-bottom: 15px; font-weight: normal; }

</style>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="cont980">
<div class="reorder_blk">
<!--<div style="float:right; margin-left:5px; width:780px; margin-top:5px; ">
--><?
//echo "SELECT * FROM shop_order WHERE userid = '$userid' AND mode != 'TEST' AND order_status != 'inc' ORDER BY order_c_date DESC";
$qGetMyCart=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND order_status != 'inc'  ORDER BY order_c_date DESC");
$totrecord = mysql_num_rows($qGetMyCart);
?>
<h1>Wish to Reorder  <span style="font-size:13px; margin-left:3px;">
(Total Orders: <?=$totrecord;?>)</span></h1>
<?
if($totrecord == 0){
	echo '<div class="vs10"></div>';
	echo '<h2>Sorry no Order...</h2>';?>
	<div class="vs30"></div>
	<div ><a href="/shop/" class="redButton cornerRoundAll">Continue Shopping</a></div>
	<div id="clearall"></div>
	<? ;
}else{
?>
 <table>
<thead>
<tr>
      <th width="18%">Order Date</th>
      <th width="18%">Order Id</th>
      <th width="18%">Order  Amount</th>
      <th width="18%">Payment Method</th>
      <th width="10%">Qty</th>
      <th width="18%">Want To Reorder</th>
      
    </tr></thead><tbody>
	<?
	while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart["order_id"]."' ORDER BY order_c_date ");
		$totalord=$partialdetail['ord'];
		$partilord=explode(',',$totalord);
	?>
    
   <tr>
   <td><?=showdate($rowMyCart["order_c_date"], "d M o")?></td>
      <td><?=$rowMyCart["order_id"];?></td>
     <td>Rs. <? if($rowMyCart["order_status"]=='0'){$paidamu=$paidamu+$rowMyCart["order_amount"];}else{$unpaidamu=$unpaidamu+$rowMyCart["order_amount"];}?><?=number_format($rowMyCart["order_amount"],0);?></td>
      <? 
	   $qGetMyCartCount=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart["order_id"]."'");
	   $totitem = mysql_num_rows($qGetMyCartCount);
	 ?>
      <td><?=$AOrderMethod[$rowMyCart["order_method"]];?></td>
     <td><?=$totitem;?></td>      
      <td><a href="reorders.php?order_id=<?=$rowMyCart["order_id"];?>" style="text-decoration:underline;">Reorder</a></td>
     
    </tr>
    
<? }?> 
</tbody></table>
<? }?>
<div>
<div id="clearall"></div>
</div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>