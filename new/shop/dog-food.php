<?
require_once('constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-ArrayShopAttributeValue.php');

if($userid=='jyoti05')
  {
  include_once($DOCUMENT_ROOT . '/new/shop/dog-food-bootstrap.php');
  exit;
  }
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection = "shop";
$ecomm_prodid = "";
$ecomm_totalvalue = "";
$ecomm_pagetype = 'category';
//------------Show
$maxshow = 100;
if(empty($show)) {
	$show = 0;
}else{
 $show = $show - 1;
}
 $showRecord = $show * $maxshow;
 $nextShow = $showRecord + $maxshow;
//-------------Show
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Dog Supplies Store | Online Shop for Dogs & Dog Products| DogSpot.in</title>
<meta name="keywords" content="Dog Store, Dog Store online, Best Dog Store, Online Dog store India" />
<meta name="description" content="High quality dog supplies and dog products store in India. Shop online for your dog's daily needs. Easy shopping, hassle-free shipping, pay cash on delivery." />
<link rel="canonical" href="https://www.dogspot.in/dog-store/" />
<link rel="amphtml" href="https://www.dogspot.in/amp/dog-store/" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<script type="text/javascript" src="/new/js/tabs.js"></script>
<link href="/new/css/style.css?h=599" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/slide.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script type="text/javascript" src="/new/js/iepngfix_tilebg.js"></script>
<?
if($section[0]!='gosf'){ ?>
<script type="text/javascript">
function disablePopup(){
<?php /*?>_gaq.push(['_trackPageview', 'https://www.dogspot.in/<?=$section[0] ?>/']);
_gaq.push(['_trackEvent', 'product recommendation', 'no', '<?=$section[0] ?>']);<?php */?>
jQuery('#ajaxdiv').fadeOut("slow");
jQuery('#mask').css({
			"display": "none"
		});
		
$("#cookDSPopSubscribe").val('No');	
ShaAjaxJquary("/Recommend-Product/session_store.php?s=1", "#ajaxdiv23", '', '', 'GET', '', '<img src="/images/indicator.gif" />','REP');
}

function abcd() {
ga('send', 'event', 'Need Help', 'Yes', {'page':'/<?=$section[0]?>/'});

	//$('a.login-window').click(function() {
<? 		
//if($sessionDSPopSubscribe!="NO"){ ?>		
var loginBox = $('a.login-window').attr('href');
$(loginBox).fadeIn(300);

var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
//});
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});

};
<? // } 
$v=0;
if($category_id!='91' && $category_id!='87' ){
for($i=175;$i<=225;$i++){
	if($category_id==$i)$v++;
	}
if($v==0){?>
//setTimeout(abcd, 10000);
<? }}?>
</script>
<? } ?>
<script>
IEPNGFix.blankImg = 'blank.gif'; 
</script>
<style>


.catpage_catenamebox {
	margin-top: 5px;
	float: left;
	width: 100%;
	font-family: lato, sans-serif;
	border-bottom: 1px solid #ddd;
	margin-bottom: 30px;
	padding-bottom: 20px;
}
.catpage_catenamebox a:hover{ text-decoration:none;color: #333;}
.catpage_catname {
	text-decoration: none;
	font-size: 18px;
	color: #555;
	font-weight: normal;
}

.header-banner{width: 100%; margin:0px 0px 10px; float:left; text-align:center;}
.header-banner img{ max-width:100%; height:auto;}
</style>
<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>
<!-- breadcrumb -->
<div class="about-cat-section">
  <div class="cont980" >
    <div id="productFilter" itemscope itemtype="http://schema.org/Product">
	<h1 id="productFilter" itemprop="name">Dog Store</h1>
	</div>
    <p>Find a consolidated list of Dog Products ranging from toys to bowls to everything that falls in between by scrolling below. One of the most extensive collection of pet products in the nation. We at DogSpot aim to serve every pet owner of the country with all the demands their pets pose to them. </div>
  </p>
</div>
<div class="breadcrumb" >
  <div class="cont980">
    <div class="breadcrumb_cont"><span> <a href="/">Home</a></span> <Span>></Span> <span class="brd_font_bold">Dog Store</span> </div>
    <div class="cb"></div>
  </div>
</div>

<!-- breadcrumb -->
<?php /*?><div style="background:url(/banners/gosf-index.png);width:980px;height:281px;float:left;" ></div><?php */?>
<!-- main container -->

<div class="cont980"> 
      <div class="puppies-top-sec">


<div class="header-banner" >
<?php require_once($DOCUMENT_ROOT . '/new/header-banner.php'); ?>
</div>
</div> 
  <!-- three column approach -->
  <div id="login-box" class="login-popup" style="padding:0; box-shadow:none; border:0px solid #fff;background: rgba(255, 255, 255, 0);">
    <?	if($userid=='Guest'){ 
	//require_once($DOCUMENT_ROOT.'/Recommend-Product/not_logged_in.php'); 
	}else{
	//require_once($DOCUMENT_ROOT.'/Recommend-Product/select_breed_2.php');
	} ?>
  </div>
  <a href="#login-box" class="login-window"></a> 
  <!-- column 1 -->
  
  <form id="formFilter" name="formFilter" method="post" action="">
    
    <!---Food Categories--->
    <div class="filterContainer fl comment" id="comment" >
      <div class="filet-img-sec"><img src="/new/pix/cat-drop-down.png"></div>
      <h4 class="browse-cat">Browse by Categories</h4>
      <div class="filterContainer_wrapper">
        <div class="filterCategory browse-cat-fl" >
          <?
                   $queryItem=query_execute("SELECT * FROM `shop_category` where category_parent_id = 0 AND category_status = 'active' AND domain_id = '1' AND category_id NOT IN('105','75','174','29','227','173','172') ORDER BY `cat_display_order` ASC" );
                   while($categoryid = mysql_fetch_array($queryItem)){
                       
                       if($categoryid['category_nicename'] == 'others-health'){ ?>
          <div class="filterCategoryList"><a alt="<?=$categoryid['category_name']?>" title="<?=$categoryid['category_name']?>" href="/<?=$categoryid['category_nicename']?>/">Health Care</a></div>
          <? }
                       else if($categoryid['category_nicename'] == 'others-dog'){ ?>
          <div class="filterCategoryList"><a alt="<?=$categoryid['category_name']?>" title="<?=$categoryid['category_name']?>" href="/<?=$categoryid['category_nicename']?>/">Pet Lovers Gallery</a></div>
          <? }                       
                       else{ ?>
          <div class="filterCategoryList"><a alt="<?=$categoryid['category_name']?>" title="<?=$categoryid['category_name']?>" href="/<?=$categoryid['category_nicename']?>/">
            <?=$categoryid['category_name']?>
            </a></div>
          <? } } ?>
        </div>
      </div>
    </div>
    <!-- column 1 --> 
    <!-- column 2 --> 
    
    <!---Food Categories Ends--->
    
    <div class="displayContainer fr" style="width:710px;">
    <div >
    <div class="productListing716">
      <?
     $fetchprod=query_execute("SELECT * FROM `shop_category` where category_parent_id = 0 AND category_status = 'active' AND domain_id = '1' AND category_id NOT IN('105','75','174','29','227','173','172') ORDER BY `cat_display_order` ASC");     
     while($productdetail = mysql_fetch_array($fetchprod)){
      //  $productlist = query_execute("SELECT * FROM `shop_category` where category_parent_id = '".$categoryid['category_id']."' AND category_id NOT IN('74','92','105','36','83','29','61','102','189','89','190','49','164','167','60','76','77','162','163','40','199','225','66','206','208','212','214','215','216','217','153','114','166','111','109','124','159','120','121','128','129','130','131','132','133','134','156','169','138','158','152','228','227') AND category_status = 'active' ");
     //   while($productdetail = mysql_fetch_array($productlist)){  
		$qItem_count = query_execute_row("SELECT count(i.item_id) as ttcout FROM shop_item_category as c, shop_items as i WHERE c.category_id = '".$productdetail['category_id']."' AND i.visibility='visible' AND i.item_display_status!='delete' AND i.stock_status='instock' AND i.item_id=c.item_id AND i.type_id='simple' ORDER BY i.item_display_order ASC LIMIT 4"); 
		if($qItem_count['ttcout']!='0'){
     ?>
      <h2 class="catpage_catenamebox"><a class="catpage_catname" alt="<?=$productdetail['category_name']?>" title="<?=$productdetail['category_name']?>" href="/<?=$productdetail['category_nicename']?>/">
        <?=$productdetail['category_name']?>
        <span>View All</span>
        </a>
        <?php /*?> <a href="/<?=$productdetail['category_nicename']?>/" class="cat_viewallproduct">view more ></a></span><?php */?>
      </h2>
      <ul class="box-product" >
        <? 		
	 $qItem = query_execute("SELECT DISTINCT(i.item_id), i.name, i.nice_name, i.price, i.selling_price, i.item_parent_id,i.stock_status FROM shop_item_category as c, shop_items as i WHERE c.category_id = '".$productdetail['category_id']."' AND i.visibility='visible' AND i.item_display_status!='delete' AND i.stock_status='instock' AND i.item_id=c.item_id AND i.type_id='simple' ORDER BY i.item_display_order ASC LIMIT 3");  
	while($rowItem = mysql_fetch_array($qItem)){
	$item_id=$rowItem["item_id"];
	$name=stripslashes($rowItem["name"]);
	$nice_name=$rowItem["nice_name"];
	$price=$rowItem["price"];
	$selling_price=$rowItem["selling_price"];
	$item_parent_id=$rowItem["item_parent_id"];
	$stock_status=$rowItem["stock_status"];
	$qdataM1=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	$countM=mysql_num_rows($qdataM1);
	if($countM>'0'){
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	}else{
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
	}
	$rowdatM = mysql_fetch_array($qdataM);
	
	if($rowdatM["media_file"]){
		$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL='/imgthumb/150x160-'.$rowdatM["media_file"];
	}else{
		$src = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
		$imageURL='/shop/image/no-photo-t.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'150','160','ratiowh');
	
	?>
        <li>
          <? 
					  $dcnt=number_format(100-(($price/$selling_price)*100));
					  if($dcnt!=0){ ?>
          <span class="precent_box"><? echo number_format(100-(($price/$selling_price)*100))?>% </span>
          <? } ?>
          <a href="/<?=$nice_name?>/" ><img src="<?=$imageURL?>" alt="<?=$name?>" title="<?=$name?>"/></a>
          <h3> <a href="/<?=$nice_name?>/" alt="<?=$name?>" title="<?=$name?>">
            <?=substr($name,0,70);?>
            </a></h3>
          <div class="priece_precent" >
            <? if($selling_price>$price){ ?>
            <del><? echo 'Rs. '.number_format($selling_price); ?></del>
            <? } ?>
            <span>Rs.
            <?=number_format($price)?>
            </span> </div>
          <div  class="ds_buy_now_cont"><a href="/<?=$nice_name?>/" class="link ds_buy_now_btn" unbxdattr="AddToCart" unbxdparam_sku="<?=$item_id?>" rel='nofollow'>Buy Now</a></div>
        </li>
        <? $ci++; }?>
      </ul>
   
      <? }?>
      <?
          //  }
        }
        ?>
    </div>
    <div class="container pageBody">
      <div class="wrap" style="width:99.5%">
        <div style="width:99.5%">
          <div style="text-align:justify"></div>
        </div>
      </div>
      <div class="read-more" >
        <p>Welcome to the DogSpot Dog Store! This is your one-stop shop for all puppy and dog products. Here we have a huge inventory for you to choose from, and every day we put endless efforts to add more and more relevant and quality matter in this for you to shop. To make a start, how about buying some toys for your little one? Engage him into fun activities by choosing the correct toys for him from our widespread selection of dog toys, and keep him away from boredom. We promise that just having a glance on our product choices, will make you fall in love with us!</p>
        <p>From dog kennels to food, grooming products to toys, bowls to clothes, our listed categories encompass almost everything that your dog may need ever in his lifespan. Just like you, a lot can be said about your dog’s personality by the stuff he uses every day. So pamper him with the array of toys we have got at your disposal! Select some cool ones like rope toys, squeaky toy, Kong toys etc. from our toys section. And while you spoil him, keep him on a bay using our training products like leashes, harnesses, and other training and behavioural aids and so on. We have a range of every day utility items also like bowls and feeders, variety of dog food and treats, regular and fancy grooming products, beds, mattresses and more. Apart from all these, we have got some nice clothes and accessories too for the budding little fashionista in your pooch.</p>
        <p>While procuring all these products and making them available to you, we take special care of three things- utmost quality of the product, comfort of your dog, and ease of handling of these items for you. We take care that whatever goes into your household has to be safe and prove its completecworth. All our products are available in variants which suit dogs of different breeds and sizes, and are improvised as per customer feedback we keep getting every day. Our dog store is a big dog supermarket available on a single click at your web screen!</p>
      </div>
    </div>
    <div class="cb"></div>
  </form>
  <!-- column 2 --> 
</div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
