<?php 

$emailTemplate = '<table border="0" align="center" cellspacing="1" cellpadding="1" width="660">
    <tbody>
        <tr>
            <td bgcolor="#FFFFFF"> 
                <table border="0" align="center" cellspacing="0" cellpadding="0" width="660"> 
					<tbody>
						<tr> 
					        <td bgcolor="#f3f3f3" height="30" width="6"> </td> 
					        <td bgcolor="#f3f3f3" width="479" style="font-family:Arial,Helvetica,sans-serif;font-size:15px;color:#2d2d2d"> Order No.: <strong>'.$orderid.'</strong> </td> 
					<td bgcolor="#f3f3f3" width="175" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#2d2d2d"><strong>Order Date: '.date("d-m-Y").'</strong> </td> 
					    </tr> 
					    <tr> 
					        <td height="28"> </td> 
					        <td height="30" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#2d2d2d" colspan="2"> <strong>Payment Mode: '.$AOrderMethod[$arrayEmail['mod']].'</strong> </td> 
					    </tr> 
					    <tr> 
					    	<td colspan="3"> 
					        <table border="0" align="center" cellspacing="0" cellpadding="0" width="649"> 
					            <tbody>
					                <tr style="font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:bold;color:#2d2d2d"> 
					                    <td bgcolor="#F3F3F3" height="25" width="8"> </td> 
					                    <td bgcolor="#F3F3F3" width="178"> Product Name </td> 
					                    <td bgcolor="#F3F3F3" width="75"> MRP </td> 
					                    <td bgcolor="#F3F3F3" width="90"> Discount </td> 
					                    <td bgcolor="#F3F3F3" width="78"> Price </td> 
					                    <td bgcolor="#F3F3F3" width="30"> Qty </td>                         
					                    <td bgcolor="#F3F3F3" width="56"> Sub Total</td> 
					                </tr>';
		$total_discount=0; $grand_mrp=0; 
		
		while($order_result_row = mysql_fetch_array($order_query_cart)){ 
			$grand_total = $grand_total + $order_result_row['subtotal'];
		    $total_discount= $total_discount + $order_result_row['discount'];
			$grand_mrp = $grand_mrp + $order_result_row['mrp_price'];                
					                
			$emailTemplate .= '<tr style="font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#2d2d2d">
					                    <td height="30"></td>
					                    <td height="30"><a href="https://www.dogspot.in/'.$order_result_row['nice_name'].'" target="_blank" title="'.$order_result_row['name'].'"></a>'.$order_result_row['name'].'</a></td>
					                    <td height="30">'.number_format($order_result_row['mrp_price'],2).'</td>
					                    <td height="30">'.number_format($order_result_row['discount'],2).'</td>
					                    <td height="30">'.number_format($order_result_row['item_price'],2).'</td>
					                    <td height="30">'.$order_result_row['item_qty'].'</td>                        
					                    <td height="30">'.number_format($order_result_row['subtotal'],2).'</td>
					                </tr>';
		} 

		$emailTemplate .= '<tr>
					                    <td bgcolor="#D6D6D6" height="1" colspan="8"></td>  
					                </tr> 
					            </tbody>
							</table> 
					     	</td>
						</tr> 
					    <tr> 
					        <td> </td> 
					        <td colspan="2"> 
					            <table border="0" align="center" cellspacing="0" cellpadding="0" width="642"> 
					                <tbody>
					                    <tr> 
					                        <td height="96" width="405"> 
												<table border="0" align="left" cellspacing="0" cellpadding="0" width="403"> 
												    <tbody>
												        <tr> 
												            <td align="left" height="0" width="403" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:bold;color:#2d2d2d"> Your Order would be Shipped to: </td> 
												        </tr> 
												        <tr> 
												            <td height="70" style="font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#2d2d2d">'.$arrayEmail['ship_name'].'<br />'.$arrayEmail['ship_address1'].'&nbsp;'.$arrayEmail['ship_address2'].'<br />'.$arrayEmail['ship_city'].' '.$arrayEmail['ship_state'].'<br />'.$arrayEmail['ship_pin'].'</td> 
												        </tr> 
												    </tbody>
												</table> 
					                        </td> 
					                        <td bgcolor="#D6D6D6" width="1"> </td> 
					                        <td align="left" width="232" valign="top"> 
					                            <table border="0" align="left" cellspacing="0" cellpadding="0" width="232"> 
					                                <tbody>                                    
					                                    <tr> 
					                                        <td bgcolor="#D6D6D6" height="1" colspan="4"> </td> 
					                                    </tr>  
					                                    <tr>
						                                    <table>
					                                    		<tbody>
						                                    		<tr>
					                                    				<td height="25" width="232"> <span style="font-family:Arial,Helvetica,sans-serif;font-size:14px;font-weight:bold;color:#2d2d2d">Total MRP.</span> </td> 
					                                        			<td height="25"> Rs.'.$grand_mrp.' </td>
					                                    			</tr>
					                                    			<tr>
					                                    				<td height="25" width="232"> <span style="font-family:Arial,Helvetica,sans-serif;font-size:14px;font-weight:bold;color:#2d2d2d">Total Discount: </span> </td> 
					                                        			<td height="25"> <span>Rs.'.$total_discount .'</span> </td> 
					                                    			</tr>
					                                    			<tr>
					                                    				<td height="25" width="232"> <span style="font-family:Arial,Helvetica,sans-serif;font-size:14px;font-weight:bold;color:#2d2d2d">Net Payment: </span> </td> 
					                                        			<td height="25"> Rs.'.$grand_total.' </td>
					                                    			</tr>
					                                    		</tbody>
					                                    	</table>  
					                                    </tr> 
					                                </tbody>
					                            </table> 
					                    	</td> 
					                    </tr> 
					                </tbody>
					            </table> 
					        </td> 
					    </tr> 
					</tbody>
				</table>
			</td> 
		</tr> 
	</tbody>
</table>';

echo $emailTemplate;


?>