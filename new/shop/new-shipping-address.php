<?php
error_reporting(0);
$DOCUMENT_ROOT = '/home/dogspot/public_html';
require_once('../../constants.php');
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT . '/arrays.php');
$sitesection = 'shop';
$ant_section = 'Shop';
$page        = 'shipping';
$ant_page    = 'Billing Shipping';
$session_id  = session_id();
if ($userid == 'Guest') {
    header("Location: /new/shop/new-shipping.php");
    exit();
}
if ($_POST['submit'] == 'Update Address') {
    $address_name_edit    = $_POST['address_name_edit'];
    $address_zip_edit     = $_POST['address_zip_edit'];
    $address_phone_edit   = $_POST['address_phone_edit'];
    $address_address_edit = str_replace('%0D%0A','\r\n',$_POST['address_address_edit']);
    $address_city_edit    = $_POST['address_city_edit'];
    $address_land_edit    = str_replace('%0D%0A','\r\n',$_POST['address_land_edit']);
    $address_state_edit1  = $_POST['address_state_edit1'];
    if ($address_state_edit1) {
        $select_state   = query_execute_row("SELECT * FROM state WHERE state_name='$address_state_edit1'");
        $state_id       = $select_state['state_id'];
        $state_nicename = $select_state['state_nicename'];
        $state_name     = $select_state['state_name'];
    } else {
        $state          = explode(';', $address_state_edit);
        $state_id       = $state[0];
        $state_nicename = $state[2];
        $state_name     = $state[1];
        
    }
    $delete = query_execute("UPDATE shop_order_more_address SET address_name='$address_name_edit',address_address1='$address_address_edit',address_city='$address_city_edit',address_phone1='$address_phone_edit',address_zip='$address_zip_edit',address_address2='$address_land_edit',address_state='$state_name',address_state_id='$state_id',address_state_nicename='$state_nicename' WHERE more_address_id='$address_id_edit'");
}
?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta http-equiv="X-UA-Compatible" content="IE=Edge"> 
<title>Shipping Address</title> 
<meta name="keywords" content="Shipping Address | DogSpot" /> 
<meta name="description" content="Shipping Address | DogSpot" /> 
<?php
if ($userid != 'Guest') {
    $u_email = useremail($userid);
    
}
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?> 
<script> 
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); 

  ga('create', 'UA-1552614-5', 'auto'); 
  ga('send', 'pageview'); 
 function check() 
{   
    var citystate=$('#citystate').val(); 
    if(citystate!='') { 
    $('#csdetail').css('display','block'); 
    var a = citystate.split(",") // Delimiter is a string 
    var city=a[0]; 
    var state=a[1]; 
    $('#address_city').val(city); 
    $('#address_state').val(state); 
    if(city=='' || state==''){ 
    $("#address_city").attr("readonly", false); 
    $("#address_state").attr("readonly", false); 
    }else{ 
    $("#address_city").attr("readonly", false); 
    $("#address_state").attr("readonly", true); 
    } 
    } 
     
} 
 function checkonpin() 
{ 
    var pin = $('#address_zip').val(); 
        var count=pin.length; 
        if(count==6){ 
        ShaAjaxJquary('checkpincode.php?address_zip='+pin+'', '#citystate23', '','', 'GET', '#citystate23', '', 'REP'); 
        ShaAjaxJquary('/shop/checkcod2.php?pin='+pin+'', '#codbox2', '', '', 'GET', '#codbox2', '', 'REP'); 
        setTimeout('forshowpin()',2000); 
         
    } 
} 

function forshowpin() 
{ 
var codstatus = $('#codstatus').val(); 
var NOnarea=$('#codbox2').text();
if(codstatus=='1') 
{ 

document.getElementById('beforepincode').style.display='none'; 
document.getElementById('beforewrongpincode').style.display='none'; 
document.getElementById('afterrightpincode').style.display='inline'; 
setTimeout('check()',1000); 
} 
else if(codstatus=='0') 
{ 
document.getElementById('beforepincode').style.display='none'; 
if(NOnarea==''){
document.getElementById('beforewrongpincode').style.display='inline'; 
}
document.getElementById('afterrightpincode').style.display='none'; 
setTimeout('check()',1000); 
}else 
{ 
document.getElementById('beforepincode').style.display='inline'; 
document.getElementById('beforewrongpincode').style.display='none'; 
document.getElementById('afterrightpincode').style.display='none'; 
setTimeout('check()',1000); 
} 
} 
function checkonpinedit() 
{ 
        var pin = $('#address_zip_edit').val(); 
        var count=pin.length; 
        $('#address_state_edit').css("display","block"); 
        $('#address_state_edit1').css("display","none"); 
         $('#address_state_edit1').val(''); 
        $('#address_state_edit').prop( "required","true" );     
       $('#address_state_edit1').removeAttr('required'); 
        if(count==6){ 
        ShaAjaxJquary('checkpincode.php?address_zip='+pin+'', '#citystate34', '','', 'post', '#citystate34', '', 'REP'); 
        setTimeout('check2()',2000); 
    } 
} 
function valid1(f) { 
     
    f.value = f.value.replace(/[0-9\/!@#$&%^*()-+.:;,'"\><]/ig,''); 
} 
function check2() 
{   
    var citystate=$('#citystate').val(); 
    if(citystate!='') { 
    var a = citystate.split(",") // Delimiter is a string 
    var city=a[0]; 
    var state=a[1]; 
    $('#address_city_edit').val(city); 
    $('#address_state_edit').val(state); 
    if(city=='' || state==''){ 
    $("#address_city_edit").attr("readonly", false); 
    $("#address_state_edit").attr("readonly", false); 
    }else{ 
    $("#address_city_edit").attr("readonly", false); 
    $("#address_state_edit").attr("readonly", true); 
    } 
    } 
     
} 
function show_ship_detail(divName,divNameHide) 
{ 
    var ship_d=document.getElementById(divName).style.display; 
    if(ship_d=='none' && divName !='showAddress') 
    { 
        document.getElementById(divName).style.display='inline'; 
        document.getElementById(divNameHide).style.display='none'; 
    } 
    if(divName=="showAddress" && document.getElementsByClassName('imgtxtcontwag').length !== 0) 
    { 
        document.getElementById(divName).style.display='inline'; 
        document.getElementById(divNameHide).style.display='none';     
    } 
} 
</script> 
<script type="text/javascript"> 

var cat_nice=''; 
 $(document).ready(function() {  
 var addA=$('#addAddress').css('display'); 
 var editA=$('#editAddress').css('display'); 
 if(addA=='none' && editA=='none'){ 
 cat_nice=document.getElementById('txt1').innerHTML; 
 var c=0; 
 countr=document.getElementById('txt2').innerHTML; 
 $(window).data('ajaxready', true).scroll(function(e) { 
    if ($(window).data('ajaxready') == false) return; 
    if(($(document).height() - $(window).height()) - $(window).scrollTop() < 200) {     
    $('#loadMoreComments').show(); 
    $(window).data('ajaxready', false); 

    $.ajax({ 
    cache: false, 
    dataType : "html" , 
    contentType : "application/x-www-form-urlencoded" , 
    url: "/new/shop/loadmoreAddress.php?lastComment="+ $(".imgtxtcontwag:last").attr('id') , 
    data: {cat_nicename:cat_nice} , 
    success: function(html) { 
    if(html){         
    $("#rytPost_list").append(html); 
    $('#loadMoreComments').hide();c=c+1; 
    }else { 
    $('#loadMoreComments').html(); 
    } 
    $(window).data('ajaxready', true); 
    } 
    }); // ajex close 
 
 // c condition close 
    } 

    }); 
 } 
    }); 
  
    function pick_address(address_id) 
    { 
    $('#address_id').val(address_id); 
    document.ship_address.submit();     
    } 
    function delete_address(del_id,add_id) 
    { 
    var confoim=confirm("Are you sure you want to delete this address?"); 
    var userid23=$('#user_id12').val(); 
    if(confoim==true) 
    { 
    ShaAjaxJquary('delete_address1.php?address_id='+del_id+'&userid='+userid23+'', '#new_address', '','', 'post', '#new_address', '', 'REP'); 
    $('#deletedinfo').show(); 
    $('#'+add_id).remove(); 
    } 
     
     } 

    function edit_address(del_id,add_id) 
    { 
    var userid23=$('#user_id12').val(); 
    ShaAjaxJquary('edit_address1.php?address_id='+del_id+'&userid='+userid23+'', '#'+add_id, '','', 'post', '#'+add_id, '', 'REP'); 
    } 
    function changeType() 
    { 
    $('#address_state_edit').css("display","block"); 
    $('#address_state_edit1').css("display","none"); 
    $('#address_state_edit1').val(''); 
    $('#address_state_edit').prop( "required","true" );     
    $('#address_state_edit1').removeAttr('required');     
    checkonpinedit();     
    } 
</script> 
<?php
require_once($DOCUMENT_ROOT . '/new/common/header1.php');

$checkuserr   = query_execute_row("SELECT count(*) as moreadd1 FROM shop_order_more_address WHERE userid='$userid' AND address_name!='' AND address_address1!='' AND address_city!='' AND address_state!='' AND address_phone1!='' ORDER BY more_address_id");
$checkAddress = query_execute("SELECT * FROM shop_order_more_address WHERE userid='$userid' AND address_name!='' AND address_address1!='' AND address_city!='' AND userid !=''  AND address_state!='' AND address_phone1!='' ORDER BY more_address_id DESC LIMIT 3");
if ($checkuserr['moreadd1'] > 0) {
?> 

<div class="container" id="showAddress"> 
  <div class="checkou-sec"> 
    <h1>Where do you want us to deliver?</h1> 
    <?php
    if ($_POST['submit']) {
?> 
  
    <div id="deletedinfoa" class="delTxtPop_ship succes-message">The address has been successfully updated.</div> 
     
    <?php
    }
?> 
  <?php
    /*?>
    <div id="deletedinfo" class="delTxtPop_ship succes-message" style="display: none;">The address has been successfully deleted.</div><?php */
?> 
  
    <!-- check out login start--> 
    <div class="new-login-form">  
      <!-- email--> 
      <form method="post" name="ship_address" id="ship_address" action="/new/shop/new-payment.php"> 
        <div id="new_address"></div> 
                <?php
if ($_GET['pincodeNot'] == 'no') {
?> 
     <div id="info" class="delTxtPop_ship login-error" >Sorry, We do not offer shipping facility in your area</div>
        <?php
}
?>
        <div class="new-login-form-details" id="rytPost_list">  
          <!-- address book--> 
          <input type="hidden" name="address_id" id="address_id" value="" /> 
          <?php
    $iid = 0;
    while ($addressAdd = mysql_fetch_array($checkAddress)) {
?> 
         <div id="txt1"></div> 
          <div id="txt2" style="display:none"> 
            <?= $checkuserr['moreadd1'] ?> 
         </div> 
          <div class="address-book imgtxtcontwag" id='<?= $iid ?>'> 
            <div class="cs-full-name"> 
              <?= $addressAdd['address_name'] ?> 
           </div> 
            <input type="hidden" name="shipping_address" id="shipping_address" value="address" /> 
            <input type="hidden" id="us_email" name="us_email" value="<?= $addressAdd['address_email'] ?>" /> 
            <input type="hidden" name="us_id" id="us_id" value="<?= $userid; ?>" /> 
            <div class="address-delete"> <a href="#" onclick="edit_address('<?= $addressAdd['more_address_id'] ?>','<?= $iid ?>')"><img src="/checkout/images/edit.jpg" style="margin-right:3px" /></a> <a href="#" onclick="delete_address('<?= $addressAdd['more_address_id'] ?>','<?= $iid ?>')"><img src="/checkout/images/delete-icons.png" /></a> </div> 
            <div class="cs-address"> 
              <?= stripallslashes(str_replace('\r\n',' ',$addressAdd['address_address1'])); ?> 
             <?= $addressAdd['address_city'] ?> 
             <p> PH: 
                <?= $addressAdd['address_phone1'] ?> 
             </p> 
            </div> 
            <div class="contuinue-btn"> 
              <input  class="btn" id="deliver_address"   type="button" onclick="pick_address('<?= $addressAdd['more_address_id'] ?>')" value="Deliver to this address" /> 
            </div> 
          </div> 
          <?php
        $iid++;
    }
?> 
       </div> 
      </form> 
      <!-- email details end-->  
      <!-- or line--> 
      <div class="step1_dwd"><img src="/checkout/images/or-line-2.png" /></div> 
      <!-- or line end-->  
      <!-- social login--> 
      <div class="new-login-form-social"> 
        <div class="pick-address-book"><a href="#" onclick="show_ship_detail('addAddress','showAddress');"> 
          <input class="p-btns" type="button" value="Add a new delivery address"  /> 
          </a></div> 
      </div> 
      <!-- social login-->  
    </div> 
    <!-- check out end-->  
  </div> 
</div> 
<?php
}
?> 
<div class="container" id="addAddress" <?php
if ($checkuserr['moreadd1'] != 0) {
?>style="display:none"<?php
}
?> /> 
<div class="checkou-sec"> 
  <h1>Where do want us to deliver?</h1> 
  <!-- check out login start--> 
  <div class="new-login-form">  
    <!-- email--> 
    <div class="new-login-form-details"> 
      <form action="/new/shop/new-payment.php" name="formr" id="formr" method="post"> 
        <?php
if ($_GET['pincodeNot'] == 'no') {
?> 
       <div id="info" class="delTxtPop_ship" >Sorry, We do not offer shipping facility in your area</div> 
        <?php
}
?> 
       <div class="email-text-text">Full Name</div> 
        <div class="email-text-box"> 
          <input  type="text" required="required" autocomplete="off" name="address_name" id="address_name" placeholder="Full name" /> 
        </div> 
        <input type="hidden" id="us_email" name="us_email" value="<?= $u_email ?>" /> 
        <input type="hidden" name="us_id" id="us_id" value="<?= $userid; ?>" /> 
        <div class="email-text-text">Mobile number</div> 
        <div class="email-text-box"> 
          <input  type="text" onKeyUp="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="13" minlength="10" name="address_phone"  autocomplete="off"  required="required"  
id="address_phone" placeholder="Mobile number" /> 
        </div> 
        <div class="email-text-text">Pincode</div> 
        <div class="email-text-box"> 
          <input  type="text" onKeyUp="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"   
 name="address_zip" id="address_zip" autocomplete="off" onblur="checkonpin();"  maxlength="6" minlength="6" required="required" placeholder="Pincode" style="display:block;" /> 
          <div id="citystate23" style="display:inline-block"></div> 
          <div id="codbox2" style="display:inline-block"></div> 
          <span style="color:#666;display:inline" id="beforepincode"></span> 
          <div style="display:none" id="afterrightpincode" class="succes-message"> 
          Cash On Delivery available for this area</div>  
          <span style="color:#666;display:none" id="beforewrongpincode"><img src="images/img/cls.png" width="16" height="16" alt="" style="float:left; margin-right:3px;" />Cash On Delivery is not available for this area</span></div> 
        <div class="email-text-text">Address</div> 
        <div class="email-text-box"> 
          <textarea cols="40" rows="6" required="required" autocomplete="off"  name="address_address"  id="address_address" placeholder="Address"></textarea> 
        </div> 
        <div class="email-text-text">Landmark</div> 
        <div class="email-text-box"> 
          <input  type="text" autocomplete="off"  name="address_address2"  id="address_address2" placeholder="Landmark" /> 
        </div> 
        <div id="csdetail" style="display:none"> 
          <div class="email-text-text">City</div> 
          <div class="email-text-box"> 
            <input  type="text" name="address_city"   id="address_city" required="required" placeholder="City" /> 
          </div> 
          <div class="email-text-text">State</div> 
          <div class="email-text-box"> 
            <select  required="required"  name="address_state" id="address_state" > 
              <option value="" class="required">Select State</option> 
              <?php
$queryState = mysql_query("SELECT * FROM state ORDER BY state_name ASC");
if (!$queryState) {
    die(mysql_error());
}
while ($rowState = mysql_fetch_array($queryState)) {
    
    $state_id_new   = $rowState["state_id"];
    $state          = $rowState["state_name"];
    $state_nicename = $rowState["state_nicename"];
    
    print "<option value='$state_id_new;$state;$state_nicename'";
    if ($state_id == "$state_id_new") {
        echo "selected='selected'";
    }
    print ">$state</option>";
}
?> 
           </select> 
          </div> 
        </div> 
        <input type="hidden" name="address_id" id="address_id" value="" /> 
        <input type="hidden" name="user_id12" id="user_id12" value="<?= $userid ?>" /> 
        <input type="hidden" name="session_id" id="session_id" value="<?= $session_id ?>" /> 
         
        <div id="editsection"></div> 
        <div id="citystate34" style="display:none"></div> 
        <input type="hidden" id="us_email" name="us_email" value="<?= $u_email; ?>" /> 
        <input type="hidden" name="us_id" id="us_id" value="<?= $userid; ?>" /> 
        <div id="productleftpanal"></div> 
        <div class="contuinue-btn"> 
          <input  class="btn" type="Submit"  id="proceedToPayment" name="submit"  value="Deliver to this address" /> 
        </div> 
      </form> 
    </div> 
    <!-- email details end-->  
    <?php
if ($checkuserr['moreadd1'] != 0) {
?> 
   <div id="addSocial"> 
    <!-- or line--> 
    <div class="step1_dwd"><img src="/checkout/images/or-line-2.png" /></div> 
    <!-- or line end-->  
    <!-- social login--> 
    <div class="new-login-form-social"> 
      <div class="pick-address-book"><a href="#" onclick="show_ship_detail('showAddress','addAddress');"> 
        <input class="p-btns" type="button" value="Pick from your address book"  /> 
        </a></div> 
    </div> 
    <!-- social login--> 
    <?php
}
?> 
   </div>  
  </div> 
  <!-- check out end-->  
</div> 
</div> 
<div class="container" id="editAddress" style="display:none"> 
  <div class="checkou-sec"> 
    <h1>Where do want us to deliver?</h1> 
    <!-- check out login start--> 
    <div class="new-login-form">  
      <!-- email--> 
      <div class="new-login-form-details"> 
        <form action="/new/shop/new-shipping-address.php" name="formr1" id="formr1" method="post"> 
          <input type="hidden" name="address_id_edit" id="address_id_edit" value="" /> 
          <div class="email-text-text">Full Name</div> 
          <div class="email-text-box"> 
            <input  type="text" required="required" name="address_name_edit" id="address_name_edit" placeholder="Full name" /> 
          </div> 
          <div class="email-text-text">Mobile number</div> 
          <div class="email-text-box"> 
            <input  type="text" onKeyUp="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="13" onblur="check();" minlength="10" name="address_phone_edit"  autocomplete="off"  required="required"  
id="address_phone_edit" placeholder="Mobile number" /> 
          </div> 
          <div class="email-text-text">Pincode</div> 
          <div class="email-text-box"> 
          <input  type="text" onKeyUp="checkonpinedit();if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"   
 name="address_zip_edit" id="address_zip_edit" autocomplete="off"  onblur="checkonpinedit();" onchange="checkonpinedit();" maxlength="6" minlength="6" required="required" placeholder="Pincode" /> 
          <div id="citystate23" style="display:inline-block"></div> 
          <div id="codbox2" style="display:inline-block"></div> 
          <span style="color:#666;display:inline" id="beforepincode"></span> 
          <div style="color:#666;display:none" id="afterrightpincode" class="succes-message">  
           
          Cash On Delivery available for this area</div> <span style="color:#666;display:none" id="beforewrongpincode"> 
          <img src="images/img/cls.png" width="16" height="16" alt="" style="float:left; margin-right:3px;" /> 
          Cash On Delivery is not available for this area</span> </div> 
          <div class="email-text-text">Address</div> 
          <div class="email-text-box"> 
            <textarea cols="40" rows="6" required="required"  name="address_address_edit"  id="address_address_edit" placeholder="Address"></textarea> 
          </div> 
          <div class="email-text-text">Landmark</div> 
          <div class="email-text-box"> 
            <input  type="text" name="address_land_edit"  id="address_land_edit" placeholder="Landmark" /> 
          </div> 
          <div class="email-text-text">City</div> 
          <div class="email-text-box"> 
            <input  type="text" name="address_city_edit"   id="address_city_edit" required="required" placeholder="City" /> 
          </div> 
          <div class="email-text-text">State</div> 
          <div class="email-text-box"> 
            <input  type="text" name="address_state_edit1" onclick="changeType()"   id="address_state_edit1" required="required" placeholder="City" /> 
            <select style="display:none"  name="address_state_edit" id="address_state_edit" > 
              <option value="">Select State</option> 
              <?php
$queryState = mysql_query("SELECT * FROM state ORDER BY state_name ASC");
if (!$queryState) {
    die(mysql_error());
}
while ($rowState = mysql_fetch_array($queryState)) {
    
    $state_id_new   = $rowState["state_id"];
    $state          = $rowState["state_name"];
    $state_nicename = $rowState["state_nicename"];
    
    print "<option value='$state_id_new;$state;$state_nicename'";
    
    print ">$state</option>";
}
?> 
           </select> 
          </div> 
          <div id="citystate34" style="display:none"></div> 
          <input type="hidden" id="us_email" name="us_email" value="<?= $u_email ?>" /> 
          <input type="hidden" name="address_email_edit" id="address_email_edit" value="<?= $getemail['u_email'] ?>" /> 
          <div id="productleftpanal"></div> 
          <div class="contuinue-btn"> 
            <input  class="btn" type="Submit"  id="UpdatePayment" name="submit"  value="Update Address" /> 
          </div> 
        </form> 
      </div> 
    </div> 
    <!-- check out end-->  
  </div> 
</div> 
<?php

require_once($DOCUMENT_ROOT . '/new/common/bottom1.php');
?> 

