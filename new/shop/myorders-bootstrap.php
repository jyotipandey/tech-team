<?php 
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions-new.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
$sitesection = "shop";
$session_id = session_id();

$status=array("new"=>"Order Placed","pending-dispatch"=>"Order  Confirmed","dispatched-ready"=>"Order Packed","dispatched"=>
"Dispatched","delivered"=>"Delivered","canceled"=>"Cancelled","cancelled"=>"Cancelled","rto"=>"Order Return to Origin");
$title="My Order Details";
$keyword="My Order Details Page";
$desc="DogSpot My Order Details";
 
    $alternate="https://www.dogspot.in/new/shop/myorders.php";
	$canonical="https://www.dogspot.in/new/shop/myorders.php";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='shop';
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/myorders.css" />

<div class="breadcrumbs">
  <div class="container">
    <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
      <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a href="/" itemprop="item"><span itemprop="name">Home</span></a>
        <meta itemprop="position" content="1" />
        </span> <span> / </span> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <span itemprop="item"> <span itemprop="name" class="active-bread">My Orders</span></span>
        <meta itemprop="position" content="2" />
        </span> </div>
    </div>
  </div>
</div>
<?
$qGetMyCart6=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND u_id !='0' AND userid !='Guest' AND mode != 'TEST' AND order_status != 'inc' ORDER BY order_c_date DESC");
$qGetMyCart=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND u_id !='0' AND userid !='Guest' AND mode != 'TEST' AND order_status != 'inc' AND order_c_date BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) AND NOW() ORDER BY order_c_date DESC");
$totrecord = mysql_num_rows($qGetMyCart);
//echo "SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND u_id !='0' AND userid !='Guest' AND mode != 'TEST' AND order_status != 'inc' AND order_c_date BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) AND NOW() ORDER BY order_c_date DESC";
$qGetMyCart10=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND u_id !='0' AND userid !='Guest' AND mode != 'TEST' AND order_status != 'inc' AND order_c_date < DATE_SUB(NOW(), INTERVAL 60 day) ORDER BY order_c_date DESC");

$qGetMyCart12=query_execute("SELECT * FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND u_id !='0' AND userid !='Guest' AND mode != 'TEST' AND order_status != 'inc' AND order_c_date < DATE_SUB(NOW(), INTERVAL 60 day) ORDER BY order_c_date DESC");
$totrecord10 = mysql_num_rows($qGetMyCart12);
?>
<section class="myorder-sec">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div id="tab" class="btn-group btn-group-justified myorder-tabs" data-toggle="buttons"> 
		  <ul>
          <li class="active"><a href="#recentorder" class="btn focus" data-toggle="tab">
          <div>Recent Orders <span>(Last 2 Months)</span></div>
          </a></li>
        <li>  <a href="#pastorder" class="btn" data-toggle="tab">
          <div>Past Orders</div>
          </a></li>
</ul>
          </div>
         </div>
          <div class="col-sm-12">
<div class="tab-content">
     <? if($userid =='Guest'){
	echo '<div class="no_order_box text-center">
	<h2>Please Login to see your orders,You have not placed any orders in last 2 months</h2>';?>
	<div><a href="/" class="redButton cornerRoundAll start_shoppingbtn">Start Shopping</a></div></div>
	<? } elseif($totrecord==0){
		echo '<div class="no_order_box text-center"><h2>You have not placed any orders in last 2 months</h2>';?>

	<div ><a href="/" class="redButton cornerRoundAll start_shoppingbtn">Start Shopping</a></div></div>
		<?

}else{
$s=1;?>
<div class="tab-pane active" id="recentorder"><?
while($rowMyCart = mysql_fetch_array($qGetMyCart)){
$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart["order_id"]."' ORDER BY order_c_date ");
$totalord=$partialdetail['ord'];
$scaanneddetail=query_execute_row("SELECT review_section_id as ord FROM section_reviews WHERE review_name='scanned' AND review_section_id='".$rowMyCart10["order_id"]."' group BY review_section_id ");
 $scannedO=$scaanneddetail['ord'];
$partilord=explode(',',$totalord);
//for incomplete order(Reorder)
$qGetMyCart_re=query_execute_row("SELECT order_id FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND (order_status = 'inc' OR  delevery_status='canceled' OR delevery_status='cancelled')  AND order_id='".$rowMyCart["order_id"]."'");
$reorderid=$qGetMyCart_re['order_id'];
addItem($rowMyCart["order_id"],11210,2000);  ?> 
<div class="myorders-box">
    <div class="myorders-box-header">
       <div class="row"> <div class="col-xs-4 col-sm-3 col-md-2">
        <div class="order-unit myorders-box1">
<div class="myorder_id_btn"><a href="/new/shop/myorder.php?order_id=<?=$rowMyCart["order_id"]?>">Order Id:<?=$rowMyCart["order_id"]?></a></div>
</div></div>
          <div class="col-xs-4  col-sm-3 col-md-2">
          <div class="order-unit myorders-box1">
            <div class="textuc">Ship to</div>
            <? $ship_name=query_execute_row("SELECT address_name FROM shop_order_address WHERE order_id='".$rowMyCart['order_id']."'");?>
            <div><?=$ship_name['address_name'];?></div>
</div>
</div>
 <div class="col-xs-4  col-sm-3 col-md-3">
          <div class="order-unit myorders-box1 pdg-left15">
            <div class="textuc">Total</div>
            <div><i class="fa fa-inr"></i>  
 <? $qGetMyCarttotal=query_execute_row("SELECT count(*) as c FROM shop_cart WHERE cart_order_id = '".$rowMyCart["order_id"]."' ");?>
           <?=number_format($rowMyCart["order_amount"],0)?></div>
</div>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-5 text-right">
              <div class="order-unitr">
<? if($reorderid!=''){?>
<a class="orderIdBtn btn btn-medium btn-blue" href="/new/shop/reorders.php?order_id=<?=$rowMyCart["order_id"]?>">
  <input type="button" value="ReOrder" class="trackorder_btn">
</a>
<? }else{?>
<? if($rowMyCart["delevery_status"]!='delivered' && $rowMyCart["delevery_status"]!='cancelled' && $rowMyCart["delevery_status"]!='canceled'){?>
           <a class="orderIdBtn btn btn-medium btn-blue"  href="/new/shop/myorder.php?order_id=<?=$rowMyCart["order_id"]?>#track_order"><input type="button" value="Track Order" class="trackorder_btn"></a><? }?>
           <? if(($rowMyCart["delevery_status"]=='new' || $rowMyCart["delevery_status"]=='pending-dispatch') &&  $scannedO=='0'){?>
           <a href="#login-box<?=$rowMyCart["order_id"]?>" class="login-window"><input type="button" value="Cancel Order" class="cancel_item_btn"></a>
           <? }?>
  <? }?>
  </div>
              </div>
        </div>
          </div>
       <?
$qGetMyCart1=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart["order_id"]."' ");	
	while($rowMyCart1 = mysql_fetch_array($qGetMyCart1)){
		$item_id = $rowMyCart1["item_id"];
		$vendor_id = $rowMyCart1["vendor_id"];
		$cart_id=$rowMyCart1["cart_id"];
		$selectstar=mysql_query("SELECT * FROM vendor_rating WHERE userid='$userid' AND item_id='$item_id'");
		$mysql=mysql_num_rows($selectstar);
		$qdata=query_execute("SELECT name, price, weight, item_parent_id,item_status,stock_status,item_display_status,nice_name FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		//if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart1["item_totalprice"]+$ItemGtotal;
		$item_parent_id=$rowdat["item_parent_id"];
		$weight=$rowdat["weight"];
		if($weight=='')
		{
		$selectstarweight=query_execute_row("SELECT * FROM shop_item_measurement WHERE item_id='$item_id'");	
		$weight=$selectstarweight['weight'];
		}
		// Get Tital option
		$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];
		// END
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		$sqlmediaco = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
$trecount=mysql_fetch_array($sqlmediaco);   
    if ($trecount['trecd'] != '0') {
		//echo "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC";
	$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
}else{
	$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY 
	position ASC");
}	
		$rowdatM = mysql_fetch_array($qdataM);
		$imglinkm='https://ik.imagekit.io/2345/tr:h-100,w-100/shop/item-images/orignal/'.$rowdatM['media_file'];
		if($rowdat["item_parent_id"]!=0){
			$rownicename=query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
			$nice_name=$rownicename["nice_name"];
		}else{
			$nice_name=$rowdat["nice_name"];
		}	
	?>        
        <!--from shop cart order item detail---->
          <div class="order-list-box">
        <div class="ccol-xs-4  col-sm-3 col-md-2">
        <div class="order_p_img order-unit">
         <img src="<?=$imglinkm?>" alt="<?=$rowdatM["label"]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/> </div>
         </div>
              <div class="col-xs-4  col-sm-3 col-md-2">
               <div class="order_p_info order-unit">
                <div class="order_item_name"><a href="/<?=$nice_name?>/" target="_blank"><?=$rowdat["name"];?></a></div>
                <div class="order_item_price"><i class="fa fa-inr"></i>  <?=$rowMyCart1["item_price"]?></div>
                <div class="order-size-qty"><span>Size: <?=number_format($weight,2)?>gm </span> <span class="order_qty">Qty: <?=$rowMyCart1["item_qty"]?></span></div>
             </div>
              </div>

          <div class="col-xs-4  col-sm-3 col-md-3">
          <div class="order_cont order-unit">
        <div class="">
            <div>Order Placed</div>
            <div><?=showdate($rowMyCart["order_c_date"], "d M y h:m a");?></div>
           </div>
          
             
            </div>
          </div>
            
              <div class="col-xs-12 col-sm-3 col-md-5 text-center">
              <div class="delinfo_nd">
        <? 
$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart["order_id"]."' ORDER BY order_c_date ");
$totalord=$partialdetail['ord'];
$partilord=explode(',',$totalord);
		?>
            <? if($partilord[0]!=''){ echo "Partial Dispatch with new order ids  :</br>". $partilord[0] .' and '. $partilord[1];}else{ echo ucwords($status[$rowMyCart["delevery_status"]]);}?>                
            </div>
          </div>
       
                    <?  if($rowMyCart["delevery_status"]=='new' || $rowMyCart["delevery_status"]=='pending-dispatch'){?>
             <div id="login-box<?=$rowMyCart["order_id"]?>" class="login-popup" style="box-shadow:none; border:8px solid #bfbfbf;">
					<?php  include($DOCUMENT_ROOT.'/new/shop/order-cancel.php'); ?></div>
        <!--End from shop cart order item detail----><? }?>
      </div>         
                    
              
<? }?>
</div>
<? }?>
</div>

<div class="tab-pane" id="pastorder">
<? while($rowMyCart10 = mysql_fetch_array($qGetMyCart10)){
$iid=$iid+1;
$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart10["order_id"]."' ORDER BY order_c_date ");

$scaanneddetail=query_execute_row("SELECT review_section_id as ord FROM section_reviews WHERE review_name='scanned' AND review_section_id='".$rowMyCart10["order_id"]."' group BY review_section_id ");
 $scannedO=$scaanneddetail['ord'];
$totalord=$partialdetail['ord'];

$partilord=explode(',',$totalord);
$qGetMyCart_re_1=query_execute_row("SELECT order_id FROM shop_order WHERE (userid = '$userid' OR u_id='$sessionDPid') AND mode != 'TEST' AND (order_status = 'inc' OR  delevery_status='canceled' OR delevery_status='cancelled')  AND order_id='".$rowMyCart10["order_id"]."'");
$reorderid_1=$qGetMyCart_re_1['order_id'];
?>
<div class="myorders-box">
    <div class="myorders-box-header">
       <div class="row"> <div class="col-xs-4 col-sm-3 col-md-2">
        <div class="order-unit myorders-box1">
<div class="myorder_id_btn"><a href="/new/shop/myorder.php?order_id=<?=$rowMyCart10["order_id"]?>">Order Id:<?=$rowMyCart10["order_id"]?></a></div>
</div></div>
          <div class="col-xs-4  col-sm-3 col-md-2">
          <div class="order-unit myorders-box1">
            <div class="textuc">Ship to</div>
            <? $ship_name=query_execute_row("SELECT address_name FROM shop_order_address WHERE order_id='".$rowMyCart10['order_id']."'");?>
            <div><?=$ship_name['address_name'];?></div>
</div>
</div>
 <div class="col-xs-4  col-sm-3 col-md-3">
          <div class="order-unit myorders-box1 pdg-left15">
            <div class="textuc">Total</div>
            <div><i class="fa fa-inr"></i>  
 <? $qGetMyCarttotal=query_execute_row("SELECT count(*) as c FROM shop_cart WHERE cart_order_id = '".$rowMyCart10["order_id"]."' ");?>
           <?=number_format($rowMyCart10["order_amount"],0)?></div>
</div>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-5 text-right">
              <div class="order-unitr">
<? if($reorderid!=''){?>
<a class="orderIdBtn btn btn-medium btn-blue" href="/new/shop/reorders.php?order_id=<?=$rowMyCart10["order_id"]?>">
  <input type="button" value="ReOrder" class="trackorder_btn">
</a>
<? }else{?>
<? if($rowMyCart10["delevery_status"]!='delivered' && $rowMyCart10["delevery_status"]!='cancelled' && $rowMyCart10["delevery_status"]!='canceled'){?>
           <a class="orderIdBtn btn btn-medium btn-blue"  href="/new/shop/myorder.php?order_id=<?=$rowMyCart10["order_id"]?>#track_order"><input type="button" value="Track Order" class="trackorder_btn"></a><? }?>
           <? if(($rowMyCart10["delevery_status"]=='new' || $rowMyCart10["delevery_status"]=='pending-dispatch') &&  $scannedO=='0'){?>
           <a href="#login-box<?=$rowMyCart10["order_id"]?>" class="login-window"><input type="button" value="Cancel Order" class="cancel_item_btn"></a>
           <? }?>
  <? }?>
  </div>
              </div>
          </div></div>
       <?
$qGetMyCart12=query_execute("SELECT * FROM shop_cart WHERE cart_order_id = '".$rowMyCart10["order_id"]."' ");	
	while($rowMyCart12 = mysql_fetch_array($qGetMyCart12)){
		$item_id = $rowMyCart12["item_id"];
		$cart_order_id = $rowMyCart12["cart_order_id"];
		$vendor_id = $rowMyCart12["vendor_id"];
		$cart_id=$rowMyCart12["cart_id"];
		$selectstar=mysql_query("SELECT * FROM vendor_rating WHERE userid='$userid' AND item_id='$item_id'");
		$mysql=mysql_num_rows($selectstar);
		$qdata=query_execute("SELECT name, price, weight, item_parent_id,item_status,stock_status,item_display_status,nice_name FROM shop_items WHERE item_id='$item_id'");
		$rowdat = mysql_fetch_array($qdata);
		//if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart12["item_totalprice"]+$ItemGtotal;
		$item_parent_id=$rowdat["item_parent_id"];
		$weight=$rowdat["weight"];
		if($weight=='')
		{
		$selectstarweight=query_execute_row("SELECT * FROM shop_item_measurement WHERE item_id='$item_id'");	
		$weight=$selectstarweight['weight'];
		}
		// Get Tital option
		$qOptionID=query_execute("SELECT option_type_id FROM shop_cart_option WHERE cart_id='$cart_id'");
		$rowOptionID = mysql_fetch_array($qOptionID);
		$option_type_id = $rowOptionID["option_type_id"];
		// END
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		$sqlmediaco = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
$trecount=mysql_fetch_array($sqlmediaco);   
    if ($trecount['trecd'] != '0') {
		//echo "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC";
	$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
}else{
	$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY 
	position ASC");
}	
		$rowdatM = mysql_fetch_array($qdataM);
			$imglinkm='https://ik.imagekit.io/2345/tr:h-100,w-100/shop/item-images/orignal/'.$rowdatM['media_file'];
	
		if($rowdat["item_parent_id"]!=0){
			$rownicename=query_execute_row("SELECT nice_name FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
			$nice_name=$rownicename["nice_name"];
		}else{
			$nice_name=$rowdat["nice_name"];
		}	
	?>        
        <!--from shop cart order item detail---->
          <div class="order-list-box">
        <div class="ccol-xs-4  col-sm-3 col-md-2">
        <div class="order_p_img order-unit">
         <img src="<?=$imglinkm?>" alt="<?=$rowdatM["label"]?>" width="100" height="100" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/> </div>
         </div>
              <div class="col-xs-4  col-sm-3 col-md-2">
               <div class="order_p_info order-unit">
                <div class="order_item_name"><a href="/<?=$nice_name?>/" target="_blank"><?=$rowdat["name"];?></a></div>
                <div class="order_item_price"><i class="fa fa-inr"></i>  <?=$rowMyCart12["item_price"]?></div>
                <div class="order-size-qty"><span>Size: <?=number_format($weight,2)?>gm </span> <span class="order_qty">Qty: <?=$rowMyCart12["item_qty"]?></span></div>
             </div>
              </div>

          <div class="col-xs-4  col-sm-3 col-md-3">
          <div class="order_cont order-unit">
        <div class="">
            <div>Order Placed</div>
            <div><?=showdate($rowMyCart10["order_c_date"], "d M y h:m a");?></div>
           </div>
          
             
            </div>
          </div>
            
              <div class="col-xs-12 col-sm-3 col-md-5 text-center">
              <div class="delinfo_nd">
        <? 
$partialdetail=query_execute_row("SELECT group_concat(order_id) as ord FROM shop_order WHERE order_parent_id='".$rowMyCart12["cart_order_id"]."' ORDER BY order_c_date ");
$totalord=$partialdetail['ord'];
$partilord=explode(',',$totalord);
		?>
            <? if($partilord[0]!=''){ echo "Partial Dispatch with new order ids  :</br>". $partilord[0] .' and '. $partilord[1];}else{ echo ucwords($status[$rowMyCart10["delevery_status"]]);}?>                
            </div>
          </div>

      </div>         
                    
              
<? }?>
</div>
                  <? }?>
</div>

<? } ?>
</div></div>
</div></section>
  <?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>