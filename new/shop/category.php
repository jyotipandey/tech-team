<?php
require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
$sitesection = "shop";

$ecomm_prodid = "";
$ecomm_totalvalue = "";
$ecomm_pagetype = 'category';
$session_id = session_id();

//NKS FOR 404
//echo $section[2];
$qdata=query_execute("SELECT * FROM shop_category WHERE category_nicename='$section[2]'");
$rowTotal = mysql_num_rows($qdata);
if($rowTotal==0){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();
}

//END NKS FOR 404


//------------Show
$maxshow = 100;
if(empty($show)) {
	$show = 0;
}else{
 $show = $show - 1;
}
 $showRecord = $show * $maxshow;
 $nextShow = $showRecord + $maxshow;
//-------------Show

// Get Category Details
$qdata=query_execute("SELECT * FROM shop_category WHERE category_nicename = '$section[2]'");
$rowdat = mysql_fetch_array($qdata);
$category_id = $rowdat["category_id"];
if($rowdat["category_nicename"]){
	$pageSection=$rowdat["category_nicename"];
}
// Get Category Details END

// Get SubCatList
$qdataSubCat=query_execute("SELECT * FROM shop_category WHERE category_parent_id = '$category_id'");
$totsubcat = mysql_num_rows($qdataSubCat);
// END SubCat List

//GET PARENT CATEGORY
$qParentCat=query_execute("SELECT category_parent_id FROM shop_category WHERE category_id='".$category_id."'"); //NKS
$rowParentCat = mysql_fetch_array($qParentCat); //NKS
$catParentName=$ArrayShopCategorys[$rowParentCat["category_parent_id"]];
$category_parent_id = $rowParentCat["category_parent_id"];
$qParentCatNice=query_execute("SELECT category_nicename FROM shop_category WHERE category_id='".$category_parent_id."'"); //NKS
$rowParentCatNice = mysql_fetch_array($qParentCatNice); //NKS
$catParentNiceName = $rowParentCatNice["category_nicename"];
if($rowParentCatNice["category_nicename"]){
	$pageSection=$rowParentCatNice["category_nicename"];
}
//echo 'id:'.$category_id.' pid:'.$category_parent_id; //NKS

// END PARENT CATEGORY

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title><?=$rowdat["category_title"]?>, Buy Dog Products Online, Pet Shop, DogSpot</title>
<meta name="keywords" content="<?=$rowdat["category_keyword"]?> Pet Shop, DogSpot" />
<meta name="description" content="<?=$rowdat["category_desc"]?> Pet Shop, DogSpot" />
<? if($show > '0'){?>
<meta name="robots" content="noindex, follow">
<? }?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>

         <div class="cb"></div>
             
	<!-- greenstrip 2 -->  
    	<div class="categoryGreenStrip">
        	<div class="cont980">
               <!-- left navigation -->
              
        	<!-- left navigation -->
                <!-- left container -->
             
                <div class="cont300" style="margin-right:12px;"> <img src="/new/pix/category_greenstrip_topleft.gif" alt="" title=""  />
              </div><!-- left container -->
                
                <!-- right container -->
                <div class="cont660" style="margin-right:0px;">
                <?
				
				if($category_parent_id){
					$imgcaturl='/new/shop/images/category/'.$category_parent_id.'.jpg';
				}else{
					$imgcaturl='/new/shop/images/category/'.$category_id.'.jpg';
				}
				if (!file_exists($DOCUMENT_ROOT.$imgcaturl)) {
					$imgcaturl='/new/shop/images/category/generic.jpg';
				}
				
				if(!$imgcaturl){
					$imgcaturl='/new/shop/images/category/generic.jpg';
				}
				?>
                <img src="<?=$imgcaturl?>" alt="" title=""  />
            </div><!-- right container -->
                <div class="cb"></div>
       		</div>
        </div><!-- greenstrip 2 -->
        
        
    <!-- main container -->
    <div class="cont980">
   	 <div class="vs10"></div>
    
        <!-- category tabs -->
            
            <div class="cb"></div>
         <!-- category tabs -->
         <h1><?=$rowdat["category_name"]?></h1>
         <div class="vs10"></div>
          
    <!--  category products -->     
         <!--  row1 -->
     <ul class="shopslide">
  <?
$ci=1;
if($totsubcat > 0){
	while($rowSubCat = mysql_fetch_array($qdataSubCat)){
		$subcategory_name = $rowSubCat["category_name"];
		$subcategory_id = $rowSubCat["category_id"];
		$subcategory_nicename = $rowSubCat["category_nicename"];
		
		$qItemAllSub=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price FROM shop_item_category as c, shop_items as i WHERE c.category_id = '$subcategory_id' AND i.visibility='visible' AND i.item_id=c.item_id ORDER BY i.item_display_order ASC");
		$totsubcatitem = mysql_num_rows($qItemAllSub);
		if($totsubcatitem > 0){
		$rowItemSub = mysql_fetch_array($qItemAllSub);
		$subitem_id=$rowItemSub["item_id"];
		$subname=$rowItemSub["name"];
		
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$subitem_id' ORDER BY position ASC");
		$rowdatM = mysql_fetch_array($qdataM);
		if($rowdatM["media_file"]){
			$src = '/shop/item-images/orignal/'.$rowdatM["media_file"];
			$dest='/imgthumb/160x120-'.$rowdatM["media_file"];
		}else{
			$src = '/shop/image/no-photo.jpg';
			$dest='/imgthumb/160x120-no-photo.jpg';
		}

		createImgThumbIfnot($DOCUMENT_ROOT.$src,$DOCUMENT_ROOT.$dest,'160','120','ratiowh');
?>        
  <li style="width:230px;" class="last mr10 m10"><a href="/shop/category/<?=$subcategory_nicename?>/" class="vs160 db"><img src="<?=$dest?>" alt="<?=$rowdatM["label"]?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>" class="mB20"/></a><h2><a href="/shop/category/<?=$subcategory_nicename?>/"><?=snippetwop($subcategory_name,$length=20,$tail="...");?></a></h2><a href="/shop/category/<?=$subcategory_nicename?>/" class="viewall">View More »</a></li>
  <?
if($ci%4==0){
	echo '<div class="vs10 mb10 cb"></div>';
	}
$ci++;
		}// If total sub item is their
	}// End While 
}else{

$qItemAll=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id FROM shop_item_category as c, shop_items as i WHERE c.category_id = '$category_id' AND i.visibility='visible' AND i.item_id=c.item_id");
$totrecord = mysql_num_rows($qItemAll);

$qItem=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price, i.item_parent_id FROM shop_item_category as c, shop_items as i WHERE c.category_id = '$category_id' AND i.visibility='visible' AND i.item_id=c.item_id ORDER BY c.cat_order ASC LIMIT $showRecord, $maxshow");

while($rowItem = mysql_fetch_array($qItem)){
	$item_id=$rowItem["item_id"];
	$name=$rowItem["name"];
	$nice_name=$rowItem["nice_name"];
	$price=$rowItem["price"];
	$item_parent_id=$rowItem["item_parent_id"];
	
	if($item_parent_id == '0'){
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	}else{
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
	}
	
	$rowdatM = mysql_fetch_array($qdataM);
	//$rowdatM["media_file"].';'.$rowdatM["label"];
	if($rowdatM["media_file"]){
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		$imageURL='/shop/item-images/thumb_'.$rowdatM["media_file"];
	}else{
		$imagepath = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
		$imageURL='/shop/image/no-photo-t.jpg';
	}
	
	if(file_exists($imagepath)){
		$new_w = 200;
		$new_h = 160;
		$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
    }
?>        
  <li style="width:230px;" class="last mr10 m10"><a href="/shop/<?=$nice_name?>/" class="vs160 db"><img src="<?=$imageURL?>" alt="<?=$rowdatM["label"]?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>" class="mB20"/></a><h2><a href="/shop/<?=$nice_name?>/"><?=snippetwop($name,$length=30,$tail="...");?></a></h2> <a href="/shop/<?=$nice_name?>/" class="viewall">Rs. <?=number_format($price)?></a></li> 
       
  <?
if($ci%4==0){
			echo '<div class="vs10 mb10 cb"></div>';
		}
$ci++;
}

}?>        
     </ul>
      <div class="cb"></div>
        <!-- row1 --><!--  row2 -->
<? if($section[2]=='dog-magazines'){?>
<div class="vs20"></div>
      <div style="margin-left:auto; margin-right:auto; width:80px;"><a href="/new/shop/category-infibeam.php" class="btn">page 2</a></div>
      <? }?>
        
     <div class="vs20"></div>
        
       <!-- category brands -->
     <!-- category brands -->
        
        <!--  category products -->
     
<!-- Google Code for Landing Page Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1018004414;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "hWRsCPrz2gIQvoe25QM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1018004414/?label=hWRsCPrz2gIQvoe25QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
    
   <!-- footer -->
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
   	<!-- footer 2-->