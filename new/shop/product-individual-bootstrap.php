<?php
require_once('constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions-new.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-ArrayShopAttributeSet.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-ArrayShopAttributeValue.php');
$ip=ipCheck();
$sitesection = "shop";
$ecomm_pagetype = 'product';
$ant_section = 'Shop';
$ant_page = 'Product';
$google_conversion_label="hWRsCPrz2gIQvoe25QM";
$session_id = session_id();

$qdata=query_execute("SELECT * FROM shop_items WHERE nice_name='$section[0]' AND item_display_status!='delete'");

$rowTotal = mysql_num_rows($qdata);
if($rowTotal==0){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();
}
$rowdat = mysql_fetch_array($qdata);
$domain_id=$rowdat['domain_id'];
	$dty=strlen($section[0]);
	for($i=0;$i<$dty;$i++){
	if(strcspn($section[0], 'ABCDEFGHJIJKLMNOPQRSTUVWXYZ')==$i){
	$section[0]=strtolower($section[0]);
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/$section[0]/");
	//exit();	
	}
	}
//Url in small Ends	

$name1 = $rowdat["name"];
$name1=stripslashes($name1);
$title1 = $rowdat["title"];
$desc1 = $rowdat["description"];
$item_id = $rowdat["item_id"];
$domain_id_item = $rowdat["domain_id"];
$item_id_for_recent_item = $item_id;
$type_id = $rowdat["type_id"];
$price = $rowdat["price"];
$sku = $rowdat["sku"];
$weight = $rowdat["weight"];
$brand_id = $rowdat["item_brand"];
$nice_name_out = $rowdat["nice_name"];
$stock_status = $rowdat["stock_status"];
$selling_price=$rowdat["selling_price"];
$visibility=$rowdat["visibility"];
$item_status=$rowdat["item_status"];
$main_qty=$rowdat["qty"];
$virtual_qty=$rowdat["virtual_qty"];
$payment_mode=$rowdat["payment_mode"];
$master_cat=$rowdat["master_cat"];
$item_shipping_amount=$rowdat["item_shipping_amount"];
$weight_price=number_format($rowdat["price"]/$rowdat["weight"],0);
$tag=$rowdat["tag"];
$feature=$rowdat["feature"];
$child_cat_breadcrum=$rowdat["child_cat"];
$ecomm_prodid = "$item_id";
	if($type_id=='configurable'){
	$selectchild=mysql_query("SELECT nice_name FROM shop_items WHERE item_parent_id='$item_id' AND stock_status='instock' AND item_display_status='active' AND visibility='visible' LIMIT 1");
	$countchild=mysql_num_rows($selectchild);
	if($countchild > '0'){
while($rowchildname=mysql_fetch_array($selectchild)){
$nice=$rowchildname['nice_name'];
}
	header("HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/".$nice."/");
	}else{
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/");
	}}
$ecomm_totalvalue = number_format($price,2);
if($master_cat=='835')
{
 header("Location: /food-subscription/home/"); 
 exit;	
}

$qCat=query_execute("SELECT category_id FROM shop_item_category WHERE item_id='$item_id' AND category_id!=0");
$rowCat = mysql_fetch_array($qCat);
$category_id = $rowCat["category_id"];
// Custom Variable for Google analytics
$CustomVar[3]='Sub-Category|'.$ArrayShopCategorys[$category_id];
// Custom Variable for Google analytics
$qParentCat=query_execute("SELECT category_parent_id, category_nicename, category_name FROM shop_category WHERE category_id='".$category_id."'"); //NKS
$rowParentCat = mysql_fetch_array($qParentCat); //NKS
$category_parent_id = $rowParentCat["category_parent_id"];
$category_nicename = $rowParentCat["category_nicename"];
$category = $rowParentCat["category_name"];
$ant_category = "$category";

//breadcrumdata
$getcatname=query_execute_row("SELECT category_nicename, category_name, domain_id FROM shop_category WHERE category_id='".$child_cat_breadcrum."'");
$cat_name_bread=$getcatname['category_name'];
$cat_nicename_bread=$getcatname['category_nicename'];
$domain_bread=$getcatname['domain_id'];
// Custom Variable for Google analytics
$CustomVar[2]='Category|'.$ArrayShopCategorys[$category_parent_id];
// Custom Variable for Google analytics
$qParentCatNice=query_execute("SELECT category_nicename FROM shop_category WHERE category_id='".$category_parent_id."'"); //NKS
$rowParentCatNice = mysql_fetch_array($qParentCatNice); //NKS
$catParentNiceName = $rowParentCatNice["category_nicename"];
$qBrand=query_execute("SELECT brand_name,brand_nice_name FROM shop_brand WHERE brand_id='$brand_id'");
$rowBrand = mysql_fetch_array($qBrand);
$brandName=$rowBrand["brand_name"];
$brandNameNice=$rowBrand["brand_nice_name"];
// Custom Variable for Google analytics
$CustomVar[4]='Brand|'.$brandName;
// Custom Variable for Google analytics
// Get all images
$sqlmediaco = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
$trecount=mysql_fetch_array($sqlmediaco);   
    if ($trecount['trecd'] != '0') {
	$sqlmedia="SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC";
}else{
	$sqlmedia="SELECT media_file, label FROM shop_item_media WHERE item_id='".$rowdat["item_parent_id"]."' ORDER BY position ASC";
}

$qdataM=query_execute("$sqlmedia");
while($rowdatM = mysql_fetch_array($qdataM)){
	$ArrayAllImg[]=$rowdatM["media_file"].';'.$rowdatM["label"];
}
$imgFLBase=explode(';',$ArrayAllImg[0]);
// END

$catParentName=$ArrayShopCategorys[$rowParentCat["category_parent_id"]]; //NKS

$catName=$ArrayShopCategorys[$rowCat["category_id"]];
$pageHone=$rowdat["name"];
$pageHone=stripslashes($pageHone);

$pageTitle=$pageHone.' | DogSpot - Online Pet Supply Store';
$pageKeyword=$pageHone.', '.$brandName.', '.$brandName.' '.$catName.', '.$catName;
//$pageDesc=$pageHone.' '.$catName.' '.$art_body.' Online Pet Shop, Buy Dog Products, Pet Stores India DogSpot.in';
$pageDesc=$rowdat["short_description"];
$pageDesc=stripslashes($pageDesc);
	$pageDesc = preg_replace('/\"/', ' ', $pageDesc);
	$pageTitle = preg_replace('/\"/', ' ', $pageTitle);
	$pageKeyword = preg_replace('/\"/', ' ', $pageKeyword);
	$pageHone = preg_replace('/\"/', ' ', $pageHone);

/*$imgURL=get_first_image($desc1, $DOCUMENT_ROOT);
$imgURLAbs = make_absolute($imgURL, 'https://www.dogspot.in');*/
$fbprice=(int)$price;
if($selling_price>$price){$sa=$selling_price-$price;

$discount_per=number_format(($sa/$selling_price)*100);
}
$title=$pageTitle;
$keyword=$pageKeyword;
$desc="Buy $pageHone online by $brandName Brand at reasonable Price. Order $catName from DogSpot.in. Available ✓Cash on Delivery ✓30 Days Return";
$alternate="https://m.dogspot.in/$section[0]/";
$canonical="https://www.dogspot.in/$section[0]/";
$og_url=$canonical;
$imgURLAbs="https://www.dogspot.in/shop/item-images/orignal/$imgFLBase[0]";
require_once($DOCUMENT_ROOT.'/new/common/header-bootstrap.php'); ?>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/product-individual.css?v=17" />
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<?php
$shopProductArray['id']=$item_id;
$shopProductArray['name']=$name1;
$shopProductArray['category']=$ant_category;
$shopProductArray['brand']=$brandName;
$shopProductArray['price']=$price;
$shopProductArray['variant']='';
$shopProductArray['position']='1';
$shopProductArray['dimension1']='Shop';
$itemE =10;
$getdataItem=query_execute_row("SELECT asin,id FROM  shop_item_affiliate WHERE item_id='$item_id' ORDER BY id DESC"); 
	
$response = getAmazonPrice("in", $getdataItem['asin']);

if($response['price']!='0.00' && $response['price'] !=''){
$update=query_execute("UPDATE shop_item_affiliate SET price='".$response['price']."'  WHERE id='".$getdataItem['id']."'");
if($response['mrp_price'] !='0.00' && $response['mrp_price'] !='')
{
$update=query_execute("UPDATE shop_item_affiliate SET mrp_price='".$response['mrp_price']."'  WHERE id='".$getdataItem['id']."'");
}else
{
$update=query_execute("UPDATE shop_item_affiliate SET mrp_price='".$response['price']."'  WHERE id='".$getdataItem['id']."'");	
}
}	
$getdataItem=query_execute_row("SELECT min(price) as price,mrp_price,count(id) as c,merchant_link FROM  shop_item_affiliate WHERE item_id='$item_id' AND price !='' AND price !='0'"); 
$minPrice=$getdataItem['price'];
$mrp_price=$getdataItem['mrp_price'];
if($getdataItem['c']>0){
$itemE=$getdataItem['c'];
}
$merchant_link=$getdataItem['merchant_link'];?>
<!-- product list start-->
<div class="breadcrumbs">
    <div class="container">
  <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
  <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList">
   
      <? if($domain_bread=='1' && $child_cat_breadcrum!='95' && $child_cat_breadcrum!='99'){?>
      <span itemscope itemtype="http://schema.org/ListItem">
      <a href="/dog-store/" itemprop="item"><span itemprop="name">Home</span>
      <meta itemprop="position" content="1" />
      </a>
      </span> <span> / </span>
      <? }?>
     
      <? if($child_cat_breadcrum!=''){ ?>
      <span itemscope itemtype="http://schema.org/ListItem">
      <a href="/<?=$cat_nicename_bread;?>/" itemprop="item">
      <span itemprop="name"><?=$cat_name_bread?></span>
      <meta itemprop="position" content="2" />
      </a></span>
      <? }else{?>
      <span itemscope itemtype="http://schema.org/ListItem"><a href="/<?=$category_nicename?>/" itemprop="item">
      <span itemprop="name"><?=$category;?></span>
      <meta itemprop="position" content="2" />
      </a></span>
      <? }?>
      <span> / </span> 
      <span  itemscope itemtype="http://schema.org/ListItem">
          <span itemprop="name" class="active-bread"><?=creatbreedcrumname(stripslashes($pageHone));?></span>
      <meta itemprop="position" content="3" />
      </span> 
  </div>
</div>
  </div>
</div>
<!-- product -details page start-->
<section class="prd-details-sec" itemscope itemtype="http://schema.org/IndividualProduct">
<div class="modal fade shipping_details" id="shipping_details">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                <h3 class="modal-title">
    Shipping and Delivery details for Products</h3>
            </div>
            <div class="modal-body">
                
                  <div class="sh-text-m">
                  <h2>We ship only in India. </h2>
<div class="text-shp">For domestic (India) buyers, orders are shipped through registered domestic courier companies and speed post. Orders are shipped within 2-3 Business days from the day of placing the order. The delivery of the shipment is subject to Courier Company / post office norms.Radox Trading &amp; Marketing Pvt Ltd (DogSpot.in) is not liable for any delay in delivery by the courier company / postal authorities and only guarantees to hand over the consignment to the courier company or postal authorities within 2-3 Business days from the date of the order and payment. All orders will be delivered to the registered address of the buyer as per the credit/debit card records (Unless specified at the time of Order). Radox Trading &amp; Marketing Pvt Ltd (DogSpot.in) is in no way responsible for any damage to the order while in transit to the buyer.</div>
   <div style=" text-align:left;"> 
    <h2 style="color:#333; margin-top:10px; margin-bottom:5px; ">Shipping Charges</h2>
    
     <div class="text-shp">Free Shipping on all orders above Rs. 500*</div>
          
<h2 style="color:#333; margin-top:10px; margin-bottom:5px;"> *Terms and Conditions</h2>

 <div class="text-shp">1. Dog food products would be charged itemized shipping charge of <strong>Rs. 75/quantity</strong></div>

 <div class="text-shp">2. If the order value is more than or equal to Rs. 500 with no dog food and cat litter then shipping  would be <strong>FREE</strong>
 </div>

 <div class="text-shp">
 3. If the order value is less then Rs. 500 with no dog food or cat litter in the order  then shipping would be <strong>Rs. 49</strong></div>

 <div class="text-shp">4. If  the order has pet food or cat litter + any other items then the itemized shipping will be  applicable <strong>Rs. 75/quantity</strong> of pet food and cat litter</div>
</div>
   
 <div class="text-shp">Rs. 49 applicable for Cash On Delivery (COD) payment. </div>
 <h2> When is an item Out of Stock? </h2>

<div class="text-shp">Currently we do not have these products available for sale. Please use the "Notify Me" feature and we will let you know via Email and SMS once it is available for sale at DogSpot.in</div>

<h2>How is the delivery done?</h2>
 <div class="text-shp" style="margin-bottom:10px;">We process all deliveries through reliable couriers companies that match our delivery service standards, depending on the items ordered and delivery location.</div>
</div>
                    
                
            </div>
        </div>
    </div>
</div>
    <div class="container" >
   <div class="row">
	         <div class=" col-xs-12 col-sm-6 col-md-5 product-img-box">
                
               <div id="examples">

			<ul id="dspzoom">
                        <? if($discount_per && $itemE ==10){?>
          <div class="discount-sticker">
            <?=$discount_per?>
            %</div>
          <? }elseif($itemE ==10 && $mrp_price>$minPrice){
			  $discount=($mrp_price-$minPrice)/$mrp_price*100;?>
              <div class="discount-sticker">
            <?=$discount?>
            %</div>
          <? }?>
				 <? if($ArrayAllImg){
				//	echo 'hei';
			foreach($ArrayAllImg as $imgFileLabel){
				$imgFL=explode(';',$imgFileLabel);
				$imglinkm='https://ik.imagekit.io/2345/tr:h-280,w-300,c-at_max/shop/item-images/orignal/'.$imgFL[0];
	  ?>
          <li class="item"> 
          <img class="dspzoom_thumb_image" itemprop="image" src="<?=$imglinkm?>" alt="<?=$pageHone?>" title="<?=$pageHone?>" />
          <img class="dspzoom_source_image" src="https://ik.imagekit.io/2345/shop/item-images/orignal/<?=$imgFL[0]?>" alt="<?=$pageHone?>" title="<?=$pageHone?>" /> </li>
          <? }}
		        ?>
			</ul>
    	</div>
     </div>
     
     <div class="col-xs-12 col-sm-6 col-md-7 product-shop">
                
      <div class="ds-p-title">
        <h1  itemprop="name"><?=stripslashes($pageHone);?></h1>
      </div>
      <div class="ds-p-span"><span>Item No:  <?=$item_id?></span> <span>|</span> 
      <span>Brand Name: <a href="/<?=$brandNameNice?>/"><?=$brandName?></a></span> 
      <? if($itemE =='10'){?> <span >|</span> <span class="ds-p-in-stock">
        <?=$AShopStock[$rowdat["stock_status"]]?>
        </span>
        <? }?>
        <!--<span class="ds-p-out-of-stock">Out Of Stock</span>--></div>
      <? if($main_qty !='0' || $itemE==10){?>
      <form id="checkpin" name="checkpin" method="post">
        <div class="ds-p-pincode"> <div class="input-group"><span>Check Availability at</span>
          <input type="text" name="pincode" id="pincode" value="<?=$pincode?>" placeholder="Enter Your Pincode" />
          <input type="hidden" name="item_idPIN" id="item_idPIN" value=<?=$item_id?> />
          <input class="check-now-btn " type="button" value="Check Now" name="Check Now" onclick="checkPincode()" />
          <div id="pinmsg" class="check-pincode" style="margin-top:0px;">
            <? if($pincode){ 
		     $pin=$pincode;
			require_once($DOCUMENT_ROOT.'/shop/pinCheck.php'); }?>
          </div>
          </div>
        </div>
      </form>
      <? }?>
       <form id="formShopItem" name="formShopItem" enctype="multipart/form-data" method="POST" action="/new/shop/new-cart.php" >
       <? if($main_qty =='0' && $itemE !=10){?>
        <div class="ds-price-details"> 
        <span class="ds-p-dis-price">Rs.
        <?=number_format($minPrice,0)?>
        </span>
        <? if($mrp_price>$minPrice){ ?>
        <span class="ds-p-cross-price">Rs. <del>
        <?=number_format($mrp_price,0)?>
        </del></span>
        <? }?>
		<? }elseif($main_qty !='0' && $itemE !=10){?>
        <div class="ds-price-details" style="display:none;">
        <div class="ds-price-details" style="display:none;" >
          <? if($minPrice>$price){?>
          <span class="ds-p-dis-price">Rs.
          <?=number_format($price,0)?>
          </span>
          <? if($selling_price>$price){ ?>
          <span classs="ds-p-cross-price">Rs. <del>
          <?=number_format($selling_price,0)?>
          </del></span>
          <? }?>
          <div class="price-tex">Prices are inclusive of all taxes</div>
          <? }else{?>
          <span class="ds-p-dis-price">Rs.
          <?=number_format($minPrice,0)?>
          </span>
          <? if($mrp_price>$minPrice){ ?>
          <span class="ds-p-cross-price">Rs. <del>
          <?=number_format($mrp_price,0)?>
          </del></span>
          <? } 
        }?>
        </div>
         <? }else{ ?>
          <div class="ds-price-details" style="padding-bottom:0px;">
        <span class="ds-p-dis-price">Rs.
        <?=number_format($price,0)?>
        </span>
        <? if($selling_price>$price){ ?>
        <span class="ds-p-cross-price">Rs. <del>
        <?=number_format($selling_price,0)?>
        </del></span>
        <? } ?>
        
        <div class="price-tex">Prices are inclusive of all taxes</div>
        <div class="ds-shiping-details"><? if($item_shipping_amount>0){?> <span>Shipping Charge of <span class="ds-p-shipping-p"><? echo '+'.number_format($item_shipping_amount,0);?>
              / qty
              </span> </span> <? }?> <span><a data-toggle="modal" data-target="#shipping_details">Shipping Details</a></span> <span class="ds-p-cod"> Cash On Delivery: <span class="ds-cod-a"> <? if($payment_mode!='cod'){?>Not Available<? }else{?>Available<? }?></span></span></div>
              <? }?>
              <?php if($child_cat_breadcrum=='239'){ ?>
          <div class="vetProduct vet-box"> <img src="https://ik.imagekit.io/2345/new/pix/veticon.png" width="20" style=" vertical-align: middle;margin-right: 5px;"> This food is only available on a veterinarian prescription <a href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Send us a copy of the prescription either a scanned or through an image upload"><i class="fa fa-info-circle"></i></a></div>
          <? }?>
        
      </div>
      <div class="ds-p-scq-box" style="padding-top:15px;"><? if($type_id=='configurable')
		  {
		  
	$qChild=query_execute("SELECT item_id, price, selling_price FROM shop_items WHERE item_parent_id='$item_id' AND stock_status='instock'");
	while($rowChild = mysql_fetch_array($qChild)){
			$Childitem_id=$rowChild["item_id"];
			$Childprice=$rowChild["price"];
			if($rowChild["selling_price"]>$Childprice){
				$Childselling_price=number_format($rowChild["selling_price"]);
			}		
			$qAtt=query_execute("SELECT DISTINCT i.attribute_set_id, i.attribute_value_id FROM shop_item_attribute as i WHERE i.item_id='$Childitem_id' AND i.attribute_set_id != 0 ORDER BY i.attribute_set_id ASC");
		  while($rowAtt = mysql_fetch_array($qAtt)){
			
			$attribute_set_id = $rowAtt["attribute_set_id"];
			$attribute_value_id = $rowAtt["attribute_value_id"];
			
			if($attribute_set_id != 0){
				$qAttOrder=query_execute("SELECT attribute_value_order FROM shop_attribute_value WHERE attribute_value_id='$attribute_value_id'");
				$rowAtOrdert = mysql_fetch_array($qAttOrder);
				$attribute_value_order = $rowAtOrdert["attribute_value_order"];
				$Aattribut[] = "$attribute_set_id$attribute_value_order"."-$attribute_set_id-$attribute_value_id-$Childprice-$Childselling_price";
			}			
		}
	}
	if($Aattribut){
	$Aattribut = array_unique($Aattribut);
	asort($Aattribut);
//	print_r($Aattribut);
	$keyold=0;
	foreach($Aattribut as $aatt){
		$attkey=explode('-', $aatt);
		if($attkey[1]==$keyold){		
			echo '<option value="'.$attkey[3].'-'.$attkey[1].'-'.$attkey[2].'-'.$item_id.'-'.$attkey[4].'">'.$Ashop_attribute_value[$attkey[2]].''; if($attkey[3]){echo' Rs '.number_format($attkey[3]).'';} echo'</option>';
		}else{
			if($selstart==1){ echo'</select></div>';}
			?>
      <div class="selectbox" id="att_<?=$attkey[1]?>">
        <? if($stock_status=='instock'){
				if($Ashop_attribute_set[$attkey[1]]!=''){
			echo '<b>Choose a '.$Ashop_attribute_set[$attkey[1]].':</b> <br>';
				}
			?>
        <select name="ItemOption[<?=$attkey[1]?>]" id="select<?=$attkey[1]?>" onchange="Javascript:updatePrice($(this).val(), '<?=$attkey[1]?>');" title="" class="required"<? if($keyold!=0){echo'disabled="disabled"';}?>>
          <option value="">Select</option>
          <?
			echo '<option value="'.$attkey[3].'-'.$attkey[1].'-'.$attkey[2].'-'.$item_id.'-'.$attkey[4].'">'.$Ashop_attribute_value[$attkey[2]]; if($attkey[3]){echo ' Rs '.number_format($attkey[3]).'';} echo '</option>';		
		}
		$selstart=1;
		$keyold=$attkey[1];
	}}

	?>
        </select>
        <? } ?>
      </div>
      <?
	 
		  } ?>        <? if($itemE =='10'){?> <span>Qty
       <? if($item_status=='soft') { ?>
          <input type="button" value="-" onclick="descrementValue()"   class="ds-p-qqty-minus">
          <input name="item_qty" type="text" maxlength="3" style="text-align:center; width:25px; height:25px; margin-left:-5px; border:1px solid #ccc;" id="item_qty" value="1">
          <input type="button" onclick="incrementValue()" value="+" class="ds-p-qqty-plus">
          <? }else{?>
          <select id="item_qty" name="item_qty"  class="ShopQty required">
            <? for($vit=1;$vit<=$virtual_qty;$vit++) { ?>
            <option value="<?=$vit?>">
            <?=$vit?>
            </option>
            <? } ?>
          </select>
          <? }?>    </span> <? }?></div>
          <? if($main_qty !='0' && $itemE  !=10 && $stock_status=='instock'){?>
          <div class="ds-p-btn-section aff-mar-sec" style="border-top:0px;">
            <div class="row">
              <div class="col-md-4 aff-logo-sec"> <img src="https://ik.imagekit.io/2345/new/shop/images/ds-logo.jpg" > </div>
              <div class="col-md-4 aff-price-sec"> <span class="aff-price">Rs.
                <?=number_format($price,0)?>
                </span>
                <? if($selling_price>$price){ ?>
                <span class="ds-p-cross-price">&nbsp;Rs. <del>
                <?=number_format($selling_price,0)?>
                </del></span>
                <? }?>
              </div>
              <div  class="col-md-4 text-right aff-price-buy-sec">
                <input name="addtoItemCart" type="submit" unbxdattr="AddToCart" onclick="productCart()" id="addtoItemCart" class="ds-p-buy-now-cart" value="Buy Now" unbxdparam_sku="2975"  alt="Submit">
              </div>
            </div>
            <div class="pls-shipping-chrg">
              <? if($item_shipping_amount>0){ echo '+'.number_format($item_shipping_amount,0);?>
              Shipping Charges
              <? }?>
            </div>
          <!-- shipping details-->
          <div class="ds-shiping-details" style="padding-bottom:0px; border-bottom:0px; text-transform:none;">
           
            <span><a data-toggle="modal" data-target="#shipping_details" class="login-window-pro" >Shipping Details</a></span> <span class="ds-p-cod"> Cash On Delivery:
            <? if($payment_mode!='cod'){?>
            <span class="ds-cod-a" style="color:#ff7a22;">Not Available</span>
            <? }else{?>
            <span class="ds-cod-a">Available</span>
            <? }?>
            </span></div>
          <!-- shipping details end-->
          </div>
          <?  }
		  if($itemE !=10)
	      {
		  ?>
		  <div class="ds-p-btn-section aff-mar-sec" >
            <div class="row">
              <div class="col-md-4 aff-logo-sec"> <img src="https://ik.imagekit.io/2345/new/common/images/amazon-logo.jpg"> </div>
              <div class="col-md-4 aff-price-sec"> <span class="aff-price">Rs.
                <?=$minPrice?>
                </span>
                <? if($mrp_price>$minPrice){ ?>
                <span class="ds-p-cross-price">&nbsp;Rs. <del>
                <?=number_format($mrp_price,0)?>
                </del></span>
                <? }?>
              </div>
              <div  class="col-md-4 text-right aff-price-buy-sec"> <a target="_blank" href="<?=$merchant_link?>">
                <input name="addtoItemCart12" type="button" unbxdattr="AddToCart" id="addtoItemCart12" class="ds-p-buy-now-cart" value="Buy Now" unbxdparam_sku="2975"  alt="Submit">
                </a> </div>
            </div>
          </div><?
	  }elseif($stock_status=='instock'){?>
      <div class="ds-p-btn-section">
            <input name="addtoItemCart1"   type="button"  id="addtoItemCart1" class="ds-p-add-to-cart add-to-cart" value="Add To Cart" unbxdparam_sku="<?=$item_id?>"  onclick="productCart()"  alt="Submit"/ >
            <input name="addtoItemCart"  type="submit" unbxdattr="AddToCart" id="addtoItemCart" class="ds-p-buy-now-cart" value="Buy Now" unbxdparam_sku="<?=$item_id?>"  onclick="productCart()"  alt="Submit"/ >
      </div> <? }else{?>
	  <div  style="float:left">
          <div id="notime" >
            <div class="not-me-stock" >Notify me when this product is in Stock.</div>
            <table width="100%">
              <tr >
                <td width="40%" style="padding:0px"><span>Email id</span>
                  <input class="noti_fy_btn " placeholder="Email id" name="notify_email" id="notify_email" value="<?=useremail($userid)?>" type="email" />
                  <div id="invalidformat1" class="notify-error" style="display:none"> This field is required. </div></td>
                <td width="40%" style="padding:0px"><span>Mobile no</span>
                  <input  class="noti_fy_btn " name="notify_mobile" placeholder="Mobile No" id="notify_mobile" value="<?=userphonenumbr($userid)?>" type="text" maxlength="10" />
                  <div id="invalidformat" class="notify-error" style="display:none"> This field is required. </div></td>
                <td width="20%;" style="padding:0px; text-align:right;"><input type="hidden" id="notify_item" name="notify_item" value="<?=$item_id?>" />
                  <input name="notify" class="notify-submit-btn" type="button" id="notify" style="border:0px; margin-top:0px;" onclick="validateForm()" value="Notify Me"  /></td>
              </tr>
              <tr>
                <td width="40%"></td>
                <td width="40%"></td>
                <td width="20%">&nbsp;</td>
              </tr>
            </table>
          </div>
          <div id="notifyme1" class="notifyme1" > </div>
        </div><?
	  }
	 ?>
            <? if($stock_status=='instock' && $itemE  ==10){?>
      <?
       if(($type_id=='simple' || $type_id=='combo') && $userid!='Guest')  { 
		  $strecount = query_execute_row("SELECT count(*) as referal from shop_wishlist where userid='$userid' AND item_id='$item_id'");
		  if($strecount['referal']=='0' ) {   ?>
          <div class="add-to-my-wishlist"> <? if($userid!='Guest') { ?>
        <a  href="javascript:check('<?=$item_id?>')">Add to My Wishlist</a>
        <? } else { ?>
        <a  href="/login.php"> Add to My Wishlist</a>
        <? } ?>
         </div> <div id="frstwish" style="display:none; margin-bottom:20px; "> <img src="https://ik.imagekit.io/2345/new/pix/wishlist.png" style="vertical-align: middle; margin-top:-5px;"> &nbsp; Added to Wishlist. <a href="/new/shop/wishlist/"><strong>View Your Wishlist</strong></a></div>
      <? } else { ?>
      <div class="add-to-my-wishlist">Already in Wishlist<a href="/new/shop/wishlist/" style="margin-left:5px;     font-size: 12px;">View Wishlist.</a></div>
      <? }  } }?></div>
   
              </div>
      <div id="ShopQtyBox" style="display:inline">
            <input name="itembasceprice" type="hidden" id="itembasceprice" value="<?=$price?>" />
            <input name="item_id" type="hidden" id="item_id" value="<?=$item_id;?>" />
            <input name="tag" type="hidden" id="tag" value="<?=$tag;?>" />
            <input name="item_price" type="hidden" id="item_price" value="<?=$price?>" />
            <input name="type_id" type="hidden" id="type_id" value="<?=$type_id?>" />
            <input name="cemantauser" type="hidden" id="cemantauser" value="<?=$cemantauser?>" />
            <input name="email" type="hidden" id="email" value="<?=$email?>" />
            <input name="sku" type="hidden" id="sku" value="<?=$sku?>" />
            <input name="userid" type="hidden" id="userid" value="<?=$userid?>" />
          </div>
          <div id="catchange" style="display:inline"></div>        
            </form>
        
      </div>
  </div>
  </section>
  
  <!-- combos section start-->
<?  $arrayItem=query_execute("SELECT DISTINCT(item_id) FROM shop_item_affiliate");
	      while($getItem=mysql_fetch_array($arrayItem))
		  {
			 $affilate=$getItem['item_id'];
			 $affilateitem.= $affilate.',';
		  }
		  $item_affilate=rtrim($affilateitem,',');

 $getotherItems=query_execute("SELECT *  FROM `shop_cart` as sc,shop_items as si WHERE sc.item_id=si.item_id AND  sc.`item_id` ='$item_id' AND si.item_brand !='80' AND session_id !='yturtyruty57344' AND session_id !='' group by session_id having count(*) >1");
	    while($get=mysql_fetch_array($getotherItems))
		{
		$order_id.=$get['cart_order_id'].',';	
		}
	    $order_id=rtrim($order_id,',');
		
		if($order_id){
			
		$selectOtherCat=query_execute("SELECT * FROM shop_items as si,shop_cart as sc WHERE si.item_id=sc.item_id AND si.item_id NOT IN ($item_affilate) AND cart_order_id IN ($order_id) AND si.item_brand !='80' AND sc.item_id !='$item_id' AND si.type_id !='configurable' AND visibility='visible' AND child_cat !='$child_cat_breadcrum'  AND stock_status='instock' AND item_display_status !='delete' AND cart_order_id !='0' group by child_cat ORDER BY sc.item_id DESC LIMIT 4 ")	;
		$checkCount1=mysql_num_rows($selectOtherCat);
		if($checkCount1<4)
		{
		
		$selectOtherCat=query_execute("SELECT * FROM shop_items as si,shop_item_media as sim WHERE si.item_id=sim.item_id AND si.item_id NOT IN ($item_affilate) AND  si.item_id !='$item_id' AND si.item_brand !='80' AND child_cat !='$child_cat_breadcrum' AND si.type_id !='configurable' AND master_cat !='$master_cat' AND domain_id='$domain_bread'  AND item_brand IN (23,111,289,260) AND visibility='visible' AND stock_status='instock' AND item_display_status !='delete' group by child_cat ORDER BY si.item_id DESC LIMIT 4 ");
		$checkCount=mysql_num_rows($selectOtherCat);
		if($checkCount<4)
		{
		$selectOtherCat=query_execute("SELECT * FROM shop_items as si,shop_item_media as sim WHERE si.item_id=sim.item_id AND si.item_id NOT IN ($item_affilate) AND si.item_id !='$item_id' AND si.item_brand !='80' AND child_cat !='$child_cat_breadcrum' AND si.type_id !='configurable' AND master_cat !='$master_cat' AND domain_id='$domain_bread' AND visibility='visible' AND stock_status='instock' AND item_display_status !='delete' group by child_cat ORDER BY si.item_id DESC LIMIT 4 ");	
		}
		}
		}else
		{
		$selectOtherCat=query_execute("SELECT * FROM shop_items as si ,shop_item_media as sim WHERE si.item_id=sim.item_id AND si.item_id NOT IN ($item_affilate) AND si.item_id !='$item_id' AND si.item_brand !='80' AND child_cat !='$child_cat_breadcrum' AND si.type_id !='configurable' AND domain_id='$domain_bread' AND item_brand IN (23,111,289,260) AND visibility='visible' AND stock_status='instock' AND item_display_status !='delete' group by child_cat ORDER BY si.item_id DESC LIMIT 4 ");
		$checkCount=mysql_num_rows($selectOtherCat);
		if($checkCount<4)
		{
		$selectOtherCat=query_execute("SELECT * FROM shop_items as si ,shop_item_media as sim WHERE si.item_id=sim.item_id AND si.item_id NOT IN ($item_affilate) AND si.item_id !='$item_id' AND si.item_brand !='80' AND child_cat !='$child_cat_breadcrum' AND si.type_id !='configurable' AND domain_id='$domain_bread' AND visibility='visible' AND stock_status='instock' AND item_display_status !='delete' group by child_cat ORDER BY si.item_id DESC LIMIT 4 ");	
		}
		}
		$item_idCo=mysql_num_rows($selectOtherCat);
			  		
		if($item_idCo>=4){?>
	
    		
<section class="ds-p-combo-box visible-lg visible-md">
  <div class="container">
    <h2>Frequently Bought Together</h2>
   <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-8">
    <div class="ds-combo-p-s"> 
        <!-- combo product-->
         <? $i=1;
		while($qItemq=mysql_fetch_array($selectOtherCat)){
			$i++;
	$item_id=$qItemq["item_id"];
	$type_id1=$qItemq["type_id"];
	$tag=$qItemq["tag"];
	$name=$qItemq["name"];
	$domain_id=$qItemq["domain_id"];
	$stock_status=$qItemq["stock_status"];
	$price=$qItemq["price"];	
	$item_parent_id=$qItemq["item_parent_id"];
	 $qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 $count=mysql_num_rows($qdataM1);
	 if($count>0)
	 {
	  $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 }else{
	if($item_parent_id == '0'){
		 $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	}else{		
		$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
	}
	 }
	$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
	$nice_name=$qItemnice["nice_name"];	
	$rowdatM = mysql_fetch_array($qdataM);
		$imglink='https://ik.imagekit.io/2345/tr:h-120,w-160,c-at_max/shop/item-images/orignal/'.$rowdatM["media_file"];
	   $newprice=$price+$newprice;
		?>
        <div class="ds-p-combo-products">
          <div class="ds-product-check-box">
            <input name="price_product" id="price_<?=$item_id?>" type="checkbox" checked="checked" value="<?=$item_id.'@'.$price?>" onclick="productcheck('<?=$item_id?>','<?=str_replace(',','',number_format($price,0))?>')" 
 data-name="<?=$type_id1?>">
            <label for="price_<?=$item_id?>">&nbsp;</label>
          </div>
          <div class="ds-p-comb_img"> <a href="/<?=$nice_name?>/" target="_blank" class="db vs140"> <img src="<?=$imglink?>" border="0" align="middle" title="<?=$name?>" alt="<?=$name?>" height="120" width="128"></a></div>
          <div class="ds-p-combo-name"> <a href="/<?=$nice_name?>/" target="_blank" title="<?=$name?>" alt="<?=$name?>">
            <?=snippetwop($name,$length=33,$tail="...")?>
            </a> </div>
          <div class="ds-p-combo-price ">Rs.
            <?=number_format($price,0)?>
          </div>
       </div>
       <? if($i<=4){?>
        <div class="ds-p-combo-plus-box"> + </div>
        <? }else{?>
        <div class="ds-p-combo-plus-box">= </div>
        <?
		}}?>
        <!-- combo product end--> 
     
    
      
   </div>

  </div>
  <div class="col-xs-12 col-sm-6 col-md-4 ">
      <div class="ds-como-buy-now-s">
        <div class="ds-p-combo-total">Rs. <div id="pro_price" style="display:inline;"><?=number_format($newprice,0);?></div></div>
        <div>
          <input name="addtoItemCart" type="submit" id="addtoItemCart" class="ds_buy_now_mediumbtn" value="Buy Now" alt="Submit"  onclick="addWItemToCart()">
        <div id="ttt" class="ttt"></div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<? }?>
  <!-- combos section end-->
  <!-- product description-->
 <section class="product-desc">
  <div class="container">
  <div class="row">
  <div class="col-xs-12 col-sm-6 col-md-9">
  <div class="p-ind-desc-box">
    <div class="p-ind-desc">
      <h2>Product Description</h2>
    </div>
    <div class="p-ind-descrion-p">
       <?  if($rowdat["item_parent_id"]!='0'){
				  
					//echo "SELECT item_about FROM shop_items WHERE item_id='".$rowdat["item_id"]."'";
				  $rowPabout=query_execute_row("SELECT item_about FROM shop_items WHERE item_id='".$rowdat["item_id"]."'");
				  if($rowPabout['item_about']){
					  echo stripallslashes(htmlspecialchars_decode($rowPabout["item_about"]));
					  
				  }
				 else
				 {
				    $rowAabout=quaery_execute_row("SELECT item_about FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
					echo stripallslashes(htmlspecialchars_decode($rowAabout['item_about']));	
				 }

			  }else{
				 
				  echo stripallslashes(htmlspecialchars_decode($rowdat["item_about"]));
				 
				  
			  } ?>
      <? if($rowdat["features"]!=''){?><div class="p-head"> Features</div>
         <?
			if($rowdat["item_parent_id"]!='0'){
					//echo "SELECT features FROM shop_items WHERE item_id='".$rowdat["item_id"]."'";
				  $rowPabout=query_execute_row("SELECT features FROM shop_items WHERE item_id='".$rowdat["item_id"]."'");
				  if($rowPabout['features']){
					  echo htmlspecialchars_decode(stripallslashes($rowPabout["features"]));
				  }
				 else
				 {
				    $rowAabout=quaery_execute_row("SELECT features FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
					echo htmlspecialchars_decode(stripallslashes($rowAabout['features']));	
				 }

			  }else{
				  echo htmlspecialchars_decode(stripallslashes($rowdat["features"]));
				   
			  } ?>
      <? }?>
      <? if($rowdat["specification"]!=''){?>
      <div class="p-ind-item-desc">
        <?
			if($rowdat["item_parent_id"]!='0'){	
					//echo "SELECT specification FROM shop_items WHERE item_id='".$rowdat["item_id"]."'";
				  $rowPabout=query_execute_row("SELECT specification FROM shop_items WHERE item_id='".$rowdat["item_id"]."'");
				  if($rowPabout['specification']){
					  echo htmlspecialchars_decode(stripallslashes($rowPabout["specification"]));
				  }
				 else
				 {
				    $rowAabout=quaery_execute_row("SELECT specification FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
					echo htmlspecialchars_decode(stripallslashes($rowAabout['specification']));	
				 }

			  }else{
				  echo stripallslashes(htmlspecialchars_decode($rowdat["specification"]));
				  
			  } ?>
      </div>
      <? }?>
      <? if($rowdat["precautions"]!=''){?>
      <div class="p-ind-item-desc">
        <?
			 if($rowdat["item_parent_id"]!='0'){
				  $rowPabout=query_execute_row("SELECT precautions FROM shop_items WHERE item_id='".$rowdat["item_id"]."'");
				  if($rowPabout['precautions']){
					  echo stripallslashes(htmlspecialchars_decode($rowPabout["precautions"]));
				  }
				 else
				 {
				    $rowAabout=quaery_execute_row("SELECT precautions FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
					echo stripallslashes(htmlspecialchars_decode($rowAabout['precautions']));	
				 }

			  }else{
				  
				  echo stripallslashes(htmlspecialchars_decode($rowdat["precautions"]));
			  } ?>
      </div>
      <? }?>
      <? if($rowdat["others"]!=''){?>
      <div>
        <?   if($rowdat["item_parent_id"]!='0'){
				  $rowPabout=query_execute_row("SELECT others FROM shop_items WHERE item_id='".$rowdat["item_id"]."'");
				  if($rowPabout['others']){
					  echo stripallslashes(htmlspecialchars_decode($rowPabout["others"]));
				  }
				 else
				 {
				    $rowAabout=quaery_execute_row("SELECT others FROM shop_items WHERE item_id='".$rowdat["item_parent_id"]."'");
					echo stripallslashes(htmlspecialchars_decode($rowAabout['others']));	
				 }

			  }else{
				  echo stripallslashes(htmlspecialchars_decode($rowdat["others"]));
			  } ?>
      </div>
      <? }?>
      
      
    </div>
  </div>
  </div>
  <!-- similar products start-->
  <div class="col-xs-12 col-sm-6 col-md-3">
    
      <h3> Similar Products</h3>
      <div class="similars_products">
         <?php  
	 // echo "SELECT * FROM shop_items WHERE (master_cat='$master_cat_id' OR child_cat='$child_cat_id') AND item_id !='$item_id' AND `type_id` != 'configurable' AND `visibility` = 'visible' AND `stock_status` = 'instock' AND `item_display_status` != 'delete' ORDER by created_at DESC LIMIT 3";
	  $select_other_item=query_execute("SELECT name,nice_name,price,si.item_id FROM shop_items as si ,shop_item_media as sim WHERE si.item_id=sim.item_id AND (child_cat='$child_cat_breadcrum') AND child_cat !='0'  AND si.item_id !='$item_id' AND `type_id` != 'configurable' AND `visibility` = 'visible' AND `stock_status` = 'instock' AND `item_display_status` != 'delete' group by si.item_id ORDER by created_at DESC LIMIT 3");
	  $countSi=mysql_num_rows($select_other_item);
		if($countSi<3)
		{
		$max=3-$countSi;
		$select_other_item1=query_execute("SELECT name,nice_name,price,si.item_id FROM shop_items as si ,shop_item_media as sim WHERE si.item_id=sim.item_id AND (master_cat='$master_cat') AND master_cat !='0' AND si.item_id !='$item_id' AND `type_id` != 'configurable' AND `visibility` = 'visible' AND `stock_status` = 'instock' AND `item_display_status` != 'delete' group by si.item_id ORDER by created_at DESC LIMIT $max");	
		}
	  while($rowItemall=mysql_fetch_array($select_other_item)){
    $priceSimilar=$rowItemall['price'];	
	$nicenameSimilar=$rowItemall['nice_name'];
	$nameSimilar=$rowItemall['name'];						
$sqlmediaco1 = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='".$rowItemall['item_id']."'"); 
$trecount1=mysql_fetch_array($sqlmediaco1);   
    if ($trecount1['trecd'] != '0') {
								$SQ_Img = query_execute_row("SELECT media_file, label FROM shop_item_media WHERE item_id='".$rowItemall['item_id']."' ORDER BY position ASC LIMIT 1");
							}
							else{
								$SQ_Img = query_execute_row("SELECT media_file, label FROM shop_item_media WHERE item_id='".$rowItemall["item_parent_id"]."' ORDER BY position ASC LIMIT 1");
							}	
					$imglink='https://ik.imagekit.io/2345/tr:h-300,w-280,c-at_max/shop/item-images/orignal/'.$SQ_Img["media_file"];
							
    				?>
    		<div class="product_list">
				      	<div class="smililars_p_img">
				      		
				      		<a href="/<?=$nicenameSimilar?>/">
                            <img src="<?=$imglink?>" border="0" align="middle" title="" alt="" height="170" width="181" style=""></a>
				      	</div>
				      	<div class="smililars_p_details"><a href="/<?=$nicenameSimilar?>/"><?=substr($nameSimilar,0,20)?></a></div>
						<div class="smililars_p_price">Rs. <?=number_format($priceSimilar,0);?></div>
				    </div>		
    				    				<!-- product list-->
				   <? }?> 
    				
                    
                    
				</div>
    
  </div>
  </div>
  <!-- similar product end-->
  </div>
  </section>
  <!-- product description end-->
<!--Comment system-->
<section class="discuss-cmt-sec"> 
 <div class="container">
 <div style="margin-bottom: 30px; background-color:#fff; ">
        <div id="disqus_thread"></div>
        <noscript>
        Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>
        </noscript>
        <a href="https://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a> </div>
 </div>
 </section>
<!-- discuss end--> 
<!-- product detail end-->

<!-- for image zoom -->
<script src="/bootstrap/js/product-zoomer.js"></script>
  <script>
			jQuery(document).ready(function($){

				$('#dspzoom').dspzoom({
					thumb_image_width: 340,
					thumb_image_height: 324,
					source_image_width: 900,
					source_image_height: 839,
					show_hint: true,
				});
				});
				function productcheck(t,e){
var ab=$("#pro_price").text();
var astring = ab.replace(/[^\d\.\-\ ]/g, '');
var astring = ab.replace(/,/g, '');
var  a=parseInt(astring);
var user=$('#userid').val();
$("#price_"+t).is(":checked")?(
 nprice=Number(a)+Number(e),$("#pro_price").text(nprice)):(nprice=Number(a)-Number(e),$("#pro_price").text(nprice))
 }function productCart()
{
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('send', 'event', 'Add to Cart', 'desktop', 'product page');
	
}</script>
 <script type="text/javascript" defer="defer">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'dogspot'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'https://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script> 
 <script type='text/javascript' src="/js/codex-fly.js"></script> 
 <script type="text/javascript">
$(document).ready(function(){
	$('.add-to-cart').on('click',function(e){
		//Scroll to top if cart icon is hidden on top
		if(($('.cart_anchor').is(":visible"))){
		$('html, body').animate({
			'scrollTop' : $(".cart_anchor").position().top
		});
		}else
		{
		$('html, body').animate({
			'scrollTop' : $(".cart_anchor_top").position().top
		});
			
		}
		//Select item image and pass to the function
		var itemImg = $('.item').parent().find('img').eq(0);
		var item_id=$('#item_id').val();
		var item_qty=$('#item_qty').val();
		var user=$('#userid').val();
		if(($('.cart_anchor').is(":visible"))){
		flyToElement($(itemImg), $('.cart_anchor'));
		}else{
		flyToElement($(itemImg), $('.cart_anchor_top'));	
		}
		ShaAjaxJquary('/new/shop/carts.php?item_id='+item_id+'&item_qty='+item_qty+'&userid='+user+'&session_id=<?=$session_id?>&cart=cart', '#cartItemCountheader', '', '', 'GET', '#cartItemCountheader', '', 'REP');
		 e.preventDefault();
	});
});
function check(wishlist_item,wish_cart_id) {	
	ShaAjaxJquary("/new/shop/wishlist/wishlist_new.php?item_id="+wishlist_item+"", "#wishlist12", '', '', 'GET', "#wishlist12", '<img src="/images/indicator.gif" />','REP');	
	$('#frstwish').css('display','block');
	$('.add-to-my-wishlist').css('display','none');
}
function checkPincode()
{
      var pin=$('#pincode').val();	
	  var item_idPIN=$('#item_idPIN').val();
	  ShaAjaxJquary("/shop/pinCheck.php?pin="+pin+"&item_id="+item_idPIN+"", "#pinmsg", '', '', 'GET', "#pinmsg", '','REP');
}
function validateForm()
 {
	
 		var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 		if(document.getElementById('notify_email').value.length<1)
 		{		
 			document.getElementById('invalidformat1').style.display='block';
			document.getElementById('invalidformat').style.display='none';
 			return false;
 		}
 		else if(!regex.test(document.getElementById('notify_email').value))
 		{
 			alert("Invalid email address format");
 			return false;
 		}
		if(document.getElementById('notify_mobile').value.length<1)
 		{
 			document.getElementById('invalidformat').style.display='block';
			document.getElementById('invalidformat1').style.display='none';
 			return false;
 		}
 		document.getElementById('invalidformat1').style.display='none';
		document.getElementById('invalidformat').style.display='none';
 		notifymesagar();
 }

function notifymesagar() {
	
	var emailid=document.getElementById('notify_email').value;
	
	var phoneno=document.getElementById('notify_mobile').value;
	var itemid=document.getElementById('notify_item').value;
	ShaAjaxJquary("/new/shop/notifyme.php?item_id="+itemid+"&emaiid="+emailid+"&phnno="+phoneno+"", "#notifyme1", '', '', 'GET', "#notifyme1", '<img 	src="/images/indicator.gif" />','REP');

document.getElementById('notime').style.display='none';	
event.preventDefault();	
}
</script>
  <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script> 
<script type="text/javascript">
function incrementValue()
{
    var value = parseInt(document.getElementById('item_qty').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('item_qty').value = value;
}
function descrementValue()
{
    var value = parseInt(document.getElementById('item_qty').value, 10);
	if(value > 1){
    value = isNaN(value) ? 0 : value;
    value--;
    document.getElementById('item_qty').value = value;
}}
</script>
 <?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
