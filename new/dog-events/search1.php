<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Dog Show</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<?php  require_once($DOCUMENT_ROOT . '/new/common/header.php');?> 
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/new/dog-events/search.php" method="GET">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  

<!--nav-->
<div id="wrapper" class="clearfix"> 
     <div>
	<div id="content">  
    <div class="cb vs10"></div>     
    <div  class="recent_show_albem">
	<h1 style="border:0;">Showing Results For: <?=$_GET['searchName'];?> </h1>
    <h2> Albums</h2>
</div>
<? 
// Solar search table show_dog_solr  
//Solar search for Images
//echo "singh";
//echo "http://101.53.137.39/solr/dogspotshow/select?q=$qs&group=true&group.field=album_id&wt=xml&mm=1&indent=true&defType=dismax&qf=text&rows=16";
$imageurl="http://101.53.137.39/solr/dogspotshow/select?q=$qs&wt=xml&mm=1&indent=true&defType=dismax&qf=text&rows=16";
	$imageurl = str_replace(" ","%20",$imageurl);
	$resultimage = get_solr_result($imageurl);
	$totrecordimage= $resultimage['TOTALHITS']; 
	echo $totrecordimage."bharat";
	if($totrecordimage!='0'){?>
  <div class="dog_show_wrapper">
  <div class="dog_show_box">
 <div class="dog_show_wrapper">
  <div class="dog_show_box">
  <?php
 	foreach($resultimage['HITS'] as $rowItem){
	$image_nicename=$rowItem["image_nicename"];
	$album_id=$rowItem["album_id"];
	$album_name=$rowItem["album_name"];
	$imagenice_name=$rowItem["image_nicename"];
	$image_id=$rowItem["image_id"];
	$sqlsir=query_execute_row("SELECT title, cdate, no_views, image FROM photos_image WHERE image_id='$image_id'");
	$title=$sqlsir["title"];
	$date=showdate($sqlsir["cdate"], "d M o");
	$image=$sqlsir["image"];
	$sqlalb=query_execute_row("SELECT album_nicename FROM photos_album WHERE album_id='$album_id'");
	$nice_name=$sqlalb["album_nicename"];
	if($image){
	$src = $DOCUMENT_ROOT.'/photos/images/'.$image;
	$destm = $DOCUMENT_ROOT.'/imgthumb/200x134-'.$image;
	createImgThumbIfnot($src,$destm,'200','134','ratiowh');
 ?>
 <div class="dogshows_wrapperBox" <? if ($i%2 == 0){ echo "style='margin-right:0'";} ?>>
                
					<div class="dog_shows_thumbnail">

						<a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank">
	<img src="<?='/imgthumb/200x134-'.$image;?>" alt="<?=$title?>" title="<?=$title?>" height="134" width="200" align="middle"></a>
					</div>

					<div class="dog_shows_title">					
						<p class="dog_name"><a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank" ><? echo snippetwop($album_name, 18, '');
?></a></p>
<?
 $qcount= query_execute_row("SELECT count(*) as image from photos_image where album_id='$album_id'");?>
                           <p class="dog_date"><a href="#"><? echo $qcount["image"]." "."Photos";?></a> | <a href="#"><?=$date?></a>                  </p>					
					
					</div>

				</div>
                 <? // echo $i;      
} 

} 
?>
				</div>
				
 
 
 </div>
				
				</div>
				
 
 
 </div>
 <? }
 ?>
      </div>
      
	  </div>
           <?  
		   //Solar search for show result
		   $resulturl="http://101.53.137.39/solr/dogspotshow/select?q=$qs&wt=xml&mm=1&indent=true&defType=dismax&qf=text&rows=6";
			$resulturl = str_replace(" ","%20",$resulturl);
			$resultr = get_solr_result($resulturl);
			$totrecordr= $resultr['TOTALHITS'];
				if($totrecordr!='0'){ ?>
      <div class="cb vs10"></div>
          <div  class="recent_show_albem">
	<h2>Dog Show Results </h2>
</div>
  <div class="dogshow_sechduletable">
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Address</th>
            <th class="" valign="top" width="129"> Date</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
	$dit=0;
 foreach($resultr['HITS'] as $rowShow){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$sqldes=query_execute_row("SELECT show_nicename, date, show_desc, location FROM show_description WHERE show_id = '$show_id'");
	$show_nicename = $sqldes["show_nicename"];
	$show_desc = $sqldes["show_desc"];
	$location = $sqldes["location"];
	$date = $sqldes["date"];
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT ring_id FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 
	 if($ring_id || $album_nicename){ $dit=$dit+1;
 if($ring_id ){?>          
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><img src="/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14"></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="/<?=$show_nicename?>/">
				<?=$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$location;?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <? print(showdate($date, "d M Y"));?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/<?=$show_nicename?>/">Show Results</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon blueborder" <? }else{ ?> class="dark icon blueborder" <? }?> valign="top" width="62"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
           <? }if($dit==5){break;}}?>    
        </tbody></table>

      </div>
      <? }?>
 <? //Solar search for Upcoming show
 $upcomingurl="http://101.53.137.39/solr/dogspotshow/select?q=$qs&wt=xml&mm=1&indent=true&defType=dismax&qf=text&rows=6";
			$upcomingurl = str_replace(" ","%20",$upcomingurl);
			$resultupcoming = get_solr_result($upcomingurl);
			$totrecordup= $resultupcoming['TOTALHITS'];
				if($totrecordup!='0'){
 ?>      
       <div class="cb vs10"></div>
          <div  class="recent_show_albem">
	<h2>Upcoming Shows </h2>
</div>     
      <div class="dogshow_sechduletable">
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Location</th>
            <th class="" valign="top" width="129"> Closing date</th>
            <th class="" valign="top" width="117"> Days left</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
$s=1;
foreach($resultupcoming['HITS'] as $rowItem2){
$kennel_club=$rowItem2["kennel_club"];
	  $sqlalu=query_execute_row("SELECT kennel_id FROM kennel_club WHERE kennel_name='$kennel_club'");
      $getshowdata=query_execute_row("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' AND organized_by='".$sqlalu['kennel_id']."'");
$show_name=$getshowdata['event_name'];
$location=$getshowdata['venue'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['start_date'];
$edate=$getshowdata['end_date'];
$show_id=$getshowdata['link_id'];
$event_id=$getshowdata['event_id'];
$organized_by=$getshowdata['organized_by'];
$event_nice_name=$getshowdata['event_nice_name'];
$selke=query_execute_row("SELECT kennel_name, city FROM kennel_club WHERE kennel_id='$organized_by'");

$toDis=date("Y-m-d H:i:s");
$startTimeStamp = strtotime($edate, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);
$numberDays = $timeDiff/86400;  // 86400 seconds in one day
// and you might want to convert to integer
$numberDays = intval($numberDays);
?>       
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><img src="/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14"></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="/dog-show/show-details.php?event_id=<?=$event_id;?>">
				<?=$selke['kennel_name']."</br>".$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$selke['city'];?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="129"><? print(showdate($edate, "d M Y"));?></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <?=$numberDays?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <? if($show_id){?>
                    <a id="" class="showschedule_enter_online" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
                    <? }?>
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/dog-show/show-details.php?event_id=<?=$event_id?>">Show details</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon blueborder" <? }else{ ?> class="dark icon blueborder" <? }?> valign="top" width="62"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
               
        </tbody></table>
      </div>
      <? }?>
      <? //Solar search for kennel club detail 
	  $kennelurl="http://101.53.137.39/solr/dogspotshow/select?q=$qs&wt=xml&mm=1&indent=true&defType=dismax&qf=text&rows=5";
			$kennelurl = str_replace(" ","%20",$kennelurl);
			$kennelimage = get_solr_result($kennelurl);
			$totrecordk= $kennelimage['TOTALHITS'];
				if($totrecordk!='0'){ ?>     
             <div class="cb vs10"></div>
          <div  class="recent_show_albem">
	<h2>About Club </h2>
</div>     
      <div class="dogshow_sechduletable">
      <? 				 foreach($kennelimage['HITS'] as $rowItem3){
					$kennel_club=$rowItem3["kennel_club"];
					$kcont=query_execute_row("SELECT contact_detail FROM kennel_club WHERE kennel_name='$kennel_club'");
					?>
         <table id="tbl_data" border="0" width="100%">
            <tbody><tr>
              <td width="25"><img src="/dog-show/Images/buttons/organized by.png" alt="Calendar" height="13" width="15"></td>
              <td width="597"><strong>Club Name:</strong> <?=$kennel_club;?></td>
            </tr>
            <tr>
              <td width="25"><img src="/dog-show/Images/buttons/Contact-icon.jpg" alt="Calendar" height="13" width="15"></td>
              <td width="597"><strong>Contact Detail:</strong> <?=$kcont['contact_detail'];?></td>
            </tr>
            </tbody></table>
      <? }?>
      </div>
      <? }?>
  </div>
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
