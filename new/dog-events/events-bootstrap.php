<?php
include($DOCUMENT_ROOT."/session-no.php"); 
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");
include($DOCUMENT_ROOT."/new/dog-events/calender.php");

$sitesection = "dog-events";
$date_session_array=''; 

$title="Dog Show, Dog Events, Dog Championship, KCI | Dog Spot";
$keyword="Dog Show, Dog Events, Dog Championship, KCI, Dog Spot" ;
$desc="Dog Show, Dog Events, Dog Championship, KCI, Dog Spot" ;
 
    $alternate="https://www.dogspot.in/dog-events/show-schedules/";
	$canonical="https://m.dogspot.in/dog-events/show-schedules/";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='dog-events';

require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
?>
<link type="text/css" rel="stylesheet" href="/bootstrap/css/dog-show.css?v=2">
<section class="dog-show-section">

<div class="container-fluid  visible-lg visible-md">
<div class="row">
<div class="col-md-12 padding-0">
<img src="https://ik.imagekit.io/2345/dog-show/Images/photos/show-schedules.jpg" width="1350" height="319" class="img-responsive" style="width:100%;">
<div class="slideshowtext">
        <h1>Show schedules</h1>
        
        </div>
        </div>

</div>
</div>
<div class="container-fluid breeds-nav" id="wrapper">
    <div class="container" id="content">
      <nav class="navbar">
        <ul class="nav navbar-nav">
          
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
        
        
      </nav>
    </div>
  </div>


<div class="container">
<div class="row">
<h2>Shows</h2>
<div class="col-md-12">

           <form id="formOrderSearch" name="formOrderSearch" method="post" class="ds-drp-dog-show">
              <div class="input-group">
              <select name="typeid" id="typeid" class="selectpicker">
	<option value="">Display by...</option>
     <? $seshow=query_execute("SELECT a.type_name, a.type_id FROM show_type as a, events as b WHERE b.show_type=a.type_id AND b.end_date>NOW() AND b.publish_status='publish' GROUP BY a.type_name ORDER BY a.type_name ASC");
 while($getre=mysql_fetch_array($seshow)){
	 $type_id=$getre['type_id'];
	 $type_name=$getre['type_name'];
	 ?>
 <option  value="<?=$type_id?>"<? if($typeid=='$type_id'){ echo "selected='selected'";} ?>><?=$type_name?></option>
 <? }?>
 </select>
              <button type="submit" name="searchOrder" id="searchOrder" alt="Go" value="go">
                
                <span class="fa fa-search"></span></button>
              </div>
            </form>
          
</div>
<div class="col-md-12">
<div class="dogshow_sechduletable">
<table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Location</th>
            <th class="" valign="top" width="129"> Closing date</th>
            <th class="" valign="top" width="117"> Days left</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
if($typeid!=''){
$showype = " AND show_type='$typeid'";
	}
		$showdes=query_execute("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' $showype ORDER BY start_date ASC");

$s=1;
while($getshowdata=mysql_fetch_array($showdes)){
$show_name=$getshowdata['event_name'];
$location=$getshowdata['venue'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['start_date'];
$edate=$getshowdata['end_date'];
$show_id=$getshowdata['link_id'];
$event_id=$getshowdata['event_id'];
$organized_by=$getshowdata['organized_by'];
$event_nice_name=$getshowdata['event_nice_name'];
$selke=query_execute_row("SELECT kennel_name, city FROM kennel_club WHERE kennel_id='$organized_by'");

$toDis=date("Y-m-d H:i:s");
$startTimeStamp = strtotime($edate, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);
$numberDays = $timeDiff/86400;  // 86400 seconds in one day
// and you might want to convert to integer
$numberDays = intval($numberDays);
$sqldentry=query_execute_row("SELECT entry FROM show_description WHERE show_id='$show_id'");
$sqlcdate=$sqldentry['entry'];
?>        
               <tr>
                <td  class=<?php if($s % 2 == 0){?>"" <? }else{ ?>"dark " <? }?> valign="top" >
                <img src="https://ik.imagekit.io/2345/dog-show/Images/buttons/tbl_paw.png" alt="Paw" height="14" width="14" /></td>

<td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="/dog-events/<?=$event_nice_name;?>/"><?=$selke['kennel_name']."</br>".$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$selke['city'];?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="129"><? print(showdate($edate, "d M Y"));?></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <?=$numberDays?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <? if($show_id){?>
                    <? if($sqlcdate!='close'){?>
                    <a id="" class="showschedule_enter_online" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
                    <? }?><? }?>
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/dog-events/<?=$event_nice_name?>/">Show details</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class=" blueborder" <? }else{ ?> class="dark  blueborder" <? }?> valign="top" width="62"><img src="https://ik.imagekit.io/2345/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?> <? if($date!=$edate){ ?> - <? print(showdate($edate, "d M Y")); }?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
               
        </tbody></table>
        

      </div>
</div>
</div>

</div>


</section>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
