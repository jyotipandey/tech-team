<?php 
//include($DOCUMENT_ROOT."/constants.php");
include($DOCUMENT_ROOT."/session.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");

require_once($DOCUMENT_ROOT.'/facebookconnect/facebooksettings.php');
require_once($DOCUMENT_ROOT.'/facebookconnect/facebookfunctions.php');
require_once($DOCUMENT_ROOT.'/twitterconnect/twitterfunctions.php');

//include 'events.php';


$sitesection = "add-events";
$user_ip = ipCheck();


$sel=query_execute("SELECT * FROM events WHERE event_nice_name = '$event_nice_name'");
 $selget=mysql_fetch_array($sel);
 $date=$selget['start_date'];
 $edate=$selget['end_date'];
 $organized_by=$selget['organized_by'];
 $member_club=$selget['member_club'];
 $member_sec=$selget['member_sec'];
 $map=$selget['map'];
 $event_name=$selget['event_name'];
 $show_id=$selget['link_id'];
 $location=$selget['venue'];
 $event_id=$selget['event_id'];
 $pdf=$selget['pdf'];
 $pointed=$selget['pointed'];
 $online_close_date=$selget['online_close_date'];
 $seljud=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' ORDER BY judge_id DESC");
 $judge=$seljud['judge'];
 $judge_id=$seljud['judge_id'];
 $seljud1=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id' ORDER BY judge_id DESC");
 $judge1=$seljud1['judge'];
 $judge_id1=$seljud1['judge_id'];
 $seljud2=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id1' AND judge_id!='$judge_id' ORDER BY judge_id DESC");
 $judge2=$seljud2['judge'];
 $judge_id2=$seljud2['judge_id'];
 $seljud3=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id2' AND judge_id!='$judge_id' AND judge_id!='$judge_id1' ORDER BY judge_id DESC");
 $judge3=$seljud3['judge'];

 $selk=query_execute_row("SELECT kennel_name, kennel_nicename, contact_detail FROM kennel_club WHERE kennel_id='$organized_by'");
 $kname=$selk['kennel_name'];
 $contact_detail=$selk['contact_detail'];
 $quotes_event_name = str_replace('"', "", $event_name);
$sitesection = "dog-events"; 
 $title='Dog Event Information organized by <?=$kname.", ".$quotes_event_name?> <?=showdate($date, "d M Y")?>';
	$keyword='Dog Event Information, <? echo $kname.", ".$quotes_event_name?> <?=showdate($date, "d M Y")?>"';
    $desc='Get Dog Event Information organized by <? print =$kname.", ".$quotes_event_name?> ."held on." <? print(showdate($date, "d M Y"));?>"';
	$alternate="https://m.dogspot.in/dog-events/<?=$section[1];?>/";
	$canonical="https://www.dogspot.in/dog-events/<?=$section[1];?>/";
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in/new/pix/dog-show-2016.jpg";
	$page_type='dog-events';
   require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
   <link type="text/css"  rel="stylesheet" href="/bootstrap/css/dog-show.css?v=12" />

<!-- dog show schedules -->
<section class="dog-show-section">
<div class="container-fluid visible-lg visible-md">
<div class="row">
<div class="col-md-12 padding-0">
<img src="https://ik.imagekit.io/2345/dog-show/Images/photos/show-schedules.jpg" width="1350" height="319" class="img-responsive" style="width:100%;">
<div class="slideshowtext">
        <h1>Show schedules</h1>
        
        </div>
        </div>

</div>
</div>
<div class="container-fluid breeds-nav" id="wrapper">
    <div class="container" id="content">
      <nav class="navbar">
        <ul class="nav navbar-nav">
          
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
        
        
      </nav>
    </div>
  </div>
    
<div class="container ds-about-us">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-8 ">

          <h2><? echo $event_name ?><? if($pointed=='Y'){ echo "(Pointed)";}?></h2>
       

<div id="schedule_col_left">
          <table id="tbl_data" border="0" width="100%">
            <tbody><tr>
              <td style="padding: 10px 0px;"> <img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="16" width="16"></td>
              <td style="padding: 10px;"><strong>Show date:</strong> <? print(showdate($date, "d M Y"));?></td>
            </tr>
            <? 
			if($online_close_date!='0000-00-00 00:00:00'){?>
            <tr>
              <td style="padding: 10px 0px;"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="16" width="16"></td>
              <td style="padding: 10px;"><strong>Online entry closing on:</strong> <? print(showdate($online_close_date, "d M Y"));?></td>
            </tr>
            <? }?>

<?php /*?>            <? if($edate != $date){ ?>
            <tr id="">
	<td><img src="/dog-show/Images/buttons/tbl_mail.jpg" alt="Postal" height="11" width="16"></td>
	<td><strong>Postal entries close on:</strong> <? print(showdate($edate, "d M Y"));?></td>
</tr>
 <tr id="">
	<td><img src="/dog-show/Images/buttons/data-entry-icon.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Online entries close on:</strong> <? print(showdate($edate, "d M Y"));?></td>
</tr>
<? }?><?php */?>


<tr id="">
	<td style="padding: 10px 0px;"><img src="/dog-show/Images/buttons/organized by.png" alt="Globe" height="16" width="16"></td>
	<td style="padding: 10px"><strong>Organized By:</strong><a href="/<?=$selk['kennel_nicename']?>/"> <?=$selk['kennel_name'];?></a></td>
</tr>

<tr id="">
	<td  style="padding: 10px 0px;"><img src="/dog-show/Images/buttons/judges.png" alt="Globe" height="16" width="16"></td>
	<td style="padding: 10px"><strong>Judges:</strong> <?=$judge?><? if($judge1!=''){ echo ", ".$judge1;} if($judge2!=''){ echo ", ".$judge2;} if($judge3!=''){ echo ", ".$judge3;} ?></td>
</tr>

<? if($location!=''){ ?>
<tr id="">
	<td style="padding: 10px 0px;"><img src="/dog-show/Images/buttons/venue.png" alt="Globe" height="16" width="16"></td>
	<td style="padding: 10px"><strong>Venue:</strong> <?=stripcslashes(htmlspecialchars_decode($location))?></td>
</tr>
<? }?>
          </tbody></table>
         <?=stripcslashes(htmlspecialchars_decode($map));?>	 
     <table id="tbl_data" border="0" width="100%">
        <tr id="">
	<td style="padding: 10px 0px;"><img src="/dog-show/Images/buttons/Contact-icon.jpg" alt="Globe" height="16" width="16"></td>
    	
	<td style="padding: 10px"><strong>Contact Detail:</strong><?=stripcslashes(htmlspecialchars_decode($contact_detail))?></td>
</tr>
</table>


<div id="online_help">
       
        <p>Online support is available <br />
        Monday - Friday 10:00am - 06:00pm,<br /> please call now on +91-9212196633.</p>
         </div>
        </div></div>
<aside class="col-xs-12 col-sm-12 col-md-4">
        <div class="row">
         <!-- Widget header -->
         <section class="col-sm-6 col-md-12 widget "> 
            <header class="clearfix">
              <h4>In this section</h4>
            </header>
<div id="schedule_butts" class="dogshow_sub_nav" style="    padding: 10px;"> 
            <? if($show_id){?>
            <a id="" class="schedule_butts_4" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>" target="_blank">Enter on-line</a>
            <? }?> 
            <a id="" class="schedule_butts_3" href="/dog-show/Images/entry-form.jpg" target="_blank">Entry Form</a>
            <? if($pdf){?>
            <a id="" class="schedule_butts_1" href="/dog-show/show-schedule.php?event_id=<?=$event_id?>" target="_blank">Show Schedule</a>
            <? }?>
        </div>
</section></div></aside>
  </div>
</div>


<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>

