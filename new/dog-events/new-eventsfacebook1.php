<?php
include($DOCUMENT_ROOT."/session-require.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/arrays.php");


require_once($DOCUMENT_ROOT.'/facebookconnect/facebooksettings.php');
require_once($DOCUMENT_ROOT.'/facebookconnect/facebookfunctions.php');
require_once($DOCUMENT_ROOT.'/twitterconnect/twitterfunctions.php');
require_once($DOCUMENT_ROOT.'/facebookconnect/src/facebook.php');
if($submit){

 $access_token = "AAACEdEose0cBAIZCWX9qBytuxXXvVX1V1xnAURzjK8kmoPhQdFBE1SG2pYWdI6TZCJAMtlj7laXGnkdzcxKbngwjPwKujFQz3AH4fJADLxkhDyvoZA1";
$name = $_POST['name'];
$token = $access_token;
$startTime = $_POST['start_time'];
$endTime = $_POST['end_time'];
$location = $_POST['location'];
$description = $_POST['description'];

$fileName = "./me2.jpg"; //profile picture of the event

$fb = new Facebook(array(
    'appId'      => FACEBOOK_APP_ID,
    'secret'     => FACEBOOK_SECRET,
    'cookie'     => false,
    'fileUpload' => true     // this is important !
));

$fb->setAccessToken($token);

$data = array("name"=>$name,
              "access_token"=>$token,
              "start_time"=>$startTime,
              "end_time"=>$endTime,
              "location"=>$location,
              "description"=>$description,
              basename($fileName) => '@'.$fileName
);
try{
    $result = $fb->api("/me/events","post",$data);
    $facebookEventId = $result['id'];
    echo $facebookEventId;
}catch( Exception $e){
    echo "0";
}
}
?>
<!doctype html>
<html>
<head>
<title>Create An Event</title>
<style>
label {float: left; width: 100px;}
input[type=text],textarea {width: 210px;}
#msg {border: 1px solid #000; padding: 5px; color: red;}
</style>
</head>
<body>
<?php if( isset($msg) ) { ?>
<p id="msg"><?php echo $msg; ?></p>
<?php } ?>
<form enctype="multipart/form-data" action="" method="post">
    <p><label for="name">Event Name</label><input type="text" name="name" value="" /></p>
    <p><label for="description">Event Description</label><textarea name="description"></textarea></p>
    <p><label for="location">Location</label><input type="text" name="location" value="" /></p>
    <p><label for="">Start Time</label><input type="text" name="start_time" value="<?php echo date('Y-m-d H:i:s'); ?>" /></p>
    <p><label for="end_time">End Time</label><input type="text" name="end_time" value="<?php echo date('Y-m-d H:i:s', mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))); ?>" /></p>
    <p><label for="picture">Event Picture</label><input type="file" name="picture" /></p>
    <p>
        <label for="privacy_type">Privacy</label>
        <input type="radio" name="privacy_type" value="OPEN" checked='checked'/>Open&nbsp;&nbsp;&nbsp;
        <input type="radio" name="privacy_type" value="CLOSED" />Closed&nbsp;&nbsp;&nbsp;
        <input type="radio" name="privacy_type" value="SECRET" />Secret&nbsp;&nbsp;&nbsp;
    </p>
    <p><input type="submit" value="Create Event" name="submit" id="submit" />
        <input type="hidden" value="submit" name="submit" id="submit" /></p>
</form>
</body>
</html>