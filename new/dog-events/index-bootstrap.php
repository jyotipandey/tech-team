<?php
include('../../constants.php');
include($DOCUMENT_ROOT."/session-no.php"); 
include($DOCUMENT_ROOT."/functions.php");
//include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");

$sitesection = "dog-events"; 
 $title="Dog Show | Dog Events | Dog Championship | Kennel Club India";
	$keyword="Dog Show, Dog Events, Dog Championship, dog show albums, KCI, Kennel Club India events.";
    $desc="Find the Dog show results, online show entry details, recent articles on dogs as well as old events albums.";
	$alternate="https://m.dogspot.in/dog-events/";
	$canonical="https://www.dogspot.in/dog-events/";
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in/new/pix/dog-show-2016.jpg";
	$page_type='dog-business';
   require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>

<form method="post" name="formcomnt" id="formcomnt">
<section class="dog-show-section">

<div class="container-fluid  visible-lg visible-md">
<div class="row">
<div class="col-md-12 padding-0">
<img src="https://www.dogspot.in/dog-show/Images/slide-banner/dog-show-3new1.jpg" width="1350" height="319" class="img-responsive" style="width:100%;" />
<div class="slideshowtext">
        <h1>Making your<br>
          show a success</h1>
        <p>DogSpot is the power behind a successful dog show. With almost 20 years of experience with dogs, we take away the hassle to ensure your show runs smoothly and all of the pre and post-show data to be accessible on one portal. </p>
        <a href="/new/dog-events/enter-online.php" class="dogshow_enter">Enter on-line</a> <a href="/dog-events/about-us/" class="dogshow_running">Running a show?</a> </div>
</div>

</div>
</div>

<!-- dog show nav-->
<div class="container-fluid breeds-nav">
    <div class="container">
      <nav class="navbar">
        <ul class="nav navbar-nav">
          
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
        
        
      </nav>
    </div>
  </div>
<!-- dog show-nav end-->


 <div class="container">
<div class="row"> 

<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="box_list box-show"> 
  <div class="box_show_title">Show Results</div>
        <ul>
<?
$selectShow = mysql_query("SELECT * FROM show_description WHERE show_id != '2' AND show_id != '123' AND show_id != '124' ORDER BY date DESC LIMIT 55");
 if(!$selectShow){	die(mysql_error());	}
	$dit=0;
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT * FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 
	 if($ring_id){ 
	 	$dit=$dit+1;
?>   
        <li>
            <? if($ring_id ){?> <a id="" href="/<?=$show_nicename?>/" style="color:#162E44;" title="<?=$show_name?> Dog Show Results"><?=$show_name?>,              
                <span class="date"><? print(showdate($date, "d M Y")); ?></span></a><? }?>
        </li>
    <? }if($dit==5){break;}}?>

         <li>        
  <a href="/show-results/" class="view-more">view more »</a>     
  </li>
 </ul>
      </div>
</div>
      <!-- #EndLibraryItem -->
     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="box_list box-show"> 
  <div class="box_show_title">Recent Articles</div>
        <ul>
        <?
						$selectMC = mysql_query("SELECT * FROM wp_posts WHERE (post_content like '%dog shows%' OR post_title like '%dog shows%') AND post_status='publish' ORDER BY post_date DESC limit 4");
						
					  if(!$selectMC){	die(mysql_error());	} ?>
 <!-- content -->
                             <? $i=0;
							  while($rowArt = mysql_fetch_array($selectMC)){
								$i=$i+1;  
								$article_id = $rowArt["ID"];
								$art_subject = $rowArt["post_title"];
								$art_body = $rowArt["post_content"];
								$c_date = $rowArt["post_date"];
								$artuser = $rowArt["userid"];
								$art_name = $rowArt["post_name"];
								  
						$art_subject = stripslashes($art_subject);
						$art_subject = breakLongWords($art_subject, 12, " ");
						$art_body = stripslashes($art_body);
						$art_body = strip_tags($art_body);
						$art_body = trim($art_body);
						$art_body = substr($art_body,0,104);
					
						$art_body = stripslashes($art_body);
						$art_body = breakLongWords($art_body,60, " ");
					 
					  ?>
                      <li>
           <a href="/<?=$art_name?>/" style="color:#162E44;" title="<?=$art_subject?> Dog Show Results"><? echo"$art_body...";?>,              
               </a>
        </li>
					<? }?>
      <li>        
  <a href="/dog-blog/events/" class="view-more">view more »</a>     
  </li>
      </ul>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
<div class="box_list "> 
  <div class="box_show_title">Show entry</div>
        <img src="/bootstrap/images/Enter-Online.jpg" width="302" height="321"  alt="" title="" style="width:100%"  />
     <div class="text-right dogshow_enterbtn">
     <a href="/new/dog-events/enter-online.php">Enter 
          on-line</a>
          </div>
      </div>
</div>
</div>
</div>
 <div class="share-bnt-box">
            <? 
				$surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				include($DOCUMENT_ROOT."/new/articles/social.php"); 
			?>
            
            </div>  
<div class="container">
<h2>Recent Albums</h2>

<div class="row rytPost_list" id="rytPost_list">
            
<? $iid=0;?>   
  <!-- dog show box star-->

  
  <?php
$qItem1=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name, album_link_id FROM photos_album
WHERE  album_link_name ='event' order by cdate desc limit 0,12 ");
$qItem1all=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE  album_link_name ='event' order by cdate desc");
$totrecord = mysql_num_rows($qItem1all);
$i=0;
		$div_count='0';
while($rowItem = mysql_fetch_array($qItem1)){
				$iid++;
		$div_count++;
	$album_id=$rowItem["album_id"];
	$album_link_name=$rowItem["album_link_name"];
	$i=$i+1;
	$album_name=$rowItem["album_name"];
	$nice_name=$rowItem["album_nicename"];
	
	$qdataM12=query_execute_row("SELECT cover_img FROM photos_image where album_id='$album_id' AND cover_img =  '1'");
	//$cover_image=$qdataM12['cover_img'];
	if($qdataM12) {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id' AND cover_img='1'");
	//echo "SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where    album_id='$album_id'";
	}
	else {
	//	echo "SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where    album_id='$album_id' AND cover_img='1'";
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id'");	
	}
	$sqldatsh=query_execute_row("SELECT date FROM show_description WHERE show_id='".$rowItem['album_link_id']."'");
		$image=$qdataM["image"];
		$imagenice_name=$qdataM["image_nicename"];
		$title=$qdataM["title"];
		if($sqldatsh['date']!=''){ $date=showdate($sqldatsh["date"], "d M o"); }else{ $date=showdate($qdataM["cdate"], "d M o");}
	if($image){
$destm="https://ik.imagekit.io/2345/tr:h-134,w-200,c-at_max/photos/images/".$image;

 ?>
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 imgtxtcontwag" id='<?=$iid?>'> 
<div class="dog-shows-thumbnail dog-bg-gray">
<a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank">
<img src="<?=$destm?>" alt="<?=$album_name?> " title="<?=$album_name?>" height="134" width="200" align="middle" class="img-responsive" style="max-height: 175px;">

		<div class="dog-shows-caption text-center">					
						<h3 class="title">
                        <?=substr($album_name,0,20);?>
                        </h3>
                          <p class="dog_date">
                <?  $qcount= query_execute_row("SELECT count(*) as image from photos_image where album_id='$album_id'");?>
                          <span><? echo $qcount["image"]." "."Photos";?></span> | <span><?=$date?></span>                  </p>					
					
					</div>
 </a>   				
					</div>
</div>
<? }}?>

<div id='loadMoreComments' style='display:none'><img src="/new/pix/loading2.gif" /></div>
		<div id="txt1" style='display:none; cursor:pointer' ><? echo $cattype; ?></div>
<div id="txt2" style='display:none'><? echo $totrecord-1; ?></div></div>
  
 </div>     
</section>
<div id="mis"></div>
</form>	
 <script defer type="text/javascript" src="/js/shaajax.min.2.1.js" ></script> 
<script type="text/javascript">
var cat_nice='';
 $(document).ready(function() { 
 //alert('233');
 cat_nice='tt';
 var c=0;
 //alert('j');
 countr=document.getElementById('txt2').innerHTML;
 //alert(2);
//alert ( $(".imgtxtcontwag:last").attr('id'));
$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < 200) {	
	$('#loadMoreComments').show();
	//alert('height');
	$(window).data('ajaxready', false);
	//alert(111);
	if(c!=2){
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/new/dog-events/loadmore-bootstrap.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<a href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='text-align: right;font-weight: bold;color: #6c9d06;margin-right: 16px;font-size: 18px;width: 97.5%;text-align: center;margin: 10px 10px 20px;border: 1px solid #6c9d06;float: left;padding: 6px 10px;border-radius: 5px;' >See More </div></a>");
		
	c=0;}$('#loadMoreComments').hide();
	} // c condition close
	}

	});
	});
</script>

<script>
function get(rt){
	$('#divw'+rt).hide();
	c=0;

	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < '200') {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c!=3){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/new/dog-events/loadmore-bootstrap.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
		c=0;}$('#loadMoreComments').hide();
	
	} // c condition close-->
	}
	
	}

</script>

<script type="text/javascript">
	jQuery(document).ready(function($){
		var valueurl="https://www.dogspot.in/dog-events/";
		ShaAjaxJquary("/share-update-des.php?surl="+valueurl+"", "#mis", '', 'formcomnt', 'GET', '#loading', '','REP');
	});
</script>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/dog-show.css?v=2" />
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
