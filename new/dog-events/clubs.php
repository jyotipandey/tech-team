<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?>
<?
$snice_name=$section[0];
$selname=query_execute_row("SELECT kennel_id, kennel_name, city FROM kennel_club WHERE kennel_nicename='$snice_name'");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$selname['kennel_name']?> | <?=$selname['kennel_name']?> info | <?=$selname['kennel_name']?> images | DogSpot.in</title>
<meta name="keywords" content="<?=$selname['kennel_name'];?>, kennel club, Kennel club update details, Registered <?=$selname['kennel_name'];?>, KCI, DogSpot.in" />
<meta name="description" content="Find <?=$selname['kennel_name'];?> information, updated details of kennel clubs and Registered <?=$selname['kennel_name'];?> at DogSpot.in" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="canonical" href="https://www.dogspot.in/<?=$section[0]?>/" />
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<?php  require_once($DOCUMENT_ROOT . '/new/common/header.php');?> 
     <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-events/" class="butt_1" rel="nofollow">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2" rel="nofollow">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3" rel="nofollow">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4" rel="nofollow">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/new/dog-events/search.php" method="GET">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  

<!--nav-->
<div id="wrapper" class="clearfix"> 
     <div>
                
          <div  class="recent_show_albem" style="margin-top:10px;">
          	<h1><?=$selname['kennel_name'];?> </h1>
          <? 
		  $sqkdet=query_execute("SELECT kennel_name, contact_detail FROM kennel_club WHERE kennel_nicename='$snice_name'");
		  ?>
	<h2 style="width: 100%; padding-bottom: 10px; margin-top: 10px; font-weight: normal; font-size: 20px; text-align: left; color: #333; border:0px;">About Club </h2>
</div>     
      <div class="dogshow_sechduletable" style="margin-bottom:0px;">
      <? while($getcdet=mysql_fetch_array($sqkdet)){?>
         <table id="tbl_data" border="0" width="100%">
            <tbody><tr>
              <td width="25"><img src="/dog-show/Images/buttons/organized by.png" alt="Calendar" height="13" width="15"></td>
              <td width="597"><strong>Club Name:</strong> <?=$getcdet['kennel_name'];?></td>
            </tr>
            <tr>
              <td width="25"><img src="/dog-show/Images/buttons/Contact-icon.jpg" alt="Calendar" height="13" width="15"></td>
              <td width="597"><strong>Contact Detail:</strong> <?=$getcdet['contact_detail'];?></td>
            </tr>
            </tbody></table>
      <? }?>
      </div>
      
	  </div>
<?

 $selectShow12 = mysql_query("SELECT a.event_link_id, a.show_id, a.show_name, a.show_nicename, a.show_desc, a.location,
 a.date, a.kennel_link_id, b.ring_id FROM show_description as a, show_ring as b WHERE a.kennel_link_id='".$selname['kennel_id']."' AND a.show_id!='2' AND a.show_id!='123' AND a.show_id!='124' AND a.show_id=b.show_id  ORDER BY a.date DESC");
$sqlget=mysql_fetch_array($selectShow12);
$ring123=$sqlget['ring_id'];
	 if($ring123!=''){
		 $selectShow = mysql_query("SELECT * FROM show_description WHERE show_id != '2' AND show_id != '123' AND show_id != '124' AND kennel_link_id='".$selname['kennel_id']."' ORDER BY date DESC LIMIT 55");
 ?>   
          <div  class="recent_show_albem" style="margin-top:0px;">
	<h2  style="width: 100%; padding-bottom: 10px; margin-top: 0px; font-weight: normal; font-size: 20px; text-align: left; color: #333; border:0px;">Dog Show Results </h2>
</div>
  <div class="dogshow_sechduletable" style="margin-bottom:0px;">
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Address</th>
            <th class="" valign="top" width="129"> Date</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
 if(!$selectShow){	die(mysql_error());	}
	$dit=0;
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$kennel_link_id = $rowShow["kennel_link_id"];
	$event_link_id = $rowShow["event_link_id"];
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	$rowphoto =query_execute_row("SELECT image, image_nicename FROM photos_image WHERE album_id='$album_id' ORDER BY cover_img DESC");
	$imaged=$rowphoto['image'];
	$nicenamed=$rowphoto['image_nicename'];
	if($imaged){
	$src = $DOCUMENT_ROOT.'/photos/images/'.$imaged;
	$destm = $DOCUMENT_ROOT.'/imgthumb/50x50-'.$imaged;
	//echo $src;
	
	createImgThumbIfnot($src,$destm,'50','50','ratiowh');
	}
	$revent=query_execute_row("SELECT event_nice_name FROM events WHERE organized_by='$kennel_link_id'");
	//if($event_link_id!=''){
		$sqleselect=query_execute_row("SELECT event_nice_name FROM events WHERE event_id='$event_link_id'");
		//$enicename=$sqleselect['event_nice_name'];
		//}else{
			$enicename=$show_nicename;
			//}

	 $rowPri = query_execute_row("SELECT ring_id FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];	 
	 if($ring_id || $album_nicename){ $dit=$dit+1;
 if($ring_id ){
	 ?>  
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><? if($album_nicename!=''){?><a href="https://www.dogspot.in/photos/album/<?=$album_nicename?>/" target="_blank"><? }?>
	<img src="<?='/imgthumb/50x50-'.$imaged;?>" alt="<?=$show_name?>" title="<?=$show_name?>" width="50" height="33" align="middle"><? if($album_nicename!=''){?>
    </a><? }?>
					</td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><? if($ring_id!=''){?><a id="" class="show_title_link" href="/<?=$enicename?>/"><? }?>
				<?=$show_name;?><? if($ring_id!=''){?></a><? }?></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$location;?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <? print(showdate($date, "d M Y"));?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <? if($ring_id!=''){?><a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/<?=$enicename?>/"><? }?>Show Results<? if($ring_id!=''){?></a><? }?>
                </td>
              </tr>
			  <tr>

              </tr>
            <? 
			$s=$s+1;
			}
			   }if($dit==5){break;}}?>    
        </tbody></table>

      </div>
      <? } ?>
       <?
	  $sqlalu=query_execute_row("SELECT kennel_id FROM kennel_club WHERE kennel_nicename='$snice_name'");
      		$showdes=query_execute("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' AND organized_by='".$sqlalu['kennel_id']."' ORDER BY start_date ASC LIMIT 5");
		$sqccount=mysql_num_rows($showdes);
		if($sqccount!='0'){
			?>     
      
          <div  class="recent_show_albem" style="margin-top:0px;">
	<h2 style="width: 100%; padding-bottom: 10px; margin-top: 10px; font-weight: normal; font-size: 20px; text-align: left; color: #333; border:0px;">Upcoming Shows </h2>
</div>     
      <div class="dogshow_sechduletable" style="margin-bottom:0px;">
            
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Location</th>
            <th class="" valign="top" width="129"> Closing date</th>
            <th class="" valign="top" width="117"> Days left</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
$s=1;
while($getshowdata=mysql_fetch_array($showdes)){
$show_name=$getshowdata['event_name'];
$location=$getshowdata['venue'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['start_date'];
$edate=$getshowdata['end_date'];
$show_id=$getshowdata['link_id'];
$event_id=$getshowdata['event_id'];
$organized_by=$getshowdata['organized_by'];
$event_nice_name=$getshowdata['event_nice_name'];
$selke=query_execute_row("SELECT kennel_name, city FROM kennel_club WHERE kennel_id='$organized_by'");

$toDis=date("Y-m-d H:i:s");
$startTimeStamp = strtotime($edate, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);
$numberDays = $timeDiff/86400;  // 86400 seconds in one day
// and you might want to convert to integer
$numberDays = intval($numberDays);
?>        
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><img src="/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14"></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="/dog-events/<?=$event_nice_name;?>/">
				<?=$selke['kennel_name']."</br>".$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$selke['city'];?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="129"><? print(showdate($edate, "d M Y"));?></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <?=$numberDays?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <? if($show_id){?>
                    <a id="" class="showschedule_enter_online" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
                    <? }?>
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/dog-show/show-details.php?event_id=<?=$event_id?>">Show details</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon blueborder" <? }else{ ?> class="dark icon blueborder" <? }?> valign="top" width="62"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
               
        </tbody></table>
<? }?>
 <? 
 $qItem1=query_execute("SELECT album_name, album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE kennel_link_id='".$selname['kennel_id']."' order by cdate desc");
 $qItem111=query_execute_row("SELECT album_name, album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE kennel_link_id='".$selname['kennel_id']."' order by cdate desc");
 if($qItem111['album_name']!=''){ ?>
<div id="content">      
    <div  class="recent_show_albem" style="margin-top:0px;">
    <h2> Albums</h2>
</div>
  <div class="dog_show_wrapper">
  <div class="dog_show_box">
 <div class="dog_show_wrapper" style="margin-top:5px;">
  <div class="dog_show_box">
  <?php
$i=0;
while($rowItem = mysql_fetch_array($qItem1)){
	$album_id=$rowItem["album_id"];
	$album_link_name=$rowItem["album_link_name"];
	$i=$i+1;
	$album_name=$rowItem["album_name"];
	$nice_name=$rowItem["album_nicename"];
	
	$qdataM12=query_execute_row("SELECT cover_img FROM photos_image where album_id='$album_id' AND cover_img =  '1'");
	//$cover_image=$qdataM12['cover_img'];
	if($qdataM12) {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id' AND cover_img='1'");
	}
	else {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id'");	
	}
		$image=$qdataM["image"];
		$imagenice_name=$qdataM["image_nicename"];
		$title=$qdataM["title"];
		$qeventdate=query_execute_row("SELECT start_date FROM events WHERE photo_album_id='$album_id'");
		$datewe=$qeventdate['start_date'];
		if($datewe==''){
			$date=showdate($qdataM["cdate"], "d M o");
						
			}else{
				$date=showdate($datewe, "d M o");
				}
		
	if($image){
	$src = $DOCUMENT_ROOT.'/photos/images/'.$image;
	$destm = $DOCUMENT_ROOT.'/imgthumb/200x134-'.$image;
	//echo $src;
	
	createImgThumbIfnot($src,$destm,'200','134','ratiowh');

 ?>
 <div class="dogshows_wrapperBox" <? if ($i%2 == 0){ echo "style='margin-right:0'";} ?>>
                
					<div class="dog_shows_thumbnail">

						<a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank">
	<img src="<?='/imgthumb/200x134-'.$image;?>" alt="<?=$title?>" title="<?=$title?>" height="134" width="" align="middle"></a>
					</div>

					<div class="dog_shows_title">					
						<p class="dog_name"><a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank" ><? echo snippetwop($album_name, 18, '');
?></a></p>
<?
 $qcount= query_execute_row("SELECT count(*) as image from photos_image where album_id='$album_id'");?>
                           <p class="dog_date"><? echo $qcount["image"]." "."Photos";?> | <?=$date?>               </p>					
					
					</div>

				</div>
                 <? // echo $i;      
} 

} 
?>
				</div>
				
 
 
 </div>
				
				</div>
 </div>

      </div>
      <? }?>
      </div>
      
  </div>
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>