<?php
include('../../constants.php');
include($DOCUMENT_ROOT."/session-no.php"); 
include($DOCUMENT_ROOT."/functions.php");
//include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");

$sitesection = "dog-events";
 if($userid=='jyoti05')
  {
require_once($DOCUMENT_ROOT . '/new/dog-events/index-bootstrap.php');
  exit;
  } 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<link rel="canonical" href="https://www.dogspot.in/dog-events/" />
<link rel="amphtml" href="https://www.dogspot.in/amp/dog-events/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/dog-events/" />
<title>Dog Show | Dog Events | Dog Championship | Kennel Club India</title>
<meta name="keywords" content="Dog Show, Dog Events, Dog Championship, dog show albums, KCI, Kennel Club India events." />
<meta name="description" content="Find the Dog show results, online show entry details, recent articles on dogs as well as old events albums." />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.dogspot.in/dog-events/" /> 
<meta property="og:title" content="DOGSPOT FEATURING DOG SHOWS, PICTURES, RESULTS & SCHEDULE" /> 
<meta property="og:description" content="Find the Dog show results, online show entry details, recent articles on dogs as well as old events albums." />
<meta property="og:image" content="https://www.dogspot.in/new/pix/dog-show-2016.jpg" />



<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<link href="/dog-show/css/layout.css?v=199" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/dog-show/js/jsmain.js" ></script>
<script type="text/javascript" src="/dog-show/js/fade.js"></script> 
<?php /*?><script src="/dog-show/js/cufon-yui.js" type="text/javascript"></script><?php */?>
<script type="text/javascript" src="/dog-show/js/main.js" ></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>

<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<script type="text/javascript"  defer="defer" src="/social-share/social-share.min.js"></script> 
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});

</script>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
<script type="text/javascript">
var cat_nice='';
 $(document).ready(function() { 
 //alert('233');
 cat_nice='tt';
 var c=0;
 //alert('j');
 countr=document.getElementById('txt2').innerHTML;
 //alert(2);
//alert ( $(".imgtxtcontwag:last").attr('id'));
$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	//alert('height');
	$(window).data('ajaxready', false);
	//alert(111);
	if(c!=2){
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/dog-show/loadmore.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<a href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='text-align: right; font-weight: bold; color: #668000; margin-right: 16px; font-size: 18px;' >See More </div></a>");
		
	c=0;}$('#loadMoreComments').hide();
	} // c condition close
	}

	});
	});
</script>
<script>
function get(rt){
	$('#divw'+rt).hide();
	c=0;

	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c!=3){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/dog-show/loadmore.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
		c=0;}$('#loadMoreComments').hide();
	
	} // c condition close-->
	}
	
	}

</script>
<?php  require_once($DOCUMENT_ROOT . '/new/common/header.php');?>

<div class="dogshow-slider-sec" >
 <div id="dogshow_banner_holder" style="background-color: black;" >
 
      <div id="dogshow_banner" ><?php /*?> <img src="/dog-show/Images/slide-banner/dog-show-3new1.jpg" alt="" class="active" width="100%" height="319" /><?php */?>
     </div>
     <div id="wrapper" class="clearfix">
      <div id="slideshowtext">
        <h1>Making your<br />
          show a success</h1>
        <p>DogSpot is the power behind a successful dog show. With almost 20 years of experience with dogs, we take away the hassle to ensure your show runs smoothly and all of the pre and post-show data to be accessible on one portal. </p>
        <a href="/new/dog-events/enter-online.php" class="dogshow_enter">Enter on-line</a> <a href="/dog-events/about-us/" class="dogshow_running">Running a show?</a> </div>
      <div id="show_result"> 
        <div class="show_title">Upcoming Shows</div>
        <ul>
<?  $var = date("Y-m-d h:i:s");
  						$var1 = date("Y-m-d");
						
  $query = mysql_query("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' ORDER BY start_date ASC LIMIT 4");
 if(!$query){	die(mysql_error()); 	}	
  while($rowArt = mysql_fetch_array($query)){
	$event_id = $rowArt["event_id"];
	$venue = $rowArt["venue"];
	$country_name = $rowArt["country_name"];
	$city_name = $rowArt["city_name"];
	$start_date = $rowArt["start_date"];
	$event_name = $rowArt["event_name"];
	$event_nice_name = $rowArt["event_nice_name"];
	$organized_by = $rowArt["organized_by"]; 
	$sellid=query_execute_row("SELECT kennel_name FROM kennel_club WHERE kennel_id='$organized_by'");
	?>            
        <li>
            <a id="" href="/dog-events/<?=$event_nice_name?>/"><? if($sellid['kennel_name']!=''){ echo $sellid['kennel_name']; }else{ print $event_name; } ?>             
                <span class="date"><? print(showdate($start_date, "l, d M, o")); ?></span>
            </a>
        </li>
    <? }?>
    <a href="/dog-events/show-schedules/" class="view-more">view more &raquo;</a>
        </ul>
      </div>
    </div>
    </div></div>
	<!-- slideshow--> 
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-events/" class="butt_1" rel="nofollow">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2" rel="nofollow">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3"  rel="nofollow">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4" rel="nofollow">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   	<div class="ds_searchboxl">
        <form action="/new/dog-events/search.php" method="GET" id="formcomnt" name="formcomnt">
            <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input" style="    width: 339px;">
            <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
        </form>
   	</div>
</div>
</div>
</div>  

<!--nav-->
<div id="wrapper" class="clearfix">

  
    

    <div>
	
	<!-- search box end-->
     <div>

    <!-- content--> 
	
	<div id="content">
<!-- slideshow-->        
   
	
	
    <div id="Dogshow_box">
	<div class="box_list"> 
  <div class="box_show_title">Show Results</div>
        <ul>
<?
$selectShow = mysql_query("SELECT * FROM show_description WHERE show_id != '2' AND show_id != '123' AND show_id != '124' ORDER BY date DESC LIMIT 55");
 if(!$selectShow){	die(mysql_error());	}
	$dit=0;
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT * FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 
	 if($ring_id){ 
	 	$dit=$dit+1;
?>   
        <li>
            <? if($ring_id ){?> <a id="" href="/<?=$show_nicename?>/" style="color:#162E44;" title="<?=$show_name?> Dog Show Results"><?=$show_name?>,              
                <span class="date"><? print(showdate($date, "d M Y")); ?></span></a><? }?>
        </li>
    <? }if($dit==5){break;}}?>
    <a href="/show-results/" class="view-more">view more &raquo;</a>        </ul>
      </div>
      <!-- #EndLibraryItem -->
      <div id="box_news" class="margin_left">
	  <?php /*?>#BeginLibraryItem "/Library/recentnews.lbi"<?php */?>
	  
	  <div class="box_show_title">Recent Articles</div>
	  <?php /*?><marquee id="box_news" direction="up" onmouseover="this.stop();" onmouseout="this.start();"><?php */?>
        
        <? //echo "SELECT * FROM wp_posts WHERE (post_content like '%dog shows%' OR post_title like '%dog shows%') AND post_status='publish' ORDER BY post_date DESC limit 2";
			$selectMC = mysql_query("SELECT * FROM wp_posts WHERE (post_content like '%dog shows%' OR post_title like '%dog shows%') AND post_status='publish' ORDER BY post_date DESC limit 2");
						
					  if(!$selectMC){	die(mysql_error());	} ?>
 <!-- content -->
                             <? $i=0;
							  while($rowArt = mysql_fetch_array($selectMC)){
								$i=$i+1;  
							//	$articlecat_id = $rowArt["articlecat_id"];
								$article_id = $rowArt["ID"];
								$art_subject = $rowArt["post_title"];
								$art_body = $rowArt["post_content"];
								$c_date = $rowArt["post_date"];
								$artuser = $rowArt["post_author"];
								$art_name = $rowArt["post_name"];
								  
						$art_subject = stripslashes($art_subject);
						$art_subject = breakLongWords($art_subject, 12, " ");
					
					// Get Post Teaser
						$art_body = stripslashes($art_body);
						$art_body = strip_tags($art_body);
						$art_body = trim($art_body);
						$art_body = substr($art_body,0,100);
					
						$art_body = stripslashes($art_body);
						$art_body = breakLongWords($art_body, 30, " ");
					 
					  ?>
					 <div class="dog_show_recentNews">
                    
                     
                      <p class="recent_news_title"><a href="<? echo"/$art_name/";?>"><? echo"$art_subject"; ?></a></p>
                   <p><? echo"$art_body...";?><? echo " <a href='/$art_name/' class='recent_news_readmore'> Read More</a>"; ?></p>
     <?php /*?>    <br />
          <span style="color:#666; font-style:italic;"><? if(($artuser == $userid || $sessionLevel == 1 ) && $userid != "Guest"){ echo"<a   href='/new/articles/articles_new.php?article_id=$article_id'>Edit</a>";?> | <a href="<? echo"javascript:confirmDelete('/new/articles/articles.php?article_id=$article_id&del=del&reDirURL=$refUrl')"; ?>" style="text-decoration:none;">Delete</a> | <? } ?> Posted by :<a href="/profile/<? echo"$artuser"; ?>/" style="color:#a33b00;"> <? print getTeaser(dispUname($artuser),10); ?> </a>| <? print(showdate($c_date, "l, d M, o")); ?></span></p><?php */?>
					 	</div>  <!-- class="imgtxtcont"  above div         -->
                    
							  <? } ?>
                               <div class="recent_news_viewmore"><a href="/dog-blog/events/" class="view-more">view more &raquo;</a></div>
      <?php /*?></marquee><?php */?>
	  <!-- #EndLibraryItem -->
      </div>
      
      <div  id="box_show" class="margin_left">
    <?php /*?>#BeginLibraryItem "/Library/showentry.lbi" <?php */?>
    <div id="box_show">
	
  <div class="box_show_title">Show entry</div>
        <a href="/new/dog-events/enter-online.php" class="dogshow_enterbtn">Enter <br />
          on-line</a> </div><!-- #EndLibraryItem -->
          </div>
    </div>
	<!-- three boxes-->
    
    <div  class="recent_show_albem" style="margin-top:0px;">
    	<div style="width:100%; float:left; margin-top:20px; border-bottom:2px solid #9BC105; padding-bottom: 10px;">
        <div style="float:left; width:50%;">
			<h2 style="float:left; width:50%; border:none;">Recent Albums </h2></div>
            <div style=" text-align:right;  float: left;
    width: 50%;">
            <? 
				$surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				include($DOCUMENT_ROOT."/Adoption1/social1_article.php"); 
			?>
            
            </div>
        	
        </div>
</div>
          <div class="rytPost_list" id="rytPost_list">
         <? $iid=0;
 ?>   
  <!-- dog show box star-->
  <div class="dog_show_wrapper imgtxtcontwag" id='<?=$iid?>'>
  <div class="dog_show_box">
  <?php
$qItem1=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name, album_link_id FROM photos_album
WHERE  album_link_name ='event' order by cdate desc limit 0,12 ");
$qItem1all=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE  album_link_name ='event' order by cdate desc");
$totrecord = mysql_num_rows($qItem1all);
$i=0;
		$div_count='0';
while($rowItem = mysql_fetch_array($qItem1)){
				$iid++;
		$div_count++;
	$album_id=$rowItem["album_id"];
	$album_link_name=$rowItem["album_link_name"];
	$i=$i+1;
	$album_name=$rowItem["album_name"];
	$nice_name=$rowItem["album_nicename"];
	
	$qdataM12=query_execute_row("SELECT cover_img FROM photos_image where album_id='$album_id' AND cover_img =  '1'");
	//$cover_image=$qdataM12['cover_img'];
	if($qdataM12) {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id' AND cover_img='1'");
	//echo "SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where    album_id='$album_id'";
	}
	else {
	//	echo "SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where    album_id='$album_id' AND cover_img='1'";
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id'");	
	}
	$sqldatsh=query_execute_row("SELECT date FROM show_description WHERE show_id='".$rowItem['album_link_id']."'");
		$image=$qdataM["image"];
		$imagenice_name=$qdataM["image_nicename"];
		$title=$qdataM["title"];
		if($sqldatsh['date']!=''){ $date=showdate($sqldatsh["date"], "d M o"); }else{ $date=showdate($qdataM["cdate"], "d M o");}
	if($image){
	$src = $DOCUMENT_ROOT.'/photos/images/'.$image;
	$destm = $DOCUMENT_ROOT.'/imgthumb/200x134-'.$image;
	//echo $src;
	
	createImgThumbIfnot($src,$destm,'200','134','ratiowh');

 ?>
 <div class="dogshows_wrapperBox" <? if ($i%2 == 0){ echo "";} ?>>
                
					<div class="dog_shows_thumbnail">
<a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank" >
							
	<img src="<?='/imgthumb/200x134-'.$image;?>" alt="<?=$title?>" title="<?=$title?>" height="134" width="200" align="middle"></a>

						
					</div>

					<div class="dog_shows_title">					
						<p class="dog_name"><a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank" ><? echo snippetwop($album_name, 26, '');
?></a></p>
<?
 $qcount= query_execute_row("SELECT count(*) as image from photos_image where album_id='$album_id'");?>
                           <p class="dog_date"><? echo $qcount["image"]." "."Photos";?> | <?=$date?> </p>					
					
					</div>

				</div>
                 <? // echo $i;      
} 
?>
<? if($div_count=='4'){ 
			$div_count='0';
			?>
				</div>
				 
 </div>
 <div class="dog_show_wrapper imgtxtcontwag" id='<?=$iid?>'>
  <div class="dog_show_box">
 <? }} ?></div>
<div id='loadMoreComments' style='display:none'><img src="/new/pix/loading2.gif" /></div>
		<div id="txt1" style='display:none; cursor:pointer' ><? echo $cattype; ?></div>
<div id="txt2" style='display:none'><? echo $totrecord-1; ?></div></div>
  
 </div>     
	  </div>
	  <!-- dog show box star-->
	<!-- content-->

  
  </div>
  </div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var valueurl="https://www.dogspot.in/dog-events/";
		ShaAjaxJquary("/share-update-des.php?surl="+valueurl+"", "#mis", '', 'formcomnt', 'GET', '#loading', '','REP');
	});
</script>
<!-- InstanceEnd -->
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
