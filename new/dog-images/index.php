<?php

/* 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");*/
include_once($DOCUMENT_ROOT."/functions.php");
include_once($DOCUMENT_ROOT."/database.php");
include_once($DOCUMENT_ROOT."/arrays.php");
include_once($DOCUMENT_ROOT."/session.php");
if($breed)
{
$breed_name=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$breed'");
$b_name=$breed_name['breed_name'];	
$title="$b_name Dog and Puppies Images | DogSpot.in";
$keys="$b_name Dog Images, $b_name Puppies Images, $b_name Dog Pictures, $b_name Dog Photos, $b_name Puppies Pictures, $b_name Puppies Photos, $b_name Pics of Puppies, $b_name Dog and Puppies Images.";
$desc="Every dog breed has a different type of appearance and by capturing these appearance at a right time we have made an great collection of images of these dogs. Come and check out the images of $b_name dog here at DogSpot.in";	
}else
{
$title="Dog Images | Puppies Photos Free Download | DogSpot.in";
$keys="Dog Images, Puppies Images, Dog Pictures, Dog Photos, Puppies Pictures, Puppies Photos, Pics of Puppies,Dog and Puppies Images";
$desc="Dogs are very important part of our life and everyone is very much interested in the images and pictures of their loved ones. So, find out the cute and adorable images of these cute dog and puppies.";	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title?></title>
<meta name="keywords" content="<?=$keys?>" />
<meta name="description" content="<?=$desc?>" />
<meta property="fb:app_id" content="119973928016834" />
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="images" />
<meta property="og:url" content="https://www.dogspot.in/photos/" />
<meta property="og:title" content="Cute Dog and Puppies Images" />
<meta property="og:description" content="Dogs are very important part of our life and everyone is very much interested in the images and pictures of their loved ones. So, find out the cute and adorable images of these cute dog and puppies.
" />
<link rel="canonical" href="https://www.dogspot.in<?=$_SERVER[REQUEST_URI]?>" />
<link rel="amphtml" href="https://www.dogspot.in/amp<?=$_SERVER[REQUEST_URI]?>" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/<?=$_SERVER[REQUEST_URI]?>" >
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<style>
  .dog-names{
    margin: 4px;
    width: 70%;
  }
 .dog-names > ul{
        padding: 0;
        list-style: none;
        background: #fff;
        border: 1px solid #f4f8f9;
    }
  .dog-names > ul li{
        display: block;
        position: relative;
        line-height: 32px;
        text-align: left;
        font-size: 16px;
    }
   .dog-names > ul li a{
        display: block;
        padding: 8px 25px;
        color: #333;
        text-decoration: none;
    }
   .dog-names > ul li a:hover{
        color: #fff;
        background: #939393;
    }
    .dog-names > ul li ul.dropdown{
        min-width: 100%; /* Set width of the dropdown */
        height: 200px;
        overflow-y: scroll;
        background: #fff;
        display: none;
        font-size: 16px;
        position: absolute;
        z-index: 999;
        left: 0;
    }
    .dog-names:hover ul.dropdown{
        display: block; /* Display the dropdown */
    }
    ul li ul.dropdown li{
        display: block;
    }
  }
  .selectedB{
    color: red;
    font-size: x-large;
    font-family: fantasy;
  }
  .head-img{
    background-color: #000;
  }
</style>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php');?>
<section class="dog-image">
<div class="<? if($b_name){echo('head-img');}else{echo('image-header');} ?>"> 
<h1><? if($b_name){echo $b_name." Images";}else{echo "Dog Images";} ?></h1>
<h2>Dogs leave paw prints on your heart	&#10084;</h2>
</div>
<div class="breadcrumb" style="position: absolute; margin-top: -29px; z-index: 999999999">
 
  <div class="container" itemscope="" itemtype="http://schema.org/Breadcrumb" style="text-align: left;color:#fff;">
    <div class="breadcrumb_cont" itemscope="" itemtype="http://schema.org/BreadcrumbList" style="    margin-left: 18px" > 
    <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
     <a href="/" itemprop="item"><span itemprop="name" style="color:#fff;">Home</span></a>
      <meta itemprop="position" content="1"> </span>
     <span> / </span>
     <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
      <span itemprop="name" class="brd_font_bold" style="color:#f2f2f2;">Dog Photos
</span>
       <meta itemprop="position" content="2"> </span>
     </div>
    <div class="cb"></div>
    
  </div>
</div>
<div class="col-md-12 col-lg-12">
<div class="col-md-6 col-lg-6" style="margin-top: 15px;text-align: right;">
      <?
      if($b_name){
        ?>
          <a href="/<?=$breed;?>/" style="font-size: 16px;color: red;border:1px solid red;padding: 7px;"><?=$b_name;?> Dog Breed Information</a>
        <?
      }
        ?>
    </div>
<div class="container col-md-6" style="margin-bottom:0px;">
    <div class="dog-names">
           <ul>
             <li>
               <?
                if($b_name){
                  print_r($b_name);
                }else{
                  echo("Select Breed");
                }
               ?>
               <ul class="dropdown">
               </ul>
             </li>
           </ul>
    </div>
    </div>
          <div class="image-bottom">
<div class="container">
           <input type="hidden" name="user_id" id="user_id" value="<?=$userid?>"  />	
      	   <div id='loadMoreComments' style='display:none'></div>
       		<div>
            <ul id="og-grid" class="og-grid">
             <div class="rytPost_list" id="rytPost_list">

              <? if($b_name){ //'hello';
				 $iid=0;
			//	 echo "SELECT pi.image,image_id,new_keywords FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%' ORDER BY image_id ASC LIMIT 12";
			 $breedI=query_execute("SELECT pi.image,image_id,new_keywords FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%' ORDER BY image_id ASC LIMIT 30");
			 $breedIList=query_execute("SELECT pi.image,image_id,new_keywords FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%'");
			 $countBreedImg=mysql_num_rows($breedIList);?>	 <div id="txt1" style='display:none; cursor:pointer' ><?=$b_name?> images</div>
      <div id="txt2" style='display:none'><?=$countBreedImg?></div><?
			 while($dog_details=mysql_fetch_array($breedI)){
			 $image=$dog_details['image'];
			 $new_keywords=$dog_details['new_keywords'];
			 $image_id=$dog_details['image_id'];
			// $img="https://ik.imagekit.io/2345/tr:h-500,w-700/photos/images/".$image;
			 $iid++;
			 $src = $DOCUMENT_ROOT.'/photos/images/'.$image;
			 $imageURL = '/imgthumb/700x500-'.$image;
			 $dest = $DOCUMENT_ROOT.$imageURL;
		createImgThumbIfnot($src,$dest,'700','500','ratiowh');
			 ?>
             <li class="gallery-img imgtxtcontwag" id='<?=$iid?>'> <a> <img src="<?=$imageURL?>" alt="<?=$new_keywords?> image" title="<?=$new_keywords?> image"> </a>
             <? $wag_activity=mysql_query("SELECT * FROM photo_activity_wag WHERE image_id='$image_id' AND userid='$userid'");
$wag_like1=mysql_num_rows($wag_activity);
if($wag_like1==0){ if($userid=='Guest'){ ?>
                    <div class="like-icon-box" id="postwagbefore<?=$image_id?>">
                    <a href="/login.php?refUrl=/photos/" data-ajax="false" rel="nofollow">
                    <img src="/dog-images/images/heart-icons.png" style=" width: 30px; height: auto; " /></a>
                    </div>
                    <? }else{?>
                     <div class="like-icon-box" id="postwagbefore<?=$image_id?>">
                    <a onclick="wagfuncountImg('<?=$image_id?>');">
                    <img src="/dog-images/images/heart-icons.png" style=" width: 30px; height: auto; " /></a>
                    </div>
                   
                    <? }}else{ ?>
                      <div class="like-icon-box" id="postwagafter<?=$image_id?>">
                    <a href="javascript:void(0)" onclick="wagcountImg('<?=$image_id?>');">
                    <img src="/dog-images/images/heart-red-icon.png" style=" width: 30px; height: auto; " /></a>
                   </div><? }?>
 <div class="like-icon-box" id="postwagafter<?=$image_id?>" style="display:none">
                    <a onclick="wagcountImg('<?=$image_id?>');">
                    <img src="/new/dog-images/images/heart-red-icon.png"  style=" width: 30px; height: auto; " /></a>
                   </div>
 <div class="like-icon-box" id="postwagbefore<?=$image_id?>" style="display:none">
                    <a onclick="wagfuncountImg('<?=$activity_id?>');">
                    <img src="/new/dog-images/images/heart-icons.png" style=" width: 30px; height: auto; "  /></a>
                 </div><span id="wag" style="display:none"></span>
             </li>
       <? }  ?>
		 <? }else{ $iid=0;
		 $getbestImage=query_execute("SELECT image,da.dog_id,da.dog_breed,daa.id FROM dogs_activity as daa,dogs_available as da WHERE daa.dog_id=da.dog_id AND daa.dog_image !='no' AND daa.publish_status='publish' ORDER BY id ASC LIMIT 12"); 
?>	 <div id="txt1" style='display:none; cursor:pointer' >Dog images</div>
      <div id="txt2" style='display:none'>99</div><?		
 while($dog_details=mysql_fetch_array($getbestImage)){
			 $dog_breed=$dog_details['dog_breed'];
			 $dog_id=$dog_details['dog_id'];
			 $activity_id=$dog_details['id'];
			 $img=$DOCUMENT_ROOT."/dogs/images/".$dog_details['image'];
			 if($img){
			$src = $DOCUMENT_ROOT.'/dogs/images/'.$dog_details['image'];
			$imageURL1='/dogs/images/700x500_'.$dog_details['image'];
			$imageURL2='/dogs/images/280x200_'.$dog_details['image'];
		   
		    }
		$dest = $DOCUMENT_ROOT.$imageURL1;
		$dest2 = $DOCUMENT_ROOT.$imageURL2;
	    createImgThumbIfnot($src,$dest,'700','500','ratiowh'); 
	    createImgThumbIfnot($src,$dest2,'280','200','ratiowh'); 
	   
	    $image_info01 = getimagesize($dest);		
		$image_width01 = $image_info01[0];
		$image_height01 = $image_info01[1];
		$iid++;
			 ?>  
				    <li class="gallery-img " id='<?=$iid?>'>
                     <input type="hidden" name="dog_id" id="dog_id" value="<?=$dog_id?>"  />    
                    <a><img src="<?=$imageURL1?>" alt="<?=$dog_breed?> image" title="<?=$dog_breed?> image">
                    </a>
                     <? $wag_activity=mysql_query("SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND userid='$userid' AND activity_id='$activity_id'");
$count_activity_wag=mysql_query("SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND activity_id='$activity_id'");
$count_activity=mysql_num_rows($count_activity_wag);
$wag_like1=mysql_num_rows($wag_activity);
if($wag_like1==0){ if($userid=='Guest'){ ?>
                    <div class="like-icon-box" id="postwagbefore<?=$activity_id?>">
                    <a href="#modal" id="modal_trigger21" class="dog-images" rel="nofollow" >
                    <img src="/new/dog-images/images/heart-icons.png" /></a>
                    </div>
                    <? }else{?>
                     <div class="like-icon-box" id="postwagbefore<?=$activity_id?>">
                    <a onclick="wagfuncount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/new/dog-images/images/heart-icons.png" /></a>
                    </div>
                   
                    <? }}else{ ?>
                      <div class="like-icon-box" id="postwagafter<?=$activity_id?>">
                    <a href="javascript:void(0)" onclick="wagcount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/new/dog-images/images/heart-red-icon.png" /></a>
                   </div><? }?>
 <div class="like-icon-box" id="postwagafter<?=$activity_id?>" style="display:none">
                    <a onclick="wagcount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/new/dog-images/images/heart-red-icon.png" /></a>
                   </div>
 <div class="like-icon-box" id="postwagbefore<?=$activity_id?>" style="display:none">
                    <a onclick="wagfuncount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/new/dog-images/images/heart-icons.png" /></a>
                 </div>

<span id="wag<?=$activity_id?>"></span>
                    </li>
                     
                    <? }?>
                    <? }?>
                    </div>
                   
				</ul>
			</div>
</div></div> 
 </section>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<script src="/new/dog-images/js/dogimage-slider.js?v=ioxnza" type="text/javascript"></script> 
<script type="text/javascript">
var cat_nice='';
 var c=0;
 $(document).ready(function() { 
 //alert('233');
 cat_nice=document.getElementById('txt1').innerHTML;

 //alert('j');
 countr=document.getElementById('txt2').innerHTML;
 //alert(2);
//alert ( $(".imgtxtcontwag:last").attr('id'));
$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/new/dog-images/loadmore.php?b_name=<?=$b_name?>&cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	c=c+1; 
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	// c condition close
	}

	});
	});
	function get(rt){
	$('#divw'+rt).hide();
	c=0;

	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c==0){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/new/dog-images/loadmore.php?b_name=<?=$b_name?>&cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
// c condition close-->
	}
	
	}
</script>

<script>
function wagfuncount1(activity_id,dog_id)
{
var user=$("#user_id").val();
if(user){

 ShaAjaxJquary("/wag_club/wag_insert.php?activity_wag=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
}
}
function wagcount1(activity_id,dog_id)
{
var user=$("#user_id").val();
//var dog_id=$("#dog_id").val();
if(user){
ShaAjaxJquary("/wag_club/wag_insert.php?activity_wag_delete=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
}
}
function wagfuncountImg(image_id){
	var user=$("#user_id").val();
if(user){
	ShaAjaxJquary("/new/dog-images/wag_insert.php?activity_wag=1&user="+user+"&image_id="+image_id+"", "#wag", '',  '', 'POST', "", '','REP')
}
}

function wagcountImg(image_id){
	var user=$("#user_id").val();
if(user){
	ShaAjaxJquary("/new/dog-images/wag_insert.php?activity_wag_delete=1&user="+user+"&image_id="+image_id+"", "#wag", '',  '', 'POST', "", '','REP')
}
	
}
</script>
<script src="/new/js/jquery.leanModal.min.js" type="text/javascript"></script>
<script type="text/javascript">
          $('.gallery-img').Am2_SimpleSlider();
		    $(document).ready(function(){ 
			$(".dog-images").click(function(){
	  		$("#tab-1").removeClass("current");
            $("#tab-2").addClass("current");
			$("#login_email_new").focus();
			$("#cust_email_new").blur();
			$(".dog-images").leanModal({top : 120, overlay : 0.6, closeButton: ".modal_close" });
    }); });
       </script>
       
<script>
function callpage(urlre)
{
window.location.replace(urlre);	
}
</script>  
<script>
        //When the page has loaded.
        $( document ).ready(function(){
            //Perform Ajax request.
            $.ajax({
                url: '/new/dog-images/getbreedata.php?b_name=<?=$b_name?>',
                type: 'get',
                success: function(data){
                    //If the success function is execute,
                    //then the Ajax request was successful.
                    //Add the data we received in our Ajax
                    //request to the "content" div.
                    $('.dropdown').html(data);
                    var x = $('.selectedB').content();
                    alert(x);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                  //  var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                  //  $('#content').html(errorMsg);
                  }
            });
        });
        </script>     
<link type="text/css" rel="stylesheet" href="/new/dog-images/css/image-style.css?v=8902" />      
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>