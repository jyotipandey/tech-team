<?php 
include($DOCUMENT_ROOT."/constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
include($DOCUMENT_ROOT."/dog-blog/catarray.php");

//require_once($DOCUMENT_ROOT.'/facebookconnect/facebooksettings.php');
//require_once($DOCUMENT_ROOT.'/facebookconnect/facebookfunctions.php');
//require_once($DOCUMENT_ROOT.'/twitterconnect/twitterfunctions.php');
 $sitesection = "dog-business";




if($business_name){  $business_name = stripslashes($business_name);   }


// Get Business Informaton
	$selectArt = mysql_query ("SELECT comp_org, bus_nicename FROM business_listing WHERE business_id = '$business_id'");
	if (!$selectArt){		die(mysql_error());	}	
	$rowArt = mysql_fetch_array($selectArt);
	$comp_org = stripslashes($rowArt["comp_org"]);
	$bus_nicename = $rowArt["bus_nicename"];
// Get Business Inforamtion END

// Global Image functions
//define a maxim size for the uploaded images
define ("MAX_SIZE","10000"); 
// define the width and height for the thumbnail
// note that theese dimmensions are considered the maximum dimmension and are not fixed, 
// because we have to keep the image ratio intact or it will be deformed
define ("WIDTH","160"); 
define ("HEIGHT","120");  

define ("LWIDTH","800"); 
define ("LHEIGHT","600"); 

// This variable is used as a flag. The value is initialized with 0 (meaning no error found) 
//and it will be changed to 1 if an errro occures. If the error occures the file will not be uploaded.
$siteRoot = $DOCUMENT_ROOT;
$imgFolder = "/dog-listing";
$errors=0;
// Global Image functions END
// Image Upload From URL
if(isset($_POST["upimgUrl"])){
	if($img_url){
		define('ALLOWED_FILENAMES', 'jpg|jpeg|gif|png');
		// define a directory the webserver can write to
		define('IMAGE_DIR', 'dog-listing/temp-img');
		// check against a regexp for an actual http url and for a valid filename, also extract that filename using a submatch (see PHP's regexp docs to understand this)
		if(!preg_match('#^http://.*([^/]+\.('.ALLOWED_FILENAMES.'))$#', $_POST['img_url'], $m)) {
		  die('Invalid url given');
		}
		// try getting the image
		if(!$img = file_get_contents($_POST['img_url'])) {
		  die('Getting that file failed');
		}
		// try writing the file with the original filename -- note that this will overwrite any existing filename in the same directory -- that's up to you to check for
		if(!file_put_contents(IMAGE_DIR.'/'.$m[1], $img)) {
		  die('Writing the file failed');
		}else{
			$temp_file = IMAGE_DIR.'/'.$m[1];
			$size=getimagesize($temp_file);
			$sizekb=filesize($temp_file);
			
			$extension = getExtension($temp_file);
			$extension = strtolower($extension);
			//compare the size with the maxim size we defined and print error if bigger
			if ($sizekb > MAX_SIZE*1024){
				echo '<h1>You have exceeded the size limit!</h1>';
				$errors=1;
			}
			
			$p_img=$bus_nicename.'.'.$extension;	
			// the new thumbnail image will be placed in images/thumbs/ folder
			$thumb_name= $siteRoot.$imgFolder.'/images/thumb_'.$p_img;
			$large_name=$siteRoot.$imgFolder.'/images/'.$p_img;
			// call the function that will create the thumbnail. The function will get as parameters 
			//the image name, the thumbnail name and the width and height desired for the thumbnail
			$thumb=make_thumb($temp_file,$thumb_name,WIDTH,HEIGHT);
			$reSize=make_thumb($temp_file,$large_name,LWIDTH,LHEIGHT);
			
			$DelFilePath = "$temp_file";
			if (file_exists($DelFilePath)) { unlink ($DelFilePath); }
			
			if($temp_file){
				// Insert data
				$edit = mysql_query ("UPDATE business_listing SET logo = '$p_img' WHERE business_id = '$business_id'");									
			}
				
		}// if file uploaded on the server
	}// If is Image imageUrl
} // END upimgUrl
// Image Upload From URL END----------------------------------------------------------------------

// Photo Upload
if(isset($_POST["upimg"])){
	// checks if the form has been submitted
		if(isset($_POST['upimg'])){
			//reads the name of the file the user submitted for uploading
			
			$image=$_FILES['image']['name'];
			// if it is not empty
			if ($image) {
			// get the original name of the file from the clients machine
			$filename = stripslashes($_FILES['image']['name']);
		
			// get the extension of the file in a lower case format
			$extension = getExtension($filename);
			$extension = strtolower($extension);
			// if it is not a known extension, we will suppose it is an error, print an error message and will not upload the file, otherwise we continue
			if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
				echo "<h1>Unknown extension! $extension </h1>";
				$errors=1;
			}else{
				// get the size of the image in bytes
				// $_FILES[\'image\'][\'tmp_name\'] is the temporary filename of the file in which the uploaded file was stored on the server
				$size=getimagesize($_FILES['image']['tmp_name']);
				$sizekb=filesize($_FILES['image']['tmp_name']);
				
				//compare the size with the maxim size we defined and print error if bigger
				if ($sizekb > MAX_SIZE*1024){
					echo '<h1>You have exceeded the size limit!</h1>';
					$errors=1;
				}
				
				//we will give an unique name, for example the time in unix time format
				$p_img=$bus_nicename.'.'.$extension;
				//the new name will be containing the full path where will be stored (images folder)
				$newname="$siteRoot$imgFolder/images/".$p_img;
				$img_info = mime_content_type($_FILES['image']['tmp_name']);
				if($img_info=="image/gif" || $img_info=="image/png" || $img_info=="image/jpeg" || $img_info=="image/jpg"){
					$copied = copy($_FILES['image']['tmp_name'], $newname);
				}else{
					echo "<div class='notice' style='width:500px;'>Sorry, Your Image File is not valid.Please upload an image(jpg/jpeg/gif/png)</div>";	
				}
				//we verify if the image has been uploaded, and print error instead
				if (!$copied) {
					echo '<h1>Copy unsuccessfull!</h1>';
					$errors=1;
				}else{
					// the new thumbnail image will be placed in images/thumbs/ folder
					$thumb_name= $siteRoot.$imgFolder.'/images/thumb_'.$p_img;
					// call the function that will create the thumbnail. The function will get as parameters 
					//the image name, the thumbnail name and the width and height desired for the thumbnail
					$thumb=make_thumb($newname,$thumb_name,WIDTH,HEIGHT);
					$reSize=make_thumb($newname,$newname,LWIDTH,LHEIGHT);
					$DelFilePath = "$siteRoot$imgFolder/temp-img/$p_img";
					if (file_exists($DelFilePath)) { unlink ($DelFilePath); }
				}
			}	
			//If no errors registred, print the success message and show the thumbnail image created
			//if(isset($_POST['Submit']) && !$errors) 
			//{
			if($p_img){
				// Insert data
				$edit = mysql_query ("UPDATE business_listing SET logo = '$p_img' WHERE business_id = '$business_id'");								
			}
		}
	}
	// If Photo Upload END
}
?>
<? 
/*
if($deletePhoto){
	// Delete dog logo
		$siteRoot = $DOCUMENT_ROOT;
		$imgFolder = "/dog-listing";
		
		if (file_exists("$siteRoot$imgFolder/images/$photo")) {
		if (!unlink("$siteRoot$imgFolder/images/$photo")) {
			$fileerror = "can't delete the file. <br><br>"; 
		 }if (!unlink("$siteRoot$imgFolder/images/thumb_$photo")) {
			$fileerror = "can't delete the file. <br><br>"; 
          }	
	     }// end if image
		$resultdelete = mysql_query ("UPDATE business_listing SET logo = '' WHERE event_id = '$event_id'");
		if (!$resultdelete){
			die(mysql_error());
			}
			
}// end isset array
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Business Logo: DogSpot.in</title>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<link href="/css/arrow.css" rel="stylesheet" type="text/css" /> <!--Css for arrow-->
<meta name="keywords" content="<? echo"$pageTitle";?>, dogs, dogs india, dogs world, puppies, dog DogSpot" />
<meta name="description" content="<? echo"$pageTitle";?>, dogs, dogs india, dogs world, puppies, dog | Spot for all your Dog's Needs DogSpot" />
<!--<link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />-->
<link type="text/css" rel="stylesheet" href="/new/css/main2.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/profile.css" media="all" />
<!--<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />-->
<link type="text/css" rel="stylesheet" href="/new/css/shop.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/business_listing.css" media="all" />
<link href="dropdown.vertical.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/cssun/popup.css" rel="stylesheet" type="text/css" />
<script src="/js/ajax.js" type="text/javascript"></script>


<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<link href="/css/arrow.css" rel="stylesheet" type="text/css" /> <!--Css for arrow-->
<link href="/css/global.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="JavaScript"><!--
function HideContent(d) {
document.getElementById(d).style.display = "none";
}
function ShowContent(d) {
document.getElementById(d).style.display = "block";
}
function ReverseDisplay(d) {
if(document.getElementById(d).style.display == "none") { document.getElementById(d).style.display = "block"; }
else { document.getElementById(d).style.display = "none"; }
}
//--></script>
<script language="javascript">
function confirmDelete(delUrl) {
  if (confirm("Are you sure you want to delete")) {
	document.location = delUrl;
  }
}
</script>
<?php $sitesection = "dog-business";
$business_name = stripslashes($business_name);
?>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div class="breadcrumb" style="margin-bottom:5px;;">
            	<div class="cont980">
                
                <table width="100%" border="0" cellpadding="00" cellspacing="0">
            <tr>
              <td width="32%"><div class="fl">
                        <p align="right"><a href="/">Home</a> &gt;<span class="sitenavi">Business Listings</span></p>
              </div></td>
              
                       <td width="5%"><div class="fr">
                        <p align="right"><script type="text/javascript" src="http://w.sharethis.com/widget/?tabs=web%2Cpost%2Cemail&amp;charset=utf-8&amp;style=default&amp;publisher=203f1564-d8e9-4d1a-a18a-e0a64020e484"></script></td></p>
              <td width="5%" align="right"><div id="rssFeed"><a href="/syndication/" rel="alternate" type="application/rss+xml">Subscribe Now <img src="http://www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt="" style="vertical-align:middle;border:0"/></a></div></td>
          
            </td>
            </tr>
          </table>   
          <div class="cb"></div>
  </div>
            
</div><!-- breadcrumb -->   
            
            
   <!-- brown strip -->
   	<div class="brownstrip">
    	<div class="cont980">
        	<div class="DivAddnew">
			
            	<h1>business listings <span></span> 
                 <a href="#"><img src="/new/pix/business_listings_btn.gif" alt="add your business" align="right" title="add your business"></a>       	  </div>
            <div class="cb"></div>
        </div>
    </div><!-- brown strip -->

<style type="text/css">
body,td,th {
	color: #00F;
}
.style1 {color: #0000FF}
.style2 {
	color: #FF0000;
	font-weight: bold;
}
.style3 {color: #000000}
.style4 {color: #999999}
</style>

<div class="cont980">
<div class="vs20"></div>
<br />
<h1>Dogs Listing : <? print breakLongWords($business_name, 30, " "); ?></h1><br />
<? if($status == "u"){ ?>
<p><?php require_once('update_link.php'); ?></p>
<? }else{ ?>
<ul id="arrowMove">
  <li class="arrowgreen"><a href="<? echo"/new/dog-listing/add-business.php?business_id=$business_id&status=$status&business_name=$business_name&businessURL=$businessURL";?>"><span>Require info</span></a></li>
  <li class="arrowgreen"><a href="<? echo"/new/dog-listing/category.php?business_id=$business_id&status=$status&business_name=$business_name&businessURL=$businessURL";?>"><span>Category</span></a></li>
  <li class="arrowgreen"><a href="<? echo"/new/dog-listing/about-ur-business.php?business_id=$business_id&status=$status&business_name=$business_name&businessURL=$businessURL";?>"><span>About your business</span></a></li>
  <li class="arroworrange"><span>Business logo</span></li>
  <li class="arowgrey"><span>Photos </span></li>
  <li class="arowgrey"><span>Videos</span></li>
  <li class="arowgrey"><span>Custom</span></li>
</ul>
<div id="clearall"></div>
<? } ?> <br />

<?
$selectArt = mysql_query ("SELECT logo FROM business_listing WHERE business_id = '$business_id'");
if (!$selectArt){die(mysql_error());}	

$rowArt = mysql_fetch_array($selectArt);
$logo = $rowArt["logo"];
if($logo){
?>
<img src="<? echo"/dog-listing/images/thumb_$logo";?>" class="highslide" />
<? }?>
<br /> <br />
<p><strong>Add your business Logo</strong></p>
<br />
<p>
  <input name="location" type="radio" id="radio" value="comp" checked="checked" onchange="ShowContent('divComp'); HideContent('divWeb');" />
Add a logo from your computer	<br />
<div id="divComp">
<label>
<input name="image" type="file" id="image" size="50" />
</label>
<input name="upimg" type="submit" class="yellowButton" id="upimg" value="Add Logo" />
<br />
Click &quot;Browse...&quot; to choose a file from your computer.</p>
</div><br />
<p>
  <input type="radio" name="location" id="radio2" value="site" onchange="ShowContent('divWeb'); HideContent('divComp');"/>
Add a logo from the web<br />
<div id="divWeb" style="display:none">
Please make sure you own the copyright of the image you are uploading.<br />
<input name="img_url" type="text" id="img_url" size="60" />
<input type="submit" name="upimgUrl" id="upimgUrl" value="Add Logo" class="yellowButton" />
<br />
Example: https://www.dogspot.in/images/logo-new.jpg<br />
</div>
<br />
  <div id="butStatus" align="right">
    <input name="business_name" type="hidden" id="business_name" value="<? echo"$business_name";?>" />
    <input name="businessURL" type="hidden" id="businessURL" value="<? echo"$businessURL";?>" />
	<input name="business_id" type="hidden" id="business_id" value="<? echo"$business_id";?>" />
    <input name="status" type="hidden" id="status" value="<? echo"$status";?>" />
    <a href="<? echo"/new/dog-listing/about-ur-business.php?business_id=$business_id&status=$status&business_name=$business_name&businessURL=$businessURL";?>" class="linkButtonBlue">« Back</a>
    <a href="<? echo"/new/dog-listing/photo.php?business_id=$business_id&status=$status&business_name=$business_name&businessURL=$businessURL";?>" class="linkButtonBlue">Next »</a>
    <a href="<? echo"$businessURL";?>" class="linkButtonBlue"> Finish </a>
  </div>
</form>
</div> 
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>   