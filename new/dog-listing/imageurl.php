<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?
if($upimgUrl){
// define the filename extensions you're allowing
define('ALLOWED_FILENAMES', 'jpg|jpeg|gif|png');
// define a directory the webserver can write to
define('IMAGE_DIR', 'dog-listing/images/');

// check against a regexp for an actual http url and for a valid filename, also extract that filename using a submatch (see PHP's regexp docs to understand this)
if(!preg_match('#^http://.*([^/]+\.('.ALLOWED_FILENAMES.'))$#', $_POST['img_url'], $m)) {
  die('Invalid url given');
}

// try getting the image
if(!$img = file_get_contents($_POST['img_url'])) {
  die('Getting that file failed');
}

// try writing the file with the original filename -- note that this will overwrite any existing filename in the same directory -- that's up to you to check for
if(!file_put_contents(IMAGE_DIR.'/'.$m[1], $img)) {
  die('Writing the file failed');
}
}
?>
<form id="form1" name="form1" method="post" action="">
<input type="radio" name="location" id="radio2" value="site" />
Add a photo from the web<br />
Please make sure you own the copyright of the image you are uploading.<br />
<input name="img_url" type="text" id="img_url" size="60" />
<input type="submit" name="upimgUrl" id="upimgUrl" value="Add Photo" />
<br />
Example: /images/logo-new.jpg

</form>
</body>
</html>
