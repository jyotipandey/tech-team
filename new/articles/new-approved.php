<?php
require_once($DOCUMENT_ROOT."/session-require.php"); 
require_once($DOCUMENT_ROOT."/arrays.php");
require_once($DOCUMENT_ROOT."/database.php");
require_once($DOCUMENT_ROOT."/functions.php");

$sitesection = "articles-new";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>Dog Blog : DogSpot: Spot for all your Dog's Needs</title>
<meta name="keywords" content="Dog Blog :DogSpot: Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog Blog :DogSpot: Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog" />
<!--<link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />-->
<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
<!--<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/business_listing.css" media="all" />-->


<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<?php
 $selectArt = mysql_query("SELECT * FROM articles WHERE art_name = '$art_name'");
  if(!$selectArt){	 die(sql_error());	}
  
   $rowArt = mysql_fetch_array($selectArt);
	$artcat_old = $rowArt["articlecat_id"];
	$article_id = $rowArt["article_id"];
	$art_subject = $rowArt["art_subject"];
	$artuser = $rowArt["userid"];
	
	$art_subject = stripslashes($art_subject);
	$art_subject = breakLongWords($art_subject, 30, " ");

if(!$selectArt || ($userid != $artuser)){	header ("Location: /");  }

?>
      
   	<!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="cont980">
                 <div class="fl">
                        <p align="left"><a href="/">Home</a> &gt; Approve Blog</p>
                     </div>
                  <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->
      <div class="vs15"></div>      
   <!-- main container -->
   		<div class="cont980">
   <p style="font-size:18px;" >The blog will be published after a review by a panel of experts at DogSpot. The following rules will be followed </p>

 <ul type="disc">
 <li>The blog should be well researched.</li><br />

 <li> The blog should be backed by data / information, surveys as the case may be.</li><br />

 <li>The blog should be of general public interest. Specific queries on any issues / subject would be diverted to the forum.</li><br />

 <li>The users need to dispel a sense of responsibility by not writing/publishing any information which is not in general public interest.</li><br />

 <li>The blog can represent personal views / opinion but should not be directed at any specific individual(s) representing any personal interest.</li><br />

 <li>All blogs will be moderated and no message that has inflammatory, abusive, derogatory language or any language deemed unfit for publication will displayed.
</li><br />

 <li>Though it will be endeavored that as many blogs as possible are displayed, however there will be time lag between the submission and publication of the blogs.
 </li><br />

 <li>The views represented in the blog are solely of the blogger, DogSpot does not either endorse or reject the views.</li><br />

  <li>DogSpot reserves the right to cancel, modify, edit any blog without taking any prior permission from the blogger.</li><br />

  <li>DogSpot will consider presenting the name of the blogger(s) in the up-coming Expert Zone.</li><br />

 </ul>

<br />
<br />
<br />
<p style="margin-left:8px;"><a href="/<? echo"$art_name"; ?>/"><b>View Blog</b></a></p>


   </div>
   <!-- main container -->
   
   <?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>  
