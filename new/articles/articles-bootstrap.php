<?php
if ($cat_nicename || $section[1] == 'page') {
    require_once('constants.php');
} else {
    require_once('../constants.php');
}
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/arrays.php');
include($DOCUMENT_ROOT . "/new/articles/catarray_wp.php");
header("Content-type: text/html; charset=iso-8859-1");
$sitesection = "articles1";


$mobile_browser = is_mobile2();
if ($mobile_browser > 0) {
    $request    = str_replace("/", "", $_SERVER['REQUEST_URI']);
    $rowPagesec = query_execute_row("SELECT id FROM mobile_section WHERE section_name='$request' AND status='active'");
    if ($rowPagesec['id']) {
        header('Location: https://m.dogspot.in/' . $request);
        exit();
    }
}
if ($section[2] == "category") {
    $cat_nicename = $section[2];
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: https://www.dogspot.in/dog-blog/$cat_nicename/");
}
if ($section[1] == "page") {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: https://www.dogspot.in/dog-blog/");
}
$trend_video           = query_execute_row("SELECT wpp.ID , wpp.post_title, wpp.post_name , wpp.post_content , wpm.meta_value FROM wp_posts as wpp , wp_postmeta as wpm WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wpp.post_type='post' AND wpm.post_id=wpp.ID AND wpm.meta_key like 'Trending Video' ORDER BY wpm.meta_id DESC LIMIT 1");
$trend_video_url       = $trend_video['meta_value'];
$trend_video_title     = $trend_video['post_title'];
$trend_video_name      = $trend_video['post_name'];
$sel_featured_art_main = query_execute_row("SELECT wpp.ID , wpp.post_title, wpp.post_name , wpp.post_content  FROM wp_posts as wpp , wp_postmeta as wpm WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wpp.post_type='post' AND wpm.post_id=wpp.ID AND wpm.meta_key like 'Featured Article' ORDER BY wpm.meta_id DESC LIMIT 1");

$main_fea_art_name = $sel_featured_art_main['post_title'];
$main_fea_art_nice = $sel_featured_art_main['post_name'];
$main_fea_art_id   = $sel_featured_art_main['ID'];
$main_fea_art_body = $sel_featured_art_main['post_content'];

$sel_featured_art = mysql_query("SELECT wpp.ID , wpp.post_title, wpp.post_name , wpp.post_content , wpp.views , wptr.term_taxonomy_id , wtt.term_id, wpp.post_status FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt , wp_postmeta as wpm WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpm.post_id=wpp.ID AND wpm.meta_key like 'Featured Article' ORDER BY wpm.meta_id DESC LIMIT 1,3");
if ($userid == "admin" && $sessionLevel == 1) {
    //echo "erwerw".$publish;
    if ($publish == "publish" && $article_id) {
        
        $updateStatus = mysql_query("UPDATE wp_posts SET post_status = 'publish' WHERE ID = '$article_id'");
        // Create recent Events File
        
        if (!$updateStatus) {
            die(mysql_error());
        }
        // Create recent Events File
        //   mysql_select_db("test");
        
        $selectfb = mysql_query("SELECT post_author, post_title, post_name FROM wp_posts WHERE ID = '$article_id' AND domain_id='1' AND post_type='post'");
        if (!$selectfb) {
            die(mysql_error());
        }
        $rowArtfb          = mysql_fetch_array($selectfb);
        $artuserfb_wp      = $rowArtfb["post_author"];
        $rowUser_id        = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$artuserfb_wp'");
        $artuserfb         = $rowUser_id['user_login'];
        $art_namefb        = $rowArtfb["post_name"];
        $art_subjectfb     = $rowArtfb["post_title"];
        $recent_dogs_blogs = file_get_contents(MYSITE_URL . "/backgroundjobs/create-home-recent-dog-blogs.php");
        // Publish of facebook
        $publink           = "https://www.dogspot.in/$art_namefb/";
        $pubtypmsg         = "Published an Article on DogSpot.in";
        sleep(10);
        checkfacebooklink($artuserfb, $publink, $pubtypmsg, "f");
        // Publish of facebook END
        
        //publish on twitter...
        $msg = "Published an Article on DogSpot.in: $art_subjectfb";
        publishontwitter($artuserfb, $msg, $publink, "t");
        //publish on twitter End...
        //header ("Location: /dog-blog/");
        //exit();
    }
    if ($unpublish == "unpublish" && $article_id) {
        $updateStatus = mysql_query("UPDATE wp_posts SET post_status = 'draft' WHERE ID = '$article_id'");
        if (!$updateStatus) {
            die(mysql_error());
        }
        //$recent_dogs_blogs=file_get_contents(MYSITE_URL."/backgroundjobs/create-home-recent-dog-blogs.php");
        header("Location: /dog-blog/");
        exit();
    }
}


if ($cat_nicename) {
    
    $sel_cat       = query_execute_row("SELECT * FROM wp_terms WHERE slug='$cat_nicename'");
    $cat_name      = $sel_cat['name'];
    $cat_nice      = $sel_cat['slug'];
    $articlecat_id = $sel_cat['term_id'];
    $pageT         = $sel_cat['name'];
    
    $name   = $cat_name;
    $pageT1 = $cat_nice;
    if ($pageT == '') {
        $pageT  = $name;
        $pageT1 = $name;
    }
    $pageTitle = "$cat_name, Dog Blog, Dog Article, Dogs India DogSpot.in";
    $pageh1    = "$cat_name - Dog Blog & Articles";
    $pageKey   = "$cat_name, Dog Blog, & Articles, Dogs India";
    $pageDesc  = "$cat_name Dog Blog, & Articles from Dogs Experts Dogs India DogSpot.in";
    if (trim($articlecat_id) == '') {
        header("HTTP/1.0 404 Not Found");
        require_once($DOCUMENT_ROOT . '/404.php');
        die(mysql_error());
        exit();
    }
} elseif ($artuser) {
    //mysql_select_db("dogspot_dogspot");
    $pageT     = dispUname($artuser);
    $pageTitle = "$pageT Dog Blog, Dog Article, Dogs India DogSpot.in";
    $pageh1    = "$pageT - Dog Blog & Articles";
    $pageKey   = "$pageT, Dog Blog, & Articles, Dogs India";
    $pageDesc  = "$pageT Dog Blog, & Articles from Dogs Experts Dogs India DogSpot.in";
    if (trim($pageT) == '') {
        header("HTTP/1.0 404 Not Found");
        require_once($DOCUMENT_ROOT . '/404.php');
        die(mysql_error());
        exit();
    }
} else {
    $pageTitle = "Dog Blog Category, Dog Article, Dogs India DogSpot.in";
    $pageh1    = "Dog Blog & Articles";
    $pageKey   = "Dog Blog Category, & Articles, Dogs India";
    $pageDesc  = "Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in";
    
}

if ($del) {
    $resultPostId  = mysql_query("SELECT post_author FROM wp_posts WHERE ID = '$article_id'");
    $rowPostid     = mysql_fetch_array($resultPostId);
    $postUserid_wp = $rowPostid["post_author"];
    $rowUser_id    = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$postUserid_wp'");
    $postUserid    = $rowUser_id['user_login'];
    if ($userid != 'Guest' && ($userid == $postUserid || $sessionLevel == 1)) {
    } else {
        header("Location: /");
        exit();
    }
    // del
    
    $selectArt = mysql_query("SELECT post_author FROM wp_posts WHERE ID = '$article_id'");
    if (!$selectArt) {
        die(sql_error());
    }
    $rowArt      = mysql_fetch_array($selectArt);
    $downUser_wp = $rowArt["post_author"];
    $downUser_id = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$artuserfb_wp'");
    $downUser    = $downUser_id['user_login'];
    
    //$deleteComnt  = mysql_query("UPDATE comments SET publish_status='markdel' WHERE article_id ='$article_id'");
    $resultdelete = mysql_query("UPDATE wp_posts SET post_status='markdel' WHERE ID ='$article_id'");
    
    delete_friends_update($article_id, 'dog-blog');
    
    /*// Update blog Count on users Table
    $recent_dogs_blogs=file_get_contents(MYSITE_URL."/backgroundjobs/create-home-recent-dog-blogs.php");
    $recent_dogs_comments=file_get_contents(MYSITE_URL."/backgroundjobs/ create-home-recent-comments.php");    
    // Create recent Events File*/
    
    downUserNum("users", "num_blog", $downUser);
    //echo"users,num_blog,$downUser";
    // Update blog Count on users Table END 
    if (!$resultdelete) {
        die(mysql_error());
    } else {
        header("Location: $reDirURL");
    }
}

// List of Posts
if ($userid == "admin" && $sessionLevel == 1) {
    $sqlall      = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.ID DESC";
    $sql         = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.ID DESC LIMIT 0,19";
    $sqlallcount = "SELECT count(*) as countrecord FROM wp_posts WHERE post_status = 'publish' AND domain_id='1' ORDER BY ID DESC";
} else {
    //    $sqlall      = "SELECT * FROM wp_posts WHERE post_status = 'publish' AND articlecat_id !='37' AND domain_id='1' ORDER BY ID DESC";
    //    $sql         = "SELECT * FROM wp_posts WHERE post_status = 'publish' AND articlecat_id !='37'  AND domain_id='1'  ORDER BY ID DESC LIMIT 0,19"; 
    // $sqlallcount = "SELECT count(*) as countrecord FROM wp_posts WHERE post_status = 'publish' AND articlecat_id !='37'  AND domain_id='1'  ORDER BY ID DESC";
    $sqlall = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC ";
    
    $sql = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,19";
    
    $sqlallcount = "SELECT count(*) as countrecord FROM wp_posts WHERE post_status = 'publish' AND domain_id='1' ORDER BY post_date_gmt DESC";
    
    
}

if ($cat_nicename) {
    $pageT1        = $cat_nicename;
    $sel_cat       = query_execute_row("SELECT * FROM wp_terms WHERE slug='$cat_nicename'");
    $cat_name      = $sel_cat['name'];
    $cat_nice      = $sel_cat['slug'];
    $articlecat_id = $sel_cat['term_id'];
    if ($userid == "admin" && $sessionLevel == 1) { //only for admin
        $sqlall = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wptr.term_taxonomy_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' ORDER BY wpp.post_date_gmt DESC";
        
        $sql         = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wttWHERE wptr.term_taxonomy_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,19";
        // echo"1";
        $sqlallcount = "SELECT count(wpp.ID) as countrecord FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wptr.term_taxonomy_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' ORDER BY wpp.post_date_gmt DESC";
    } else { //for all users
        $sqlall = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wtt.term_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wpp.post_status = 'publish'  AND wpp.domain_id='1' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id  GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC";
        
        $sql         = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wtt.term_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.post_status = 'publish' AND wpp.post_type='post' AND wpp.domain_id='1' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id  GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,19";
        //echo"2";
        $sqlallcount = "SELECT count(wpp.ID) as countrecord FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wtt.term_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wpp.domain_id='1' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' ORDER BY wpp.post_date_gmt DESC";
    }
    
}



if ($filter) {
    if ($filter == 'justin') {
        $pageT  = 'justin';
        $pageT1 = 'justin';
        
        $pageh1    = "Just in - Dog Blog & Articles";
        $pageKey   = "$pageT, Dog Blog, & Articles, Dogs India";
        $pageTitle = "$pageT, Dog Blog, Dog Article, Dogs India DogSpot.in";
        
        $sql = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,19";
        
        $sqlall = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC ";
        
        $sqlallcount = "SELECT count(*) as countrecord FROM wp_posts WHERE post_status = 'publish'  AND domain_id='1' AND post_type='post'  ORDER BY ID DESC";
    } else if ($filter == 'popular') {
        $pageT  = 'popular';
        $pageT1 = 'popular';
        
        $pageh1    = "Most Popular - Dog Blog & Articles";
        $pageKey   = "$pageT, Dog Blog, & Articles, Dogs India";
        $pageTitle = "$pageT, Dog Blog, Dog Article, Dogs India DogSpot.in";
        $sql       = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.views DESC LIMIT 0,19 ";
        
        $sqlall = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.views DESC";
        
        $sqlallcount = "SELECT count(*) as countrecord FROM wp_posts WHERE post_status = 'publish'  AND domain_id='1' AND post_type='post'  ORDER BY ID DESC";
    } else {
        header("HTTP/1.0 404 Not Found");
        require_once($DOCUMENT_ROOT . '/404.php');
        exit();
    }
}

if ($artuser) { //For Artical user only
    //$pageT=$artuser;
    if ($artuser == "Guest") {
        $refUrl = $_SERVER['REQUEST_URI'];
        header("Location: /reg-log/?refUrl=/dog-blog/?artuser");
    }
    //mysql_select_db("test");
    $rowUser_id = query_execute_row("SELECT ID FROM wp_users WHERE user_login ='$artuser'");
    $artuser_id = $rowUser_id['ID'];
    $sqlall     = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_author = '$artuser_id' AND wpp.post_status = 'publish'  AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC";
    
    $sql = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_author = '$artuser_id' AND wptr.object_id=wpp.ID AND wpp.post_status = 'publish' AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC ";
    
    $sqlallcount = "SELECT count(*) as countrecord FROM wp_posts WHERE post_author = '$artuser_id' AND post_status = 'publish'  AND domain_id='1' AND post_type='post' ORDER BY ID DESC";
}
$selectArt      = mysql_query("$sql");
$selectArtcount = query_execute_row("$sqlallcount");
if (!$selectArt) {
    die(mysql_error());
}
$totrecord = mysql_num_rows($selectArt);

$sel_drop_categories = mysql_query("SELECT DISTINCT(wptr.term_taxonomy_id) , wtt.term_id , wt.name , wt.slug FROM wp_term_relationships as wptr , wp_term_taxonomy as wtt , wp_terms as wt WHERE wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wt.term_id=wptr.term_taxonomy_id");
if ($section[1] == '') {
    $pageT1 = "justin";
}
if ($artuser) {
    $pageTitle = $pageTitle . ' | ' . $artuser;
    $pageKey   = $pageKey . ' | ' . $artuser;
    $pageDesc  = $pageDesc . ' | ' . $artuser;
}
$title   = $pageTitle;
$keyword = str_replace('"', '', $pageKey);
$desc    = str_replace('"', '', $pageDesc);
if ($section[1] != '' && $section[2] == '') {
    $alternate = "https://m.dogspot.in/dog-blog/$section[1]/";
    $canonical = "https://www.dogspot.in/dog-blog/$section[1]/";
} elseif ($section[2] != '') {
    $alternate = "https://m.dogspot.in/dog-blog/$section[1]/$section[2]/";
    $canonical = "https://www.dogspot.in/dog-blog/$section[1]/$section[2]/";
} else {
    $alternate = "https://m.dogspot.in/dog-blog/";
    $canonical = "https://www.dogspot.in/dog-blog/";
}
$og_url    = $canonical;
$imgURLAbs = "https://www.dogspot.in/new/pix/fb-dogspot-logo.jpg";
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
?> 
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/blog.css?v=29" /> 
<section id="title"> 
  <div class="container"> 
    <div class="title-container"> 
      <h1><span class="primary"> 
        <?= $pageh1 ?> 
       </span></h1> 
    </div> 
  </div> 
</section> 
<div class="breadcrumbs"> 
    <div class="container"> 
      <div class="row" itemscope itemtype="http://schema.org/Breadcrumb"> 
        <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
         <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> 
      <a href="/" itemprop="item"><span itemprop="name">Home</span></a> 
       <meta itemprop="position" content="1" /> </span> 
     <span> / </span> 
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">  
     <span itemprop="item"> 
     <span itemprop="name" class="active-bread">Dog Blog</span></span> 
      <meta itemprop="position" content="2" /> </span> 
        </div> 
      </div> 
    </div> 
  </div> 
<section id="content"> 
  <div class="container"> 
    <div class="row"> 
      <div id="content_div" class="col-md-8">  
        <!-- add banner--> 
        <section class="widget">  
          <!-- Widget Content --> 
          <div class="frame thick"> <a href="#" target="_blank"> <img src="http://placehold.it/728x90/f0f0f9" alt="ads"> </a> </div> 
        </section> 
        <?php

$iid = 0;

while ($rowArt = mysql_fetch_array($selectArt)) {
     $iid           = $iid + 1;
    $articlecat_id = $rowArt["term_id"];
    $article_views = $rowArt["views"];
    $sel_cat       = query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
    $cat_name      = $sel_cat['name'];
    $cat_nice      = $sel_cat['slug'];
    $article_id    = $rowArt["ID"];
    $post_title    = $rowArt["post_title"];
    $post_content  = $rowArt["post_content"];
    $post_date     = $rowArt["post_date"];
    $artuser_wp    = $rowArt["post_author"];
    $post_statuss  = $rowArt["post_status"];
    $sel_cmnts     = mysql_query("SELECT * FROM wp_comments WHERE comment_post_ID='$article_id'");
    $tot_cmnts     = mysql_num_rows($sel_cmnts);
    $rowUser_id    = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$artuser_wp'");
    $artuser       = $rowUser_id['user_login'];
    $post_name     = $rowArt["post_name"];
    $imgURL        = get_first_image($post_content, $DOCUMENT_ROOT);
    $imgURLAbs     = make_absolute($imgURL, 'https://www.dogspot.in');
    $post_title    = stripslashes($post_title);
    $post_title    = breakLongWords($post_title, 20, " ");
    
    // Get Post Teaser
    $post_content = stripslashes($post_content);
    $post_content = strip_tags($post_content);
    $post_content = trim($post_content);
    $post_content = substr($post_content, 0, 140);
    
    $post_content = stripslashes($post_content);
    $post_content = breakLongWords($post_content, 30, " ");
    $check_img    = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id' AND post_type='attachment' AND post_mime_type like '%image%' ORDER BY ID DESC");
    //mysql_select_db("dogspot_dogspot");
    $rowUser1     = query_execute_row("SELECT f_name, image FROM users WHERE userid='$artuser'");
    if (strpos($imgURLAbs, 'wordpress') !== false) {
        $exp        = explode("https://www.dogspot.in", $imgURLAbs);
        $src        = $exp[1];
        $pos        = strrpos($imgURLAbs, '/');
        $image_name = substr($imgURLAbs, $pos + 1);
        $imageURL   = '/imgthumb/190x127-' . $image_name;
    } else if ($check_img['guid'] != '') {
        $imgURLAbs  = $check_img['guid'];
        $exp        = explode("https://www.dogspot.in", $imgURLAbs);
        $src        = $DOCUMENT_ROOT . $exp[1];
        $pos        = strrpos($imgURLAbs, '/');
        $image_name = substr($imgURLAbs, $pos + 1);
        $imageURL   = '/imgthumb/190x127-' . $image_name;
    } else if ($imgURLAbs != 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
        $URL        = $imgURLAbs;
        $image_name = (stristr($URL, '?', true)) ? stristr($URL, '?', true) : $URL;
        $pos        = strrpos($image_name, '/');
        $image_name = substr($image_name, $pos + 1);
        $extension  = stristr($image_name, '.');
        if ($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg') {
            $image_name;
        }
        $src      = $DOCUMENT_ROOT . '/userfiles/images/' . $image_name;
        //$src = $imgURL;
        $imageURL = '/imgthumb/190x127-' . $image_name;
    } else if ($rowUser1["image"]) {
        $src      = $DOCUMENT_ROOT . '/profile/images/' . $rowUser1["image"];
        $imageURL = '/imgthumb/190x127-' . $rowUser1["image"];
    } else {
        $src      = $DOCUMENT_ROOT . '/images/noimg.gif';
        $imageURL = '/imgthumb/190x127-noimg.gif';
    }
    $imagestr = explode($DOCUMENT_ROOT, $src);
    $imglink  = 'https://ik.imagekit.io/2345/tr:h-250,w-250,c-at_max/' . $imagestr[0];
    
?> 
       <article class="article-medium art_cont_div" id="<?= $iid ?>" itemscope itemtype="http://schema.org/BlogPosting"> 
          <div class="row">  
            <!--Image--> 
            <div class="col-sm-5"> 
              <div class="frame">  
                <figure class="image-holder">  
                <?php // function for image with no link for guest......... 
    if (file_exists($dest)) {
    } else {
        $imageURL = "https://ik.imagekit.io/2345/imgthumb/190x127-noimg.gif";
    }
    if ($userid != 'Guest') {
        if ($article_id == '874') {
            $imageURL = "https://ik.imagekit.io/2345/imgthumb/190x127-874.jpg";
        }
?>  
    <a href="<?php
        echo "/$post_name/";
?>"><img itemprop="image"  src="<?= $imglink ?>" alt="<?= $post_title ?>"  title="<?= $post_title ?>"></a> 
    <?php
    } else {
        if ($article_id == '874') {
            $imageURL = "/imgthumb/190x127-874.jpg";
        }
?> 
   <a href="<?php
        echo "/$post_name/";
?>">><img itemprop="image" src="<?= $imageURL ?>" alt="<?= $post_title ?>" title="<?= $post_title ?>"></a> 
    <?php
    }
?>   
                </figure> 
                </div> 
            </div> 
            <div class="col-sm-7"> 
              <h4 itemprop="headline"><a href="/<?= $post_name ?>/" title="<?= $post_title ?>"> 
                <?= $post_title ?> 
               </a></h4> 
              <p class="post-meta">  
              <i class="fa fa-clock-o"></i> <span itemprop="datePublished"> 
              <?php
    print(showdate($post_date, "d M Y"));
?></span> &nbsp; 
               <span><i class="fa fa-user"></i>   <?php
    guestredirect($userid, dispUname($artuser), $artuser);
?> 
</span> 
&nbsp; <span> <i class="fa fa-folder"></i> <a href="/dog-blog/<?= $cat_nice ?>/"> <?= $cat_name ?></a></span> 
       
          </p> 
              <p itemprop="articleBody"><?= $post_content ?>...</p> 
               <div class="read-more"> 
              <a href="/<?= $post_name ?>/"><button class="btn btn-primary btn-sm">Read More</button> </a> 
                                                                </div> 
               
            </div> 
          </div> 
        </article> 
        <?php
}
?> 
          
       
        <!-- add banner end-->  
      </div> 
      <aside class="col-md-4">  
      <div class="row"> 
      <section class="col-sm-6 col-md-12 widget"> 
      <div class="write-guest-post text-center"> 
  <a href="https://www.dogspot.in/wordpress/wp-login.php?redirect_to=https://www.dogspot.in/wordpress/wp-admin/post-new.php" target="_blank">   Write a Story</a></div> 
      </section> 
       
      <section class="col-sm-6 col-md-12 widget"> 
       <header class="clearfix"><h4>Categories</h4></header> 
        <?php
$wag_new_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='wag-news' AND post_status='publish'");

$wellness_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='wellness' AND post_status='publish'");

$wag_brag_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='wag-brag' AND post_status='publish'");

$travel_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='travel-with-your-dog' AND post_status='publish'");

$wag_wiki_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='wag-wiki' AND post_status='publish'");

$training_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='training' AND post_status='publish'");

$grooming_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='grooming' AND post_status='publish'");

$adoption_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='adoption' AND post_status='publish'");

$story_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='story-from-pet-lover' AND post_status='publish'");

$events_count = query_execute_row("SELECT COUNT(*) as total FROM wp_terms AS a, wp_term_taxonomy AS b, wp_term_relationships AS c, wp_posts AS d WHERE a.term_id=b.term_id AND b.term_taxonomy_id=c.term_taxonomy_id AND c.object_id=d.ID AND b.taxonomy='category' AND slug='events' AND post_status='publish'");

?>   
                 <ul class="wiget_cat_ul"> 
                <li><span><?= $wag_new_count['total']; ?></span><a href="/dog-blog/wag-news/">Wag News </a></li> 
                <li><span><?= $wellness_count['total']; ?></span><a href="/dog-blog/wellness/">Wellness</a></li> 
                <li><span><?= $wag_brag_count['total']; ?></span><a href="/dog-blog/wag-brag/">Wag Brag</a></li> 
                   <li><span><?= $travel_count['total']; ?></span><a href="/dog-blog/travel-with-your-dog/">Travel with your dog</a></li> 
                <li><span><?= $wag_wiki_count['total']; ?></span><a href="/dog-blog/wag-wiki/">Wag wiki</a></li> 
            </ul> 
             
            <ul class="wiget_cat_ul" style="margin-left:15px"> 
                <li><span><?= $training_count['total']; ?></span><a href="/dog-blog/training/">Training</a></li> 
                <li><span><?= $grooming_count['total']; ?></span><a href="/dog-blog/grooming/">Grooming</a></li> 
                <li><span><?= $adoption_count['total']; ?></span><a href="/dog-blog/adoption/">Adoption</a></li> 
                <li><span><?= $story_count['total']; ?></span><a href="/dog-blog/story-from-pet-lover/">Story from a pet lover</a></li> 
                <li><span><?= $events_count['total']; ?></span><a href="/dog-blog/events/">Events</a></li> 
              </ul>                                               <!-- Widget content --> 
                 
             
                                                        </section> 
       
      <?php
/*?><?

include($DOCUMENT_ROOT . '/product_banner.php');
?><?php */
?> 
 <?php
?> 
    
    <section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;"> 
                                                                <!-- Widget Header --> 
                                                                <header class="clearfix"> 
                                                                        <h4>Sponsored: In the Stores</h4> 
                                                                                                        </header> 
<?php
$getItems = query_execute("SELECT si.name,si.nice_name,si.item_id,sia.price,sia.mrp_price,si.item_parent_id FROM shop_items as si,shop_item_affiliate as sia WHERE sia.item_id=si.item_id AND showbanner='1' ");
while ($fetchaffiliate = mysql_fetch_array($getItems)) {
    $name           = $fetchaffiliate['name'];
    $item_id        = $fetchaffiliate['item_id'];
    $item_parent_id = $fetchaffiliate['item_parent_id'];
    $nice_name      = $fetchaffiliate['nice_name'];
    $price          = $fetchaffiliate['price'];
    $mrp_price      = $fetchaffiliate['mrp_price'];
    $rowdatM        = query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
    if (!$rowdatM['media_file']) {
        $rowdatM = query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
    }
    
    $imglink = 'https://ik.imagekit.io/2345/tr:h-100,w-100,c-at_max/shop/item-images/orignal/' . $rowdatM['media_file'];
    
    
?> 
                       <div class="gurgaon_offers">  
             
                <div class="gurgaon_offersr"> <a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><img src="<?= $imglink ?>" width="100" height="93"></a> </div> 
                <div class="gurgaon_offersl"> 
                    <div class="gurgaon_offers_pn"><a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><?= $name ?></a> </div> 
                                        <div class="gurgaon_offers_pr"> 
                                        <?php
    if ($mrp_price > $price) {
?> 
                                       <span>Rs. <del><?= $mrp_price ?></del>  
                                        </span> &nbsp; &nbsp; <?php
    }
?> 
                                       <span class="price-color"> 
                                        Rs. <?= $price ?></span> 
                                       <div class="aff-m-logo"> <a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><img src="/new/articles/img/amazon.jpg" /></a> 
                                       </div> 
                                        </div> 
                                    </div> 
               
        </div> 
     <?php
}
?> 
        </section> 
       <section class="col-sm-6 col-md-12 widget feedburner"> 
        <header class="clearfix"><h4>Popular Post</h4></header> 
         <?php
$most_popular = mysql_query("SELECT ID, post_title, post_name, post_content, first_image_name FROM wp_posts WHERE post_status = 'publish' AND domain_id='1' AND post_type='post' AND post_name!='$cat_nice_name' ORDER BY views DESC LIMIT 5");
while ($popularRow = mysql_fetch_array($most_popular)) {
    $first_img_name   = $popularRow["first_image_name"];
    $post_title_pop   = $popularRow["post_title"];
    $post_name_pop    = $popularRow["post_name"];
    $post_content_pop = $popularRow["post_content"];
    $article_id_pop   = $popularRow["ID"];
    if ($first_img_name == '') {
        $imgURL    = get_first_image($post_content_pop, $DOCUMENT_ROOT);
        $imgURLAbs = make_absolute($imgURL, 'https://www.dogspot.in');
        
        // Insert in to database
        $GetFirst = explode("https://www.dogspot.in/wordpress/wp-content/uploads/", $imgURLAbs);
        if ($GetFirst[1] == "") {
            $updateIMG1 = "UPDATE wp_posts SET first_image_name='$imgURL' WHERE ID='$article_id_pop'";
            mysql_query($updateIMG1);
            $first_img_name = $imgURLAbs;
        } else {
            $updateIMG = "UPDATE wp_posts SET first_image_name='$GetFirst[1]' WHERE ID='$article_id_pop'";
            mysql_query($updateIMG);
            $first_img_name = "https://www.dogspot.in/wordpress/wp-content/uploads/" . $GetFirst[1];
        }
    } else {
        $first_img_name = "https://www.dogspot.in/wordpress/wp-content/uploads/" . $first_img_name;
    }
    if (strpos($first_img_name, 'userfiles') !== false) {
        $GetFirst1      = explode("https://www.dogspot.in/wordpress/wp-content/uploads/", $first_img_name);
        $first_img_name = "https://www.dogspot.in/" . $GetFirst1[1];
    }
    $first_img = str_replace('https://www.dogspot.in/', 'https://ik.imagekit.io/2345/', $first_img_name);
?> 
                                                                
            <div class="motst_read_article"> 
                <div><a href="https://www.dogspot.in/<?= $post_name_pop; ?>/"><img src="<?= $first_img; ?>" width="" height="" alt="" title="" ></a></div> 
                <div class="motst_read_article_title"><a href="https://www.dogspot.in/<?= $post_name_pop; ?>/"><?= $post_title_pop; ?></a> 
            </div> 
      </div> 
<?php
}
?> 
                                                              
                                                                 
                                                        </section> 
     
    <section class="col-sm-6 col-md-12 widget"> 
    <div class="ad-banner"> 
      <?php
require_once($DOCUMENT_ROOT . '/banner1.php');
addbanner300('250', '300', '1');
?> 
   </div> 
    </section> 
     
     <section class="col-sm-6 col-md-12 widget"> 
    <header class="clearfix"><h4>Latest post</h4></header> 

<?php
$check_latest = query_execute("SELECT wpp.ID , wpp.post_title, wpp.post_name , wpp.post_content,wpp.post_date, wtt.term_id,wpp.first_image_name FROM wp_posts as wpp, wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' ORDER BY ID DESC LIMIT 6");
?> 
 <div class="latest-post"> 
<?php
while ($check_latest1 = mysql_fetch_array($check_latest)) {
    $article_id       = $check_latest1["ID"];
    $post_title       = $check_latest1["post_title"];
    $post_date        = $check_latest1["post_date"];
    $articlecat_id    = $check_latest1["term_id"];
    $sel_cat          = query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
    $cat_name         = $sel_cat['name'];
    $cat_nice         = $sel_cat['slug'];
    $post_content     = $check_latest1["post_content"];
    $post_name        = $check_latest1["post_name"];
    $first_image_name = $check_latest1["first_image_name"];
    if ($first_image_name == '') {
        $imgURL    = get_first_image($post_content, $DOCUMENT_ROOT);
        $imgURLAbs = make_absolute($imgURL, 'https://www.dogspot.in');
        $GetFirst  = explode("https://www.dogspot.in/wordpress/wp-content/uploads/", $imgURLAbs);
        $updateArt = mysql_query("UPDATE wp_posts SET first_image_name='" . $GetFirst[1] . "' WHERE ID='$article_id'");
    } else {
        $imgURLAbs = "https://www.dogspot.in/wordpress/wp-content/uploads/" . $first_image_name;
    }
    $post_title = stripslashes($post_title);
    $post_title = breakLongWords($post_title, 20, " ");
    
    $check_img = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id' AND post_type='attachment' AND post_mime_type like '%image%'");
    if ($check_img['guid'] != '') {
        $imgURLAbs  = $check_img['guid'];
        $exp        = explode("https://www.dogspot.in", $imgURLAbs);
        $src        = $DOCUMENT_ROOT . $exp[1];
        $pos        = strrpos($imgURLAbs, '/');
        $image_name = substr($imgURLAbs, $pos + 1);
        $imageURL   = '/imgthumb/90x73-' . $image_name;
    } else if (strpos($imgURLAbs, 'wordpress') !== false) {
        $exp        = explode("https://www.dogspot.in", $imgURLAbs);
        $src        = $DOCUMENT_ROOT . $exp[1];
        $pos        = strrpos($imgURLAbs, '/');
        $image_name = substr($imgURLAbs, $pos + 1);
        $imageURL   = '/imgthumb/90x73-' . $image_name;
    }
    
    else if ($imgURLAbs != 'https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
        $URL        = $imgURLAbs;
        $image_name = (stristr($URL, '?', true)) ? stristr($URL, '?', true) : $URL;
        $pos        = strrpos($image_name, '/');
        $image_name = substr($image_name, $pos + 1);
        $extension  = stristr($image_name, '.');
        if ($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg') {
            $image_name;
        }
        $src      = $DOCUMENT_ROOT . '/userfiles/images/' . $image_name;
        //$src = $imgURLAbs;
        $imageURL = '/imgthumb/90x73-' . $image_name;
    } else if ($rowUser1["image"]) {
        $src      = $DOCUMENT_ROOT . '/profile/images/' . $rowUser1["image"];
        $imageURL = '/imgthumb/90x73-' . $rowUser1["image"];
    } else {
        $src      = $DOCUMENT_ROOT . '/images/noimg.gif';
        $imageURL = '/imgthumb/90x73-noimg.gif';
    }
    $imagestr = explode($DOCUMENT_ROOT, $src);
    $imglink  = 'https://ik.imagekit.io/2345/tr:h-73,w-90,c-at_max/' . $imagestr[1];
?>       
      <div class="article-tiny"> 
      <a href="/<?= $post_name ?>/" class="image"> 
       <figure class="image-holder"> 
         <img src="<?= $imglink ?>" width="<?= $image_width ?>" height="<?= $image_height ?>" alt="<?= $post_title ?>" title="<?= $post_title ?>"> 
       </figure> 
       </a> 
        <h5><a href="/<?= $post_name; ?>/" title="<?= $post_title ?>"><?= $post_title; ?></a></h5> 
         <p class="post-meta">  
              <i class="fa fa-clock-o"></i> <span itemprop="datePublished"> 
              <?php
    print(showdate($post_date, "d M Y"));
?></span> &nbsp; 
               
&nbsp; <span> <i class="fa fa-folder"></i> <a href="/dog-blog/<?= $cat_nice ?>/"> <?= $cat_name ?></a></span> 
       
          </p> 
                                                                                </div> 

                                                                     <?php
}
?>   
                                                                </div> 
                  <div id="txt1" style="display:none"><?= $pageT1 ?></div> 
                  <div id="txt2" style='display:none'><?= $selectArtcount['countrecord']; ?></div> 
                                                        </section> 

                                                         
     
      </div> 
      </aside> 
    </div> 
  </div> 
</section> 
<?php
if ($section[1] != 'author') {
?> 
<script type="text/javascript"> 
var cat_nice=''; 
 $(document).ready(function() {  
     var cat_nice=document.getElementById('txt1').innerHTML; 
    var c=0; 

     countr=document.getElementById('txt2').innerHTML; 
    $(window).data('ajaxready', true).scroll(function(e) { 
    if ($(window).data('ajaxready') == false) return; 

       if(($(document).height() - $(window).height()) - $(window).scrollTop() < 1000) {     
    $('#loadMoreComments').show(); 
    $(window).data('ajaxready', false); 
    if(c!=3 && countr!=0){ 
    $.ajax({ 
    dataType : "html" , 
    contentType : "application/x-www-form-urlencoded" , 
    url: "/new/articles/loadmore_wp_new_ui_bootstrap.php?lastComment="+ $(".art_cont_div:last").attr('id') , 
    data: {cat_nicename:cat_nice} , 
    success: function(html) { 
    if(html){ 
        $("#content_div").append(html);$('#loadMoreComments').hide();c=c+1; 
     
    }else { 
        $('#loadMoreComments').html(); 
    } 
    $(window).data('ajaxready', true); 
     
    } 
    }); // ajex close 
    }else{ 
        e12=  $(".art_cont_div:last").attr('id'); 
        $('#cval').val(e12); 
        if(e12!=countr){ 
        $("#content_div").append("<div id='divw"+e12+"' class='divw"+e12+ " load-more'  onclick=get("+e12+")>Load More</div>"); 
        c=0; 
    }$('#loadMoreComments').hide(); 
    } // c condition close 
    } 
    }); 
     
    }); 
</script> 
<?php
}
?> 
<script> 
function get(rt){ 
    $('#divw'+rt).hide(); 
    var cat_nice=document.getElementById('txt1').innerHTML; 
    countr=document.getElementById('txt2').innerHTML; 
     
    c=0; 
     
    if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {     
    $('#loadMoreComments').show(); 
    $(window).data('ajaxready', false); 
     
    if(c!=3){ 
     
    $.ajax({ 
    cache: false, 
    dataType : "html" , 
    contentType : "application/x-www-form-urlencoded" , 
    url: "/new/articles/loadmore_wp_new_ui_bootstrap.php?lastComment="+ $(".art_cont_div:last").attr('id') , 
    data: {cat_nicename:cat_nice} , 
    success: function(html) { 
    if(html){         
    $("#content_div").append(html); 
    $('#loadMoreComments').hide(); 
    c=c+1;  
    }else { 
    $('#loadMoreComments').html(); 
    } 
    $(window).data('ajaxready', true); 
     
    } 
    }); // ajax close 
       } 
    else{ 
        e12=  $(".art_cont_div:last").attr('id'); 
        if(e12!=countr){ 
        $("#content_div").append("<div id='divw"+e12+"' class='divw"+e12+ " load-more' style='cursor:pointer' onclick=get("+e12+")>load more..</div>"); 
        c=0;} 
        $('#loadMoreComments').hide(); 
     
    } // c condition close--> 
    } 
     
    } 

</script> 
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');