<?php 
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
require_once('constants.php');
require_once(SITEMAIN_URL.'/session.php');
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
?><!doctype html>
<html>
<head>
<title><?php echo "Purina Sample | Dogspot.in"; ?></title>
<meta name="keywords" content="Purina, Free Sample" />
<meta name="description" content="Get Free Purina Sample in Dogspot.in" />
<link href="https://www.dogspot.in/<?=$section[0]?>/" rel="canonical">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php require_once('common/script.php'); ?>
<?php require_once('common/header.php'); ?>
		
	  <style>
	  body{ padding:0px; margin:0px;}
	  .free_sample_header,.free_samle_contnet_sec,.free_samle_login_sec { 
	  width:100%;
	  float:left;
	  position:relative;
	      font-family: lato,sans-serif;
	  
	  }
	  
	  .free_sample_header{
	 background: #222222;
    display: block;
    margin: 0 auto;
	text-align:center;
	padding:70px 0px;
	  }
	   
	 .free_sample_header h1 {
        font-size: 30px;
    font-weight: normal;
    padding-top: 15px;
    text-transform: uppercase;
    color: #f2b811;
	}
	.free_sample_header h1 span{color: #ed1c25;}
		.free_samle_contnet_sec{ padding:50px 0px; background:#f3f3f3;s }
	.free_samle_contnet_sec h2{     text-align: center;
    font-size: 28px;
    color: #ed1c25;
	    font-weight: normal;
    text-transform: uppercase;
}

.steps_flex{display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-wrap: wrap;
    -moz-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    -o-flex-wrap: wrap;
    flex-wrap: wrap;}
	.steps_flex{ margin:auto; max-width:972px;}
	.flex_blocks {
    position: relative;
    overflow: hidden;
    width: calc(99.9% / 2);
	text-align:center;
}
.flex_blocks figure{ display:block; margin:0px; padding:20px 0px 0px; font-size:16px}
.flex_blocks figure img{background: #000;
    padding: 20px;
    border-radius: 12px;
    max-width: 100px;
    height: auto;
    margin-bottom: 10px;
    border: 2px solid #f08b21;}
	.free_samle_login_sec{ padding:50px 0px; margin:auto; text-align:center; font-size:18px; }
	
	.free_samle_btn{}
	.free_samle_login_sec a{ text-decoration: none;
    color: #fff;
    display: block;
    background: #ed1c25;
    margin: auto;
    max-width: 296px;
    font-size: 18px;
    line-height: 45px;
    height: 45px;
    border-radius: 10px;
    text-transform: uppercase}
	  </style>
</head>
<body>
	
<section class="free_sample_header">

<div class="free_sample_header_logo">
<img src="/images/form-super_coto-log.png" width="" height="" alt="" title="">
</div>
<h1>Claim your free Samples<br> of <span>Purina</span> Dog Food</h1>
</section>	
	<section class="free_samle_contnet_sec">

<div>
<h2>
Steps to get free sample
</h2>
</div>
<div class="steps_flex">
<div class="flex_blocks">
<figure>
<img src="/images/login-icon.png" width="" height="" alt="" title="">
<figcaption>
Login/Signup
</figcaption>
</figure>
</div>
<div class="flex_blocks">
<figure>
<img src="/images/fill-form.png" width="" height="" alt="" title="">
<figcaption>
Fill & Submit Form
</figcaption>
</figure>
</div>
</div>
	</section>
	<section class="free_samle_login_sec">
	<div>
	<a href="/login-free.php" data-ajax="false">claim the free sample</a>
	</div>
	</section>
		
</body>

</html>
<style>
.product-slider-link,.ui-btn{text-overflow:ellipsis;white-space:nowrap}.ui-mobile-viewport,html{-webkit-text-size-adjust:100%}.sticky-bar,.sticky-height{height:30px}.sale-text,.start-countdown{display:inline-block}.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333}#header,.ui-btn{text-align:left}.sticky-bar a,.sticky-bar a:hover{text-decoration:underline}.sticky-left{width:96%;float:left}.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px}.sale-text{font-size:12px}.sale-text a{color:#fc3;text-decoration:none}.ds-home-link{padding-top:30px!important}.h-m-fotter-sec .h-m-fotter h3,.h-m-fotter-sec-bottom ul li,.petstories-h-sec h2{text-transform:uppercase}.ui-btn{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;border:1px solid #ccc;padding-top:5px;padding-bottom:5px;border-radius:0!important}.ui-btn,body,button,input{font-size:1em;line-height:1.3;font-family:lato,sans-serif}.ui-input-text input{color:inherit;text-shadow:inherit}.ui-btn.ui-corner-all,.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em}.ui-corner-all{-webkit-background-clip:padding;background-clip:padding-box}.ui-body-a{border-width:1px;border-style:solid;background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3}.ui-btn:focus{outline:0}.ui-mobile,.ui-mobile body{height:99.9%}.ui-page{padding:0;margin:0;outline:0}.ui-mobile a img{border-width:0}.ui-mobile-viewport{margin:0;overflow-x:visible;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent}body.ui-mobile-viewport{overflow-x:hidden}.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0}.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden}@media screen and (orientation:portrait){.ui-mobile .ui-page{min-height:420px}}@media screen and (orientation:landscape){.ui-mobile .ui-page{min-height:300px}}.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0}.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em}.ui-btn{text-decoration:none!important;font-size:13px;margin:.5em 0;position:relative;overflow:hidden;cursor:pointer;user-select:none;padding-left:5px}#header div,#morepopup li a,.article-det-sec a,.h-m-fotter-sec .h-m-fotter ul li a,.product-slider-link a,.ui-link,ins{text-decoration:none}button.ui-btn{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-appearance:none;-moz-appearance:none}button.ui-btn::-moz-focus-inner{border:0}.ui-screen-hidden{display:none!important}.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%}.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099}.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important}.ui-popup{position:relative;padding:10px 10px 0}.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff}.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden}.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px)}.ui-input-text input{padding:.4em;line-height:1.4em;display:block;width:100%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;outline:0;background:0 0;border:1px solid #ccc;font-size:14px;margin:0;min-height:2.2em;text-align:left}.ui-input-text input::-moz-placeholder{color:#636363}.ui-input-text input:-ms-input-placeholder{color:#aaa}.ui-input-text input::-ms-clear{display:none}.ui-input-text input:focus{-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none}.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block}#header,#morepopup li:last-child{border-bottom:0}.ui-panel-page-container{overflow-x:visible}.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none}.panelicon,.purchase-product-cart{position:relative}.ui-panel-dismiss-open{display:block}.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease}.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0)}.ui-panel-position-left{left:-17em}.ui-panel-open.ui-panel-position-left{left:-1px}.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em}#header{height:50px;padding:12px;border-top:0;background:#6c9d06}#header .fa{font-size:18px;color:#fff}#header div,.panelicon{display:inline-block}#header div{color:#f7422d}.panelicon{height:19px;left:0;margin-top:0!important;top:0;width:18px}.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px}.ui-link{color:#333}.slider1-container{width:100%;height:auto}.slider1-container img{width:100%}.green_clr{font-family:gobold;margin-left:8px;color:#fff!important;font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335}*,.product-slider-link a,.recently_sec h4{font-family:lato,sans-serif}#morepopup li,#morepopup li a{color:#323232}#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px}#morepopup li img{margin-right:8px;vertical-align:top}#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px}#morepopup-screen{background:0 0}.content-search{padding-bottom:20px}.content-search form{margin:0 auto;position:relative;top:5px;width:95%}.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%}.btn-sub-sbmt,div.dsdropdown{position:relative}*{margin:0;padding:0;box-sizing:border-box}.cartsearch_blk a img{width:18px;height:19px}.content-search{width:100%;float:left;margin-top:0!important;padding:5px 0;background:#6c9d06}.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0}.petstories-h-sec{float:left;width:96%;margin-left:2%;margin-right:2%}.petstories-h-sec h2{width:100%;font-size:16px;letter-spacing:.6px;margin-top:15px}.article-det-sec{float:left;width:100%;margin-top:15px}.article-det-sec a{color:#333}.article-det-sec img{max-width:100%;height:auto}.article-det-sec h3{font-size:14px;font-weight:700;margin:10px 0 0;float:left;width:100%}.article-det-sec p{font-size:14px;color:#888;margin-top:5px;float:left;width:100%}.subscribe-sec{float:left;width:100%;margin-top:15px;background:#f9f9f9;padding:15px 5%}.subscribe-sec input{border:1px solid #ddd!important;height:45px;background:#fff}.btn-sub-sbmt{float:right;margin-right:6%;margin-top:-37px;background:#fff;border:none}.bank-logos,.h-m-fotter-sec{float:left;width:96%;margin-left:2%;margin-right:2%;margin-top:15px}.h-m-fotter-sec .h-m-fotter{float:left;width:100%;margin-bottom:15px}.h-m-fotter-sec .h-m-fotter h3{font-size:13px;margin-bottom:10px;font-weight:700}.h-m-fotter-sec .h-m-fotter ul li{float:left;padding-right:10px;border-right:1px solid #888;margin-right:10px;font-size:14px;margin-bottom:10px}.h-m-fotter-sec .h-m-fotter ul li:last-child{border-right:0;padding-left:0}.h-m-fotter-sec .h-m-fotter ul li a{color:#444}.h-m-fotter-sec-bottom{border-top:1px solid #ddd;padding:15px 0;float:left;width:100%}.h-m-fotter-sec-bottom ul{list-style:none;padding-left:5%;padding-right:5%}.h-m-fotter-sec-bottom ul li{float:left;width:31.3%;font-size:14px;padding:0 2%}.bank-logos img{max-width:100%}#header img{vertical-align:middle}.product-slider-link a{font-size:14px;color:#444;letter-spacing:.6px}.p-slide-img img{width:130px;height:auto}.p-slide-img:hover .p-s-image{-webkit-transform:scale(1.1);transform:scale(1.1)}.p-s-image{-webkit-transition:all .7s ease;transition:all .7s ease}.product-slider-link{padding:10px 0;text-align:left;line-height:24px;overflow:hidden;max-width:200px}.product-price-s{padding-bottom:10px;text-align:left}.product-price-s .p-d-price{font-size:14px;color:#999;margin-right:13px}.product-price-s .p-d-off{color:#ff7a22}.p-ds-off{color:#444;margin-right:13px}.banner-slider-top{width:100%;float:left}#defaultpanel4{z-index:99999}.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px}.m-sprite{background:url(https://m.dogspot.in/images/msprite.png) no-repeat;margin:auto}.m-user{float:left;background-position:-166px -40px}.m-orders{float:left;background-position:-33px -72px}.m-wishlist{float:left;background-position:-6px -72px}.m-logout{float:left;background-position:-78px -40px}.m-setting{float:left;background-position:-107px -40px}.m-contact-us{float:left;background-position:-139px -40px}ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc}#footer,.add-sans-sapce,.device,.nf-no,.p-slide-img,.pagination,.special-offer-section{text-align:center}ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc}ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%}ul.dsdropdown-menu{display:none}ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important}ul.dsdropdown-menu li:last-child{font-size:16px;border:0}ul.dsdropdown-menu li:hover{background:#f9f9f9}.nf-img{float:left;width:20%}.nf-no{position:absolute;width:17px;border-radius:17px;font-size:11px;height:17px;line-height:16px;background:red;color:#fff!important;border:1px solid red;top:-8px;right:-7px}.nf-text{float:left;width:80%}.nf-img img{max-width:50px;width:50px!important;height:auto!important}.nf-text h4{font-size:14px;padding-bottom:1px;color:#333}.nf-text p{color:#888;font-size:13px}.nf-active-bg{background:#f2f2f2}.cat-section,.hot_selling_sec,.recently_sec,.blogger_sec{float:left;width:100%;letter-spacing:.7px;margin-top:10px} .recently_sec h4{font-size:18px;padding:15px 10px;color:#333}.cat-section .cat-gallery,.hot_selling_carousel,.recently_carousel,.bloggers{overflow:scroll;overflow-y:hidden;white-space:nowrap;width:100%;border-bottom:1px solid #f2f2f2;padding-bottom:20px}.cat-section .cat-gallery a{font-size:12.3px;font-weight:700;letter-spacing:0;color:#555;text-decoration:none}.cat-section .cat-gallery div{margin-top:2px}.cat-section .cat-gallery ul,.hot_selling_carousel ul,.recently_carousel ul,.recently_sec ul,.bloggers ul{list-style:none;padding:0;margin:0}.cat-section .cat-gallery ul li{width:120px;text-align:center;border-left:1px solid #f4f4f4}.cat-section .cat-gallery ul li,.hot_selling_carousel ul li,.recently_carousel ul li,.blogger_sec .bloggers ul li{position:relative;display:inline-block;align-self:flex-start;border-radius:0;background:#fff;cursor:pointer;margin-left:10px}.cat-gallery::-webkit-scrollbar,.hot_selling_carousel::-webkit-scrollbar,.recently_carousel::-webkit-scrollbar{display:none}.cat-section .cat-gallery ul li:first-child{border:0}.hot_selling_carousel ul li{width:170px}.product-slider-link{width:173px}.banner-slider-top{margin-top:0}.special-offer-section{float:left;width:100%}.special-offer-lbanner img,.special-offer-sbanner img{width:100%;height:auto}.special-offer-sbanner{float:left;width:48%;padding:20px 0}.margin-left{margin-left:4%}a,body,del,div,em,form,h1,h2,h3,h4,html,i,iframe,img,ins,li,p,span,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline}#defaultpanel4,.whitewrapper{background-color:#fff}ul{list-style-type:none}:focus{outline:0}del{text-decoration:line-through}.cartsearch_blk a{margin-left:12px}.cartsearch_blk a:first-child{margin-left:0}#defaultpanel4{border-right:1px solid #ccc}#footer{font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:10px}.ui-link img{vertical-align:middle}@media only screen and (min-width :600px) and (max-width :1024px){.ui-input-text input{font-size:16px}}html{font-family:'Helvetica Neue',Helvetica,Arial,sans-serif}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-bars:before{content:"\f0c9"}.fa-bell-o:before{content:"\f0a2"}.fa-shopping-cart:before{content:"\f07a"}.fa-user-o:before{content:"\f2c0"}.fa-home:before{content:"\f015"}.fa-search:before{content:"\f002"}.swiper-wrapper{position:relative;width:100%;-webkit-transition-property:-webkit-transform,left,top;-webkit-transition-duration:0s;-webkit-transform:translate3d(0,0,0);-webkit-transition-timing-function:ease;-moz-transition-property:-moz-transform,left,top;-moz-transition-duration:0s;-moz-transform:translate3d(0,0,0);-moz-transition-timing-function:ease;-o-transition-property:-o-transform,left,top;-o-transition-duration:0s;-o-transition-timing-function:ease;-o-transform:translate(0,0);-ms-transition-property:-ms-transform,left,top;-ms-transition-duration:0s;-ms-transform:translate3d(0,0,0);-ms-transition-timing-function:ease;transition-property:transform,left,top;transition-duration:0s;transform:translate3d(0,0,0);transition-timing-function:ease;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}.swiper-slide{float:left;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;margin-bottom:-5px;max-height:100%}body{margin:0;font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height:1.5}.device{width:100%}.pagination{display:block;position:relative;width:100%;height:20px;top:-20px!important}.swiper-pagination-switch{display:inline-block;border-radius:10px;margin:0 2px;cursor:pointer}.add-sans-sapce{float:left;width:100%;margin:auto;max-height:120px}.accordion:after,.close{float:right;font-weight:700}.add-sans-sapce .add-banner{margin:auto}.panel div a{color:#333}.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none}.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4}.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4}.accordion:hover{background-color:#f8f8f8}.panel{padding:0;display:none;background-color:#fff;overflow:hidden}.panel div{padding:10px 25px}.accordion:after{content:'\002B';color:#777;margin-left:5px}.accordion.active:after{content:"\2212"}.ds-home-link{background:#6c9d06;padding:8px 10px}.ds-home-link a{color:#fff}.ds-nav-click{padding:12px}.ds-nav-click a{color:#333;text-decoration:none}.swiper-pagination-switch{background:#666!important;border:1px solid #666!important;width:14px!important;height:14px!important}.swiper-active-switch{background:#6c9d06!important;border:1px solid #6c9d06!important;width:14px!important;height:14px!important}.banner-slider-top{margin-bottom:5px}.modal{display:none;position:fixed;z-index:1;padding-top:180px;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:#000;background-color:rgba(0,0,0,.4)}.modal-content{background-color:#fefefe;margin:auto;padding:20px;border:1px solid #888;width:80%}.close{color:#aaa;font-size:28px;margin-top:-20px}.close:focus,.close:hover{color:#000;text-decoration:none;cursor:pointer}@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(https://m.dogspot.in/css/font-awesome/fonts/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal}.ac_results{padding:2px;border:1px solid #bfbfbf;background-color:#fff;overflow:hidden;z-index:999;text-align:left;top:205px;width:374px!important;border-radius:2px}.ac-results ul{width:100%;padding:0;margin:0 0 5px;font-size:16px}.highlight-suggestion{color:#333}.hl-sugg-vtcl{font-weight:700;color:#6C9D06}.ac-results li{margin:0;padding:2px 5px 2px 10px;cursor:default;display:block;font-size:13px;line-height:18px;overflow:hidden}.ac_over{background-color:#f2f2f2;color:#333}.ac-results .header{margin-top:12px;overflow:visible;background:#6C9D06;margin-bottom:5px}.ac-results .header .text{display:inline-block;position:relative;color:#fff;padding:0 5px}
.blogger_sec h4{font-size: 18px;
    padding: 15px 10px;
    color: #333;}
.blogger_sec h4 span{color: #6c9d06;}
.blogger_sec .bloggers::-webkit-scrollbar{ display:none;}
.blogger_sec .bloggers ul li{ width:130px; text-align: center;}
.blogger_sec .bloggers figcaption{padding-top: 10px;
    font-weight: 600;}
.h-m-fotter-sec-bottom{ padding-bottom:62px;}	
</style>
 <?php require_once('common/bottom-final.php'); ?>





























