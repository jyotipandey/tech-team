<div class="container" itemscope itemtype="http://schema.org/Event">
<? $query_map = query_execute_row("SELECT map FROM events where event_id = '$event_id'");?>
    <ul class="tabs">
      <li><a href="#tab1">Event Info</a></li>
      <li><a href="#tab2">About</a></li>
      <li><a href="#tab3">Other Information</a></li>
      <li><a href="#tab4">RSVP</a></li>
      <? if($query_map["map"]!='') { ?>
      <li><a href="#tab5">Map</a></li>
      <? } ?>
    </ul>
    <div class="tab_container">
      <div id="tab1" class="tab_content">
        <div class="slider">
          <table width="100%">
           <tr><td > <strong>Venue:</strong>&nbsp;</td>
                                         <td > <?= $venue ?></td>
                                            <span class="cb"></span>
                                        </tr> <? if ($late_entry_date != '0000-00-00 00:00:00') {?>
                                        <tr><td > <strong>Last Date Of Entry:</strong>&nbsp;</td>
                                      	 <td ><?= showdate($late_entry_date, "d M o") ?></td>
                                         <span class="cb"></span> <? } ?>   </tr>                                  
                                        <tr><td > <strong>Organized By:</strong>&nbsp;</td>
                                          <td > <? if ($organized_by != 1) {  echo "$organized_by";
} else {
    print breakLongWords($organized_other, 20, " ");
}
?></td>  <span class="cb"></span>
      <div class="dn">
      <span itemprop="startDate"><? print(showdate($start_date, "d M, o"));?></span>
      <span itemprop="endDate"><? print(showdate($end_date, "d M, o"));?></span>
      </div>
                                         </tr>
                                           <?
if ($recognized) {
?>
                                           <tr><td > 
                                            <strong>Recognised By:</strong>&nbsp;</td>
                                           
                                          <td >  <?
    if ($recognized && $recognized == 1) {
        $reco = "Kennel Club of India";
?>
                                            <?
        print $reco;
?>
                                            <?
    }
?>
                                            <?
    if ($recognised_other) {
        $reco = "$recognised_other";
?>
                                           <?
        print $reco;
?></td>
                                            <?
    }
?>
                                            <span class="cb"></span></tr>
                                         <?
}
?>
                                         
                                            <?
if ($expected_entry) {
?>
                                            <tr><td>
                                           
                                            <strong>Event Size:</strong>&nbsp;</td>
                                          <td> <?
    echo $expected_entry;
?>  <strong>entries expected ;</strong>  <?
    if ($expected_foot) {
        echo "$expected_foot";
?> <strong>footfalls expected</strong><?
    }
?> </td>
                                            <span class="cb"></span>
                                          </tr><?
}
?>
                                            
                                          <tr><td>
                                            <strong>Contact:</strong>&nbsp;</td>
                                            <td><?
if ($address) {
    print $address;
}
?>
       <?
if ($city_name) {
    echo "$city_name";
}
if ($state_name) {
    echo ", $state_name";
}
?>
       <?
if ($country_name) {
    echo "$country_name";
}
?><br />
       <?
if ($phone) {
    echo "$phone <br />";
}
?>
       <?
if ($mobile) {
    echo $mobile;
}
?>
       <?
if ($email) {
    print $email;
}
?></td>
   <div class="dn" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
         <span itemprop="addressLocality"><?="$city_name"?></span>
      <span itemprop="addressRegion"><?="$state_name"?></span>
      </div>
                                            <span class="cb"></span>
                                          </tr>
                                          <?
if ($entry_fee) {
?>
                                          <tr><td>
                                            
                                            <strong>Entry Fee:</strong>&nbsp;</td>
                                            <td><?
    print breakLongWords($entry_fee, 20, " ");
?></td>
                                            <span class="cb"></span>
                                          </tr>
                                            <?
}
?>

                                         <?php
/*?> <li>
<span >Entry Tickets :</span></li><?php */
?>
                                         
                                 </table>
        </div>
      </div>
      <div id="tab2" class="tab_content">
        <div class="slider">
		<table width="100%" >
       <? if ($member_club || $member_sec) { ?> <tr><td width="45%">
       <strong>Committee Members</strong></td></tr> 
          
  <? if ($member_club) { $member_club = stripslashes($member_club); ?>
      <tr>  
             <td ><strong>Club Secretary:</strong>&nbsp; </td>
             <td  ><? print breakLongWords($member_club, 20, " "); ?></td></tr>
           <?    } //End member_club  ?> 
           
        <?   if ($member_sec) {  $member_sec = stripslashes($member_sec); ?>
           <tr >
            <td ><strong>Event Secretary:</strong>&nbsp; </td>
            <td><?  print breakLongWords($member_sec, 20, " "); ?></td>
          </tr>
         <?
    } //End member_sec 
    $query4 = mysql_query("SELECT group_concat(judge) as judge FROM events_judges where event_id = '$event_id'");
    if (!$query4) {
        die(mysql_error());
    }
    $num_members = mysql_numrows($query4);
    if ($num_members != 0) {
        $row4  = mysql_fetch_array($query4);
        $judge = $row4["judge"];
        $judge = stripslashes($judge);

        if ($judge) {
?>
  <tr >
	<td><strong>Event Judge:</strong> &nbsp; </td>
    <td><? print $judge; ?></td>    
  </tr>
<?   } // judge ends here...

    } ?> 
   
<?
} //End while and if 

 //fetch data from events_other_member*******************************
$query3 = mysql_query("SELECT * FROM events_other_member where event_id = '$event_id'");
if (!$query3) {
    die(mysql_error());
}
$num_members = mysql_numrows($query3);
if ($num_members != 0) {
?>
<?
    while ($row3 = mysql_fetch_array($query3)) {
        $position = $row3["position"];
        $name     = $row3["name"];
        
        $position = stripslashes($position);
        $name     = stripslashes($name);
?>
 <?
        if ($position || $name) {
?>
  <tr >
    <td ><? if ($position) {  print breakLongWords($position, 20, " "); } ?></td>
    <td ><? if ($name) {  print breakLongWords($name, 20, " ");         }?></td>
  </tr>
<?
        }
    }
?>

<?
} //End while and if 
?>

<?
if ($about_org) {
    $about_org = makeClickableLinks($about_org);
    $about_org = nl2br(stripslashes($about_org));
    $about_org = breakLongWords($about_org, 30, " ");
?>
   <tr><td> <strong>About Organising Body:</strong>&nbsp;</td><td> <?= $about_org ?> </td></tr>   <?
}
?>
   
   <tr><td><strong>Event Posted by:&nbsp;</strong></td>
   <td>
 <a href="<?= 'https://www.dogspot.in/profile/' . $event_userid . '/'; ?>"><?= $display_name; ?></a></td></tr></table>
 </div>
      </div>
      <div id="tab3" class="tab_content">
        <div class="slider">
           <?
if ($other_info) {
    $other_info = makeClickableLinks($other_info);
    $other_info = nl2br(stripslashes($other_info));
    $other_info = breakLongWords($other_info, 30, " ");
    $other_info = stripslashes($other_info);
?>
                           
                          
                                            <span class="left"><strong>Others Information</strong>:&nbsp;&nbsp;</span>
                                            <span class="right"><?
    print $other_info;
?></span>
                                            <?
}
?>
                                            <span class="cb"></span>
                                        
                                        </br> 
                                        
                                         <?php
/*?><li>
<span class="left"><strong>Please note</strong>:&nbsp;&nbsp; </span>
<span class="right"> As event information is provided by organisers, public notices, magazines, kennel clubs etc and can change without notice, we advise you to contact organisers directly to confirm all details. DogSpot.in assumes no responsibility for any errors or omissions</span>
<span class="cb"></span>
</li><?php */
?>
        </div>
      </div>
      <div id="tab4" class="tab_content" style="clear:both;">
        <div class="slider">
         <?php
require_once($DOCUMENT_ROOT . '/new/includes/rspv.php');
?>
          

        </div>
      </div>
      <div id="tab5" class="tab_content">
        <div class="slider">
                                            <span class="left"><strong>Map</strong>:&nbsp;&nbsp;</span>
                                            <span class="right"><?= $query_map["map"]; ?></span>
                                            <span class="cb"></span>
                                        </br> 
        </div>
      </div>
    </div>
  </div>