(function(window, document, callback) {
    var d;
    var loaded = false;
    var j = window.jQuery;
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
    script.onload = script.onreadystatechange = function() {
        if (!loaded && (!(d = this.readyState) || d == "loaded" || d == "complete")) {
            callback((j = window.jQuery).noConflict(1), loaded = true, old = window.jQuery);
            j(script).remove();
        }
    };
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script);
})(window, document, function($, jquery_loaded, old) {


// initialize unbxdautocomplete by invoking the unbxdAutoSuggestFunction
// with jquery & handlebars as its parameters. you will need to do this only once.
unbxdAutoSuggestFunction($, Handlebars);

$(function(){
    //on dom load set autocomplete options
    //Usage: $(<element>).unbxdautocomplete(options);
    $("#search-query-new-unbox").unbxdautocomplete({
        siteName : 'dogspot_in-u1430736732864'//your site key which can be found on dashboard
            ,APIKey : '7672262516be61b5e812de43c043c244' //your api key which is mailed to during account creation or can be found on account section on the dashboard
            ,minChars : 1
            ,delay : 100
            ,loadingClass : 'unbxd-as-loading'
            ,mainWidth : 0
            ,zIndex : 9999999
            ,position : 'absolute'
            ,template : "2column"
            ,mainTpl: ['inFields', 'topQueries', 'popularProducts']
            ,sideTpl: ['keywordSuggestions']
            ,sideContentOn : "left"
            ,showCarts : true
            ,cartType : "separate"
            ,onSimpleEnter : function(){
                this.input.form.submit();
            }
            ,onItemSelect : function(data,original){
                if (data.type == "IN_FIELD") {
                    // catFilter = jQuery('.UI-CATEGORY').val();
                    if (data.filtername)
                        window.location = '//search.dogspot.in' + '/?q=' + encodeURI(data.value) + '&filter=' + encodeURI(data.filtername)+ '_fq:' + encodeURI('"' + data.filtervalue + '"');
                    else
                        this.input.form.submit();


                } else if (data.type == "POPULAR_PRODUCTS") {
                    //console.log(original.link);
                    window.location = original.item_link;
                } else {
                    this.input.form.submit();
                }
            }
            ,onCartClick : function(data,original){
                console.log("addtocart", arguments);
                return true;
            }
            ,inFields:{
                count: 3
                ,fields:{
                    'brand': 3
                    //,'product_type': 2
                }
                ,header: ''
                ,tpl: ''
            }
            ,topQueries:{
                count: 4
                ,header: ''
                ,tpl: ''
            }
            ,keywordSuggestions:{
                count: 4
                ,header: 'Keyword Suggestions'
                ,tpl: ''
            }
            ,popularProducts:{
                count: 3
                ,price: true
                ,priceFunctionOrKey : "price"
                ,image: true
                ,imageUrlOrFunction: "image_link"
                ,currency : "Rs. "
                ,header: 'Popular Products'
                ,tpl: ''
            }
        });
    });
});