$(function(){
	var slideHeight = 160; // px
	var defHeight = $('.wrap').height();
	if(defHeight >= slideHeight){
		$('.wrap').css('height' , slideHeight + 'px');
		$('.read-more').append('<a href="#" onclick="_gaq.push([\'_trackPageview\']);">[...] Read More</a>');
		$('.read-more a').click(function(){
			var curHeight = $('.wrap').height();
			if(curHeight == slideHeight){
				$('.wrap').animate({
				  height: defHeight
				}, "normal");
				$('.read-more a').html('[...] Read Less');
				$('.gradient').fadeOut();
			}else{
				$('.wrap').animate({
				  height: slideHeight
				}, "normal");
				$('.read-more a').html('[...] Read More');
				$('.gradient').fadeIn();
			}
			return false;
		});		
	}
});

