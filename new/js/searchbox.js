(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:9,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);
    //for QuickShop button hover event
$(document).ready(
	function () {
	    initCommonDropdownList();
	});

//for Ubernav buttons hover event
var currentUbernavHover;
var openingUbernavDropdown;
var closingUbernavDropdown;
$(document).ready(
    function(){
        $(".intersiteList li").find("a").hoverIntent(
            function(){
                currentUbernavHover = $(this);
                openingUbernavDropdown=setTimeout("showUbernavDropdown();",500);
                clearTimeout(closingUbernavDropdown);
            },
            function(){
                currentUbernavHover = "";
                closingUbernavDropdown = setTimeout("closeUbernavDropdown();",500);
                clearTimeout(openingUbernavDropdown);
            }
        );
    }
);
function showUbernavDropdown()
{
    $(".intersiteList li").find("a").removeClass("show");
    currentUbernavHover.addClass("show");
}
function closeUbernavDropdown()
{
   $(".intersiteList li").find("a").removeClass("show");
} 

    //for Dropdown List event
function initCommonDropdownList(){
    var closeTimeOut = null;
    //Drop down list
    $(".dropDownSelect").click(function () {
        $(this).find(".dropDownContent").find("ul").width($(this).find(".dropDownSelectContent").width() + 38);
        if ($(this).find(".dropDownContent").css("display") == "none") {
            $(".dropDownContent").hide();
            $(this).find(".dropDownContent").show();
        }
        else {
            $(this).find(".dropDownContent").css("display", "none");
            clearTimeout(closeTimeOut);
        }
    });
	$(".dropDownSelect ul li").click(function(){
	    var ListTitle = $(this).children("a").html();
	    $(this).parent().find("li").removeClass("selected");
	    $(this).addClass("selected");
	    $(this).parent().parent().parent().parent().find(".dropDownSelectContent").html(ListTitle);
	});

	$(".dropDownSelect").hover(
        function () {
            if (closeTimeOut != null) clearTimeout(closeTimeOut);
        },
        function () {
            closeTimeOut = setTimeout("closeSelect()", 300);
        }
    );
}

function closeSelect(){
    $(".dropDownSelect .dropDownContent").css("display", "none");
    if ($("#searchDropDown.dropDownShow").length > 0) {
        $("#searchDropDown").removeClass("dropDownShow");
    }
}

function showValueDropdownList(obj){
    var listValue = obj.find("li.selected").html();
    obj.find(".dropDownSelectContent").html(listValue);
}