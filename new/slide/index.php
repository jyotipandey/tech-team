<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dogspot: spot for all your dog needs</title>
<link rel="stylesheet" type="text/css" href="../css/dropdown.css" media="all"  />
<link type="text/css" rel="stylesheet" href="../css/main.css" media="all" />
<link type="text/css" rel="stylesheet" href="../css/headfoot.css" media="all" />
<script type="text/javascript" src="../js/jquery-1.js"></script>
<script type="text/javascript" src="../jquery.jcarousel.pack.js"></script>
<link rel="stylesheet" type="text/css" href="../skins.css" />
<link rel="stylesheet" type="text/css" href="../jquery.jcarousel.css" />
<!--   jCarousel core stylesheet-->

<script type="text/javascript">
function mycarousel_initCallback(carousel){
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        auto: 3,
        wrap: 'last',
		scroll: 1,
		initCallback: mycarousel_initCallback
    });
});
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


function showHideBox(divactive){
	
	
	$('.htab').each(function(index) {
		if(index==divactive){
			$(this).addClass("active");			
		}else{
			$(this).removeClass("active");
		}
	});	

	$('.pannel').each(function(index) {
		if(index==divactive){
			$(this).show();			
		}else{
			$(this).hide();
		}
	});
	if(divactive!=0){
		
    }
	
}
</script>
<!--[if lt IE 7]>
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<![endif]-->
</head>

<body>
	<!-- header -->
    
    	<div class="header">
        
        	<div class="vs5"></div>
            <p align="right" class="login" style="margin-top:0px;"><a href="#">Login</a> / <a href="#">Register</a> </p>
        </div>
    	  <div class="vs10"></div>
    

        
     <!-- green strip -->
	<div class="greenstrip">
		<div class="header">
			
            		<!-- logo cont -->
                    <div class="fl pt25">
            			<p align="left"><a href="#"><img src="../pix/dogspot_logo.gif" alt="Dogspot" title="Dogspot"  /></a></p></div>
                     <!-- logo cont -->
                     <!-- right link cont -->
     				<div class="fr cont497">
                  
                    	<ul class="rt" style="height:89px; overflow:hidden; margin-bottom:35px;">
                        	<li><a href="/shop/">shop</a><span>1200 products</span></li>
                            <li><a href="/puppies/">classifieds</a><span>3000 business listings</span></li>
                         	<li class="last"><a href="/dog-events/">dog shows</a><span>3000 business listings</span></li>
                            
                        </ul>
                    <span class="cl"></span>
                    
                    
					<!-- search box-->
                    	
							<div  class="search" >
                            	<div style="padding:10px 5px 0px 10px;">
                    			<div class="searchBox" >

								<div id='input-search-container' style="position:relative">
                           <div id='search-cont'>
                               <input type='text' id='search-query' value="Enter the Keywords" style="color:#666;"/> 
                           </div>
                          <div id='selection'>
                          <select name="" id="default-usage-select" style="width:135px; height:20px; border:none; margin-left:2px; color:#666;">
                             <option>Dog</option>
                                  <option>Cat</option>
                                  <option>Bird</option>
                                  <option>Reptile</option>
								  <option>Dog</option>
                             <option>Dog</option>
                                  <option>Cat</option>
                                  <option>Bird</option>
                                  <option>Reptile</option>
								  <option>Dog</option>   
                             </select>                            
                           </div>

                            <!--<div id='arrow-up-down'>
                                 <div></div> 
                           </div>
                            
                            <div id="options-cont" style="z-index:99; display:block; position:absolute;">
                            <span style="position:absolute;">
                               <ul>
                                 
                               </ul>
                               </span>
                            </div>-->
                            
                        </div>

                        </div>
                        <div style="float:left; width:50px;">
						<a href="#"><img src="../pix/search_btn.gif" alt="searh" title="search"/></a>
						</div>
						<div class="cb"></div>

                        </div>
                        </div><!-- search box-->

                   
                    
                    </div> <!-- right link cont -->
            
     		
     <div class="cb"></div>
		</div><!-- end of header -->
		
		</div><!-- end of green strip -->
    
    
     
  
        <!-- navigation -->
        <div class="petSpecificSecNavBox">
    		<div class="wrap">
                 <div class="petSpecificNavigation">
                     <div class="petSpecificsecNavSide"></div>
    
                   
                        <div id="siteNav">
                        <div id="naviCont">
   		  <ul class="dropdown" style="margin:0px; padding:0px; list-style:none;">
		    <li><a href="/" class="dir" style="padding-left:12px;">home</a></li>
			  <li><a href="/shop/" class="dir">shop</a>
           	
  								<ul>
                              <li >
                                    <strong>shop <em>categories</em></strong>
                                     <a href="./">Food</a>
                                       <a href="./">Puppy Care</a>
                                         <a href="./">Treats</a>
                                           <a href="./">Collars Leashes</a>
                                             <a href="./">Toys</a>
                                                <a href="./">Accessories</a>
                                                   <a href="./">Gromming</a>
                                                    <a href="./" class="viewall">Find All Categories</a>
                                             
                                    
                                </li>
                                <li style="border:none; padding-right:0px; margin-left:8px;">
                               		 <strong>shop <em>brands</em></strong>
                                     <a href="./">Royal Canin</a>
                                     <a href="./">Dogs & More</a>
                                     <a href="./">Forbis</a>
                                     <a href="./">Chomp</a>
                                    <a href="./">Beapher</a>
                                         <a href="./">Beapher</a>
                                     <a href="./">Head up for Trails</a>
                                      <a href="./" class="viewall">Find All Brands</a>
                              </li>
		    	
						</ul>
                

   		 </li>
         <li ><a href="/dog-blog/" class="dir">articles</a></li>
		 <li class="linkdiv"><a href="/dog-events/" class="dir">dog shows</a>
				<ul>
                              <li >
                                    <strong>shop <em>categories</em></strong>
                                     <a href="./">Food</a>
                                       <a href="./">Puppy Care</a>
                                         <a href="./">Treats</a>
                                           <a href="./">Collars Leashes</a>
                                             <a href="./">Toys</a>
                                                <a href="./">Accessories</a>
                                                   <a href="./">Gromming</a>
                                                    <a href="./" class="viewall">Find All Categories</a>
                                             
                                    
                                </li>
                                <li style="border:none; padding-right:0px;">
                               		 <strong>shop <em>brands</em></strong>
                                     <a href="./">Royal Canin</a>
                                     <a href="./">Dogs & More</a>
                                     <a href="./">Forbis</a>
                                     <a href="./">Chomp</a>
                                    <a href="./">Beapher</a>
                                         <a href="./">Beapher</a>
                                     <a href="./">Head up for Trails</a>
                                      <a href="./" class="viewall">Find All Brands</a>
                              </li>
		    	
						</ul>
		</li>
		<li class="linkdiv"><a href="/puppies/" class="dir">puppies available</a></li>
		<li class="linkdiv"><a href="/dog-listing/" class="dir">business</a>
		 <ul>
                              <li >
                                    <strong>shop <em>categories</em></strong>
                                     <a href="./">Food</a>
                                       <a href="./">Puppy Care</a>
                                         <a href="./">Treats</a>
                                           <a href="./">Collars Leashes</a>
                                             <a href="./">Toys</a>
                                                <a href="./">Accessories</a>
                                                   <a href="./">Gromming</a>
                                                    <a href="./" class="viewall">Find All Categories</a>
                                             
                                    
                                </li>
                                <li style="border:none; padding-right:0px;">
                               		 <strong>shop <em>brands</em></strong>
                                     <a href="./">Royal Canin</a>
                                     <a href="./">Dogs & More</a>
                                     <a href="./">Forbis</a>
                                     <a href="./">Chomp</a>
                                    <a href="./">Beapher</a>
                                         <a href="./">Beapher</a>
                                     <a href="./">Head up for Trails</a>
                                      <a href="./" class="viewall">Find All Brands</a>
                              </li>
		    	
						</ul>
		</li>
		<li class="linkdiv" style="position:relative;"><a href="/dogs/" class="dir">dog gallery</a>
		<ul style="position:absolute; left:-248px;">
                                <li >
                                    <strong>shop <em>categories</em></strong>
                                     <a href="./">Food</a>
                                       <a href="./">Puppy Care</a>
                                         <a href="./">Treats</a>
                                           <a href="./">Collars Leashes</a>
                                             <a href="./">Toys</a>
                                                <a href="./">Accessories</a>
                                                   <a href="./">Gromming</a>
                                                    <a href="./" class="viewall">Find All Categories</a>
                                             
                                    
                                </li>
                                <li style="border:none; padding-right:0px;">
                               		 <strong>shop <em>brands</em></strong>
                                     <a href="./">Royal Canin</a>
                                     <a href="./">Dogs & More</a>
                                     <a href="./">Forbis</a>
                                     <a href="./">Chomp</a>
                                    <a href="./">Beapher</a>
                                         <a href="./">Beapher</a>
                                     <a href="./">Head up for Trails</a>
                                      <a href="./" class="viewall">Find All Brands</a>
                              </li>
		    	
						</ul>
		</li>
		<li  style="border:none; padding-right:0px; background-image:none; position:relative"><a href="/qna/" class="dir">answers</a>
		 <ul style="position:absolute; left:-274px;">
                                <li >
                                    <strong>shop <em>categories</em></strong>
                                     <a href="./">Food</a>
                                       <a href="./">Puppy Care</a>
                                         <a href="./">Treats</a>
                                           <a href="./">Collars Leashes</a>
                                             <a href="./">Toys</a>
                                                <a href="./">Accessories</a>
                                                   <a href="./">Gromming</a>
                                                    <a href="./" class="viewall">Find All Categories</a>
                                             
                                    
                                </li>
                                <li style="border:none; padding-right:0px;">
                               		 <strong>shop <em>brands</em></strong>
                                     <a href="./">Royal Canin</a>
                                     <a href="./">Dogs & More</a>
                                     <a href="./">Forbis</a>
                                     <a href="./">Chomp</a>
                                    <a href="./">Beapher</a>
                                         <a href="./">Beapher</a>
                                     <a href="./">Head up for Trails</a>
                                      <a href="./" class="viewall">Find All Brands</a>
                              </li>
		    	
						</ul>
		</li>
    	
</ul>

 <div class="cb"></div>
 </div></div>
                        
                        <!-- navi drop down -->
                    <div class="petSpecificsecNavSide"></div>
                </div>
            </div>
         </div><!-- navigation -->
      
        
        
    
            
      
        
   <div class="vs20"></div>
   <!-- first half two column -->
   
   <!-- left cont -->
<div class="cont980">
   <div class="cont660" >
   <!--Shop Box-->
   	<div id="wrap" class="pannel">
   	<ul class="slide jcarousel-skin-tango" id="mycarousel">
    	<li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        <li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        <li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        <li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        <li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        <li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
   </ul> 
    <span class="cl"></span>
   </div>
   <!--Shop Box-->
   <!--puppy box-->
   <div id="wrap" class="pannel">
   
   	<ul class="slide">
    	<li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        <li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        <li><a href="/shop/baby-dog-milk-400gm"><img src="https://www.dogspot.in/shop/item-images/thumb_1311163805562.jpg" alt="Baby Dog Milk" title="Baby Dog Milk"   /></a><h2><a href="/shop/baby-dog-milk-400gm">Baby Dog Milk</a></h2><a href="/shop/baby-dog-milk-400gm" class="link" style="color:#900; font-weight:bold">Rs. 938</a> <a href="/shop/baby-dog-milk-400gm" class="viewall">View More »</a></li>
        
   </ul> 
    <span class="cl"></span>
   </div>
   <!--puppy box-->
   </div>
   <!-- left cont -->
   
   
   <!-- right cont -->
   
   <div class="cont300">
   <p><a href="#"><img src="../pix/advertise.gif" alt="" title="" class="border"  /></a></p>
   
   </div><!-- right cont -->
   
   

   <div class="cb"></div>
     <div class="vs10"></div>
   <!-- tabs -->
   <div>
   	<ul class="hometabs">
    	<li style="width:200px;" class="htab active"><span><a href="#" onclick="Javascript:showHideBox(0, $(this))"><b>dogs shop</b>Online Pet Shops</a></span></li>
        <li style="width:250px;" class="htab"><span><a href="#" onclick="Javascript:showHideBox(1)"><b>puppies available</b>Buys Pubs in India</a></span></li>
        <li style="width:250px;" class="htab"><span><a href="#"><b>dogs show in india</b>Photos and Results</a></span></li>
        <li style="width:245px; margin-right:0px;" class="htab"><span><a href="#"><b>answers to question!</b>Ask anything related to dog?</a></span></li>
       
   
   	</ul>
    <span class="cl"></span>
   </div><!-- tabs -->
      <!-- first half two column -->
   <div class="vs20"></div>
   <div class="hrline"></div>
   <div class="vs20"></div>
   <!-- dog gallery -->
   <div class="cont660">
   		<h3><a href="javascript:showHideBox();">dog galleryff</a></h3>
        <div class="w320 fl" style=" margin-right:15px;">
        	<p><img src="../pix/doggallery.jpg" alt="" title="" border="0" class="border" /></p>
            <div class="vs5"></div>
            <h2><a href="#">Jumping Jack</a></h2>
            <p>Dober man</p>
            <p ><a href="#" class="viewmore">Owner Details</a></p>
        
        </div>
        <div class="w320 fl">
        	<p><img src="../pix/gallery_2.jpg" class="border" style="margin-right:10px;"  alt="" title="" /><img src="../pix/gallery_2.jpg" alt="" title="" border="0" class="border" /></p>
            <p style="margin-top:10px;"><img src="../pix/gallery_2.jpg" class="border mr5"  alt="" title=""   style="margin-right:10px;"/><img src="../pix/gallery_2.jpg" alt="" title="" border="0" class="border" /></p>
            
        
        </div>
        <div class="cb"></div>
   </div>
   <div class="cont300">
   	<!-- ad slot -->
    <p class="pt10"><img src="../pix/ad3.gif" alt="" title="" border="0" /></p>
    <!-- ad slot -->
   
   </div>
   <div class="cb"></div>
     <div class="vs20"></div>
   <div class="hrline"></div>
   <div class="vs20"></div>
   
   <!-- dog gallery -->
   
   <!-- three columns -->
   	<!--column 1 -->
   	<div class="col280">
    	<!-- title -->
    	<h3><a href="#">articles</a></h3><!-- title -->
        <!-- content -->
        <div class="imgtxtcont">
            <div class="imghold"><a href="#"><img src="../pix/articles_profile.gif" alt="" title=""  /></a></div>
            <div class="txthold"><h2><a href="#">Does your dog goes to anyone else if <br  />he/she calls</a></h2><p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p></div> 
            <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont">
            <div class="imghold"><a href="#"><img src="../pix/articles_profile.gif" alt="" title=""  /></a></div>
            <div class="txthold"><h2><a href="#">Does your dog goes to anyone else if <br  />he/she calls</a></h2><p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p></div> 
            <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont">
            <div class="imghold"><a href="#"><img src="../pix/articles_profile.gif" alt="" title=""  /></a></div>
            <div class="txthold"><h2><a href="#">Does your dog goes to anyone else if <br  />he/she calls</a></h2><p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p></div> 
            <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont">
            <div class="imghold"><a href="#"><img src="../pix/articles_profile.gif" alt="" title=""  /></a></div>
            <div class="txthold"><h2><a href="#">Does your dog goes to anyone else if <br  />he/she calls</a></h2><p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p></div> 
            <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont last">
            <div class="imghold"><a href="#"><img src="../pix/articles_profile.gif" alt="" title=""  /></a></div>
            <div class="txthold"><h2><a href="#">Does your dog goes to anyone else if <br  />he/she calls</a></h2><p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p></div> 
            <div class="cb"></div>
        
        </div>
        <a href="#" class="viewall">View More »</a><!-- content -->
      </div><!--column 1 -->
      
      <!--column 2 -->
   	<div class="col280">
    	<!-- title -->
    	<h3><a href="#">upcoming events</a></h3><!-- title -->
        <!-- content -->
        <div class="imgtxtcont last" style="padding-bottom:5px;">
        <div class="imghold" style="width:120px; height:100px;"><a href="#"><img src="../pix/events_img.gif" alt="" title=""  /></a></div>
        <div class="txthold" style="width:140px;"><h2><a href="#">German Shepherd Spec</a></h2><p class="text">Organised by <br /><a href="#">DG Group Ltd</a><p class="text" style="color:#e53000;">Sat, Dec 31, 2011</p><p class="text">Delhi</p><p style=" margin-top:5px; color:#84a40d;">(22 Attending)</p></div> 
        <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont last" style="padding-bottom:5px;">
        <div class="imghold" style="width:120px; height:100px;"><a href="#"><img src="../pix/events_img.gif" alt="" title=""  /></a></div>
        <div class="txthold" style="width:140px;"><h2><a href="#">German Shepherd Spec</a></h2><p class="text">Organised by <br /><a href="#">DG Group Ltd</a><p class="text" style="color:#e53000;">Sat, Dec 31, 2011</p><p class="text">Delhi</p><p style=" margin-top:5px; color:#84a40d;">(22 Attending)</p></div> 
        <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont last" style="padding-bottom:5px;">
        <div class="imghold" style="width:120px; height:100px;"><a href="#"><img src="../pix/events_img.gif" alt="" title=""  /></a></div>
        <div class="txthold" style="width:140px;"><h2><a href="#">German Shepherd Spec</a></h2><p class="text">Organised by <br /><a href="#">DG Group Ltd</a><p class="text" style="color:#e53000;">Sat, Dec 31, 2011</p><p >Delhi</p><p style=" margin-top:5px; color:#84a40d;">(22 Attending)</p></div> 
        <div class="cb"></div>
        </div>
        
        
        <!-- content -->
      </div><!--column 2 -->
      
      
      <!--column 3 -->
   	<div class="col280" style="padding:0; margin:0; border:0; width:300px;">
    	<!-- title -->
    	<h3 style="padding-bottom:11px;"><a href="#">business listings</a></h3><!-- title -->
        <!-- content -->
        <div class="imgtxtcont">
        
        <div class="txthold" style="width:300px;"><h2><a href="#">Dog for Sales</a><img src="../pix/star_icon.gif" align="absmiddle" /></h2>
        <p class="text">Good Quality Lab pubs for sales 4 male and 3 female Available for sale in Black and Golden color...</p>
        <p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p>
        </div> 
        <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont">
        
        <div class="txthold" style="width:300px;"><h2><a href="#">Dog for Sales</a><img src="../pix/star_icon.gif" align="absmiddle" /></h2>
        <p class="text">Good Quality Lab pubs for sales 4 male and 3 female Available for sale in Black and Golden color...</p>
        <p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p>
        </div> 
        <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont">
        
        <div class="txthold" style="width:300px;"><h2><a href="#">Dog for Sales</a><img src="../pix/star_icon.gif" align="absmiddle" /></h2>
        <p class="text">Good Quality Lab pubs for sales 4 male and 3 female Available for sale in Black and Golden color...</p>
        <p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p>
        </div> 
        <div class="cb"></div>
        </div>
        
        <div class="imgtxtcont last">
        
        <div class="txthold" style="width:300px;"><h2><a href="#">Dog for Sales</a><img src="../pix/star_icon.gif" align="absmiddle" /></h2>
        <p class="text">Good Quality Lab pubs for sales 4 male and 3 female Available for sale in Black and Golden color...</p>
        <p class="caption">Posted by <a href="#">Rana</a>  |  On 22 January 2012</p>
        </div> 
        <div class="cb"></div>
        </div>
        <!-- content -->
        
       
      </div><!--column 3 -->
       
      
    <div class="cb"></div>
   
   <!-- three columns -->
   <div class="vs20"></div>
   <div class="hrline"></div>
   
   
   <!-- footer -->
   	<div id="footer">
    	<!-- left container -->
   	  <div class="fcont300">
        	<h2>Quick Links</h2>
            <div class="vs10"></div>
            <ul>
           	  <li>
                <a href="#">Home</a>
                <a href="#">Shop</a>
                <a href="#">Articles</a>
                <a href="#">Blogs</a>
                <a href="#">Shows</a>
              </li>
              
              <li>
                <a href="#">Discussion</a>
                <a href="#">Puppies Available</a>
                <a href="#">Business</a>
                <a href="#">Questions & Answer</a>
                <a href="#">Dog Gallery</a>
              </li>
              
              <li>
                <a href="#">Blogs</a>
                <a href="#">About us</a>
                <a href="#">Team</a>
                <a href="#">FAQs</a>
                <a href="#">Contact us</a>
              </li>
            
            
            
            </ul>
          <span class="cl"></span>        
          </div><!-- left container -->
          
          <!-- middle container -->
          
          <div class="fcont300" >
          	<h2>Shipping Information</h2>
            <div class="vs10"></div>
            
            <ul>
           	  <li>
                <a href="#">Shipping & Delivery Policy</a>
                <a href="#">Order Cancellation</a>
                <a href="#">Replacement</a>
                <a href="#">Refund & Return</a>
                <a href="#">Terms & Conditions</a>
              </li>
              
              <li>
                <a href="#">Disclaimer Policy</a>
                <a href="#">Privacy Policy</a>
                <a href="#">Copyright</a>
                
              </li>
              
            
            </ul>
            <span class="cl"></span>
            
            </div><!-- middle container -->
            
            <!-- right container -->
            
            <div class="fcont300" style="margin-right:0px; padding-right:0px; border:0px;">
            <h2>Payment Gateway</h2>
            <div class="vs10"></div>
            <p><a href="#"><img src="../pix/paypal_icon.gif" alt="" title="" border="0" class="mr5" /></a><a href="#"><img src="../pix/mastercard.gif" alt="" title="" border="0" class="mr5" /></a><a href="#"><img src="../pix/maestro_icon.gif" alt="" title="" border="0" class="mr5"  /></a><a href="#"><img src="../pix/visa_icon.gif" alt="" title="" border="0"  /></a></p>
            <div class="vs20"></div>
            <p class="btn"><span><a href="#">
            Recommend your friends</a></span></p>
            
            </div><!-- right container -->
          
        <div class="cb"></div>
    </div><!-- footer -->
    
    <div class="vs20"></div>
    <div class="hrline"></div>
    <div class="vs20"></div>
    
    <!-- footer2 -->
    <div class="fl f2cont750">
    <p align="left">
    Views and Articles are not endorsed by DogSpot.in. DogSpot does not assume responsibility or liability for any Comment or for any claims, damages, or losses resulting from any use of the Site or the materials contained therein. All contributions and Articles are owned by DogSpot.in. </p>
    
    </div>
    
    <div class="fr">
    <p align="right">
    <a href="#"><img src="../pix/fb_icon.gif" alt="Facebook" title="Facebook" border="0"   /></a><a href="#"><img src="../pix/twitter_icon.gif" alt="Twitter" title="Twitter" border="0"  /></a><a href="#"><img src="../pix/flicker_icon.gif" alt="Flicker" title="Flicker" border="0"  /></a> </p>
    </div><!-- footer 2-->
    <div class="cb"></div>
    
   
   
    
   
   
  
   </div><!-- main container -->
   
    
     
    
</body>
</html>
