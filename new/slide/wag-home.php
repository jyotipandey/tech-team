<?php
require_once('constants.php');
require_once(MYSITE_ROOT_URL.'/database.php');
require_once(MYSITE_ROOT_URL.'/functions.php');
require_once(MYSITE_ROOT_URL.'/session-no.php');
include(MYSITE_ROOT_URL. "/new/articles/catarray.php");

$sel_wag_dogs=query_execute("SELECT * FROM dogs_available WHERE publish_status='publish' ORDER BY cdate DESC LIMIT 21");
if(!$section[0]){
$image_session_array = "";
$video_session_array = "";
$BkUrl = "/";
$BkUrlVideo = "/";
session_register("BkUrl");
session_register("BkUrlVideo");
}
if ($logoff == "logoff"){
	if(isset($_COOKIE['cookid']) && isset($_COOKIE['cookLevel']) && isset($_COOKIE['cookName'])){
	   $cookieTime = time()-60*60*24*100;
	   setcookie("cookid", $sessionuserid, $cookieTime, "/");
	   setcookie("cookLevel", $sessionLevel, $cookieTime, "/");
	   setcookie("cookName", $sessionName, $cookieTime, "/");
	   setcookie("cookProfileImg", $sessionProfileImg,   $cookieTime, "/");
	   setcookie("cookFacebook",   $sessionFacebook,   $cookieTime, "/");
	   setcookie("cookTwitter",   $sessionTwitter,   $cookieTime, "/");
	   setcookie("cookSocialaction",   $sessionSocialaction,   $cookieTime, "/");
	}
	session_regenerate_id();
	session_destroy();
	unset($_SESSION);
	session_start();
	removeOfflineUser($userid); //remove Offline User details
	
	// refresh the page
	header ("Location: /"); 
	ob_end_flush();
	exit(); 
}
$sitesection = "HOME";
$ajaxbody = "HOME";

// TOP PHP-----------------------------------------------------------
if($sitesection != "photos"){
	$image_session_array = "";
	$BkUrl = "";
}
include($DOCUMENT_ROOT."/chquserstat.php");
// Record user
$display_user_Name = dispUname($userid);
$ip = ipCheck();
recordOnlineUser($userid, $display_user_Name, $ip, $requested);
include($DOCUMENT_ROOT."/common/savesocialsettings.php");
// TOP PHP------------------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="EAcsZ1OXjGYexR+hCVtgUTDT+zeClPHe7kvPgUUqUwo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>DogSpot.in | Dog Breeds | Online Pet Products Shopping India - Spot for all your Dog's Need</title>

<meta name="keywords" content="Dogs India, Dog Pictures, Dog Breeds, Dog Names, Dog Training, Types Of Dogs, Dog Images, Puppies In India, Dogs, Dog, Dog Games, Dog Kennel, Dog Grooming, Dog India, Dog Show" />

<meta name="description" content="DogSpot.in is India's Best site for Online Pet Products Shop and provide all dog Breeds Information,Breeders,Pet Training, Pet Articles, Dog Shows, Dog Photos ." />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<script type="text/javascript" src="/new/slide/jquery.jcarousel.pack.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script type="text/javascript">
function mycarousel_initCallback(carousel){
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

$(document).ready(function() {
    $('#fp1').jcarousel({
        auto: 0,
        wrap: 'last',
		scroll: 4,
		
		initCallback: mycarousel_initCallback
    });
	$('#fc1').jcarousel({
        auto: 0,
        wrap: 'last',
		scroll: 4,
		initCallback: mycarousel_initCallback
    });
	$('#fc2').jcarousel({
        auto: 0,
        wrap: 'last',
		scroll: 4,
		initCallback: mycarousel_initCallback
    });
	$('#fc3').jcarousel({
        auto: 1,
        wrap: 'last',
		scroll: 1,
		//animation:15000,
		animation:'slow',
		initCallback: mycarousel_initCallback
    });
	$('#fc4').jcarousel({
        auto: 1,
        wrap: 'last',
		scroll: 1,
		initCallback: mycarousel_initCallback
    });
});
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!--TOP HTML END-->

<!--Top JS-->
<?php /*?><script type="text/javascript">
function loadsubscribe(){
	centerPopup();
	loadPopup();
}
function clickclear(thisfield, defaulttext) {
	if (thisfield.value == defaulttext) {
		thisfield.value = "";
	}
}
function clickrecall(thisfield, defaulttext) {
	if (thisfield.value == "") {
		thisfield.value = defaulttext;
	}
}
</script><?php */?>
<!--Top JS END-->
<?php /*?><script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript">
hs.graphicsDir = '/images/graphics/';
</script><?php */?>

<?php /*?><script type="text/javascript">
function showLayer(d) {
var i;
	for(i = 0; i <= 6; i++){
		var hp="FeatureID"+i;
		if(d == hp){
		document.getElementById(d).style.display = "block";
		}else{
		document.getElementById(hp).style.display = "none";
		}
	}
}
function showHideBox(divactive){
	$('.htab').each(function(index) {
		if(index==divactive){
			$(this).addClass("active");			
		}else{
			$(this).removeClass("active");
		}
	});	

	$('.pannel').each(function(index) {
		if(index==divactive){
			if(divactive==0){
				clearInterval(slideShop);
				start();
			}
			
			$(this).show();			
		}else{
			$(this).hide();
		}
	});	
}
function showHideDogBox(divactiveDog){
	$('.dogGalleryBox').each(function(index) {
		if(index==divactiveDog){
			$(this).show();			
		}else{
			$(this).hide();
		}
	});	
}
</script><?php */?>
<!--function for photo slider home page-->
<link href="/shop-slider/css/nivo-slider.css" rel="stylesheet" type="text/css" />

<?php
/*?><script type="text/javascript" src="/shop-slider/js/jquery-1.7.1.min.js"></script>
<?php */
?>
<script type="text/javascript" src="/shop-slider/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="/shop-slider/js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>

<!-- function for photo slider ends here -->

<script> 
$(function(){
	start();
});

function start(){
	$('.bannerSlide ul:gt(0)').hide();
	slideShop = setInterval(function(){$('.bannerSlide ul:first-child').hide(1000).next('ul').show(1000).end().appendTo('.bannerSlide');}, 5000);	
}

</script>
<style type="text/css"> 
<!--
.bannerSlide {position:relative; height:248px; width:660px; z-index:0;}
.bannerSlide ul { position:absolute; left:0; top:0; }
-->
</style> 
<style>
.wag_rec_dg1 {
float: left;
width: 100%;
}
.wag_indexSec {
width: 714px;
float: left;
}

.wag_indexText .be_click {text-transform: uppercase;
float: left;
margin: 15px 0px;
color: #fff;
font-size: 20px;
width: 92%;
}
.wag_indexText p{ font-size:20px;text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;}
.wag_indexText .be_click a {
color: #fff;
padding: 6px 10px;
background: #a3cc00;
float: left;
width: 100%;
text-align: center;
border-radius: 3px;
text-decoration: none;
}
.wag_indexText .be_click a:hover{background: #668000;}
.wag_indexText {
float: left;
width: 217px;
margin:42px 0 0 25px;
color: #fff;
}
.wag_indexSec .dg_popBox.dg_pop2Box .dg_popImg img {
width: 100px;
height: 100px;
}
.wag_indexText p strong {
font-size: 25px;
}
.homepage-fancystripe {
height: .6em;
float: left;
width: 100%;
}
.wag_indexSec .dg_popBox.dg_pop2Box {
width: 100px;
padding: 0px;position: relative;border: 1px solid #fff;
}
.wag_indexSec .dg_popBox.dg_pop2Box:hover .dg_popTxt { display:block;}
.wag_indexSec .dg_popBox.dg_pop2Box .dg_popImg {
width: 100px;
height: 100px;
border-radius: 0px;
border: 0px solid #fff;
}
.wag_indexSec .dg_popTxt {
float: left; display:none;
background: rgba(255, 255, 255, 0.82);
margin-top: 0;
padding: 0px;
width: 100%;
text-align: center;
height: 100%;
position: absolute;
}
.dg_popTxt h3 {
text-transform: uppercase;
font-size: 14px;margin-top: 14px;
margin-bottom: 5px;
}
.wag_indexText h3 {
float: left;
color: #668000;
text-align: left;
font-size: 17px;
letter-spacing: 1px;
line-height: 25px;
font-weight: normal;
font-style: italic;
margin-top: 8px;
}
.wag_indexText h4 {
float: left;
width: 94%;
font-size: 22px;
color: #FF9D0A;
line-height: 32px;
text-transform: uppercase;
text-align: left;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
$('a.login-window').click(function() {
var loginBox = $(this).attr('href');
$(loginBox).fadeIn(300);
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
});
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});</script>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script> 
$(function(){
	$('#homeSlide a:gt(0)').hide();
	slideShop = setInterval(function(){$('#homeSlide a:first-child').hide(1000).next('a').show(1000).end().appendTo('#homeSlide');}, 5000);
});
$(function(){
	$('#homeSlide1 a:gt(0)').hide();
	slideShop = setInterval(function(){$('#homeSlide1 a:first-child').hide(1000).next('a').show(1000).end().appendTo('#homeSlide1');}, 5000);
});
</script>
<style type="text/css"> 
<!--
#homeSlide {position:relative; height:260px; width:486px;}
#homeSlide a{ position:absolute; left:0; top:0; }
#homeSlide1 {position:relative; height:260px; width:486px;}
#homeSlide1 a{ position:absolute; left:0; top:0; }
-->
</style> 
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<?php /*?><script type="text/javascript" src="/new/shop/peel/peel.js"></script><?php */?>


<? require_once($DOCUMENT_ROOT . '/new/common/header.php');?>
	<!-- header -->
    <!-- ViralMint Javascript -->
<?php /*?><script type="text/javascript">
(function(d, s, id) {
var js, vjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//cdn.viralmint.com/js/viralmint-min.js";
js.acc_id = "1365080648"; js.async=true;
vjs.parentNode.insertBefore(js, vjs);
}(document, "script", "viralmint-js"));
</script><?php */?>
<!-- ViralMint Javascript -->
 
 
 
   <!-- left cont -->
<div class="cont980">
<div class="vs10"></div>
<div style="margin:auto; width:960px; "> 
    
    <div id="photoslider" style="margin-bottom:50px; margin-top:13px; float:left">
    <div class="slider-wrapper theme-default">
      <div id="slider" class="nivoSlider">
    
<?php /*?><a href="https://www.dogspot.in/shop/?utm_source=shop_for_a_cause_banner&utm_medium=shop_for_a_cause_banner&utm_campaign=shop_for_a_cause_banner"><img src="/allbanners/dogspot-shop.jpg" title="DogSpot Shop For Cause" alt="DogSpot Shop For Cause"  border="0" /></a><?php */?>

<?php /*?><a href="http://www.fishspot.in/"><img src="/new/pix/960_banner-for-fish-products.jpg" title="Fish Products" alt="Fish Products"  border="0" /></a>
  <a href="https://www.dogspot.in/thinkdog__think-dog-delhi/"><img src="/new/pix/think-dog1.jpg" width="960" height="331" title="" alt=""  border="0" /></a><?php */?>
    <a href="https://www.dogspot.in/flea-ticks/?utm_source=DogSpot-Flea%26Ticks&utm_medium=HomepageBanner&utm_campaign=DogSpot-Flea%26Ticks"><img src="/new/pix/flea-protection-v1x960.jpg" width="960" height="331" title="" alt=""  border="0" /></a>
    
 <a href="https://www.dogspot.in/royal-canin/?utm_source=RC_Home_Page_Banner&utm_medium=RC_Home_Page_Banner&utm_campaign=RC_Home_Page_Banner"><img src="/new/pix/rc2.jpg" width="960" height="331" title="" alt=""  border="0" /></a>
 
 <a href="https://www.dogspot.in/treats/?utm_source=DogSpot-Index2&utm_medium=Treat-Pack-Banner&utm_campaign=Treat-Pack-Banner"><img src="/new/pix/dog-treat-961.jpg" title="" height="331" width="960" alt=""  border="0" /></a>

 <a href="https://www.dogspot.in/beds/?filter=filter&category_id=62&item_brand=undefined23-&s_price=Rs520-Rs10440&show1=1&record=&utm_source=DogSpot-Index2&utm_medium=DogSpot-Bed-Banner&utm_campaign=DogSpot-Bed-Banner"><img src="/new/pix/dog-bed-960.jpg" title="" height="331" width="960" alt=""  border="0" /></a>
    </div>
  </div></div>
   
   

        <!-- shop tabs -->
       <?php /*?> <div class="vs20"></div>
    	<ul class="shoptabs">
        	<li><span><b>All India Free Shipping</b>Most Reliable Logistics Partners</span></li>
            <li><span><b>Customer Care</b>+91-9212196633, Mon- Fri (10 am - 6 pm)</span></li>
            <li><span><b>Secure Payments</b>Credit/Debit Card & Net Banking</span></li>
            <li style="margin:0;"><span><b>Cash on Delivery</b>Pay at your Door Step</span></li>
            
    
        </ul><?php */?>
        <div class="cb"></div>
     <!-- shop tabs -->
    </div>

   <?php /*?><div class="cont660" style="width:580px">
   <!--Shop Box-->



   	<div id="wrap" class="pannel bannerSlide">
   	<?php require_once(MYSITE_ROOT_URL.'/new/includes/home-banner-display.php'); ?>

   </div>
   <span class="cl"></span>
   <!--Shop Box-->
   <!--puppy box-->
   <div id="wrap" class="pannel dn">
   <?php //require_once(MYSITE_ROOT_URL.'/new/includes/home-puppies-available.php'); ?>
   </div>
   <!--puppy box-->
   <!--Show box-->
   <div id="wrap" class="pannel dn" >
   	<?php //require_once(MYSITE_ROOT_URL.'/new/includes/featured-events.php'); ?>
   </div>
   <!--Show box END-->
   <!--QNA box-->
   <div id="wrap" class="pannel dn">
   	<?php //require_once(MYSITE_ROOT_URL.'/new/includes/home-recent-qna.php'); ?>
   </div>
   <!--QNA box END-->
   </div><?php */?>
   <!-- left cont -->
   
   
   <!-- right cont -->
   <div id="login-box" class="login-popup">
<?php //require_once($DOCUMENT_ROOT.'/new/popbanner.php'); ?>
</div>

   <!--<div class="cont300" style="width:380px"> 
<div class="bannerBox">
<?php /*?><div id="homeSlide">
 
 <a href="https://www.dogspot.in/combo-pack/"><img src="/banners/royalcanin/independence-day-web-sale1.jpg" alt="Pawsome Zone"  title="Pawsome Zone"  ></a>
 <a href="#login-box" target="_blank" class="login-window"><img src="../banners/royalcanin/royal-canin-banner1.jpg" alt="Royal Canin" usemap="#rc"  title="Royal Canin" border="0"  >
    <map name="rc" id="rc">
      <area shape="rect" coords="254,6,380,43" href="#" target="_blank" alt="Royal Canin" />
    </map>
</a>
</div><?php */?>

<?php /*?><a href="#login-box" target="_blank" class="login-window"><img src="../banners/royalcanin/royal-canin-banner1.jpg" alt="Royal Canin" usemap="#rc"  title="Royal Canin" width="380" height="250" border="0"  >
    <map name="rc" id="rc">
      <area shape="rect" coords="254,6,380,43" href="#" target="_blank" alt="Royal Canin" />
    </map>
</a><?php */?>
<?php /*?>  <a href="#login-box" class="login-window">
<img src="/banners/royal_new.jpg" alt="Royal Canin" usemap="#Map"  title="Royal Canin" border="0"  >
  <map name="Map" id="Map">
    <area shape="rect" coords="138,209,228,239" href="https://www.dogspot.in/royal-canin/?utm_source=RC%20Home%20Page%20Banner&amp;utm_medium=RC%20Home%20Page%20Banner&amp;utm_campaign=RC%20Home%20Page%20Banner" target="_blank" />
  </map><?php */?>
 <?php /*?> </a><?php */?>
<?php //require_once(MYSITE_ROOT_URL.'/new/includes/rc-breeds.php'); ?>
<?php //require_once($DOCUMENT_ROOT.'/banner1.php'); addbanner300('250','300','1'); ?>
</div>
</div>--><!-- right cont -->

   
   

   <div class="cb"></div>
    <!--  <div class="vs20"></div>
   tabs -->
   <?php /*?><div>
   
      <div class="fl" id="" style="height: 137px !important;"> <a href="https://www.dogspot.in/beds/?filter=filter&category_id=62&item_brand=undefined23-&s_price=Rs520-Rs10440&show1=1&record=&utm_source=DogSpot%20Homepage&utm_medium=DogSpot%20Bed%20Sale%20Banner&utm_campaign=DogSpot%20Homepage%20Bed%20Sale%20Banner" target="_blank"><img src="/banners/dog-bed.jpg" width="486" height="137"></a>
     </div>

    <div class="fl" id="homeSlide13" style="height: 137px !important;">
 <a href="https://www.dogspot.in/combo-packs/"><img src="/banners/web-banner1234.jpg" width="486" height="137"  style="" border="0"></a>

    </div>
   
 </div><?php */?>
     
  <?php /*?>	<ul class="hometabs">
    	<li style="width:200px;" class="htab active"><span><a href="javascript:void();" onclick="Javascript:showHideBox(0, $(this))"><b>dogs shop</b>Online Pet Shop</a></span></li>
        <li style="width:250px;" class="htab"><span><a href="javascript:void();" onclick="Javascript:showHideBox(1)"><b>puppies available</b>Buy  pups in India</a></span></li>
        <li style="width:250px;" class="htab"><span><a href="javascript:void();" onclick="Javascript:showHideBox(2)"><b>dog shows in india</b>Photos and Results</a></span></li>
        <li style="width:245px; margin-right:0px;" class="htab"><span><a href="javascript:void();" onclick="Javascript:showHideBox(3)"><b>answers to questions</b>Ask anything related to Dogs</a></span></li>
       
   
   	</ul><?php */
	
	?>
    <?php /*?><a href="https://www.dogspot.in/beagleshow"><img src="/banners/Banner-Home.jpg"/></a><?php */?>
    <span class="cl"></span>
  
   <div class="cb"></div>
   <!-- tabs -->
      <!-- first half two column -->
   <div class="vs20"></div>
   <div class="hrline"></div>
   <div class="vs20"></div>
   
   <div class="cont980">
   <div class="homepage-fancystripe"></div>
<div class="wag_rec_dg1">

<div class="wc_sec">
<div class="wag_indexSec">

<div class="dg_popMost">
<? while($sel_wag_dogs1=mysql_fetch_array($sel_wag_dogs)){ 
$dog_name=$sel_wag_dogs1['dog_name'];
$dog_breed=$sel_wag_dogs1['dog_breed'];
$dog_nicename=$sel_wag_dogs1['dog_nicename'];
$dog_image=$sel_wag_dogs1['dog_image'];
?>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/<?=$dog_nicename;?>">
<div class="dg_popImg">
<?
if($dog_image){
		$src_usr_img = $DOCUMENT_ROOT.'/dogs/images/'.$dog_image;
		$imageURL='/dogs/images/150-150-'.$dog_image;
	}else{
		$src_usr_img = $DOCUMENT_ROOT.'/imgthumb/100x100-no-photo.jpg';
		$imageURL='/imgthumb/150-150-no-photo.jpg';
	}
	$dest_usr_img = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src_usr_img,$dest_usr_img,'150','150','ratiowh');
?>
  <img src="<?=$imageURL ?>" alt="">
 </div>
<div class="dg_popTxt">
<h3><?=$dog_name; ?></h3>
<h4><?=$dog_breed; ?></h4>
</div></a>
</div>
<? } ?>
</div>
</div>
<div class="wag_indexText">
<h4>The Ideal Club</h4>
<h3>Wag Club connects over 6000 dogs and shares their story with the world</h3>
<?php /*?><h3>Is your dog part of the growing Family?</h3><?php */?>
<div class="be_click"><a href="https://www.dogspot.in/wag_club">Visit Wag club</a></div> 
</div>
</div></div>
<div class="homepage-fancystripe"></div>
</div>
   <!-- dog gallery -->
<?php /*?>   <div class="bg_body_wc"></div><?php */?>
   <?php /*?><a href="https://www.dogspot.in/wag_club/add_dog.php?utm_source=DogSpot-Hompage&utm_medium=Wag-Club-Add-Dog&utm_campaign=Wag-Club-Add-Dog"> 
   <div class="wc_sec">
<div class="txt_dg_wc">
<div class="text_box_dg_wc">
<h1>Wag Club</h1>
<p> <span class="font174">An</span>
<span class="font50">Exclusive</span>
<span class="font174">club</span> 
<span class="font50">just</span> 
<span class="font174">for</span> 
<span class="font50">your</span>
<span class="font174">pet</span>

</p>
<div class="add_btn_dg">

 <div class="ad_dg_btn" style="margin-top: 40px;"><span style="font-size:33px; margin-left:30px;">+</span><span style="padding: 8px;">Add Your Dog</span></div>
<? //} ?>
</div>  
</div>
</div>

</div>
</a><?php */?>
<?php /*?>   <div>
	 <?php //require_once('includes/home-doggallery-hori.php'); ?>
   </div>
<?php */?><!-- <div class="cont300">
   </div>-->
   <div class="cb"></div>
     <div class="vs20"></div>
   <div class="hrline"></div>
   <div class="vs20"></div>
   
   <!-- dog gallery -->
   <!-----------------------------------------------------------------new products section-------------------------------------------------------------------->
   <div class="contborbttm" >
     <!-- Home Reecent Products  -->
    
       <div style="height:290px">
        	<h3 style="margin-bottom:5px;">recently bought products</h3>
        	<ul class="shopslide jcarousel-skin-tango" id="fc3">
                <?
itemHomerecent('12', $DOCUMENT_ROOT, '186', '160');
?>
            </ul>
        
       </div>
     	    
      <div class="cb"></div>
      <!-- Home Reecent Products ends here  -->
</div>
  <div class="vs10"></div>
 <div class="contborbttm">
     <!-- Home featured product  -->
     
        <div style="height:310px">
        	<h3 style="margin-bottom:5px;">featured products</h3>
        	<ul class="shopslide jcarousel-skin-tango" id="fp1" style="height:280px">
                <?
itemHomeNew('f', '20', $DOCUMENT_ROOT, '184', '160');
?>
            </ul>
        
       </div><!-- featured product-->
       
     <div class="cb"></div>
</div>
  <div class="vs10"></div>
           <!-- shops categories -->
     <div style="padding-bottom:20px;">
     	<!-- featured categories-->
       <div class="contborbttm">
         <h3 style="margin-bottom:5px;"><a style="color:#84A40D" href="/treats-food/">featured dog food & treats</a></h3>
                <ul class="shopslide jcarousel-skin-tango" id="fc1">
   <?
itemHomeNew('treats', '20', $DOCUMENT_ROOT, '184', '160');
?>
    </ul>
       
       </div>
       <div class="vs10 cb"></div>
        
       <!-- featured categories-->

       <div class="contborbttm">
        	<h3 style="margin-bottom:5px;"><a style="color:#84A40D" href="/dog-grooming/">featured grooming</a></h3>
        	<ul class="shopslide jcarousel-skin-tango" id="fc2">

 <?
itemHomeNew('grooming', '20', $DOCUMENT_ROOT, '184', '160');
?>

 </ul>  </div>
        
       <!-- featured categories-->       
               
      <div class="cb"></div>
</div><!-- shops categories -->
  <!-- new arrivals products -->  
 <div class="contborbttm" style="border-bottom:none;" >     
     <!--  new arrivals -->
       <div style="height:290px">
        	<h3 style="margin-bottom:5px;">new arrivals</h3>
        	<ul class="shopslide jcarousel-skin-tango" >
                 <?
itemHomeNewarr('10', $DOCUMENT_ROOT, '184', '160');
?>
                  <? //itemHomeNewarr('n', '10', $DOCUMENT_ROOT, '184', '160') 
?>
            </ul> </div><!-- new arrivals ends here -->
             <div class="cb"></div>
  </div>
  <div class="vs20"></div>
   <!-----------------------------------------------------------------new products section end-------------------------------------------------------------------->

   <!-- three columns -->
   	<!--column 1 -->
   	<div class="cont660">
  <?php require_once('includes/home-recent-articles.php'); ?>
    </div><!--column 1 -->
      
      
      <!--column 3 -->
   	<div class="col280" style="padding:0; margin:0; border:0; width:300px;">
      <?php require_once('includes/home-recent-business.php'); ?>
    </div><!--column 3 -->
   <!-- three columns -->
   
    </div>
<?php require_once('new/common/bottom.php'); ?>