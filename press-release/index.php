<?php
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
 if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/press-release/index-bootstrap.php');
  exit;
  }
$session_id = session_id();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="canonical" href="https://www.dogspot.in/press-release/" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Press Release | DogSpot</title>
<meta name="keywords" content="Press Releases, DogSpot press release" />
<meta name="description" content="See the press releases about DogSpot posted on different business and pet related sites." />
<link rel="stylesheet" href="css/pr_style.css" type="text/css" />

<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>



<style>
.login-popup {
display: none;
background: #FFF;
padding: 10px;
border: 2px solid #ddd;
float: left;
font-size: 1.2em;
position: absolute;
top: 0px; margin-top:100px !important;
}
.press_relese_blk{ width:100%; float:left; font-family: "Segoe UI";}
.midia_coverage_text{color: #222222;font-family: "Segoe UI";font-weight: 600; text-align: center; margin:20px 0px; font-size:24px;}
.press_columns{ float:left; width:300px; text-align:center; padding:10px 12px;}
.press_columns a{ text-decoration:none; color:#333;}
.press_columns a:hover{ color:#333; text-decoration:none;}
.press_healine{ margin:15px 0px; font-size:15px;}
.press_date{ font-size:13px; color:#555;}
.press_row{ float:left; width:100%;}
</style>
<div class="cont980">
   <div class="press_relese_blk">
   <h1 class="midia_coverage_text">Press & Media Coverage</h1>
   
  <div class="press_row">
  <!--newsblk start-->
   <div class="press_columns">
   
   <a href="http://articles.economictimes.indiatimes.com/2015-09-04/news/66213328_1_indian-startups-fun-activities-boom-time" target="_blank" rel="nofollow">
   <div><img src="images/economic-times.jpg" /></div>
   <div class="press_healine">How startup companies are offering fun activities to engage and retain employees</div>
   <div class="press_date">September 4, 2015</div>
    </a>
      
   </div>
<!--newsblk end-->

  <!--newsblk start-->
   <div class="press_columns">
   
   <a href="http://dsim.in/blog/an-interview-with-ceo-of-dogspot-in-mr-rana-atheya/" target="_blank" rel="nofollow">
   <div><img src="images/dsim.jpg" /></div>
   <div class="press_healine">An Interview with CEO of DogSpot, Mr. Rana Atheya</div>
   <div class="press_date">August 19, 2015</div>
    </a>
      
   </div>
<!--newsblk end-->

<!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://articles.economictimes.indiatimes.com/2015-07-08/news/64212335_1_uber-puppies-cab" target="_blank" rel="nofollow">
   <div><img src="images/economic-times.jpg" /></div>
   <div class="press_healine">Uber wants to ease your mid-week blues, by delivering Puppies</div>
   <div class="press_date">July 8, 2015</div>
    </a>
      
   </div>
<!--newsblk end-->

</div>
<!-- Ist row end-->
    <div class="press_row">
  <!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://www.livemint.com/Consumer/lDz5HIFJKcEZLUyEjyKPzK/Uber-turns-on-charm-offensive-with-uberPUPPIES-in-Delhi.html" target="_blank" rel="nofollow">
   <div><img src="images/live-mint.jpg" /></div>
   <div class="press_healine">Uber turns on charm offensive with #uberPUPPIES in Delhi</div>
   <div class="press_date">July 8, 2015</div>
    </a>
      
   </div>
<!--newsblk end-->

<!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://www.letsintern.com/blog/rana-atheya-dospot-interview/" target="_blank" rel="nofollow">
   <div><img src="images/lets-intern.jpg" /></div>
   <div class="press_healine">“Everything started as nothing, so start something!” – Rana Atheya, CEO, Dogspot.in</div>
   <div class="press_date">December 24, 2014 </div>
    </a>
      
   </div>
<!--newsblk end-->

<!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://youngturks.in.com/video.php?id=kx4RAxEJTwM&subcat=YT@11" target="_blank" rel="nofollow">
   <div><img src="images/tech-circle-logo.jpg" /></div>
   <div class="press_healine">Identifiable private labels & engagement with pet owners our advantage over horizontal e-tailers: DogSpot.in co-founder Rana Atheya</div>
   <div class="press_date">September 10, 2014</div>
    </a>
      
   </div>
<!--newsblk end-->

 </div>
  
  <!-- IInd row end-->
   <div class="press_row">
  <!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="https://www.medianama.com/2014/08/223-dogspot-in-launches-self-branded-products/" target="_blank" rel="nofollow">
   <div><img src="images/medianama.jpg" /></div>
   <div class="press_healine">Dogspot.in launches self-branded products</div>
   <div class="press_date">August 19, 2014</div>
    </a>
      
   </div>
<!--newsblk end-->
<!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://articles.economictimes.indiatimes.com/2014-07-07/news/51133853_1_pet-lovers-animal-care-rana-atheya" target="_blank" rel="nofollow">
   <div><img src="images/economic-times.jpg" /></div>
   <div class="press_healine">Dogspot.in: Rana Atheya's Rs 4-crore co which started as a website for pet lovers</div>
   <div class="press_date">July 7, 2014</div>
    </a>
      
   </div>
<!--newsblk end-->

  <!--newsblk start-->
   <div class="press_columns">
   
   <a href="https://inc42.com/startups/dogspot-social-ecommerce-platform-pet-lovers/" target="_blank" rel="nofollow">
   <div><img src="images/inc42.jpg" /></div>
   <div class="press_healine">DogSpot.In: A Social Ecommerce Platform For Pet Lovers</div>
   <div class="press_date">May 1, 2014</div>
    </a>
      
   </div>
<!--newsblk end-->

</div>
  <!-- 3rd row end-->
  <div class="press_row">
  <!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://techcircle.vccircle.com/2013/12/13/dogspot-in-betting-big-on-social-e-commerce-looking-to-raise-5m-in-funding/" target="_blank" rel="nofollow">
   <div><img src="images/tech-circle-logo.jpg" /></div>
   <div class="press_healine">DogSpot.in betting big on social e-commerce, looking to raise $5M in funding</div>
   <div class="press_date">December 13, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->
<!--newsblk start-->
   <div class="press_columns">
   
   <a href="http://www.nextbigwhat.com/times-internet-new-appointments-fishspot-for-fish-lovers-297/" target="_blank" rel="nofollow">
   <div><img src="images/next-bigwhat1.jpg" /></div>
   <div class="press_healine">Online Roundup: Aaramshop:Pakistan, Times Internet: New Appointments; FishSpot launches</div>
   <div class="press_date"> August 6, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->
 <!--newsblk start-->
   <div class="press_columns">
   
   <a href="https://yourstory.com/2013/06/inside-the-warehouse-of-an-ecommerce-store-see-how-it-works/" target="_blank" rel="nofollow">
   <div><img src="images/your-story.jpg" /></div>
   <div class="press_healine">Inside the warehouse of an eCommerce store: See how it works for DogSpot</div>
   <div class="press_date">June 19, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->

</div>
  <!-- 4th row end-->
  
    <div class="press_row">
    <!--newsblk start-->
  
   <div class="press_columns">
   
   <a href="http://www.businesswireindia.com/news/news-details/dogspot-indias-leading-pet-ecommerce-site-gears-up-summers/35069" target="_blank" rel="nofollow">
   <div><img src="images/businesswire-logo.jpg" /></div>
   <div class="press_healine">DogSpot: India's Leading Pet Ecommerce Site Gears Up for Summers</div>
   <div class="press_date"> April 22, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->
<!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://youngturks.in.com/video.php?id=kx4RAxEJTwM&subcat=YT@11" target="_blank" rel="nofollow">
   <div><img src="images/young-truks.jpg" /></div>
   <div class="press_healine">Young Turks YT@11 Episode 28 Part 3 - DogSpot.in</div>
   <div class="press_date">February 25, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->
 <!--newsblk start-->
   <div class="press_columns">
   
   <a href="http://techcircle.vccircle.com/2013/02/21/dogspot-in-raises-fund-from-india-quotient-to-launch-2-new-sites/" target="_blank" rel="nofollow">
   <div><img src="images/tech-circle-logo.jpg" /></div>
   <div class="press_healine">DogSpot.in raises funding from India Quotient; to launch 2 new sites</div>
   <div class="press_date">February 21, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->


    </div>
  <!-- 5th row end-->
  
  <div class="press_row">
  <!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="http://www.iamwire.com/2013/02/pet-shopping-site-dogspot-raises-funding-from-india-quotient/6155" target="_blank" rel="nofollow">
   <div><img src="images/iamwire-logo.jpg" /></div>
   <div class="press_healine">Pet Shopping site DogSpot raises funding from India Quotient</div>
   <div class="press_date">February 20, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->

 <!--newsblk start-->
   <div class="press_columns">
   
   <a href="https://yourstory.com/2012/06/dogspot-in-is-the-one-stop-shop/" target="_blank" rel="nofollow">
   <div><img src="images/your-story.jpg" /></div>
   <div class="press_healine">Angel Fund India Quotient Invests in the online store for canines- Dogspot.in</div>
   <div class="press_date">February 20, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->
 <!--newsblk start-->
   <div class="press_columns">
   
   <a href="http://www.nextbigwhat.com/dogspot-funding-297/" target="_blank" rel="nofollow">
   <div><img src="images/next-bigwhat1.jpg" /></div>
   <div class="press_healine">Pet Shopping site DogSpot receives funding from India Quotient</div>
   <div class="press_date">February 20, 2013</div>
    </a>
      
   </div>
<!--newsblk end-->

</div>
<!-- 6th row end-->


<div class="press_row">
  
  
<!--newsblk start-->
   <div class="press_columns">
   
   <a href="https://yourstory.com/2012/06/dogspot-in-is-the-one-stop-shop/" target="_blank" rel="nofollow">
   <div><img src="images/your-story.jpg" /></div>
   <div class="press_healine">One Stop Shop For All Your Canine Needs: DogSpot.in</div>
   <div class="press_date">June 4, 2012</div>
    </a>
      
   </div>
<!--newsblk end-->
  <!--newsblk start-->
 
   <div class="press_columns">
   
   <a href="https://yourstory.com/2010/09/rana-atheya-shailesh-visen-gaurav-malik-founders-dogspot-in-the-leading-dog-portal-in-india/" target="_blank" rel="nofollow">
   <div><img src="images/your-story.jpg" /></div>
   <div class="press_healine">Rana Atheya , Shailesh Visen, Gaurav Malik, Founders, DogSpot.in- the Leading Dog Portal in India</div>
   <div class="press_date">September 10, 2010</div>
    </a>
      
   </div>
<!--newsblk end-->
</div>
<!-- 7th row end-->
</div></div>

<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>