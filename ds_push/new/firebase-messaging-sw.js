// [START initialize_firebase_in_sw]
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/3.7.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.7.6/firebase-messaging.js');
// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
 var config = {
    apiKey: "AIzaSyDvm0rmlzaxN1KF8_iK5m_koQjPbQDNiJQ",
    authDomain: "dogspotcromepush.firebaseapp.com",
    databaseURL: "https://dogspotcromepush.firebaseio.com",
    projectId: "dogspotcromepush",
    storageBucket: "dogspotcromepush.appspot.com",
    messagingSenderId: "226919031911"
  };
  firebase.initializeApp(config);// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
// [END initialize_firebase_in_sw]

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
console.log(payload.data.notification);
        var payloadcont = JSON.parse(payload.data.notification);
        // Customize notification here
        const notificationTitle = payloadcont.title;
        const notificationOptions = {
//            title: payloadcont.title,
            body: payloadcont.body,
            icon: payloadcont.icon,
            click_action: payloadcont.click_action
        };
        return self.registration.showNotification(notificationTitle,
                notificationOptions);
        });
// [END background_handler]
