<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot News</title>
<meta name="keywords" content="Payment Details | DogSpot" />
<meta name="description" content="Payment Details | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script>
$(function() {
    var $sidebar   = $("#leftnav"),
        $window    = $(window),
        offset     = $sidebar.offset(),
        topPadding = 15;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding
            });
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            });
        }
    });

});
</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>
<div class="cont980">
<div class="vs10"></div>
<div style="float:left; width:170px;"  id="leftnav">


<h1>DogSpot in News</h1>
<div>
<?  $qItems=query_execute("SELECT * FROM news_publisher");
	  while($rowItems = mysql_fetch_array($qItems)){
		$pub_id = $rowItems["publisher_id"];
		$pub_name = $rowItems["publisher_name"];
		$pubnicename=$rowItems["pub_nice_name"];  
	   ?>         
<a  class="userbutton" href="index.php#<?=$pub_id?>"><?=$pub_name  ?></a>

<? }  ?>

</div>
</div>

<div style="float:right; margin-left:5px; margin-top:5px; width:780px;">
 
  <? $qcontitle=query_execute("SELECT * FROM news_publisher order by u_date desc "); 
   
  while($qpublish=mysql_fetch_array($qcontitle)) { 
  $publisher_name=$qpublish['publisher_name'];
  $p_id=$qpublish['publisher_id'];    
   $qcont=query_execute("SELECT * FROM news_content where publisher_id='$p_id' order by content_id desc");?>
  <div id="<?= $p_id;?>" name="<?= $p_id;?>">
  <a id="<?= $p_id;?>" name="<?= $p_id;?>"></a>
  <? 
  $src = $DOCUMENT_ROOT.'/news/uploads/'.$qpublish['publisher_logo'];
  $destm = $DOCUMENT_ROOT.'/imgthumb/160x50-'.$qpublish['publisher_logo'];
	
	createImgThumbIfnot($src,$destm,'160','50','ratiowh'); ?>
	
  
  <div>
  <!--<div class="fl"><p class="caption1" align="left"><h3>DogSpot Featured in <? //$publisher_name;?></h3></p></div>-->
  <p class="caption1" align="center"> <img src="<?='/imgthumb/160x50-'.$qpublish['publisher_logo'];?>"  />
    </p></div>
     
<? $s=0; while($qcontent=mysql_fetch_array($qcont)) { $s=$s+1;?>


<div>
 <p><h1> <a href=<?=$qcontent['content_url'];?> target="_blank" title=<?=$qcontent['content_url'];?> ><?=$qcontent['content_title']; ?></a></h1>
    <strong>published by: </strong><?=$qcontent['published_by'];?> |  <strong>Published on: </strong> <? print(showdate($qcontent['publish_date'],"d M o"));?>
    <div class="vs2"></div>
    <h2><a href=<?=$qcontent['content_url'];?> target="_blank"><?=$qcontent['content_url'];?></a></h2></p>
<div class="vs5"></div>
  <? 
  $src = $DOCUMENT_ROOT.'/news/uploads/'.$qcontent['content'];
  $destm = $DOCUMENT_ROOT.'/imgthumb/1500x600-'.$qcontent['content'];
	
	createImgThumbIfnot($src,$destm,'1500','600','ratiowh'); ?>
	
	<a href=<?=$qcontent['content_url'];?> target="_blank" title="read more"><img src="<?='/imgthumb/1500x600-'.$qcontent['content'];?>"  /></a>
  </div>
		
   <div class="vs10"></div>
      
 <? } ?></div><div class="hrline"></div>
 <?  }?> 
</div></div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
