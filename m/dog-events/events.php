<?php
require_once("constants.php");
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');

$sitesection = "dog-events";
$date_session_array=''; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dog Show, Dog Events, Dog Championship, KCI | Dog Spot</title>
<meta name="keywords" content="Dog Show, Dog Events, Dog Championship, KCI, Dog Spot" />
<meta name="description" content="Dog Show, Dog Events, Dog Championship, KCI, Dog Spot" />
<link type="text/css" rel="stylesheet" href="/dog-events/css/dog-events.css">
<link rel="canonical" href="https://www.dogspot.in/dog-events/" />
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-events/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<style>
#dogshow_nav_header.fixed {
	position: fixed;
	top: 0px;
	float: left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>


</head>

<div class="dog-show-sec">
<div class="dog-show-banner"> <img src="/dog-events/images/show-schedules.jpg" alt="Show Secheduls"  height="" width=""> </div>
<div id="showsechedule_text">
  <h1>Shows Schedules</h1>
</div>
<div class="dogshow_nav_header" id="dogshow_nav_header">
  <div id="wrapper" class="clearfix">
    <div id="ds_top_nav">
      <ul id="nav">
        <li><a href="/dog-events/about-us/" class="butt_2" data-ajax="false">About us</a> </li>
        <li><a href="/dog-events/show-schedules/" id="" class="butt_3" data-ajax="false">Show schedules</a></li>
        <li><a href="/show-results/" id="" class="butt_4" data-ajax="false">Show results</a></li>
      </ul>
    </div>
  </div>
</div>

<!-- dog show schedules -->

<div id="wrapper" class="clearfix">
<div id="content">
  <form id="formOrderSearch" name="formOrderSearch" method="post">
    <div id="dogshow_filter_div">
      <div id="shows_frm">
        <select name="typeid" id="typeid" class="dogshow_filter_drop">
          <option value="">Display by...</option>
          <? $seshow=query_execute("SELECT a.type_name, a.type_id FROM show_type as a, events as b WHERE b.show_type=a.type_id AND b.end_date>NOW() AND b.publish_status='publish' GROUP BY a.type_name ORDER BY a.type_name ASC");
 while($getre=mysql_fetch_array($seshow)){
	 $type_id=$getre['type_id'];
	 $type_name=$getre['type_name'];
	 ?>
          <option  value="<?=$type_id?>"<? if($typeid=='$type_id'){ echo "selected='selected'";} ?>>
          <?=$type_name?>
          </option>
          <? }?>
        </select>
        <input name="searchOrder" id="searchOrder" class="dogshow_gobtn" src="" alt="Go" style="" value="go" type="submit" />
      </div>
    </div>
  </form>
  <div style="text-align:center;display:inline-block;">
<div id='div-gpt-ad-1552408890928-0' style='height:250px; width:300px;margin: 15px 24px 29px 12px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552408890928-0'); });
</script>
</div>
 </div>
  <div class="dogshow_sechduletable">
    <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
      <tbody>
        <tr>
          <th>Show</th>
          <th> Location</th>
          <th> Closing date</th>
          <th> Days left</th>
        </tr>
        <? 
if($typeid!=''){
$showype = " AND show_type='$typeid'";
	}
		$showdes=query_execute("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' $showype ORDER BY start_date ASC");

$s=1;
while($getshowdata=mysql_fetch_array($showdes)){
$show_name=$getshowdata['event_name'];
$location=$getshowdata['venue'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['start_date'];
$edate=$getshowdata['end_date'];
$show_id=$getshowdata['link_id'];
$event_id=$getshowdata['event_id'];
$organized_by=$getshowdata['organized_by'];
$event_nice_name=$getshowdata['event_nice_name'];
$selke=query_execute_row("SELECT kennel_name, city FROM kennel_club WHERE kennel_id='$organized_by'");

$toDis=date("Y-m-d H:i:s");
$startTimeStamp = strtotime($edate, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);
$numberDays = $timeDiff/86400;  // 86400 seconds in one day
// and you might want to convert to integer
$numberDays = intval($numberDays);
$sqldentry=query_execute_row("SELECT entry FROM show_description WHERE show_id='$show_id'");
$sqlcdate=$sqldentry['entry'];
?>
        <tr>
          <td  class=<?php if($s % 2 == 0){?>"icon" <? }else{ ?>"dark icon" <? }?> valign="top" ><img src="/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14" /></td>
            <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341">
          <a id="" class="show_title_link" href="/dog-events/<?=$event_nice_name;?>/">
          <?=$selke['kennel_name']."</br>".$show_name;?>
          </a>
            </td>
            <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130">
          <span class="state_name">
          <?=$selke['city'];?>
          </span>
            </td>
            <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="129">
          <? print(showdate($edate, "d M Y"));?>
            </td>
            <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
            <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>>
            </span>
          <?=$numberDays?>
            </td>
            <td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
          <? if($show_id){?>
          <? if($sqlcdate!='close'){?>
          <a id="" class="showschedule_enter_online" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
          <? }?>
          <? }?>
            <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/dog-events/<?=$event_nice_name?>/">
Show details
            </a>
            </td>
        </tr>
        <tr>
            <td id="" <? if($s % 2 == 0){?>  <? }else{ ?>  <? }?> valign="top" width="62">
          <img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15">
            </td>
            <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top">
          <? print(showdate($date, "d M Y"));?>
          <? if($date!=$edate){ ?>
          - <? print(showdate($edate, "d M Y")); }?>
            </td>
        </tr>
        <? 
			$s=$s+1;
			}?>
      </tbody>
    </table>
  </div>
</div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom-new.php');
require_once($DOCUMENT_ROOT.'/common/bottom.php');exit; ?>
