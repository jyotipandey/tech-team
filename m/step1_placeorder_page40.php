<!DOCTYPE html>

<html>
<head>
	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
	<?php require_once('/common/script.php'); ?>
	
	<div id="step1_placeorder_page40" class="p-one-em">
		<script type="text/javascript">
			$("#step1_placeorder_page40").on("pageinit", function() {
				$("#step2_page").click(function() {
					$.mobile.changePage("#step2_placeorder_page41");
				});	
			});
			$("#step1_placeorder_page40").on("pageshow", function() {

			});
		</script>
         <?php require_once('/common/top.php'); ?>
		
		<div data-role="content">
			<div class="myDogsBlock_wrp">
				<div class="myDogText">Step 1: Login<span class="triangle_down"></span></div>
				<div class="myDogText_content">
				<div class="paymentradio_blk">
					<div class="paymentradio_ttl">Login with your Dogspot account</div>
					<div class="paymentradio_content">
						<div class="input-blk">
							<div><input type="text" id="Email_signin" data-role="none" placeholder="Enter Your Email Address"></div>
							<div><input type="text" id="Password_signin" data-role="none" placeholder="Password"></div>
						</div>
						<a class="link_forgot">Forgot Password ?</a>
						<button id="step2_page" class="btnstyle2  ui-btn ui-shadow ui-corner-all">Sign In</button>
					</div>
				</div>
			</div>
			<div class="tab_blk">
					<div class="or_txt">Or</div>
					<button class="btnstyle1 facebookconnect" >
						<span class="facebook_icon"></span>Log in with Facebook
					</button>
					<button class="btnstyle1 red googleconnect" >
						<span class="google_icon"></span>Sign in with Google 
					</button>
				</div>
		</div>	
    </div>

	<?php require_once('/common/bottom.php'); ?>
	

 