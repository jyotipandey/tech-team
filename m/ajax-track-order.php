<?php
require_once('/home/dogspot/public_html/functions.php'); 
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
//require_once(SITEMAIN_URL.'/functions.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/shop/arrays/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
?>
<?php
$user_id = $userid;

if($order_id){
	//Tracking status
	$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id' AND userid='$user_id' AND order_type='complete'");
	if($SQ_shop_order['order_id']!=""){

	// Order is in New
		if($id=="approval"){
			$order_date = $SQ_shop_order['order_c_date'];
			$date = showdate($order_date, "d M o");
			$awb_no = $SQ_shop_order['order_tracking'];
			$courier_id = $SQ_shop_order['shop_courier_id'];
			$time = date('h:i A', strtotime($order_date));
			$data['msg'] = "Your order has been placed";
			$data['time_sqlcomplete'] =   $date. ' | ' .$time;
			$data['status']=true;
			$data['arrow']='
					<li><label class="track_arrow_active"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
				';
			echo json_encode($data);
		}
	//End
		
	// Order is in Dispatched-Ready
		if($id=="processing"){
			$sqld1 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched-ready%' AND review_section_id='$order_id'");
			$sqlsdat1 = $sqld1['c_date'];
			$datetimearray2 = explode(" ", $sqlsdat1);
			$date2 = $datetimearray2[0];
			$time2 = $datetimearray2[1];
			$reformatted_date2 = date('d-m-Y',strtotime($date2));
			$reformatted_time2 = date('Gi.s',strtotime($time2));
			if($sqlsdat1 == ''){
				$data['msg']="Order yet to be packed";
				$data['status']=false;
			}else{
				$data['status']=true;
				$data['msg']="Your item has been packed";
				$data['time_sqlcomplete'] = $date2. ' | ' .$time2;
			}
			$data['arrow']='
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow_active"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
				';
			echo json_encode($data);
		}	
	//End
	
	//Order is in Dispatched
		if($id=="dispatched"){
			$sqld2 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched%' AND review_section_id='$order_id'");
			$sqlsdat3 = $sqld2['c_date'];
			$datetimearray3 = explode(" ", $sqlsdat3);
			$date3 = $datetimearray3[0];
			$time3 = $datetimearray3[1];
			$reformatted_date3 = date('d-m-Y',strtotime($date3));
			$reformatted_time3 = date('Gi.s',strtotime($time3));
			if($sqlsdat3 == ''){
				$data['msg']="Order yet to be dispatched";
				$data['status']=false;
			}else{
				$data['msg'] = "Your order has been dispatched from warehouse";
				$data['time_sqlcomplete'] =   $date3. ' | ' .$time3;
				$data['status'] = true;
			}
			$data['arrow']='
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow_active"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
				';
			echo json_encode($data);
		}
	//End
	
	// Courier status
		if($id=="inTransit"){
			$courrier_sql = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%rto-transit%' AND review_section_id='$order_id'");
			if($courrier_sql['c_date']==""){
				$courrier_sql = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%delivered%' AND review_section_id='$order_id'");
				$sqlsdat_rto = $courrier_sql['c_date'];
				$date4 = showdate($sqlsdat_rto, "d M o");
				$time4 = date('h:i A', strtotime($sqlsdat_rto));
				if($sqlsdat_rto == ''){
					$data['msg']="Order yet to be in transit";
					$data['status']=false;
				}else{
					$data['msg'] = "Your order has been delivered";
					$data['time_sqlcomplete'] =   $date4. ' | ' .$time4;
					$data['status'] = true;
				}
			}else{
				$sqlsdat_rto = $courrier_sql['c_date'];
				$date4 = showdate($sqlsdat_rto, "d M o");
				$time4 = date('h:i A', strtotime($sqlsdat_rto));
				if($sqlsdat_rto == ''){
					$data['msg']="Order yet to be in transit";
					$data['status']=false;
				}else{
					$data['msg'] = "Your order has been return to origin";
					$data['time_sqlcomplete'] =   $date4. ' | ' .$time4;
					$data['status'] = true;
				}
			}
			$data['arrow']='
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow_active"></label></li>
					<li><label class="track_arrow"></label></li>
				';
			echo json_encode($data);
		}
	//End
	
	// Order is in Delivered
		if($id=="delivered"){
			$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%Delivered%' AND review_section_id='$order_id'");
			if($sqld4['c_date']== ''){
				$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%delivered%' AND review_section_id='$order_id'");
				$sqlsdat4 = $sqld4['c_date'];	
			}
			$sqlsdat4 = $sqld4['c_date'];
			$date7 = showdate($sqlsdat4, "d M o");
			$time7 = date('h:i A', strtotime($sqlsdat4));
			if($sqlsdat4 == ''){
				$data['msg']="Order yet to be delivered";
				$data['status']=false;
			}else{
				$data['status']=true;
				$data['msg']="Order has been delivered";
				$data['time_sqlcomplete'] = $date7. ' | ' .$time7;
			}
			$data['arrow']='
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow_active"></label></li>
				';
			echo json_encode($data);
		}
	//End
	
	// Order is in Delivered
		if($id=="cancelled"){
			$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%cancelled%' AND review_section_id='$order_id'");
			if($sqld4['c_date']== ''){
				$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%canceled%' AND review_section_id='$order_id'");
				$sqlsdat4 = $sqld4['c_date'];	
			}
			$sqlsdat4 = $sqld4['c_date'];
			$date7 = showdate($sqlsdat4, "d M o");
			$time7 = date('h:i A', strtotime($sqlsdat4));
			if($sqlsdat4 == ''){
				$sqlcomplete4 = "";
				$data['msg']="Order yet to be ";
				$data['status']=false;
			}else{
				$data['status']=true;
				$data['msg']="Order has been cancelled";
				$data['time_sqlcomplete'] = $date7. ' | ' .$time7;
			}
			$data['arrow']='
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow_active"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
					<li><label class="track_arrow"></label></li>
				';
			echo json_encode($data);
		}
	//End
	}
}
?>