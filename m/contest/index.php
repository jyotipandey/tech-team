<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";
$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");?>

<!DOCTYPE html>
<html>
<head>
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<title>Dog Breeds | Complete Information On Dog Breeds | Wagpedia | Dog Breed Types</title>
<link type="text/css" rel="stylesheet" href="css/contest.css?h=16">
<style>
#defaultpanel4{margin-top: -263px;}
</style>
<script>
        //Kill transitions on android
        $(document).bind("mobileinit", function() {
             //alert();
             //$.mobile.ignoreContentEnabled = true;
             $.mobile.defaultPageTransition = "slide";
             
         }); 
        
      </script>
	<script>
        var prevSelection = "tab1";
        $("#navbar ul li").live("click",function(){
            var newSelection = $(this).children("a").attr("data-tab-class");
            $("."+prevSelection).addClass("ui-screen-hidden");
            $("."+newSelection).removeClass("ui-screen-hidden");
            prevSelection = newSelection;
        });
		$( function() {
			$( "#purchase-product" ).enhanceWithin().popup();
			$( "#purchaseproductdetail" ).enhanceWithin().popup();
			$( "#purchaseproductdetail1" ).enhanceWithin().popup();
			$( "#selectsize_popup" ).enhanceWithin().popup();
			$( "#morepopup" ).enhanceWithin().popup();
			
			$( "#buynowpopup" ).enhanceWithin().popup();
			$( "#subscription_offer" ).enhanceWithin().popup();
			//$( "#defaultpanel" ).enhanceWithin().panel();
			
			
			 
	});
		
		
    </script>
<script>
$(document).ready(function() {
	$('a.login-window1').click(function() {
		var loginBox = $(this).attr('href');
		$(loginBox).fadeIn(300);
		var popMargTop = ($(loginBox).height() + 24) / 2;
		var popMargLeft = ($(loginBox).width() + 20) / 2;
		$(loginBox).css({
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		return false;
	});
	$('a.close, #mask').live('click', function() {
		$('#mask , .login-popup').fadeOut(300 , function() {
			$('#mask').remove();
		});
		return false;
	});
});
</script>


<script type="text/javascript">
function wagfuncount1(activity_id,dog_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/contest/wag_insert.php?activity_wag=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
}

function wagcount1(activity_id,dog_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/contest/wag_insert.php?activity_wag_delete=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
	
}
</script>
<script language="javascript">
function validateComment1(activity_id,maxreview){
	$('#loadingdiv'+activity_id).show();                         
    var review_body1= $('#review_body'+activity_id).val();
	var review_body=$.trim(review_body1);
	$('#review_body'+activity_id).val('');
	if(review_body!='' && typeof review_body!='Undefined'){
		ShaAjaxJquary("/contest/checkcomment.php?review_body="+review_body+"&activity_id="+activity_id+"", "#aftercmntq"+activity_id, '', '', 'POST', '#loading', '<img src="/images/indicator.gif" />','APE');
	}
    document.getElementById('review_body').value() = ""
}
</script>
<script> 
function confirmDelete2(review,url,activity_id){
	var user=$("#user_id").val();
	var viewmore=$("#viewmore"+activity_id).text();
	if(viewmore!=''){
		var view=1;
	}else{
		var view=2;
	}
	ShaAjaxJquary("/contest/review_delete.php?ram=1&review_id="+review+"&reUrl="+url+"&userid="+user+"&view="+view+"", "#aftercmntq"+activity_id, '',  '', 'post', '#loading', '<img src="/images/indicator.gif" />','APE');	
}
<?php /*?>
function submitdata(){
	var dog_name=$("#dog_name").val();
	var breed=$("#breed").val();
	var inserted_id=$("#inserted_id").val();
	var image=$("#image").val();
	var user_id=$("#user_id").val();
	if(dog_name!='' ){
		$("#error").css("display","none");
		$("#crop_img21").css("display","none");
		ShaAjaxJquary("/new/Contest/add-activity.php?acttag=1&dog_name="+dog_name+"&inserted_id="+inserted_id+"&breed="+breed+"&image="+image+"", '#show', '',  '', 'post', '', '','REP');
		//window.location.href="https://www.dogspot.in/dogs/"+dog_nicename;
		sleep(3000,inserted_id);
	}else{
		$("#error").css("display","block");
	}
	
}<?php */?>
</script>

<!--contest page start-->
<div>
<script type="text/javascript">
	$("#add_pets-page18").on("pageinit", function() {
		
		$("#adddogpic_camera").click(function(){
			navigator.camera.getPicture(onSuccessAddPicURI, onFailAddPic, { 
				 quality: 0,
				 destinationType: Camera.DestinationType.FILE_URI,
				 encodingType: 0  	// 0:JPG 1:PNG
			 });
		});
		
		$("#adddogpic_gallery").click(function(){
			navigator.camera.getPicture(onSuccessAddPicURI, onFailAddPic, { quality: 0,
				destinationType: Camera.DestinationType.FILE_URI,
				sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
				encodingType: 0 	// 0:JPG 1:PNG
			});
		});
	
		function onSuccessAddPicData(imageData) {
			add_new_dog_image=imageData;
			var image = document.getElementById('mydogpic');
			image.src = "data:image/jpeg;base64," + imageData;
		}
							 
		function onSuccessAddPicURI(FILE_URI) {
			mydogdata.photo = FILE_URI;
			var image = document.getElementById('mydogpic');
			image.src = FILE_URI;//"data:image/jpeg;base64," + imageData;
			//alert(add_new_dog_image);
		 }
		
		function onFailAddPic(message) {
			alert('Failed because: ' + message);
		}
			
		$("#backtopage_8").click(function() {
			$.mobile.changePage("#mydogs-page8",{reverse:true});
		});	
		
		$("#next-page19").click(function() {
			$.mobile.changePage("#add_pets-page20");
		});	
	});
	$("#add_pets-page18").on("pageshow", function() {
	
	});
</script>
  <div class="dog_contest">
    <div class="text_box_dg">
      <h1 style="margin-top:25px;">DogSpot Contest Page</h1>
      <p> <span class="font174">Pooch </span> <span class="font50">of</span> <span class="font174">the </span> <span class="font174">Week!</span> </p>
      <div class="add_btn_dg">
      <?php if($userid=="Guest" || $userid=="guest"){ ?>
		<a data-ajax="false" href="/login.php?refUrl=/contest/" class="bharat close_popup login-window1" id="modal_trigger2"><div class="ad_dg_btn"><span style="margin-right:5px;">+</span><span >Upload Photo</span></div></a>
    <? }else{?>
    	<a href="upload-file.php" data-ajax="false" class="close_popup" id="modal_trigger2"><div class="ad_dg_btn" ><span style="margin-right:5px;">+</span><span >Upload Photo</span></div></a>
	<? } ?> </div>
    </div>
    
    <!-- post start-->
    <div>
      <div data-role="tabs" id="tabs" class="contest_tabs">
        <div data-role="navbar">
          <ul>
            <li><a href="#one" data-ajax="false" style="color: #333 !important;">Feed</a></li>
            <li><a href="#two" data-ajax="false" style="color: #333 !important;">Winner</a></li>
          </ul>
        </div>
        
        <div id="one" class="ui-body-d ui-content">
        <div class="contest_list">
        <? //echo "SELECT * FROM dogs_activity WHERE dog_id='$sqldogy' ORDER BY dog_id DESC";
		$selectTesttotel = query_execute_row("SELECT count(*) as countrecord FROM dogs_activity WHERE publish_status ='publish' AND user_activity='contest' ORDER BY dog_id DESC");
		$sqldata=query_execute("SELECT * FROM dogs_activity as dc,dogs_available as da WHERE dc.publish_status='publish' AND user_activity='contest' AND da.dog_id=dc.dog_id ORDER BY dc.dog_id DESC");
		$iid=0;
		while($dogimg=mysql_fetch_array($sqldata)){
			$iid=$iid+1;
			$sqlimage2=preg_replace('/\\\\+/','',$dogimg['activity1']);
			$sqlimage1=$dogimg['image'];
			$dog_id=$dogimg['dog_id'];
			$activity_id=$dogimg['id'];
			if($sqlimage1){
				$src = $imageBasePath.'/dogs/images/'.$sqlimage1;
				$src1 = $imageBasePath.'/dogs/images/'.$sqlimage1;
				$imageURL1='/dogs/images/600x600-'.$sqlimage1;
				$imageURLface='/dogs/images/350x210-'.$sqlimage1;
			}else{
				$src = $imageBasePath.'/dogs/images/no-photo-t.jpg';
				$imageURL1='/dogs/images/no-photo-t.jpg';
			}
			//echo $src;
			$dest =$imageBasePath.$imageURL1;
			$dest1 = $imageBasePath.$imageURLface;
			createImgThumbIfnot($src,$dest,'600','600','ratiowh'); 
			createImgThumbIfnot($src1,$dest1,'350','210','ratiowh'); 
			//echo "<div class='imgtxtcontwag' id='$iid'>";	
			//echo "SELECT * FROM dogs_available WHERE dog_id='$sqlid'";
			
			$sqlavalible=query_execute_row("SELECT * FROM dogs_available WHERE dog_id='$dog_id'");
			$sqluserid=$sqlavalible['userid'];
			$sqldog=$sqlavalible['dog_name'];
			$sqldognicename=$sqlavalible['dog_nicename'];
			$sqlbreed=$sqlavalible['dog_breed'];
			$breed_nicename=$sqlavalible['breed_nicename'];
			$sqldesc=$sqlavalible['dogs_desc'];
			$dog_image=$sqlavalible['dog_image'];
			if($dog_image=="1002568_My_new_hide-out.jpg"){
				if($sqlimage1){
					$src = $imageBasePath.'/dogs/images/'.$sqlimage1;
					$imageURL2='/dogs/images/69x69-'.$sqlimage1;
				}else{
					$src =$imageBasePath.'/wag_club/images/no_imgDog.jpg';
					$imageURL2='/wag_club/images/69x69-no_imgDog.jpg';
				}
			}else{
				if($dog_image){
					$src = $imageBasePath.'/dogs/images/'.$dog_image;
					$imageURL2='/dogs/images/69x69-'.$dog_image;
				}else{
					$src =$imageBasePath.'/wag_club/images/no_imgDog.jpg';
					$imageURL2='/wag_club/images/69x69-no_imgDog.jpg';
				}
			}
			$dest = $imageBasePath.$imageURL2;
			createImgThumbIfnot($src,$dest,'69','69','ratiowh');
			$sqlavalible1=query_execute_row("SELECT f_name FROM users WHERE userid='$sqluserid'");
			$sqluser=$sqlavalible1['f_name'];	
			if($sqlimage1!=''){
?>
<form name="wag34" id="wag34" method="post">
<div class='imgtxtcontwag' id='<?=$iid?>'>
	<div class="post-container">
    	<div class="post-header">
        	<div class="contest_dog_small">
            	<img src="https://www.dogspot.in/<?=$imageURL2?>" alt="<?=$sqldog.''.$iid?>" title="<?=$sqldog.''.$iid?>" /> 
            </div>
            <div class="dogName_act_new">
                <input type="hidden" name="dog_id" id="dog_id" value="<?=$dog_id?>">
                <input type="hidden" name="user_id" id="user_id" value="<?=$userid?>">
                <strong> <?=ucwords($sqldog)?>, </strong>
                <a href="/dogs/breed/<?=$breed_nicename?>" style="color:#8dc059;" data-ajax="false"><?=$sqlbreed?></a> posted by <a href="/dogs/user/<?=$sqluserid?>/" style="color:#8dc059;" data-ajax="false"><?=$sqluser?></a></div>
            </div>
            <div class="post-content"> <a class="dg_mid_cmnt" href="#" data-ajax="false">
            	<div class="actImg_post_new"> <img src="https://www.dogspot.in/<?=$imageURL1?>" height="" alt="<?=$sqldog.''.$iid?>" title="<?=$sqldog.''.$iid?>"  > </div>
              </a>
              <div class="postIcon_wc_new"><?
             $wag_activity=mysql_query("SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND userid='$userid' AND activity_id='$activity_id'");
$count_activity_wag=mysql_query("SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND activity_id='$activity_id'");
$count_activity=mysql_num_rows($count_activity_wag);
$wag_like1=mysql_num_rows($wag_activity); 


if($wag_like1==0){
	

	if($userid=='Guest'){?>
    	<a href="/login.php?refUrl=<?=$refUrl?>" rel="nofollow" data-ajax="false"><div class="wagPost" id="postwagbefore<?=$activity_id?>"><img src="img/wag_new-like.png" width="28" height="26" alt="Like" title="Like"></div></a>
	<? }else{?>
		<a href="#" onclick="javascript:wagfuncount1('<?=$activity_id?>','<?=$dog_id?>');"  style="color:#fff" data-ajax="false"><div class="wagPost" id="postwagbefore<?=$activity_id?>"><img src="img/wag_new-like.png" width="28" height="26" alt="Like" title="Like">
        <? if($wag_like1!='0'){?><span id="acti_wag<?=$activity_id?>"><?=$count_activity?></span>
		<? }else{?><span id="acti_wag<?=$activity_id?>"><?=$count_activity?></span><? }?>
		</div>
		</a>
<? } }else{ ?>
	<a href="#" onclick="javascript: wagcount1('<?=$activity_id?>','<?=$dog_id?>');" style="color:#fff" data-ajax="false">
		<div class="wagPost" id="postwagafter<?=$activity_id?>"><img src="img/wag_new-liked.png" width="28" height="26" alt="Unlike" title="Unlike">
		<? if($wag_like1!='0'){?><span id="acti_wagq<?=$activity_id?>"><?=$count_activity?></span>
		<? }else{?><span id="acti_wagq<?=$activity_id?>" ><?=$count_activity?></span><? }?>
		</div>
	</a>
	<? } ?>
<a onclick="javascript: wagcount1('<?=$activity_id?>','<?=$dog_id?>');" data-ajax="false" style="color:#fff">
<div class="wagPost" id="postwagafter<?=$activity_id?>" style="display:none">
<img src="img/wag_new-liked.png" width="28" height="26" alt="Unlike" title="Unlike">
<? if($wag_like1!='0'){?><span id="acti_wagq<?=$activity_id?>" style="display: inline-block !important;"><?=$count_activity?></span>
<? }else{?><span id="acti_wagq<?=$activity_id?>" ><?=$count_activity?></span><? }?>
</div></a>
<a onclick="javascript:wagfuncount1('<?=$activity_id?>','<?=$dog_id?>');" data-ajax="false" style="color:#fff">
<div class="wagPost" id="postwagbefore<?=$activity_id?>" style="display:none;">
<img src="img/wag_new-like.png" width="28" height="26" alt="Like" title="Like">
<? if($wag_like1!='0'){?><span id="acti_wag<?=$activity_id?>"><?=$count_activity?></span>
<? }else{?><span id="acti_wag<?=$activity_id?>"><?=$count_activity?></span><? }?>
</div></a>
<span id="wag<?=$activity_id?>"></span>
<?
$titlewag=$sqldog;
$summarywag=dispUname($userid). " shared activity of ". $sqldog;
$urlwag="https://www.dogspot.in/wag_club/activity/".$activity_id;
$imagewag="https://www.dogspot.in/dogs/images/350x210-".$sqlimage1;
 
if($userid=='Guest'){?><a href="/login.php?refUrl=<?=$refUrl?>" rel="nofollow" data-ajax="false">
   <div class="wagPost"><img src="img/facebook-icon1.png" width="28" height="28" alt="Share" title="Share"></div></a>
<? }else{ ?>
<a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?=$titlewag;?>&amp;p[summary]=<?=$summarywag;?>&amp;p[url]=<?=$urlwag; ?>&amp;p[images][0]=<?=$imagewag;?>','sharer','toolbar=0,status=0,width=548,height=325,top=100,left=250');" href="javascript: void(0)" data-ajax="false">
	<div class="wagPost"><img src="img/facebook-icon1.png" width="28" height="28" alt="Share" title="Share"></div></a>
<? }?>
              </div>
              </div>
              
<!-- post comment box-->

<!---------Tag Line--------->
<div class="actTxt_post"><?=$sqlimage2?></div>
<!-----End Tag Line--------->

<?php 
	if($userid!='Guest'){
		include($DOCUMENT_ROOT . '/contest/comment.php');
	}else{
		include($DOCUMENT_ROOT . '/contest/commentguest.php');
	}
?>
<!-- post comment box-->
	</div>
</div>
<? } }?>
	</div>
</div>

	<div id="two">
        <div class="contest_list">
          <div class="post-container">
          
           <?
	$date=date('Y-m-d',strtotime("0 days"));
	 $sqldata=query_execute("SELECT * FROM dogs_activity as dc,dogs_available as da WHERE dc.publish_status='publish' AND user_activity='contest' AND winner='yes' AND da.dog_id=dc.dog_id ORDER BY dc.c_date DESC");
	 while($winner=mysql_fetch_array($sqldata)){
		$img=$winner['image']; 
		$dog_name=$winner['dog_name']; 
		$c_date=$winner['c_date']; 

    $imglink = '/dogs/images/96x96-'.$img;
    $src2 = $DOCUMENT_ROOT . '/dogs/images/'.$img;
    $dest2 = $DOCUMENT_ROOT . $imglink;
	createImgThumbIfnot($src2, $dest2, '96', '96', 'ratiowh');
    ?>
    <div class="today_winner_blk">
   <div class="today_date"><? echo date('jS F Y',strtotime($c_date));?></div>
   <div class="winner_dog_info">
  <div class="contest_img"><img src="https://www.dogspot.in/<?=$imglink?>" />
  
  </div>
  <div class="contest_dogname"><?=$dog_name?></div>
<div class="contest_tagline" style="float: left;
    font-size: 16px;
    font-family: arial;
    width: 60%;
    margin-top: 5px;">Won Pooch of the Week</div>
   </div>
    </div>
    <? }?>
    
    </div>
    </div>
        </div>
   
    <!-- post end--> 

<!-- contest page end-->
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
<!-- cart page start--> 

