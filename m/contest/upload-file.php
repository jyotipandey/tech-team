<?php
/*ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);*/

ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if ($section[0] == '') {
    require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL . "/database.php");
require_once(SITEMAIN_URL . "/check_img_function.php");
require_once(SITEMAIN_URL . "/functions.php");
require_once(SITEMAIN_URL . "/functions2.php");
require_once(SITEMAIN_URL . '/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL . '/session.php');
$sitesection  = 'dog-breeds';
$ant_section  = 'Dog Breeds';
$ant_page     = 'breedhome';
$ant_category = "";
$sel_email    = query_execute_row("SELECT * FROM users WHERE userid='$userid'");
?>

<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="css/contest.css?h=10">
<script type="text/javascript" src="<?php echo $baseUrl;?>js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl;?>js/live-validation.js"></script>
<script type="text/javascript" src="<?php echo "https://www.dogspot.in";?>/js/shaajax.min.2.1.js"></script>
<script type="text/javascript" src="https://www.dogspot.in/wag_club/file_uploads.js"></script>
<title>Dog Breeds | Complete Information On Dog Breeds | Wagpedia | Dog Breed Types</title>
<meta name="keywords" content=" Dog Breeds, Dog Breeds List, Dogs Breed Selector, Best Dog Breeds, Top Dog Breeds" />
<meta name="description" content="WagPedia - Complete Information On Dog Breeds, Temperament,Types, Pictures, Care, Diet,Training to help you determine which type of dog you should get." />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:title" content="Dog Breeds | Complete Information On Dog Breeds | Pictures | Types | Temperament | DogSpot.in" /> 
<meta property="og:description" content="Find Complete Information On Dog Breeds,Types, Pictures, Care, Diet. and this helps you determine which type of dog you should get at Dog Breeds Section with DogSpot.in" />


<body>

<?php
require_once($DOCUMENT_ROOT . '/common/top.php');
?>
 <form id="vasPLUS_Programming_Blog_Form" method="post" enctype="multipart/form-data" action="submit.php" data-ajax="false" >
<div class="myDogsBlock_wrp" style="margin:10px;">
<div class="myDogText">Make it simple and fun to capture and share the everyday magic in your dog's life. Give it a whirl!<span class="triangle_down"></span></div>
<div class="contest_uploadblk myDogText_content">

<div class="fileUpload btn btn-primary">
 <span>Upload Photo</span>
  <input type="file" accept="image/*" name="fileUpload" id="fileUpload" class="upload" value="Upload Photo" />
</div>

<div id="image-holder" class="image-holder"></div>
<div class="mrt">
	<input type="text" name="tagline" id="tagline" placeholder="Tagline" class="dog_nameinput" />
</div>   
  
<div id="image-holder" class="image-holder"></div>
<div class="mrt">
	<input type="text" name="dog_name" id="dog_name" required placeholder="Dog's Name" class="dog_nameinput" />
</div>

<div class="mrt">
<select name="breed" id="breed" required>
<option>Dog's Breed</option>
 <?php
	$queryBreed = mysql_query("SELECT * FROM dog_breeds ORDER BY breed_name ASC");
	while ($rowBreed = mysql_fetch_array($queryBreed)) {
?>
    <option value="<?= $rowBreed['breed_id'] ?>"><?= $rowBreed['breed_name'] ?></option>
                  
<?php } ?>
</select>
</div>
<div class="mrt">
<input type="Submit" name="acttag" id="acttag" value="Publish">

</div>
<div  class="mrt">
<input  value="Cancel" type="reset">

</div>
</div>
</div>
</form>

     <script type="text/javascript" src="<?php echo $baseUrl;?>js/jquery.mobile-1.4.4.min.js"></script> 
<script>
	  $("#fileUpload").on('change', function () {

     //Get count of selected files
     var countFiles = $(this)[0].files.length;
	
     var imgPath = $(this)[0].value;
     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
     var image_holder = $("#image-holder");
     image_holder.empty();

     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
         if (typeof (FileReader) != "undefined") {

             //loop for each file selected for uploaded.
             for (var i = 0; i < countFiles; i++) {

                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $("<img />", {
                         "src": e.target.result,
                             "class": "thumb-image"
                     }).appendTo(image_holder);
                 }

                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
             }

         } else {
             alert("This browser does not support FileReader.");
         }
     } else {
         alert("Pls select only images");
     }
 });
	 </script>
<?php
require_once($DOCUMENT_ROOT . '/common/bottom.php');
?>
