<?php
if($section[0]==''){
	require_once("../constants.php");
}
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>What is Wag Tag | Dog Tag | DogSpot.in</title>
<meta name="keywords" content="What is Wag Tag, Dog Tag, Wag Tag Information, Full Detail On what is Wag Tag" />
<meta name="description" content="What is Wag Tag, Dog Tag, know full information about Wag Tag" />
<link rel="canonical" href="https://www.dogspot.in/wagtag/what-is-wag-tag.php" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.dogspot.in/wagpage.php" />
<link rel="stylesheet" href="fonts.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<link rel="stylesheet" href="css/style.css?=v401000134468">
<section class="wagtag-section">
  <div class="container-fluid" style="padding:0;position: relative;"> <img src="images/wagtag-banner.png" class="img-responsive hidden-xs tag-icon-main"> <img src="images/wagtag-banner-1.png" class="img-responsive visible-xs tag-icon-mo"> <img src="images/tag-icon.png" class="img-responsive hidden-xs tag-icon"> </div>
  <div class="main-tag-menu">
    <ul class="tag-menu hidden-xs">
      <li><a  data-ajax="false" style="font-weight:600;" href="/wagtag/index-new.php"  >Activate WAGTAG</a></li>
      <li><a data-ajax="false"  class="active" href="/wagtag/what-is-wag-tag-new.php" >What is WAGTAG?</a></li>
      <li><a data-ajax="false" href="/wagtag/how-it-works-new.php" >How does it work?</a></li>
      <li><a data-ajax="false" href="/wagtag/faq-new.php" >FAQs</a></li>
      <li><a data-ajax="false" href="/wag_club/" >Wag Club</a></li>
    </ul>
    <ul class="tag-menu-mo visible-xs">
      <li><a data-ajax="false" href="/wagtag/index-new.php"  >Activate WAGTAG</a></li>
      <li><a data-ajax="false" href="/wagtag/what-is-wag-tag.php" class="active" >What is WAGTAG?</a></li>
      <li><a data-ajax="false" href="/wagtag/how-it-works-new.php" >How does it work?</a></li>
      <li><a data-ajax="false" href="/wagtag/faq-new.php" >FAQs</a></li>
      <li style="border-right:none;"><a href="/wag_club/" >Wag Club</a></li>
    </ul>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6 ">
        <div class="wt_logo"> <img src="/wagtag/images/wag-tag-banner.jpg" width="457" height="254" alt="" class="img-responsive"> </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="wt_whatImg"><img src="/wagtag/images/wag-id-banner.jpg" width="581" height="281" alt="" class="img-responsive" style="margin:auto"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="wt_infoBox">
          <h1>What is Wag Tag?</h1>
          <p>wag tag is a high grade stainless steel tag that can be easily attached to your pet’s collar and will ensure safety of your pets. Giving you respite from sleepless nights.</p>
          <p>The wag tag helps you to not only get your pet back but maintains confidentiality for the pet owner. Each of the tag comes with a unique code, if your pet gets lost the finder will call the number given on the tag and it will be connected to DogSpot. Our customer care will in turn connect the call to the rightful owner. </p>
        </div>
        <div class="wt_howBox">
          <h2> Information on your tag</h2>
          <p>The tag will contain the following details that will help the pet to be reunited with its owners</p>
          <div class="wt_fillInfo">
            <ul>
              <li>The tag will contain information of the owner, such as his number, name and address though this is not visible to the owner.</li>
              <li>In addition it contains details of the pet such as medical information such as allergies or any specific medications.</li>
              <li>Vaccination details of the lost pet</li>
              <li> Dietary requirements of the pet in case any special requirements are there</li>
              <li>It will also give information if the pet is spayed or natured and a description of his behavior</li>
            </ul>
          </div>
        </div>
        <div class="wt_howBox">
          <h2>How will it help to get your pet back?</h2>
          <div class="wt_whatTxt">
            <ul style="float:left; margin-left: 30px;">
              <li>Every dog is allocated a unique id eliminating chances of any errors</li>
              <li>The owners number is kept confidential, the call is routed to the owner through DogSpot customer care</li>
              <li>The tag is transferred when the owner is changed</li>
              <li>wagtag can be easily activated by logging on to DogSpot or through the customer service IVR</li>
              <li> Pet owners can update the information as and when required</li>
            </ul>
          </div>
          <div class="wt_howBox">
            <h2>What to do when you lose a tag?</h2>
            <p>You will have to place an order for another tag which will have a unique identification number that will be different from the previous one. The moment the new tag is generated the older tag will get deactivated. Once you receive the new tag update the information on your pet's profile</p>
          </div>
          <div class="wt_howBox">
            <h2>What happens when the owner is changed?</h2>
            <p>The wag tag comes with a unique transfer feature. If you give your dog to someone; then you can also transfer the wag tag to him or her. You just have to log on the profile page of your pet and select the Transfer your tag option.</p>
          </div>
          <div class="wt_howBox">
            <h2>Comparison</h2>
            <table class="wt_paraBox table">
              <tbody>
                <tr>
                  <th>Features</th>
                  <th>Wag tag</th>
                  <th>Traditional tag</th>
                </tr>
                <tr>
                  <td>Privacy of information</td>
                  <td>yes</td>
                  <td>no</td>
                </tr>
                <tr>
                  <td>Visibility</td>
                  <td>yes</td>
                  <td>yes</td>
                </tr>
                <tr>
                  <td>Scanning without smartphone</td>
                  <td>yes</td>
                  <td>yes</td>
                </tr>
                <tr>
                  <td>Value for money</td>
                  <td>yes</td>
                  <td>yes</td>
                </tr>
                <tr>
                  <td>Dietary information</td>
                  <td>yes</td>
                  <td>no</td>
                </tr>
                <tr>
                  <td>Medical information</td>
                  <td>yes</td>
                  <td>no</td>
                </tr>
                <tr>
                  <td>Updation of information </td>
                  <td>yes</td>
                  <td>no</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
