<?php 
	require_once('constants.php');
	require_once('functions.php'); 
	require_once(SITEMAIN_URL.'/database.php');
	//require_once(SITEMAIN_URL.'/functions.php');
	require_once(SITEMAIN_URL.'/functions2.php');
	require_once(SITEMAIN_URL.'/shop/functions.php');
	require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
	require_once(SITEMAIN_URL.'/arrays.php');
	require_once(SITEMAIN_URL.'/session.php');
	require_once('functions_gift.php');
	
	if(!$_GET['catid'] && !$_GET['brandid']){
		header("location: https://m.dogspot.in/");	
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<link rel="canonical" href="https://m.dogspot.in/filters.php?catid=<?=($catid)?>" />
 	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <?php /*?> <meta http-equiv="refresh"/><?php */?>
    <?php require_once('common/script.php'); ?>
  	<div id="filters-page24" class="whitewrapper">
		<script type="text/javascript">
            $("#filters-page24").on("pageinit", function() {
            $("#howtoactivatetag_link").click(function() {
                $.mobile.changePage("#howactivatewagtag-page6");
            });	
            var mySwiper = new Swiper('.swiper-container11',{
                pagination: '.pagination11',
                grabCursor: true,
                paginationClickable: true,
              });
            $("#backtodoggrooming").click(function() {
                $.mobile.changePage("#grooming-page23",{reverse:true});
            });
        });
        $("#filters-page24").on("pageshow", function() {
    
        });
        </script>
        
        <script type="application/javascript">
			function submitForm(){
				// For all attrbute except cat_id=15
				var type_breed = document.getElementsByName('type_breed11[]');
    			var len = type_breed.length;
				var breedValue;
				for (var i=0; i<len; i++) {
					if(type_breed[i].checked){
						breedValue  = breedValue + type_breed[i].value + "|" ;
					}
				}
				//alert(breedValue);
				//End
				
				// For Brand all category
				var brand = document.getElementsByName('brand[]');
    			var brand_len = brand.length;
				var brandValue;
				for (var i=0; i<brand_len; i++) {
					if(brand[i].checked){
						brandValue  = brandValue + brand[i].value + "-" ;
					}
				}
				//alert(brandValue);
				//End Brand
				
				
				// For life stage all category
				var type_life = document.getElementsByName('life_stage[]');
    			var life_len = type_life.length;
				var lifeValue;
				for (var i=0; i<life_len; i++) {
					if(type_life[i].checked){
						lifeValue  = lifeValue + type_life[i].value + "-" ;
					}
				}
				//alert(lifeValue);
				//End Brand
				
				
				// For Breed Name
				var breed_name = document.getElementsByName('breed_name[]');
    			var breed_name_len = breed_name.length;
				var breedNameValue;
				for (var i=0; i<breed_name_len; i++) {
					if(breed_name[i].checked){
						breedNameValue  = breedNameValue + breed_name[i].value + "-" ;
					}
				}
				//alert(breedNameValue);
				//End Brand
				
				// For Breed Type only for cat_id 15
				var breed_dog = document.getElementsByName('type_breed_dog[]');
    			var type_breed_dog_len = breed_dog.length;
				var typeBreedDogValue;
				for (var i=0; i<type_breed_dog_len; i++) {
					if(breed_dog[i].checked){
						typeBreedDogValue  = typeBreedDogValue + breed_dog[i].value + "-" ;
					}
				}
				//alert(typeBreedDogValue);
				//End Brand
				
				
				$('#breed_arr').val(breedValue);
				$('#brand_arr').val(brandValue);
				$('#life_arr').val(lifeValue);
				$('#breed_name_arr').val(breedNameValue);
				$('#dog_breed_type_arr').val(typeBreedDogValue);

				// Submit filter form
				document.getElementById("filterfrm").submit();

				/*if($('input[type=checkbox]').is(':checked')){
					document.getElementById("filterfrm").submit();
				}else{
					alert("Choose your filter criteria");
				}*/
				//
			}
			
			function resetValue(){
				// Reset all filter criteria
				$('input').attr('checked', false);
				$('label').removeClass('ui-checkbox-on');
			}
		</script>
        
    <?php require_once('common/top.php'); ?>
    
    <?php 
		
		if($_GET['catid'] || $_GET['brandid']){
			$cat_id = $_GET['catid'];
			$brand_id = $_GET['brandid'];
			if($cat_id=='500')
			{
			$salinstock="item_sale:1 AND visibility:visible AND NOT type_id:configurable AND NOT item_display_status:delete AND NOT domain_id:2 AND stock_status:instock&version=2.2&rows=6000&fl=* score&qf=name^2&indent=on";	
			}elseif($cat_id=='600')
			{
			$salinstock="item_sale:1 AND visibility:visible AND NOT type_id:configurable AND NOT item_display_status:delete AND  domain_id:1 AND stock_status:instock&version=2.2&rows=6000&fl=* score&qf=name^2&indent=on";	
			}elseif($cat_id=='700')
			{
			$salinstock="item_sale:1 AND visibility:visible AND NOT type_id:configurable AND NOT item_display_status:delete AND domain_id:3 AND stock_status:instock&version=2.2&rows=6000&fl=* score&qf=name^2&indent=on";	
			}elseif($brand_id){
				$salinstock="item_brand:\"$brand_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND stock_status:instock&version=2.2&rows=500&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
			}else{
				$salinstock="item_category_id:\"$cat_id\" AND visibility:visible AND NOT type_id:configurable  AND NOT item_display_status:delete AND stock_status:instock&version=2.2&rows=500&fl=* score&qf=name^2&df=text&wt=xml&indent=true";
			}
			$url = "http://101.53.137.39/solr/dogspotshopsolr/select/?q=$salinstock";

			$url = str_replace(" ","%20",$url);
			$resultsolr = get_solr_result($url);
			$totrecord = $resultsolr['TOTALHITS'];
			//print_r($resultsolr);
			foreach($resultsolr['HITS'] as $rowItemall){
				
				$Aitem_category[]=$rowItemall["item_cat_id"];//add code
				$Aitem_Itemid[]=$rowItemall["item_id"];
				$Aitem_brand[]=$rowItemall["item_brand"];
				$Aitem_price[]=$rowItemall["price"];
				$Aitem_category1[]=$rowItemall["item_category_id"];
				$Aitem_attribute[]=$rowItemall["item_attribute"];
				$Aitem_life_stage[]=$rowItemall["item_life_stage"];
				$Aitem_breed_id[]=$rowItemall["item_breed_id"];
				$Aitem_breed_type[]=$rowItemall["item_breed_type"];
				$Aitem_weight[]=$rowItemall["weight"];
				$A1weight_price[]=@round($rowItemall["price"]/$rowItemall["weight"],3);    
			}
			if($userid=='umesh3'){
				/*echo "<pre>";
				print_r($Aitem_brand);
				echo "</pre>";*/
			}
			if($cat_id !='500'){
			if($totrecord>0){		
				// Get unique data in array	
				$Aitem_brand=array_unique($Aitem_brand);
				$Aitem_price=array_unique($Aitem_price);
				$Aitem_attribute=array_unique($Aitem_attribute);
				$Aitem_life_stage=array_unique($Aitem_life_stage);
				$Aitem_breed_id=array_unique($Aitem_breed_id);
				$Aitem_breed_type=array_unique($Aitem_breed_type);
				$Aitem_category=array_unique($Aitem_category); //add code from
				//print_r($Aitem_category);//Array ( [0] => 62|10 [46] => 62|10|87|149 )
				$Aitem_weight=array_unique($Aitem_weight);
				$A1weight_price=array_unique($A1weight_price);
				
				//End Array Unique
				
				//Create Attribute
				foreach($Aitem_attribute as $Aitem_attribute1){ 
					$getdata=explode('|',$Aitem_attribute1);
					foreach($getdata as $getd){
						$get=explode('A',$getd);
						$key=$get['0'];
						$keyvalue=$get['1'];
						$Asd[]=$key.'@'.$keyvalue;
						$keypoint[]=$get['0'];
					}
				}
				
				$Akeypoint=array_unique($keypoint);
				asort($Akeypoint);
				$Asd=array_unique($Asd);
				//End Attrbutes
				
				// Dry Dog Food Attributes
				if($cat_id=="15"){
					$breed_type1 = array(
						array("value"=>"2","name"=>"Small Breed","hints"=>"upto 10kg"),
						array("value"=>"3","name"=>"Medium Breed","hints"=>"10-25kg"),
						array("value"=>"5","name"=>"Large Breed","hints"=>"25-45kg"),
						array("value"=>"6","name"=>"Giant Breed","hints"=>"above 45kg"),
						array("value"=>"7","name"=>"All Breed","hints"=>"for all type")
						
					);
					$attr_set_breed_type = "BREED TYPE";  
					$breed1 = array(
						array("value"=>"11","name"=>"Beagle","image"=>"https://www.dogspot.in/mobile-webservice/android/images/beagle.png"),
						array("value"=>"23","name"=>"Boxer","image"=>"https://www.dogspot.in/mobile-webservice/android/images/boxer.png"),
						array("value"=>"37","name"=>"Cocker Spaniel","image"=>"https://www.dogspot.in/mobile-webservice/android/images/cocker-spaniel.png"),
						array("value"=>"60","name"=>"German Shepherd","image"=>"https://www.dogspot.in/mobile-webservice/android/images/german-shepherd.png"),
						array("value"=>"65","name"=>"Golden Retriever","image"=>"https://www.dogspot.in/mobile-webservice/android/images/golden-retriever.png"),
						array("value"=>"67","name"=>"Great Dan","image"=>"https://www.dogspot.in/mobile-webservice/android/images/great-dane.png"),
						array("value"=>"86","name"=>"Labrador Retriever","image"=>"https://www.dogspot.in/mobile-webservice/android/images/labrador-retriever.png"),
						array("value"=>"108","name"=>"Pug","image"=>"https://www.dogspot.in/mobile-webservice/android/images/pug-dog.png"),
						array("value"=>"112","name"=>"Rottweiler","image"=>"https://www.dogspot.in/mobile-webservice/android/images/rottweiler-dog.png")
						
					);
					$attr_set_Breed = "BREED";
					$life_stages1 = array(
						array("value"=>"1","name"=>"Starter","hints"=>"upto 2 months"),
						array("value"=>"2","name"=>"Puppy & Junior","hints"=>"2-24 months"),
						array("value"=>"5","name"=>"Adult", "hints"=>"2-7 years"),
						array("value"=>"6","name"=>"Senior", "hints"=>"more than 7 years")
					);
					$attr_set_life_stage = "LIFE STAGE";
					
					$mainn[$attr_set_breed_type]=$breed_type1;
					
					$mainn[$attr_set_Breed]=$breed1;
					
					$mainn[$attr_set_life_stage]=$life_stages1;
				}
			}
		}else{
		if($totrecord>0){		
				// Get unique data in array	
			//	print_r($Aitem_category);
				$Aitem_category=array_unique($Aitem_category1);
				$Aitem_brand=array_unique($Aitem_brand);
				$Aitem_price=array_unique($Aitem_price);
				//$Aitem_category=array_unique($Aitem_category); //add code from
				foreach($Aitem_category as $Aitem_category2){
		$getdatacat=explode('|',$Aitem_category2);
	 		foreach($getdatacat as $getcat){
				$cattt[]=$getcat;
				}
				}
				$cattt=array_unique($cattt);
				asort($cattt);
				foreach($cattt as $cattt23){
				$qParentCatNice2=query_execute_row("SELECT category_name,category_id,category_parent_id FROM shop_category WHERE category_id='".$cattt23."' AND category_parent_id!='0' AND category_status='active'");
		
		$cate=$qParentCatNice2['category_name'];
		
				if($cattt23==190){} 	
				$Acatarry[]=$cate."@@".$cattt23;
					}
				asort($Acatarry);
	
			}	
		}
		}
		
	?>
   
   	<style>
		.ui-btn-icon-bottom:after,.ui-btn-icon-left:after,.ui-btn-icon-notext:after,.ui-btn-icon-right:after,.ui-btn-icon-top:after{
			content:"";position:absolute;display:block; color:#999;
		}
		.ui-checkbox input{ display:none;}
	
		.lifestage_blk ul li label.ui-btn.ui-checkbox-off:after, .lifestage_blk ul li label.ui-btn .ui-checkbox-on:after {
			border: none;
			display: block;
			height: 30px;
			margin: -14px 2px 0;
			width: 30px;
			opacity: 1;
			background:#fff;
			background: url(../images/foot.png) right center no-repeat;
		}

		.lifestage_blk ul li label.ui-checkbox-on {
			color: #bba888;
			font-weight: 700;
			
		}
		.ds-product-check-box .ui-icon-check:after, html .ui-btn.ui-checkbox-on.ui-checkbox-on:after{ background: url(../images/foot-greeen.png) right center no-repeat;}
		.lifestage_blk ul li label{ font-size:14px !important;}
	</style>
   	<div data-role="content">
		<div class="refine_search">Refine Your Search</div>
        	<form action="filter-search-result.php" name="filterfrm" id="filterfrm" method="POST"  class="filter_blk">
				<div class="filter_page">
					<div class="filter_blk">
                    
                      <!-----Price Range------>
                        <?    
                            foreach($Aitem_price as $ap){
                                $ANitem_price[]=$ap;
                            }
                            asort($ANitem_price);
                            $endnum = min($ANitem_price);
                            $startnum = max($ANitem_price);
                        ?>
                        <p>PRICE RANGE</p>
                        <div class="range">
                            <div data-role="fieldcontain">
                                <div data-role="rangeslider" data-mini="true">
                                    <input type="range" name="min_price" id="range-8a" min="0" max="<?=$startnum; ?>" value="<?=$endnum; ?>">
                                    <span>to</span>
                                    <input type="range" name="max_price" id="range-8b" min="0" max="<?=$startnum; ?>" value="<?=$startnum; ?>">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="<?=$cat_id?>" name="cat_id" id="cat_id"/>
                       	<input type="hidden"  name="type_breed11" id="breed_arr"/>
                        <input type="hidden"  name="brand_id" id="brand_arr"/>
                        <input type="hidden"  name="type_life" id="life_arr"/>
                        <input type="hidden"  name="breedname" id="breed_name_arr"/>
                        <input type="hidden"  name="dog_breed_type" id="dog_breed_type_arr"/>
                        <?php if($cat_id =='500'){?>
                          <!---For category----->
                        <?php //print_r($Acatarry);
                            foreach($Acatarry as $cat){
								$cattt1y=explode("@@",$cat);
								$cattt1=$cattt1y[1];	
								    $qbrandname1=query_execute_row("SELECT category_name,category_id,category_parent_id FROM shop_category WHERE category_id='$cattt1' AND category_parent_id!='0' AND category_status='active'");
								//echo $qbrandname1['category_name'].',',$qbrandname1['category_id'];
								if($qbrandname1['category_name'] &&($qbrandname1['category_id']!='87' )){
                                $Acatname[]=strtoupper($qbrandname1['category_name'])."@@".$cat;
								
                            }
							}
                            asort($Acatname);
							
                        ?>
                        <? if(count($Acatname) > 0){?>
               
                        <p>CATEGORY</p>
                        <div class="lifestage_blk">
                            <ul id="category">
                            
                            <? $i=1; //print_r($Acatname);
							 foreach($Acatname as $Acatname1){
								$cattt1y=explode("@@",$Acatname1);
								$cattt1name=$cattt1y[0]; 
								$cattt1=$cattt1y[2];                                 
                            ?>
                                     <li><label><?=$cattt1name; ?><input type="checkbox" value="<?=$cattt1?>" name="categoryid23[]" id="filter_category" class='getcheck' /></label></li>  
                            <?	$i++;   } ?>
                            </ul>
                        </div>
                        <? }?>
                        <!---For category----->
                        <? }?>
                        <!---For Brand----->
                        <?php 
                            foreach($Aitem_brand as $abrand23){
								
                                $qbrandname=query_execute_row("SELECT brand_name FROM shop_brand where brand_id=$abrand23");
								if($userid == 'umesh3'){
									//echo $qbrandname['brand_name']."<br/>";
								}
                                $Abrandname[]=strtoupper($qbrandname['brand_name'])."@@".$abrand23;
                            }
                            asort($Abrandname);
                        ?>
                        <? if(count($Abrandname) > 0){?>
               
                        <p>BRAND</p>
                        <div class="lifestage_blk">
                            <ul id="brand">
                            
                            <? $i=1; foreach($Abrandname as $abrandA){
									$cabrandA=explode("@@",$abrandA);//$ArrayShopBrands[$abrand]		
                                    if($cabrandA[1]!='0' && $cabrandA[1]!='35' ){
                            ?>
                                     <li><label><?=$cabrandA[0]; ?><input type="checkbox" value="<?=$cabrandA[1]?>" name="brand[]" id="filter_brand" class='getcheck' /></label></li>  
                            <?	$i++; }  } ?>
                            </ul>
                        </div>
                        <? }?>
                        <!---For Brand----->
                        <?php 
						if($cat_id !='500')
                        $i=1;
                        foreach($Akeypoint as $keyname){
                            if($keyname){
                                if(($cat_id=='40' && $keyname=='35') || $keyname=='37' || $cat_id=='15'){}else{
                                    $qAttname=query_execute_row("SELECT attribute_set_name FROM shop_attribute_set where attribute_set_id='$keyname' ");
                                    $attr_set_name = str_replace(" ","_",$qAttname['attribute_set_name']);
                        ?>
                                     <p><?=$qAttname['attribute_set_name']?></p>
                                     <div class="lifestage_blk">
                                        <ul>
                                        <?php 
                                        
                                        foreach($Asd as $Asd1){
                                            $Asd2=explode('@',$Asd1);
                                            if($keyname==$Asd2[0]){
                                                $qAttvalue=query_execute_row("SELECT attribute_value_name FROM shop_attribute_value where attribute_value_id='$Asd2[1]' ");	
                                                $name = str_replace("-"," ",$qAttvalue['attribute_value_name']);
                                        ?>
                                        <li><label><?=$name?><input type="checkbox" name="type_breed11[]" value="<?=$Asd2[0].'-'.$Asd2[1]?>"/></label></li>
                                        <?	} } ?>
                                        </ul>
                                    </div>
                        
                        <?php } } } ?>
                        <? if(count($mainn) >0){ 
                            foreach($mainn as $key=>$value){
                      ?>
                      
                      <? if($key=='LIFE STAGE'){  // For life stages?>
                        <p><?=$key?></p>
                        <div class="lifestage_blk">
                            <ul>
                                <? foreach($value as $value1){?>
                                <li>
                                    <label><?=$value1['name']; ?> <small>(<?=$value1['hints']; ?>)</small><input type="checkbox" name="life_stage[]" value="<?=$value1['value']; ?>"></label>
                                </li>
                                <? } ?>
                            </ul>
                        </div>
                        <? }?>
                        
                         <? if($key=='BREED'){ // For Breed ?>
                        <p><?=$key?></p>
                        <div class="lifestage_blk">
                            <ul>
                                <? foreach($value as $value1){?>
                                <li>
                                    <label><?=$value1['name']; ?> <input type="checkbox" name="breed_name[]" value="<?=$value1['value']; ?>"></label>
                                </li>
                                <? } ?>
                            </ul>
                        </div>
                        <? }?>
                        
                        <? if($key=='BREED TYPE'){  // For Breed Type?>
                        <p><?=$key?></p>
                        <div class="lifestage_blk">
                            <ul>
                                <? foreach($value as $value1){?>
                                <li>
                                    <label><?=$value1['name']; ?> <small>(<?=$value1['hints']; ?>)</small><input type="checkbox" name="type_breed_dog[]" value="<?=$value1['value']; ?>"></label>
                                </li>
                                <? } ?>
                            </ul>
                        </div>
                        <? }?>
                        
                        <? } }?>
                      
                    </div>
		            <!----End------->
            	</div>
                <div data-role="footer" id="paybutton" data-position="fixed" class="ui-footer ui-footer-fixed slideup ui-bar-inherit"  role="contentinfo" style="background:#fff; z-index:999;">

                <div class="filter_option">
                    <div class="filter_btn reset"><a href="" onClick="javascript: resetValue();">Reset Filter</a></div>
                    <div class="filter_btn apply"><a href="" data-ajax="flase" onClick="javacsript: submitForm();">Apply</a></div>
                </div>
                </div>
	 		</form>
      	</div>
        
		<?php require_once('common/bottom.php'); ?>