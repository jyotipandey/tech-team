<? 
function is_mobile1(){
	
	// Get the user agent

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	// Create an array of known mobile user agents
	// This list is from the 21 October 2010 WURFL File.
	// Most mobile devices send a pretty standard string that can be covered by
	// one of these.  I believe I have found all the agents (as of the date above)
	// that do not and have included them below.  If you use this function, you 
	// should periodically check your list against the WURFL file, available at:
	// http://wurfl.sourceforge.net/


	$mobile_agents = Array(


		"240x320",
		"acer",
		"acoon",
		"acs-",
		"abacho",
		"ahong",
		"airness",
		"alcatel",
		"amoi",	
		"android",
		"anywhereyougo.com",
		"applewebkit/525",
		"applewebkit/532",
		"asus",
		"audio",
		"au-mic",
		"avantogo",
		"becker",
		"benq",
		"bilbo",
		"bird",
		"blackberry",
		"blazer",
		"bleu",
		"cdm-",
		"compal",
		"coolpad",
		"danger",
		"dbtel",
		"dopod",
		"elaine",
		"eric",
		"etouch",
		"fly " ,
		"fly_",
		"fly-",
		"go.web",
		"goodaccess",
		"gradiente",
		"grundig",
		"haier",
		"hedy",
		"hitachi",
		"htc",
		"huawei",
		"hutchison",
		"inno",
		"ipad",
		"ipaq",
		"ipod",
		"jbrowser",
		"kddi",
		"kgt",
		"kwc",
		"lenovo",
		"lg ",
		"lg2",
		"lg3",
		"lg4",
		"lg5",
		"lg7",
		"lg8",
		"lg9",
		"lg-",
		"lge-",
		"lge9",
		"longcos",
		"maemo",
		"mercator",
		"meridian",
		"micromax",
		"midp",
		"mini",
		"mitsu",
		"mmm",
		"mmp",
		"mobi",
		"mot-",
		"moto",
		"nec-",
		"netfront",
		"newgen",
		"nexian",
		"nf-browser",
		"nintendo",
		"nitro",
		"nokia",
		"nook",
		"novarra",
		"obigo",
		"palm",
		"panasonic",
		"pantech",
		"philips",
		"phone",
		"pg-",
		"playstation",
		"pocket",
		"pt-",
		"qc-",
		"qtek",
		"rover",
		"sagem",
		"sama",
		"samu",
		"sanyo",
		"samsung",
		"sch-",
		"scooter",
		"sec-",
		"sendo",
		"sgh-",
		"sharp",
		"siemens",
		"sie-",
		"softbank",
		"sony",
		"spice",
		"sprint",
		"spv",
		"symbian",
		"tablet",
		"talkabout",
		"tcl-",
		"teleca",
		"telit",
		"tianyu",
		"tim-",
		"toshiba",
		"tsm",
		"up.browser",
		"utec",
		"utstar",
		"verykool",
		"virgin",
		"vk-",
		"voda",
		"voxtel",
		"vx",
		"wap",
		"wellco",
		"wig browser",
		"wii",
		"windows ce",
		"wireless",
		"xda",
		"xde",
		"zte"
	);

	// Pre-set $is_mobile to false.

	$mobile_browser = '0';

	// Cycle through the list in $mobile_agents to see if any of them
	// appear in $user_agent.

	foreach ($mobile_agents as $device) {

		// Check each element in $mobile_agents to see if it appears in
		// $user_agent.  If it does, set $is_mobile to true.

		if (stristr($user_agent, $device)) {

			$mobile_browser = '1';

			// break out of the foreach, we don't need to test
			// any more once we get a true value.

			break;
		}
	}

	return $mobile_browser;
}
// function for create order for mobile website
function applyDiscountphone($order_id, $discount_code, $userid, $payment, $user_email, $user_phone)
{
    //Discount Type
    //Open 			= 	Email - any,	Product - 	any,	Use time -  unlimited 
    //email 		=	Email -	fix,	Product -	any,	Use time -  unlimited
    //onetime		=	Email -	any, 	Product -	any, 	Use time -	Onetime
    //emailitem		=	Email - fix, 	Product	-	fix,	Use time -	unlimited
    //emailonetime	= 	Email - any, 	Product - 	any,	Use time -	onetime
    $secCart1         = query_execute_row("SELECT order_status FROM shop_order WHERE order_id = $order_id");
	if($secCart1['order_status']=='inc'){
    $discc = 1;
   
    if ($discount_code) {
		if($discount_code=='DAMAGE50'){
			
		}else{
        $secCart         = query_execute("SELECT item_qty, item_id,mrp_price, item_price,item_totalprice FROM shop_cart WHERE cart_order_id = $order_id");
        $totaldisd_price = '0';
        while ($rowCartid = mysql_fetch_array($secCart)) {
            $qItem   = query_execute("SELECT item_id, name, price,selling_price ,discount FROM shop_items WHERE item_id='" . $rowCartid["item_id"] . "'");
            $rowItem = mysql_fetch_array($qItem);
            
            if ($rowItem["price"] < $rowItem["selling_price"]) {
                $discc           = 0;
                $totaldisd_price = $totaldisd_price + ($rowCartid['mrp_price'] * $rowCartid['item_qty']);
                $discount_msg    = 'Discount coupon is not valid on already discounted items.';
            }
        
    } }
	}
	
    if ($payment == 'rod') {
        $order_shipment_amount = 0;
		 $upOrder = query_execute("UPDATE shop_order SET order_shipment_amount='$order_shipment_amount', order_method='$payment', mode='$payment' WHERE order_id='$order_id'");
    } elseif ($payment == 'cod') {
        if ($order_shipment_amount < 5) {
            $order_shipment_amount = 49;
			 $upOrder = query_execute("UPDATE shop_order SET order_shipment_amount='$order_shipment_amount', order_method='$payment', mode='$payment' WHERE order_id='$order_id'");
			 if($discount_code=='')
			 {
				 return $discount_msg;
			 }
        }
    } else {
        $order_shipment_amount = 0;
		
		 $upOrder = query_execute("UPDATE shop_order SET order_shipment_amount='$order_shipment_amount', order_method='$payment', mode='$payment' WHERE order_id='$order_id'");
		 if($discount_code=='')
			 {
				 return $discount_msg;
			 }
    } // COD ------------------ END------------------------------------------------		
    if ($discc == 1) {
        if ($discount_code) {
		
            $upOrderinc = query_execute("UPDATE shop_order SET order_discount_amount='', order_discount_id='', order_discount_percentage='' WHERE order_id='$order_id'");
            
            
            $disc          = 0;
            $discount_code = strtoupper(trim($discount_code));
            $discountType  = query_execute("SELECT * FROM shop_discount_main WHERE discount_code = '$discount_code'");
            if (mysql_num_rows($discountType) > 0) { // if cide is valid--------------------
                $rowType = mysql_fetch_array($discountType);
                $todayt  = time();
                $startt  = strtotime($rowType["discount_start_date"] . " 00:00:00");
                $endt    = strtotime($rowType["discount_end_date"] . " 23:59:59");
                if ($todayt >= $startt && $todayt <= $endt) {
                    $calc = 1;
                } //if date true ends
                else {
                    $discount_msg = 'Discount code expired';
                    $calc         = 0;
                } // date check ends 
                if ($rowType["discount_status"] == 'inactive') {
                    $discount_msg = 'Discount code is inactive';
                    $calc         = 0;
                } // check discout status
            } else {
                $discount_msg = 'Discount code is invalid';
                $calc         = 0;
            } // END if Code is invalid-----------------------------------------------------
            
            if ($calc == 1) { // Open====
                if ($rowType["discount_type"] == 'open' || $rowType["discount_type"] == 'onetime' || $rowType["discount_type"] == 'mobile') {
                   if( $rowType["discount_type"] == 'mobile')
				   {
					   $mobile_browser=is_mobile1();
					if ($mobile_browser > 0) {
		$mob_user_jyoti='1';
		$_SESSION['mob_user_sess']='1';
		 $disc = 1;
		}else
		{
			 $disc = 0;
		}
				   }else{
                   if($discount_code=='DAMAGE50' && $user_email=='vizal.atheya@gmail.com')
						{
					   $query=query_execute("UPDATE shop_cart SET item_price=mrp_price,item_discount=0,item_totalprice=mrp_price*item_qty WHERE cart_order_id='$order_id'");
						 $disc = 1;	
							
						}elseif($discount_code !='DAMAGE50'){
                    $disc = 1;
						}else
						{
							$disc = 0;
						$discount_msg = 'Discount code is invalid';
						}
				   }
                    
                } // Open End=============
                
                //   Item  Specific======
                if ($rowType["discount_type"] == 'itemspecific') {
                    
                    $vcat1        = 0;
                    $vcat2        = 0;
                    $cartprice    = '0';
                    $userSpecific = query_execute("SELECT item_id, item_price, item_qty, cart_id FROM shop_cart WHERE cart_order_id = '$order_id'");
                    while ($rowSpecific = mysql_fetch_array($userSpecific)) {
                        $item_pri      = $rowSpecific['item_price'];
                        $cart_qty      = $rowSpecific['item_qty'];
                        $cart_itemid1  = $rowSpecific['item_id'];
                        $cartne_id     = $rowSpecific['cart_id'];
                        $totalprice    = $item_pri * $cart_qty;
                        $cartprice     = $totalprice + $cartprice;
                        $qItemnew      = query_execute_row("SELECT  price, selling_price, discount FROM shop_items WHERE item_id='$cart_itemid1'");
                        $selling_price = $qItemnew['selling_price'];
                        $price         = $qItemnew['price'];
                        if ($selling_price <= $price) {
                            $cartupdate = query_execute("UPDATE shop_cart SET item_discount='0' WHERE  cart_id = '$cartne_id'");
                        } else {
                            $cartupdate = query_execute("UPDATE shop_cart SET item_discount=$selling_price*item_qty-$price*item_qty WHERE  cart_id = '$cartne_id'");
                        }
                        
                        $Itemcount = query_execute_row("SELECT count(*) as count_items, item_qty, items_item_id FROM shop_discount_items WHERE
	 items_item_id='" . $rowSpecific['item_id'] . "' AND discount_discount_id = '" . $rowType["discount_id"] . "' ");
                        //echo "count".$Itemcount['count_items'];
                        
                        $limitem_id = $Itemcount['items_item_id'];
                        
                        if ($limitem_id == $rowSpecific['item_id']) {
                            $sameitem_id = $limitem_id;
                            
                            $limit_qty = $Itemcount['item_qty'];
                            $cart_qty1 = $cart_qty;
                        }
                        
                        if ($Itemcount['count_items'] == '0') {
                            $vcat2 = $vcat2 + 1;
                        } else {
                            $vcat1 = $vcat1 + 1;
                        }
                    } // while ends
                    if($discount_code=='ANAND30' && $user_email=='anand.vishwanath@anvisinc.com')
					{
						$disc = 1;
						//$discount_msg = 'Invalid user';
					}
                    elseif ($vcat1 == '0') {
                        $disc         = 0;
                        $discount_msg = 'Discount code is invalid';
                    } else if ($cart_qty1 > $limit_qty && $limit_qty != '0') {
                        $disc         = 0;
                        $discount_msg = 'This code is applicable only on ' . $limit_qty . ' QTY of item id ' . $sameitem_id . '.';
                    } else if ($cartprice < $rowType["discount_order_amount_limit"]) {
                        $disc         = 0;
                        $discount_msg = 'Order limit is less then ' . $rowType["discount_order_amount_limit"];
                    } else {
                        if($discount_code=='ANAND30' && $user_email!='anand.vishwanath@anvisinc.com')
					{
						$disc = 0;
						$discount_msg = 'Discount code is invalid';
					}
						
					else	{
                        $userdiscuntcount1 = query_execute_row("SELECT count(*) as count_order1 FROM shop_order_address as od,shop_order as o
						 WHERE order_discount_id = '" . $rowType["discount_id"] . "' AND o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_discount_id!='0' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone')");
						
                        
                        if ($userdiscuntcount1['count_order1'] < $rowType["discount_countlimit"]) {
                            $disc = 1;
                        } else {
                            $discount_msg = 'You have already used this discount';
                            $disc         = 0;
                        }
					}
                    }
                    
                } // Item Specific End================================
                
                
                // Email ========
                if ($rowType["discount_type"] == 'email' || $rowType["discount_type"] == 'emailitem') {
                    $userEmail = query_execute("SELECT ID FROM users WHERE u_email = '" . $rowType["discount_email"] . "' AND userid = '$userid'");
                    if (mysql_num_rows($userEmail) > 0) {
                        $disc = 1;
                    } else {
                        $discount_msg = 'Discount code is invalid';
                        $disc         = 0;
                    }
                } // Email End================
				  // New User ========
                if($rowType["discount_type"] == 'newuser'){
			//echo 'hi';
			//echo "SELECT o.userid,o.order_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_discount_id!='0' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone') ";
		$userEmail1 = query_execute("SELECT o.userid,o.order_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND od.address_email ='$user_email' AND od.address_phone1='$user_phone' ");
			if(mysql_num_rows($userEmail1)==0){
				$disc=1;
			}else{
				$discount_msg = 'Discount code is invalid';
				$disc=0;
			}
		
				
		}
                // Email One time================
                if ($rowType["discount_type"] == 'emailonetime') {
                    $vcat = 0;
                    
                    $userEmail12 = query_execute("SELECT o.userid,o.order_id,o.order_discount_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_discount_id!='0' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone') ");
                    
                    while ($roworderids = mysql_fetch_array($userEmail12)) {
                        $user_order = $roworderids['order_id'];
                        $order_dis  = $roworderids['order_discount_id'];
                        
                        if ($rowType["discount_id"] == $order_dis) {
                            $vcat = $vcat + 1;
                        } else {
                            $vcat = 0;
                        }
                    }
?>
	<input type="hidden" value="<?= $userEmail12['userid'] ?>" name="userid_email" id="userid_email"/>
    <input type="hidden" value="<?= $vcat ?>" name="email_counter" id="email_counter"/>
    
	<?
                    if ($vcat >= $rowType["discount_countlimit"]) {
                        $disc         = 0;
                        $discount_msg = 'You have already used this discount';
                        
                    } else {
                        $disc = 1;
                    }
                } // emailonetime =======================================================================================================
                
                
                // if Discount Type is Campaign
                if ($rowType["discount_type"] == 'campaign') {
                    
                    $vcat        = '0';
                    $userEmail12 = query_execute("SELECT o.userid,o.order_id,o.order_discount_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_discount_id!='0' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone')");
                    
                    while ($roworderids = mysql_fetch_array($userEmail12)) {
                        $order_dis = $roworderids['order_discount_id'];
                        
                        $userdis      = query_execute_row("SELECT discount_name FROM shop_discount_main  WHERE  discount_id = '$order_dis' ");
                        $campaignname = $userdis['discount_name'];
                        
                        $disname      = query_execute_row("SELECT discount_name FROM shop_discount_main  WHERE  discount_id = '" . $rowType["discount_id"] . "' ");
                        $discountname = $disname['discount_name'];
                        //echo $campaignname."sasd".$discountname."<br>";
                        if ($campaignname == $discountname) {
                            $vcat = $vcat + 1;
                        } else {
                            $vcat = 0;
                        }
                        
                    }
                    if ($vcat >= $rowType["discount_countlimit"]) {
                        $disc         = 0;
                        $discount_msg = 'You have already used a discount coupon for same campaign';
                        
                    } else {
                        $disc = 1;
                    }
                }
                
                if ($disc == 1) {
                    
                    $secOrder = query_execute("SELECT * FROM shop_order WHERE order_id = $order_id");
                    $rowOrder = mysql_fetch_array($secOrder);
                    
                    if ($rowType["discount_order_amount_limit"] > 0 && $rowType["discount_order_amount_limit"] > $rowOrder["order_amount"]) {
                        $discount_msg = 'Order limit is less then ' . $rowType["discount_order_amount_limit"];
                        $disc         = 0;
                    }
                    /*else if($rowOrder['order_discount_amount']<$rowOrder['order_items_amount']){
                    $discount_msg = 'Order Discount is less then '.$rowOrder["order_items_amount"];
                    $disc=0;
                    }*/
                    
                    else {
                        
                        if ($rowType["discount_type"] == 'emailitem') {
                            $secCart               = query_execute("SELECT item_id, item_totalprice FROM shop_cart WHERE cart_order_id = $order_id");
                            $order_discount_amount = '0';
                            while ($rowCartid = mysql_fetch_array($secCart)) {
                                $qItem         = query_execute("SELECT discount,price,selling_price FROM shop_items WHERE item_id='" . $rowCartid["item_id"] . "'");
                                $rowItem       = mysql_fetch_array($qItem);
                                $selling_price = $rowItem['selling_price'];
                                $price         = $rowItem['price'];
                                if ($selling_price <= $price) {
                                    $discAmt               = ($rowCartid["item_totalprice"] / 100) * $rowType["discount_percentage"];
                                    $order_discount_amount = $discAmt + $order_discount_amount;
                                }
                            } // END While----------------------------------------------------------------------------------	
                            
                            $per     = $order_discount_amount / ($rowOrder['order_items_amount']);
                            $secCart = query_execute("SELECT item_id,cart_id FROM shop_cart WHERE cart_order_id = $order_id");
                            while ($rowCartid = mysql_fetch_array($secCart)) {
                                $cart_id       = $rowCartid['cart_id'];
                                $item_id       = $rowCartid['item_id'];
                                $qItem         = query_execute("SELECT discount,price,selling_price FROM shop_items WHERE item_id='" . $rowCartid["item_id"] . "'");
                                $rowItem       = mysql_fetch_array($qItem);
                                $selling_price = $rowItem['selling_price'];
                                $price         = $rowItem['price'];
                                if ($selling_price <= $price) {
                                    
                                    $cartupdate = query_execute("UPDATE shop_cart SET item_discount=item_totalprice*$per WHERE  cart_id = '$cart_id'");
                                }
                            }
                            
                        } else if ($rowType["discount_type"] == 'itemspecific') {
                            
                            $secCart1 = query_execute("SELECT c.item_id,c.item_totalprice as totalprice, c.item_price as price FROM shop_cart as c,shop_discount_items as de WHERE c.cart_order_id = '$order_id' AND c.item_id=de.items_item_id AND  de.discount_discount_id = '" . $rowType["discount_id"] . "'");
                            
                            $totalpricespecific = '0';
                            //$rowSpecific1 = mysql_fetch_array($secCart1);
                            while ($rowSpecific1 = mysql_fetch_array($secCart1)) {
                                $totalpricespecific = $rowSpecific1['totalprice'] + $totalpricespecific;
                                $item_id12[]        = $rowSpecific1['item_id'];
                            }
                            $itemid12 = array_unique($item_id12);
                            
                            
                            if ($rowType["discount_amount_type"] == 'percentage') {
                                $order_discount_amount = (($totalpricespecific) / 100) * $rowType["discount_percentage"];
                                $per                   = ($rowType["discount_percentage"] / 100);
                            } else { // if type is amount
                                
                                $order_discount_amount = $rowType["discount_percentage"];
                                
                                $per = $order_discount_amount / ($totalpricespecific);
                            }
                            
                            foreach ($itemid12 as $dicoitem_id) {
                                
                                $secCart    = query_execute("SELECT item_id,cart_id FROM shop_cart WHERE cart_order_id = '$order_id' AND item_id='$dicoitem_id'");
                                $rowCartid  = mysql_fetch_array($secCart);
                                $cart_id    = $rowCartid['cart_id'];
                                $item_id    = $rowCartid['item_id'];
                                $cartupdate = query_execute("UPDATE shop_cart SET item_discount=item_totalprice*$per WHERE  cart_id = '$cart_id'");
                            }
                            
                            
                            
                            
                        }
                        
                        else {
                            
                            if ($rowType["discount_amount_type"] == 'percentage') {
                                $order_discount_amount = (($rowOrder["order_items_amount"]) / 100) * $rowType["discount_percentage"];
                                $per                   = $rowType["discount_percentage"] / 100;
                                $secCart               = query_execute("SELECT item_id,cart_id FROM shop_cart WHERE cart_order_id = $order_id");
                                while ($rowCartid = mysql_fetch_array($secCart)) {
                                    $cart_id       = $rowCartid['cart_id'];
                                    $item_id       = $rowCartid['item_id'];
                                    $qItem         = query_execute("SELECT discount,price,selling_price FROM shop_items WHERE item_id='$item_id'");
                                    $rowItem       = mysql_fetch_array($qItem);
                                    $selling_price = $rowItem['selling_price'];
                                    $price         = $rowItem['price'];
                                    if ($selling_price <= $price) {
                                        $cartupdate = query_execute("UPDATE shop_cart SET item_discount=item_totalprice*$per WHERE  cart_id = '$cart_id'");
                                    }
                                }
                            }
                            if ($rowType["discount_amount_type"] == 'amount') {
                                
                                $order_discount_amount = $rowType["discount_percentage"];
                                $item_amount           = $rowOrder["order_items_amount"];
                                $per                   = $order_discount_amount / $item_amount;
                                $secCart               = query_execute("SELECT item_id,cart_id FROM shop_cart WHERE cart_order_id = $order_id");
                                while ($rowCartid = mysql_fetch_array($secCart)) {
                                    $cart_id       = $rowCartid['cart_id'];
                                    $item_id       = $rowCartid['item_id'];
                                    $qItem         = query_execute("SELECT discount,price,selling_price FROM shop_items WHERE item_id='$item_id'");
                                    $rowItem       = mysql_fetch_array($qItem);
                                    $selling_price = $rowItem['selling_price'];
                                    $price         = $rowItem['price'];
                                    if ($selling_price <= $price) {
                                        $cartupdate = query_execute("UPDATE shop_cart SET item_discount=item_totalprice*$per WHERE  cart_id = '$cart_id'");
                                    }
                                }
                            }
                        } // end else----------------------------------------------------------------------
                        
                        $order_amount = ($rowOrder["order_items_amount"] + $order_shipment_amount) - $order_discount_amount;
                        $secOrder2 = query_execute("SELECT item_shipping_amount FROM shop_order WHERE order_id = $order_id");
            $rowOrder2 = mysql_fetch_array($secOrder2);
            
            $order_amount = ($rowOrder1["order_items_amount"] + $order_shipment_amount+$rowOrder2["item_shipping_amount"]);
                        $upOrder = query_execute("UPDATE shop_order SET order_shipment_amount='$order_shipment_amount',order_method='$payment', order_discount_amount='$order_discount_amount', order_discount_id='" . $rowType["discount_id"] . "', order_discount_percentage='" . $rowType["discount_percentage"] . "', order_amount='$order_amount', order_method='$payment', mode='$payment' WHERE order_id='$order_id'");
                        
                    } // end order limit---------------------------------------------------------------
                } // end if disc----------------------------------------------------------------------
            } // end if calc Discount------------------------------------------------------------------	
        } // End if no Discount code---------------------------------------------------------------
        
        if ($calc == 0 || $disc == 0) {
            $secOrder1 = query_execute("SELECT order_items_amount,item_shipping_amount FROM shop_order WHERE order_id = $order_id");
            $rowOrder1 = mysql_fetch_array($secOrder1);
            
            $order_amount = ($rowOrder1["order_items_amount"] + $order_shipment_amount+$rowOrder1["item_shipping_amount"]);
            $upOrder      = query_execute("UPDATE shop_order SET order_shipment_amount='$order_shipment_amount', order_amount='$order_amount', order_discount_amount='', order_discount_id='', order_discount_percentage='' WHERE order_id='$order_id'");
            
        }
    }
    return $discount_msg;
} 
}
// function MobcreateOrder($arrayOrderData){
// 	if($arrayOrderData['order_id']){
// 		$u_email = $arrayOrderData["u_email"];
// 		$name = $arrayOrderData["name"];
// 		$address1 = $arrayOrderData["address1"];
// 		$city=$arrayOrderData["city"];
// 		$state=$arrayOrderData["state"];
// 		$pin=$arrayOrderData["pin"];
// 		$country_name=$arrayOrderData["country_name"];
// 		$mobile1=$arrayOrderData["mobile1"];
// 		$ship_name=$arrayOrderData["ship_name"];
// 		$ship_address1=$arrayOrderData["ship_address1"];
// 		$ship_city=$arrayOrderData["ship_city"];
// 		$ship_state=$arrayOrderData["ship_state"];
// 		$other_ship_state=$arrayOrderData["other_ship_state"];
// 		$ship_pin=$arrayOrderData["ship_pin"];
// 		$ship_country=$arrayOrderData["ship_country"];
// 		$ship_phone1=$arrayOrderData["ship_phone1"];
// 		$payment=$arrayOrderData["payment"];
// 		$petname=$arrayOrderData["petname"];
// 		$breedname=$arrayOrderData["breedname"];
// 		$discount_code=$arrayOrderData["discount_code"];
// 		$discount_percentage=$arrayOrderData["discount_percentage"]; 
// 		$order_items_amount=$arrayOrderData["order_items_amount"];
// 		$order_discount_amount=$arrayOrderData["order_discount_amount"];
// 		$order_discount_id=$arrayOrderData["order_discount_id"];
// 		$order_amount=$arrayOrderData["order_amount"];
// 		$referralprice=$arrayOrderData["referralprice"];
// 		//$referralprice=$arrayOrderData["referralprice"];
// 		$order_shipment_amount=$arrayOrderData["order_shipment_amount"];
// 		$tc=$arrayOrderData["tc"];
// 		$ContinueCheckout=$arrayOrderData["ContinueCheckout"];
// 		$addresschq = $arrayOrderData["addresschq"];
// 		$order_id=$arrayOrderData["order_id"];
// 		$newname=$arrayOrderData["newname"];
// 		$ChckBxShowShipping=$arrayOrderData["ChckBxShowShipping"];
		
// 		$userid=$arrayOrderData["userid"];
// 		$session_id=$arrayOrderData["session_id"];
// 		$ip=$arrayOrderData["order_user_ip"];
		
// 	$name=addslashes($name);
// 	$address1=addslashes($address1);
// 	$ship_name=addslashes($ship_name);
// 	$ship_address1=addslashes($ship_address1);
	
// 	$countryv = explode(";", $country_name);
// 	$cityv = explode(";", $city);
// 	/*if($cityv[0]=='-1'){
// 		$cityv[1]=$other_city;
// 	}*/
// 	$cityv[1]=$city;
// 	$statev = explode(";", $state);
	
// 	if($userid=='Guest'){
// 		// if User is Guest but entered a user email that is registered with us
// 		$getUsersEmailDetail = query_execute("SELECT ID, userid FROM users WHERE u_email = '$u_email'");
// 		$u_id = $getUsersEmailDetail['ID'];
// 		$totuseremail = mysql_num_rows($getUsersEmailDetail);
// 		if($totuseremail==0){
// 		// if User is Guest but entered a user email that is registered with us----------------------------END
		
// 			//generate random varchar data..................................................
// 			$chars = '0123456789ABCDEFGHIJabchefghijklmnopqrstuvwyzKLMNOPQRSTUVWXYZ';
// 			$length = 8;
			
// 			$max_i = strlen($chars)-1;
// 			$author_code = '';
// 			for($i = 0; $i < $length; $i++){
// 				$author_code .= $chars{mt_rand(0,$max_i)};
// 			}
// 			//generate random varchar data.............................End.................
			
// 			// Send email for create account-------------------------------------------------------------------------------------
// 			/*$mailSubject = "DogSpot - Account created - Explore your new account Now!";
// 			$toEmail = '"'.$u_email.'" <'.$u_email.'>';
// 			$mailBody = '<p> Hi '.$name. ',</p>
// 	We have created a new DogSpot account for you. A DogSpot account gives you a personalized shopping experience.<br>
// 	<br>
// 	Your account details are:<br>
// 	Email Id for login: '.$u_email.'<br>
// 	Password: '.$author_code.'<br>
// 	<br>
// 	---------------------------------------------------------------------<br>
// 	DogSpot.in<br>
// 	Spot for all your Dog needs. The Store That Delivers at Your Door.<br>
// 	---------------------------------------------------------------------  ';
// 			$headers  = 'MIME-Version: 1.0' . "\r\n";
// 			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
// 			$headers .= 'From: DogSpot <noreply@dogspot.in>' . "\r\n";
// 			$headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";
// 			$headers .= 'Reply-To: DogSpot <noreply@dogspot.in>'. "\r\n";
// 			$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
// 			$headers .= "Organization: DogSpot\r\n";
		
// 			if(mail($toEmail, $mailSubject, $mailBody, $headers, 
// "-f contact@dogspot.in mybounceemail@dogspot.in")){
// 				//echo"Send Mail";
// 			 }*/
// 	// Send email for create account END-----------------------------------------------------------------------------------				
	
	
			
// 			$name = trim($name); //NKS
// 			$address1 = trim($address1); //NKS
// 			$address2 = trim($address2); //NKS
// 			$resultinsert = query_execute("INSERT INTO users (userid, u_password, u_email, f_name, address1, address2, city, city_id, city_nicename, state, state_id, state_nicename, country_code, country_name, country_nicename, pin, phone1, mobile1, updatenews, c_date, current_visit, last_visit, a_code, auth, user_status, user_ip) 
// 						VALUES('', '$author_code', '$u_email', '$name', '$address1', '$address2', '$cityv[1]', '$cityv[0]', '$cityv[2]', '$statev[1]', '$statev[0]', '$statev[2]', '$countryv[0]', '$countryv[1]', '$countryv[2]', '$pin', '$phone1', '$mobile1', 'Y', NULL, NULL, NULL, '$author_code', '0', '1', '$ip')");
			
// 			$u_id = mysql_insert_id();
// 			$user_imap_email = "$u_id@m.dogspot.in";
// 			$updateImap = query_execute("UPDATE users SET imapmail = '$user_imap_email', userid='$u_id' WHERE ID = '$u_id'");
// 			$userid=$u_email;
			
			
			
// 			/* Activation Mail On 17-03-2015 */
				
// 			   $toEmail = $u_email;
// 			   $mailSubject = "Kindly Activate Your DogSpot  account!!!";
// 			   $mailBody    = '<p>Dear <strong>' . $name . '</strong><br><br></p>
// 		<p>Welcome you to <a href="https://www.dogspot.in/"><strong>DogSpot</strong></a> family. <br><br></p>';
// 				if($author_code){
// 					$mailBody .= '<p>
// 					To activate your account please verify your email address by pasting the link on your browser
// 					<br><br>
// 					<a href="https://www.dogspot.in/reg-log/verify-email.php?userid=' . $u_id . '&a_code=' . $author_code . '&Activate=Activate">https://www.dogspot.in/reg-log/verify-email.php?userid=' .$u_id. '&a_code=' . $author_code . '&Activate=Activate</a><br><br></p>';        
		
// 					$mailBody .= '<p>For any further assistance please free to call us on +91-9212196633(Mon to Sat from 9:00 am to 7:00 pm).<br><br></p>';
// 				}
				
// 				$mailBody .= '<p>We wish for a fruitful and long term association with you.</p><br><br>
// 				<p>Thanks and Regards,<br><br><a href="https://www.dogspot.in/">Team DogSpot</a></p>'; 				
								
// 				$headers = 'MIME-Version: 1.0' . "\r\n";
// 				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";        
// 				// Additional headers
// 				$headers .= 'From: DogSpot <contact@dogspot.in>' . "\r\n";
// 				$headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";
// 				if( $arrayEmail['order_transaction_source']=='EBS'){ 
// 					$headers .= 'Bcc: DogSpot <sneha@dogspot.in>' . "\r\n";
// 				}        
// 				$headers .= "Reply-To: DogSpot <contact@dogspot.in>\r\n";
// 				$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
// 				$headers .= "Organization: DogSpot\r\n";
				
// 				if(mail($toEmail, $mailSubject, $mailBody, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")) 															                {
// 					$error_msg = "Error Occur";
// 				}else{
// 					$error_msg = "Successfully sent.";
// 				}
					
// 				/* Activation Mail ENDS */
			
			
			
			
// 		}else{// if User is Guest but entered a user email that is registered with us----------------------------END 
// 			$rowUsersIDEmail = mysql_fetch_array($getUsersEmailDetail);
// 			$u_id = $rowUsersIDEmail["ID"];
// 			$userid = $rowUsersIDEmail["userid"];
// 		}
// 	}else{
// 		$getUsersID = query_execute("SELECT ID FROM users WHERE userid = '$userid'");
// 		$rowUsersID = mysql_fetch_array($getUsersID);
// 		$u_id = $rowUsersID["ID"];
// 	}
	
// 	// COD ------------------
// 	if($payment=='cod'){
// 		if($order_shipment_amount<5){
// 			$order_shipment_amount=50;
// 		}		
// 	}else{
// 		$order_shipment_amount=0;
// 	}// COD ------------------ END
// 	//$order_amount=($order_items_amount+$order_shipment_amount)-$order_discount_amount;
	
	
// 	if($discount_code){
// 	$selecttype=query_execute_row("SELECT discount_type,discount_id,discount_code FROM shop_discount_main WHERE discount_code='$discount_code'");
// 	if($selecttype['discount_type']!='emailonetime' || $selecttype['discount_type']!='newuser'){
// 	applyDiscountphone($order_id, $discount_code, $userid, $payment,$u_email,$ship_phone1);	
// 	}
	
// 	}
// $dog_nicename = trim($petname);
// $petname = trim($petname);
// $dog_nicename = createSlug($dog_nicename);
// $dog_nicename = checkSlugAll('dogs_available', 'dog_nicename', $dog_nicename);
// if($petname && $breedname){
// $selectbreed=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$breedname' ");	
// $selectdogw=query_execute_row("SELECT dog_id FROM `dogs_available` WHERE dog_name='$petname' AND breed_nicename='".$breedname."' AND userid='$userid' AND userid!='Guest' ORDER BY dog_id DESC LIMIT 1 ");
// //echo "SELECT dog_id FROM `dogs_available` WHERE dog_name='$petname' AND dog_nicename='".$breedname."' AND userid='$userid' AND userid!='Guest' ORDER BY dog_id DESC LIMIT 1 ";
// if($selectdogw['dog_id']==''){
// $insert=mysql_query("INSERT INTO `dogs_available`(`userid`, `dog_name`, `dog_breed`, `breed_nicename`, `dog_nicename`,	cdate) VALUES ('$userid','$petname','".$selectbreed['breed_name']."','$breedname','$dog_nicename',NULL)");
// $query_insert_dog=mysql_insert_id();
// }else
// {
// 	$query_insert_dog=$selectdogw['dog_id'];
// }}
	
// 	$queryOrdUp = query_execute("UPDATE shop_order SET u_id='$u_id', userid='$userid', order_shipment_amount='$order_shipment_amount', order_status='1', order_method='$payment' WHERE order_id='$order_id'");
	
// 	$insert=mysql_query("UPDATE shop_cart SET userid='$userid' WHERE cart_order_id='$order_id' ");
// 	$insert=mysql_query("UPDATE shop_cart SET petinfo='$query_insert_dog' WHERE userid='$userid' AND cart_order_status='new' ");	
// 	$CARTPET=query_execute_row("SELECT petinfo FROM shop_cart WHERE cart_order_id='$order_id'");
// if($CARTPET['petinfo'])
// {
// $orderdate_update1 = query_execute("UPDATE shop_order SET petinfo='".$CARTPET['petinfo']."' WHERE order_id = '$order_id'");
// $orderdate_update12 = query_execute("UPDATE dogs_available SET userid='$userid' WHERE dog_id='".$CARTPET['petinfo']."'");
// }
// 	//echo "UPDATE shop_order SET order_amount=order_amount-order_discount_amount WHERE order_id='$order_id'";
	
// $queryOrdUp2 = query_execute("UPDATE shop_order SET order_amount=(order_items_amount+order_shipment_amount+item_shipping_amount-order_discount_amount) WHERE order_id='$order_id'");
// 	//$queryOrdUp = query_execute("UPDATE shop_order SET order_amount=order_amount+49 WHERE order_id='$order_id'");
// /*	$resultinsert = query_execute("INSERT INTO shop_order (u_id, userid, session_id, order_items_amount, order_shipment_amount, order_discount_amount, order_discount_id, order_discount_percentage, order_amount, order_status, order_method, order_c_date, order_user_ip) 
// 				VALUES('$u_id', '$userid', '$session_id', '$order_items_amount', '$order_shipment_amount', '$order_discount_amount', '$order_discount_id', '$discount_percentage', '$order_amount', '1', '$payment',NULL, '$ip')");
// 	$order_id = mysql_insert_id();*/
	
	
// 	$name = trim($name); //NKS
// 	$address1 = trim($address1); //NKS
// 	$address2 = trim($address2); //NKS
// 	$resultinsert = query_execute("INSERT INTO shop_order_address (order_id, userid, address_type_id, address_name, address_email, address_address1, address_address2, address_city, address_city_id, address_city_nicename, address_state, address_state_id, address_state_nicename, address_zip, address_country, address_country_code, address_country_nicename, address_phone1, address_phone2, address_fax) 
// 				VALUES('$order_id', '$userid', '1', '$name', '$u_email', '$address1', '$address2', '$cityv[1]', '$cityv[0]', '$cityv[2]', '$statev[1]', '$statev[0]', '$statev[2]', '$pin', '$countryv[1]','$countryv[0]','$countryv[2]', '$mobile1', '$phone1', '$fax')");
				
// 	if($ChckBxShowShipping=='s'){
// 		$name = trim($name); //NKS
// 		$address1 = trim($address1); //NKS
// 		$address2 = trim($address2); //NKS
// 		$resultinsert = query_execute("INSERT INTO shop_order_address (order_id, userid, address_type_id, address_name, address_email, address_address1, address_address2, address_city, address_city_id, address_city_nicename, address_state, address_state_id, address_state_nicename, address_zip, address_country, address_country_code, address_country_nicename, address_phone1, address_phone2, address_fax) 
// 					VALUES('$order_id', '$userid', '2', '$name', '$u_email', '$address1', '$address2', '$cityv[1]', '$cityv[0]', '$cityv[2]', '$statev[1]', '$statev[0]', '$statev[2]', '$pin', '$countryv[1]', '$countryv[0]', '$countryv[2]', '$mobile1', '', '')");
// 	}else{
// 		$ship_countryv = explode(";", $ship_country);
// 		$ship_cityv = explode(";", $ship_city);
// 		/*if($ship_cityv[0]=='-1'){
// 			$ship_cityv[1]=$other_city_ship;
// 		}*/
// 		$ship_cityv[1]=$ship_city;
// 		$ship_statev = explode(";", $ship_state);
// 		if($ship_statev[0]=='-1'){
// 			$ship_statev[1]=$other_ship_state;
// 		}
// 		$ship_name = trim($ship_name); //NKS
// 			$ship_address1 = trim($ship_address1); //NKS
// 			$ship_address2 = trim($ship_address2); //NKS
// 		$resultinsert = query_execute("INSERT INTO shop_order_address (order_id, userid, address_type_id, address_name, address_email, address_address1, address_address2, address_city, address_city_id, address_city_nicename, address_state, address_state_id, address_state_nicename, address_zip, address_country, address_country_code, address_country_nicename, address_phone1, address_phone2, address_fax) 
// 					VALUES('$order_id', '$userid', '2', '$ship_name', '$u_email', '$ship_address1', '$ship_address2', '$ship_cityv[1]', '$ship_cityv[0]', '$ship_cityv[2]', '$ship_statev[1]', '$ship_statev[0]', '$ship_statev[2]', '$ship_pin', '$ship_countryv[1]', '$ship_countryv[0]', '$ship_countryv[2]', '$ship_phone1', '', '')");
// 	}
// 	 $upCartOrdDate = query_execute("UPDATE shop_order SET order_c_date=NULL WHERE order_id='$order_id'");	
// 	if($order_id){
		
// 		//s$updateCart=query_execute("UPDATE shop_cart SET cart_order_status = 'billed' WHERE cart_order_id = '$order_id'");
// 		/*if($nid=='Guest'){
// 			$updateCart=query_execute("UPDATE shop_cart SET cart_order_id = '$order_id', cart_order_status = 'billed' WHERE session_id = '$session_id' AND cart_order_status='new'");
// 		}else{
// 			$updateCart=query_execute("UPDATE shop_cart SET cart_order_id = '$order_id', cart_order_status = 'billed' WHERE userid = '$userid' AND cart_order_status='new'");
// 		}*/
// 		if($payment=='cod' || $payment=='dd'){
// 			//$x=base64_encode($order_id);

// 			header("Location: /response.php?cart_order_id=$order_id&mode=$payment&session=$session_id");
// 		}else{
// 			if($payment=='pm')
// 			{
// 			header("Location: /checkout-payumoney.php?order_id=$order_id");	
// 			}elseif($payment=='rod')
// 			{
// 				header("Location: /checkout-payupaisa.php?order_id=$order_id");
// 			}
// 			elseif($payment == 'cc' || $payment == 'dc'){
// 				header("Location: /checkout-payu.php?order_id=$order_id&session=$session_id");
// 			}
// 		    elseif($payment == 'ccpaytm'){
// 				header("Location: /checkout-paytm.php?order_id=$order_id");
// 			}
// 			else{
// 				header("Location: /checkout-payu.php?order_id=$order_id&session=$session_id");
// 			}
// 		}
		
// 		//ob_end_flush();
// 		exit();
// 	}			
// 	}else{
// 		if($arrayOrderData['userid']=='Guest'){
// 			$qGetMyCart=query_execute("SELECT SUM(item_totalprice) as order_amount FROM shop_cart WHERE session_id = '".$arrayOrderData['session_id']."' AND cart_order_status='new'");
// 		}else{
// 			$qGetMyCart=query_execute("SELECT SUM(item_totalprice) as order_amount FROM shop_cart WHERE userid = '" . $arrayOrderData['userid'] . "' AND cart_order_status='new' AND dog_show='' AND donate_bag='0'");
// 		}
// 		$rowTP = mysql_fetch_array($qGetMyCart);
		
// 		if($rowTP['order_amount'] > 0){
// 			if($arrayOrderData['userid']=='Guest'){
// 			$qGetMyCart=query_execute("SELECT cart_order_id FROM shop_cart WHERE session_id = '".$arrayOrderData['session_id']."' AND cart_order_status='new'");
// 		}else{
// 			$qGetMyCart=query_execute("SELECT cart_order_id FROM shop_cart WHERE userid = '" . $arrayOrderData['userid'] . "' AND cart_order_status='new' AND dog_show='' AND donate_bag='0' ORDER BY cart_order_id DESC");
// 		}
		
// 		}else
// 		{
// 			$co = query_execute("INSERT INTO shop_order (u_id, userid, session_id, order_items_amount, order_amount,item_shipping_amount, delevery_status, order_c_date, order_user_ip, order_status) 
// 												VALUES('$u_id', '".$arrayOrderData['userid']."', '".$arrayOrderData['session_id']."', '".$rowTP['order_amount']."', '".$rowTP['order_amount']."','$shippingCharge_constant', 'new', NULL, '".$arrayOrderData['order_user_ip']."', 'inc')");
// 			$order_id = mysql_insert_id();
// 		}
// 		if($qGetMyCart['cart_order_id']==0){		
// 			$getUsersID = query_execute("SELECT ID FROM users WHERE userid = '".$arrayOrderData['userid']."'");
// 			$rowUsersID = mysql_fetch_array($getUsersID);
// 			$u_id = $rowUsersID["ID"];
			
// 			$co = query_execute("INSERT INTO shop_order (u_id, userid, session_id, order_items_amount, order_amount,item_shipping_amount, delevery_status, order_c_date, order_user_ip, order_status) 
// 												VALUES('$u_id', '".$arrayOrderData['userid']."', '".$arrayOrderData['session_id']."', '".$rowTP['order_amount']."', '".$rowTP['order_amount']."','$shippingCharge_constant', 'new', NULL, '".$arrayOrderData['order_user_ip']."', 'inc')");
// 			$order_id = mysql_insert_id();
// 			$upd=query_execute("UPDATE shop_order SET order_amount=order_amount+$shippingCharge_constant WHERE order_id='$order_id'");
			

// 		}else{
// 			$rowDo = mysql_fetch_array($qGetMyCart);
// 			$order_id = $rowDo["cart_order_id"];
// 			// $upCartOrdDate = query_execute("UPDATE shop_order SET order_c_date=NULL WHERE order_id='$order_id'");
// 			  $upCartOrdDate = query_execute("UPDATE shop_order SET order_items_amount='".$rowTP['order_amount']."',order_amount=order_items_amount+item_shipping_amount+order_shipment_amount-order_discount_amount WHERE order_id='$order_id'");
// 		}// if Duplicate-----------------------------------
// 			if($arrayOrderData['userid']=='Guest'){
// 				$upCartOrdId=query_execute("UPDATE shop_cart SET cart_order_id='$order_id' WHERE session_id = '".$arrayOrderData['session_id']."' AND cart_order_status='new'");
// 			}else{
// 				$upCartOrdId=query_execute("UPDATE shop_cart SET cart_order_id='$order_id' WHERE userid = '".$arrayOrderData['userid']."' AND cart_order_status='new' AND dog_show='' AND donate_bag='0'");
// 			}		
		
// 	} // end Else----------------------------------
// 	return $order_id;
// }// Function

function gift1($amount,$session_id,$userid){
	
	
	if ($userid == 'brajendra'){
		if ($userid == 'Guest') {
            $qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");
			        } else {
            
            $qGetcart1 = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");
			$count=mysql_num_rows($qGetcart1);
			if($count==0)
			{
			$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");	
			}else
			{
		$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");
			}
			
        }
		$count_cart=mysql_num_rows($qGetcart);
		if($count_cart>0){
					
				if($amount>=500){/*
			$qdata=query_execute("SELECT name,domain_id,nice_name, price,selling_price, weight, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount FROM shop_items WHERE item_id='8253'");
			$item_id=8253;
$qty=1;
		$rowdat = mysql_fetch_array($qdata);
$payment_mode=$rowdat['payment_mode'];
$item_shipping_amount=$rowdat['item_shipping_amount'];
$nice_name=$rowdat['nice_name'];	
if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart["item_totalprice"]+$ItemGtotal;
		$item_shipping_total=($rowdat["item_shipping_amount"]*$item_qty)+$item_shipping_total;
		$item_parent_id=$rowdat["item_parent_id"];
		$itemdomain=$rowdat['domain_id'];
		$totalitem=$totalitem+1;
		// Get Tital option
		$qOptionID=query_execute("SELECT * FROM shop_item_attribute WHERE item_id='$item_id'");
		while($rowOptionID = mysql_fetch_array($qOptionID)){
			$attribute_set_id = $rowOptionID["attribute_set_id"];
			$attribute_value_id = $rowOptionID["attribute_value_id"];
			$arrayAttOp[]="$attribute_set_id-$attribute_value_id";
		}
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id' AND position=1");
		$rowdatM = mysql_fetch_array($qdataM);
		
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		if(file_exists($imagepath)){
			$new_w = 100;
			$new_h = 100;
			$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
		}?>
	<tr >
  	<td width=""><?=$rowdat["name"];?><br>Item No.: <?=$item_id;?><br />Rs. <?=$rowdat["price"]?>
    <p><span style="float:left;"><font size='2'>Cash On Delivery: <? if($payment_mode!='cod'){?><font color="#FF0000">Not Available</font><? }else{?><font color="#33CC33">Available<? }?>
</font></font>
     </span>
     </p><br>
         </td>
     
                                        <td><input name="item_qty[]" type="text" readonly class="ShopQty required" style="text-align:center; width:20px;" id="item_qty[]" value="<?=$qty?>" />
       
       </td>
                                        
                                        
                                        <td>Rs. 0</td>
                                        <td></td>
                                    </tr><?
			}*/}
		}
				
	
				if ($userid == 'Guest') {
            $qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id");
			        } else {
            
            $qGetcart1 = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id");
			$count=mysql_num_rows($qGetcart1);
			if($count==0)
			{
			$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id");	
			}else
			{
		$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id");
			}
			
        }
		
		$a=0;$b=0;
		while($gifttype=mysql_fetch_array($qGetcart))
		{
			$it[]=$gifttype['item_id'].'@@'.$gifttype['item_qty'];
		}
		foreach($it as $freecheck){
			$item=explode("@@",$freecheck);
			//$qGetcart=query_execute_row("SELECT a.category_id FROM shop_item_category as a,shop_items as si WHERE a.item_id = '$freecheck' AND a.category_id!=95  AND a.category_id!=99 AND a.category_id!=91  AND a.category_id!=107 AND a.category_id!=152 AND a.category_id!=153 AND a.category_id!=113 AND a.category_id!=114 AND a.category_id!=115 AND a.category_id!=116 AND a.category_id!=117 AND a.category_id!=166 AND a.category_id!=111 AND a.category_id!=109 AND a.category_id!=110 AND a.category_id!=112 AND a.category_id!=118 AND a.category_id!=124 AND a.category_id!=125 AND a.category_id!=154 AND a.category_id!=127 AND a.category_id!=128 AND a.category_id!=134 AND a.category_id!=139 AND a.category_id!=159 AND si.domain_id!='2' AND si.item_id=a.item_id ");
			$qGetcart=query_execute_row("SELECT item_id,free_item_id FROM add_free_items WHERE status='1' AND  item_id='".$item[0]."' ");
		// $item_id[]=$qGetcart['item_id'];	
		 $free_item_id[]=$qGetcart['free_item_id'];	
		  $item_id_item[]=$qGetcart['item_id'].'@@'.$qGetcart['free_item_id'].'@@'.$item[1];	
		}
		//echo "count".count($cat_id_array);
		foreach($item_id_item as $item_id_add){
			$item_add=explode("@@",$item_id_add);
			$item_id=$item_add[1];
			$qty=$item_add[2];
		if($item_id_item){
		//echo "SELECT name,domain_id,nice_name, price,selling_price, weight, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount FROM shop_items WHERE item_id='$item_id'";
			$qdata=query_execute("SELECT name,domain_id,nice_name, price,selling_price, weight, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount FROM shop_items WHERE item_id='$item_id'");
			//$item_id=87;
		$rowdat = mysql_fetch_array($qdata);
$payment_mode=$rowdat['payment_mode'];
$item_shipping_amount=$rowdat['item_shipping_amount'];
$nice_name=$rowdat['nice_name'];	
if($rowdat['stock_status']=='instock' && $rowdat['item_display_status']!='delete' ){
		$ItemGtotal=$rowMyCart["item_totalprice"]+$ItemGtotal;
		$item_shipping_total=($rowdat["item_shipping_amount"]*$item_qty)+$item_shipping_total;
		$item_parent_id=$rowdat["item_parent_id"];
		$itemdomain=$rowdat['domain_id'];
		$totalitem=$totalitem+1;
		// Get Tital option
		$qOptionID=query_execute("SELECT * FROM shop_item_attribute WHERE item_id='$item_id'");
		while($rowOptionID = mysql_fetch_array($qOptionID)){
			$attribute_set_id = $rowOptionID["attribute_set_id"];
			$attribute_value_id = $rowOptionID["attribute_value_id"];
			$arrayAttOp[]="$attribute_set_id-$attribute_value_id";
		}
		if($item_parent_id=='0'){
			$media_item_id=$item_id;
		}else{
			$media_item_id=$item_parent_id;
		}
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$media_item_id'");
		$rowdatM = mysql_fetch_array($qdataM);
		
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
		if(file_exists($imagepath)){
			$new_w = 100;
			$new_h = 100;
			$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
		}
	?>
		<tr >
  	<td width=""><?=$rowdat["name"];?><br>Item No.: <?=$item_id;?><br />Rs. <?=$rowdat["price"]?>
    <p><span style="float:left;"><font size='2'>Cash On Delivery: <? if($payment_mode!='cod'){?><font color="#FF0000">Not Available</font><? }else{?><font color="#33CC33">Available<? }?>
</font></font>
     </span>
     </p><br>
         </td>
     
                                        <td><input name="item_qty[]" type="text" readonly class="ShopQty required" style="text-align:center; width:20px;" id="item_qty[]" value="<?=$qty?>" />
       
       </td>
                                        
                                        
                                        <td>Rs. 0</td>
                                        <td></td>
                                    </tr> <?
}
			}
			
}
	}
}
function response1($session_id,$userid,$cart_order_id,$order_status,$email)
{
	if ($userid == 'brajendra'){
		
					if ($userid == 'Guest') {
            $qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");
			        } else {
            
            $qGetcart1 = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");
			$count=mysql_num_rows($qGetcart1);
			if($count==0)
			{
			$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");	
			}else
			{
		$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id AND b.domain_id='1'");
			}
			
        }
		$count_cart=mysql_num_rows($qGetcart);
		if($count_cart>0){	
					$check=query_execute_row("SELECT sc.cart_order_id FROM shop_cart as sc,shop_order_address as sos WHERE sc.cart_order_id=sos.order_id AND sos.address_type_id='1' AND sos.address_email='$email' AND sc.item_id='8253' AND sc.cart_order_status=0 ");
					$checkorder=query_execute_row("SELECT order_id FROM shop_order WHERE order_status=0 AND order_id ='".$check['cart_order_id']."' AND  (delevery_status='delivered' OR session_id='$session_id')");
					
					if($checkorder['order_id']==''){
						$check23=query_execute_row("SELECT order_amount FROM shop_order WHERE order_id='$cart_order_id'");
				if($check23['order_amount']>=500){/*
	 $qBuySel        = query_execute("SELECT name,price, selling_price, buying_price,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana,item_shipping_amount FROM shop_items WHERE item_id = '8253'");
	 $item_id=8253;
	 $qty=1;
            $rowBuySel      = mysql_fetch_array($qBuySel);
            $price_item     = $rowBuySel["price"];
            $selling_price  = $rowBuySel["selling_price"];
            $buying_price   = $rowBuySel["buying_price"];
            $tax_vat        = $rowBuySel["tax_vat"];
            $tax_added_type = $rowBuySel["tax_added_type"];
            $tax_added      = $rowBuySel["tax_added"];
            $tax_inharyana  = $rowBuySel["tax_inharyana"];
            $tax_outharyana = $rowBuySel["tax_outharyana"];
			$discount=$selling_price*$qty;
			
			//echo "INSERT INTO shop_cart (userid, session_id, item_id, item_qty, item_price, mrp_price, buying_price,item_discount,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana, item_totalprice, cart_c_date, cart_user_id,donate_bag) VALUES ('$orderuserid', '$session_id', '8253', '1', '0', '$selling_price', '$buying_price','0','$tax_vat','$tax_added_type','$tax_added','$tax_inharyana','$tax_outharyana', '0', NULL, '$ip', '2')";
if($item_id!=0){
	$qItem           = query_execute("INSERT INTO shop_cart (userid, session_id, item_id, item_qty, item_price, mrp_price, buying_price,item_discount,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana, item_totalprice, cart_c_date, cart_user_id,donate_bag) VALUES ('$userid', '$session_id', '$item_id', '$qty', '0', '$selling_price', '$buying_price','$discount','$tax_vat','$tax_added_type','$tax_added','$tax_inharyana','$tax_outharyana', '0', NULL, '$ip', '2')");
	$new_cart_id=mysql_insert_id();
	$orderStat = query_execute("UPDATE shop_cart SET cart_order_id='$cart_order_id',cart_order_status='$order_status' WHERE cart_id='$new_cart_id'");
	 $upOrderfree1     = query_execute_row("SELECT order_discount_amount,order_items_amount FROM shop_order WHERE order_id='$cart_order_id'");
	 $disc=$upOrderfree1['order_discount_amount']+ $selling_price*$qty;
	 //$orderStat = query_execute("UPDATE shop_order SET order_discount_amount='$disc'  WHERE order_id='$cart_order_id'");
	 $selling_price23=$upOrderfree1['order_items_amount']+ $selling_price*$qty;
$orderStat = query_execute("UPDATE shop_order SET order_discount_amount='$disc',order_items_amount='$selling_price23' WHERE order_id='$cart_order_id'");				
		
			

				}
				*/}
				}
		}
	
	if ($userid == 'Guest') {
		//echo "SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id";
            $qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id");
			        } else {
            
            $qGetcart1 = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id");
			$count=mysql_num_rows($qGetcart1);
			if($count==0)
			{
			$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.session_id = '$session_id' AND a.cart_order_status='new' AND a.item_id=b.item_id");	
			}else
			{
		$qGetcart = query_execute("SELECT a.cart_id,a.item_id,b.domain_id,a.item_qty  FROM shop_cart as a,shop_items as b WHERE a.userid = '$userid' AND a.cart_order_status='new' AND a.item_id=b.item_id");
			}
			
        }
			
           // $qGetcart = query_execute("SELECT item_id,item_qty FROM shop_cart  WHERE cart_order_id='$cart_order_id'");
			        
		
		$a=0;$b=0;
		while($gifttype=mysql_fetch_array($qGetcart))
		{
			$it[]=$gifttype['item_id'].'@@'.$gifttype['item_qty'];
		}
		foreach($it as $freecheck){
		$item=explode("@@",$freecheck);
			//$qGetcart=query_execute_row("SELECT a.category_id FROM shop_item_category as a,shop_items as si WHERE a.item_id = '$freecheck' AND a.category_id!=95  AND a.category_id!=99 AND a.category_id!=91  AND a.category_id!=107 AND a.category_id!=152 AND a.category_id!=153 AND a.category_id!=113 AND a.category_id!=114 AND a.category_id!=115 AND a.category_id!=116 AND a.category_id!=117 AND a.category_id!=166 AND a.category_id!=111 AND a.category_id!=109 AND a.category_id!=110 AND a.category_id!=112 AND a.category_id!=118 AND a.category_id!=124 AND a.category_id!=125 AND a.category_id!=154 AND a.category_id!=127 AND a.category_id!=128 AND a.category_id!=134 AND a.category_id!=139 AND a.category_id!=159 AND si.domain_id!='2' AND si.item_id=a.item_id ");
			$qGetcart=query_execute_row("SELECT item_id,free_item_id FROM add_free_items WHERE status='1' AND  item_id='".$item[0]."' ");
		// $item_id[]=$qGetcart['item_id'];	
		 $free_item_id[]=$qGetcart['free_item_id'];	
		  $item_id_item[]=$qGetcart['item_id'].'@@'.$qGetcart['free_item_id'].'@@'.$item[1];	
		}
		//echo "count".count($cat_id_array);
		foreach($item_id_item as $item_id_add){
			$item_add=explode("@@",$item_id_add);
			$item_id=$item_add[1];
			$qty=$item_add[2];
		if($item_id_item){
	   $qBuySel        = query_execute("SELECT name,price, selling_price, buying_price,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana,item_shipping_amount FROM shop_items WHERE item_id = '$item_id'");
            $rowBuySel      = mysql_fetch_array($qBuySel);
            $price_item     = $rowBuySel["price"];
            $selling_price  = $rowBuySel["selling_price"];
            $buying_price   = $rowBuySel["buying_price"];
            $tax_vat        = $rowBuySel["tax_vat"];
            $tax_added_type = $rowBuySel["tax_added_type"];
            $tax_added      = $rowBuySel["tax_added"];
            $tax_inharyana  = $rowBuySel["tax_inharyana"];
            $tax_outharyana = $rowBuySel["tax_outharyana"];
			$discount=$selling_price*$qty;
//			echo "INSERT INTO shop_cart (userid, session_id, item_id, item_qty, item_price, mrp_price, buying_price,item_discount,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana, item_totalprice, cart_c_date, cart_user_id,donate_bag) VALUES ('$orderuserid', '$session_id', '87', '1', '0', '$selling_price', '$buying_price','0','$tax_vat','$tax_added_type','$tax_added','$tax_inharyana','$tax_outharyana', '0', NULL, '$ip', '2')";
if($item_id!=0){
	$qItem           = query_execute("INSERT INTO shop_cart (userid, session_id, item_id, item_qty, item_price, mrp_price, buying_price,item_discount,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana, item_totalprice, cart_c_date, cart_user_id,donate_bag) VALUES ('$userid', '$session_id', '$item_id', '$qty', '0', '$selling_price', '$buying_price','$discount','$tax_vat','$tax_added_type','$tax_added','$tax_inharyana','$tax_outharyana', '0', NULL, '$ip', '2')");
	$new_cart_id=mysql_insert_id();
	$orderStat = query_execute("UPDATE shop_cart SET cart_order_id='$cart_order_id',cart_order_status='$order_status' WHERE cart_id='$new_cart_id'");
	 $upOrderfree1     = query_execute_row("SELECT order_discount_amount,order_items_amount FROM shop_order WHERE order_id='$cart_order_id'");
	 $disc=$upOrderfree1['order_discount_amount']+ $selling_price*$qty;
	 //$orderStat = query_execute("UPDATE shop_order SET order_discount_amount='$disc'  WHERE order_id='$cart_order_id'");
	 $selling_price23=$upOrderfree1['order_items_amount']+ $selling_price*$qty;
$orderStat = query_execute("UPDATE shop_order SET order_discount_amount='$disc',order_items_amount='$selling_price23' WHERE order_id='$cart_order_id'");
$queryOrdUp2 = query_execute("UPDATE shop_order SET order_amount=(order_items_amount+order_shipment_amount+item_shipping_amount-order_discount_amount) WHERE order_id='$cart_order_id'");
}
	
		
}
}
	}
}
function showcartmobile($order, $user, $session, $amount)
{
	if($user=='Guest'){
	$qGetMyCartcheck=query_execute("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price,sc.item_discount,sc.item_totalprice FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='0' ");

}else{

	$qGetMyCartcheck=query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status
FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id
WHERE sc.userid =  '$user'
AND sc.cart_order_status =  'new'
AND si.stock_status =  'instock'
AND si.item_display_status !=  'delete'
AND (
sc.item_price !=  '0'
OR sc.item_id =  '1283'
) AND sc.donate_bag='0'");
}
while($rowMyCartcheck = mysql_fetch_array($qGetMyCartcheck)){
		$item_idcheck = $rowMyCartcheck["item_id"];
		$cart_idcheck=$rowMyCartcheck["cart_id"];
		$item_qtycheck=$rowMyCartcheck["item_qty"];
		if($user=='nareshsingh')
		{
			echo "SELECT name,domain_id, price,tag,feature, weight,nice_name,selling_price, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount,sku FROM shop_items WHERE item_id='$item_idcheck'";
		}
		$qdatacheck=query_execute("SELECT name,domain_id, price,tag,feature, weight,nice_name,selling_price, item_parent_id,item_status,stock_status,item_display_status,payment_mode,item_shipping_amount,sku FROM shop_items WHERE item_id='$item_idcheck'");
		$rowdatcheck = mysql_fetch_array($qdatacheck);
$payment_mode=$rowdatcheck['payment_mode'];
$tag=$rowdatcheck['tag'];
$feature=$rowdatcheck['feature'];
$cemantaname1[]=$rowdatcheck['sku'];
$cemantitemid[]=$rowMyCartcheck['item_id'];
$nice_name=$rowdatcheck['nice_name'];
$item_shipping_amount=$rowdatcheck['item_shipping_amount'];
if($rowdatcheck['stock_status']=='instock' && $rowdatcheck['item_display_status']!='delete' )
{
	if($rowMyCartcheck['item_price'] != $rowdatcheck['price'] || $rowMyCartcheck['mrp_price'] != $rowdatcheck['selling_price'])
	{
		if($user=='nareshsingh')
		{
			echo "UPDATE shop_cart SET item_price='".$rowdatcheck['price']."' AND mrp_price='".$rowdatcheck['selling_price']."' WHERE cart_id='".$cart_idcheck."'";
		}
	$qGetMyCartchupdate=query_execute("UPDATE shop_cart SET item_price='".$rowdatcheck['price']."', mrp_price='".$rowdatcheck['selling_price']."',
	item_totalprice='".$rowdatcheck['price']*$item_qtycheck."' WHERE cart_id='".$cart_idcheck."'");	
	}
}
}
   if ($user == 'Guest') {
$qGetMyCart = query_execute("SELECT * FROM shop_cart WHERE session_id = '$session' AND cart_order_status='new' AND donate_bag='0'  AND (item_price!='0' OR item_id='1283') ");
} else {
$qGetMyCart = query_execute("SELECT * FROM shop_cart WHERE userid = '$user' AND cart_order_status='new' AND donate_bag='0' AND (item_price!='0' OR item_id='1283') ");
}  
   
$order_amount  = 0;
        $cart_order_id = 0;
        while ($rowTP = mysql_fetch_array($qGetMyCart)) {
            if ($rowTP['cart_order_id'] != 0) {
                $cart_order_id = $rowTP['cart_order_id'];
            }
            $arrayitem_id[$rowTP['cart_id']] = $rowTP['item_id'];
            $order_amount                    = $order_amount + $rowTP['item_totalprice'];
        }
      
    //echo "SELECT item_id,item_price,mrp_price FROM shop_cart WHERE session_id = '$session' AND cart_order_status='new' AND cart_order_id='0'";
    if ($order == '') {
        if ($user == 'Guest') {
            
            $qGetMyshowCart1 =query_execute("SELECT sc.cart_id,sc.userid,sc.cart_order_id,sc.session_id,sc.item_id,sc.item_qty,sc.item_price,sc.mrp_price , sc.item_discount,sc.item_totalprice FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND sc.donate_bag='0' AND (sc.item_price!='0' OR sc.item_id='1283') ");
            
            $qGetMyCart1 = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc
JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session' AND sc.donate_bag='0' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc.item_id='1283')");
        } else {
            //echo "SELECT item_id,item_price,mrp_price FROM shop_cart WHERE userid = '$user' AND cart_order_status='new'";
            //echo "SELECT item_id,item_price,mrp_price FROM shop_cart WHERE userid = '$user' AND cart_order_status='new'";
            //echo "SELECT * FROM shop_cart WHERE userid = '$user' AND cart_order_status='new'";
			$qGetMyshowCart1 = query_execute("SELECT sc.cart_id, sc.userid, sc.cart_order_id, sc.session_id, sc.item_id, sc.item_qty, sc.item_price, sc.mrp_price, sc.item_discount, sc.item_totalprice, sc.cart_order_status FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$user' AND sc.donate_bag='0' AND sc.cart_order_status = 'new' AND si.stock_status = 'instock' AND si.item_display_status != 'delete' AND ( sc.item_price != '0' OR sc.item_id = '1283' )");
            //$qGetMyshowCart1 = query_execute("SELECT * FROM shop_cart WHERE userid = '$user' AND cart_order_status='new' AND (item_price!='0' OR item_id='1283')");
            $qGetMyCart1     = query_execute("SELECT SUM(sc.item_totalprice) as order_amount,SUM(sc.mrp_price) as mrp_price,SUM(sc.item_discount) as item_discount FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid = '$user' AND sc.cart_order_status = 'new' AND sc.donate_bag='0' AND si.stock_status = 'instock' AND si.item_display_status != 'delete' AND ( sc.item_price != '0' OR sc.item_id = '1283' )");
        }
        $total = mysql_num_rows($qGetMyshowCart1);
        $rowTP = mysql_fetch_array($qGetMyCart1);
?>

<h3>Summary</h3><span>(<?= $total; ?> items)</span>
<?php /*?><a href="/new/shop/new-cart.php" class="modify">Back To Cart</a><?php */?>
<ul class="order_list">
<div class="cart_menus" style="max-height:170px; overflow:auto;"><?
        $totalwithoutorder = '';
        $t = 0;
		 $order_amount  = 0;
        $cart_order_id = 0;
		  
/// echo $totalwithoutorder;
		 gift("".$rowTP['order_amount']."","$session","$user",'shipping');
		?><input type="hidden" name="msgcod" id="msgcod" value="<?=$rowTP['order_amount']?>" /><?
         while ($rowshowTP = mysql_fetch_array($qGetMyshowCart1)) {
            //echo "SELECT order_discount_id FROM shop_order WHERE order_id='".$rowshowTP['cart_order_id']."'";
            $itemdiscountcart = query_execute_row("SELECT order_discount_id FROM shop_order WHERE order_id='" . $rowshowTP['cart_order_id'] . "'");
            //echo "SELECT * FROM shop_items WHERE item_id='".$rowshowTP['item_id']."'";
            $itemshowcart     = query_execute_row("SELECT * FROM shop_items WHERE item_id='" . $rowshowTP['item_id'] . "'");
            $totalwithoutorder += $rowshowTP["mrp_price"] * $rowshowTP['item_qty'];
			//$totalshippingamount += $itemshowcart["item_shipping_amount"] * $rowshowTP['item_qty'];
?>
<table class="payment_page_table" >
<tr>
<th class="width45p">Item</th>
<th class="width10p">Qty</th>
<th class="width18p">Unit Price</th>
<th class="width18p">Sub Total</th>

</tr>
<tr>
<td class="width45p"><label><?= $itemshowcart['name'] ?></label></td>
<td class="width10p"><?= $rowshowTP['item_qty'] ?>
</td>
<td class="width18p"><span class="price"><?
            if ($rowshowTP["mrp_price"] <= $rowshowTP["item_price"]) {
?>
<span style="color:#962327; margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />&nbsp <?= number_format($rowshowTP["item_price"]*$rowshowTP['item_qty'], 0) ?></span>
<?
            } else {
                $t = 1;
?>

<strike style="color:#000; margin-right: 6px;;"><img src="https://www.dogspot.in/new/pix/checkout/rupee_icon.png"><?= number_format($rowshowTP["mrp_price"]*$rowshowTP['item_qty'], 0) ?></strike>
<span style="color:#962327;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />
<?= number_format($rowshowTP["item_price"]*$rowshowTP['item_qty'], 0) ?></span>
<?
            }
?>
</span>
</td>

<td class="width18p">
<span class="price">
<span style="color:#962327; margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" /><?= number_format($totalwithoutorder, 0) ?></span>
</span>
</td>
<?
        }
?>
</tr>
<tr>
<td colspan="3" class="width78p">
<label style="width:100px;">Shipping Charges</label>
</td>
<td class="width18p">
<span class="price" style="width:93px;">
<span style="color:#962327; margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />$shippingCharge_constant
</span>
</span>
</td>
</tr>
 <? if ($t == 1) {?>
 <tr>
<td colspan="3" class="width78p">
<label>Discount</label>
</td>
<td class="width18p">
<span class="price">
<span style="color:#962327; margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />
<?=number_format(($rowTP['item_discount']), 0);
?></span>
</span>
</td>
</tr>
<? }?>
<tr>
<td colspan="3" class="width78p">
<label><strong>Grand Total</strong></label>
</td>
<td class="width22p">
<span class="price">
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />
<?=number_format(($rowTP['item_discount']), 0);
?></span>
</span>
</td>
</tr>
</table>
</div>
</ul>
<?
    } else {
        $secCart   = query_execute("SELECT item_qty,cart_id, item_id,mrp_price, item_price,item_discount FROM shop_cart WHERE cart_order_id = '$order' AND (item_price!='0' OR item_id='1283')");
		 $secCart1   = query_execute("SELECT item_qty,cart_id, item_id,mrp_price, item_price,item_discount FROM shop_cart WHERE cart_order_id = '$order' AND (item_price!='0' OR item_id='1283')");
		 $secCart12donate   = query_execute("SELECT item_qty,cart_id, item_id,mrp_price, item_price,item_discount FROM shop_cart WHERE cart_order_id = '$order' AND (item_price!='0' OR item_id='1283') AND donate_bag='1'");
        $totalsub  = '';
        $totaldisc = '';
        $tot       = mysql_num_rows($secCart);
		$totdonate       = mysql_num_rows($secCart12donate);
?>
<?
        if ($tot > 0) {
?>
<h3>Summary</h3><span>(<?= $tot; ?> items)</span>

<ul class="order_list">
<div class="cart_menus" style="max-height:124px; overflow:auto;">
<?
 while ($rowCartidfree = mysql_fetch_array($secCart1)) {
                $cart_idfree = $rowCartidfree['cart_id'];
				
				$qItem1   = query_execute("SELECT item_id, name, price,selling_price, discount,item_display_status,stock_status FROM shop_items WHERE item_id='" . $rowCartidfree["item_id"] . "'");
                $rowItem1 = mysql_fetch_array($qItem1);
                if ($rowItem1['stock_status'] == 'instock' && $rowItem1['item_display_status'] != 'delete') {
                    $totalsub1  = $rowCartidfree["mrp_price"] * $rowCartidfree["item_qty"] + $totalsub1;
                    $totaldisc1 = $rowCartidfree["item_discount"] + $totaldisc1;
					//$item_shipping_amount = ($rowItem["item_shipping_amount"]*$rowCartid["item_qty"])+$item_shipping_amount ;
					
				}}
 $check12=$totalsub1-$totaldisc1;
 
			 //echo $check;
			 
					$checkwag=query_execute_row("SELECT sc.cart_order_id FROM shop_cart as sc,shop_order_address as sos WHERE sc.cart_order_id=sos.order_id AND sos.address_type_id='1' AND sos.userid='$user' AND sc.item_id='8253' AND sc.cart_order_status=0");
					$checkordertag=query_execute_row("SELECT order_id FROM shop_order WHERE order_status=0 AND order_id='".$checkwag['cart_order_id']."' AND delevery_status='delivered'");
		if($totdonate<1){			
		 gift("$check12","$session","$user",'shipping');
		 if($totaldisc1){
		?><input type="hidden" name="msgcod" id="msgcod" value="<?=$check12?>" /><? }
		 if($checkordertag['order_id']==''){
		 if($totalsub1>=500 && $totaldisc1!='' && $check12<=500)
		 {
			 ?><script>
			// alert('frf');
			//var amount_data1=500-<?=$check12?>;
			//var amount_data=amount_data1.toFixed(2)
			//var data="Add products worth Rs."+amount_data1+" to get FREE Gift for your Pet";
		//	$('#free_discount').text(data);
			// $('#free_discount').css("display","block");
             </script>
			 <?
		 }else
		 {?>
			 <script>
			//  alert('frf1');
			 $('#free_discount').css("display","none");
             </script><?
		 }
		 }
		 }
            while ($rowCartid = mysql_fetch_array($secCart)) {
                $cart_id = $rowCartid['cart_id'];
				
				$qItem   = query_execute("SELECT item_id, name, price,selling_price, discount,item_display_status,stock_status FROM shop_items WHERE item_id='" . $rowCartid["item_id"] . "'");
                $rowItem = mysql_fetch_array($qItem);
                if ($rowItem['stock_status'] == 'instock' && $rowItem['item_display_status'] != 'delete') {
                    $totalsub  = $rowCartid["mrp_price"] * $rowCartid["item_qty"] + $totalsub;
                    $totaldisc = $rowCartid["item_discount"] + $totaldisc;
					//$item_shipping_amount = ($rowItem["item_shipping_amount"]*$rowCartid["item_qty"])+$item_shipping_amount ;
					 $upOrder      = query_execute("UPDATE shop_order SET  order_items_amount='$totalsub',order_discount_amount='$totaldisc',
order_amount=order_items_amount+order_shipment_amount-order_discount_amount WHERE order_id='$order'");
 $upOrder1      = query_execute_row("SELECT order_items_amount,order_discount_amount FROM shop_order WHERE order_id='$order'");
//$check1=$upOrder1['order_items_amount']-$upOrder1['order_discount_amount'];
	
?>       
          

 <li class="order_l_bg"><label><?= snippetwop($rowItem["name"], $length = 30, $tail = "..."); ?><br/>(Qty: <?= $rowCartid['item_qty'] ?>)</label>

<span class="price"><?
                    if ($rowCartid["mrp_price"] > $rowCartid["item_price"]) {
?>
<strike style="color:#000;;margin-right: 6px;;"><img src="https://www.dogspot.in/new/pix/checkout/rupee_icon.png"><?= number_format($rowCartid["mrp_price"]*$rowCartid["item_qty"], 0) ?></strike>
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />
<?= number_format($rowCartid["item_price"]*$rowCartid["item_qty"], 0) ?></span>
<br />
<?
                    } else {
?>
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" /><?= number_format($rowCartid["item_price"]*$rowCartid["item_qty"], 0) ?></span> <?
                    }
?>
</span>
</li>

  
      
   
<?
                } else {
                    $qRemoveItemCart = query_execute("DELETE FROM shop_cart WHERE cart_id='$cart_id'");
                }
            }
			
           
            
	
	
			
            // END While----------------------------------------------------------------------------------	
?>
</div> 
<?
//$upOrder      = query_execute("UPDATE shop_order SET  order_items_amount='$totalsub',order_discount_amount='$totaldisc',
//order_amount=order_items_amount+order_shipment_amount-order_discount_amount WHERE order_id='$order'");
 $upOrder      = query_execute_row("SELECT order_amount FROM shop_order WHERE order_id='$order'");
//$check=$upOrder['order_amount'];

  $checkamount=query_execute_row("SELECT SUM(amount) as total FROM shop_order_coupon WHERE order_id='$order'");
 $checkamount2=query_execute("SELECT * FROM shop_order_coupon WHERE order_id='$order'");
$check=$upOrder['order_amount']-$checkamount['total'];   
$count=mysql_num_rows($checkamount2);       
	
	$iopi=$shippingCharge_constant;
	$o_amount=$check+$iopi;

  $upOrder      = query_execute("UPDATE shop_order SET order_amount='$o_amount',item_shipping_amount='$iopi' WHERE order_id='$order'");
            $secOrderDisp1 = query_execute("SELECT order_discount_amount FROM shop_order WHERE order_id = $order");
            $rowOrderDisp1 = mysql_fetch_array($secOrderDisp1);
			if($rowOrderDisp1['order_discount_amount']==0)
			{
				$upOrder      = query_execute("UPDATE shop_order SET order_discount_id='',order_discount_percentage=''  WHERE order_id='$order'");
			}
			$secOrderDisp = query_execute("SELECT * FROM shop_order WHERE order_id = $order");
            $rowOrderDisp = mysql_fetch_array($secOrderDisp);
            $secDiscType  = query_execute("SELECT discount_amount_type,discount_percentage FROM shop_discount_main WHERE discount_id = '" . $rowOrderDisp["order_discount_id"] . "'");
            $rowDiscType  = mysql_fetch_array($secDiscType);
			
			if($rowDiscType['discount_amount_type']=='percentage')
			{
				$val1=$rowDiscType['discount_percentage'].'%';
			}else
			{
				$val1="<img src='https://www.dogspot.in/new/shop/images/img/rupee_icon1.png' />".$rowDiscType['discount_percentage'];
			}
?>
 <li class="order_l_bg no_bg "><label>Sub Total</label>
                                       <span class="price">
<span style="color:#962327; float:right;margin: 0px">
<?= number_format($totalsub, 0); ?></span></span></li>
                                   <li class="order_l_bg no_bg no_border"><label>COD Charges</label>
<span class="price">
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />
<?= number_format($rowOrderDisp["order_shipment_amount"], 0); ?></span></span></li>
<li class="order_l_bg no_bg no_border"><label style="width:100px;">Shipping Charges</label>
<span class="price" style="width:93px;" >
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" />
$shippingCharge_constant</span></span></li>

 <?
 if($checkamount['total']>0)
 {?>
  <li class="order_l_bg no_bg "><label>Giftcard: <?=$count?></label>
 
 
                                 
<span class="price">
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" /><?= number_format($checkamount['total'], 0);?></span></span></li>
 <?
 }
            if ($rowOrderDisp["order_discount_amount"] != '0' || $rowOrderDisp["order_discount_percentage"] != '0') {
?>
                                   <li class="order_l_bg no_bg "><label>Discount: <?=$val1?></label>
 
 
                                 
<span class="price">
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" /><?= number_format($totaldisc, 0);?></span></span></li><?
            } 

?>

                                   
       
                                 
                                <li class="order_l_bg no_bg no_border tot_bg"><label><strong>Grand Total</strong></label>
                                      <span class="price">
<span style="color:#962327; float:right;margin: 0px"><img src="https://www.dogspot.in/new/shop/images/img/rupee_icon1.png" /><strong><? 

echo number_format($rowOrderDisp["order_amount"], 2);
 ?></strong></span></span></li>
                                  
                                   
</ul><?
            
            
		   }
		}
}
function applyDiscountMsg1($order_id, $discount_code, $userid, $payment, $user_email, $user_phone,$user_pin)
{
    //Discount Type
    //Open 			= 	Email - any,	Product - 	any,	Use time -  unlimited 
    //email 		=	Email -	fix,	Product -	any,	Use time -  unlimited
    //onetime		=	Email -	any, 	Product -	any, 	Use time -	Onetime
    //emailitem		=	Email - fix, 	Product	-	fix,	Use time -	unlimited
    //emailonetime	= 	Email - any, 	Product - 	any,	Use time -	onetime
     $secCart1         = query_execute_row("SELECT order_status FROM shop_order WHERE order_id = $order_id");
	if($secCart1['order_status']=='inc'){
    $discc = 1;
   
    if ($discount_code) {
		if($discount_code=='DAMAGE50'){
			
		}else{
        $secCart         = query_execute("SELECT item_qty, item_id,mrp_price, item_price,item_totalprice FROM shop_cart WHERE cart_order_id = $order_id");
        $totaldisd_price = '0';
        while ($rowCartid = mysql_fetch_array($secCart)) {
            $qItem   = query_execute("SELECT item_id, name, price,selling_price ,discount FROM shop_items WHERE item_id='" . $rowCartid["item_id"] . "'");
            $rowItem = mysql_fetch_array($qItem);
            
            if ($rowItem["price"] < $rowItem["selling_price"]) {
                $discc           = 0;
                $totaldisd_price = $totaldisd_price + ($rowCartid['mrp_price'] * $rowCartid['item_qty']);
                $discount_msg    = 'Discount coupon is not valid on already discounted items.';
            }else
			{
				 $totaldisd_price = $totaldisd_price + ($rowCartid['mrp_price'] * $rowCartid['item_qty']);
			}
        
    } }
	}
    if ($payment == 'rod') {
        $order_shipment_amount = 0;
    } elseif ($payment == 'cod') {
        if ($order_shipment_amount < 5) {
            $order_shipment_amount = 49;
        }
    } else {
        $order_shipment_amount = 0;
    } // COD ------------------ END------------------------------------------------		
    if ($discc == 1) {
        if ($discount_code) {
            $upOrderinc = query_execute("UPDATE shop_order SET order_discount_amount='', order_discount_id='', order_discount_percentage='' WHERE order_id='$order_id'");
            
            
            $disc          = 0;
            $discount_code = strtoupper(trim($discount_code));
			$discountType  = query_execute("SELECT * FROM shop_discount_main WHERE discount_code = '$discount_code'");
            if (mysql_num_rows($discountType) > 0) { 
			// if cide is valid--------------------
                $rowType = mysql_fetch_array($discountType);
                $todayt  = time();
                $startt  = strtotime($rowType["discount_start_date"] . " 00:00:00");
                $endt    = strtotime($rowType["discount_end_date"] . " 23:59:59");
				
				
                if ($todayt >= $startt && $todayt <= $endt) 
				{
                    $calc = 1;
                } //if date true ends
                else {
                    $discount_msg = 'Discount code expired';
                    $calc         = 0;
                } // date check ends 
                if ($rowType["discount_status"] == 'inactive') 
				{
                    $discount_msg = 'Discount code is inactive';
                    $calc         = 0;
                } // check discout status
            }
			 else {
                $discount_msg = 'Discount code is invalid';
                $calc         = 0;
            } // END if Code is invalid-----------------------------------------------------
            
            if ($calc == 1) { // Open====
                if ($rowType["discount_type"] == 'open' || $rowType["discount_type"] == 'onetime') {
					if($discount_code=='GGN20' || $discount_code=='GGMB20' || $discount_code=='GGN15'){
                    $user_check_pincode = query_execute_row("SELECT count(*) as coount FROM shop_courier_pincodes WHERE pincode = '$user_pin' AND city='GURGAON'");
					if($user_check_pincode['coount']=='0'){
						$disc         = 0;
                        $discount_msg = 'Sorry! This Discount Code is applicable only for Customers located in Gurgaon';
						}
					}else{
                   if($discount_code=='DAMAGE50' && $user_email=='vizal.atheya@gmail.com')
						{
							
							
						}elseif($discount_code !='DAMAGE50'){
                    $disc = 1;
						}else
						{
							$disc = 0;
						$discount_msg = 'Discount code is invalid';
						}
					}
                } // Open End=============
                 if ($rowType["discount_type"] == 'mobile') {
					
							$disc = 0;
						$discount_msg = 'Discount code is only applicable on mobile user';
						
                } // Open End=============
                //   Item  Specific======
                if ($rowType["discount_type"] == 'itemspecific') {
					
					
					
					
                    $vcat1        = 0;
                    $vcat2        = 0;
                    $cartprice    = '0';
                    $userSpecific = query_execute("SELECT item_id, item_price, item_qty, cart_id FROM shop_cart WHERE cart_order_id = '$order_id'");
                    while ($rowSpecific = mysql_fetch_array($userSpecific)) {
                        $item_pri      = $rowSpecific['item_price'];
                        $cart_qty      = $rowSpecific['item_qty'];
                        $cart_itemid1  = $rowSpecific['item_id'];
                        $cartne_id     = $rowSpecific['cart_id'];
                        $totalprice    = $item_pri * $cart_qty;
                        $cartprice     = $totalprice + $cartprice;
                        $qItemnew      = query_execute_row("SELECT  price, selling_price, discount FROM shop_items WHERE item_id='$cart_itemid1'");
                        $selling_price = $qItemnew['selling_price'];
                        $price         = $qItemnew['price'];
                        if ($selling_price <= $price) {
                            $cartupdate = query_execute("UPDATE shop_cart SET item_discount='0' WHERE  cart_id = '$cartne_id'");
                        } else {
                            $cartupdate = query_execute("UPDATE shop_cart SET item_discount=$selling_price*item_qty-$price*item_qty WHERE  cart_id = '$cartne_id'");
                        }
                        
                        $Itemcount = query_execute_row("SELECT count(*) as count_items, item_qty, items_item_id FROM shop_discount_items WHERE
	 items_item_id='" . $rowSpecific['item_id'] . "' AND discount_discount_id = '" . $rowType["discount_id"] . "' ");
                        //echo "count".$Itemcount['count_items'];
                        
                        $limitem_id = $Itemcount['items_item_id'];
                        
                        if ($limitem_id == $rowSpecific['item_id']) {
                            $sameitem_id = $limitem_id;
                            
                            $limit_qty = $Itemcount['item_qty'];
                            $cart_qty1 = $cart_qty;
                        }
                        
                        if ($Itemcount['count_items'] == '0') {
                            $vcat2 = $vcat2 + 1;
                        } else {
                            $vcat1 = $vcat1 + 1;
                        }
                    } // while ends
                    if($discount_code=='ANAND30' && $user_email=='anand.vishwanath@anvisinc.com')
					{
						$disc = 1;
					}else if ($vcat1 == '0') {
                        $disc         = 0;
                        $discount_msg = 'Discount code is invalid';
                    } else if ($cart_qty1 > $limit_qty && $limit_qty != '0') {
                        $disc         = 0;
                        $discount_msg = 'This code is applicable only on ' . $limit_qty . ' QTY of item id ' . $sameitem_id . '.';
                    } else if ($cartprice < $rowType["discount_order_amount_limit"]) {
                        $disc         = 0;
                        $discount_msg = 'Order limit is less then ' . $rowType["discount_order_amount_limit"];
                    } else {
						if($discount_code=='ANAND30' && $user_email!='anand.vishwanath@anvisinc.com')
					{
						$disc = 0;
						$discount_msg = 'Invalid user';
					}
						
					else	{
							 $userdiscuntcount1 = query_execute_row("SELECT count(*) as count_order1 FROM shop_order_address as od,shop_order as o
						 WHERE order_discount_id = '" . $rowType["discount_id"] . "' AND o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_status='0' AND o.order_parent_id='0' AND o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone' OR od.address_email!='' )");
						
                        
                        if ($userdiscuntcount1['count_order1'] < $rowType["discount_countlimit"]) {
                            $disc = 1;
                        } else {
                            $discount_msg = 'You have already used this discount';
                            $disc         = 0;
                        }
					}
                    }
                    
                } // Item Specific End================================
                
                
                // Email ========
                if ($rowType["discount_type"] == 'email' || $rowType["discount_type"] == 'emailitem') {
                    $userEmail = query_execute("SELECT ID FROM users WHERE u_email = '" . $rowType["discount_email"] . "' AND userid = '$userid'");
                    if (mysql_num_rows($userEmail) > 0) {
                        $disc = 1;
                    } else {
                        $discount_msg = 'Discount code is invalid';
                        $disc         = 0;
                    }
                } // Email End================
				  // New User ========
                if($rowType["discount_type"] == 'newuser'){
			//echo 'hi';
			//echo "SELECT o.userid,o.order_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_discount_id!='0' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone') ";
		$userEmail1 = query_execute("SELECT o.userid,o.order_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone') ");
			if(mysql_num_rows($userEmail1)==0){
				$disc=1;
			}else{
				$discount_msg = 'This code is valid only for new users or you might have already used the code in your earlier purchase';
				$disc=0;
			}
		
				
		}
                // Email One time================
                if ($rowType["discount_type"] == 'emailonetime') {
                    $vcat = 0;
                   
                    $userEmail12 = query_execute("SELECT o.userid,o.order_id,o.order_discount_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_discount_id!='0' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone') ");
                    
                    while ($roworderids = mysql_fetch_array($userEmail12)) {
                        $user_order = $roworderids['order_id'];
                        $order_dis  = $roworderids['order_discount_id'];
                        
                        if ($rowType["discount_id"] == $order_dis) {
                            $vcat = $vcat + 1;
                        } else {
                            $vcat = 0;
                        }
                    }
?>
	<input type="hidden" value="<?= $userEmail12['userid'] ?>" name="userid_email" id="userid_email"/>
    <input type="hidden" value="<?= $vcat ?>" name="email_counter" id="email_counter"/>
    
	<?
                    if ($vcat >= $rowType["discount_countlimit"]) {
                        $disc         = 0;
                        $discount_msg = 'You have already used this discount';
                        
                    } else {
                        $disc = 1;
                    }
                } // emailonetime =======================================================================================================
                
                
                // if Discount Type is Campaign
                if ($rowType["discount_type"] == 'campaign') {
                    
                    $vcat        = '0';
                    $userEmail12 = query_execute("SELECT o.userid,o.order_id,o.order_discount_id FROM shop_order_address as od,shop_order as o WHERE o.order_id=od.order_id AND  od.address_type_id='1' AND o.order_discount_id!='0' AND o.order_status='0' AND o.order_parent_id='0'  AND  o.order_status!='inc' AND (od.address_email ='$user_email' OR od.address_phone1='$user_phone')");
                    
                    while ($roworderids = mysql_fetch_array($userEmail12)) {
                        $order_dis = $roworderids['order_discount_id'];
                        
                        $userdis      = query_execute_row("SELECT discount_name FROM shop_discount_main  WHERE  discount_id = '$order_dis' ");
                        $campaignname = $userdis['discount_name'];
                        
                        $disname      = query_execute_row("SELECT discount_name FROM shop_discount_main  WHERE  discount_id = '" . $rowType["discount_id"] . "' ");
                        $discountname = $disname['discount_name'];
                        //echo $campaignname."sasd".$discountname."<br>";
                        if ($campaignname == $discountname) {
                            $vcat = $vcat + 1;
                        } else {
                            $vcat = 0;
                        }
                        
                    }
                    if ($vcat >= $rowType["discount_countlimit"]) {
                        $disc         = 0;
                        $discount_msg = 'You have already used a discount coupon for same campaign';
                        
                    } else {
                        $disc = 1;
                    }
                }
                
                if ($disc == 1) {
                    
                    $secOrder = query_execute("SELECT * FROM shop_order WHERE order_id = $order_id");
                    $rowOrder = mysql_fetch_array($secOrder);
                    
                    if ($rowType["discount_order_amount_limit"] > 0 && $rowType["discount_order_amount_limit"] >$totaldisd_price) {
                        $discount_msg = 'Order limit is less then ' . $rowType["discount_order_amount_limit"];
                        $disc         = 0;
                    }
                    /*else if($rowOrder['order_discount_amount']<$rowOrder['order_items_amount']){
                    $discount_msg = 'Order Discount is less then '.$rowOrder["order_items_amount"];
                    $disc=0;
                    }*/
                    
                       } // end if disc----------------------------------------------------------------------
            } // end if calc Discount------------------------------------------------------------------	
        } // End if no Discount code---------------------------------------------------------------
        
       
    }
    return $discount_msg;
}
}
 function format_num($num, $precision = 2) {
   if ($num >= 1000 && $num < 1000000) {
    $n_format = number_format($num/1000,$precision).'K';
    } else if ($num >= 1000000 && $num < 1000000000) {
    $n_format = number_format($num/1000000,$precision).'M';
   } else if ($num >= 1000000000) {
   $n_format=number_format($num/1000000000,$precision).'B';
   } else {
   $n_format = $num;
    }
  return $n_format;
  } 

function stripallslashes($string) { 
	return $string=preg_replace('/\\\\+/','',$string);
}
?>