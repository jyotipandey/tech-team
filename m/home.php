<?php 
include_once("constants.php");
include_once("database.php");
//include_once(SITEMAIN_URL."/shop/functions.php");
///include_once(SITEMAIN_URL."/shop/arrays/arrays.php");
include_once(SITEMAIN_URL."/functions.php");
include_once(SITEMAIN_URL."/functions2.php");
include_once(SITEMAIN_URL.'/session.php');
$sitesection = 'HOME';
if($utm_source=="sbibuddy")
{
 setcookie("sbibuddy", 'sbi', time()+3600*24*30, "/"); /* expire in 30 Days */
}
?>
<? header("Content-type: text/html; charset=iso-8859-1"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>DogSpot.in-Online Pet Supplies Store | Shop for Dog,Cat,Birds Products</title>
<meta name="keywords" content="DogSpot.in, Online Pet Supplies Store, Dog Store, Cat shop, Birds products, pet shopping India, puppies shop, cat supplies, dog shop online." />
<meta name="description" content="DogSpot.in Pet Supplies Store - India's best online shopping site for pets supplies, accessories & products for pets. Shop for Dog, Cat, Birds and small pets at great price from anywhere." />
<link rel="canonical" href="https://www.dogspot.in" />
<link rel="amphtml" href="https://www.dogspot.in/amp/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/" />
<?php require_once(SITEMAIN_URL .'/common/script.php');?>

<style>
.product-slider-link,.ui-btn{text-overflow:ellipsis;white-space:nowrap}.ui-mobile-viewport,html{-webkit-text-size-adjust:100%}.sticky-bar,.sticky-height{height:30px}.sale-text,.start-countdown{display:inline-block}.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333}#header,.ui-btn{text-align:left}.sticky-bar a,.sticky-bar a:hover{text-decoration:underline}.sticky-left{width:96%;float:left}.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px}.sale-text{font-size:12px}.sale-text a{color:#fc3;text-decoration:none}.ds-home-link{padding-top:30px!important}.h-m-fotter-sec .h-m-fotter h3,.h-m-fotter-sec-bottom ul li,.petstories-h-sec h2{text-transform:uppercase}.ui-btn{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;border:1px solid #ccc;padding-top:5px;padding-bottom:5px;border-radius:0!important}.ui-btn,body,button,input{font-size:1em;line-height:1.3;font-family:lato,sans-serif}.ui-input-text input{color:inherit;text-shadow:inherit}.ui-btn.ui-corner-all,.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em}.ui-corner-all{-webkit-background-clip:padding;background-clip:padding-box}.ui-body-a{border-width:1px;border-style:solid;background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3}.ui-btn:focus{outline:0}.ui-mobile,.ui-mobile body{height:99.9%}.ui-page{padding:0;margin:0;outline:0}.ui-mobile a img{border-width:0}.ui-mobile-viewport{margin:0;overflow-x:visible;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent}body.ui-mobile-viewport{overflow-x:hidden}.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0}.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden}@media screen and (orientation:portrait){.ui-mobile .ui-page{min-height:420px}}@media screen and (orientation:landscape){.ui-mobile .ui-page{min-height:300px}}.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0}.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em}.ui-btn{text-decoration:none!important;font-size:13px;margin:.5em 0;position:relative;overflow:hidden;cursor:pointer;user-select:none;padding-left:5px}#header div,#morepopup li a,.article-det-sec a,.h-m-fotter-sec .h-m-fotter ul li a,.product-slider-link a,.ui-link,ins{text-decoration:none}button.ui-btn{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-appearance:none;-moz-appearance:none}button.ui-btn::-moz-focus-inner{border:0}.ui-screen-hidden{display:none!important}.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%}.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099}.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important}.ui-popup{position:relative;padding:10px 10px 0}.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff}.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden}.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px)}.ui-input-text input{padding:.4em;line-height:1.4em;display:block;width:100%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;outline:0;background:0 0;border:1px solid #ccc;font-size:14px;margin:0;min-height:2.2em;text-align:left}.ui-input-text input::-moz-placeholder{color:#636363}.ui-input-text input:-ms-input-placeholder{color:#aaa}.ui-input-text input::-ms-clear{display:none}.ui-input-text input:focus{-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none}.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block}#header,#morepopup li:last-child{border-bottom:0}.ui-panel-page-container{overflow-x:visible}.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none}.panelicon,.purchase-product-cart{position:relative}.ui-panel-dismiss-open{display:block}.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease}.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0)}.ui-panel-position-left{left:-17em}.ui-panel-open.ui-panel-position-left{left:-1px}.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em}#header{height:50px;padding:12px;border-top:0;background:#6c9d06}#header .fa{font-size:18px;color:#fff}#header div,.panelicon{display:inline-block}#header div{color:#f7422d}.panelicon{height:19px;left:0;margin-top:0!important;top:0;width:18px}.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px}.ui-link{color:#333}.slider1-container{width:100%;height:auto}.slider1-container img{width:100%}.green_clr{font-family:gobold;margin-left:8px;color:#fff!important;font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335}*,.product-slider-link a,.recently_sec h4{font-family:lato,sans-serif}#morepopup li,#morepopup li a{color:#323232}#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px}#morepopup li img{margin-right:8px;vertical-align:top}#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px}#morepopup-screen{background:0 0}.content-search{padding-bottom:20px}.content-search form{margin:0 auto;position:relative;top:5px;width:95%}.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%}.btn-sub-sbmt,div.dsdropdown{position:relative}*{margin:0;padding:0;box-sizing:border-box}.cartsearch_blk a img{width:18px;height:19px}.content-search{width:100%;float:left;margin-top:0!important;padding:5px 0;background:#6c9d06}.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0}.petstories-h-sec{float:left;width:96%;margin-left:2%;margin-right:2%}.petstories-h-sec h2{width:100%;font-size:16px;letter-spacing:.6px;margin-top:15px}.article-det-sec{float:left;width:100%;margin-top:15px}.article-det-sec a{color:#333}.article-det-sec img{max-width:100%;height:auto}.article-det-sec h3{font-size:14px;font-weight:700;margin:10px 0 0;float:left;width:100%}.article-det-sec p{font-size:14px;color:#888;margin-top:5px;float:left;width:100%}.subscribe-sec{float:left;width:100%;margin-top:15px;background:#f9f9f9;padding:15px 5%}.subscribe-sec input{border:1px solid #ddd!important;height:45px;background:#fff}.btn-sub-sbmt{float:right;margin-right:6%;margin-top:-37px;background:#fff;border:none}.bank-logos,.h-m-fotter-sec{float:left;width:96%;margin-left:2%;margin-right:2%;margin-top:15px}.h-m-fotter-sec .h-m-fotter{float:left;width:100%;margin-bottom:15px}.h-m-fotter-sec .h-m-fotter h3{font-size:13px;margin-bottom:10px;font-weight:700}.h-m-fotter-sec .h-m-fotter ul li{float:left;padding-right:10px;border-right:1px solid #888;margin-right:10px;font-size:14px;margin-bottom:10px}.h-m-fotter-sec .h-m-fotter ul li:last-child{border-right:0;padding-left:0}.h-m-fotter-sec .h-m-fotter ul li a{color:#444}.h-m-fotter-sec-bottom{border-top:1px solid #ddd;padding:15px 0;float:left;width:100%}.h-m-fotter-sec-bottom ul{list-style:none;padding-left:5%;padding-right:5%}.h-m-fotter-sec-bottom ul li{float:left;width:31.3%;font-size:14px;padding:0 2%}.bank-logos img{max-width:100%}#header img{vertical-align:middle}.product-slider-link a{font-size:14px;color:#444;letter-spacing:.6px}.p-slide-img img{width:130px;height:auto}.p-slide-img:hover .p-s-image{-webkit-transform:scale(1.1);transform:scale(1.1)}.p-s-image{-webkit-transition:all .7s ease;transition:all .7s ease}.product-slider-link{padding:10px 0;text-align:left;line-height:24px;overflow:hidden;max-width:200px}.product-price-s{padding-bottom:10px;text-align:left}.product-price-s .p-d-price{font-size:14px;color:#999;margin-right:13px}.product-price-s .p-d-off{color:#ff7a22}.p-ds-off{color:#444;margin-right:13px}.banner-slider-top{width:100%;float:left}#defaultpanel4{z-index:99999}.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px}.m-sprite{background:url(https://m.dogspot.in/images/msprite.png) no-repeat;margin:auto}.m-user{float:left;background-position:-166px -40px}.m-orders{float:left;background-position:-33px -72px}.m-wishlist{float:left;background-position:-6px -72px}.m-logout{float:left;background-position:-78px -40px}.m-setting{float:left;background-position:-107px -40px}.m-contact-us{float:left;background-position:-139px -40px}ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc}#footer,.add-sans-sapce,.device,.nf-no,.p-slide-img,.pagination,.special-offer-section{text-align:center}ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc}ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%}ul.dsdropdown-menu{display:none}ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important}ul.dsdropdown-menu li:last-child{font-size:16px;border:0}ul.dsdropdown-menu li:hover{background:#f9f9f9}.nf-img{float:left;width:20%}.nf-no{position:absolute;width:17px;border-radius:17px;font-size:11px;height:17px;line-height:16px;background:red;color:#fff!important;border:1px solid red;top:-8px;right:-7px}.nf-text{float:left;width:80%}.nf-img img{max-width:50px;width:50px!important;height:auto!important}.nf-text h4{font-size:14px;padding-bottom:1px;color:#333}.nf-text p{color:#888;font-size:13px}.nf-active-bg{background:#f2f2f2}.cat-section,.hot_selling_sec,.recently_sec,.blogger_sec{float:left;width:100%;letter-spacing:.7px;margin-top:10px} .recently_sec h4{font-size:18px;padding:15px 10px;color:#333}.cat-section .cat-gallery,.hot_selling_carousel,.recently_carousel,.bloggers{overflow:scroll;overflow-y:hidden;white-space:nowrap;width:100%;border-bottom:1px solid #f2f2f2;padding-bottom:20px}.cat-section .cat-gallery a{font-size:12.3px;font-weight:700;letter-spacing:0;color:#555;text-decoration:none}.cat-section .cat-gallery div{margin-top:2px}.cat-section .cat-gallery ul,.hot_selling_carousel ul,.recently_carousel ul,.recently_sec ul,.bloggers ul{list-style:none;padding:0;margin:0}.cat-section .cat-gallery ul li{width:120px;text-align:center;border-left:1px solid #f4f4f4}.cat-section .cat-gallery ul li,.hot_selling_carousel ul li,.recently_carousel ul li,.blogger_sec .bloggers ul li{position:relative;display:inline-block;align-self:flex-start;border-radius:0;background:#fff;cursor:pointer;margin-left:10px}.cat-gallery::-webkit-scrollbar,.hot_selling_carousel::-webkit-scrollbar,.recently_carousel::-webkit-scrollbar{display:none}.cat-section .cat-gallery ul li:first-child{border:0}.hot_selling_carousel ul li{width:170px}.product-slider-link{width:173px}.banner-slider-top{margin-top:0}.special-offer-section{float:left;width:100%}.special-offer-lbanner img,.special-offer-sbanner img{width:100%;height:auto}.special-offer-sbanner{float:left;width:48%;padding:20px 0}.margin-left{margin-left:4%}a,body,del,div,em,form,h1,h2,h3,h4,html,i,iframe,img,ins,li,p,span,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline}#defaultpanel4,.whitewrapper{background-color:#fff}ul{list-style-type:none}:focus{outline:0}del{text-decoration:line-through}.cartsearch_blk a{margin-left:12px}.cartsearch_blk a:first-child{margin-left:0}#defaultpanel4{border-right:1px solid #ccc}#footer{font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:10px}.ui-link img{vertical-align:middle}@media only screen and (min-width :600px) and (max-width :1024px){.ui-input-text input{font-size:16px}}html{font-family:'Helvetica Neue',Helvetica,Arial,sans-serif}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-bars:before{content:"\f0c9"}.fa-bell-o:before{content:"\f0a2"}.fa-shopping-cart:before{content:"\f07a"}.fa-user-o:before{content:"\f2c0"}.fa-home:before{content:"\f015"}.fa-search:before{content:"\f002"}.swiper-wrapper{position:relative;width:100%;-webkit-transition-property:-webkit-transform,left,top;-webkit-transition-duration:0s;-webkit-transform:translate3d(0,0,0);-webkit-transition-timing-function:ease;-moz-transition-property:-moz-transform,left,top;-moz-transition-duration:0s;-moz-transform:translate3d(0,0,0);-moz-transition-timing-function:ease;-o-transition-property:-o-transform,left,top;-o-transition-duration:0s;-o-transition-timing-function:ease;-o-transform:translate(0,0);-ms-transition-property:-ms-transform,left,top;-ms-transition-duration:0s;-ms-transform:translate3d(0,0,0);-ms-transition-timing-function:ease;transition-property:transform,left,top;transition-duration:0s;transform:translate3d(0,0,0);transition-timing-function:ease;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}.swiper-slide{float:left;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;margin-bottom:-5px;max-height:100%}body{margin:0;font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height:1.5}.device{width:100%}.pagination{display:block;position:relative;width:100%;height:20px;top:-20px!important}.swiper-pagination-switch{display:inline-block;border-radius:10px;margin:0 2px;cursor:pointer}.add-sans-sapce{float:left;width:100%;margin:auto;max-height:120px}.accordion:after,.close{float:right;font-weight:700}.add-sans-sapce .add-banner{margin:auto}.panel div a{color:#333}.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none}.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4}.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4}.accordion:hover{background-color:#f8f8f8}.panel{padding:0;display:none;background-color:#fff;overflow:hidden}.panel div{padding:10px 25px}.accordion:after{content:'\002B';color:#777;margin-left:5px}.accordion.active:after{content:"\2212"}.ds-home-link{background:#6c9d06;padding:8px 10px}.ds-home-link a{color:#fff}.ds-nav-click{padding:12px}.ds-nav-click a{color:#333;text-decoration:none}.swiper-pagination-switch{background:#666!important;border:1px solid #666!important;width:14px!important;height:14px!important}.swiper-active-switch{background:#6c9d06!important;border:1px solid #6c9d06!important;width:14px!important;height:14px!important}.banner-slider-top{margin-bottom:5px}.modal{display:none;position:fixed;z-index:1;padding-top:180px;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:#000;background-color:rgba(0,0,0,.4)}.modal-content{background-color:#fefefe;margin:auto;padding:20px;border:1px solid #888;width:80%}.close{color:#aaa;font-size:28px;margin-top:-20px}.close:focus,.close:hover{color:#000;text-decoration:none;cursor:pointer}@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(https://m.dogspot.in/css/font-awesome/fonts/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal}.ac_results{padding:2px;border:1px solid #bfbfbf;background-color:#fff;overflow:hidden;z-index:999;text-align:left;top:205px;width:374px!important;border-radius:2px}.ac-results ul{width:100%;padding:0;margin:0 0 5px;font-size:16px}.highlight-suggestion{color:#333}.hl-sugg-vtcl{font-weight:700;color:#6C9D06}.ac-results li{margin:0;padding:2px 5px 2px 10px;cursor:default;display:block;font-size:13px;line-height:18px;overflow:hidden}.ac_over{background-color:#f2f2f2;color:#333}.ac-results .header{margin-top:12px;overflow:visible;background:#6C9D06;margin-bottom:5px}.ac-results .header .text{display:inline-block;position:relative;color:#fff;padding:0 5px}
.blogger_sec h4{font-size: 18px;
    padding: 15px 10px;
    color: #333;}
.blogger_sec h4 span{color: #6c9d06;}
.blogger_sec .bloggers::-webkit-scrollbar{ display:none;}
.blogger_sec .bloggers ul li{ width:130px; text-align: center;}
.blogger_sec .bloggers figcaption{padding-top: 10px;
    font-weight: 600;}
.h-m-fotter-sec-bottom{ padding-bottom:62px;}	
</style>
 </head>

<?php require_once(SITEMAIN_URL .'/common/header.php');?>
 <div>
<?php require_once($DOCUMENT_ROOT .'/category-common.php'); ?>
  
  

<div class="banner-slider-top">
            <div class="device">
				<div class="slider1-container">
                <?php  include('includes/home/home-banner-images.php');  ?>
				</div>
  			</div>
            <div class="pagination"></div>
   
    </div>
    
    
   
    <div class="blogger_sec">
    <h4>  
    Top Bloggers At <span>DogSpot</span>
   </h4>
   
   <div class="bloggers">
   <ul>
   <li>
   <a href="/dog-blog/author/rana_vr1/" data-ajax="false">
   <figure>
   <img src="/images/rana-atheya-blogger.png" alt="Rana Atheya" title="Rana Atheya" width="110" height="110" />
   </figure>
   <figcaption>
   Rana Atheya
   </figcaption>
   </a>
   </li>
   <li>
   <a href="/dog-blog/author/neha.manchanda/" data-ajax="false">
   <figure>
   <img src="/images/neha-manchanda-bloggers.png" alt="Neha Manchanda" title="Neha Manchanda" width="110" height="110" />
   </figure>
   <figcaption>
Neha Manchanda
   </figcaption>
   </a>
   </li>
   <li>
   <a href="/dog-blog/author/adnan-khan/" data-ajax="false">
   <figure>
   <img src="/images/adnan-khan-blogger.png" alt="Adnan Khan" title="Adnan Khan" width="110" height="110" />
   </figure>
   <figcaption>
  Adnan Khan
   </figcaption>
   </a>
   </li>
   <li>
   <a href="/dog-blog/author/krithika1/" data-ajax="false">
   <figure>
   <img src="/images/kritika-kumar.png" alt="Krithika" title="Krithika" width="110" height="110" />
   </figure>
   <figcaption>
   Krithika
   </figcaption>
   </a>
   </li>
    <li>
   <a href="/dog-blog/author/228573/" data-ajax="false">
   <figure>
   <img src="/images/dipshika-thalani.png" alt="Deepshikha" title="Deepshikha" width="110" height="110" />
   </figure>
   <figcaption>
   Deepshikha
   </figcaption>
   </a>
   </li>
    <li>
   <a href="/dog-blog/author/ritwikmishra/" data-ajax="false">
   <figure>
   <img src="/images/ritwik-mihsra-blogger.png" alt="Ritwik Mishra" title="Ritwik Mishra" width="110" height="110" />
   </figure>
   <figcaption>
   Ritwik Mishra
   </figcaption>
   </a>
   </li>
   </ul>
   </div>
    </div>
   
  
    
    
    <div>
 

  
  
  
   <div class="special-offer-lbanner" style=" margin:10px auto; text-align:center; width:100%; float:left; ">
	 <div class="breed-selection" style="margin-bottom:10px;">
<a href="/dog-breeds/?utm_source=website&utm_medium=dog-breeds-home-banner&utm_campaign=banner" data-ajax="false" >    <img src="/images/dog-breeds-banner.jpg" 
width="480" height="60" alt="Dog Breed Information Center" title="Dog Breed Information Center" style="width:100%; height:auto;">
    </a>
    </div>
    <div class="cat-section">
  
    <div class="cat-gallery">
      <ul>
        <?php /*?><li> <a data-ajax="false" href="/dog-food/"> <img class="lazy" data-src="/images/dog-food-images.jpg" alt="Dog Food" title="Dog Food" width="100" height="60">
        
        <div>Dog Food</div>
         </a>
        </li><?php */?>
       <li><a data-ajax="false"  href="/beagle/"> <img src="https://www.dogspot.in/new/images/beagle-home.jpg" alt="beagle" title="beagle" width="100" height="60">
              <div>Beagle</div>
              </a> </li>
			  <li><a data-ajax="false"  href="/labrador-retriever/"> <img src="https://www.dogspot.in/new/images/lebrador-home.jpg" alt="Labrador Retriever" title="Labrador Retriever" width="100" height="60">
              <div style="margin-left: 5px;">Labrador Retriever</div>
              </a> </li>
        
        <li><a data-ajax="false" href="/german-shepherd-dog-alsatian/"> <img class="lazy" data-src="https://www.dogspot.in/new/images/german-home.jpg" alt="German Shepherd" title="German Shepherd" width="100" height="60"> 
        <div>German Shepherd</div>
        </a>
        
        </li>
        <?php /*?><li> <a data-ajax="false" href="/dog-biscuits/"> <img src="/images/dog-biscuits.jpg" alt="Dog Biscuits" title="Dog Biscuits" width="100" height="60">
        
        <div>Dog Biscuits</div>
         </a>
        </li><?php */?>
               <li> <a data-ajax="false" href="/golden-retriever/"> <img class="lazy" data-src="https://www.dogspot.in/new/images/golden-home.jpg" alt="Golden Retriever" title="Golden Retriever" width="100" height="60">
        
        <div>Golden Retriever</div>
         </a>
        </li>
        <li><a data-ajax="false" href="/pug/"> <img class="lazy" data-src="https://www.dogspot.in/new/images/pug-home.jpg" alt="Pug" title="Pug" width="100" height="60"> 
          <div>Pug</div></a></li>
       
      
      </ul>
    </div>
  </div>
  </div>   
  <?php /*?><div class="special-offer-lbanner" >
  <a href="/dry-dog-food/?utm_source=website&utm_medium=rc-home-&utm_campaign=banner" data-ajax="false">
  <img src="/images/rc-banner-new-d.jpg" width="1020" height="311" alt="Colorful & Durable Leashes,
Collar & Harness" title="Colorful & Durable Leashes, Collar & Harness">
  </a>
  
  </div><?php */?>

 <?php /*?><div class="special-offer-lbanner" style=" margin-top:20px;">
  <a href="/store-99/?utm_source=website&utm_medium=sotore-banner-home-&utm_campaign=banner" data-ajax="false">
  <img src="/images/99-store.jpg" width="1020" height="311" alt="Best products @ budget 
friendly prices" title="Best products @ budget 
friendly prices">
  </a>
  
  </div><?php */?>
  
  <div class="special-offer-lbanner" style=" margin-top:20px;">
<a href="/touchdog/?utm_source=website&amp;utm_medium=dog-bowls-home-banner&amp;utm_campaign=banner" data-ajax="false">
  

<img src="/images/tou-dd-bbb.jpg" alt="Touchdog Beds" title="Touchdog Beds" width="530" height="312">  </a>
  
  </div>

  <div class="special-offer-section" style="margin:10px auto">
<?php /*?><div>
<!-- Responsive-Head -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="6045502591"
     data-ad-format="auto"></ins>
<script>
$(document).ready(function(){(adsbygoogle = window.adsbygoogle || []).push({})})

</script>
</div><?php */?>
<!-- /21630298032/Mobile_300x250_home -->
<div id='div-gpt-ad-1554451796038-0' style='margin-left:10px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1554451796038-0'); });
</script>
</div></div>
  
 
</div>
	<!-- shop by category end-->
	<div class="hot_selling_sec">
    <h1 style="  font-size: 18px;padding: 15px 10px;color: #333; ">  
    
   Hot Selling Products</h1>
    <div class="hot_selling_carousel">
      <ul>
        <? $item_idSell=hotSelling();
	   foreach($item_idSell as $item){$qItemq=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$type_id1=$qItemq["type_id"];
					$name=$qItemq["name"];
					$domain_id=$qItemq["domain_id"];
					$stock_status=$qItemq["stock_status"];
					$price=number_format($qItemq["price"],0);
					$selling_price=number_format($qItemq["selling_price"],0);
					$discountPer=number_format(($selling_price-$price)/$selling_price*100,0);	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
					$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
					$nice_name=$qItemnice["nice_name"];	
					$rowdatM = mysql_fetch_array($qdataM);
				if($rowdatM["media_file"]){
		//$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		//$imageURL='/imgthumb/250x250-'.$rowdatM["media_file"];
		$src = $imageBasePath.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL=$absPath.'/imgthumb/130x130-'.$rowdatM["media_file"];
		$dest = $imageBasePath.'/imgthumb/130x130-'.$rowdatM["media_file"];
	}else{
		//$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		//$imageURL='/shop/image/no-photo-t.jpg';
		$src = $imageBasePath.'/shop/image/no-photo-t.jpg';
		$imageURL=$absPath.'/shop/image/no-photo-t.jpg';
		$dest = $imageBasePath.'/shop/image/no-photo-t.jpg';
	}
     createImgThumbIfnot($src,$dest,'130','130','ratiowh');
	 $image_info   = getimagesize($dest);
                    $image_width  = $image_info[0];
                    $image_height = $image_info[1];
?>
        <li>
          <div class="p-slide-img"> <a href="/<?=$nice_name?>/" data-ajax="false"><img src="<?=$imageURL?>" class="p-s-image" title="<?=$name?>" alt="<?=$name?>" 
                width="130" height="130"></a> </div>
          <div class="product-slider-link"><a href="/<?=$nice_name?>/ " data-ajax="false">
            <?=$name?>
            </a></div>
          <? if($selling_price>$price){?>
          <div class="product-price-s"> <span class="p-ds-off">
            <?=iconv("UTF-8", "ISO-8859-1",'&#8377;')?>
            <?=$price?>
            </span><span class="p-d-price"><del>
            <?=iconv("UTF-8", "ISO-8859-1",'&#8377;')?>
            <?=$selling_price?>
            </del></span> <span class="p-d-off">
            <?=$discountPer?>%
            </span></div>
          <? }else{?>
          <div class="product-price-s"> <span class="p-ds-off">
            <?=iconv("UTF-8", "ISO-8859-1",'&#8377;')?>
            <?=$price?>
            </span></div>
          <? }?>
          <div></div>
        </li>
        <? }?>
      </ul>
    </div>
  </div>
  <div class="special-offer-section" style="margin:10px auto">
<?php /*?><div>
<!-- Responsive-Head -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="6045502591"
     data-ad-format="auto"></ins>
<script>
$(document).ready(function(){(adsbygoogle = window.adsbygoogle || []).push({})})

</script>
</div><?php */?>
  <div style="color:#333;font-size: 26px;">DogSpot Trusted Partners</div>
        <ul style="border: 1px solid #eee;float: left;width: 100%;padding: 0;margin:10px auto;
    list-style-type: none;">
          <a href="https://www.petshopdelhi.in/" target="_blank">
          <li style="width:100%;text-align:center;"><img style="margin-left: auto;
	margin-right: auto;
	display: block;" src="https://www.dogspot.in/new/images/doggy-world-new.jpg" alt=" Doggy World" width="250" height="150" /></li>
          <li>
            <div style="font-size: 16px;
    font-weight: 300;
    margin-bottom: 10px;"> Doggy World</div>
            <div>Doggy World provides unmatched care & service for your lovable pet </div>
            <div style="color:#F30">Veterinary Hospital, Pet Grooming and Pet Shop</div>
            <div class="vet-gray"> <img src="https://www.dogspot.in/new/images/map-icons.jpg"
            alt="Map Icons" title="Map Icons" width="20" height="29" style="vertical-align:middle; max-width:14px; height:auto; margin-right:2px;"  />B-6/147-148, Sector - 8, Rohini, Delhi</div>
          </li>
          <li style="display:inline-block;">
             <div style="text-align:center;margin:5px;float:left;"> <img style="margin-left: auto;
	margin-right: auto;
	display: block;" src="https://www.dogspot.in/new/images/aradhana.jpg" height="128" width="122" / alt="Dr. Aradhana Pandey">
                <div class="dr-name">Dr. Aradhana Pandey</div>
              </div>
              <div style="text-align:center;margin:5px;float:left;"> <img style="margin-left: auto;
	margin-right: auto;
	display: block;" src="https://www.dogspot.in/new/images/sk-panday.jpg" height="128" width="122" / alt="Dr. Aradhana Pandey">
                <div class="dr-name">Dr. S. K. Pandey</div>
              </div>
           </li>
          </a>
        </ul>
      </div>
    <div class="special-offer-lbanner">
  <a href="/cat-sales/?utm_source=website&utm_medium=cat-scratchers-home-banner&utm_campaign=banner" data-ajax="false">
  <img src="/images/cat-banner-home-new.jpg" width="1080" height="312" alt="Upto 74% Off On Cat Products" title="Upto 74% Off On Cat Products">
  </a>
  
  </div>
	
   
           <?  if($userid=='Guest'){
	      $countuser=query_execute("SELECT  DISTINCT(si.item_id) FROM user_items_history as us,shop_items as si,shop_item_category as sic WHERE sic.item_id=si.item_id AND sic.category_id !='836' AND sic.category_id !='837' AND sic.category_id !='835'  AND si.item_id=us.item_id  AND visibility='visible' AND stock_status='instock' AND user_ip='$ip' AND type_id !='configurable' ORDER BY id DESC LIMIT 10"); 
	  }elseif($userid !='Guest')
	   {
	   $countuser=query_execute("SELECT  DISTINCT(si.item_id) FROM user_items_history as us,shop_items as si ,shop_item_category as sic WHERE sic.item_id=si.item_id AND sic.category_id !='836' AND sic.category_id !='837' AND sic.category_id !='835' AND si.item_id=us.item_id  AND visibility='visible' AND stock_status='instock' AND userid='$userid' AND type_id !='configurable' ORDER BY id DESC LIMIT 10");  
	   }
	  // echo "SELECT item_id FROM user_items_history WHERE userid='$userid' ORDER BY id DESC";
$c_count=mysql_num_rows($countuser);
if($c_count>3)
{
	//echo "SELECT item_id FROM user_items_history WHERE (userid='$userid' OR user_ip='$ip') AND userid !='Guest' ORDER BY id DESC";
	?>
    <div class="recently_sec" style="margin-bottom: 10px;">
    <h4>Recently Viewed</h4>
    <div class="recently_carousel">
      <ul>
        <? while($item_idS=mysql_fetch_array($countuser))
	   {
		$item_idRec[]= $item_idS['item_id']; 
	   }
	   foreach($item_idRec as $item){$qItemq=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$type_id1=$qItemq["type_id"];
					$name=$qItemq["name"];
					$domain_id=$qItemq["domain_id"];
					$stock_status=$qItemq["stock_status"];
					$price=number_format($qItemq["price"],0);
					$selling_price=number_format($qItemq["selling_price"],0);
					$discountPer=number_format(($selling_price-$price)/$selling_price*100,0);	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
					$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
					$nice_name=$qItemnice["nice_name"];	
					$rowdatM = mysql_fetch_array($qdataM);
			if($rowdatM["media_file"]){
		//$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		//$imageURL='/imgthumb/250x250-'.$rowdatM["media_file"];
		$src = $imageBasePath.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL=$absPath.'/imgthumb/130x130-'.$rowdatM["media_file"];
		$dest = $imageBasePath.'/imgthumb/130x130-'.$rowdatM["media_file"];
	}else{
		//$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		//$imageURL='/shop/image/no-photo-t.jpg';
		$src = $imageBasePath.'/shop/image/no-photo-t.jpg';
		$imageURL=$absPath.'/shop/image/no-photo-t.jpg';
		$dest = $imageBasePath.'/shop/image/no-photo-t.jpg';
	}
     createImgThumbIfnot($src,$dest,'130','130','ratiowh');
	 $image_info   = getimagesize($dest);
                    $image_width  = $image_info[0];
                    $image_height = $image_info[1];
?>
        <li>
          <div class="p-slide-img"> <a href="/<?=$nice_name?>/" data-ajax="false"><img src="<?=$imageURL?>" class="p-s-image" title="<?=$name?>" alt="<?=$name?>" 
                width="" height=""></a> </div>
          <div class="product-slider-link"><a data-ajax="false" href="/<?=$nice_name?>/ ">
            <?=$name?>
            </a></div>
          <? if($selling_price>$price){?>
          <div class="product-price-s"> <span class="p-ds-off">
            <?=iconv("UTF-8", "ISO-8859-1",'&#8377;')?>
            <?=$price?>
            </span><span class="p-d-price"><del>
            <?=iconv("UTF-8", "ISO-8859-1",'&#8377;')?>
            <?=$selling_price?>
            </del></span> <span class="p-d-off">
            <?=$discountPer?>%
           </span></div>
          <? }else{?>
          <div class="product-price-s"> <span class="p-ds-off">
            <?=iconv("UTF-8", "ISO-8859-1",'&#8377;')?>
            <?=$price?>
            </span></div>
          <? }?>
          <div></div>
        </li>
        <? }?>
      </ul>
    </div>
  </div>
<? }?>
 </div> 

<script>
        var prevSelection = "tab1";
        $("#navbar ul li").live("click",function(){
            var newSelection = $(this).children("a").attr("data-tab-class");
            $("."+prevSelection).addClass("ui-screen-hidden");
            $("."+newSelection).removeClass("ui-screen-hidden");
            prevSelection = newSelection;
        });
		$( function() {
			$( ".pagination span:first-child" ).addClass('swiper-visible-switch swiper-active-switch');
		//	$( "#purchase-product" ).enhanceWithin().popup();
			$( "#purchaseproductdetail" ).enhanceWithin().popup();
			$( "#purchaseproductdetail1" ).enhanceWithin().popup();
			$( "#selectsize_popup" ).enhanceWithin().popup();
			$( "#morepopup" ).enhanceWithin().popup();
			$( "#buynowpopup" ).enhanceWithin().popup();
			$( "#subscription_offer" ).enhanceWithin().popup();
			//$( "#defaultpanel" ).enhanceWithin().panel();
		});
    </script>   


  <?php require_once('common/bottom-new.php');
	  require_once('common/bottom-final.php');
//}
?>
	    
		
  