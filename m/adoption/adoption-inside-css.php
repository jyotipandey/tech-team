<?php
	require_once(SITEMAIN_URL."/database.php");
	require_once(SITEMAIN_URL."/functions.php");
	require_once(SITEMAIN_URL."/shop/functions.php");
	require_once(SITEMAIN_URL."/functions2.php");
	require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
	require_once(SITEMAIN_URL.'/session.php');
	require_once(SITEMAIN_URL.'/banner1.php');
	$sitesection='adoption';
	
	$puppi_details = mysql_query("SELECT * FROM puppies_available WHERE puppy_nicename='$puppy_nicename' AND adoption='yes' AND publish_status='publish'");
	if(mysql_num_rows($puppi_details) > 0){
		while($rowData = mysql_fetch_array($puppi_details)){
			$puppi_id = $rowData['puppi_id'];
			$puppy_owner    = $rowData['userid'];
			$name = $rowData['puppi_name'];
            $puppi_breed = $rowData['puppi_breed'];
			$puppi_name = $puppyDet['puppi_name'];
			$city = $rowData['city'];
			$source = $rowData['source'];
			$status = $rowData['status'];
			$temperament = $rowData['temperament'];
			$condition1 = $rowData['condition1'];
			$puppi_sex = $rowData['puppi_sex'];
			$year = $rowData['year'];
			$month = $rowData['month'];
			$day = $rowData['day'];
			$puppi_desc = $rowData['puppi_desc'];
			$puppi_img = $rowData['puppi_img'];
			
			// For Social sharing
			$title = $name."|".$puppi_breed."|".$city;
			$stext=urlencode(trim($title));	
			$whatapp=$name.','.$puppi_breed." for adoption in ".$city;
			$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$pattern = 'm.dogspot.in';
			$abc="www.dogspot.in";
			$surl= str_replace($pattern,$abc,$url);
		}	
	}
	$sql = "SELECT share_key, share_value FROM share_count WHERE share_url = '$surl'";
	
	$shared_query = query_execute($sql);  
	while ($row_shared = mysql_fetch_array($shared_query)) {
		$share_key = $row_shared['share_key'];
		$share_value = $row_shared['share_value'];
		$shareData[$share_key]=$share_value;		
	}
if(!$puppi_id)
{
header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?="$name | $puppi_breed for Adoption | $city | $source | $puppi_id "; ?>| DogSpot</title>
<meta name="keywords" content="<?="$name, $puppi_breed for Adoption, $city, $puppy_owner, $puppi_id " ?>" />
<meta name="description" content="<?="Adopt a Dog or Puppy in India.$name, $puppi_breed for Adoption, $city, $puppy_owner, $puppi_id" ?>" />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="adoption" />
<meta property="og:url" content="https://www.dogspot.in/adoption/<?=$puppy_nicename ?>/" /> 
<meta property="og:title" content="<?="$name | $puppi_breed | $city | $source  | Adoption "; ?>" /> 
<meta property="og:description" content="Adoption" />
<meta property="og:image" content="<?="https://www.dogspot.in/puppies/images/".$puppi_img ?>" />
<link rel="canonical" href="https://www.dogspot.in/adoption/<?=$puppy_nicename ?>/" />
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top-home-test.php'); ?>
<style>
/*! CSS Used from: Embedded */
.sticky-bar,.sticky-height{height:30px;}
.sale-text,.start-countdown{display:inline-block;}
.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333;}
.sticky-bar a,.sticky-bar a:hover{text-decoration:underline;}
.sticky-left{width:96%;float:left;}
.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px;}
.sale-text{font-size:12px;}
.sale-text a{color:#fc3;text-decoration:none;}
.ds-home-link{padding-top:30px!important;}
body{font-family:lato,sans-serif;}
table{border-collapse:collapse;}
body,button,input{font-size:1em;line-height:1.3;font-family:lato,sans-serif;}
.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em;}
.ui-corner-all{-webkit-background-clip:padding;background-clip:padding-box;}
.ui-body-a{border-width:1px;border-style:solid;}
.ui-body-a{background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3;}
.ui-mobile body{height:99.9%;}
.ui-page{padding:0;margin:0;}
.ui-mobile a img{border-width:0;}
.ui-mobile-viewport{margin:0;overflow-x:visible;-webkit-text-size-adjust:100%;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent;}
body.ui-mobile-viewport{overflow-x:hidden;}
.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0;}
.ui-page{outline:0;}
.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden;}
@media screen and (orientation:portrait){
.ui-mobile .ui-page{min-height:420px;}
}
@media screen and (orientation:landscape){
.ui-mobile .ui-page{min-height:300px;}
}
.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0;}
.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em;}
.ui-content{border-width:0;overflow-x:hidden;}
.ui-screen-hidden{display:none!important;}
.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%;}
.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099;}
.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important;}
.ui-popup{position:relative;padding:10px 10px 0;}
.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff;}
.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden;}
.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px);}
.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block;}
.ui-panel-page-container{overflow-x:visible;}
.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none;}
.ui-panel-dismiss-open{display:block;}
.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease;}
.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0);}
.ui-panel-position-left{left:-17em;}
.ui-panel-open.ui-panel-position-left{left:-1px;}
.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em;}
body{margin:0;padding:0;background:#F8F8F8;}
div,form,h1,h4,h6,img,p,table,ul{margin:0;padding:0;border:0;}
#header{text-align:left;height:50px;padding:12px;border-top:0;border-bottom:0;background:#6c9d06;}
#header .fa{font-size:18px;color:#fff;}
td{font-weight:400;}
#header div,#morepopup li a,.ui-link{text-decoration:none;}
table{width:100%;}
tr:nth-of-type(odd){background:#ddd;}
td{padding:5px;border:1px solid #ccc;text-align:left;font-size:14px;}
#header div,.panelicon{display:inline-block;}
#header div{color:#f7422d;}
.panelicon{background:url(https://m.dogspot.in/adoption/images/mobile-panel-icon-white.png);height:19px;left:0;margin-top:0!important;position:relative;top:0;width:18px;}
.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px;}
.ui-link{color:#333;}
.green_clr{font-family:gobold;margin-left:8px;}
.green_clr{color:#fff!important;}
.green_clr{font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335;}
#morepopup li,#morepopup li a{color:#323232;}
#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px;}
#morepopup li img{margin-right:8px;vertical-align:top;}
#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px;}
#morepopup li:last-child{border-bottom:0;}
#morepopup-screen{background:0 0;}
.content-search{padding-bottom:20px;}
.content-search form{margin:0 auto;position:relative;top:5px;width:95%;}
.purchase-product-cart{position:relative;}
.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%;}
*{margin:0;padding:0;font-family:lato,sans-serif;}
.adoption_tab tr:nth-of-type(odd){background:#fff;}
.adoption_dsc_blk{margin:10px;}
.adoption_dsc_blk h1{font-size:20px;margin-top:15px;margin-left:10px;font-weight:400;}
.adoption_dsc_blk img{max-width:100%;height:auto;margin-bottom:10px;margin-top:10px;}
.adoption_besic_info{color:#8dc059;font-size:22px;}
.adoption_tab{margin-top:15px;}
.adoption_tab td{border:1px solid #ddd;padding:10px;}
.adoption_text{font-size:16px;margin-top:10px;margin-bottom:10px;width:100%;float:left;}
.ui-content{clear:both;}
.cartsearch_blk a img{width:18px;height:19px;}
.content-search{width:100%;float:left;}
.content-search{margin-top:0!important;padding:5px 0;background:#6c9d06;}
.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0;}
#header img{vertical-align:middle;}
.ui-content{overflow:hidden;}
#defaultpanel4{z-index:99999;}
.ui-content{float:left;width:100%;}
.widget h6,div.dsdropdown{position:relative;}
.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px;}
.m-sprite{background:url(https://m.dogspot.in/adoption/images/msprite.png) no-repeat;margin:auto;}
.m-user{float:left;background-position:-166px -40px;}
.m-orders{float:left;background-position:-33px -72px;}
.m-wishlist{float:left;background-position:-6px -72px;}
.m-logout{float:left;background-position:-78px -40px;}
.m-setting{float:left;background-position:-107px -40px;}
.m-contact-us{float:left;background-position:-139px -40px;}
.widget h6{color:#666;font-size:16px;font-weight:700;margin:0 0 13px;background:#f9f9f9;line-height:25px;float:left;width:100%;padding:5px 0 5px 10px;}
.gurgaon_offers{width:100%;float:left;border-bottom:1px solid #ddd;margin-bottom:10px;padding-bottom:15px;}
.gurgaon_offers:last-child{border-bottom:0;}
.gurgaon_offersr{float:left;width:44%;padding-left:10px;}
.gurgaon_offersl{font-size:13px;float:left;width:50%;padding-top:10px;font-weight:700;line-height:1.5;}
.gurgaon_offers_pn{margin-bottom:5px;}
ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc;}
ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc;}
ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%;}
ul.dsdropdown-menu{display:none;}
ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important;}
ul.dsdropdown-menu li:last-child{font-size:16px;border:0;}
ul.dsdropdown-menu li:hover{background:#f9f9f9;}
.nf-img{float:left;width:20%;}
.nf-text{float:left;width:80%;}
.nf-img img{width:50px!important;height:auto!important;}
.nf-text h4{font-size:14px;padding-bottom:1px;color:#333;}
.nf-text p{color:#888;font-size:13px;}
.nf-active-bg{background:#f2f2f2;}
.cat-section{float:left;width:100%;letter-spacing:.7px;margin-top:10px;}
.cat-section .cat-gallery{overflow:scroll;overflow-y:hidden;white-space:nowrap;width:100%;border-bottom:1px solid #f2f2f2;padding-bottom:20px;}
.cat-section .cat-gallery a{font-size:12.3px;font-weight:700;letter-spacing:0;color:#555;text-decoration:none;}
.cat-section .cat-gallery div{margin-top:2px;}
.cat-section .cat-gallery ul{list-style:none;padding:0;margin:0;}
.cat-section .cat-gallery ul li{width:120px;text-align:center;border-left:1px solid #f4f4f4;}
.cat-section .cat-gallery ul li{position:relative;display:inline-block;align-self:flex-start;border-radius:0;background:#fff;cursor:pointer;margin-left:10px;}
.cat-gallery::-webkit-scrollbar{display:none;}
.cat-section .cat-gallery ul li:first-child{border:0;}
a,b,body,del,div,em,form,h1,h4,h6,i,iframe,img,ins,li,p,span,table,tbody,td,tr,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline;}
body{line-height:1.3;}
ul{list-style-type:none;}
:focus{outline:0;}
ins{text-decoration:none;}
del{text-decoration:line-through;}
table{border-collapse:collapse;border-spacing:0;}
.cb{clear:both;}
.cartsearch_blk a{margin-left:12px;}
.cartsearch_blk a:first-child{margin-left:0;}
#defaultpanel4{border-right:1px solid #ccc;background-color:#fff;}
.breadcrumb{overflow:hidden;padding:8px;margin-bottom:0;}
.breadcrumb a{font:12px Arial,Helvetica,sans-serif;color:#4b3a15;text-decoration:underline;}
.breadcrumb a:hover{text-decoration:none;}
.whitewrapper{background-color:#fff;}
.ui-link img{vertical-align:middle;}
*{box-sizing:border-box;}
.add-sans-sapce{float:left;width:100%;text-align:center;margin:auto;max-height:120px;}
.add-sans-sapce .add-banner{margin:auto;}
.panel div a{color:#333;}
.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none;}
.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4;}
.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4;}
.accordion:hover,.active{background-color:#f8f8f8;}
.panel{padding:0;display:none;background-color:#fff;overflow:hidden;}
.panel div{padding:10px 25px;}
.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px;}
.accordion.active:after{content:"\2212";}
.ds-home-link{background:#6c9d06;padding:8px 10px;}
.ds-home-link a{color:#fff;}
.ds-nav-click{padding:12px;}
.ds-nav-click a{color:#333;text-decoration:none;}
.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.fa-bars:before{content:"\f0c9";}
.fa-bell-o:before{content:"\f0a2";}
.fa-shopping-cart:before{content:"\f07a";}
.fa-user-o:before{content:"\f2c0";}
.fa-home:before{content:"\f015";}
.fa-search:before{content:"\f002";}
.header-banner{width:96%;margin:15px 2% 10px;float:left;text-align:center;}
.header-banner img{max-width:100%;height:auto;}
/*! CSS Used fontfaces */
@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'), url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'), url(https://m.dogspot.in/css/font-awesome/fonts/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'), url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'), url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal;}
</style>  
</head>
<? //echo addBannerProductM($_SERVER['REQUEST_URI']); ?>


<?php require_once($DOCUMENT_ROOT .'/category-common.php'); ?>
   
      	<div class="adoption_dsc_blk" style="float:left; width:100%">
        
        <div class="breadcrumb" >
 
  <div itemscope itemtype="http://schema.org/Breadcrumb">
    <div class="breadcrumb_cont" itemscope itemtype="http://schema.org/BreadcrumbList"> 
    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <a href="https://www.dogspot.in/" data-ajax="false" itemprop="item"><span itemprop="name">Home</span></a>
      <meta itemprop="position" content="1" /> </span>
     <span> / </span>
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <span itemprop="name"><a href="/adoption/" data-ajax="false" itemprop="item">Pet Adoption</a></span>
     <meta itemprop="position" content="2" /> 
     </span>
     <span> / </span>
      <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <span itemprop="name" class="brd_font_bold">
      <?=$puppi_name;?>
      (
      <?=$puppi_breed;?>
      )</span>
      <meta itemprop="position" content="3" /> 
      </span>
     
    </div>
    <div class="cb"></div>
    
  </div>
</div>
<div class="header-banner" >
 
   <?php require_once($DOCUMENT_ROOT .'/header-banner.php'); ?> 
</div>
        <?php /*?><div class="widget-pup-new Label banner-card">
       <? $getItems=query_execute("SELECT si.name,si.nice_name,si.item_id,si.price,si.selling_price,si.item_parent_id FROM shop_items as si WHERE banner_type='yes' ORDER By rand() LIMIT 3 ");
				 while($fetchaffiliate=mysql_fetch_array($getItems)){
			 $name=$fetchaffiliate['name'];
			 $item_id=$fetchaffiliate['item_id'];
			  $item_parent_id=$fetchaffiliate['item_parent_id'];
			   $nice_name=$fetchaffiliate['nice_name'];
			    $price=$fetchaffiliate['price'];
			   $mrp_price=$fetchaffiliate['selling_price'];
		 $rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
		 if(!$rowdatM['media_file'])
		 {
		$rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
		 }?>
                       <div class="banner-wrap"> 
        	<a data-ajax="false" href="/<?=$nice_name?>/?UTM=bannerrepublic">
                <div class="banner-img"> <img src="https://ik.imagekit.io/2345/tr:h-197,w-197/shop/item-images/orignal/<?=$rowdatM["media_file"]?>" width="100" height="93" style="margin-top:0px;"> </div>
                <div class="banner-text">
                    <div class="gurgaon_offers_pn"><?=$name?></div>
                                        <div class="gurgaon_offers_pr">
                                        <? if($mrp_price>$price){?>
                                        <span>Rs. <del><?=number_format($mrp_price,0)?></del> 
                                        </span> &nbsp; &nbsp; <? }?>
                                        <span style="color: #f00;">
                                        Rs. <?=number_format($price,0)?></span>
                                        </div>
                                    </div>
          	</a> 
		</div>
                
                
                
                
      
                <? }?>
                 </div><?php */?>
                 
			<h1 style="margin-top:0px;"><?=$name; ?>(<?=$puppi_id?>), <?=$puppi_breed; ?> for Adoption, <?=$city; ?></h1>
            
        	<!-- adoption list start-->
        	<div class="adoption_dsc_blk">
			<div id="pageone" data-role="main" class="ui-content">
   		   	<a href="" data-rel="popup" data-position-to="window"><img src="https://www.dogspot.in/puppies/images/330x400_<?=$puppi_img?>" alt="<?=$name?>"
title="<?=$name?>"			></a>
	  </div>
      <? if($status=='adopted'){?>
                 <div style="
   position: absolute;
   opacity: 0.8;
"><img src="https://www.dogspot.in/adoption/images/adopted-images.jpg" width="661" height="313"  alt="Aodpted Stamp" title="Aodpted Stamp"></div>
                <? }?>
        <div class="adoption_besic_info">Basic Information</div>
        <table class="adoption_tab">
        <? if($name){?>
        <tr>
            <td>Dog Name:</td>
            <td><?=$name; ?></td>
        </tr>
        <? }?>
         <? if($puppi_breed){?>
        <tr>
            <td>Breed Name:</td>
            <td><?=$puppi_breed; ?></td>
        </tr>
        <? }?>
         <? if($source){?>
        <tr>
            <td>Source:</td>
            <td> <?=$source; ?></td>
        </tr>
        <? }?>
          <? if($status){?>
        <tr> <? if($status=='rescue' || $status=='foster'){
					 $colr='color:green;';
					 $status='Available';
					 }else
					 {
					 $colr='color:#f00;';
					 $status='Adopted';
					 
				     }?>
            <td>Status:</td>
            <td> <span style=" <?=$colr?>"><?=$status; ?></span></td>
        </tr>
        <? }?>
         <? if($temperament){?>
        <tr>
            <td>Temperament:</td>
            <td> <?=$temperament; ?></td>
        </tr>
        <? }?>
        <? if($condition1){?>
        <tr>
            <td>Condition:</td>
            <td> <?=$condition1; ?></td>
        </tr>
        <? }?>
        <? if($puppi_sex){?>
        <tr>
            <td>Gender:</td>
            <td> <? if($puppi_sex=='M' || $puppi_sex=='m'){ echo "Male"; }else{ echo "Female"; }; ?></td></tr><? }?> <? if($city){?>
        <tr>  
      
            <td>Location:</td>
            <td> <?=$city; ?></td>
        </tr>
        <? }?>
        <? if($day && $month && $year){?>
        <tr>
            <td>Age: </td>
            <td>
            <? print (pupDOB($day, $month, $year)); ?>
            </td>
        </tr> 
        <? }?>       
        </table>
        <div id="misdata"></div>
        <? if($userid!='Guest'){ 
			if($status!='adopted'){
			$cur_date2233=date("Y-m-d");
			$selectpup=query_execute_row("SELECT count(*) as cc FROM adoptionmail WHERE userid='".$userid."' AND c_date='".$cur_date2233."'");
			if($selectpup['cc']=='0'){		
		?>
        <div><input  data-role="none" style="width: 100%;height: 35px;background: #ff8226;border: 1px solid #ff8226;border-radius: 3px;color: #fff;font-size: 18px;:margin-top: 15pxmargin-bottom: 15px;margin: 10px 0px;"  type="button" value="Interested" class="intrested_btn" onclick="sendmail('<?=$puppy_owner?>','<?=$userid?>','<?=$name?>','<?=$puppi_id ?>');"></div>
        <span><b></b>Click here to adopt a pet, give them forever home</span>
        <? } } }?> 
        <div style="float:left; width:100%; margin:15px 0px">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive-Head -->
<ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-3238649592700932"
    data-ad-slot="6045502591"
    data-ad-format="auto"></ins>
<script>
$(document).ready(function(){(adsbygoogle = window.adsbygoogle || []).push({})})
</script>

        </div>
		
        <div class="adoption_text"><?=stripslashes(htmlspecialchars_decode($puppi_desc));?></div>
        
        <? if($userid=='brajendra'){?>
           <? $qdataM1=mysql_query("SELECT `puppi_img`, city, puppi_name, puppy_nicename FROM `puppies_available` WHERE city='$city' AND `puppi_img`!='' AND puppi_name!='' AND publish_status='publish' ORDER BY c_date DESC LIMIT 10");
		 $qdataM12=mysql_query("SELECT `puppi_img`, city, puppi_name, puppy_nicename FROM `puppies_available` WHERE city='$city' AND `puppi_img`!='' AND puppi_name!='' AND publish_status='publish' LIMIT 10");
		 $ttdtotal=mysql_num_rows($qdataM12);
		 if($ttdtotal>2){
		 ?>
<div id="slider">   

<h2 class="related_pets">More Pets From <?=$city;?></h2> 

            <div class="scrollButtons left sb_icon_left"></div>

			<div style="overflow: hidden;" class="scroll">
	    <?

while($sqlgetqdata=mysql_fetch_array($qdataM1)){
	$imdet=$sqlgetqdata['puppi_img'];
	$puppy_nicename=$sqlgetqdata['puppy_nicename'];
		 if($imdet){
		$src = $DOCUMENT_ROOT.'/puppies/images/'.$imdet;
		$imageURL121='/puppies/images/145x143_'.$imdet;
		//$imageURLface='/puppies/images/350x210-'.$sqlimage1;
		
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL121='/dogs/images/no-photo-t.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL121;
	//echo "jyoti". $src.$dest;
	//$dest1 = $DOCUMENT_ROOT.$imageURLface;
	createImgThumbIfnot($src,$dest,'145','143','ratiohh');
	$image_info = getimagesize($dest);
$image_width = $image_info[0];
$image_height = $image_info[1]; 
?>
				<div class="scrollContainer">
	
	                <div class="panel" id="panel">
						<div class="inside">
							<div class="insideimgBox"><a href="/adoption/<?=$puppy_nicename?>/">
                             <img src="<?=$imageURL121?>" alt="picture"  width="<?=$image_width?>" height="<?=$image_height?>"/></a></div>
<h4><a href="/adoption/<?=$puppy_nicename?>/"><?=$sqlgetqdata['puppi_name'].",".' '.$sqlgetqdata['city']?></a></h4>
							
						</div>
					</div>
                </div>
<? }?>
				<div id="left-shadow"></div>
				<div id="right-shadow"></div>
               
            </div>

			<div class="scrollButtons right sb_icon_right"></div>
 
        </div>  <? }?> 
        <? }?>
        <?php /*?><div class="w-article-cat slide-banner"> 
        <div class="slide-banner-block" >
         <a href="/flea-ticks/" data-ajax="false"><img src="https://m.dogspot.in/dog-blog/images/flean-ticks-banner.jpg" class="slide-banner-one" ></a>
        </div>
        <div class="slide-banner-block" >
        <a href="/cat-litter-boxes/" data-ajax="false"><img src="https://m.dogspot.in/dog-blog/images/cat-litter-tray1.jpg" class="slide-banner-two"></a>
        </div>
        
        </div><?php */?>
        </div>
        <!-- adoption end-->
        </div>
         	<!--<div style="float:left; width:100%; text-align:center; margin-bottom:10px;">
  <script type="text/javascript" language="javascript">
     var aax_size='300x250';
     var aax_pubname = 'dog0bd-21';
     var aax_src='302';
   </script>
  <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>
</div>-->
      
      
        <div class="widget Label" style="margin-top:20px;">
                 <h6><span>Sponsored: In the Stores</span></h6>
                 <!--<div class="ggn15_code"><span> code: GGN15</span></div>-->
      <? $getItems=query_execute("SELECT si.name,si.nice_name,si.item_id,si.price,si.selling_price,si.item_parent_id FROM shop_items as si WHERE banner_type='yes' ");
				 while($fetchaffiliate=mysql_fetch_array($getItems)){
			 $name=$fetchaffiliate['name'];
			 $item_id=$fetchaffiliate['item_id'];
			  $item_parent_id=$fetchaffiliate['item_parent_id'];
			   $nice_name=$fetchaffiliate['nice_name'];
			    $price=$fetchaffiliate['price'];
			   $mrp_price=$fetchaffiliate['selling_price'];
		 $rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
		 if(!$rowdatM['media_file'])
		 {
		$rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
	     }
			 ?>
                         <div class="gurgaon_offers"> 
        	<a href="/<?=$nice_name?>/" data-ajax="false">
                <div class="gurgaon_offersr"> 
				<img src="https://ik.imagekit.io/2345/tr:h-197,w-197/shop/item-images/orignal/<?=$rowdatM["media_file"]?>" alt="<?=$name?>" title="<?=$name?>" width="100" height="93"> </div>
                <div class="gurgaon_offersl">
                    <div class="gurgaon_offers_pn"><?=$name?></div>
                                        <div class="gurgaon_offers_pr">
                                        <? if($mrp_price>$price){?>
                                        <span>Rs. <del><?=number_format($mrp_price,0)?></del> 
                                        </span> &nbsp; &nbsp; <? }?>
                                        <span style="color: #f00;">
                                        Rs. <?=number_format($price,0)?></span>
                                                                               </div>
                                    </div>
          	</a> 
		</div>
                
                
                
                
                

                <? }?>
                 </div>

		<?php require_once($DOCUMENT_ROOT .'/common/bottom-home-test.php'); ?>