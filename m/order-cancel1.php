<?php
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
//require_once(SITEMAIN_URL.'/functions.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
?>
<!DOCTYPE html>
<html>
<head>
<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
<div  class="p-one-em myorder_page27">
	 <?php require_once('common/script.php'); ?>
	 <?php require_once('common/top.php'); ?>
    <?php 
 	if(isset($_POST['cancelOrder'])){
 	$cancelled_reason = $_POST['cancel_reason'];
	$user_comment = mysql_real_escape_string($_POST['comments']);
	$order_id=$_POST['order_id'];
	mysql_query("UPDATE `shop_order` SET cancelled_reason='$cancelled_reason',user_comment='$user_comment',delevery_status='cancelled' WHERE order_id='$order_id' && userid='$userid'");
	/* For Order Cancel Mail to care@dogspot.in */
	$usermail = mysql_query("SELECT address_name, address_email from shop_order_address where order_id = '$order_id' AND address_type_id='1'");
	$result_mail = mysql_fetch_array($usermail);
	$user_mail = $result_mail['address_email'];
	$user_name1 = $result_mail['address_name'];
	$user_name2 = explode(" ",$user_name1);
	$user_name = $user_name2[0];
	
	$rec_check = mysql_query("SELECT * from shop_order where order_id='$order_id' AND order_status='0' AND order_method NOT IN('cod','dd')");
	$check_result = mysql_num_rows($rec_check);
	if($check_result > 0){
		 
			
		$e="care@dogspot.in";
	
		$mailSubject = "$order_id Cancel Please check and refund";
		$mailbody = "$order_id Cancel Please check and refund";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: DogSpot.in <care@dogspot.in>' . "\r\n";
		$headers .= 'Bcc: DogSpot.in <contact@dogspot.in>' . "\r\n";
		$headers .= 'Cc: jyoti@dogspot.in' . "\r\n";
		$headers .= 'Cc: DogSpot <finance@dogspot.in>' . "\r\n";
		$headers .= 'Cc: DogSpot <kshitij@dogspot.in>' . "\r\n";
		$headers .= 'Cc: DogSpot <sharmila@dogspot.in>' . "\r\n";
		$headers .= 'Reply-To: DogSpot.in <care@dogspot.in>'. "\r\n";
		$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
		$headers .= "Organization: DogSpot\r\n";
	
		//echo $e.$mailSubject.$mailbody.$headers;
		if(mail($e,$mailSubject, $mailbody, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")){
	
			//echo $emailMsg='Email send successfully to '.$e;
		}
		else{
			//echo $emailMsg='Error in sending email to '.$email;
		}
	
	}
	/* For Order Cancel Mail to care@dogspot.in ENDS */
	
	
	// Email to customer
	if($user_mail){
			
		$SQ_shop = 	query_execute_row("SELECT mode,order_status FROM shop_order WHERE order_id='$order_id'");
		$order_mode = $SQ_shop['mode'];
		$status = $SQ_shop['status'];
		$SQ_section_review_id = 	query_execute_row("SELECT section_review_id FROM  section_reviews WHERE review_section_id='$order_id' AND (review_name='cancelled' OR review_name='canceled') ORDER by section_review_id DESC LIMIT 1 ");
		if($SQ_section_review_id['section_review_id']==''){
			
			$mailSubject_customer = "Cancellation of DogSpot Order No# $order_id.";
			$mailbody_customer = "<p>Dear $user_name,</p>
			<p>Greetings from DogSpot!</p>
			<p>We regret to inform you that your order Number: $order_id placed with us has been cancelled by us.</p><p><b>Reason For cancellation:</b></p><p><i>As per your request</i></p>
			<p>We value you as a loyal DogSpot customer and hope you understand that this was a situation that could not have been avoided in spite of our best efforts.</p>
			<p>If applicable, we will process the refund and the same will be reflecting in your account within 7-10 working days.</p>
			<p>You could write to us on <a href='mailto:care@dogspot.in'>care@dogspot.in</a> or call our Customer Support Team at +91-9599090487(Mon to Sat from 10:00 am to 6:00 pm). </p>
			<p>Once again we apologize for the inconvenience and hope to see you again on <a href='http://m.dogspot.in'>DogSpot.in</a></p>
			<br/>
			<p>Regards,</p>
			<p>Team DogSpot</p>";
				
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: DogSpot.in <care@dogspot.in>' . "\r\n";
			$headers .= 'Bcc: DogSpot.in <contact@dogspot.in>' . "\r\n";
			$headers .= 'Reply-To: DogSpot.in <care@dogspot.in>'. "\r\n";
			$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
			$headers .= "Organization: DogSpot\r\n";
				
			if(mail($user_mail,$mailSubject_customer, $mailbody_customer, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")){
				$respond_order_cancel = send_sms_to_customer($order_id, "OrderCanxByCustomer", "", "", "", ""); 	//// Send auto Sms customer cancelled order.
				$resultinsert = query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body,review_name, c_date, user_ip) VALUES ('".$userid."', 'shop-order', '".$order_id."', '".$cancel_reason.$ordercomnt."','cancelled', null, '')");
				echo '<script>
				$(location).attr("href","/myorder.php");
				</script>';
			}
		}
	}
	//echo '<script>				$(location).attr("href","/myorder.php");				</script';
 }else{
 	//header("location:/myorder.php");
 }
 
?>
<div  id="reason_for_cancel" class="reason_cancel_popup" >
<div class="order_cancel_reason">Reasons for cancelling order?</div>
<form action="#" id="formorder" name="formorder" method="post">
<div>
<div>
<select id="cancel_reason" name="cancel_reason">
			<option>--Select Reason--</option>	
            <option>Not interested any more</option>
            <option>Order delivery delayed</option>
            <option>Ordered duplicate items by mistake</option>
            <option>Purchased wrong item</option>
            <option>Other reasons</option>
          </select>
<textarea name="comments" placeholder="Write your comments"></textarea>

</div>
<div class="option_button">
<ul>
<li>
<input class="cancel_resion-btn cancel_btnstyle" data-role="none" type="submit" value= "submit" name="cancelOrder" id="cancelOrder"></li>
<li><a href="/myorder.php" class="cancel_resion-btn" style="width:90%;">
<button class="cancel_btnstyle" data-role="none" style="background:#cfcfcf;">Cancel</button></a></li>
<input type="hidden" name="order_id" id="order_id" value="<?php echo $_REQUEST['orderId'];?>">
</ul>
</div>
</div>
</form>
  
</div>
<?php require_once('common/bottom.php'); ?>