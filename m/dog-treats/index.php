<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top-new.php'); ?>
<title> Feed Your Dog The Real Love</title>
       	 <script>
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 200,
    minSlides: 2,
    maxSlides: 8,
    slideMargin: 15
  });
});
</script>
<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    
    
   <link type="text/css" rel="stylesheet" href="css/dog-treats.css?v=8" />
<link rel="canonical" href="https://m.dogspot.in/dog-treats/" />


   <!-- treats header start-->
<div class="dog-treats-header">
<h1>Feed Your Dog the real Love</h1>
<div class="cont980">

<div class="treats-slider-sec">
<div class="slider1">
  <div class="slide"><a data-ajax="false" href="https://m.dogspot.in/petspot-duck-slices-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-83089324314762571221.jpg">
  <h3> Duck Slices</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false" href="https://m.dogspot.in/petspot-duck-slices-100-gm/">Buy Now</a></div>
  </div>
     <div class="slide"><a data-ajax="false" href="https://m.dogspot.in/petspot-softduck-jerky-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-127499705114762572721.jpg">
  <h3> Soft Duck Jerky</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false" href="https://m.dogspot.in/petspot-softduck-jerky-100-gm/">Buy Now</a></div>
  </div>
  
   <div class="slide"><a data-ajax="false"  href="https://m.dogspot.in/petspot-soft-chicken-slices-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-146534228614762584742.jpg">
  <h3>Soft Chicken Slices</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-soft-chicken-slices-100-gm/">Buy Now</a></div>
  </div>
 
    <div class="slide"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chickenjerky-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-61915255714762582161.jpg">
  <h3>Chicken Jerky</h3></a>
   <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chickenjerky-100-gm/">Buy Now</a></div>
  </div>
    <div class="slide"><a data-ajax="false"  href="https://www.dogspot.in/petspot-mini-duck-slices-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-204164508614762567681.jpg">
  <h3>Duck Sandwich</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-mini-duck-slices-100-gm/">Buy Now</a></div>
  </div>
    <div class="slide"><a data-ajax="false"  href="https://www.dogspot.in/petspot-duck-sandwich-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-118079571014762576501.jpg">
  <h3>Mini Duck Slices</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-duck-sandwich-100-gm/">Buy Now</a></div>
  </div>
    <div class="slide"><a data-ajax="false"  href="https://m.dogspot.in/petspot-mini-chicken-slices-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-159820535914762578191.jpg">
  <h3> Mini Chicken Slices</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-mini-chicken-slices-100-gm/">Buy Now</a></div>
  </div>
  <div class="slide"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chicken-sandwich-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-159820535914762578191.jpg">
  <h3> Mini Chicken Slices</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chicken-sandwich-100-gm/">Buy Now</a></div>
  </div>
  <div class="slide"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chicken-sandwich-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-72210916214762579880.JPG">
  <h3>Chicken Sandwich</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chicken-sandwich-100-gm/">Buy Now</a></div>
  </div>
   <div class="slide"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chickencakes-100-gm/">
  <img src="https://www.dogspot.in/imgthumb/300x280-201302261714762580741.jpg">
  <h3>Chicken Cakes</h3></a>
  <p>Rs. 325</p>
  <div class="buy-now-btn"><a data-ajax="false"  href="https://m.dogspot.in/petspot-chickencakes-100-gm/">Buy Now</a></div>
  </div>
</div>
</div>
</div>

</div>
<!-- treats header end-->
<!--eat back gurrenty section-->
<div class="eat-back">
<div class="cont980">
"If your dog will not eat it. Send us back we will eat it." 
<div><img src="images/eat-back.jpg" style="    vertical-align: middle;" /></div>
</div>

</div>
<!--eat back gurrenty section end-->
<!-- about the products-->
<div class="about-dog-treats">
<div class="cont980">

<h3>About the products</h3> 
<p class="about-dog-treats-p-b">
Since dogs are our extended family we understand the love and care that they need.
Petspot brings 100%; REAL CHICKEN and DUCK Treats Which are highly palatable for your dogs to go bonkers. We offer the largest range of REAL Meats Treats.</p>
<p >These are easy to chew and suitable for dogs of all breed and age. The treats are completely natural, free of sugar, artificial coloring and are rich in protein to keep your dog healthy and nutritious.</p>
</div>
</div>
<!-- about the products-->
<div class="benifits-sectiion">
<div class="cont980"><div class="benifits-sectiion-bg">
<h3>BENEFITS </h3>

<div class="benifits-sec">
<ul>
<li>
<strong>01- Rich in Nutrients</strong>- Good for overall health 

</li>
<li><strong>02- High Palatable & Easy to Chew</strong>- Good for Teeth</li>
<li><strong>03- Free of Sugar</strong>-  Good for Coat & Hair</li>
<li>
<strong>04- Rich in Protein</strong>- Good for Muscles</li>
</ul>
</div>
</div>
</div>

</div>
<!-- benifits section-->

<!--benifits section end-->
<!-- comparision-->
<div class="comparision-section">
<div class="cont980">
<h3 style="text-align:center">EXPLORE &amp; BUY NOW</h3>
<div class="treats-sec">
<ul class="duck-n-chicken">
		
      <li>
     <a href="https://m.dogspot.in/duck-treats/"> 
     <img src="https://www.dogspot.in/dog-treats/images/duck-treats.jpg" width="" height="" style="max-width:100%;"></a>
      </li>  
      <li>
      <a href="https://m.dogspot.in/chicken-treats/">
      <img src="https://www.dogspot.in/dog-treats/images/chicken-treats.jpg" width="" height="" style="max-width:100%;">
      </a>
      </li>
        
        
	</ul>
 
	




</div>

</div>
</div>
<!--comparision end-->
<!-- bootstrap footer-->
<div class="dog-treats-footer">
<div class="cont980"><h3>Contact Us</h3>
<div><img src="images/pet-spot-b.png" width="140" height="42" /></div>
<!--<div class="dog-treats-footer-l">
<form class="form-control">
<div> <input type="text" placeholder="First Name" />

</div>
<div> <input type="text" placeholder="Last Name" /></div>
<div> <input type="text" placeholder="Email Id" /></div>
<div> <input type="text" placeholder="Phone No" /></div>
<div>
<textarea placeholder="Message">
</textarea>
</div>
<div>
<input type="submit" value="submit" class="btn-treats" />
</div>
</form>
</div>-->
<div class="dog-treats-footer-r">
<div>
<h3>Call Us:</h3>
<p>Customer Care +91- 9212196633</p>
<p>Timing: 9AM to 6 PM- Mon to Sat</p>
<p>(Standard Calling Charges Apply)</p>

</div>
<div>
<h3>Mail Us:</h3>
<p>Marketing Alliances- marketing@dogspot.in</p>
</div>
<div>
<h3>Reach Us:</h3>
<p>PetsGlam Services Pvt Ltd.</p> 
<p>Plot no. 2017, Sukhrali Enclave</p>
<p>Atul Kataria Chowk</p>
<p>Old Delhi Road</p> 
<p>Gurgaon- 122001, Haryana</p></div>
</div>

</div>
</div>
<!--- bootstrap footer end-->
   

	   
	   
  
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
<!-- cart page start--> 

