<?php

require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
//require_once(SITEMAIN_URL.'/functions.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection = "HTML Sitemap";
$session_id = session_id();


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">



<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>DogSpot Sitemap</title>
<meta name="keywords" content="DogSpot Sitemap" />
<meta name="description" content="Navigate all Pages of Dog Products, Photos, Brands and Breeds of Dog through DogSpot Sitemap." />

<?php require_once('common/script.php'); ?>
<script type="text/javascript" src="https://www.dogspot.in/new/js/sitemap-jquery.js"></script>

<script type="text/javascript"> 
$(document).ready(function(){
$(".paperflip").click(function(){
	   $(".paper").slideToggle("slow");
  });
  
  $(".filmflip").click(function(){
    $(".film").slideToggle("slow");
  });
  
  $(".wovenflip").click(function(){
    $(".woven").slideToggle("slow");
  });
  
  $(".eventflip").click(function(){
    $(".event").slideToggle("slow");
  });
  
   $(".shopflip").click(function(){
    $(".shop").slideToggle("slow");
  });
  
   $(".microflip").click(function(){
    $(".micro").slideToggle("slow");
  });
  $(".trendflip").click(function(){
    $(".trend").slideToggle("slow");
  });
   $(".photoalbum").click(function(){
    $(".photo").slideToggle("slow");
  });
   $(".shopbrands").click(function(){
    $(".brands").slideToggle("slow");
  });
});


</script>
      <?php require_once('common/top.php'); ?>
<style>
table {width: 100%;  border-collapse: collapse; }
th {background:#ddd;color: #fff; font-weight:normal;font-size:16px;}
td { width:20%;}
td, th {padding: 10px !important;  border: 1px solid #ccc;  text-align: left;  font-weight:normal;  font-size:12px;}
.header a{ text-decoration:none;}
.paperflip h1{background: #ddd;float: left;width: 100%;padding: 10px;}
.paperflip h3, .filmflip h3, .wovenflip h3, .shopflip h3, .shopbrands h3{background: #ddd;  float: left; width: 100%; padding: 10px; }
tr:nth-of-type(odd){ background:#fff !important; border-top:1px solid #ddd; border-bottom:1px solid #ddd;}

</style>
  <!-- breadcrumb -->
	
 <div class="breadcrumb">
            	<div class="header" style="height:0;">
                <div class="cont980"> 
                    <div class="fl">
                        <p align="left"><a href="/" style="text-decoration:none;">Home</a> &gt; Sitemap</p>
                     </div>
                 
                     <!--<div class="fr">
              <p align="right">
                        <a href="https://www.facebook.com/indogspot">
                        <img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a>
                        <a href="https://twitter.com/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  />
                        </a></p>
                        </div>
                        -->
                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->  
<div class="cont980"> 
<div class="paperflip" >

     <p><h1 >Dog Sitemap</h1></p>
     <hr>
     <div class="paper">
      <table>
<thead>
<tr>
      <? $result1 = mysql_query("SELECT breed_name,nicename FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC ");
	  $s=0;
while($rowArt1 = mysql_fetch_array($result1)){     ?>
    <td ><a href="/<?=$rowArt1['nicename']?>/"><?=ucwords(strtolower($rowArt1['breed_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>


<? // sitemap for articles ?>
     <div class="paperflip" style="margin-top:20px;" >
     <p><h3>Articles</h3></p>
     <hr>
     <div class="paper">
      <table>
<thead>
<tr>
      <? 
	 // mysql_connect("localhost", "dogspot", "india123dogs");
//mysql_select_db("test");
	  $result1 = mysql_query("SELECT DISTINCT(wptr.term_taxonomy_id) , wtt.term_id , wt.name , wt.slug FROM wp_term_relationships as wptr , wp_term_taxonomy as wtt , wp_terms as wt WHERE wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wt.term_id=wptr.term_taxonomy_id");
	  $s=0;
while($rowArt1 = mysql_fetch_array($result1)){     ?>
    <td ><a href="https://m.dogspot.in/dog-blog/<?=$rowArt1['slug'];?>/"><?=ucwords(strtolower($rowArt1['name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      <br />
      <div class="filmflip" >
     <p><h3 > Qna</h3></p>
     <hr>
     <div class="film">
      <table>
<thead>
<tr>
      <? 	  //mysql_connect("localhost", "dogspot", "india123dogs");
//mysql_select_db("dogspot_dogspot");
	   $result3 = mysql_query("SELECT cat_nicename,cat_desc FROM qna_cat ");
	  $s=0;
while($rowArt3 = mysql_fetch_array($result3)){     ?>
    <td ><a href="https://m.dogspot.in/qna/<?=$rowArt3['cat_nicename'];?>/"><?=ucwords(strtolower($rowArt3['cat_desc']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      <br />
      <div class="wovenflip" >
     <p><h3 > Business</h3></p>
     <hr>
     <div class="woven">
      <table>
<thead><tr>

<tr>
      <? $result4 = mysql_query("SELECT category_name,cat_nice_name FROM business_category ");
	  $s=0;
while($rowArt4 = mysql_fetch_array($result4)){     ?>
    <td ><a href="https://m.dogspot.in/dog-listing/category/<?=$rowArt4['cat_nice_name'];?>/"><?=ucwords(strtolower($rowArt4['category_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      <br />

      <div class="shopflip" >
     <p><h3 > Shop</h3></p>
     <hr>
     <div class="shop">
     <table>
<thead>
<tr>
      <? 
	  $result2 = mysql_query("SELECT category_name,category_nicename FROM shop_category");
	  $s=0;
while($rowArt2 = mysql_fetch_array($result2)){     ?>
    <td ><a href="https://m.dogspot.in/<?=rtrim($rowArt2['category_nicename']);?>/"><?=ucwords(strtolower($rowArt2['category_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      
  <br />
  <div class="shopbrands" >
     <p><h3 > Brands</h3></p>
     <hr>
     <div class="brands">
     <table>
<thead>
<tr>
      <? $result_brand = mysql_query("SELECT brand_name,brand_nice_name FROM shop_brand WHERE brand_status='1'");
	  $s=0;
while($rowArt_brand1 = mysql_fetch_array($result_brand)){     ?>
    <td ><a href="https://m.dogspot.in/<?=$rowArt_brand1['brand_nice_name'];?>/"><?=ucwords(strtolower($rowArt_brand1['brand_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      
    <div class="shopbrands" >


     <p><h3> Search Trends</h3></p>
     <hr>
     
      <table>
<thead>
<tr><td ><a href="/dogs-for-adoption/"  title="Dogs for adoption">Dogs For Adoption</a></td>
<td ><a href="/puppies-for-adoption/"  title="Puppies for adoption">Puppies for Adoption</a></td>
<!--<td ><a href="/dogs-for-adoption-by-city/"  title="Dogs For Adoption By City">Dogs For Adoption By City</a></td>-->
<td ><a href="/puppies-for-adoption-by-city/"  title="Puppies For Adoption By City">Puppies For Adoption By City</a></td>

</tr>
<tr>

<!--<td ><a href="/breed-puppies-for-adoption/"  title="Puppies Breeds for adoption  with city">Puppies Breeds For Adoption By City</a></td>-->
<!--<td ><a href="/breed-for-adoption/"  title="Breeds for adoption  with city">Breeds For Adoption  By City</a></td>-->
<td ><a href="/search-breed-for-adoption/"  title="Breeds for adoption">Breeds For Adoption</a></td>
<td ><a href="/search-breed-dogs-for-adoption/"  title="Dogs Breeds for adoption">Dogs Breeds For Adoption</a></td>
<td ><a href="/search-breed-puppies-for-adoption/"  title="Puppies Breeds for adoption">Puppies Breeds For Adoption</a></td>
<td ><a href="/cats-for-adoption/"  title="Cats for adoption">Cats For Adoption</a></td>
<td ><a href="/search-cats-for-adoption/"  title="Cats for adoption with city">Cats For Adoption By City</a></td>
</tr><tr>

<!--<td ><a href="/breed-dog-for-adoption/"  title="Dogs Breeds for adoption with city">Dogs Breeds For Adoption By City</a></td>-->
<td ><a href="/adoption-for-city/"  title="Adoption By City">Adoption By City</a></td>
<td ><a href="/dog-names/"  title="Dog Names">Dog Names</a></td>
</tr>

      </thead></thead></table>
      
      
      
       
      
      
      
      
     
    	</div> <!-- main container -->
	<?php require_once('common/bottom.php'); ?>