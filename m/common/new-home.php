<?php 

include_once("constants.php");
include_once("database.php");
//echo SITEMAIN_URL;
include_once(SITEMAIN_URL."/shop/functions.php");
include_once(SITEMAIN_URL."/shop/arrays/arrays.php");
include_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
include_once(SITEMAIN_URL."/functions.php");
include_once(SITEMAIN_URL."/functions2.php");
include_once(SITEMAIN_URL.'/session.php');
$sitesection = 'HOME';
if($utm_source=="sbibuddy")
{
 setcookie("sbibuddy", 'sbi', time()+3600*24*30, "/"); /* expire in 30 Days */
}
?>

 <?php require_once('common/script.php'); ?>
<!DOCTYPE html>
<html>
<head>
<title>DogSpot.in-Online Pet Supplies Store | Shop for Dog,Cat,Birds Products</title>

<meta name="keywords" content="DogSpot.in, Online Pet Supplies Store, Dog Store, Cat shop, Birds products, pet shopping India, puppies shop, cat supplies, dog shop online." />
<meta name="description" content="DogSpot.in Pet Supplies Store - India's best online shopping site for pets supplies, accessories & products for pets. Shop for Dog, Cat, Birds and small pets at great price from anywhere." />
<link href="https://www.dogspot.in" rel="canonical">
 
  <?php require_once('common/top-new.php'); ?>
       	<div >      
    <!--slider space-->
  <div class="banner-slider-top">
    <div class="device">
				<div class="slider1-container">
                <?php  include('includes/home/home-banner-images.php');  ?>
				</div>
                
			</div>
            <div class="pagination"></div>
			<!-- homepage banner container end  -->
              <style>
	

	
            </style>
    </div>
	<!-- shop by  category start-->
	
	<!-- shop by category start-->
	<!-- shop by category-->
	<div class="cat-90">
<div class="cont980">
<div id="jcl-demo">
<div class=" cateogry-cont">
        <div class="p-text-h">
        <div class="p-text-l">Shop by Category</div>
        <div class="p-text-arrow"><a href="#" class="prev" data-ajax="false"><img src="images/next-prv-arrow.jpg" /></a> <a href="#" class="next" data-ajax="false"><img src="images/next-prv-arrow.jpg" class="rotate-icon" /></a>
        </div>
        </div>
        
        <div class="category-slider ">
            <ul>
                <li class="">
                <a href="/dog-food/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-food-cat.jpg" />
              </div>
<div class="banner-cat-t"><strong>Food</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div>
                </a>
				 <a href="/biscuits-treats/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-treats.jpg" />
              </div>
<div class="banner-cat-t"><strong>DOGS TREATS</strong><div class="cat-off">Upto <span>25%</span>  <br/>OFF</div></div>
                </a>
                </li>
                <li>
                   <a href="/health-wellness/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/health-n-wellness.jpg" />
              </div>
<div class="banner-cat-t"><strong>Health & <br />Wellness</strong></div>
                </a>
				 <a href="/dog-toy/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/toys.jpg" />
              </div>
<div class="banner-cat-t"><strong>Toys</strong> UPTO <span>60%</span>  OFF</div>
                </a>
				</li>
                <li>
                   <a href="/collars-leashes/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/collar-n-leash.jpg" />
              </div>
<div class="banner-cat-t"><strong>Collars & <br />Leashes</strong></div>
                </a>
				 <a href="/dog-grooming/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/groming.jpg" />
              </div>
<div class="banner-cat-t"><strong>Grooming</strong> UPTO <span>60%</span>  OFF</div>
                </a>
				</li>
                <li>
                   <a href="/dog-bowls/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/bowls-n-fedders.jpg" />
              </div>
<div class="banner-cat-t"><strong>Bowls & Feeders</strong></div>
                </a>
				 <a href="/flea-ticks/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/flea-ntcks.jpg" />
              </div>
<div class="banner-cat-t"><strong>FLEA & TICKS</strong></div>
                </a>
				</li>
                <li>
                   <a href="/crates-beds/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/crates-n-cages.jpg" />
              </div>
<div class="banner-cat-t"><strong>CRATES & CAGES</strong></div>
                </a>
				 <a href="/dog-clean-up/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-cleaning.jpg" />
              </div>
<div class="banner-cat-t"><strong>Dog CLEANING</strong></div>
                </a>
				</li>
                
                <li>
                <a href="/dog-training-behavior/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/tranning.jpg" />
              </div>
<div class="banner-cat-t"><strong>Training</strong></div>
                </a>
                   <a href="/clothing-accessories/" data-ajax="false">
              <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/cloth.jpg" />
              </div>
<div class="banner-cat-t"><strong>CLOTHING</strong> <div class="cat-off">Upto <span>80%</span>  <br/>OFF</div></div>
                </a>
				 
				</li>
                
           </ul>
        </div>

       
        <div class="clear"></div>
    </div>




</div>
</div>
</div>
	<!-- shop by category end-->
	<div class="cat-90">
<div class="cont980">
<div id="jcl-demo">
<div class="custom-container mixedContent">
        <div class="p-text-h">
        <div class="p-text-l">Hot Selling Products</div>
        <div class="p-text-arrow"> <a href="#" class="prev" data-ajax="false"><img src="images/next-prv-arrow.jpg" /></a> <a href="#" class="next" data-ajax="false"><img src="images/next-prv-arrow.jpg" class="rotate-icon" /></a></div></div>
        
        <div class="carousel">
               <ul>
               <? $item_idSell=hotSelling();
	   foreach($item_idSell as $item){$qItemq=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$type_id1=$qItemq["type_id"];
					$name=$qItemq["name"];
					$domain_id=$qItemq["domain_id"];
					$stock_status=$qItemq["stock_status"];
					$price=number_format($qItemq["price"],0);
					$selling_price=number_format($qItemq["selling_price"],0);
					$discountPer=number_format(($selling_price-$price)/$selling_price*100,0);	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
					$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
					$nice_name=$qItemnice["nice_name"];	
					$rowdatM = mysql_fetch_array($qdataM);
				if($rowdatM["media_file"]){
		//$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		//$imageURL='/imgthumb/250x250-'.$rowdatM["media_file"];
		$src = $imageBasePath.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL=$absPath.'/imgthumb/300x280-'.$rowdatM["media_file"];
		$dest = $imageBasePath.'/imgthumb/300x280-'.$rowdatM["media_file"];
	}else{
		//$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		//$imageURL='/shop/image/no-photo-t.jpg';
		$src = $imageBasePath.'/shop/image/no-photo-t.jpg';
		$imageURL=$absPath.'/shop/image/no-photo-t.jpg';
		$dest = $imageBasePath.'/shop/image/no-photo-t.jpg';
	}
     createImgThumbIfnot($src,$dest,'300','280','ratiowh');
?>
                <li>
                <div class="p-slide-img">
                <a href="/<?=$nice_name?>/" data-ajax="false"><img src="<?=$imageURL?>" class="p-s-image" title="<?=$name?>" alt="<?=$name?>" 
                width="" height=""></a>
                </div>
                <div class="product-slider-link"><a href="/<?=$nice_name?>/ " data-ajax="false"><?=$name?></a></div>
               <? if($selling_price>$price){?> <div class="product-price-s"> <span class="p-ds-off">₹<?=$price?></span><span class="p-d-price"><del>₹<?=$selling_price?></del></span> <span class="p-d-off"><?=$discountPer?>%</span></div> <? }else{?><div class="product-price-s"> <span class="p-ds-off">₹<?=$price?></span></div><? }?>
                <div></div>
                </li>
                <? }?>
              </ul>
        </div>

       
        <div class="clear"></div>
    </div>



 

</div>
</div>
</div>
    <!-- slider  space end-->
           <?  if($userid=='Guest'){
	    $countuser=query_execute("SELECT item_id FROM user_items_history WHERE user_ip='$ip' ORDER BY id DESC"); 
	   }elseif($userid !='Guest')
	   {
		   $countuser=query_execute("SELECT item_id FROM user_items_history WHERE userid='$userid' ORDER BY id DESC");  
	   }
	  // echo "SELECT item_id FROM user_items_history WHERE userid='$userid' ORDER BY id DESC";
$c_count=mysql_num_rows($countuser);
if($c_count>3)
{
	//echo "SELECT item_id FROM user_items_history WHERE (userid='$userid' OR user_ip='$ip') AND userid !='Guest' ORDER BY id DESC";
	?>
    <div class="cat-90">
<div class="cont980">
<div id="jcl-demo">
<div class="recentView">
        <div class="p-text-h">
        <div class="p-text-l">Recently Viewed</div>
        <div class="p-text-arrow"> <a href="#" class="prev" data-ajax="false"><img src="images/next-prv-arrow.jpg" /></a> <a href="#" class="next" data-ajax="false"><img src="images/next-prv-arrow.jpg" class="rotate-icon" /></a></div></div>
        
        <div class="carousel">
               <ul>
               <? while($item_idS=mysql_fetch_array($countuser))
	   {
		$item_idRec[]= $item_idS['item_id']; 
	   }
	   foreach($item_idRec as $item){$qItemq=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$type_id1=$qItemq["type_id"];
					$name=$qItemq["name"];
					$domain_id=$qItemq["domain_id"];
					$stock_status=$qItemq["stock_status"];
					$price=number_format($qItemq["price"],0);
					$selling_price=number_format($qItemq["selling_price"],0);
					$discountPer=number_format(($selling_price-$price)/$selling_price*100,0);	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
					$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
					$nice_name=$qItemnice["nice_name"];	
					$rowdatM = mysql_fetch_array($qdataM);
				if($rowdatM["media_file"]){
		//$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		//$imageURL='/imgthumb/250x250-'.$rowdatM["media_file"];
		$src = $imageBasePath.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL=$absPath.'/imgthumb/300x280-'.$rowdatM["media_file"];
		$dest = $imageBasePath.'/imgthumb/300x280-'.$rowdatM["media_file"];
	}else{
		//$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		//$imageURL='/shop/image/no-photo-t.jpg';
		$src = $imageBasePath.'/shop/image/no-photo-t.jpg';
		$imageURL=$absPath.'/shop/image/no-photo-t.jpg';
		$dest = $imageBasePath.'/shop/image/no-photo-t.jpg';
	}
     createImgThumbIfnot($src,$dest,'300','280','ratiowh');
?>
                <li>
                <div class="p-slide-img">
                <a href="/<?=$nice_name?>/" data-ajax="false"><img src="<?=$imageURL?>" class="p-s-image" title="<?=$name?>" alt="<?=$name?>" 
                width="" height=""></a>
                </div>
                <div class="product-slider-link"><a data-ajax="false" href="/<?=$nice_name?>/ "><?=$name?></a></div>
               <? if($selling_price>$price){?> <div class="product-price-s"> <span class="p-ds-off">₹<?=$price?></span><span class="p-d-price"><del>₹<?=$selling_price?></del></span> <span class="p-d-off"><?=$discountPer?>%</span></div> <? }else{?><div class="product-price-s"> <span class="p-ds-off">₹<?=$price?></span></div><? }?>
                <div></div>
                </li>
                <? }?>
              </ul>
        </div>

       
        <div class="clear"></div>
    </div>



 

</div>
</div>
</div>
<? }?>
    <!-- shop by brands section start-->
    <div class="shop-by-brands-sec">
    <style>.shop-by-brands-sec{ float:left; width:100%; background:#fafafa; padding:10px 0px;}
	.shop-by-brands-sec .phtext-h{ width:90%; margin:0px 5%;}
	.shop-by-brands-sec ul{ float:left; width:90%; margin:0px 5%; list-style:none;}
	.shop-by-brands-sec ul li{ float:left; width:33.3%;}
	.shop-by-brands-sec ul li img{ max-width:100%; min-width:70px;}
    </style>
    <div class="p-text-h">
        <div class="p-text-l"><span style="color: #ff8a00;">Exclusive</span> International Pet Brands</div>
        </div>
        <ul>
        <li>
        <a href="/spotty/" data-ajax="false"><img src="/images/spoty.png"></a>
        </li>
          <li>
        <a href="/furminator/" data-ajax="false"><img src="/images/furminator-1.png"></a>
        </li>
        
        
        <li>
        <a href="/urine-off/" data-ajax="false"><img src="/images/urine-off.png"></a>
        </li>
        </ul>
    
    </div>
    <!-- shop by brands section end-->
    

	   </div>   
	   <script src="js/jquery.jcarousellite.min.js"></script>

         
     
     <script type="text/javascript">
        $(function() {
            $(".cateogry-cont .category-slider").jCarouselLite({
                btnNext: ".cateogry-cont .next",
                btnPrev: ".cateogry-cont .prev"
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $(".mixedContent .carousel").jCarouselLite({
                btnNext: ".mixedContent .next",
                btnPrev: ".mixedContent .prev"
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $(".recentView .carousel").jCarouselLite({
                btnNext: ".recentView .next",
                btnPrev: ".recentView .prev"
            });
        });
    </script>
       	   <?php require_once('common/bottom-new.php'); ?>
	   <?php require_once('common/bottom.php'); ?>
		
  