<?
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
//require_once(SITEMAIN_URL.'/functions.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
$ch = curl_init();
$url='https://www.dogspot.in/mobile-webservice/android/native-api/getTopProductsListing.php';
$type=$_REQUEST['request_type'];
$title='';
if($type=='sale')
{
header("Location: /sales/");
exit();	
}
if($type=='newArrival'){
	$DATA='mode=DogSpotAppAuth&product_type=all_new_arrival&product_type_id=2';
	$title='New Arrival Product';
}else if($type=='youMayLike'){
	$DATA='mode=DogSpotAppAuth&product_type=all_feature_product&product_type_id=4';
	$title='You May Like Product';
}else if($type=='sale'){
	$DATA='mode=DogSpotAppAuth&product_type=all_sale_item&product_type_id=6';
	$title='Sale Product';
}else{
	$DATA='mode=DogSpotAppAuth&product_type=all_new_arrival&product_type_id=2';
	$title='What Others Are Buying Products';
}

if (!$ch){
	die("Couldn't initialize a cURL handle");
}
$ret = curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt ($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt ($ch, CURLOPT_POSTFIELDS,"$DATA");
$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$curlresponse = curl_exec($ch);
$allData=json_decode($curlresponse);
$data=$allData->data;
?>

<html>
<head>
	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
	<div  class="p-one-em myorder_page27">
	 <?php require_once('common/script.php'); ?>
		<script type="text/javascript">
		  var totalProducts='<?=sizeof($data)?>';
		  var urlload='/feature_products_loader.php';	  
		  var track_load=1;
		  var type='<?=$type?>';
		  var loading  = false;
			$(document).ready(function(){
			 $(window).scroll(function() {
					var height = $(window).height();
					var pageheight = $(document).height();
					var top = $(window).scrollTop() + 1000;
					if(top + height >= pageheight){
						  if(!loading){ 
								 loading = true;			
								 $('#wait_section').show();
								 $.post(urlload,{'group_no': track_load,'type':type}, function(data){									
									 if($.trim(data)!= ''){	
										$("#loader_result").append(data);
										track_load++; 
										loading = false; 	
									 }else{					 	
										$('#wait_section').hide();
									 }						 
								 }).fail(function(xhr, ajaxOptions, thrownError) {
									  $('#wait_section').hide();
									  loading = false;				
								 });				
						 }
					 } 
			  });
			});
			
		</script>
<?php require_once('common/top.php'); ?>
	<?php /*?><div id="title" style="text-align:center;"><?=$title?></div><?php */?>
	<div id="error" style="display:none;text-align:center;color:red;"></div>
	<div class="ui-panel-wrapper" id="loader_result">
	  <? if(sizeof($data)>0 && $allData->status=='success'){ $i=0; foreach($data as $key=>$val){ ?>
		   <div data-role="content" class="ui-content" role="main">
				<div class="sol_blk">
					<ul class="sol_listing groom">
						<li>
                        	<?php $nicename = str_replace("https://www.dogspot.in/","/",$val->product_nicename); ?>
							<div class="img_blk"><a href="<? echo $nicename?>" class="ui-link" data-ajax="false"><img title="" alt="" src="<?=$val->product_image?>"></a></div>
							<div class="txt_blk">
                            	
								<a href="<? echo $nicename?>" class="ui-link" data-ajax="false"><?=$val->product_name?></a>
								<p><span><i class="fa fa-inr"></i></span>  <?=$val->retail_price?></p>
								<?
								$mrp=$val->retail_price;
								$price=$val->sale_price;
								if($price>$mrp){?>
								<small>
						        <i class="fa fa-inr"></i><strike><?=$val->sale_price?></strike>&nbsp;<?=$val->saving_percentage?>% off</small>
								<?} ?>
							</div>
						</li>
					</ul>
				</div>
			</div>
	  <? $i++;}}else{?>
		  <div id="apiError" style="text-align:center;">Opps! result not found.</div>
	  <? } ?>
	 </div>
	 <div style="text-align:center;" id="wait_section"><img src="/images/ajax_loader.gif" width="100" height="100"></div>
	<?php require_once('common/bottom.php'); ?>