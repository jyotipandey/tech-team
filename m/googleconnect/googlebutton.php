<?	
require_once ($DOCUMENT_ROOT.'/googleconnect/openid.php');
$openid = new LightOpenID("https://www.dogspot.in");
 
$openid->identity = 'https://www.google.com/accounts/o8/id';
$openid->required = array(
  'namePerson/first',
  'namePerson/last',
  'contact/email',
   'birthDate',
   'person/gender', 
    'contact/postalCode/home',
	'contact/country/home',
	'pref/language',  
	 'pref/timezone',
);
$openid->returnUrl = 'https://www.dogspot.in/googleconnect/glogin.php'
?>
 
<a href="<?php echo $openid->authUrl() ?>">
<img src="/new/common/images/transparent.gif" width="1" height="1" class="sprite loginSocial_sm gm_sm" />
</a>
