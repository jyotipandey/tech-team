<?php
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");
if($section[0]==''){
	require_once("../constants.php");
}else
{
/*error_reporting(E_ALL);
ini_set("display_errors", 1);
*/require_once("constants.php");	
}
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
//require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$page='photo';
if($breed)
{
$breed_name=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$breed'");
$b_name=$breed_name['breed_name'];	
$title="$b_name Dog and Puppies Images | DogSpot.in";
$keys="$b_name Dog Images, $b_name Puppies Images, $b_name Dog Pictures, $b_name Dog Photos, $b_name Puppies Pictures, $b_name Puppies Photos, $b_name Pics of Puppies, $b_name Dog and Puppies Images.";
$desc="Every dog breed has a different type of appearance and by capturing these appearance at a right time we have made an great collection of images of these dogs. Come and check out the images of $b_name dog here at DogSpot.in";	
}else
{
$title="Dog Images | Puppies Photos Free Download | DogSpot.in";
$keys="Dog Images, Puppies Images, Dog Pictures, Dog Photos, Puppies Pictures, Puppies Photos, Pics of Puppies,Dog and Puppies Images";
$desc="Dogs are very important part of our life and everyone is very much interested in the images and pictures of their loved ones. So, find out the cute and adorable images of these cute dog and puppies.";	
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title?></title>
<meta name="keywords" content="<?=$keys?>" />
<meta name="description" content="<?=$desc?>" />
<meta property="fb:app_id" content="119973928016834" />
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="images" />
<meta property="og:url" content="https://www.dogspot.in/photos/" />
<meta property="og:title" content="Cute Dog and Puppies Images" />
<meta property="og:description" content="Dogs are very important part of our life and everyone is very much interested in the images and pictures of their loved ones. So, find out the cute and adorable images of these cute dog and puppies.
" />
<link rel="canonical" href="https://www.dogspot.in<?=$_SERVER[REQUEST_URI]?>" />
<link rel="amphtml" href="https://www.dogspot.in/amp<?=$_SERVER[REQUEST_URI]?>" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/<?=$_SERVER[REQUEST_URI]?>" >
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>

<link type="text/css" rel="stylesheet" href="/dog-images/css/image-style.css?=1" />
<section class="dog-image" style="margin-top:10px;"> <img src="/dog-images/images/dog-banner.jpg"   alt="dog image" title="dog image">
  <div class="image-header">
    <h1>Dog Images</h1>
    <h2>Dogs leave paw prints on your heart	&#10084;</h2>
  </div>
  <div class="breadcrumb" style="position: absolute; margin-top: -29px; z-index: 999999999">
 
  <div class="container" itemscope="" itemtype="http://schema.org/Breadcrumb" style="text-align: left;color:#fff;">
    <div class="breadcrumb_cont" itemscope="" itemtype="http://schema.org/BreadcrumbList" style="    margin-left: 18px" > 
    <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
     <a href="/" itemprop="item"><span itemprop="name" style="color:#fff;">Home</span></a>
      <meta itemprop="position" content="1"> </span>
     <span> / </span>
     <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
      <span itemprop="name" class="brd_font_bold" style="color:#f2f2f2;">Dog Photos
</span>
       <meta itemprop="position" content="2"> </span>
     </div>
    <div class="cb"></div>
    
  </div>
</div>
  <div class="image-bottom">
    <div class="container">
       <div class="product-sorting-bar" style="background:#fff;margin-bottom: -15px">
            <div>
              <div class="btn-group bootstrap-select">
              <select data-role="none" name="breed" class="selectpicker" id="breed" data-width="180px" tabindex="-98" onchange="callpage(this.value)">
              <option value="" <? if($b_name==''){?> selected="selected" <? }?> >Select Breed</option>
              </select>
			  </div>
            </div>
          </div></div> 
       <div id='loadMoreComments' style='display:none'></div>
     
      <div>
      <input type="hidden" name="user_id" id="user_id" value="<?=$userid?>"  />
      <div style="text-align:center;display:inline-block;">
<div id='div-gpt-ad-1552408890928-0' style='height:250px; width:300px;margin: 15px 24px 29px 42px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552408890928-0'); });
</script>
</div>
 </div>
        <ul id="rytPost_list" class="og-grid">
        
          <div class="rytPost_list imgtxtcontwag" >
            <? if($b_name){ ?>
				 <?	 $iid=0;
			//	 echo "SELECT pi.image,image_id,new_keywords FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%' ORDER BY image_id ASC LIMIT 12";
			 $breedI=query_execute("SELECT pi.image,image_id,new_keywords FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%' ORDER BY image_id ASC LIMIT 12");
			 $breedIList=query_execute("SELECT pi.image,image_id,new_keywords FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%'");
			 $countBreedImg=mysql_num_rows($breedIList);?><div id="txt1" style='display:none; cursor:pointer' ><?=$b_name?> images</div>
      <div id="txt2" style='display:none'><?=$countBreedImg?></div>
	   <?
			
			 while($dog_details=mysql_fetch_array($breedI)){
			 $image=$dog_details['image'];
			 $new_keywords=$dog_details['new_keywords'];
			 $image_id=$dog_details['image_id'];
			// $img="/tr:h-500,w-700/photos/images/".$image;
				$src = $imageBasePath.'/photos/images/'.$image;
			$imageURL1='/photos/images/700x500_'.$image;
			$imageURL2='/photos/images/160x114_'.$image;
		$dest = $imageBasePath.$imageURL1;
		$dest2 = $imageBasePath.$imageURL2;
	    createImgThumbIfnot($src,$dest,'700','500','ratiowh'); 
	    createImgThumbIfnot($src,$dest2,'160','114','ratiowh'); 
		
		
			 $iid++;
			 ?>
            <li class="gallery-img " id='<?=$iid?>'> <a> <img src="<?=$absPath.$imageURL2?>" alt="<?=$new_keywords?> image" title="<?=$new_keywords?> image"> </a>
             <? $wag_activity=mysql_query("SELECT * FROM photo_activity_wag WHERE image_id='$image_id' AND userid='$userid'");
$wag_like1=mysql_num_rows($wag_activity);
if($wag_like1==0){ if($userid=='Guest'){ ?>
                    <div class="like-icon-box" id="postwagbefore<?=$image_id?>">
                    <a href="/login.php?refUrl=/photos/" data-ajax="false" rel="nofollow">
                    <img src="/dog-images/images/heart-icons.png"  alt="Heart" title="Heart" style=" width: 30px; height: auto; " /></a>
                    </div>
                    <? }else{?>
                     <div class="like-icon-box" id="postwagbefore<?=$image_id?>">
                    <a onclick="wagfuncountImg('<?=$image_id?>');">
                    <img src="/dog-images/images/heart-icons.png"  alt="Heart" title="Heart" style=" width: 30px; height: auto; " /></a>
                    </div>
                   
                    <? }}else{ ?>
                      <div class="like-icon-box" id="postwagafter<?=$image_id?>">
                    <a href="javascript:void(0)" onclick="wagcountImg('<?=$image_id?>');">
                    <img src="/dog-images/images/heart-red-icon.png"  alt="Heart" title="Heart" style=" width: 30px; height: auto; " /></a>
                   </div><? }?>
 <div class="like-icon-box" id="postwagafter<?=$image_id?>" style="display:none">
                    <a onclick="wagcountImg('<?=$image_id?>');">
                    <img src="/dog-images/images/heart-red-icon.png"  alt="Heart" title="Heart"  style=" width: 30px; height: auto; " /></a>
                   </div>
 <div class="like-icon-box" id="postwagbefore<?=$image_id?>" style="display:none">
                    <a onclick="wagfuncountImg('<?=$activity_id?>');">
                    <img src="/dog-images/images/heart-icons.png"  alt="Heart" title="Heart" style=" width: 30px; height: auto; "  /></a>
                 </div><span id="wag" style="display:none"></span>
             </li>
             <? if($iid==2){?>
            
            <? } }  ?>
		 <? }else{ $iid=0;
		 $getbestImage=query_execute("SELECT image,da.dog_id,da.dog_breed,daa.id FROM dogs_activity as daa,dogs_available as da WHERE daa.dog_id=da.dog_id AND daa.dog_image !='no' AND daa.publish_status='publish' ORDER BY id ASC LIMIT 12"); 
		 ?> <div id="txt1" style='display:none; cursor:pointer' >Dog images</div>
      <div id="txt2" style='display:none'><? echo '99'; ?></div>
	   <?
		 while($dog_details=mysql_fetch_array($getbestImage)){
			 $dog_breed=$dog_details['dog_breed'];
			 $dog_id=$dog_details['dog_id'];
			 $activity_id=$dog_details['id'];
			$src = $imageBasePath.'/dogs/images/'.$dog_details['image'];
			$imageURL1='/dogs/images/700x500_'.$dog_details['image'];
			$imageURL2='/dogs/images/160x114_'.$dog_details['image'];
		$dest = $imageBasePath.$imageURL1;
		$dest2 = $imageBasePath.$imageURL2;
	    createImgThumbIfnot($src,$dest,'700','500','ratiowh'); 
	    createImgThumbIfnot($src,$dest2,'160','114','ratiowh'); 
	   $iid++;
			 ?>
            <li class="gallery-img " id='<?=$iid?>'> <a> <img src="<?=$absPath.$imageURL2?>" alt="<?=$dog_breed?> image" title="<?=$dog_breed?> image"> </a>
             <? $wag_activity=mysql_query("SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND userid='$userid' AND activity_id='$activity_id'");
$wag_like1=mysql_num_rows($wag_activity);
if($wag_like1==0){ if($userid=='Guest'){ ?>
                    <div class="like-icon-box" id="postwagbefore<?=$activity_id?>">
                    <a href="/login.php?refUrl=/photos/" data-ajax="false" rel="nofollow">
                    <img src="/dog-images/images/heart-icons.png" alt="Heart" title="Heart" style=" width: 30px; height: auto; " /></a>
                    </div>
                    <? }else{?>
                     <div class="like-icon-box" id="postwagbefore<?=$activity_id?>">
                    <a onclick="wagfuncount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/dog-images/images/heart-icons.png"  alt="Heart" title="Heart" style=" width: 30px; height: auto; " /></a>
                    </div>
                   
                    <? }}else{ ?>
                      <div class="like-icon-box" id="postwagafter<?=$activity_id?>">
                    <a href="javascript:void(0)" onclick="wagcount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/dog-images/images/heart-red-icon.png"  alt="Heart" title="Heart" style=" width: 30px; height: auto; " /></a>
                   </div><? }?>
 <div class="like-icon-box" id="postwagafter<?=$activity_id?>" style="display:none">
                    <a onclick="wagcount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/dog-images/images/heart-red-icon.png"   alt="Heart" title="Heart" style=" width: 30px; height: auto; " /></a>
                   </div>
 <div class="like-icon-box" id="postwagbefore<?=$activity_id?>" style="display:none">
                    <a onclick="wagfuncount1('<?=$activity_id?>','<?=$dog_id?>');">
                    <img src="/dog-images/images/heart-icons.png"  alt="Heart" title="Heart" style=" width: 30px; height: auto; "  /></a>
                 </div><span id="wag" style="display:none"></span>
             </li>
            
             
            <? } ?> 
           <? }?>
          </div>
        </ul>
        <div style="text-align:center;display:inline-block;">
<div id='div-gpt-ad-1552408890928-0' style='height:250px; width:300px;margin: 35px 24px 29px 42px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552408890928-0'); });
</script>
</div>
 </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
var cat_nice='';
 var c=0;
 $(document).ready(function() { 
 cat_nice=document.getElementById('txt1').innerHTML;
 countr=document.getElementById('txt2').innerHTML;
 $(document).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	if(($(document).height() - $(window).height()) - $(window).scrollTop() >= 0) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	//alert(c);
	if(c!=0){
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/dog-images/loadmore.php?b_name=<?=$b_name?>&cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	}// c condition close
		else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<a href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='position: absolute; width: 94%; height: 40px; left: 0px; text-align: center; text-transform: uppercase; color: #6c9d06; font-size: 14px; font-weight: bold; border: 2px solid #6c9d06; padding: 10px 0px; margin: 0px 3%;' >See More </div></a>");
		
	}$('#loadMoreComments').hide();
	}}

	});
	});
	function get(rt){
	$('#divw'+rt).hide();
	c=0;
    if(($(document).height() - $(window).height()) - $(window).scrollTop() >= 0 ) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c==0){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/dog-images/loadmore.php?b_name=<?=$b_name?>&cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
// c condition close-->
	}
	
	}
</script>
<script type="text/javascript">
function wagfuncount1(activity_id,dog_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/dog-images/wag_insert.php?activity_wag=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag", '',  '', 'POST', "", '','REP')
}

function wagcount1(activity_id,dog_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/dog-images/wag_insert.php?activity_wag_delete=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag", '',  '', 'POST', "", '','REP')
	
}
function wagfuncountImg(image_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/dog-images/wag_insert.php?activity_wag=1&user="+user+"&image_id="+image_id+"", "#wag", '',  '', 'POST', "", '','REP')
}

function wagcountImg(image_id){
	var user=$("#user_id").val();
	ShaAjaxJquary("/dog-images/wag_insert.php?activity_wag_delete=1&user="+user+"&image_id="+image_id+"", "#wag", '',  '', 'POST', "", '','REP')
	
}

</script>
<script src="/dog-images/js/dogimage-slider.js?v=113x" type="text/javascript"></script> 
<script type="text/javascript">
         $('.gallery-img').Am2_SimpleSlider();
</script>
<script>
function callpage(urlre)
{
//alert(urlre);	
window.location.replace(urlre);	
}
</script>
   <script>
        //When the page has loaded.
        $( document ).ready(function(){
            //Perform Ajax request.
            $.ajax({
                url: '/dog-images/getbreedata.php?b_name=<?=$b_name?>',
                type: 'get',
                success: function(data){
                    //If the success function is execute,
                    //then the Ajax request was successful.
                    //Add the data we received in our Ajax
                    //request to the "content" div.
                    $('#breed').html(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                  //  var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                  //  $('#content').html(errorMsg);
                  }
            });
        });
        </script>
 <?php require_once($DOCUMENT_ROOT .'/common/bottom-new.php'); ?>       
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
