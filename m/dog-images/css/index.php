<?php
if($section[0]==''){
	require_once("../constants.php");
}
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<title>Dog Images</title>
<script src="/dog-images/js/dogimage-slider.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="/dog-images/css/image-style.css?=20" />
<section class="dog-image">
<img src="/dog-images/images/dog-banner.jpg">
<div class="image-header"> 
<h1>Dog Images</h1>
<h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h2>
</div>

<div class="image-bottom">
<div class="container">	
			
			
            <div>
            <ul id="og-grid" class="og-grid">
				    <li class="gallery-img">
                 <a>
                    <img src="/dog-images/images/dog-images-nn.jpg" alt="img01">
                    </a>
                    <div data-desc="Image1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </div>
                
						
					</li>
					<li class="gallery-img">
						<a>
							<img src="/dog-images/images/dddd.jpg" alt="img01"/>
						</a>
                        <div data-desc="Image2. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry">
                    </div>
					</li>
                    <li class="gallery-img">
                 <a>
                    <img src="/dog-images/images/dog-images-nn.jpg" alt="img01">
                    </a>
                    <div data-desc="Image1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </div>
                
						
					</li>
					<li class="gallery-img">
						<a>
							<img src="/dog-images/images/dddd.jpg" alt="img01"/>
						</a>
                        <div data-desc="Image2. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry">
                    </div>
					</li>
					<li class="gallery-img">
                 <a>
                    <img src="/dog-images/images/dog-images-nn.jpg" alt="img01">
                    </a>
                    <div data-desc="Image1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </div>
                
						
					</li>
					<li class="gallery-img">
						<a>
							<img src="/dog-images/images/dddd.jpg" alt="img01"/>
						</a>
                        <div data-desc="Image2. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry">
                    </div>
					</li>
					<li class="gallery-img">
                 <a>
                    <img src="/dog-images/images/dog-images-nn.jpg" alt="img01">
                    </a>
                    <div data-desc="Image1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </div>
                
						
					</li>
					<li class="gallery-img">
						<a>
							<img src="/dog-images/images/dddd.jpg" alt="img01"/>
						</a>
                        <div data-desc="Image2. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry">
                    </div>
					</li>
				</ul>
			</div>
            <div  class="box-load-more">
                    
                    
                    <a href="#">Load More Images</a>
                    
                    </div>
            
</div>
</div>
 </section>
<script type="text/javascript">
         $('.gallery-img').Am2_SimpleSlider();
</script>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>


