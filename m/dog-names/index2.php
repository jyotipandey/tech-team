<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
if($section[0]==''){
require_once("../constants.php");
}
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/session.php');
//require_once(SITEMAIN_URL.'/banner1.php');

$ecomm_pagetype='articles'; 
$sitesection = "dog-names";
$exp_value=explode('-', $section[1],2);
$dog_breed_name=query_execute_row("SELECT breed_name, nicename FROM dog_breeds WHERE nicename='$breed_nname'");
$dog_breed_name_sex=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
$sex_bred=$dog_breed_name_sex['breed_name'];
$breed=$dog_breed_name['breed_name'];
$br_nicename=$dog_breed_name['nicename'];
$breednice=$breeddetails['nicename'];
if(($exp_value[0]=="male" || $exp_value[0]=="female") && $exp_value[1]!=""){
$select_sex_options=query_execute("SELECT dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND breed_nicename='$exp_value[1]' AND dog_sex!='' GROUP BY dog_sex");
}else{
$select_sex_options=query_execute("SELECT dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex!='' GROUP BY dog_sex");
}

if($exp_value[0]=="male"){
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex='M' AND (breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND breed_nicename!='bull-terrier-miniature' AND  breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') GROUP BY breed_nicename ORDER BY dog_breed ASC");
}elseif($exp_value[0]=="female"){
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1'  AND dog_sex='F' AND (breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND breed_nicename!='bull-terrier-miniature' AND  breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') GROUP BY breed_nicename ORDER BY dog_breed ASC");
}else{
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND (dog_breed!=''  AND breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND  breed_nicename!='bull-terrier-miniature' AND breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY breed_nicename ORDER BY dog_breed ASC");
}
if($sex=="male"){$sex="M";}if($sex=="female"){$sex="F";}
if(strlen($section[1])=='1' && !is_nan($section[1])){
$sc_dog_query=" AND dog_name LIKE '$section[1]%'";	
}elseif($sex!='' && $breed==''){
$sc_dog_query=" AND dog_sex='$sex'";
$option_sex=$sex;	
}elseif($sex=='' && $breed!=''){
if($br_nicename=='bull-terrier'){
$sc_dog_query=" AND (breed_nicename='bull-terrier' OR breed_nicename='bull-terrier-miniature')";	
}elseif($br_nicename=='kombai'){
$sc_dog_query=" AND (breed_nicename='kombai' OR breed_nicename='combai')";		
}elseif($br_nicename=='dachshund-miniature-long-smooth-wire-haired'){
$sc_dog_query=" AND (breed_nicename='dachshund-miniature-long-haired' OR breed_nicename='dachshund-miniature-smooth-haired' OR breed_nicename='dachshund-miniature-long-smooth-wire-haired')";		
}elseif($br_nicename=='dachshund-standard-long-smooth-wire-haired'){
$sc_dog_query=" AND (breed_nicename='dachshund-standard-long-haired' OR breed_nicename='dachshund-standard-smooth-haired' OR breed_nicename='dachshund-standard-long-smooth-wire-haired' OR breed_nicename='dachshundstandard(smoothhaired)')";		
}elseif($br_nicename=='poodle-standard-miniature-toy'){
$sc_dog_query=" AND (breed_nicename='poodle-miniature' OR breed_nicename='poodle-standard' OR breed_nicename='poodle-toy'  OR breed_nicename='poodle-standard-miniature-toy')";		
}elseif($br_nicename=='chihuahua-long-smooth-coat'){
$sc_dog_query=" AND (breed_nicename='chihuahua' OR breed_nicename='chihuahua-long-smooth-coat')";		
}elseif($br_nicename=='spitz'){
$sc_dog_query=" AND (breed_nicename='spitz-indian' OR breed_nicename='spitz' OR breed_nicename='spitz-german')";		
}else{
$sc_dog_query=" AND breed_nicename='$br_nicename'";		
}	
$breed_n_name=query_execute_row("SELECT nicename FROM dog_breeds WHERE breed_name='$breed'");
$option_breed=$breed_n_name['nicename'];
}elseif($sex!='' && $breed!=''){
$sc_dog_query=" AND breed_nicename='$br_nicename' AND dog_sex='$sex'";
$option_sex=$sex;
$option_breed=$breed;
}elseif($exp_value[0]=="male"){
$sc_dog_query=" AND breed_nicename='$exp_value[1]' AND dog_sex='M'";
$option_sex='M';
$option_breed=$exp_value[1];
}elseif($exp_value[0]=="female"){
$sc_dog_query=" AND breed_nicename='$exp_value[1]' AND dog_sex='F'";
$option_sex='F';
$option_breed=$exp_value[1];
}elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[1]!=''){
$sc_dog_query=" AND breed_nicename='$breed_nname'";
$option_breed=$breed_nname;	
}else{
	$sc_dog_query=" AND dog_name LIKE 'a%'";
	$option_sex='';
$option_breed='';
}
//echo "SELECT dog_name, breed_nicename, dog_breed, dog_nicename, dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY dog_name ORDER BY dog_name ASC";
$seach_dog_filter=query_execute("SELECT dog_name, breed_nicename, dog_breed, dog_nicename, dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY dog_name ORDER BY dog_name ASC");

$url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
/*$pattern = 'm.dogspot.in';
$abc="www.dogspot.in";
$surl= str_replace($pattern,$abc,$url);
$shared_query = query_execute("SELECT SUM(share_value) as val FROM share_count WHERE share_url = '$surl'");  
while ($row_shared = mysql_fetch_array($shared_query)) {
$sumT=$row_shared['val'];		
}
if($sumT==''){
$sumT=0;	
}
$sum=format_num($sumT,1);*/
if(strlen($section[1])=='1' && !is_nan($section[1])){
	$bread_crum='Starts with Letter'.' '.$section[1];
    }elseif($section[1]=="male"){
    $bread_crum='Male';
}elseif($section[1]=="female"){
	$bread_crum='Female';
	}elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[1]!=''){
	$bread_crum=$breed;
    $dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$section[1]'");	
	 $firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$section[1];
	}elseif($exp_value[0] == "male" && $exp_value[1]!=""){
		$dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
		$firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$exp_value[1];
		$bread_crum='Male'.' '.$dog_breed_name_bredcrum['breed_name'];
    }elseif($exp_value[0] == "female" && $exp_value[1]!=""){
		$dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
		$firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$exp_value[1];
		$bread_crum='Female'.' '.$dog_breed_name_bredcrum['breed_name'];
	}
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<? if($section[0]=='' && $section[1]==''){?>
<title>Dog Names | Complete list of popular dog names | Dogspot.in</title>
<meta name="keywords" content="Dog name, puppy names, cute dog names, popular dog names, cool dog names, friendly dog name, funny puppy namess" />
<meta name="description" content="Names are very important for our little furry friends. Here is the complete list of popular male and female dog names at dogspot.in" />
<? $h1_data='Dog Names'; ?>
<? }elseif($section[1]=='female'){?>
<title>Best Dog Name For Female | Popular Puppy Names for Girl</title>
<meta name="keywords" content="Dog name for females, female dog name, list of dog names for girl, best dog name for girl." />
<meta name="description" content="Search best dog names for your female puppy. Discover list of cute and funny dog names for girl." />
<? $h1_data='Dog Names for Female Puppy'; ?>
<? }elseif($section[1]=='male'){?>
<title>Best Dog Name For Male | Popular Puppy Names for Boy</title>
<meta name="keywords" content="Dog name for Males, male dog name, list of dog names for boy, best dog name for boy." />
<meta name="description" content="Search best dog names for your male puppy. Discover list of cute and funny dog names for boy." />
<? $h1_data='Dog Names for Male Puppy'; ?>
<? }elseif(strlen($section[1])=='1' && !is_nan($section[1])){?>
<title>Dog Name Start with Letter <?=ucwords($section[1])?> | Popular Puppy Names Begin with Alphabet <?=ucwords($section[1])?></title>
<meta name="keywords" content="Dog name with letter <?=ucwords($section[1])?>, <?=ucwords($section[1])?> letter Names for puppy, best dog name start with letter <?=ucwords($section[1])?>, popular puppy name with Alphabet <?=ucwords($section[1])?>" />
<meta name="description" content="Search Dog Names starts with Letter <?=ucwords($section[1])?> for your male and female puppy. Discover best and funny dog names begin with Alphabet <?=ucwords($section[1])?>." />
<? $h1_data='Dog Names Start with Letter'.' '. ucwords($section[1]); ?>
<? }elseif($exp_value[0] == "female"){?>
<title>Best Dog Names for Female <?=$sex_bred?> | Popular Puppy Names for Girl <?=$sex_bred?></title>
<meta name="keywords" content="Best Dog Names for female <?=$sex_bred;?>, <?=$sex_bred;?> female dog names, best <?=$sex_bred;?> nicknames for girl." />
<meta name="description" content="Search best dog names for your female <?=$sex_bred;?>. Discover list of cute and funny <?=$sex_bred?> dog names for girl." />
<? $h1_data='Dog Names for Female'.' '. $sex_bred; ?>
<? $h2_data='Here is the popular dog names for Female'.' '. $sex_bred; ?>
<? }elseif($exp_value[0] == "male"){?>
<title>Best Dog Names for Male <?=$sex_bred?> | Popular Puppy Names for Boy <?=$sex_bred?></title>
<meta name="keywords" content="Best Dog Names for male <?=$sex_bred?>, <?=$sex_bred?> male dog names, best <?=$sex_bred?> nicknames for boy." />
<meta name="description" content="Search best dog names for your male <?=$sex_bred?>. Discover list of cute and funny <?=$sex_bred?> dog names for boy." />
<? $h1_data='Dog Names for Male'.' '. $sex_bred; ?>
<? $h2_data='Here is the popular dog names for Male'.' '. $sex_bred; ?>
<? }elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[1]!=''){?>
<title>Best Dog Names for <?=$breed;?> | Popular Puppy Names for <?=$breed;?></title>
<meta name="keywords" content="Best Dog Names for <?=$breed;?>, <?=$breed;?> dog names, best <?=$breed;?> nicknames" />
<meta name="description" content="Search best dog names for your <?=$breed;?>. Discover list of cute and funny <?=$breed;?> dog names." />
<? $h1_data='Dog Names for'.' '. $breed; ?>
<? }?>
<?
if($section[1]==''){ ?>
<link rel="next" href="/dog-names/a/1/">	
<?	}
 if($titlepage>1){ ?>
<link rel="prev" href="/dog-names/<? if($section[1]!=''){ ?><?=$section['1'];?>/<? echo ($titlepage-1)?>/<? }?>">
<? }?>
<? if($paginationcount>1 && $section[1]!=''){ ?>
<link rel="next" href="/dog-names/<? if($section[1]!=''){ ?><?=$section['1'];?>/<? echo ($titlepage+1)?>/<? }?>">
<? }?>
<link rel="canonical" href="https://www.dogspot.in/dog-names/<? if($section[1]!=''){ ?><?=$section[1];?>/<? }?>" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/dog-names/<? if($section[1]!=''){ ?><?=$section[1];?>/<? }?>" />
<?php require_once($DOCUMENT_ROOT .'/common/script.php');?>
<?php require_once($DOCUMENT_ROOT .'/common/header.php'); ?>

</head>
<body>
<div class="breadcrumb">
<div itemscope itemtype="http://schema.org/Breadcrumb"> 
<div itemscope itemtype="http://schema.org/BreadcrumbList">
<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<a href="/" itemprop="item"  data-ajax="false" ><span itemprop="name">Home</span></a>
<meta itemprop="position" content="1" /> </span>
<span> / </span>  

 <? $wag_dog_brrrd = query_execute_row("SELECT breed_engine FROM dog_breeds WHERE nicename='$firstBreadcum' ");
 if($wag_dog_brrrd['breed_engine'] == '1'){?>
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<a href="/<?=$firstBreadcum?>/" itemprop="item"  data-ajax="false" ><span itemprop="name"><?=$firstBread?></span></a>
  <meta itemprop="position" content="2" /> 
</span>
<?  }else{?>
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <a href="/dog-names/" itemprop="item"  data-ajax="false" > <span itemprop="name">Dog Names</span> </a>
     <meta itemprop="position" content="2" /> 
     </span>
     <? }?>
    <? if($section[0]!=''){?>  <span> / </span>
    <span itemprop="itemListElement">
      <span class="brd_font_bold" itemprop="name"><?=ucwords($bread_crum);?> Names</span>
       <meta itemprop="position" content="3" /> </span><? }?>          
             
</div>
</div>
</div>
  <div class="dog_name_wrap">

<div class="header-banner" >
<?php require_once($DOCUMENT_ROOT .'/header-banner.php'); ?> 
   
</div>
  <?php /*?><div class="widget-pup-new Label banner-card">
      <? $getItems=query_execute("SELECT si.name,si.nice_name,si.item_id,si.price,si.selling_price,si.item_parent_id FROM shop_items as si WHERE banner_type='yes' ORDER By rand() LIMIT 3 ");
				 while($fetchaffiliate=mysql_fetch_array($getItems)){
			 $name=$fetchaffiliate['name'];
			 $item_id=$fetchaffiliate['item_id'];
			  $item_parent_id=$fetchaffiliate['item_parent_id'];
			   $nice_name=$fetchaffiliate['nice_name'];
			    $price=$fetchaffiliate['price'];
			   $mrp_price=$fetchaffiliate['selling_price'];
		 $rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
		 if(!$rowdatM['media_file'])
		 {
		$rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
	     }
		 ?>
           <div class="banner-wrap"> 
        	<a data-ajax="false" href="/<?=$nice_name?>/?UTM=bannerrepublic">
                <div class="banner-img"> <img src="https://ik.imagekit.io/2345/tr:h-197,w-197/shop/item-images/orignal/<?=$rowdatM["media_file"]?>" width="100" height="93" style="margin-top:0px;" alt="<?=$name?>" title="<?=$name?>"> </div>
                <div class="banner-text">
                    <div class="gurgaon_offers_pn"><?=$name?></div>
                                        <div class="gurgaon_offers_pr">
                                        <? if($mrp_price>$price){?>
                                        <span>Rs. <del><?=number_format($mrp_price,0)?></del> 
                                        </span> &nbsp; &nbsp; <? }?>
                                        <span style="color: #f00;">
                                        Rs. <?=number_format($price,0)?></span>
                                        </div>
                                    </div>
          	</a> 
		</div>
 <? }?>
                 </div><?php */?>
        
      <div class="dogname_content">
		
         <div class="dognameBlock">
        
        <? if($section[0]==''){?><h1><? }?> <div class="dog-nameText">Dog Names</div><? if($section[0]==''){?></h1><? }?>
          <div style="text-align:justify; padding-left:10px; padding-right:10px;">
Have you met new parents or parents - to - be? One of the most interesting discussions that often arise in the company of new parents is "what would be the new baby called?", followed by a myriad of suggestions by family and friends. Just like human babies, new pet parents also face a similar predicament "what to name their new furry baby". Your search for the perfect name ends right here. We bring to you a list of names that you can select and choose from! Happy searching!! 
Here is the popular dog names <? if($firstBreadcum){?> for
       <a style="color:#6c9d06; text-decoration:underline;" href="https://www.dogspot.in/<?=$firstBreadcum?>/" data-ajax="false"  ><?=$breed;?></a><? }?>.
				<div>	<div class="col2"  style="margin-top:10px;">
                    
                    
                             <select name="breed" id="breed" onchange="get_sex_data()">
                             <option value="">Select Breed</option>
                             <? while($get_breed=mysql_fetch_array($select_dog_data)){
				             $dg_breed=$get_breed['breed_nicename'];?>
                             <option value="<?=$get_breed['breed_nicename'];?>" <? if($dg_breed==$option_breed && $option_breed !=''){echo'selected="selected"';}?>><?=$get_breed['dog_breed'];?></option><? }?>
                             </select>
                   
                   <div>
                   
                   </div>
                             
                            </div>
                    <div class="col2" style="margin-top: 10px;" >
                            <div id="att_2">
                            <select name="sex" id="sex">
                            <option value="">Select Gender</option>
                            <? while($get_sex_options=mysql_fetch_array($select_sex_options)){
				           $sex_data_opt=$get_sex_options['dog_sex'];
				           if($sex_data_opt=='M'){
					$v_data_opt='male';
					$dis_data_opt='Male';
					}
					if($sex_data_opt=='F'){
					$v_data_opt='female';
					$dis_data_opt='Female';
					}
				?>
            <option value="<?=$v_data_opt?>" <? if($option_sex==$sex_data_opt){echo'selected="selected"';}?> id="<?=$sex_data_opt; ?>"><?=$dis_data_opt;?></option>
            <? }?>
                            </select>
                            </div>
                            </div></div>
                       </div>
                        <button data-role="none" name="go" class="dogname-btn" id="redirecttopage41" onclick="callbreedurl()">Go</button>
                    </div>
                    <ul class="filter-a-z">
          <li><strong>Names A-Z</strong></li>
          <li><a href="/dog-names/a/" <? if($section[1]=='a' || $section[1]==''){?>class="active"<? }?> data-ajax="false">A</a></li>
          <li><a href="/dog-names/b/" <? if($section[1]=='b'){?>class="active"<? }?> data-ajax="false">B</a></li>
          <li><a href="/dog-names/c/" <? if($section[1]=='c'){?>class="active"<? }?> data-ajax="false">C</a></li>
          <li><a href="/dog-names/d/" <? if($section[1]=='d'){?>class="active"<? }?> data-ajax="false">D</a></li>
          <li><a href="/dog-names/e/" <? if($section[1]=='e'){?>class="active"<? }?> data-ajax="false">E</a></li>
          <li><a href="/dog-names/f/" <? if($section[1]=='f'){?>class="active"<? }?> data-ajax="false">F</a></li>
          <li><a href="/dog-names/g/" <? if($section[1]=='g'){?>class="active"<? }?> data-ajax="false">G</a></li>
          <li><a href="/dog-names/h/" <? if($section[1]=='h'){?>class="active"<? }?> data-ajax="false">H</a></li>
          <li><a href="/dog-names/i/" <? if($section[1]=='i'){?>class="active"<? }?> data-ajax="false">I</a></li>
          <li><a href="/dog-names/j/" <? if($section[1]=='j'){?>class="active"<? }?> data-ajax="false">J</a></li>
          <li><a href="/dog-names/k/" <? if($section[1]=='k'){?>class="active"<? }?> data-ajax="false">K</a></li>
          <li><a href="/dog-names/l/" <? if($section[1]=='l'){?>class="active"<? }?> data-ajax="false">L</a></li>
          <li><a href="/dog-names/m/" <? if($section[1]=='m'){?>class="active"<? }?> data-ajax="false">M</a></li>
          <li><a href="/dog-names/n/" <? if($section[1]=='n'){?>class="active"<? }?> data-ajax="false">N</a></li>
          <li><a href="/dog-names/o/" <? if($section[1]=='o'){?>class="active"<? }?> data-ajax="false">O</a></li>
          <li><a href="/dog-names/p/" <? if($section[1]=='p'){?>class="active"<? }?> data-ajax="false">P</a></li>
          <li><a href="/dog-names/q/" <? if($section[1]=='q'){?>class="active"<? }?> data-ajax="false">Q</a></li>
          <li><a href="/dog-names/r/" <? if($section[1]=='r'){?>class="active"<? }?> data-ajax="false">R</a></li>
          <li><a href="/dog-names/s/" <? if($section[1]=='s'){?>class="active"<? }?> data-ajax="false">S</a></li>
          <li><a href="/dog-names/t/" <? if($section[1]=='t'){?>class="active"<? }?> data-ajax="false">T</a></li>
          <li><a href="/dog-names/u/" <? if($section[1]=='u'){?>class="active"<? }?> data-ajax="false">U</a></li>
          <li><a href="/dog-names/v/" <? if($section[1]=='v'){?>class="active"<? }?> data-ajax="false">V</a></li>
          <li><a href="/dog-names/w/" <? if($section[1]=='w'){?>class="active"<? }?> data-ajax="false">W</a></li>
          <li><a href="/dog-names/x/" <? if($section[1]=='x'){?>class="active"<? }?> data-ajax="false">X</a></li>
          <li><a href="/dog-names/y/" <? if($section[1]=='y'){?>class="active"<? }?> data-ajax="false">Y</a></li>
          <li><a href="/dog-names/z/" <? if($section[1]=='z'){?>class="active"<? }?> data-ajax="false">Z</a></li>
        </ul>
        
        <div style="float:left; width:100%;">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive-Head -->
<ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-3238649592700932"
    data-ad-slot="6045502591"
    data-ad-format="auto"></ins>
<script>
$(document).ready(function(){(adsbygoogle = window.adsbygoogle || []).push({})})
</script>

        </div>
          <div class="dog_name_filter"><? if($section[0]==''){?><h2 style="margin-left:-10px;">Dog Names A to Z List</h2><? }else{?><h1><?=$h1_data;?></h1><? }?></div>
                    <table class="dog_name_table dog-name-tab">
                    <thead>
                    <th>Dog Name</th>
                    <th>Breed</th>
                     <th>Gender</th>
                    
                    </thead>
                    <? while($get_search_data=mysql_fetch_array($seach_dog_filter)){
			  $dg_name=$get_search_data['dog_name'];
			  $string=preg_replace('/[^A-Za-z0-9\-]/', ' ', ucwords(strtolower($get_search_data['dog_name'])));
			  $wordlist = array("ch", "biss", "Ind Ch", "Bis", "Ch", "Grand Ind", "sale", "month", "imp", "Sale", "Month", "Imp", "Import", "Dogs", "-");
              foreach ($wordlist as &$word) {
              $word = '/\b' . preg_quote($word, '/') . '\b/';
              }
			  $string = preg_replace('/[0-9]+/', '', $string);
              $string = preg_replace($wordlist, '', $string);
			  $breed_engine_data=query_execute_row("SELECT nicename FROM dog_breeds WHERE breed_name='".$get_search_data['dog_breed']."' AND breed_engine='1'");
			  $breed_engine_nicename=$breed_engine_data['nicename'];?>
                   <tr>
            <td class="td-highlight"><?=$string;?></td>
            <td><? $breed_engine_nicename?> <?=$get_search_data['dog_breed'];?></td>
            <td><? if($get_search_data['dog_sex']=="M"){echo "Male";}else{echo "Female";}?></td>
           
          </tr>
          
          <? }?>
      
                    </table>
              <div style="width: 100%; float: left; text-align: center; margin: 10px 0px 40px;">   <button id="next" style="padding: 7px 13px; font-size: 14px;">Show More</button></div>
        <div class="relted-dog-name">
<h2>Most Searched Dog Names By Breed</h2>
<ul>
<li class="rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/golden-retriever/">Dog Names for Golden Retriever <span class="rel-right">&gt;&gt;</span></a> </li>
<li class="rel-ml rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/german-shepherd-dog-alsatian/">Dog Names for German Shepherd<span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/tibetan-mastiff/">Dog Names for Tibetan Mastiff<span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/rottweiler/">Dog Names for Rottweiler <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/great-dane/">Dog Names for Great Dane <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/labrador-retriever/">Dog Names for Labrador Retriever <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/pug/">Dog Names for Pug <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/siberian-husky/">Dog Names for Siberian Husky <span class="rel-right">&gt;&gt;</span></a></li>
<li class=" rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/beagle/">Dog Names for Beagle <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/pomeranian/">Dog Names for Pomeranian <span class="rel-right">&gt;&gt;</span></a></li>
<li class=""><a data-ajax="false" href="https://m.dogspot.in/dog-names/stbernard/">Dog Names for St.Bernard <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml "><a  data-ajax="false" href="https://m.dogspot.in/dog-names/bull-dog/">Dog Names for Bull Dog <span class="rel-right">&gt;&gt;</span></a></li>
</ul>
</div>
<div class="relted-dog-name">
<h2>Dog Names by Gender</h2>
<ul>
<li class="rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/male/">
Male Dog Names <span class="rel-right">>></span></a> </li>
<li class="rel-ml rel-mb"><a data-ajax="false" href="https://m.dogspot.in/dog-names/female/">
Female Dog Names<span class="rel-right">>></span></a></li>
</ul>
</div>
<?php /*?>      	<div style="float:left; width:100%; text-align:center; margin-bottom:10px;">
  <script type="text/javascript" language="javascript">
     var aax_size='300x250';
     var aax_pubname = 'dog0bd-21';
     var aax_src='302';
   </script>
  <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>
</di<?php */?>
                  
                 
                    
                    </div>
                         <div style="float:left; width:100%;">
 <!-- Responsive-Head -->
<ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-3238649592700932"
    data-ad-slot="6045502591"
    data-ad-format="auto"></ins>
<script>
$(document).ready(function(){(adsbygoogle = window.adsbygoogle || []).push({})})
</script>

        </div>
<script >
function callbreedurl(){
	var brd=document.getElementById("breed").value;
	var sex=document.getElementById("sex").value;
	var sexvalue=document.getElementById("sex").value;
	var sexvalue=sexvalue+'-';
	if(brd!='' && sex==''){
	window.location.href = '/dog-names/'+brd;
	
}
	if(brd=='' && sex!=''){
	window.location.href = '/dog-names/'+sex;
}
	if(brd!='' && sex!=''){
	var breedImplode=sexvalue.concat(brd);
	window.location.href = '/dog-names/'+breedImplode;
}
}
</script>
<script >
function get_sex_data(){
var sex_data=$('#breed').val();
ShaAjaxJquary('/dog-names/ajax-data.php?sex_data='+sex_data+'', '#att_2', '', '', 'GET', '#att_2', 'Loding...', 'REP');
}
</script>
<script>
$(document).ready(function(){

      var list = $(".dog-name-tab tr");
      var numToShow = 10;
      var button = $("#next");
      var numInList = list.length;
      list.hide();
      if (numInList > numToShow) {
        button.show();
      }
      list.slice(0, numToShow).show();

      button.click(function(){
          var showing = list.filter(':visible').length;
          list.slice(showing - 1, showing + numToShow).fadeIn();
          var nowShowing = list.filter(':visible').length;
          if (nowShowing >= numInList) {
            button.hide();
          }
      });

});
</script>
<style >#google_pedestal_container{display:none}.ui-select .ui-btn select,button.ui-btn{-webkit-appearance:none;-moz-appearance:none}.sticky-bar,.sticky-height{height:30px}.sale-text,.start-countdown{display:inline-block}.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333}#header,.ui-btn,td,th{text-align:left}.sticky-bar a,.sticky-bar a:hover{text-decoration:underline}.sticky-left{width:96%;float:left}.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px}.sale-text{font-size:12px}.sale-text a{color:#fc3;text-decoration:none}.ds-home-link{padding-top:30px!important}.ui-btn{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;border:1px solid #ccc;padding-top:5px;padding-bottom:5px;border-radius:0!important}.ui-icon-carat-d:after{background-image:url(data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20width%3D%2214px%22%20height%3D%2214px%22%20viewBox%3D%220%200%2014%2014%22%20style%3D%22enable-background%3Anew%200%200%2014%2014%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cpolygon%20style%3D%22fill%3A%23FFFFFF%3B%22%20points%3D%2211.949%2C3.404%207%2C8.354%202.05%2C3.404%20-0.071%2C5.525%207%2C12.596%2014.07%2C5.525%20%22%2F%3E%3C%2Fsvg%3E)}.ui-btn,body,button,input,select{font-size:1em;line-height:1.3;font-family:lato,sans-serif}.ui-btn.ui-corner-all,.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em}.ui-corner-all{-webkit-background-clip:padding;background-clip:padding-box}.ui-body-a{border-width:1px;border-style:solid;background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3}.ui-btn:focus{outline:0}.ui-mobile body{height:99.9%}.ui-page{padding:0;margin:0;outline:0}.ui-mobile a img{border-width:0}.ui-mobile-viewport{margin:0;overflow-x:visible;-webkit-text-size-adjust:100%;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent}body.ui-mobile-viewport{overflow-x:hidden}.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0}.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden}@media screen and (orientation:portrait){.ui-mobile .ui-page{min-height:420px}}@media screen and (orientation:landscape){.ui-mobile .ui-page{min-height:300px}}.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0}.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em}.ui-btn{text-decoration:none!important;font-size:13px;margin:.5em 0;position:relative;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;cursor:pointer;user-select:none;padding-left:5px}#header div,#morepopup li a,.ui-link,ins{text-decoration:none}.ui-btn-icon-right{padding-right:2.5em}.ui-btn-icon-right:after{content:"▼";position:absolute;display:block;color:#999;top:50%;margin-top:-8px;right:.5625em}button.ui-btn{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}button.ui-btn::-moz-focus-inner{border:0}.ui-screen-hidden{display:none!important}.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%}.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099}.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important}.ui-popup{position:relative;padding:10px 10px 0}.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff}.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden}.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px)}.ui-select{position:relative}.ui-select .ui-btn{margin:0;opacity:1;border-radius:5px!important}.ui-select .ui-btn select{position:absolute;top:0;left:0;width:100%;min-height:1.5em;min-height:100%;height:3em;max-height:100%;outline:0;-webkit-border-radius:inherit;border-radius:inherit;cursor:pointer;filter:Alpha(Opacity=0);opacity:0;z-index:2}.ui-select .ui-btn>span:not(.ui-li-count){display:block;text-overflow:ellipsis;overflow:hidden!important;white-space:nowrap}.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block}#header,#morepopup li:last-child{border-bottom:0}.ui-panel-page-container{overflow-x:visible}.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none}.panelicon,.purchase-product-cart{position:relative}.ui-panel-dismiss-open{display:block}.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease}.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0)}.ui-panel-position-left{left:-17em}.ui-panel-open.ui-panel-position-left{left:-1px}.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em}#header{height:50px;padding:12px;border-top:0;background:#6c9d06}#header .fa{font-size:18px;color:#fff}td,th{font-weight:400;padding:5px;border:1px solid #ccc;font-size:14px}table{width:100%;border-collapse:collapse;border-spacing:0}tr:nth-of-type(odd){background:#ddd}th{color:#fff}#header div,.panelicon{display:inline-block}#header div{color:#f7422d}.panelicon{background:url(https://m.dogspot.in/images/mobile-panel-icon-white.png);height:19px;left:0;margin-top:0!important;top:0;width:18px}.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px}.ui-link{color:#333}.green_clr{font-family:gobold;margin-left:8px;color:#fff!important;font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335}#morepopup li,#morepopup li a{color:#323232}#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px}#morepopup li img{margin-right:8px;vertical-align:top}#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px}#morepopup-screen{background:0 0}.content-search{padding-bottom:20px}.content-search form{margin:0 auto;position:relative;top:5px;width:95%}.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%}.col2{float:left;margin-right:12px;width:47.5%}.col2:last-child{margin-right:0}*{margin:0;padding:0;font-family:lato,sans-serif;box-sizing:border-box}.ui-select .ui-btn{background:#fff!important}.cartsearch_blk a img{width:18px;height:19px}.content-search{width:100%;float:left;margin-top:0!important;padding:5px 0;background:#6c9d06}.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0}#header img{vertical-align:middle}#defaultpanel4{z-index:99999}div.dsdropdown{position:relative}.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px}.m-sprite{background:url(https://m.dogspot.in/images/msprite.png) no-repeat;margin:auto}.m-user{float:left;background-position:-166px -40px}.m-orders{float:left;background-position:-33px -72px}.m-wishlist{float:left;background-position:-6px -72px}.m-logout{float:left;background-position:-78px -40px}.m-setting{float:left;background-position:-107px -40px}.m-contact-us{float:left;background-position:-139px -40px}ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc}ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc}ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%}.panel,ul.dsdropdown-menu{display:none}ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important}ul.dsdropdown-menu li:last-child{font-size:16px;border:0}ul.dsdropdown-menu li:hover{background:#f9f9f9}.nf-img{float:left;width:20%}.nf-text{float:left;width:80%}.nf-img img{width:50px!important;height:auto!important}.nf-text h4{font-size:14px;padding-bottom:1px;color:#333}.nf-text p{color:#888;font-size:13px}.nf-active-bg{background:#f2f2f2}a,body,div,em,form,h1,h2,h4,i,iframe,img,ins,li,p,span,strong,table,tbody,td,th,thead,tr,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline}#defaultpanel4,.whitewrapper{background-color:#fff}ul{list-style-type:none}:focus{outline:0}.cartsearch_blk a{margin-left:12px}.cartsearch_blk a:first-child{margin-left:0}#defaultpanel4{border-right:1px solid #ccc}#footer{font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:10px;text-align:center}.breadcrumb{overflow:hidden;padding:8px;margin-bottom:0; font-size:12px; color:#555;}.breadcrumb a{text-decoration:none}.ds-nav-click a,ul.filter-a-z li a{text-decoration:none}.ui-link img{vertical-align:middle}.dog_name_wrap{background:#fff;padding:10px;float:left;width:100%}.dogname_content{background:#fff;overflow:hidden;border:1px solid #d1d1d1}.dognameBlock{font-size:14px}.dogname-btn,table.dog_name_table,ul.filter-a-z{margin-top:10px;width:100%}.dogname-btn{height:35px;background:#d9d9d9!important;font-size:16px;font-weight:700;border-radius:5px;border:1px solid #d9d9d9;cursor:pointer}ul.filter-a-z{margin-bottom:10px;padding:0 0 0 5px;float:left}ul.filter-a-z li{float:left;padding-left:11px;padding-right:11px;font-size:15px;margin-bottom:5px}ul.filter-a-z li a{color:#333}ul.filter-a-z li .active{color:#5e83fb;font-weight:700}.ui-select span{font-size:14px;color:#777;padding-top:3px;padding-bottom:3px}.dog_name_filter{font-size:16px;padding-left:10px}.dog-nameText{font-size:18px;background:#ddd;padding:5px 10px;margin-bottom:10px;border-radius:3px 3px 0 0;border-bottom:0;border-bottom:1px solid #d1d1d1}.dog_name_table td,.dog_name_table th{padding:5px 5px 5px 15px;width:33.2%;color:#333}.dog_name_table th{font-size:14px}.dog_name_table td{background:#fff!important;border-bottom:1px solid #dedede;font-size:13px}.relted-dog-name{width:100%;float:left}.dog_name_filter h2,.relted-dog-name h2{font-size:18px;font-weight:500;color:#333;text-transform:none;width:100%;float:left;margin-bottom:15px;text-align:center;margin-top:5px}.relted-dog-name ul{list-style:none;width:100%;padding:0;margin:0}.relted-dog-name ul li{background:#f2f2f2;width:92%;float:left;padding:10px;border-radius:3px;margin:0 4% 14px}.rel-right{float:right}.rel-mb{margin-bottom:15px}.add-sans-sapce{float:left;width:100%;text-align:center;margin:auto;max-height:120px}.add-sans-sapce .add-banner{margin:auto}.panel div a{color:#333}.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none}.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4}.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4}.accordion:hover,.active{background-color:#f8f8f8}.panel{padding:0;background-color:#fff;overflow:hidden}.panel div{padding:10px 25px}.accordion.active:after{content:"\2212"}.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px}.ds-home-link{background:#6c9d06;padding:8px 10px}.ds-home-link a{color:#fff}.ds-nav-click{padding:12px}.ds-nav-click a{color:#333}.header-banner{width:96%;margin:0 2% 15px;float:left;text-align:center}.header-banner img{max-width:100%;height:auto}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-bars:before{content:"\f0c9"}.fa-bell-o:before{content:"\f0a2"}.fa-shopping-cart:before{content:"\f07a"}.fa-user-o:before{content:"\f2c0"}.fa-home:before{content:"\f015"}.fa-search:before{content:"\f002"}body{margin:0;font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height:1.5}@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot#iefix&v=4.7.0) format('embedded-opentype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal}.ac_results { padding: 2px; border: 1px solid #bfbfbf; background-color: #fff; overflow: hidden; z-index: 999; text-align: left; top: 205px; width: 374px!important; border-radius: 2px } .ac-results ul { width: 100%; padding: 0; margin: 0 0 5px; font-size: 16px } .highlight-suggestion { color: #333 } .hl-sugg-vtcl { font-weight: 700; color: #6C9D06 } .ac-results li { margin: 0; padding: 2px 5px 2px 10px; cursor: default; display: block; font-size: 13px; line-height: 18px; overflow: hidden } .ac_over { background-color: #f2f2f2; color: #333 } .ac-results .header { margin-top: 12px; overflow: visible; background: #6C9D06; margin-bottom: 5px } .ac-results .header .text { display: inline-block; position: relative; color: #fff; padding: 0 5px}</style>
 <?php require_once($DOCUMENT_ROOT .'/common/bottom-final.php'); ?>

  