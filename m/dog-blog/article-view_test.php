<?php
if($section[0]==''){
	require_once("../constants.php");
}
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/functions2.php');
require_once($DOCUMENT_ROOT . '/facebookconnect/facebookbutton.php');
$ecomm_pagetype='articles'; 
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once($DOCUMENT_ROOT . '/common/script.php'); ?>
<?php 
	
	/*	ID 	post_author 	post_date 	post_content 	post_title 	post_status 	post_name 	views 	domain_id 	art_tag 	title_tag 	term_taxonomy_id 	term_id 	display_name 	article_user*/
		$sqlall="SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wpp.post_author=wu.ID AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.ID='$article_id'";
		$post_detail=mysql_query($sqlall);
		$rowpost_detail=mysql_fetch_array($post_detail);
		$article_id=$rowpost_detail['ID'];
		$artuser=$rowpost_detail['article_user'];
		$date=$rowpost_detail['post_date'];
		$art_name=$rowpost_detail['post_name'];
		$reformatted_date = showdate($date, "d M o");
		$post_title=$rowpost_detail['post_title'];
		$author=$rowpost_detail['display_name'];
		$post_content=stripcslashes($rowpost_detail['post_content']);
		$articlecat_id  = $rowpost_detail["term_id"];
		$sel_cat=query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
		$cat_name=$sel_cat['name'];
		$cat_nice=$sel_cat['slug'];
				
		$post_title = stripslashes($post_title);
		$post_title = breakLongWords($post_title, 30, " ");
		$whatapp=$post_title;
		$post_content = stripslashes($post_content);
		$post_content = breakLongWords($post_content, 200, " ");

		if ($title_tag) {
			$title = $title_tag . ' | ' . $cat_name . ' | DogSpot.in';
		} else {
			$title = $post_title;
		}
		if ($art_tag) {
			$keyword = $art_tag;
		} else {
			$keyword = $post_title . ' , ' . $cat_name;
		}

		if ($post_content) {
			$pageDesc = $post_content;
			$pageDesc = strip_tags($pageDesc);
			$pageDesc = myTruncate($pageDesc, 200); // funtion to split 200 words
			
		}				
				
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$pattern = 'm.dogspot.in';
		$abc="www.dogspot.in";
		$surl= str_replace($pattern,$abc,$url);
		//echo $a;
		$shared_query = query_execute("SELECT SUM(share_value) as val FROM share_count WHERE share_url = '$surl'");  
		while ($row_shared = mysql_fetch_array($shared_query)) {
			$sumT=$row_shared['val'];		
		}
		if($sumT==''){
        $sumT=0;	
         }
        $sum=format_num($sumT,1);
		//var_dump($shareData); 
		$feat_imgURL    = get_first_image($post_content, '/home/dogspot/domains/m.dogspot.in/public_html');
		$imgURLAbs = make_absolute($feat_imgURL, 'https://m.dogspot.in/');
		?>
<title>
<?=$title ?>
</title>
<meta name="keywords" content="<?= $keyword ?>" />
<meta name="description" itemprop="description" content="<?
$pageDesc = str_replace('"', '', $pageDesc);
$stext=urlencode(trim($title));
echo "$pageDesc";
?>" />
<link href="https://www.dogspot.in/<?=$section[0];?>/" rel="canonical">
<style>.fa{font-size: 16px !important;}.art_view_blk{background:#fff;}.art_view_blk table{border: 1px solid #ddd; margin:10px 0px;}.art-content{background: #f2f2f2;}.art_view_blk tr:nth-of-type(odd){ background:#fff !important;}.art_view_blk td{padding: 10px !important; border: 1px solid #ddd !important;}
.next_article_sce{
width: 100%;float: left;background: #fFf;border-top: 1px solid #888;border-bottom: 1px solid #888;padding: 10px;font-size: 14px;color: #555;
}
.next_article_img{ float:left; width:25%; margin-right: 10px}
.next_article_head{ float:left; width:69%;}
.article_divider {border-top: 5px solid #ccc;font-size: 15px;color: #555; font-family: sans-serif;text-align: center; background:#fff; width: 100%; float:left; margin-top:-19px; font-weight: bold;}
.article_divider:before {content: "";display: block;margin-bottom: 10px;width: 0;height: 0;
border-left: 10px solid transparent;border-right: 10px solid transparent;border-top: 10px solid #ccc;margin-left: 8px;
}
.sticky{ background:#f00;}
.fixed {position: fixed; top:0; left:0;width: 100%; z-index:99999;}

</style>

<!------Twitter--------->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@indogspot">
<meta name="twitter:title" content="<?= $title ?>">
<meta name="twitter:description" content="<?= trim($pageDesc) ?>">
<meta name="twitter:url" content="<?=$url ?>">
<meta name="twitter:image" content="<?= $imgURLAbs ?>" />
<!----------->

<!--------Facebook----->
<meta property="fb:app_id" content="119973928016834" />
<meta property="og:site_name" content="DogSpot" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?=$url ?>" />
<meta property="og:title" content="<?= $title ?>" />
<meta property="og:description" content="<?= trim($pageDesc) ?>" />
<meta property="og:image" content="<?= $imgURLAbs ?>" />
<? //if($art_name=='amen-sending-these-angels'){
//http://www.youtube.com/embed/WV2nwTAU2do
?>
<meta property="og:video" content="http://youtube.googleapis.com/v/WV2nwTAU2do"/>
<meta property="og:video:type" content="application/x-shockwave-flash"/>
<script type="text/javascript">
  var urlload='/dog-blog/loadmore.php';
  var track_load=1;
  var loading  = false;
	$(document).ready(function(){
	 $(window).scroll(function() {
			var height = $(window).height();
			var pageheight = $(document).height();
			var articlecat_id = $('#articlecat_id').val();
			var article_id = $('#article_id').val();
			var top = $(window).scrollTop() + 5000;
			if(top + height >= pageheight){
				  if(!loading){ 
						 loading = true;			
						 $('#wait_section').show();
						 $.post(urlload,{'articlecat_id': articlecat_id,'article_id': article_id,'lastComment':$(".imgtxtcontwag:last").attr('id')}, function(data){									
							 if($.trim(data)!= ''){	
								$("#rytPost_list").append(data);
								track_load++; 
								loading = false; 	
							 }else{					 	
								$('#wait_section').hide();
							 }						 
						 }).fail(function(xhr, ajaxOptions, thrownError) {
							  $('#wait_section').hide();
							  loading = false;				
						 });				
				 }
			 } 
	  });
	});

</script>
<script>jQuery(document).ready(function ($) {

    setInterval(function (){
        moveRight();
    }, 7000);

  
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider').css({ width: slideWidth, height: slideHeight });
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
		$('#slider').css('display','block');
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

});    
</script>
 
<!--------------End-------->
<?php require_once($DOCUMENT_ROOT . '/common/top.php');
$iid=1; ?>
<script type='text/javascript' src='https://www.dogspot.in/js/shaajax2.1.js'></script>
<div data-role="content" class="art-content">
  <div class="art_blk rytPost_list" id="rytPost_list">
    <div class="art_view_blk imgtxtcontwag" id='<?=$iid?>'>
      <h1><?=$post_title?></h1>
      <div class="art_post_detail"><a href="/dog-blog/<?=$cat_nice?>/" class="art_cat_name" data-ajax="false"><?=$cat_name?></a>,
        <?=$reformatted_date?>, By <?=$author?></div>
       <input type="hidden" name="txt1" id="txt1" value="article">
        <input type="hidden" name="txt2" id="txt2" value="5">
         <input type="hidden" name="urlpara<?=$iid?>" id="urlpara<?=$iid?>" value="<?=$art_name?>" />
             <input type="hidden" name="urltitle<?=$iid?>" id="urltitle<?=$iid?>" value="<?=$post_title;?> | <?=$cat_name?> | DogSpot.in" />
              <input type="hidden" name="title<?=$iid?>" id="title<?=$iid?>" value="<?=$post_title;?>" />
              <input type="hidden" name="htmlHidden<?=$iid?>" id="htmlHidden<?=$iid?>" value="<?=$sum?>"/>
      	<?php
		$check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id' AND post_type='attachment' AND post_mime_type like '%image%' ORDER BY ID DESC");
		if($check_img['guid'] !=''){
			$imgURLAbs=$check_img['guid'];	
			$exp=explode("https://www.dogspot.in",$imgURLAbs);
			$src = $DOCUMENT_ROOT.$exp[1];
			$pos = strrpos($imgURLAbs,'/');
			$image_name = substr($imgURLAbs,$pos+1);
			$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
		}elseif (strpos($imgURLAbs,'wordpress') !== false) {
			$exp=explode("https://www.dogspot.in",$imgURLAbs);
			$src = $DOCUMENT_ROOT.$exp[1];
			$pos = strrpos($imgURLAbs,'/');
			$image_name = substr($imgURLAbs,$pos+1);
			$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
		}else if ($imgURLAbs!='https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
			$URL=$imgURLAbs;
			$image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
			$pos = strrpos($image_name,'/');
			$image_name = substr($image_name,$pos+1);
			$extension = stristr($image_name,'.');
			if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
				$image_name;
			}
			$src = $DOCUMENT_ROOT.'/userfiles/images/'.$image_name;
			//$src = $imgURL;
			$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
		}else if($rowUser1["image"]) {
			$src = $DOCUMENT_ROOT.'/profile/images/'.$rowUser1["image"];
			$imageURL = 'https://www.dogspot.in/imgthumb/190x127-'.$rowUser1["image"];
		}else{
	        $src = $DOCUMENT_ROOT.'/images/noimg.gif';
			$imageURL = 'https://www.dogspot.in/imgthumb/190x127-noimg.gif';
		}
		$dest = $DOCUMENT_ROOT.$imageURL;
		createImgThumbIfnot($src,$dest,'190','127','ratiowh');
	?>
    <p><?=$post_content;?></p>
      <input type="hidden" name="articlecat_id" id="articlecat_id" value="<?=$articlecat_id?>"/>
    <input type="hidden" name="article_id" id="article_id" value="<?=$article_id?>"/>
    </div>
    <? 
		$sql_prev = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MAX(ID) FROM wp_posts WHERE ID < '$article_id' AND post_status='publish')");
		$sql_next = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MIN(ID) FROM wp_posts WHERE ID > '$article_id' AND post_status='publish')");
	if($sql_next['ID']=='')
	{
	$show=$current_art_id-100;	
	$sql_next = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MIN(ID) FROM wp_posts WHERE ID > '$show' AND post_status='publish')");	
	}
	if($sql_prev['ID']=='')
	{
	$show=$current_art_id+100;	
	$sql_prev = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MIN(ID) FROM wp_posts WHERE ID > '$show' AND post_status='publish')");	
	}

    ?>
    
 <?php /*?>   <div class="w-article-news-btns">
    	<? if($sql_prev['ID']!=""){ ?>
			<a href="/<?=$sql_prev['post_name'];?>" class="w-article-news-btn w-article-news-left" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: -5px;"></i><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Prev</a>
        <? }else{?>
        	<a href="#" class="w-article-news-btn w-article-news-left" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: -5px;"></i><i class="fa fa-angle-left" style="margin-right: 5px;"></i>No More</a>
        <? }?>
        
        <? if($sql_next['ID']!=''){?>
			<a href="/<?=$sql_next['post_name'];?>" class="w-article-news-btn w-article-news-right" data-ajax="false">Next <i class="fa fa-angle-right" style="margin-left: 5px;"></i><i class="fa fa-angle-right" style="margin-left: -5px;"></i></a>
        <? }else{?>
	        <a href="#" class="w-article-news-btn w-article-news-right" data-ajax="false">No More <i class="fa fa-angle-right" style="margin-left: 5px;"></i><i class="fa fa-angle-right" style="margin-left: -5px;"></i></a>
        <? }?>
    </div>

<!-- next pervios circle btn-->
	<? if($sql_prev['ID']!=""){ ?>
	<a href="/<?=$sql_prev['post_name'];?>" class="w-article-btn-next" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px; font-size: 35px !important; margin-left: 20px; margin-top: 3px; color: #fff;"></i></a>
    <? }?>
    
    <? if($sql_next['ID']!=''){?>
	<a href="/<?=$sql_next['post_name'];?>" class="w-article-btn-perv" data-ajax="false"><i class="fa fa-angle-right" style="font-size: 35px !important;margin-left: 17px;margin-top: 3px;color: #fff;"></i></a>
    <?php } ?>
<?php */?>
<? $riO=$article_id+1;
$selectArt = mysql_query("SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id , wu.display_name,wpp.first_image_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wptr.term_taxonomy_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post'  AND wpp.post_author=wu.ID AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.ID!='$article_id' AND wpp.ID!='112'  AND wpp.ID!='95'  AND wpp.ID!='243'  AND wpp.ID!='74'  AND wpp.ID!='231' AND wpp.ID !='426' AND wpp.post_status='publish'  ORDER BY wpp.ID ASC LIMIT $riO,1");

  $getName=mysql_fetch_array($selectArt);?>
    <div data-articlepage-start=""><div class="top_banner"><div><div id="" style="border: 0pt none;">

</div></div></div><div class="next_article_sce">
<div class="next_article_img"><img src="https://m.dogspot.in/dog-blog/images/next-story-icon2.jpg" alt="next story" title="next story"></div>
<div class="next_article_head"><?=$getName['post_title']?></div>
       	   
       	</div></div>
      
      	<div class="w-article-cat slide-banner"> 
        <div class="slide-banner-block" >
         <a href="/flea-ticks/" data-ajax="false"><img src="https://m.dogspot.in/dog-blog/images/flean-ticks-banner.jpg" alt="Flea n Ticks"  title="Flea n Ticks"  class="slide-banner-one" ></a>
        </div>
        <div class="slide-banner-block" >
        <a href="/cat-litter-boxes/" data-ajax="false"><img src="https://m.dogspot.in/dog-blog/images/cat-litter-tray1.jpg" alt="Cat Litter" title="Cat Litter" class="slide-banner-two"></a>
        </div>
        
        </div>
        
   	<div class="article_divider">
       	   &nbsp;
       	</div>
		
  </div>
  <div class="w-article-cat" style="margin-bottom:0px;">
        	<div class="sitemap-cat">
            	<div class="widget-title"><label>Categories</label></div>
                <div class="sitemap" data-region="sitemap">
                	<table>
                		<tr>
							<td class="sitemap-item">
								<a href="/dog-blog/wag-news/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Wag News </a>
			   				</td>
							<td class="sitemap-item">
								<a href="/dog-blog/wellness/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Wellness</a>
			   				</td>
						</tr>
						<tr>
							<td class="sitemap-item">
								<a href="/dog-blog/wag-brag/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Wag Brag</a>
			   				</td>
							<td class="sitemap-item">
								<a href="/dog-blog/wag-wiki/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Wag wiki </a>
			   				</td>
						</tr>
						<tr>
							<td class="sitemap-item">
								<a href="/dog-blog/grooming/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Grooming</a>
			   				</td>
							<td class="sitemap-item">
			 					<a href="/dog-blog/training/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Training</a>
			   				</td>
						</tr>
						<tr>
							<td class="sitemap-item">
								<a href="/dog-blog/travel-with-your-dog/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Travel with your dog</a>
			   				</td>
							<td class="sitemap-item">
			 					<a href="/dog-blog/adoption/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Adoption</a>
			   				</td>
						</tr>
						<tr>
							<td class="sitemap-item">
								<a href="/dog-blog/story-from-pet-lover/" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Story from a pet lover</a>
			   				</td>
							<td class="sitemap-item">
			 					<a href="/dog-blog/events" data-ajax="false"><i class="fa fa-angle-left" style="margin-right: 5px;"></i>Events</a>
			   				</td>
						</tr>
			        </table>
				</div>
			</div>
		</div>
</div>
<?php /*?><script type="text/javascript">  
	var absPath='<?php echo $absPath; ?>';
	$.ajax({
		type: "GET",
		url: absPath+"/social-share/social1.php?stext=<?=urlencode($stext)?>&surl=<?=$surl?>&ssurl=<?=$surl?>",
		success: function(datas){
		}
   });
</script> <?php */?>

<form id="formsub" name="formsub">
  	<input type="hidden" name="urlShare" id="urlShare" value="<?=$surl?>">
  	<div  data-role="footer" data-position="fixed" class="dog_article_sharefooter">
    	<div class="ds_social_cont" style="margin-bottom:0px;">
      		<ul class="ds_social_buttons">
        		<li class="ds_social_list" style="text-align:center;">
          			<div class="ds_social_number">
            			<div class="ds_social_number_cont_num"><?=$sum;?></div>
            			<div>Shares</div>
         			</div>
        		</li>
        		<li class="ds_social_list ds_facebook_share social"> <a rel="nofollow" href="http://www.facebook.com/sharer.php?m2w&u=<?=$surl ?>"  target="_blank" title="Share This on Facebook"><i class="fa fa-facebook"> </i></a></li>
                <li class="ds_social_list ds_twitter_share"> <a href="https://twitter.com/intent/tweet?url=https://www.dogspot.in/<?=$section[0]; ?>/&original_referer=https://www.dogspot.in/<?=$section[0]; ?>/ ?>/&source=dogspot&text=<?=$post_title ?>" target="_blank" onclick="return windowpop(this.href, 545, 433)" rel="nofollow" title=""><i class="fa fa-twitter"> </i> </a> </li>
                <li class="ds_social_list ds_whatsapp_share"> <a class="whatsapp" data-text="<?=$whatapp?>" data-link="https://www.dogspot.in/<?=$section[0]; ?>/" onclick="" href="" rel="nofollow" title=""><i class="fa fa-whatsapp"></i></a> </li>
                <li class="ds_social_list ds_google_share social"> <a rel="nofollow" href="https://plus.google.com/share?url=<?=$surl ?>" onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"> </i></a></li>
      		</ul>
		</div>
  	</div>
</form>
<script type="text/javascript">
	$(document).ready(function() {
		$('.whatsapp').on("click", function(e) {
			if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
				var article = $(this).attr("data-text");
				var weburl = $(this).attr("data-link");
				//alert(article);
				var whats_app_message = encodeURIComponent(article)+" - "+encodeURIComponent(weburl);
				var whatsapp_url = "whatsapp://send?text="+whats_app_message;
				window.location.href= whatsapp_url;
			}else{
				alert('you are not using mobile device.');
			}
		});
		var valueurl=$('#urlShare').val();
		ShaAjaxJquary("/share-update.php?surl="+valueurl+"", "#mis", '', 'formsub', 'GET', '#loading', '','REP');
	});
</script>
<script type="text/javascript" language="javascript">
         $(function () {
             var $win = $(window);

             $win.scroll(function () {
                 var i=0;
				     var sticky = $('.sticky'),
  					 scroll = $(window).scrollTop();
                     if (scroll >= 100){
				     sticky.addClass('fixed');
                     }
                     else { sticky.removeClass('fixed')};
                    $('#rytPost_list .imgtxtcontwag').each(function(){
						
						// Is this element visible onscreen?
						i++;
						//var decet='partial';
						var visible = isScrolledIntoView(this);
						
						if(visible==true){
						var id=this.id;
						var urlP="/"+$('#urlpara'+id).val()+"/";
						var urlT=$('#urltitle'+id).val();
						var Title=urlT.split('|');
						var pathname = window.location.pathname;
						var pathtitle = $(document).find("title").text().split('|');
						var curT=pathtitle[0];
						if(urlP != "/undefined/"){
						if(pathname != urlP){
						var url="http://m.dogspot.in"+urlP+"";
					    var data1=$('#htmlHidden'+id).val();
						$(".ds_social_number_cont_num").text(data1);
						$('.social').find("a").each(function(){
						var href=$(this).attr('href');
						url2 = href.split('www.dogspot.in/');
						url2[1]=urlP;
						var arra=[url2[0],'www.dogspot.in',url2[1]];
						var val = arra.join('');
						$(this).attr('href',val);
						});
						var tesxt=$('.ds_twitter_share').children('a').attr('href');
						var repltext=tesxt.split(pathname).join(urlP);
						var repltextt=repltext.split(curT).join(Title[0]);
						 $('.ds_twitter_share').children('a').attr('href',repltextt);
						var whata=$('.ds_whatsapp_share').children('a').attr('data-text');
						var whattext=whata.split(curT).join(Title[0]);
						$('.ds_whatsapp_share').children('a').attr('data-text',whattext);
						var whataL=$('.ds_whatsapp_share').children('a').attr('data-link');
						var whattlink=whataL.split(pathname).join(urlP);
						$('.ds_whatsapp_share').children('a').attr('data-link',whattlink);
						(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
                         Date();a=s.createElement(o),
                         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	                     ga('send', 'event', 'load more article', 'mobile', urlT);
                       	 if(urlP !== undefined){
                          ga('send', {'hitType': 'pageview','page': ''+urlP+'','title': ''+urlT+''});
	                     }
                        window.history.pushState({path:url},'',url);
						document.title = urlT;
						 return false;
						}
						}
					    return false;	
						}
						//exit();
					});
                 
             });
         });
		 function isScrolledIntoView(elem)
{
    var $elem = $(elem);
    var $window = $(window);
    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom  = elemTop + $elem.height();
	
     if(elemTop >= docViewTop)
	 {
		return (elemTop >= docViewTop); 
	 }
	else if(elemBottom >= docViewBottom)
	 {
		return (elemBottom >= docViewBottom); 
	 }	
	
}
    </script>
<?php require_once($DOCUMENT_ROOT . '/common/bottom.php'); ?>