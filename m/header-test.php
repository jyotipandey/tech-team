<!DOCTYPE html>

<html>
<head>
	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>

	<?php require_once('common/top.php'); ?>
  <div class="wagtag_wrp ui-content" data-role="content">
			<div class="white_wrp">
				<ul class="tabs">
					<li class="active" id="signinlink">Sign In</li>
					<li id="createaccountlink">Create Account</li>
				</ul>
				<div class="input-wrp">
					<div class="input-blk">
						<div><input type="text" placeholder="Email Address" data-role="none" id="Email_signin" /></div>
						<div><input type="text" placeholder="Password" data-role="none" id="Password_signin"/></div>
					</div>
					<a id="forgot_link">Forgot Password ?</a>
					<button class="btnstyle2 " id="signinPage">Sign In</button>
				</div>
			</div>
			<div class="tab_blk">
					<div class="or_txt mt20">Or</div>
					<button class="btnstyle1 facebookconnect" >
						<span class="facebook_icon"></span>Log in with Facebook
					</button>
					<button class="btnstyle1 red googleconnect" >
						<span class="google_icon"></span>Sign in with Google 
					</button>
				</div>
		</div>
    <?php require_once('common/bottom.php'); ?>