<!DOCTYPE html>
<html>
<head>
 <?php require_once('common/script.php'); ?>
    <title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title> 
     <div id="productnotavaliable_page37" class="whitewrapper" >
		<script type="text/javascript">
			$("#productnotavaliable_page37").on("pageinit", function() {				
				$("#backtopage37").click(function() {
					//$.mobile.changePage("#homepage_page32",{reverse:true});
					history.go(-1);
				});	
				var mySwiper = new Swiper('.swiper-container8',{
					pagination: '.pagination8',
					loop:true,
					grabCursor: true,
					paginationClickable: true
				  });
				  var mySwiper = new Swiper('.swiper-container13',{
					pagination: '.pagination13',
					grabCursor: true,
					loop:false,
					slidesPerView:'auto',
					paginationClickable: true,
			  });
			});
			$("#productnotavaliable_page37").on("pageshow", function() {

			});
		</script>
        <?php require_once('common/top.php'); ?>	
		<div data-role="content">
		<!----------------------------------------->
			<div class="product_det_blk">
				<div class="stock_det"><span>Out of Stock</span></div>
				<div class="device">
					<a class="arrow-left" href="#"></a> 
					<a class="arrow-right" href="#"></a>
					<div class="swiper-container8">
					  <div class="swiper-wrapper">
						<div class="swiper-slide"> <img src="images/pro-img.png"> </div>
						<div class="swiper-slide"> <img src="images/pro-img.png"> </div>
						<div class="swiper-slide"> <img src="images/pro-img.png"> </div>
					  </div>
					</div>
					<div class="pagination8"></div>
				</div>
			</div>
		<!----------------------------------------->
			
			<div class="product_info">
				<h2><u>Cibau Puppy</u> Formula All Breed Size Dog Food 3kg</h2>
				<p class="product_usage">Complete balanced food for puppies and gestating or breast-feeding bitches.</p>
				<div>
					<div class="rating"><img src="images/five_star.png"/></div>
				</div>
				<div class="price_info"><strike><p>Rs. 1,690</p></strike><span>Rs. 1,390</span></div>
				<div class="pro_saving"><p>Today's savings: <b>8%</b></p><p>Prices are inclusive of all taxes</p></div>
			</div>
			<div class="notify_det"><div><a href="">Notify Me</a></div></div>
			<div class="pro_detail">Cibau Puppy Formula is a complete food for puppies in the weaning and growth phase, as well as for pregnant and lactating bitches. Cibau Puppy Formula contains all the best possible ingredients to offer your dog a complete and balanced diet. Chicken to guarantee top protein intake.<p><a href="">>> Read More About this product</a></p>
			</div>
			<div class="recommendation product_sale">Recommended Products</div>
			
        </div>
	
    <?php require_once('common/bottom.php'); ?>
	