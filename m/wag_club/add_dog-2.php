<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";
$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");
$Testtotel = query_execute_row("SELECT count(*) as countrecord FROM dogs_activity WHERE publish_status ='publish' ORDER BY dog_id DESC");
?>

<!DOCTYPE html>
<html>
<head>
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<title>Add your Dog | WagClub</title>
<meta name="keywords" content="Add your Dog, WagClub" />
<meta name="description" itemprop="description" content="Add your Dog in WagClub WagClub Is a Pet Social Network, Dog Social Sites" />
<link type="text/css" rel="stylesheet" href="/wag_club/css/dg_style.css?h=12">
<style>
.ui-btn-icon-bottom:after, .ui-btn-icon-left:after, .ui-btn-icon-notext:after, .ui-btn-icon-right:after, .ui-btn-icon-top:after{ content:"" !important;}
.radio1, .radio2{height: 40px !important;}
.dog-info-form label{ padding-left:0px;}
.btn {
    display: block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}

.btn-primary {
    background-color: #d1d1d1;
}

.fileUpload {
    position: relative;
    overflow: hidden;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.ui-input-text input {
    border: none!important;
    border-bottom: 1px solid #ccc!important;
}
</style>
<form>
  <div class="dog-info-form">

    <div class="form-item">
      <label>Where does he live?</label>
      <input data-role="none"  type="text" placeholder="City Name" style="width: 48%; display: inline-block; background-color: #fff;">
      <input data-role="none"  type="text" placeholder="State Name" style="width:48%; display: inline-block; background-color: #fff;">
    </div>
    
       <div class="form-item">
      <label>Short Description</label>
  
                        <textarea placeholder="Write Something" id="dogText_Msg" rows="10"></textarea>
			
            
    </div>
                
                <div class="fileUpload btn btn-primary form-item">
                <span>Upload Photo</span>
                <input type="file" accept="image/*" name="upload_logo" id="upload_logo" class="upload" value="Upload Business Logo"/>
                </div>
    <span style="display:block; text-align:center;margin-top: -10px;">Minimum size of the file should be - 200Kb</span>
	<div id="image-holder" class="image-holder form-item"></div>
    <div class="btn-finish" id="gotomydogs-page8"><a href="">FINISH</a></div>
    <div class="wow"></div>
  </div>
</form>
<script type="text/javascript">
		$("#upload_logo").on('change', function () {
			//Get count of selected files
			var countFiles = $(this)[0].files.length;
			var imgPath = $(this)[0].value;
			var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
			var image_holder = $("#image-holder");
			image_holder.empty();
			if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg"){
				
				if (typeof (FileReader) != "undefined"){
					//loop for each file selected for uploaded.
					for (var i = 0; i < countFiles; i++){
						var reader = new FileReader();
						reader.onload = function (e){
							$("<img />", {
								"src": e.target.result,
								 "class": "thumb-image"
							}).appendTo(image_holder);
						}
						image_holder.show();
						reader.readAsDataURL($(this)[0].files[i]);
					}
				}else{
					alert("This browser does not support FileReader.");
				}
			}else{
				alert("Pls select only images");
			}
		});
	</script>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
