<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";
$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");?>

<!DOCTYPE html>
<html>
<head>
<title>Wag Club | Pet Social Networks | Dog Social Site</title>
<meta name="keywords" content="Wag Club,Pet Social Networks, Dog Social Site" />
<meta name="description" content="Wag Club. An Exclusive club just for your pet. +Add Your Dog at Wag Club" />
<link type="text/css" rel="stylesheet" href="css/dg_style.css?h=22">

<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
</head>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<div style="float:left; width:100%;">
  <div class="dog_rec_dg1">
<div class="text_track">
<div class="text_box_dg">8 Most Popular Dogs</div>

</div>


<div class="dg_popMost_top">
<ul>
<li>
<a href="/dogs/notty_7/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/450-450-1043075_DSC_2749.JPG" alt="Notty" title="Notty">

<div class="dg_popTxt">
<h3>Notty</h3>
<h4>Labrador Retriever</h4>
<p>Do not be fooled with these bambi eyes, this prankster can be quiet a handful</p>
</div>
</a>

</li>

<li>
<a href="/dogs/jumbo_31/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/450-450-600x600-1043071_image.jpeg" alt="Jumbo" title="Jumbo">
 
<div class="dg_popTxt">
<h3>Jumbo</h3>
<h4>Lhasa Apso</h4>
<p>This energy bundle keeps its owners on its feet always with her antics</p>
</div>
</a>

</li>
</ul>
</div>



<div class="dg_popMost_top">
<ul>
<li>


<a href="/dogs/stella_47/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/270-270-1042565_12033081_10204516265766008_7361282637287533533_n.jpg" alt="Jumbo" title="Jumbo">

<div class="dg_popTxt">
<h3>Stella</h3>
<h4>Labrador Retriever</h4>
</div>
</a>
</li>


<li>
<a href="/dogs/iris-kol-achabh/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/270-270-1041824_DSC_0322.JPG" alt="Jumbo" title="Jumbo">

<div class="dg_popTxt">
<h3>Iris Kol Achabh</h3>
<h4>St.Bernard</h4>
</div>
</a>

</li>




</ul>
</div>


<div class="dg_popMost_top">
<ul>
<li><a href="/dogs/tito_3/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/270-270-1040012_IMG_2613.JPG" alt="Jumbo" title="Jumbo">

<div class="dg_popTxt">
<h3>Tito</h3>
<h4>Lhasa Apso</h4>
</div></a>

</li>
<li>
<a href="/dogs/bruno_1047/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/270-270-1039293_DSC_2984.JPG" alt="Jumbo" title="Jumbo">

<div class="dg_popTxt">
<h3>Bruno</h3>
<h4>Labrador Retriever</h4>
</div></a>
</li>

<li>

<a href="/dogs/ginger_6/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/270-270-IMG_88171880848710.jpeg" alt="Jumbo" title="Jumbo">

<div class="dg_popTxt">
<h3>Ginger</h3>
<h4>Cocker Spaniel (American)</h4>
</div></a>
</li>

<li>

<a href="/dogs/canny/" data-ajax="false">

  <img src="https://www.dogspot.in/dogs/images/270-270-DSC_5855.JPG" alt="Jumbo" title="Jumbo">
 
<div class="dg_popTxt">
<h3>Canny</h3>
<h4>Labrador Retriever</h4>
</div></a>
</li>
</ul>


</div>

</div>
  
  <!-- add dog -->
  <div class="dog_slider" style="float: left;  width: 100%; ">

<div class="left_slider_dg">
<h1>Have more than one?</h1>
<p>Your dog is always at the forefront in WagClub!</p>
</div>
<div class="ryt_text_dg">
<a href="/wag_club/add_dog.php" data-ajax="false">
<div class="ad_dg_btn"><span>+</span><span style="padding: 8px;">Add Your Dog</span></div>
</a>
</div>
</div>
  <!-- add end--> 
</div>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
