<div class="form-placeholder">
		  			<div class="control-group default_requester">
		  				 <label class=" required control-label requester-label " for="helpdesk_ticket_email">Requester</label>
            <div class="controls   ">
              <div class="row-fluid">
	<input class="span12 email required" data-check-email-path="/support/tickets/check_email" data-remote-triggers="focusout" data-remote="true" id="helpdesk_ticket_email" name="helpdesk_ticket[email]" size="30" type="email" />
		
	<div id="name_field" class="default_name_field hide">
		<input class="text span12 name_field" placeholder="Your Name" 
			name="helpdesk_ticket[name]" type="text" disabled="disabled" data-user-name=""/>
	</div>
</div>
            </div> 
		  			</div>
		  			<div class="control-group default_subject">
		  				 <label class=" required control-label subject-label " for="helpdesk_ticket_subject">Subject</label>
            <div class="controls   ">
              <input class=" required text span12" id="helpdesk_ticket_subject" name="helpdesk_ticket[subject]" size="30" type="text" />
            </div> 
		  			</div>
		  			<div class="control-group default_description">
		  				 <label class=" required control-label description-label " for="helpdesk_ticket_description">Description</label>
            <div class="controls   ">
               <textarea class=" required_redactor html_paragraph span12" cols="40" id="helpdesk_ticket_ticket_body_attributes_description_html" name="helpdesk_ticket[ticket_body_attributes][description_html]" rows="6">
</textarea>   
            </div> 
		  			</div>
		  	<input id="helpdesk_ticket_source" name="helpdesk_ticket[source]" type="hidden" value="9" />
	  	</div>
  	<div class="form-portal">
</div>
  <div class="control-group">
    <div class="controls recaptcha-control">
      <div id="captcha_wrap">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="g-recaptcha " data-sitekey="6LdvejUUAAAAAEn6wjOFcPVRyQr4KtAJ03ltA1SQ" display="themewhitesizecompact"></div>
          <noscript>
            <div>
              <div style="width: 302px; height: 422px; position: relative;">
                <div style="width: 302px; height: 422px; position: absolute;">
                  <iframe
                    src="https://www.google.com/recaptcha/api/fallback?k=6LdvejUUAAAAAEn6wjOFcPVRyQr4KtAJ03ltA1SQ"
                    frameborder="0" scrolling="no"
                    style="width: 302px; height:422px; border-style: none;">
                  </iframe>
                </div>
              </div>
              <div style="width: 300px; height: 60px; border-style: none;
                bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
                background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
                <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                  class="g-recaptcha-response"
                  style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
                  margin: 10px 25px; padding: 0px; resize: none;" value="">
                </textarea>
              </div>
            </div>
          </noscript>

        <div class="recaptcha-error-message hide" id="helpdesk_ticket_captcha-error">
          CAPTCHA verification is required.
        </div>
      </div>
      <!-- end captch_wrap -->
      <input type="hidden" name="check_captcha" value="true" />
    </div>
  </div>
  <div class="extra-space"></div>
</div>