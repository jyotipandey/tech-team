<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop-policy";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot Shop Policy | DogSpot</title>
<meta name="keywords" content="DogSpot Shop Policy | DogSpot" />
<meta name="description" content="DogSpot Shop Policy | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div class="cont980">
<div class="pageBody">
<br />
<h1> Policies of DogSpot</h1>
<div class="vs10"></div>

<form method="post" action="https://api-3t.sandbox.paypal.com/nvp">
		<input type="hidden" name="USER" value="API_username">
		<input type="hidden" name="PWD" value="API_password">
		<input type="hidden" name="SIGNATURE" value="API_signature">
		<input type="hidden" name="VERSION" value="XX.0">
		<input type="hidden" name="PAYMENTREQUEST_0_PAYMENTACTION" value="Sale">
		<input type="hidden" name="PAYERID" value="7AKUSARZ7SAT8">
		<input type="hidden" name="TOKEN" value= "EC%2d1NK66318YB717835M">
		<input type="hidden" name="PAYMENTREQUEST_0_AMT" value= "19.95">
		<input type="submit" name="METHOD" value="DoExpressCheckoutPayment">
	</form>
    
</div>    
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
