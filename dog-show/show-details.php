<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
 $sel=query_execute("SELECT * FROM events WHERE event_id = '$event_id'");
 $selget=mysql_fetch_array($sel);
 $date=$selget['start_date'];
 $edate=$selget['end_date'];
 $organized_by=$selget['organized_by'];
 $member_club=$selget['member_club'];
 $member_sec=$selget['member_sec'];
 $map=$selget['map'];
 $event_name=$selget['event_name'];
 $show_id=$selget['link_id'];
 $location=$selget['venue'];
 $seljud=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' ORDER BY judge_id DESC");
 $judge=$seljud['judge'];
 $judge_id=$seljud['judge_id'];
 $seljud1=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id' ORDER BY judge_id DESC");
 $judge1=$seljud1['judge'];
 $judge_id1=$seljud1['judge_id'];
 $seljud2=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id1' AND judge_id!='$judge_id' ORDER BY judge_id DESC");
 $judge2=$seljud2['judge'];
 $judge_id2=$seljud2['judge_id'];
 $seljud3=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id2' AND judge_id!='$judge_id' AND judge_id!='$judge_id1' ORDER BY judge_id DESC");
 $judge3=$seljud3['judge'];

 $selk=query_execute_row("SELECT kennel_name, contact_detail FROM kennel_club WHERE kennel_id='$organized_by'");
 $kname=$selk['kennel_name'];
 $contact_detail=$selk['contact_detail'];
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$event_name?> Dog Show Details | Dog Show Information </title>
<meta name="keywords" content="<?=$event_name?> Dog Show Details,Dog Show Information" />
<meta name="description" content="<?=$event_name?> Check Dog Show Details and Dog Show Information " />
<link rel="canonical" href="https://www.dogspot.in<?=$_SERVER[REQUEST_URI]?>" />
<script type="text/javascript" src="/dog-show/js/main.js" ></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		selectFirst: false
	});
});

</script>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<!-- dog show schedules -->
<div class="dogshow-slider-sec">
 <div id="dogshow_banner_holder">
 
      <div id="dogshow_banner"> <img src="/dog-show/Images/photos/show-schedules.jpg" alt="" class="active" width="100%" height="319" />
     </div>
     <div id="wrapper" class="clearfix">
      <div id="showsechedule_text">
        <h1>Show schedules</h1>
      </div>

    </div></div></div>
    
<!-- dog show schedules -->
<!-- dog show nav -->
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-show/" class="butt_1" rel="nofollow">Home</a></li>
	    <li><a href="/dog-show/about-us.php" class="butt_2" rel="nofollow">About us</a>  </li>
      
	    <li><a href="/dog-show/upcoming-shows.php" id="" class="butt_3" rel="nofollow">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4" rel="nofollow">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/dog-show/search.php" method="post">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  
<!-- dog show nav -->

<div id="wrapper" class="clearfix">
	
 <div id="show_results_full_column">
        <div id="dog_schedule_start">
          <h2><? echo $kname."</br>".$event_name?></h2>
        </div>
 </div>
       

<div id="schedule_col_left">
          <table id="tbl_data" border="0" width="100%">
            <tbody><tr>
              <td width="25"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="16" width="16"></td>
              <td width="597"><strong>Show date:</strong> <? print(showdate($date, "d M Y"));?></td>
            </tr>
            <? if($edate != $date){ ?>
            <tr id="">
	<td><img src="/dog-show/Images/buttons/tbl_mail.jpg" alt="Postal" height="11" width="16"></td>
	<td><strong>Postal entries close on:</strong> <? print(showdate($edate, "d M Y"));?></td>
</tr>
 <tr id="">
	<td><img src="/dog-show/Images/buttons/data-entry-icon.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Online entries close on:</strong> <? print(showdate($edate, "d M Y"));?></td>
</tr>
<? }?>
<tr id="">
	<td><img src="/dog-show/Images/buttons/organized by.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Organized By:</strong> <?=$selk['kennel_name'];?></td>
</tr>

<tr id="">
	<td><img src="/dog-show/Images/buttons/judges.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Judges:</strong> <?=$judge?><? if($judge1!=''){ echo ", ".$judge1;} if($judge2!=''){ echo ", ".$judge2;} if($judge3!=''){ echo ", ".$judge3;} ?></td>
</tr>

<tr id="">
	<td><img src="/dog-show/Images/buttons/venue.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Venue:</strong> <?=$location?></td>
</tr>
          </tbody></table>
          <font <p="" size="+1"><p></p>
            
          <?=$map;?>	
        </font>
        <div >&nbsp;</div>
        <table id="tbl_data" border="0" width="100%">
        <tr id="">
	<td><img src="/dog-show/Images/buttons/Contact-icon.jpg" alt="Globe" height="16" width="16"></td>
	<td style="width:380px; float:left;"><strong>Contact Detail:</strong> <?=$contact_detail?></td>
</tr>
</table>
        </div>

<div id="schedule_butts"> 
            <a id="" class="schedule_butts_3" href="/dog-show/Images/entry-form.jpg" target="_blank">Entry Form</a>
            <? if($show_id){?>
            <a id="" class="schedule_butts_4" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>" target="_blank">Enter on-line</a>
            <? }?> 
        </div>
<div id="online_help">
        <h6>Online help</h6>
        <p>Online support is available 
        Monday - Friday 10:00am - 06:00pm, please call now on +91-7840011911.</p>
         </div>
  </div>
  
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>