<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$title="Dog Show What We Do | DogSpot.in";
$keyword="Dog Show What We Do, DogSpot.in";
$desc="What we do and what is DogSpot.in ";
 
    $alternate="https://www.dogspot.in/dog-events/what-we-do/";
	$canonical="https://www.dogspot.in/dog-events/what-we-do/";
	$og_url=$canonical;
	$imgURLAbs="";
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>

<link type="text/css" rel="stylesheet" href="/bootstrap/css/dog-show.css?v=2">
<section class="dog-show-section">
<div class="container-fluid  visible-lg visible-md">
<div class="row">
<div class="col-md-12 padding-0">
<img src="https://ik.imagekit.io/2345/dog-show/Images/photos/about-us-dog.jpg" width="1350" height="319" class="img-responsive" style="width:100%;padding-top:10px;">
<div class="slideshowtext">
        <h1 style="color:#222;">What We Do</h1>
        
          </div>
</div>

</div>
</div>
<div class="container-fluid breeds-nav">
    <div class="container">
      <nav class="navbar">
        <ul class="nav navbar-nav">
          
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
        
        
      </nav>
    </div>
  </div>
<div class="container ds-about-us">
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-8">
      <h2>What We Do</h2>
       <p>DogSpot is elated to announce a new and advanced section for ‘Dog Shows’. Here in addition, to the prevalent facilities (photography, results, certificates) we are introducing a plethora of new features to make the administrative process of Dog Shows simpler and digitally accessible.</p>
      <p>The services provided are as follows: </p>
      <ul>
        <li>Register Your Dog for a show online</li>
        <li>Organize a dog show</li>
        <li>Keep up to date with dog show results and pictures</li>
        <li>Know the winners and connect via Wag Club</li>
      </ul>
      <p>Furthermore, we also provide some additional services to the kennel clubs such as: </p>
      <ul>
        <li>Printing and presenting of show certificates</li>
        <li>Printing and submitting of the ‘show return’ to KCI of every dog show</li>
        <li>Information about Kennel Clubs and respective breed standards</li>
        <li>SMS updates to confirm new registration as well as event and venue details a day before the dog show</li>
      </ul>
      <p>In addition to the primary features listed above. Users can also check out the history and profiles of the following: </p>
      <ul>
        <li>Breeder</li>
        <li>Show Judge</li>
        <li>Ring Steward</li>
        <li>Participant and Winner Dogs (via Wag Club)</li>
      </ul>
       
      <div class="col-xs-12 col-sm-12 col-md-6"> 
      <img src="https://ik.imagekit.io/2345/dog-show/Images/photos/dog_phone.jpg" alt="Dog Holding Phone" class="float_left_img" width="233" height="209">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
        <h4 style="margin-top: 10px;">Get in contact</h4>
        <a class="dogshow_contact" href="/contactus.php">
        Contact us</a> </div>
        </div>

<aside class="col-xs-12 col-sm-12 col-md-4">
        <div class="row">
         <!-- Widget header -->
         <section class="col-sm-6 col-md-12 widget "> 
            <header class="clearfix">
              <h4>In this section</h4>
            </header>
         <div class="dogshow_sub_nav">
  <ul>
           <li><a href="/dog-events/about-us/">About us</a></li>
            <li><a href="/dog-events/what-we-do/">What we do</a></li>
          </ul>
        </div> 
            </section>
          
         
        </div>
      </aside>
</div>
</div>
      
</section>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
