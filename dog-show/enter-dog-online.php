<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Dog Show Enter Online Data | Fill Online Dog Show Data </title>
<meta name="keywords" content="Dog Show Enter Online Data,Fill Online Dog Show Data" />
<meta name="description" content="Fill Online Dog Show Data at Dog Show Section in DogSpot" />
<script type="text/javascript" src="/dog-show/js/main.js" ></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<!-- dog show schedules -->
<div class="dogshow-slider-sec">
 <div id="dogshow_banner_holder">
 
      <div id="dogshow_banner"> <img src="/dog-show/Images/photos/show-schedules.jpg" alt="" class="active" width="100%" height="319" />
     </div>
     <div id="wrapper" class="clearfix">
      <div id="showsechedule_text">
        <h1>Enter Your Dog</h1>
      </div>

    </div></div></div>
    
<!-- dog show schedules -->
<!-- dog show nav -->
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-show/" class="butt_1">Home</a></li>
	    <li><a href="/dog-show/about-us.php" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-show/upcoming-shows.php" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/dog-show/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/dog-show/search.php" method="post">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  
<!-- dog show nav -->
<div class="cont980">
<div id="wrapper" class="clearfix">   
    <div  class="recent_show_albem">
    <h1> Enter Your Dog</h1>
     
<div style="display: block;font-family: 'SegoeUI';border-bottom: 0px;width: 422px; overflow: hidden;font-size: 18px;margin: auto; padding-top:15px;">
    <strong>There is no dog listed by you please enter you dog.</strong>
    
<div class="kci_proced_btnDiv" style="text-align:center;">
<a href="/dog-show/data-entry.php?show_id=<?=$show_id?>"><input type="button" class="kci_add_newDogbtn" value="Add New Dog" /></a>
</div>
    </div>
</div>
</div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>