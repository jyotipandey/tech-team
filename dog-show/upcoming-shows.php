<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dog Show, Dog Events, Dog Championship, KCI | Dog Spot</title>
<meta name="keywords" content="Dog Show, Dog Events, Dog Championship, KCI, Dog Spot" />
<meta name="description" content="Dog Show, Dog Events, Dog Championship, KCI | Dog Spot" />
<script type="text/javascript" src="/dog-show/js/main.js" ></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<!-- dog show schedules -->
<div class="dogshow-slider-sec">
 <div id="dogshow_banner_holder">
 
      <div id="dogshow_banner"> <img src="/dog-show/Images/photos/show-schedules.jpg" alt="" class="active" width="100%" height="319" />
     </div>
     <div id="wrapper" class="clearfix">
      <div id="showsechedule_text">
        <h1>Show schedules</h1>
      </div>

    </div></div></div>
    
<!-- dog show schedules -->
<!-- dog show nav -->
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-show/" class="butt_1" rel="nofollow">Home</a></li>
	    <li><a href="/dog-show/about-us.php" class="butt_2" rel="nofollow">About us</a>  </li>
      
	    <li><a href="/dog-show/upcoming-shows.php" id="" class="butt_3" rel="nofollow">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4" rel="nofollow">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/dog-show/search.php" method="post">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  
<!-- dog show nav -->


      <!-- dog show schedules -->

<div id="wrapper" class="clearfix">	

	<div id="content">
        
     
 <div id="show_results_full_column">
        <div id="dog_schedule_start">
          <h2>Shows</h2>
        </div>
 </div>
 <form id="formOrderSearch" name="formOrderSearch" method="post">
        <div id="dogshow_filter_div">
          <div id="shows_frm">
   <select name="typeid" id="typeid" class="dogshow_filter_drop">
	<option value="">Display by...</option>
     <? $seshow=query_execute("SELECT * FROM show_type");
 while($getre=mysql_fetch_array($seshow)){
	 $type_id=$getre['type_id'];
	 $type_name=$getre['type_name'];
	 ?>
 <option  value="<?=$type_id?>"<? if($typeid=='$type_id'){ echo "selected='selected'";} ?>><?=$type_name?></option>
 <? }?>
 </select>
              <input name="searchOrder" id="searchOrder" class="dogshow_gobtn" src="" alt="Go" style="" value="go" type="submit">
            
          </div>
        </div>
		</form>		
<div class="dogshow_sechduletable">
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Location</th>
            <th class="" valign="top" width="129"> Closing date</th>
            <th class="" valign="top" width="117"> Days left</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
//echo "SELECT * FROM show_description WHERE entry!='close' $showtypeid $secdate";
		$showdes=query_execute("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' ORDER BY start_date ASC");

$s=1;
while($getshowdata=mysql_fetch_array($showdes)){
$show_name=$getshowdata['event_name'];
$location=$getshowdata['venue'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['start_date'];
$edate=$getshowdata['end_date'];
$show_id=$getshowdata['link_id'];
$event_id=$getshowdata['event_id'];
$organized_by=$getshowdata['organized_by'];
$event_nice_name=$getshowdata['event_nice_name'];
$selke=query_execute_row("SELECT kennel_name, city FROM kennel_club WHERE kennel_id='$organized_by'");

$toDis=date("Y-m-d H:i:s");
$startTimeStamp = strtotime($edate, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);
$numberDays = $timeDiff/86400;  // 86400 seconds in one day
// and you might want to convert to integer
$numberDays = intval($numberDays);
?>        
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><img src="/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14"></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="/dog-show/show-details.php?event_id=<?=$event_id;?>">
				<?=$selke['kennel_name']."</br>".$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$selke['city'];?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="129"><? print(showdate($edate, "d M Y"));?></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <?=$numberDays?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <? if($show_id){?>
                    <a id="" class="showschedule_enter_online" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
                    <? }?>
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/dog-show/show-details.php?event_id=<?=$event_id?>">Show details</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon blueborder" <? }else{ ?> class="dark icon blueborder" <? }?> valign="top" width="62"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
               
        </tbody></table>

      </div>


  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>