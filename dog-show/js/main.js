// We buy stamps

if (typeof sIFR == "function") {
    sIFR.replaceElement(named({sSelector: "#sidebar h2", sFlashSrc: "/flash/fonts/trebuchet.swf", sColor: "#ffffff", sWmode: "transparent"}));
    sIFR.replaceElement(named({sSelector: "#sidebar p.sifr", sFlashSrc: "/flash/fonts/trebuchet.swf", sColor: "#ffffff", sWmode: "transparent"}));    
    sIFR.replaceElement(named({sSelector: "#sidebar p.large", sFlashSrc: "/flash/fonts/trebuchet.swf", sColor: "#ffffff", sWmode: "transparent"}));    
}

var WBS = {

    checkSubmitForm: function (fields) {
        
        var validated = true;
        var message = 'Please fill in following fields:\n';

        for (var i = 0; i < fields.length; i++) {
            var field = document.getElementById(fields[i][0]);
            if (field.value == '') {
                message += fields[i][1] + '\n';
                validated = false;
            }
        }
        
        if (validated) {
            document.getElementById('contact-form').submit();
        } else {
            alert(message);
        }
    }
} 

sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
