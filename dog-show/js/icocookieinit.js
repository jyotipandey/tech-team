// create a new ICO cookie object
var ico = new ICOCookie();

// mandatory settings
ico.DesignPath = '/cookie/cookiepopup.htm'; // the path to the htmlpage which contains the content 
ico.AcceptID = 'cookiepolicy-accept'; // the id of the element on the htmlpage which when clicked is an acceptance
ico.InfoID = 'cookiepolicy-more'; // the id of the element on the htmlpage which when clicked redirects to the info page
ico.InfoPath = '/help/cookies.html'; // the path of the info page

// optional settings
ico.DesignHolder = 'cookiepl'; // optional. The id of the element on the htmlpage which contains the html to render. This only works when running with jQuery
ico.Position = "BottomLeft";  // default = 'BottomLeft', valid values are TopLeft, TopRight, BottomLeft, BottomRight
ico.Margin = 50; // the distance the popup will appear from the left or right of the page
ico.ValidFor = 365 // default = 365, the number of days that the cookie which holds the acceptance is valid for
ico.NoJQuery = false // default = false. force the dialog to show without using the JQuery libs - this will only work when run through IIS not the filesystem

// initialise
ico.Init();