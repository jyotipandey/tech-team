function ICOCookie() 
{
    this.DesignPath = '';
    this.AcceptID = '';
    this.InfoID = '';
    this.InfoPath = '';
    this.DesignHolder = '';
    this.ValidFor = 365;
    this.Position = "BottomLeft";
    this.Margin = "20";
    this.NoJQuery = false;
}

function ICOCookie_PositionStyle() {
    if (this.Position == "TopLeft")
        return "left:" + this.Margin + "px;top:0px;";
    if (this.Position == "TopRight")
        return "right:" + this.Margin + "px;top:0px;";
    if (this.Position == "BottomLeft")
        return "left:" + this.Margin + "px;bottom:0px;";
    if (this.Position == "BottomRight")
        return "right:" + this.Margin + "px;bottom:0px;";
    if (this.Position == "BottomCenter")
        return "bottom:0px;";

}

function ICOCookie_InitNoJQuery() {
    var ico = this;
    window.onload = function () {
        var cpaccepted = ico.GetCookie('icocookie__accepted');
        if (cpaccepted == null || cpaccepted != 'yes') {
            var objDiv = document.createElement("div");
            objDiv.setAttribute('id', 'icocookie_outer');
            objDiv.setAttribute('style', 'position:fixed;display:none;' + ico.PositionStyle());
            document.body.appendChild(objDiv);
            var xmlhttp = null;
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            }
            else if (window.ActiveXObject) {
                xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.status == 200) {
                        objDiv.innerHTML = xmlhttp.responseText;
                        objDiv.style.display = '';
                        document.getElementById(ico.AcceptID).onclick = function () {
                            var expires = new Date();
                            expires.setDate(expires.getDate() + ico.ValidFor);
                            ico.SetCookie('icocookie__accepted', 'yes', expires);
                            document.getElementById('icocookie_outer').style.display = 'none';
                        };
                        document.getElementById(ico.InfoID).onclick = function () {
                            location.href = ico.InfoPath;
                        };
                    }
                }
            }
            xmlhttp.open('GET', ico.DesignPath);
            xmlhttp.send();
        }
    };
}

function ICOCookie_Init() {
    if (this.NoJQuery || typeof(jQuery) == 'undefined') {
        this.InitNoJQuery();
        return;
    } 

    var ico = this;
    jQuery(document).ready(function () {
        var cpaccepted = ico.GetCookie('icocookie__accepted');
        if (cpaccepted == null || cpaccepted != 'yes') {
            jQuery("body").prepend('<div id="icocookie_outer" style="position:fixed;display:none;' + ico.PositionStyle() + '"></div>');
            var designPath = ico.DesignPath;
            if (ico.DesignHolder != null && ico.DesignHolder != '')
                designPath += ' #' + ico.DesignHolder;
            jQuery('#icocookie_outer').load(designPath, function () {
                jQuery('#' + ico.AcceptID).click(function () {
                    var expires = new Date();
                    expires.setDate(expires.getDate() + ico.ValidFor);
                    ico.SetCookie('icocookie__accepted', 'yes', expires);
                    jQuery('#icocookie_outer').slideUp(1000);
                });
                jQuery('#' + ico.InfoID).click(function () { location.href = ico.InfoPath; });
                jQuery('#icocookie_outer').slideDown(1000);
            });
        }
    });
}

function ICOCookie_SetCookie(cookieName, cookieValue) {
    var argv = ICOCookie_SetCookie.arguments;
    var argc = ICOCookie_SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    var str = cookieName + "=" + escape(cookieValue) +
		((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
		((path == null) ? "" : ("; path=" + path)) +
		((domain == null) ? "" : ("; domain=" + domain)) +
		((secure == true) ? "; secure" : "");
    document.cookie = str;
}

function ICOCookie_GetCookie(cookieName) {
    var arg = cookieName + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg) return this.GetCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

function ICOCookie_DeleteCookie(cookieName) {
    var argv = ICOCookie_DeleteCookie.arguments;
    var argc = ICOCookie_DeleteCookie.arguments.length;
		var exp = new Date();
		exp.setDate (exp.getDate() - 1);  // This cookie is history
		var cval = this.GetCookie(cookieName);
                if( argc > 1 ) {
		    this.SetCookie( cookieName, null, exp, argv[1] );
                } else {
		    this.SetCookie( cookieName, null, exp );
                }
}

function ICOCookie_GetCookieVal(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

ICOCookie.prototype.PositionStyle = ICOCookie_PositionStyle
ICOCookie.prototype.InitNoJQuery = ICOCookie_InitNoJQuery;
ICOCookie.prototype.GetCookie = ICOCookie_GetCookie;
ICOCookie.prototype.SetCookie = ICOCookie_SetCookie;
ICOCookie.prototype.DeleteCookie = ICOCookie_DeleteCookie;
ICOCookie.prototype.Init = ICOCookie_Init;
ICOCookie.prototype.GetCookieVal = ICOCookie_GetCookieVal
