<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$session_id = session_id();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/dg_style.css" />
<link rel="stylesheet" type="text/css" href="cookie/cookie.css"/>
<style type="text/css">
  @import url("/css/reset.css");
</style>
<title>All Listed Dogs</title>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
    <script type="text/javascript" src="cookie/icocookie.js"></script>
    
    <script type="text/javascript" src="cookie/icocookieinit.js"></script>
<link href="css/layout.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script> 
<script type="text/javascript" src="js/fade.js"></script> 
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/Futura_Md_BT_400.font.js" type="text/javascript"></script>
<script type="text/javascript" src="js/main.js" ></script>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<meta name="keywords" content="" />
<meta name="description" content="" />
<body>
<div id="wrapper" class="clearfix">

  
    
	
	<div id="top_nav">
	  <ul id="dogshow_nav">
	    <li><a href="/" class="butt_1">Home</a></li>
	    <li><a href="/about_us/" class="butt_2">About us</a>
        </li>
	    <li><a href="/show_schedules/" id="ctl00_lnkSchedule" class="butt_3">Show schedules</a></li>
	    <li><a href="/show_results" id="ctl00_lnkResults" class="butt_4">Show results</a></li>
	   
      </ul>
	</div>
 
	<div id="content">
        
      <div id="dogshow_schedules_sec" class="dogshow_schedules_dog">
        <h1>Show schedules</h1>
      </div>
 <div id="show_results_full_column">
        <div id="dog_schedule_start">
          <h2>Shows</h2>
        </div>
 </div>
 <form id="formOrderSearch" name="formOrderSearch" method="post">
        <div id="dogshow_filter_div">
          <div id="shows_frm">
   <select name="typeid" id="typeid" class="dogshow_filter_drop">
	<option value="">Display by...</option>
     <? $seshow=query_execute("SELECT * FROM show_type");
 while($getre=mysql_fetch_array($seshow)){
	 $type_id=$getre['type_id'];
	 $type_name=$getre['type_name'];
	 ?>
 <option  value="<?=$type_id?>"<? if($typeid=='$type_id'){ echo "selected='selected'";} ?>><?=$type_name?></option>
 <? }?>
 </select>
              <input name="searchOrder" id="searchOrder" class="dogshow_gobtn" src="" alt="Go" style="" value="go" type="submit">
              <select name="shortdata" id="shortdata" class="dogshow_filter_drop">
	<option value="">Sort by...</option>
 <option  value="Name Of Show"<? if($shortdata=='Name Of Show'){ echo "selected='selected'";} ?>>Name Of Show</option>
 <option  value="Date Of Show"<? if($shortdata=='Date Of Show'){ echo "selected='selected'";} ?>>Date Of Show</option>
</select>
                <input name="searchOrder" id="searchOrder" class="dogshow_gobtn" alt="Go" type="submit" value="go" >
            
          </div>
        </div>
		</form>
<? 
if($typeid){
$showtypeid=" AND type_id='$typeid'"; } 
if($shortdata=='Date Of Show'){
	$secdate="ORDER BY date DESC";
	}
	if($shortdata=='Name Of Show'){
	$secdate="ORDER BY show_name ASC";
	}
?>		
<div class="dogshow_sechduletable">
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Location</th>
            <th class="" valign="top" width="129"> Closing date</th>
            <th class="" valign="top" width="117"> Days left</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
//echo "SELECT * FROM show_description WHERE entry!='close' $showtypeid $secdate";
$showdes=query_execute("SELECT * FROM show_description WHERE entry!='close' $showtypeid $secdate");
$s=1;
while($getshowdata=mysql_fetch_array($showdes)){
$show_name=$getshowdata['show_name'];
$location=$getshowdata['location'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['date'];
$show_id=$getshowdata['show_id'];
?>        
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><img src="images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14"></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="#"><?=$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$location?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="129"></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                    
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="#">Show details</a>
                    <a id="" class="showschedule_enter_online" href="user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon blueborder" <? }else{ ?> class="dark icon blueborder" <? }?> valign="top" width="62"><img src="images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
               
        </tbody></table>

      </div>


  </div>
  


</body>
</html>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>