<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

   if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/dog-show/about-us-bootstrap.php');
  exit;
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="canonical" href="https://www.dogspot.in/dog-events/about-us/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/dog-events/about-us/" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Dog Show About Us | Dog Show Details </title>
<meta name="keywords" content="Dog Show About Us,Dog Show Details" />
<meta name="description" content="Dog Show About us Information. Check More About DogSpot Show Sction." />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<script type="text/javascript" src="/dog-show/js/main.js" ></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>


<!-- dog show schedules -->
<div class="dogshow-slider-sec">
 <div id="dogshow_banner_holder">
 
      <div id="dogshow_banner"> <img src="/dog-show/Images/photos/about-us-dog.jpg" alt="" class="active" width="100%" height="319" />
     </div>
     <div id="wrapper" class="clearfix">
      <div id="showsechedule_text">
          <h1 style="color: #7E7878;">About Dog Show</h1>
      </div>

    </div></div></div>
    
<!-- dog show schedules -->
<!-- dog show nav -->
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/new/dog-events/search.php" method="GET">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  
<!-- dog show nav -->

<div id="wrapper" class="clearfix">

	
	<!-- search box end-->
	<div id="content">
	
    <div id="left_col">
      <h2 style="width: 100%;
    font-size: 22px;
    color: #9BC105;
    font-weight: normal;
    margin: 0px;
    float: left;
    padding-bottom: 5px;
    border-bottom: 3px solid #9BC105;
    overflow: hidden;">About Us</h2>
<?php /*?>      <div class="intro">Data handling is charged at a fixed fee per dog which includes the following:</div>
      <ul>
        <li>Numbering of entry forms</li>
        <li>Compilation of catalogue from entry forms (which need not be pre-sorted), checking the classes entered for age, sex, breed or variety</li>
        <li>Telephone calls to exhibitors concerning queries on entry forms</li>
        <li>Summary catalogue</li>
        <li>Accounts reports</li>
        <li>Optional reports (if requested) </li>
      </ul><?php */?>
      <div class="intro">DogSpot, since its inception in 2007 as a blogging website, has been a synonymous name with the Dog Show community.  The operations evolved towards the e-commerce dimension in 2011, with the initial focus of the entire blogging and commercial concept being targeted towards the show circles of breeders and judges which can be credited towards being the initial influencers of the setup and success.</div>
      <p>In some instances, it may appear that you are paying more [dependent upon entry] but if you take into consideration the increased profit on catalogue sales you will find that in real terms our costs have not increased.</p>
      <p>DogSpot owes as much to the national and regional kennel clubs for their growth as the committees do to DogSpot for their development. Till date, DogSpot handles the offline information systems such as data entry, photo albums and printing of certificates for various Kennel Clubs pan-India.<br />
      </p><div id="get_in_contact"> <img src="/dog-show/Images/photos/dog_phone.jpg" alt="Dog Holding Phone" class="float_left_img" width="233" height="209"/>
        <h2>Get in contact</h2>
        <a href="/contactus.php" class="cta_contact">Contact us</a> </div><!-- #EndLibraryItem --></div>
    <div id="right_col" class="clearfix">
	 <div class="dogshow_sider"><h3 style="margin: 0px; font-size: 22px;color: #9BC105;border-bottom: 3px solid #9BC105;padding-bottom: 5px;font-weight: normal;">In this section</h3></div>
      <div class="dogshow_sub_nav">
  <ul>
            <li><a href="/dog-events/about-us/">About us</a></li>
            <li><a href="/dog-events/what-we-do/">What we do</a></li>
          </ul>
        </div>
      </div></div>
  
</div></div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
