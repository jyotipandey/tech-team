<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Dog Show</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<?php  require_once($DOCUMENT_ROOT . '/new/common/header.php');?> 
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-show/" class="butt_1">Home</a></li>
	    <li><a href="about-us.php" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-show/upcoming-shows.php" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/dog-show/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/dog-show/search.php" method="GET">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  

<!--nav-->
<div id="wrapper" class="clearfix"> 
     <div>
	<div id="content">  
    <div class="cb vs10"></div>     
    <div  class="recent_show_albem">
	<h1>Showing Results For: <?=$_GET['searchName'];?> </h1>
    <h2> Albums</h2>
</div>
  <div class="dog_show_wrapper">
  <div class="dog_show_box">
 <div class="dog_show_wrapper">
  <div class="dog_show_box">
  <?php
  $sqlcis=query_execute("SELECT city,city_name,organized_by FROM events WHERE event_name LIKE '%".$_GET['searchName']."%'");
  $grtc=mysql_fetch_array($sqlcis);
  $gcitry=$grtc['city'];
  $gcitryname=$grtc['city_name'];
  $gorzy=$grtc['organized_by'];
  if($gcitry==''){
	  $sqlals=query_execute_row("SELECT city FROM kennel_club WHERE kennel_id='".$gorzy."'");
	  $gcitryname=$sqlals['city'];
	  }
  if($gcitry!=''){
	  $sqlcname=query_execute_row("SELECT city_name FROM city WHERE city_id='$gcitry'");
$qItem1=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE  album_link_name ='event' AND album_name LIKE '%".$sqlcname['city_name']."%' order by cdate desc limit 12 ");  
  }elseif($gcitry==''){
	  $sqlals=query_execute_row("SELECT city FROM kennel_club WHERE kennel_id='".$gorzy."'");
	  $scityk=$sqlals['city'];
	  $qItem1=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE  album_link_name ='event' AND album_name LIKE '%".$scityk."%' order by cdate desc limit 12 ");
	  }else{	
$qItem1=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE  album_link_name ='event' AND album_name LIKE '%".$_GET['searchName']."%' order by cdate desc limit 12 ");
	  }
$i=0;
while($rowItem = mysql_fetch_array($qItem1)){
	$album_id=$rowItem["album_id"];
	$album_link_name=$rowItem["album_link_name"];
	$i=$i+1;
	$album_name=$rowItem["album_name"];
	$nice_name=$rowItem["album_nicename"];
	
	$qdataM12=query_execute_row("SELECT cover_img FROM photos_image where album_id='$album_id' AND cover_img =  '1'");
	//$cover_image=$qdataM12['cover_img'];
	if($qdataM12) {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id' AND cover_img='1'");
	}
	else {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id'");	
	}
		$image=$qdataM["image"];
		$imagenice_name=$qdataM["image_nicename"];
		$title=$qdataM["title"];
		$date=showdate($qdataM["cdate"], "d M o");
	if($image){
	$src = $DOCUMENT_ROOT.'/photos/images/'.$image;
	$destm = $DOCUMENT_ROOT.'/imgthumb/200x134-'.$image;
	//echo $src;
	
	createImgThumbIfnot($src,$destm,'200','134','ratiowh');

 ?>
 <div class="dogshows_wrapperBox" <? if ($i%2 == 0){ echo "style='margin-right:0'";} ?>>
                
					<div class="dog_shows_thumbnail">

						<a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank">
	<img src="<?='/imgthumb/200x134-'.$image;?>" alt="<?=$title?>" title="<?=$title?>" height="134" width="200" align="middle"></a>
					</div>

					<div class="dog_shows_title">					
						<p class="dog_name"><a href="https://www.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank" ><? echo snippetwop($album_name, 18, '');
?></a></p>
<?
 $qcount= query_execute_row("SELECT count(*) as image from photos_image where album_id='$album_id'");?>
                           <p class="dog_date"><a href="#"><? echo $qcount["image"]." "."Photos";?></a> | <a href="#"><?=$date?></a>                  </p>					
					
					</div>

				</div>
                 <? // echo $i;      
} 

} 
?>
				</div>
				
 
 
 </div>
				
				</div>
				
 
 
 </div>
      </div>
      
	  </div>
      <div class="cb vs10"></div>
          <div  class="recent_show_albem">
	<h2>Dog Show Results </h2>
</div>
  <div class="dogshow_sechduletable">
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Address</th>
            <th class="" valign="top" width="129"> Date</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
	   if($gorzy==''){
		   	  $sqlals=query_execute_row("SELECT city FROM kennel_club WHERE kennel_id='".$gorzy."'");
	  $scityk=$sqlals['city'];
		  $selectShow = mysql_query("SELECT show_id, show_name, show_nicename, show_desc, location, date FROM show_description WHERE show_name LIKE '%".$gcitryname."%' ORDER BY date DESC LIMIT 7"); 
		   }else{
$selectShow = mysql_query("SELECT show_id, show_name, show_nicename, show_desc, location, date FROM show_description WHERE show_name LIKE '%".$gcitryname."%' ORDER BY date DESC LIMIT 7");
		   }
 if(!$selectShow){	die(mysql_error());	}
	$dit=0;
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT ring_id FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 
	 if($ring_id || $album_nicename){ $dit=$dit+1;
 if($ring_id ){?>      
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><img src="/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14"></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="/<?=$show_nicename?>/">
				<?=$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$location;?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <? print(showdate($date, "d M Y"));?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/<?=$show_nicename?>/">Show Results</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon blueborder" <? }else{ ?> class="dark icon blueborder" <? }?> valign="top" width="62"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
           <? }if($dit==5){break;}}?>    
        </tbody></table>

      </div>
      
       <div class="cb vs10"></div>
          <div  class="recent_show_albem">
	<h2>Upcoming Shows </h2>
</div>     
      <div class="dogshow_sechduletable">
      <?
	  $sqlalu=query_execute_row("SELECT kennel_id FROM kennel_club WHERE city  LIKE '%$gcitryname%'");
      		$showdes=query_execute("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' AND organized_by='".$sqlalu['kennel_id']."' ORDER BY start_date ASC LIMIT 5");
		$sqccount=mysql_num_rows($showdes);
		if($sqccount=='0'){
			?>
            <strong>No Upcoming Shows Listed For This Club</strong>
            <?
			}else{ ?>
        <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
          <tbody><tr>
            <th class=" " valign="top">&nbsp;</th>
            <th class="" valign="top" width="356">Show</th>
            <th class="" valign="top" width="115"> Location</th>
            <th class="" valign="top" width="129"> Closing date</th>
            <th class="" valign="top" width="117"> Days left</th>
            <th class=" " valign="top" width="171">&nbsp;</th>
          </tr>

<? 
$s=1;
while($getshowdata=mysql_fetch_array($showdes)){
$show_name=$getshowdata['event_name'];
$location=$getshowdata['venue'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['start_date'];
$edate=$getshowdata['end_date'];
$show_id=$getshowdata['link_id'];
$event_id=$getshowdata['event_id'];
$organized_by=$getshowdata['organized_by'];
$event_nice_name=$getshowdata['event_nice_name'];
$selke=query_execute_row("SELECT kennel_name, city FROM kennel_club WHERE kennel_id='$organized_by'");

$toDis=date("Y-m-d H:i:s");
$startTimeStamp = strtotime($edate, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);
$numberDays = $timeDiff/86400;  // 86400 seconds in one day
// and you might want to convert to integer
$numberDays = intval($numberDays);
?>        
               <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon" <? }else{ ?> class="dark icon" <? }?> valign="top"><img src="/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14"></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="341"><a id="" class="show_title_link" href="/dog-show/show-details.php?event_id=<?=$event_id;?>">
				<?=$selke['kennel_name']."</br>".$show_name;?></a></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="130"><span class="state_name"><?=$selke['city'];?></span></td>

                <td id="" <? if($s % 2 == 0){?> class="" <? }else{ ?> class="dark" <? }?> valign="top" width="129"><? print(showdate($edate, "d M Y"));?></td>

                <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
                    <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>></span> 
                  <?=$numberDays?>  
                </td>
				<td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
                    <? if($show_id){?>
                    <a id="" class="showschedule_enter_online" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
                    <? }?>
                    <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/dog-show/show-details.php?event_id=<?=$event_id?>">Show details</a>
                </td>
              </tr>
			  <tr>
                <td id="" <? if($s % 2 == 0){?> class="icon blueborder" <? }else{ ?> class="dark icon blueborder" <? }?> valign="top" width="62"><img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15"></td>

                <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top"><? print(showdate($date, "d M Y"));?></td>

              </tr>
            <? 
			$s=$s+1;
			}?>
               
        </tbody></table>
<? }?>
      </div>
             <div class="cb vs10"></div>
          <div  class="recent_show_albem">
          <? 
		  $sqkdet=query_execute("SELECT kennel_name, contact_detail FROM kennel_club WHERE city LIKE '%$gcitryname%'");
		  ?>
	<h2>About Club </h2>
</div>     
      <div class="dogshow_sechduletable">
      <? while($getcdet=mysql_fetch_array($sqkdet)){?>
         <table id="tbl_data" border="0" width="100%">
            <tbody><tr>
              <td width="25"><img src="/dog-show/Images/buttons/organized by.png" alt="Calendar" height="13" width="15"></td>
              <td width="597"><strong>Club Name:</strong> <?=$getcdet['kennel_name'];?></td>
            </tr>
            <tr>
              <td width="25"><img src="/dog-show/Images/buttons/Contact-icon.jpg" alt="Calendar" height="13" width="15"></td>
              <td width="597"><strong>Contact Detail:</strong> <?=$getcdet['contact_detail'];?></td>
            </tr>
            </tbody></table>
      <? }?>
      </div>
  </div>
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>