<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

   if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/dog-show/what-we-do-bootstrap.php');
  exit;
  }
 
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Dog Show What We Do | DogSpot.in </title>
<meta name="keywords" content="Dog Show What We Do, DogSpot.in" />
<meta name="description" content="What we do and what is DogSpot.in " />
<link rel="canonical" href="https://www.dogspot.in/dog-events/what-we-do/" />
<script type="text/javascript" src="/dog-show/js/main.js" ></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>
<link href="/dog-show/css/layout.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<script>
var txtbox = $('input[type="text"]');
txtbox.change(function () {
    txtbox.removeAttr('placeholder');
});
</script>
<script type="text/javascript">
$().ready(function() {
	$("#searchName").autocomplete("/dog-show/getall2.php", {
		width: 390,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
<style>

#dogshow_nav_header.fixed {
  position: fixed;
  top: 0px;
  float:left;
}
</style>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
 <!-- dog show schedules -->
<div class="dogshow-slider-sec">
 <div id="dogshow_banner_holder">
 
      <div id="dogshow_banner"> <img src="/dog-show/Images/photos/about-us-dog.jpg" alt="" class="active" width="100%" height="319" />
     </div>
     <div id="wrapper" class="clearfix">
      <div id="showsechedule_text">
          <h1 style="color: #7E7878;">What we do</h1>
      </div>

    </div></div></div>
    
<!-- dog show schedules -->
<!-- dog show nav -->
    <div class="dogshow_nav_header" id="dogshow_nav_header">
	<div id="wrapper" class="clearfix">
	<div id="ds_top_nav">
	  <ul id="nav">
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>     
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
	</div>
<div id="ds_searchbox">
   <div class="ds_searchboxl">
   <form action="/new/dog-events/search.php" method="GET">
    <input autocomplete="off" placeholder="Search here" id="searchName" name="searchName" value="" class="dogshow_input ac_input">
   </div>
   <div class="ds_searchboxr"><input class="dogshow_sub_btn" id="submit" name="submit" value="" type="submit"></div>
   </form>
</div>
		</div>
</div>  
<!-- dog show nav -->

<div id="wrapper" class="clearfix">

	
	<!-- search box end-->
	<div id="content">
	
    <div id="left_col">
      <h2 style="margin: 0px;  font-size: 22px;  color: #9BC105;border-bottom: 3px solid #9BC105;padding-bottom: 5px;font-weight: normal;">What We Do</h2>
      <div class="intro">DogSpot is elated to announce a new and advanced section for ‘Dog Shows’. Here in addition, to the prevalent facilities (photography, results, certificates) we are introducing a plethora of new features to make the administrative process of Dog Shows simpler and digitally accessible.</div>
      <p>The services provided are as follows: </p>
      <ul>
        <li>Register Your Dog for a show online</li>
        <li>Organize a dog show</li>
        <li>Keep up to date with dog show results and pictures</li>
        <li>Know the winners and connect via Wag Club</li>
      </ul>
      <p>Furthermore, we also provide some additional services to the kennel clubs such as: </p>
      <ul>
        <li>Printing and presenting of show certificates</li>
        <li>Printing and submitting of the ‘show return’ to KCI of every dog show</li>
        <li>Information about Kennel Clubs and respective breed standards</li>
        <li>SMS updates to confirm new registration as well as event and venue details a day before the dog show</li>
      </ul>
      <p>In addition to the primary features listed above. Users can also check out the history and profiles of the following: </p>
      <ul>
        <li>Breeder</li>
        <li>Show Judge</li>
        <li>Ring Steward</li>
        <li>Participant and Winner Dogs (via Wag Club)</li>
      </ul>
      <div id="get_in_contact"> <img src="/dog-show/Images/photos/dog_phone.jpg" alt="Dog Holding Phone" class="float_left_img" width="233" height="209"/>
        <h2>Get in contact</h2>
        <span class="get_in_touch_text">For further information, questions or advice on the services offered please contact here.</span> <a href="/contactus.php" class="cta_contact">Contact us</a> </div><!-- #EndLibraryItem --></div>
    <div id="right_col" class="clearfix">
	 <div class="dogshow_sider"><h3 style="margin: 0px;  font-size: 22px;  color: #9BC105;border-bottom: 3px solid #9BC105;padding-bottom: 5px;font-weight: normal;">In this section</h3></div>
      <div class="dogshow_sub_nav">
          <ul>
            <li><a href="/dog-events/about-us/">About us</a></li>
            <li><a href="/dog-events/what-we-do/">What we do</a></li>
          </ul>
        </div>
      </div></div>
  
</div></div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
