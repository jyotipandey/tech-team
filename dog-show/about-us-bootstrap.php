<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$title="Dog Show About Us | Dog Show Details ";
$keyword="Dog Show About Us,Dog Show Details";
$desc="Dog Show About us Information. Check More About DogSpot Show Sction.";
 
    $alternate="https://www.dogspot.in/dog-events/about-us/";
	$canonical="https://m.dogspot.in/dog-events/about-us/";
	$og_url=$canonical;
	$imgURLAbs="";
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>

<link type="text/css" rel="stylesheet" href="/bootstrap/css/dog-show.css?v=2">
<section class="dog-show-section">
<div class="container-fluid visible-lg visible-md">
<div class="row">
<div class="col-md-12 padding-0">
<img src="https://ik.imagekit.io/2345/dog-show/Images/photos/about-us-dog.jpg" width="1350" height="319" class="img-responsive" style="width:100%;padding-top:10px;">
<div class="slideshowtext">
        <h1 style="color:#222;">About Dog Show</h1>
        
          </div>
</div>

</div>
</div>
<div class="container-fluid breeds-nav">
    <div class="container">
      <nav class="navbar">
        <ul class="nav navbar-nav">
          
	    <li><a href="/dog-events/" class="butt_1">Home</a></li>
	    <li><a href="/dog-events/about-us/" class="butt_2">About us</a>  </li>
      
	    <li><a href="/dog-events/show-schedules/" id="" class="butt_3">Show schedules</a></li>
	    <li><a href="/show-results/" id="" class="butt_4">Show results</a></li>
	   
      </ul>
        
        
      </nav>
    </div>
  </div>
<div class="container ds-about-us">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-8 ">
      <h2>About Us</h2>
      <p>
      DogSpot, since its inception in 2007 as a blogging website, has been a synonymous name with the Dog Show community.  The operations evolved towards the e-commerce dimension in 2011, with the initial focus of the entire blogging and commercial concept being targeted towards the show circles of breeders and judges which can be credited towards being the initial influencers of the setup and success.
      </p>
      <p>
      In some instances, it may appear that you are paying more [dependent upon entry] but if you take into consideration the increased profit on catalogue sales you will find that in real terms our costs have not increased.</p>
      <p>
     DogSpot owes as much to the national and regional kennel clubs for their growth as the committees do to DogSpot for their development. Till date, DogSpot handles the offline information systems such as data entry, photo albums and printing of certificates for various Kennel Clubs pan-India.<br>
      </p>
      
      <div class="col-xs-12 col-sm-12 col-md-6"> 
      <img src="https://ik.imagekit.io/2345/dog-show/Images/photos/dog_phone.jpg" alt="Dog Holding Phone" class="float_left_img" width="233" height="209">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
        <h4 style="margin-top: 10px;">Get in contact</h4>
        <a class="dogshow_contact" href="/contactus.php">
        Contact us</a> </div>
        </div>


<aside class="col-xs-12 col-sm-12 col-md-4">
        <div class="row">
         <!-- Widget header -->
         <section class="col-sm-6 col-md-12 widget "> 
            <header class="clearfix">
              <h4>In this section</h4>
            </header>
         <div class="dogshow_sub_nav">
  <ul>
           <li><a href="/dog-events/about-us/">About us</a></li>
            <li><a href="/dog-events/what-we-do/">What we do</a></li>
          </ul>
        </div> 
            </section>
          
         
        </div>
      </aside>
</div>
</div>
      
</section>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
