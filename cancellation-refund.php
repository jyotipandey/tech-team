<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address
header("location: /shop-policy.php#cancelation");
exit();
/*
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">



<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order Cancellation | DogSpot</title>
<meta name="keywords" content="Order Cancellation | DogSpot" />
<meta name="description" content="Order Cancellation | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
*/ ?>
<?php /*require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); */ ?>
<? /*
<div class="cont980">
<div class="vs10"></div>
<h1>Order Cancellation</h1></br>
  <p>If you cancel your order before your product has shipped, we  will refund the entire amount.<br />
If the cancellation is after your product has shipped, you  can do one of the following:</p>
  <ol start="1" type="1">
    <li>If your product has shipped       but has not yet been delivered, contact Customer Support and inform them       of the same</li>
    <li>If you received the product,       it will be eligible for replacement, only in cases where there are defects       found with the product.</li>
  </ol>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); */ ?>
