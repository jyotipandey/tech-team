<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Android Developer at DogSpot.in</title>
<meta name="keywords" content="Android Developer, Android SDK, Social Medial API developer." />
<meta name="description" content="Urgent requirement of Android Developer at DogSpot.in with 1-3 years of experience in mobile domain." />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="vs20"></div>
<div class="cont980 pageBody">
<div class="vs10"></div>

  
  
  <h1>Android Developer</h1>
   <p><strong>Responsibilities include:</strong></p>
  <ul>
    <li> Drive development using the latest mobile standards & Android Application Architecture.</li>
    <li> Practically implement the Test Driven Development process.</li>
    <li>  Participate in all phases of our software development lifecycle in order to convert concepts and specifications into released Android applications and features.</li>
    <li> Design and Develop our application on the Android platform</li>
  <li> Apply Android best practices & Unit Testing Methodology for application development</li>
  <li>  Build, Distribute and Submit the Android app (Beta and Production) </li>
  <li>  Focus on Object Oriented Design and Design Patterns</li></ul>
  <p> <strong>Skills Required:</strong></p>
  <ul>
    <li>Expertise in Java and the Android SDK </li>
    <li>SQLite Database, ORM, GPS, HTTP(s) Connection </li>
    <li> Knowledge of dealing with Threads </li>
    <li>Previous experience with REST, JSON and XML </li>
    <li>Experience of using Social Medial API  </li>
    <li>  Excellent debugging skills on the platforms in question and proficient usage of the tools available in SDK</li>
    <li>  Good knowledge of performance and memory optimisations for the platforms in question</li>
  </ul>
   <p><strong>    Preferred Qualification:</strong></p>
  <ul>
    <li> 1-3 years of extensive work experience in the mobile domain</li>
   
  </ul>
  <p><strong> Basic Qualifications:</strong></p>
  <ul>
    <li>Good written and verbal communication</li>
    <li>Ability to work in a fast paced environment, both independently and as part of a team </li>
    <li>Creative ability for identifying & solving challenging user problems through ideation of new products</li>
   
  </ul>
  <p>Location: Gurgaon<br />
   Position open: 1<br />
    Position Type: Full-Time<br />
 <p> <h4> To apply please send your resume to <a href="mailto:meghna@dogspot.in">meghna@dogspot.in</a></h4></p>
  
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
