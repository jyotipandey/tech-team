<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Assistant Manager - Operations at DogSpot.in</title>
<meta name="keywords" content="Assistant Manager, Operations AM." />
<meta name="description" content="Urgent Requirement of Assistant Manager - Operations at DogSpot.in with 1-2 years of experience in FMCG retail or Ecommerce set up." />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="vs20"></div>
<div class="cont980 pageBody">
<div class="vs10"></div>

  
  
  <h1>Assistant Manager - Operations</h1>
   <p><strong>Responsibilities include:</strong></p>
  <ul>
    <li> Take ownership to Dispatch and Deliver the orders as received from the customers</li>
    <li>  Get Order processed with the help of team of packers</li>
    <li>  Adhere to Delivery and Dispatch SLAs and execute smooth fulfillment of orders by coordinating with logistics partners</li>
    <li> Ensure product availability by managing the supply chain and warehouse</li>
  <li> Support customer care  to solve issues</li>
 </ul>
  <p> <strong>Skills Required:</strong></p>
  <ul>
    <li>Good understanding of ecommerce order management cycle </li>
    <li>Fundamental knowledge of Inventory management and supply chain </li>
    <li>  Adopt to multi tasking and manage priority </li>
   
  </ul>
   <p><strong>    Preferred Qualification:</strong></p>
  <ul>
    <li> Any Graduate</li>
     <li> 1-2 Years  experience</li>
   
  </ul>
  <p><strong> Basic Qualifications:</strong></p>
  <ul>
    <li>Experience of working in logistics, FMCG retail or Ecommerce set up</li>
    <li>Good analytical and MIS skills</li>
   
   
  </ul>
  <p>Location: Gurgaon/Indore<br />
   Position open: 2<br />
    Position Type: Full-Time<br />
 <p> <h4> To apply please send your resume to <a href="mailto:meghna@dogspot.in">meghna@dogspot.in</a></h4></p>
  
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
