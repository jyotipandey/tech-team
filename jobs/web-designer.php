<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Web Designer at DogSpot.in | UI and UX designer | PHP application designer</title>
<meta name="keywords" content="Web Designer, UI and UX designer, PHP application designer." />
<meta name="description" content="Urgent Requirement of Web Designer having knowledge of Div based html, UI and UX design, PHP application, DHTML, HTML 5.0, XHTML, CSS3, XML, OOPs, RESTful, Web Services, Dreamweaver, Photoshop, Illustrator, Flash, Flex, Slicing, Mocks, Web Form, BETA etc." />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="vs20"></div>
<div class="cont980 pageBody">
<div class="vs10"></div>

<h1>Web Designer</h1>
   <p><strong>Responsibilities Include:</strong></p>
  <ul><li>Take ownership to maintain and improve current design</li>
    <li>Create new products web designs by understanding the flows described in wire frames</li>
   <li>Research and keep self updated about new technologies</li>
    <li>Work closely with PHP and Product Management team to deliver world class UIs in time</li></ul>
  <p> <strong>Skills Required:</strong></p>
  <ul>
    <li>Must be able to convert PSD design to div based html</li>
    <li>Quick learner with a good grasp on upcoming technologies</li>
    <li>Good understanding of UI and UX design principles and standards</li>
    <li>Should have worked with PHP application teams in building web applications</li>
    <li>Key Skills required : DHTML, HTML 5.0, XHTML, CSS3, XML, OOPs, RESTful, Web Services, Dreamweaver, Photoshop, Illustrator, Flash, Flex, Slicing, Mocks, Web Form, BETA</li>
    
  </ul>
 
  <p><strong>Good to Have:</strong></p>
  <ul>
    <li> Experience of handling designs for iOS and Android</li>
    <li> Knowledge of E-Commerce and Open source website design like Zen Cart, Word press, osC</li>
      </ul>
  <p>Location: Gurgaon<br />
    Position Type: Full-Time<br />
  
 <p> <h4> To apply please send your resume to <a href="mailto:jobs@dogspot.in">jobs@dogspot.in</a></h4> </p>
  
  <hr />
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
