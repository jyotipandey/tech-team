                                             
<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Visual/Graphic Designer at DogSpot.in</title>
<meta name="keywords" content="Visual/Graphic Designer at DogSpot.in" />
<meta name="description" content="Visual/Graphic Designer at DogSpot.in" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="vs20"></div>
<div class="cont980 pageBody">
<div class="vs10"></div>

<h1>Visual/Graphic Designer</h1>
   <p><strong>Responsibilities Include:</strong></p>
  <ul><li>Understand the product philosophy and create designs and identities </li>
    <li>Estimating the number of projects and their relative importance and prioritise accordingly</li>
   <li>Thinking creatively to produce new ideas and concepts</li>
    <li>Using innovation to redefine a design brief within the constraints of cost and time</li></ul>
<li>Working with a wide range of media, including videos, photography and other computer-aided designs</li></ul>
<li>Use of illustrative skills with sketches</li></ul>
<li>Working on layouts of web pages ready for HTML Designing</li></ul>
<li>Keeping abreast of emerging technologies in new media, particularly design programs such as  Illustrator and Photoshop</li></ul>
<li>Developing interactive user experiences rather just designs</li></ul>
<li>Working as part of a team with marketers, content writers, product manager, web designers and photographers</li></ul>

  <p> <strong>Qualifications</strong></p>
  <ul>
    <li>Good sense of humor and quick learning abilities is a plus  </li>
    <li>Excellent knowledge of Adobe Illustrator and Photoshop</li>
    <li>Experience in developing Uber User Experiences for web based products/apps</li>
    
    <li>Key Skills required : DHTML, HTML 5.0, XHTML, CSS3, XML, OOPs, RESTful, Web Services, Dreamweaver, Photoshop, Illustrator, Flash, Flex, Slicing, Mocks, Web Form, BETA</li>
    
  </ul>
 
  <p><strong>Good to Have:</strong></p>
  <ul>
    <li> Experience of handling designs for iOS and Android</li>
    <li> Knowledge of E-Commerce and Open source website design like Zen Cart, Word press, osC</li>
      </ul>
  <p>Location: Gurgaon<br />
    Position Type: Full-Time<br />
  
 <p> <h4> To apply please send your resume to <a href="mailto:jobs@dogspot.in">jobs@dogspot.in</a></h4> </p>
  
  <hr />
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>