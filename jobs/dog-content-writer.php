<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Writer at DogSpot.in</title>
<meta name="keywords" content="Content Writer, dog content." />
<meta name="description" content="Urgent requirement of Content Writer at DogSpot.in, with excellent written and verbal communication skills." />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="vs20"></div>
<div class="cont980 pageBody">
<div class="vs10"></div>
  
  <h1>Content Writer, Dog Keeping</h1>
   <p><strong>Responsibilities include:</strong></p>
  <p> 1. Enhancing Content Quality with an Innovative & Creative Approach<br />
    2. Building insightful and unique product descriptions for attracting/retaining customers<br />
    3. Developing holistic strategies and adopting various media for pitching content<br />
   4. Copy editing our published content and creating catchy blogs and articles for the webpage<br />
 	</p>
   <p> <strong>Skills Required:</strong></p>
 1.	Excellent written and verbal communication skills<br />
 2.	Basic Knowledge of SEO and Content Development is an added advantage<br />
 3.	Knowledge of MS Office – Word & Excel
  <p><strong>Basic Qualifications:</strong></p>
 1.	Should be a pet lover with understanding of various dog breeds<br />
 2.	Ability to Analyse on-going Consumption trends<br />
 3.	Ability to Market content to attract customers<br />
 <p>Location: Gurgaon<br />
    Position Type: Full-Time<br />
 <p><h4> To apply please send your resume to <a href="mailto:jobs@dogspot.in">jobs@dogspot.in</a></h4></p>
  
  <hr />
 
 
  
  
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
