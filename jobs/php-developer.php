<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP Web Developer at DogSpot.in</title>
<meta name="keywords" content="PHP Web Developer, PHP Developer, MySQL, JavaScript Developer." />
<meta name="description" content="Urgent requirement of PHP Web Developer with 3 years of experience and good knowledge of either jQuery/Prototype, PHP and MySQL." />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="vs20"></div>
<div class="cont980 pageBody">
<div class="vs10"></div>

  
  
  <h1>PHP Web Developer</h1>
   <p><strong>Responsibilities include:</strong></p>
  <ul>
    <li>Write maintainable, unit-tested codes in PHP and Java</li>
    <li> Develop object-oriented models and designing data structures for new content and ecommerce   projects</li>
    <li> Mentor fellow engineers on software design, coding practices and TDD strategies</li>
    <li> Initiate best practices for website development and champion their adoption while working with product managers to estimate and plan projects in an agile development  frame work</li>
  <li> Maintain and monitor Source Control (SVN/GIT)</li></ul>
  <p> <strong>Skills Required:</strong></p>
  <ul>
    <li> PHP, MySQL</li>
    <li>Javascript, Good knowledge of either jQuery/Prototype</li>
    <li>Understanding of: Nginx,Caching (Memcache), Apache Solr, Lucene, Eaccelerator, Mongo DB</li>
  </ul>
   <p><strong>    Preferred Qualification:</strong></p>
  <ul>
    <li> M.Sc (IT)/MCA/ BE/BTech/ M Tech</li>
    <li> 0-3 years of extensive work experience in Core PHP Coding & MySQL specifically on Linux</li>
  </ul>
  <p><strong> Basic Qualifications:</strong></p>
  <ul>
    <li>Hands-on experience in developing custom modules and integrating 3rd party tools</li>
    <li>Good written and verbal communication </li>
    <li>Ability to work in a fast paced environment, both independently and as part of a team</li>
    <li>Ability to work on product design & development right from whiteboard specifications</li>
    <li>Creative ability for identifying & solving challenging user problems through ideation of new products</li>
  </ul>
  <p>Location: Gurgaon<br />
   Position Type: 3<br />
    Position Type: Full-Time<br />
 <p> <h4> To apply please send your resume to <a href="mailto:meghna@dogspot.in">meghna@dogspot.in</a></h4></p>
  
  </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
