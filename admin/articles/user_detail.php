<?

include($DOCUMENT_ROOT."/constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$maxshow = 30;
if(empty($show)){
  $show = 0;
}else{
  $show = $show - 1;
}
$showRecord = $show * $maxshow;
$nextShow = $showRecord + $maxshow;
 if($datepicker ){			
				$from = "$datepicker";
				$to = date('Y-m-d', strtotime(date("Y-m-d", strtotime(trim($datepicker1))) . '+1 day'));
				$searchSql=" AND a.c_date BETWEEN '$from' AND '$to'";
				$toDis = date('Y-m-d', strtotime(date("Y-m-d", strtotime(trim($datepicker1)))));
						} 
			else {	
				$from = date('Y-m-d', strtotime('-30 days'));
				$to = date('Y-m-d',strtotime('+1 day'));
				$toDis = date('Y-m-d');
				$searchSql=" AND a.c_date BETWEEN '$from' AND '$to'";
				}

$sql1="Select date(a.c_date) as date,group_concat(u.userid) as u_email,count(u.userid) as user FROM users as u,articles as a where a.userid=u.userid AND u.userlevel='8'  $searchSql GROUP BY date order by date DESC ";
$qNumberofuser=query_execute("$sql1");
 $max = mysql_num_rows($qNumberofuser);
while($Numberofuser = mysql_fetch_array($qNumberofuser)){	
	$arrayNumuser[$Numberofuser["date"]]=$Numberofuser["user"];
} // end Shop users				
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Daily basis report for content writer</title>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<script src="/jquery/jquery.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="/jquery.tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery.tablesorter/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>



<!--DatePicker code//-->


<script>

$(document).ready(function(){
	$("#ReportTable").tablesorter();
});

$(function() {
  $( "#datepicker" ).datepicker({
    showWeek: true,
    firstDay: 1,
    dateFormat: 'yy-mm-dd'
  });
});

$(function() {
  $( "#datepicker1" ).datepicker({
    showWeek: true,
    firstDay: 1,
    dateFormat: 'yy-mm-dd'
  });
});
</script>

<!--end of date picker code.//-->


</head>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>

<body>
<h1>Content Writing Details</h1>
<form id="formOrderSearch" name="formOrderSearch" method="post" action="">
<table width="100%" cellspacing="0" cellpadding="2" style="text-align:center; border-color:#999; border-width:1px; border-style:solid; background-color:#edeaea">
  <tr>
 <td>
   <p> From:  <input type="datepicker" id="datepicker" name ="datepicker"  value="<?= $from ?>"  >   </p>
    
</td>  
<td>
   <p>  To: <input type="datepicker1"   id="datepicker1"  name ="datepicker1"  value="<?= $to ?>" >  </p>

</td>  </tr>
</table>
<div style="text-align:center; margin-top:5px;"><input type="submit" name="searchOrder" id="searchOrder" value="                    Search                 " /></div>
<div style=" margin-top:5px;"><hr />
Showing results from <strong><?=showdate($from, "d M o");?></strong> to  <strong><?=showdate($toDis, "d M o");?></strong>
<table width="100%" border="1" cellpadding="5" cellspacing="0" id="ReportTable" class="myClass tablesorter">
<thead>
  <tr style="width:100%">
    <th>Date</th>
    <th>Day</th>        
    <th><strong>Total Article</strong></th>
    <th><strong>Publish</strong></th>
    <th><strong>Unpublish</strong></th>
   </tr>
  </thead>
<tbody>
<? 

function getDaysInBetween($from, $to) { 
 $day = 86400; // Day in seconds 
 $format = 'Y-m-d'; // Output format (see PHP date funciton) 
 $startdate = strtotime($from); // Start as time 
 $enddate = strtotime($to); // End as time 
 $numDays = round(($enddate - $startdate) / $day)-1 ; 
 $days = array(); 
 
 for ($d = $numDays; $d>-1 ; $d--) { 
  $days[] = date($format, ($startdate + ($d * $day))); 
      } 
	   
 return $days; // Return days
}
$days = getDaysInBetween($from, $to);
$max=0;
foreach ($days as $val) {  ?>
  <tr style="width:400px">
  <td><?=$val?></td>	
  <td><?=date('l',strtotime($val))?></td>
  <td><? if($arrayNumuser[$val]) {echo $arrayNumuser[$val];} else {echo '0';}?></td>
  <td><? $sql2publish=query_execute_row("SELECT count(*) as publish FROM users as u,articles as a where a.userid=u.userid $searchSql AND u.userlevel='8' AND publish_status='publish' AND a.c_date LIKE '%$val%' ");
  echo $sql2publish['publish']; ?>
  </td>
  <td><? $unpublish=query_execute_row("SELECT count(*) as unpublish FROM users as u,articles as a where a.userid=u.userid $searchSql AND u.userlevel='8' AND publish_status!='publish' AND a.c_date LIKE '%$val%' ");
  echo $unpublish['unpublish'];?></td>
  </tr>
<? } // } ?>
</tbody>
</table>
</div>
</form>
