<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/session-no.php');

$qShow=query_execute("SELECT show_name FROM show_description WHERE show_id='$show_id'");
$rowShow = mysql_fetch_array($qShow);
$show_name=$rowShow["show_name"];

if($del=='del'){
	$qShow=query_execute("DELETE FROM show_dog_prize WHERE show_dog_prize_id='$show_dog_prize_id'");
}

if($savePrize){
	
	$qDog=query_execute("SELECT * FROM show_dog_show WHERE exhibit_num = '$exhibit_num' AND show_id = '$show_id'");
	$rowDog = mysql_fetch_array($qDog);
	$dog_id=$rowDog["dog_id"];
	$breed_id=$rowDog["dog_breed"];
	$group_id=$rowDog["group_id"];
	
	$qIns=query_execute("INSERT INTO show_dog_prize (show_id, prize_id, dog_id, exhibit_num, breed_id, group_id, class_code) VALUES ('$show_id', '$prize_id', '$dog_id', '$exhibit_num', '$breed_id', '$group_id', '$class_code')");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Show Results | DogSpot</title>
<script src="/jquery/jquery-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
	  $("#formShow").validate({
		  
	});
});
</script>
</head>

<body>
<h1><?=$show_name?></h1>
<form id="formShow" name="formShow" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="200"><strong>Exhibit Num</strong></td>
    <td><select name="exhibit_num" id="exhibit_num" class="required">
    <option value="">Select Exhibit Num</option>
      <? 
	  $qExhi=query_execute("SELECT DISTINCT exhibit_num FROM show_dog_show WHERE show_id = '$show_id' ORDER BY exhibit_num ASC");
		while($rowExhi = mysql_fetch_array($qExhi)){
		$dog_id=$rowExhi["dog_id"];
		$exhibit_num=$rowExhi["exhibit_num"];
	  ?>
      <option value="<?=$exhibit_num?>"><?=$exhibit_num?></option>
      <? }?>
    </select></td>
  </tr>
  <tr>
    <td><strong>Prize</strong></td>
    <td><select name="prize_id" id="prize_id" class="required">
    <option value="">Select Prize</option>
      <? 
	  $qprize=query_execute("SELECT prize_id, prize_code, prize_name FROM show_prize ORDER BY prize_code");
		while($rowPrize = mysql_fetch_array($qprize)){
		$prize_id=$rowPrize["prize_id"];
		$prize_code=$rowPrize["prize_code"];
		$prize_name=$rowPrize["prize_name"];
		$aPrize[$prize_id]=$prize_name;
	  ?>
      <option value="<?=$prize_id?>"><?=$prize_code.' - '.$prize_name?></option>
      <? }?>
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="savePrize" id="savePrize" value="     Save     " />
      <input name="show_id" type="hidden" id="show_id" value="<?=$show_id?>" /></td>
  </tr>
</table>

</form>
<h2>Show Results</h2>
<table width="100%" border="1" cellspacing="0" cellpadding="3">
  
<?php
$qresult=query_execute("SELECT prize_id, exhibit_num, dog_id, show_dog_prize_id FROM show_dog_prize WHERE show_id='$show_id' ORDER BY prize_id");
		while($rowResult = mysql_fetch_array($qresult)){
		$prize_id=$rowResult["prize_id"];
		$exhibit_num=$rowResult["exhibit_num"];
		$dog_id=$rowResult["dog_id"];
		$show_dog_prize_id=$rowResult["show_dog_prize_id"];
?> 
<tr> 
    <td width="300"><?=$prize_id.' - '.$aPrize[$prize_id]?></td>
    <td><?
    $qDogd=query_execute("SELECT dog_name, dog_breed_name FROM show_dog WHERE dog_id='$dog_id'");
$rowDogd = mysql_fetch_array($qDogd);
$dog_name=stripslashes($rowDogd["dog_name"]);
$dog_breed_name=$rowDogd["dog_breed_name"];
	echo $exhibit_num.' - '.$dog_name.' - '.$dog_breed_name?></td>
    <td width="50"><a href="?del=del&show_dog_prize_id=<?=$show_dog_prize_id?>&show_id=<?=$show_id?>">delete</a></td>
    </tr>
<?php
		}
?>    
  
</table>
<p>&nbsp;</p>
</body>
</html>