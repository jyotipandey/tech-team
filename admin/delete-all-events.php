<?php
require_once('../session.php');
require_once('../database.php');
require_once('../functions.php');

//Get user level Details
if($sessionLevel != 1){
 header("Location: /");
 ob_end_flush();
 exit(); 
}
//Get user level Details END

 $sitesection = "dog-events";

if(isset($deleteEvents)){
list($end_dd, $end_mm, $end_yy) = split("/", $todate); //split list-------------------
$todate = "$end_yy-$end_mm-$end_dd 00:00:00"; //make end date foe compare...

$Get_All_Events = mysql_query("SELECT event_id FROM events WHERE end_date < '$todate'");
 if(!$Get_All_Events){   die(mysql_error());   }
 $Total_Rows = mysql_num_rows($Get_All_Events);
	
while($rowAllEvents = mysql_fetch_array($Get_All_Events)){
$event_id = $rowAllEvents["event_id"];

//echo"$event_id <br>";

$sql = mysql_query("SELECT photo_album_id, logo FROM events WHERE event_id = '$event_id'");
if(!$sql){   die (mysql_error());   }
$row = mysql_fetch_array($sql);
$photo_album_id = $row["photo_album_id"];
$logo = $row["logo"];

// Remove Busines Logo
if($logo){
 $DelFilePath = "dog-events/images/$logo";
 if(file_exists($DelFilePath)){  unlink($DelFilePath);  }
 $DelFilePath = "dog-events/images/thumb_$logo";
 if(file_exists($DelFilePath)){  unlink($DelFilePath);  }
}

// Remove Busines Logo END

// Get Album images // deleting photos album
$selImage = mysql_query("SELECT image_id, image FROM photos_image WHERE album_id = '$photo_album_id'"); 
 if(!$selImage){   die(sql_error());   }
	
while($rowImg = mysql_fetch_array($selImage)){
$image_id[] = $rowImg["image_id"];
$image = $rowImg["image"];

$DelFilePath = "photos/images/$image";
if(file_exists($DelFilePath)){  unlink($DelFilePath);  }
$DelFilePath = "photos/images/thumb_$image";
if(file_exists($DelFilePath)){  unlink($DelFilePath);  }
}	
// Get Album images

$deletePhotos = mysql_query("DELETE FROM photos_image WHERE album_id = '$photo_album_id'"); 
 if(!$deletePhotos){	die(sql_error());	}
		
// Remove image		
if($image_id){
foreach($image_id as $img_id){		
 $deleteCmt = mysql_query ("DELETE FROM photos_comment WHERE image_id ='$img_id'");
  if(!$deleteCmt){	die(sql_error());	}
}
}// Remove image	

$deleteAlbum = mysql_query ("DELETE FROM photos_album WHERE album_id ='$photo_album_id'");
 if(!$deleteAlbum){	  die(sql_error());	  }
// Delete Whole Album photos

$delete_Cust = mysql_query("DELETE FROM friends_update WHERE section_id = '$event_id' AND section_name = 'event'");	// Delete custom from events_custom table
 if(!$delete_Cust){  die(mysql_error());	}

$delete_Cust = mysql_query("DELETE FROM events_custom WHERE event_id = '$event_id'"); // Delete custom from events_custom table
 if(!$delete_Cust){  die(mysql_error());	}

$delete_Judge = mysql_query("DELETE FROM events_judges WHERE event_id = '$event_id'"); // Delete Judges from events_judges table
 if(!$delete_Judge){   die(mysql_error());  }

$delete_Member = mysql_query("DELETE FROM events_other_member WHERE event_id = '$event_id'"); // Delete Members from events_other_member table
 if(!$delete_Member){	die(mysql_error());	  }

$del_Member_attend = mysql_query("DELETE FROM events_attending WHERE event_id = '$event_id'"); // Delete Members from events_attending table
 if(!$del_Member_attend){	die(mysql_error());	  }

$delete_Review=mysql_query("DELETE FROM event_review WHERE review_id = '$review_id'"); // Delete review from event_review table
if(!$delete_Review){  die(mysql_error());	}
$incNum = mysql_query("UPDATE events SET num_review = '$num_review -1' WHERE event_id = '$event_id'");
	if(!$incNum){	die(mysql_error()); 	}
//downNumCount("events", "num_review", "event_id", $event_id); // Update Number of review Decrese on events Table******************


$delete_Event = mysql_query("DELETE FROM events WHERE event_id = '$event_id'"); // Delete event from events table
 if(!$delete_Event){   die(mysql_error());	}

 }//End while loop...
if($Total_Rows > 0){
 $msg = "<h3> $Total_Rows Event Deleted </h3>";	
 } 

}//End if isset...
// Delete Review---------------------End
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" media="all" href="/jscalendar/skins/aqua/theme.css" title="Aqua" />
<!-- import the calendar script -->
<script type="text/javascript" src="/jscalendar/calendar.js"></script>

<!-- import the language module -->
<script type="text/javascript" src="/jscalendar/lang/calendar-en.js"></script>

<!-- other languages might be available in the lang directory; please check
your distribution archive. -->

<!-- helper script that uses the calendar -->
<script type="text/javascript">

var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  if (oldLink) oldLink.style.fontWeight = 'normal';
  oldLink = link;
  link.style.fontWeight = 'bold';
  return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(0, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
}
</script>
</head>

<body>
<?php include($DOCUMENT_ROOT."/common/top.php"); ?>
 <div id="pagebody">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr> <td colspan="2"> <? echo"$msg"; ?></td></tr>
   <tr>
    <td width="160" valign="top">&nbsp;  </td>
    <td> 
     <form id="formDelete" name="formDelete" method="post" action="">
      <table width="100%" border="0" cellpadding="2" cellspacing="0">
       <tr>
         <td colspan="3"><h1>Delete all Events having event date less than Selected date</h1></td>
         </tr>
       <tr><td> <strong>Select date</strong></td>
           <td><input name="todate" type="text" value="dd/mm/yy" size="10" id="sel4"/><img id="icon3" border="0" onmouseout="this.style.cursor='default'" onmouseover="this.style.cursor='pointer'" src="/images/calendar1.gif" alt=" Zapatec Calendar" style="cursor: default;" onclick="return showCalendar('sel4', '%d/%m/%Y');"/> </td>
           <td align="left"><input type="submit" name="deleteEvents" id="deleteEvents" value="Delete Events" class="searchBut"/></td>
       </tr>
     </table>
    </form>
   </td>
   <td width="180" valign="top">&nbsp; </td>
  </tr>
 </table>

</div>
<?php include($DOCUMENT_ROOT."/common/bottom.php"); ?>

</body>
</html>