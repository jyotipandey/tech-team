<?
//Get user level Details
if($sessionLevel!=1){
Header("Location: /");
ob_end_flush();
exit(); 
}
//Get user level Details END
?>

<table width="600" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#a6b6c6" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
  <tr>
    <td><a href="http://www.dogspot.in/"></a>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="123"><img src="http://www.dogspot.in/images/dogspot-logo-2.jpg" width="123" height="44" /></td>
          <td valign="bottom"><table width="100%" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#FFFFFF" bgcolor="#0171bf"  >
            <tr>
              <td width="83"  align="center"><a href="http://www.dogspot.in/dog-blog/" style="font-size: 12px; color: #FFFFFF; text-decoration:none">Dog Articles</a></td>
              <td width="107" align="center" ><a href="http://www.dogspot.in/puppies/" style="font-size: 12px; color: #FFFFFF; text-decoration:none">Puppies Available</a></td>
              <td width="75" align="center" ><a href="http://www.dogspot.in/dog-events/" style="font-size: 12px; color: #FFFFFF; text-decoration:none">Dog Events</a></td>
              <td width="67" align="center"><a href="http://www.dogspot.in/photos/" style="font-size: 12px; color: #FFFFFF; text-decoration:none">Photos</a></td>
              <td width="100" align="center"><a href="http://www.dogspot.in/videos/" style="font-size: 12px; color: #FFFFFF; text-decoration:none">Videos</a></td>
            </tr>
          </table>
          </td>
        </tr>
      </table></td>
  </tr>
  
  <tr>
    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" >
      
      <tr>
        <td>
          <table width="597">
            <tr>
              <td><table width="598">
                <tr>
                  <td width="97"><img src="http://www.dogspot.in//images/dog_left.jpg" width="97" height="134" /></td>
                  <td width="382" align="left" valign="top"><div align="center" class="style1">
                    <div align="center"><br />
                      <br />
                      <span style="font-weight: bold"><samp style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:ffc742;text-align:center;"><span style=" font-family:Arial, Helvetica, sans-serif; font-size:20px; color:fb403f;"> New</span> <span class="style2">Dog Show </span>Listing by <span style=" font-family:Arial, Helvetica, sans-serif; font-size:20px; color:0069b2;">Dogspot</span><br />
                      Get information about All Dog Show In India.</samp></span></div>
                  </div></td>
                  <td width="103"><img src="http://www.dogspot.in/images/rigt_dog.jpg" width="103" height="130" /></td>
                </tr>
              </table></td>
            </tr>
          </table>
          <table width="606">
            <tr>
              <td width="10" align="left" style="font-family:Arial, Helvetica, sans-serif ; font-size:12px; color:ea702b">&nbsp;</td>
              <td width="584" align="left" style="font-family:Arial, Helvetica, sans-serif ; font-size:14px;  color:ea702b"><strong>Upcoming Dog Events</strong></td>
            </tr>
          </table>
          <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" bordercolor="#D0CEB9" bgcolor="f6f6f6">
          <tr>
            <td><a href="http://www.dogspot.in/dog-events/86th-87th-championship-dog-show/" style="font-family:verdana; font-size:12px;
            color:0168af" ><b>86th  87th Championship Dog Show</b></b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Chennai)<br /> 
              Sunday, 05 Oct, 2008 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/7th-8th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>7th &amp; 8th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Thrissur)<br />
              Saturday, 22 Nov, 2008 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/11th-12th-championship-dog-show/" style="font-family:verdana; font-size:12px;color:0168af"  ><b>11th &amp; 12th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Kottayam)<br />
              Saturday, 29 Nov, 2008</samp></td>
          </tr>
          <tr>
            <td><a href="http://www.dogspot.in/dog-events/16th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>16th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Bangalore)<br />
              Saturday, 29 Nov, 2008</samp> </td>
            <td><a href="http://www.dogspot.in/dog-events/28th-29th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>28th &amp; 29th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Ludhiana)<br />
              Sunday, 30 Nov, 2008 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/32nd-33rd-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>32nd &amp; 33rd Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Palghat)<br />
              Sunday, 14 Dec, 2008 </samp></td>
          </tr>
          <tr>
            <td><a href="http://www.dogspot.in/dog-events/39th-40th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>39th &amp; 40th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Coimbatore)<br /> 
              Sunday, 07 Dec, 2008 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/23rd-24th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>23rd &amp; 24th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Mumbai)<br />
              Sunday, 14 Dec, 2008 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/kennel-indias-5th-6th-fci-internation-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>Kennel Club Of India’s 5th &amp; 6th FCI International Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Gurgaon)<br />
              Saturday, 20 Dec, 2008</samp></td>
          </tr>
          <tr>
            <td><a href="http://www.dogspot.in/dog-events/38th-39th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>38th &amp; 39th Championship Dog show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Dehradun)<br />
              Sunday, 28 Dec, 2008</samp> </td>
            <td><a href="http://www.dogspot.in/dog-events/18th-19th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>18th &amp; 19th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Kolhapur)<br />
              Sunday, 04 Jan, 2009 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/88th-89t-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>88th &amp; 89th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Chennai)<br />
              Saturday, 10 Jan, 2009</samp></td>
          </tr>
          <tr>
            <td><a href="http://www.dogspot.in/dog-events/41st-42nd-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>41st &amp; 42nd Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Jamshedpur)<br />
              Saturday, 10 Jan, 2009 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/146th-147th-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>146th &amp; 147th Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Kolkata)<br />
              Friday, 16 Jan, 2009 </samp></td>
            <td><a href="http://www.dogspot.in/dog-events/21st-22nd-championship-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>21st &amp; 22nd Championship Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Calicut)<br />
              Sunday, 25 Jan, 2009</samp></td>
          </tr>
          <tr>
            <td><a href="http://www.dogspot.in/dog-events/66th-67th-delhi-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>66th &amp; 67th Delhi Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Delhi)<br />
              Saturday, 22 Nov, 2008 </samp> </td>
            <td><a href="http://www.dogspot.in/dog-events/58th-59th-lucknow-dog-show/" style="font-family:verdana; font-size:12px; color:0168af"><b>58th &amp; 59th Lucknow Dog Show</b></a><br /><samp style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
              (Lucknow)<br />
              Sunday, 09 Nov, 2008 </samp></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="60" align="center" valign="middle"> <a href="http://www.dogspot.in/dog-blog/" style="color:#143b66">Dog Blog</a> | <a href="http://www.dogspot.in/qna/" style="color:#143b66">Dog Q &amp; A</a> | <a href="http://www.dogspot.in/dogs/" style="color:#143b66">Dogs Gallery</a> | <a href="http://www.dogspot.in/puppies/" style="color:#143b66">Puppy Available</a> | <a href="http://www.dogspot.in/photos/" style="color:#143b66">Dogs Photos</a> | <a href="http://www.dogspot.in/videos/" style="color:#143b66">Dogs Videos</a> | <a href="http://www.dogspot.in/newsletter/unsubscribe.php?ID=$ID&user_email=$user_email" style="color:#143b66">Unsubscribe</a><br />
    <span style="font-size:10px; color:#666666">©2005-2008 DogSpot. All rights reserved.</span></td>
  </tr>
</table>
