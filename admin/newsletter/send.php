<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Send Email</title>
<script type="text/javascript" src="http://www.dogspot.in/jquery/jquery.min.js"></script>
<script type="text/javascript" src="http://www.dogspot.in/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="http://www.dogspot.in/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	
	  $("#form1").validate({
		  rules: {
			mailSubject: {
				required: true,
				minlength: 2
			},
			mailBody: "required"
		  }
	  });
});	

</script>
</head>
<body>
<form action="send-email.php" method="post" name="form1" target="newsletterFrame" id="form1">
  <table width="100%" border="1" cellpadding="3" cellspacing="0">
    <tr>
      <td width="100"><strong>To:</strong></td>
      <td><select name="select" id="select"  selected="selected">
        <option value="0">Test Users</option>
        <option value="1">All Users</option>
      </select></td>
    </tr>
    <tr>
      <td valign="top"><strong>Subject:</strong></td>
      <td><input name="mailSubject" type="text" id="mailSubject" style="width:98%" value="<? echo $mailSubject;?>"/></td>
    </tr>
    <tr>
      <td valign="top"><strong>Mail Body:</strong></td>
      <td><textarea name="mailBody" id="mailBody" cols="45" rows="5" class="ckeditor"><? echo $mailBody;?></textarea></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="sendnewsletter" id="button" value="Send Newsleter" /></td>
    </tr>
  </table>
</form>
<iframe id="newsletterFrame" name="newsletterFrame" src="#" style="width:600px;border:0px solid #fff;"></iframe> 
</body>
</html>