<?php 
require_once($DOCUMENT_ROOT.'/session-no.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

require_once($DOCUMENT_ROOT.'/facebookconnect/facebooksettings.php');
require_once($DOCUMENT_ROOT.'/facebookconnect/facebookfunctions.php');
require_once($DOCUMENT_ROOT.'/twitterconnect/twitterfunctions.php');

$sitesection = "photos";

//Get Photo album Details...
	$rowAlbDetails = albumDetails($album_id);
	$album_name = $rowAlbDetails["album_name"];
	$albumUser = $rowAlbDetails["userid"];
	$album_nicename = $rowAlbDetails["album_nicename"];
	
    $album_name = stripslashes($album_name);

// If Save 
if($save){

if($cover_img){
// Make all 0
$updateCover = mysql_query("UPDATE photos_image SET cover_img = '0' WHERE album_id = '$album_id'");
	if(!$updateCover){	die(mysql_error());	}
$updateCover = mysql_query("UPDATE photos_image SET cover_img = '1' WHERE image_id = '$cover_img'");
	if(!$updateCover){	die(mysql_error());	}
// make all 0
}
$siteRoot = $DOCUMENT_ROOT;
$imgFolder = "/photos";

// Get aray count
$AllImgCount = count($image_id);
$numImg = $AllImgCount - 1;
for($i=0; $i<=$numImg; $i++){
$image_nicename = createSlug($title[$i]);
$image_nicename = checkSlugall('photos_image', 'image_nicename', $image_nicename);

$file_old = "$siteRoot$imgFolder/images/".$image[$i];
$extension = getExtension($file_old);

$new_image_name=$image_nicename.".".$extension;

$file_new = "$siteRoot$imgFolder/images/$new_image_name";

if(!rename($file_old, $file_new)){
    echo ("Rename failed");
}
$file_old_thum = "$siteRoot$imgFolder/images/thumb_".$image[$i];
$file_new_thum = "$siteRoot$imgFolder/images/thumb_$new_image_name";
if(!rename($file_old_thum, $file_new_thum)){
    echo ("Rename failed");
}

// Update Photo details
if($image_desc[$i] == "Click to add image Description"){
 $image_desc[$i] = "";
}
$title[$i] = addslashes($title[$i]);
$keywords[$i]=createNiceTag($keywords[$i]);
$image_desc[$i] = addslashes($image_desc[$i]);
$album_name = addslashes($album_name);

$updateCap = mysql_query("UPDATE photos_image SET image = '$new_image_name', title = '$title[$i]', image_nicename = '$image_nicename', keywords = '$keywords[$i]', image_desc = '$image_desc[$i]' WHERE image_id = '$image_id[$i]'");
	if(!$updateCap){
		die(mysql_error());
	}else{
		$error = "0";
	}
// update photo details END
} // End array
//Update Friends Feed & facebook && twitter connect...
 $update_text = "added $AllImgCount photo in album $album_name";
//$updateFeed = mysql_query("UPDATE friends_update SET nicename = '$image_nicename', update_text = '$update_text' WHERE section_id = '$image_id[$i]' AND section_name = 'photos' AND userid = '$userid'");

    $i = $i - 1;
	updateFriendsFeed($userid, 'photos', $image_id[$i], $image_nicename, $update_text); // Update Friends Update
	
	// Publish of facebook
	$numImgF=$numImg+1;
	$publink="https://www.dogspot.in/photos/album/$album_nicename/";
	$pubtypmsg="added $numImgF photo in album $album_name on DogSpot.in&picture=https://www.dogspot.in/photos/images/$new_image_name";
	checkfacebooklink($userid, $publink, $pubtypmsg, "f");
	// Publish of facebook END
	
	//publish on twitter...
	$msg = "added $numImgF photo in album $album_name on DogSpot.in";	
	publishontwitter($userid, $msg, $publink, "t");
	//publish on twitter End...
	
//Update Friend Feed  & facebook && twitter connect... END	

// if Delete
if($delete){
foreach($delete as $image_id){

$selectAlb = mysql_query("SELECT image FROM photos_image WHERE image_id = '$image_id'");
 if(!$selectAlb){	die(mysql_error());   }	
$rowAlb = mysql_fetch_array($selectAlb);
$image = $rowAlb["image"];

$DeleteAlb = mysql_query("DELETE FROM photos_image WHERE image_id = '$image_id'"); 
if(!$DeleteAlb){   die(mysql_error());	}
$DelFilePath = "images/$image";
if (file_exists($DelFilePath)) { unlink ($DelFilePath); }
$DelFilePath = "images/thumb_$image";
if (file_exists($DelFilePath)) { unlink ($DelFilePath); }
// Delete Comments
$deleteComnt = mysql_query("DELETE FROM photos_comment WHERE image_id ='$image_id'"); 
if(!$deleteComnt){	die(mysql_error());		}
//Delete
// Update num Photos
downNumCount("photos_album", "num_photos", "album_id", $album_id);
// Update num Photos END	
} // end Foreach
}// if Delete END

if($error == "0"){
  header("Location: /photos/album.php?album_id=$album_id");
}

}
// If Save END.............................................
	
//If all image Edited	
if(!$insertImg_id){
	$sqlquery = "SELECT image_id FROM photos_image WHERE album_id = '$album_id'";
	$result = mysql_query ("$sqlquery"); 
	$resultNumRow = mysql_num_rows($result);
	if(!$result){	die(mysql_error());  }
	 while($row = mysql_fetch_array($result)){
	 	$newimage_id = $row["image_id"];
		$insertImg_id.=$newimage_id."-";
	 }
}// If all image Edited END

$imgID = split("-", $insertImg_id);

$totalImg = count($imgID);
$totalImg = $totalImg - 1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><? echo"$album_name";?> | DogSpot</title>
<meta name="keywords" content="<? echo"$album_name";?> | DogSpot" />
<meta name="description" content="<? echo"$album_desc";?> | DogSpot" />
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />
<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />
<link rel="stylesheet" type="text/css" href="/new/css/business_listing.css" media="all"  />
<script type="text/javascript" src="/new/js/jquery-1.js"></script>
<script src="/new/js/ajax.js" type="text/javascript"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script src="/js/ajax.js" type="text/javascript"></script>
<script type="text/javascript">
function clickclearFiled(thisfield, defaulttext, EleID){
if(thisfield.value == defaulttext){
 thisfield.value = "";
 document.getElementById(EleID).style.color = "black";
}
}
function clickrecallFiled(thisfield, defaulttext, EleID){
if(thisfield.value == ""){
 thisfield.value = defaulttext;
 document.getElementById(EleID).style.color = "#CCCCCC";
}
}

function checkuncheckall(){
	if(document.getElementById('checkuncheck').checked==true){
		for(i=1;i<=<?=$resultNumRow;?>;i++){
 			document.getElementById('delete[]'+i).checked=true;
		}
	}else{
		for(i=1;i<=<?=$resultNumRow;?>;i++){
 			document.getElementById('delete[]'+i).checked=false;
		}
	}
}

</script>

 <?php require_once($DOCUMENT_ROOT.'/new/common/top.php'); ?>
<div class="cont980">
  <div id="pagebody">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      
      <td valign="top">
<div id="inbody">
   <p class="sitenavi"><a href="/">Home</a> &gt; <a href="/photos/">Photos</a> &gt; Edit Photo Ablum</p>
   <h1><? print breakLongWords($album_name, 30, " "); ?> (recently added: <? echo"$totalImg"; ?> photo)</h1>
     <input name="checkuncheck" type="checkbox" id="checkuncheck" onClick="javascript:checkuncheckall()"/> 
     delete all | <a href="<? echo"/photos/album/$album_nicename";?>/">back to album page</a>
     <hr />
<form id="form1" name="form1" method="post" action="editphotos.php">    
   <table width="100%" border="0" cellpadding="5" cellspacing="0">
    <?
	    $i=1;
		foreach($imgID as $image_id){

		 if($image_id){
		  $selectAlb = mysql_query("SELECT  image, cover_img, title, image_nicename, keywords, image_desc FROM photos_image WHERE image_id = '$image_id'");
			if(!$selectAlb){	die(mysql_error());	  }
		   $RowNum = mysql_num_rows($selectAlb);
		   $rowAlb = mysql_fetch_array($selectAlb);
		   	$image = $rowAlb["image"];
			$cover_img = $rowAlb["cover_img"];
			$title = $rowAlb["title"];
			$image_nicename = $rowAlb["image_nicename"];
			$keywords = $rowAlb["keywords"];
			$image_desc = $rowAlb["image_desc"];
			
			  $title = stripslashes($title);
			  $keywords = stripslashes($keywords);
			  $image_desc = stripslashes($image_desc);
		?>
		 <tr>
		  <td width="170" align="center" valign="top" class="bottomLine"><img src="<? echo"/photos/images/thumb_$image";?>" alt="<? echo"$title";?>" border="0" />
		  <input name="image[]" type="hidden" id="image[]" value="<? echo"$image";?>" /></td>
		  <td valign="top" class="bottomLine"><input name="title[]" type="text" id="title[]" value="<? echo"$title";?>" size="50" class="textWrite"/>
		  <input name="image_id[]" type="hidden" id="image_id[]" value="<? echo"$image_id";?>" />
		  <br />
		  <br />
		<strong>
		<? if(!$image_desc){
			$image_desc = "Click to add image Description";
			$descFlag = 1;
		}else{
			$descFlag = 0;
		}
		?>
		<textarea name="image_desc[]" id="image_desc<? echo"$image_id";?>" cols="50" rows="3" <? if($descFlag == 1){ ?>onBlur="clickrecallFiled(this,'<? echo"$image_desc";?>', 'image_desc<? echo"$image_id";?>')" onClick="clickclearFiled(this, '<? echo"$image_desc";?>', 'image_desc<? echo"$image_id";?>')" style="color:#CCCCCC" <? }?>class="textWrite"><? echo"$image_desc";?></textarea>
		</strong><br />
		<br />
		Keywords (saperated with commas: travel india, mountains, sea beaches)<br />
		<input name="keywords[]" type="text" id="keywords[]" value="<? echo"$keywords";?>" size="50" class="textWrite"/>
		<br />
		<br />
		<input name="cover_img" type="radio" id="radio" value="<? echo"$image_id";?>" />
       
		album cover <input name="delete[]<?=$i;?>" type="checkbox" id="delete[]<?=$i;?>" value="<? echo"$image_id";?>" />
        
delete<br /></td>
	  </tr>
	 <? $i++;} } ?>  
     </table> 
     <br />
        <a href="<? echo"/photos/album/$album_nicename";?>/" class="linkButtonBlue">« back to album</a> 
        <input name="save" type="submit" class="searchBut" id="save" value="     Save Changes     " /> 
        <a href="<? echo"/photos/album/$album_nicename";?>/" class="linkButtonBlue">Cancel</a>
        <input name="album_id" type="hidden" id="album_id" value="<? echo"$album_id";?>" />
    </form>
</div> 
        </td>
        
      </tr>
    </table> 
  </div></div>
  <?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>  
