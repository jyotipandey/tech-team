<?php
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

///If creat button press____________end

?>
<link href="/fileupload/css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/fileupload/swfupload/swfupload.js"></script>
<script type="text/javascript" src="/fileupload/js/swfupload.queue.js"></script>
<script type="text/javascript" src="/fileupload/js/fileprogress.js"></script>
<script type="text/javascript" src="/fileupload/js/handlers.js"></script>
<script type="text/javascript">
	var swfu;
	function uploadPhotos(){
		//window.onload = function() {
			var settings = {
				flash_url : "/fileupload/swfupload/swfupload.swf",
				upload_url: "/photos/upload.php",	// Relative to the SWF file
				post_params: {"PHPSESSID" : "<?php echo session_id(); ?>", "album_id" : "<? echo"$album_id";?>", "album_name" : "<? echo"$album_name";?>", "upload_dir" : "<? echo $DOCUMENT_ROOT."/photos/images/";?>"},
				file_size_limit : "100 MB",
				file_types : "*.jpg;*.jpeg;*.gif",
				file_types_description : "Image Files",
				file_upload_limit : 100,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings
				button_image_url: "/images/choose-photos.gif",	// Relative to the Flash file
				button_width: 122,
				button_height: 24,
				button_placeholder_id: "spanButtonPlaceHolder",
				//button_text: '<span class="theFont">Choose photos</span>',
				button_text_style: ".theFont { font-size: 16; }",
				button_text_left_padding: 12,
				button_text_top_padding: 3,
				
				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
			};
	</script>
<div class="main-cont1" style="width: 500px; bottom: 166px; left: 406.5px; display: block;" >
<div style="float:right;"><a  href="#" class="close"><img src="/new/pix/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
</div>
<h3>Create Album</h3>
  <div id="boxbodywhite" class="boxbodywhite"> 
<form action="/photos/editphotos.php" method="get" enctype="multipart/form-data" name="fileUpload" id="fileUpload">
    <p>You can upload multiple JPG, GIF, or PNG files. (Maximum size of 10MB per photo).</p>

   <div class="fieldset flash" id="fsUploadProgress"> <span class="legend">Upload Queue</span> </div>
   <div id="divStatus">0 Files Uploaded</div>
   
  <div>
     <span id="spanButtonPlaceHolder"></span>
     <input id="btnstart" type="button" value="" onclick="swfu.startUpload();" class="submitButton" />                
     <input id="btnCancel" type="button" value="" onclick="swfu.cancelQueue();" disabled="disabled" class="submitButton" />
     <input type="hidden" name="insertImg_id" id="insertImg_id" value="<? echo"$insertImg_id";?>"/>
     <input name="album_id" type="hidden" id="album_id" value="<? echo"$album_id";?>" />
  </div>
</form>
</div>
</div>