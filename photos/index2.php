<?php
require_once($DOCUMENT_ROOT.'/session-no.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

$sitesection = "photos";
$titlepage=$show ;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="canonical" href="https://www.dogspot.in/photos/filter/popular/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/photos/filter/popular/" />
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dogs Puppies Photo Albums | DogSpot | <?=$filter?> <?php if($titlepage>0) {echo " | ".$titlepage;}?> </title>
<meta name="keywords" content="Dogs Photo, Dogs Albums, Puppy Photo, Puppy Albums, dogs, dogs india, dogs world, puppies, dog , <?=$filter?> ,<?php if($titlepage>0) {echo " , ".$titlepage;}?>"  />
<meta name="description" content="Dogs Puppies Photo Albums: Spot for all your Dog's Needs, dogs, dogs india, dogs world , <?=$filter?> ,  puppies, dog : DogSpot <?php if($titlepage>0) {echo " , ".$titlepage;}?>"  />
<? if($section[0]!='dogspot' && $section[0]!='training-products' && (strpos($urldogspot,'/training-products/?shopQuery=') !== false || strpos($urldogspot,'/dogspot/?shopQuery=') !== false)){?>
 <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<? }?>
<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
<link rel="stylesheet" type="text/css" href="/shapopup/shapopup.css" media="all"  />
<link rel="stylesheet" type="text/css" href="/new/css/showpages.css" media="all"  />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<!--<link href="/css/common.css?v=020112" rel="stylesheet" type="text/css" />-->
<script type="text/javascript" src="/new/slide/jquery.jcarousel.pack.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<script src="/js/ajax.js" type="text/javascript"></script>
<!--<script src="/jquery/jquery.min.js" type="text/javascript"></script>
-->
<script language="javascript">
function confirmDelete(delUrl){
  if(confirm("Are you sure you want to delete")){
    document.location = delUrl;
  }
}
</script>

<SCRIPT language="JavaScript">

function submitForms() {
if (document.formAlb.name.value == "") {
	alert ("\n Please give your Album Title.")
	document.formAlb.name.focus();
	return false;
} else if (document.formAlb.desc.value == "") {
	alert ("\n Please give your Album Description.")
	document.formAlb.desc.focus();
	return false;
} else {
	return true;
}
}
</script>
<link href="/new/css/login-style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$(document).ready(function() {
$('a.login-window').click(function() {
// Getting the variable's value from a link
var loginBox = $(this).attr('href');
//Fade in the Popup and add close button
$(loginBox).fadeIn(300);
//Set the center alignment padding + border
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
// Add the mask to body
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);

return false;
});

// When clicking on the button close or the mask layer the popup closed
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});
</script>
<script type="text/javascript">
function mycarousel_initCallback(carousel){
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

$(window).load(function () {
	var val= new Array();
	  $("input[name='coupin']").each(function(){
    	val.push($(this).val());
		}); 
		var x;
		for (x in val){		
		 $('#fp1'+x).jcarousel({
        auto: 0,
        wrap: 'last',
		scroll: 1,
		initCallback: mycarousel_initCallback
    });
		
		}
   
});

</script>
<?php 
require_once($DOCUMENT_ROOT.'/arrays.php');

// Delete Whole Album
if($del == "del"){
// Check valud user

$resultPostId = mysql_query("SELECT userid FROM photos_album WHERE album_id = '$album_id'");
$rowPostid = mysql_fetch_array($resultPostId);
$postUserid = $rowPostid["userid"];
if($userid != 'Guest' && ($userid == $postUserid || $sessionLevel == 1)){
	}else{
	  header("HTTP/1.0 404 Not Found");
		require_once($DOCUMENT_ROOT.'/404.php');
		die(mysql_error());
		exit();
	}
// Check valud user END
	
// Get Album images

$selImage = mysql_query("SELECT image_id, image FROM photos_image WHERE album_id = '$album_id'"); 
   if(!$selImage){	die(sql_error());	}
	
while($rowImg = mysql_fetch_array($selImage)){
$image_id[] = $rowImg["image_id"];
$image = $rowImg["image"];

$DelFilePath = $DOCUMENT_ROOT."/photos/images/$image";
if (file_exists($DelFilePath)){ unlink ($DelFilePath);  }
$DelFilePath = $DOCUMENT_ROOT."/photos/images/thumb_$image";
if (file_exists($DelFilePath)){ unlink ($DelFilePath);  }
//$DelFilePath = "/photos/images/m_$image";
//if (file_exists($DelFilePath)) { unlink ($DelFilePath); }

}	
// Get Album images

$deletePhotos = mysql_query ("DELETE FROM photos_image WHERE album_id = '$album_id'"); 
  if(!$deletePhotos){	die(sql_error());	}
		
// Remove image
if($image_id){
foreach($image_id as $img_id){
  downNumCount("users", "num_photo", "userid", $postUserid); //down num count in user table
  delete_friends_update($img_id, "photos"); //delete friends update
  
  $deleteCmt = mysql_query("DELETE FROM photos_comment WHERE image_id = '$img_id'"); 
	if(!$deleteCmt){	die(sql_error());	}
 }
}// Remove image	

$deleteAlbum = mysql_query("DELETE FROM photos_album WHERE album_id = '$album_id'"); 
	if(!$deleteAlbum){	 die(sql_error());	}
// Update blog Count on users Table
$selectArt = mysql_query("SELECT userid FROM photos_album WHERE album_id = '$album_id'");
   if(!$selectArt){ die(sql_error()); }	
	 $rowArt = mysql_fetch_array($selectArt);
	 $downUser = $rowArt["userid"];
	  downUserNum("users","num_album",$downUser);
// Update blog Count on users Table END 
}// Delete Whole Album
?>
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="cont980" style="margin-top:10px">
                
                    <div class="fl">
                        <p align="left"><a href="/">Home</a> &gt; <a href="/photos/">Photo Albums</a></p>
                     </div>
                 
                     <div class="fr">
                        <p align="right"><a href="https://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a><a href="https://twitter.com/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a></p>
                     </div>
                     <div class="cb"></div>
                 </div>
  <!-- black strip -->
        	<!-- black strip -->
            
 <div class="cont980">
   	 <div class="greyContainer mB20">
<div class="fl" style="width:389px; padding-left:20px; height:130px; border-right:1px dotted #666;">
      <h1>dog shows</h1>
      <div style="margin-bottom:15px;">
       <select style="width:350px" onchange="document.location.href=this.value" >  
       <?php require_once($DOCUMENT_ROOT.'/new/dog-events/dogshows.php'); ?></select>
       </div>         
       <div class="orangeButton"><? if($userid != "Guest"){ ?> <a href="#login-box" class="login-window">create album</a>
       <? }else {?>
       
       <a href="<? echo"/reg-log/index.php"; ?>">create album</a>
       <? }?>
       </div>
</div>

<div class="fl" style="width:520px; padding-left:20px; height:130px;">
      <h2>photos of the week</h2>
      <div style="margin-bottom:15px;">
      <?  showphotoofweek(4);?>  
      
      </div>
</div>
  </div>
     <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <div class="events_land_cont718">
            	
   <div id="login-box" class="login-popup">
					<?php require_once($DOCUMENT_ROOT.'/photos/Ajx-create-album.php'); ?></div>
   <div class="cb"></div>
   <h3><? if($filter=='justin') {echo "just in";}else{echo "popular";} ?></h3>
   
   
   
<?php
//------------Show
$maxshow = 10;
if(empty($show)) {
 $show = 0;
}else{
 $show = $show - 1;
}
$showRecord = $show * $maxshow;
$nextShow = $showRecord + $maxshow;
if($section[0]!='dogspot' && $section[0]!='training-products' && (strpos($urldogspot,'/training-products/?shopQuery=') !== false || strpos($urldogspot,'/dogspot/?shopQuery=') !== false)){
 $showRecord=0;	
}
//-------------Show
if($filter=='popular'){
	
if($member_id){
 $sqlquery = "SELECT * FROM photos_album WHERE userid = '$member_id' AND m_upload = '0' AND num_photos!='0' AND album_link_name='event' AND publish_status='publish' ORDER BY no_views DESC LIMIT $showRecord, $maxshow";
 $sqlqueryall = "SELECT * FROM photos_album WHERE userid = '$member_id' AND m_upload = '0' AND album_link_name='event' ";
}else{
 $sqlquery = "SELECT * FROM photos_album WHERE m_upload = '0' AND num_photos!='0' AND album_link_name='event' AND publish_status='publish' ORDER BY no_views DESC LIMIT $showRecord, $maxshow";
 $sqlqueryall = "SELECT * FROM photos_album WHERE m_upload = '0' AND album_link_name='event' ";
} } 
if($filter=='justin'){
	if($member_id){
 
  $sqlquery = "SELECT * FROM photos_album WHERE userid = '$member_id' AND m_upload = '0' AND num_photos!='0' AND album_link_name='event'  AND publish_status='publish' ORDER BY cdate DESC LIMIT $showRecord, $maxshow";
 $sqlqueryall = "SELECT * FROM photos_album WHERE userid = '$member_id' AND m_upload = '0' AND album_link_name='event' ";
}else{
  $sqlquery = "SELECT * FROM photos_album WHERE m_upload = '0' AND num_photos!='0' AND album_link_name='event'  AND publish_status='publish' ORDER BY cdate DESC LIMIT $showRecord, $maxshow";
 $sqlqueryall = "SELECT * FROM photos_album WHERE m_upload = '0' AND album_link_name='event' ";
} 
} 
$numrecord = mysql_query ("$sqlqueryall");
$totrecord = mysql_num_rows($numrecord);

$result = mysql_query ("$sqlquery"); 
if(!$result){    die(sql_error());   }

$numcols = 4;
$rows = 1;
?>

<!--<table width="100%" border="0">
  <tr>
    <td><h1>Latest Albums</h1></td>
    <td align="right">Viewing <?// echo"$showRecord - $nextShow out of $totrecord";?> Photos Albums</td>
  </tr>
</table>-->
<? $s=0;
  while($row = mysql_fetch_array($result)){
	  $s=$s+1;
	 $album_id = $row["album_id"];
	 $album_name = $row["album_name"];
	 $rateing = $row["rateing"];
	 $albumUser = $row["userid"];
	 $album_nicename = $row["album_nicename"];
	 $cdate = $row["cdate"];
	 
	$album_name = stripslashes($album_name);
    $album_name = breakLongWords($album_name, 20, " ");
    $image = coverAlbumimg($album_id);
$qAlbumcount  = query_execute_row("SELECT count(*) as countimage FROM photos_image WHERE album_id='$album_id'");
if($image){
	$src = $DOCUMENT_ROOT."/photos/images/thumb_$image";
	$destm = $DOCUMENT_ROOT.'/imgthumb/147x122-'.$image; 
	createImgThumbIfnot($src,$destm,'147','122','ratiowh');
}

if($qAlbumcount['countimage']>=8){

if($s==10){ ?>
	<div class="imgtxtcont" style="padding-bottom:0px; margin-bottom:0px; border:none;" id="div_<?=$album_id?>">
	<? } else {
?> 
<div class="imgtxtcont" style="padding-bottom:15px; margin-bottom:15px;" id="div_<?=$album_id?>"> <? }?>
 <div class="imghold" style="width:147px; height:122px;" align="center">
   <a href="<? echo"/photos/album/$album_nicename/"; ?>" class="photoAlbum"><img src="<? echo"/imgthumb/147x122-$image";?>" alt="<? echo"$album_name | DogSpot.in | Dogs India"; ?>" border="0"  /></a>
 </div>
 <div ><? if($albumUser == $userid || $sessionLevel == 1){/* ?>
<div id="editBut" style="width:560px;">
<div class="buttonwrapper" style="width:560px;">
<a href="<? echo"javascript:confirmDelete('?del=del&album_id=$album_id')"; ?>" class="ovalbutton"><span>Delete</span></a> 
<a href="<? echo"javascript:OpenAjaxPostCmd('editAlbum.php','photoEdit$album_id','?album_id=$album_id','Loading..','photoEdit$album_id','2','2')"; ?>" class="ovalbutton"><span>Edit</span></a></div>
</div>
<? */} ?></div>
<div class="txthold" style="width:560px;">
<input name="coupin" type="hidden" id="coupin" value="<?=$album_id?>" />
<div id="<? echo"photoEdit$album_id"; ?>">
<h5><a href="<? echo"/photos/album/$album_nicename/"; ?>"><? echo"$album_name"; ?></a></h5>

</div>

                                <div class="fl">
                                    <p style="color:#757575;">
									<? guestredirect1($userid, dispUname($albumUser),$albumUser) ?><br/>
									<? print(albumPhotos($album_id)); ?> Photos | <? print(showdate($cdate, "d M o")); ?></p>
                                </div>
                                
                                
                                <div class="cb"></div>
                                <div class="vs5"></div>
                                <div class="vs2"></div>
                                    <ul class="dog_gallery shopslide" id="fp1<?=$album_id?>" style="height:70px" >
                                    <?  $qAlbum  = query_execute("SELECT * FROM photos_image WHERE album_id='$album_id' LIMIT 6 "); 
									$i=1;
									 while ($rowItems = mysql_fetch_array($qAlbum)) {
									 $image_nicename = $rowItems["image_nicename"];
									$title=$rowItems["title"];
									$keywords=$rowItems["keywords"];
									$image=$rowItems["image"];	
								if($image){
							$src = $DOCUMENT_ROOT."/photos/images/thumb_$image";
							$destm = $DOCUMENT_ROOT.'/imgthumb/70x70-'.$image; 
							createImgThumbIfnot($src,$destm,'70','70','ratiowh');
						}	
																		?>
                                              
       <li style="width:70px;height:70px;text-align:center;">
       <a href="/photos/<?=$image_nicename?>/" class="db vs160"><img src="<? echo"/imgthumb/70x70-$image";?>" alt="<?=$keywords?>" border="0"  align="middle" title="<?=$title?>" /></a>
</li> <? $i=$i+1; } ?></ul>
                                    
                                   
                                <div class="cb"></div>

</div>
<div class="cb"></div>
</div>
<div class="cb"></div>
<? 
   $rows = $rows + 1;
}
}// End While
?>
<div class="showPages">
<? 
$pageUrl = "/photos/filter/$filter";
showPages($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl);
?>
</div>

</div>
       
       </div>
       <!-- right column -->
         	
            <div class="events_land_cont230">
            	<?php include($DOCUMENT_ROOT.'/new/dog-events/eventrightpanel.php'); ?>

         
            </div><!-- right column -->
            
            <div class="cb"></div> 
  </div>
<div id="clearall"></div>
  <?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?> 