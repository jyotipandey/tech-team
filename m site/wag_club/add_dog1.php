<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";
$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");
$Testtotel = query_execute_row("SELECT count(*) as countrecord FROM dogs_activity WHERE publish_status ='publish' ORDER BY dog_id DESC");
?>

<!DOCTYPE html>
<html>
<head>

<title>Add your Dog | WagClub</title>
<meta name="keywords" content="Add your Dog, WagClub" />
<meta name="description" itemprop="description" content="Add your Dog in WagClub WagClub Is a Pet Social Network, Dog Social Sites" />
<link type="text/css" rel="stylesheet" href="/wag_club/css/dg_style.css?h=12">
<style>
.ui-btn-icon-bottom:after, .ui-btn-icon-left:after, .ui-btn-icon-notext:after, .ui-btn-icon-right:after, .ui-btn-icon-top:after{ content:"" !important;}
.radio1, .radio2{height: 40px !important;}
</style>
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<form>
  <div class="dog-info-form">
    <div class="form-item">
      <input data-role="none"  type="text" name="name" id="name" placeholder="Dog Name">
    </div>
    <div class="form-item"> </div>
    <!--<div class="form-item">
					<select data-role="none"  id="Breed" style="background-color: #fff;">
                            <option value="" default selected>please select Breed</option>
					</select>
					 
				</div>-->
    <div class="form-item">
      <select data-role="none">
        <option value="" default selected> Select Breed</option>
        <? $query=mysql_query("SELECT * FROM dog_breeds ORDER BY breed_name ASC");
		while($row=mysql_fetch_array($query)){?>
		<option value="<?="breed".$row['breed_id'].'@@'.$row['breed_name']?>"><?=$row['breed_name']?></option><?
		}?>
      </select>
    </div>
    <div class="form-item">
      <label>
        <input type="checkbox" value="mix-breed" id="mix-breed">
        I have a mix breed</label>
    </div>
    <div class="form-item gender-opt">
      <div class="radio1">
        <label>BOY
          <input type="radio" value="M" name="gender"/>
        </label>
      </div>
      <div class="radio2">
        <label>GIRL
          <input type="radio" value="F" name="gender"/>
        </label>
      </div>
    </div>
    <div class="form-item">
      <select data-role="none"  id="dates" style="width: 32%; display: inline-block; background-color: #fff;" onclick="myFunction()">
        <option value="" default selected>Dates</option>
      <?
				for($i=1;$i<=31;$i++)
				{
					echo '<option value='.$i.'>'.$i.'</option>';
					}?>   </select>
      <select data-role="none"  id="months" style="width: 32%; display: inline-block; background-color: #fff;">
        <option value="" default selected>Months</option>
        <option value="1">January</option>
				<option value="2">February</option>
				<option value="3">March</option>
				<option value="4">April</option>
				<option value="5">May</option>
				<option value="6">June</option>
				<option value="7">July</option>
				<option value="8">August</option>
				<option value="9">September</option>
				<option value="10">October</option>
				<option value="11">November</option>
				<option value="12">December</option>
      </select>
      <select data-role="none"  id="years" style="width: 32%; display: inline-block; background-color: #fff;">
        <option value="" default selected>Years</option>
        <? $month=date('m');
				
					$year=date('Y');
				
				
				for($i=1989;$i<=$year;$i++)
				{
					echo '<option value='.$i.'>'.$i.'</option>';
					}?>
      </select>
    </div>
    <div class="form-item">
      <select data-role="none">
        <option value="" default selected> Date of Birth</option>
      </select>
    </div>
    <div class="next-btn" id="nexttodescription"><a href="">NEXT</a></div>
    <div class="wow"></div>
  </div>
</form>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
