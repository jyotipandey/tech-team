<?php
/*ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);*/
 
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once($imageBasePath . '/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
require_once($imageBasePath. '/dogs/arraybreed.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";

$user_ip = ipCheck();

$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");
// redirect it with 301 redirect to the page with /
makeProperURL($requestedUrl, $requested);
// redirect it with 301 redirect to the page with /

$dogDet            = dogDetailsNice($dog_nicename);
$dog_owner         = $dogDet['userid'];
$dogstatatus       = $dogDet["publish_status"];
$dog_id            = $dogDet['dog_id'];
$dog_name          = $dogDet['dog_name'];
$city              = $dogDet['city'];
$dog_sex           = $dogDet['dog_sex'];
$city_nicename     = $dogDet['city_nicename'];
$state             = $dogDet['state'];
$country_name      = $dogDet['country_name'];
$dog_image_profile = $dogDet['dog_image'];
$dog_image         = $dogDet['dog_image'];
$dog_name1         = str_replace('"', " ", $dogDet['dog_name']);

$dog_image1       = $dogDet['dog_image1'];
$dog_image2       = $dogDet['dog_image2'];
$dog_image3       = $dogDet['dog_image3'];
$dog_image4       = $dogDet['dog_image4'];
$dog_nicename     = $dogDet['dog_nicename'];
$dog_color        = $dogDet['dog_color'];
$day              = $dogDet['day'];
$month            = $dogDet['month'];
$year             = $dogDet['year'];
$dogs_desc        = $dogDet['dogs_desc'];
$tag              = $dogDet['tag'];
$wag              = $dogDet['wag'];
$publish_status   = $dogDet['publish_status'];
$image_margin_top = $dogDet['image_margin_top'];
$breed_nicename   = $dogDet['breed_nicename'];
$Dogbreed         = $ArrDogBreed[$breed_nicename];

$dogprofile          = $dog_owner;
$dog_name            = stripslashes($dogDet[dog_name]);
$dog_NAME            = breakLongWords($dog_name, 30, " ");
$user_email_transfer = useremail($userid);
//if(($publish_status!='publish' && ($userid == "Guest" || $dog_owner!=$userid || $sessionLevel!= 1))){

//----------------------------------------wag tag code--------------------------------------------------
$sel_transfer = query_execute_row("SELECT * FROM wag_tag_transfer WHERE dog_id='$dog_id' AND status='0'");
if ($sel_transfer['new_owner_email'] != '') {
    $new_owner_email = $sel_transfer['new_owner_email'];
} else {
    $new_owner_email = "0";
}
//--------------------------------------------ends-------------------------------------------------------

if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
    //if($userid=='jyotimmmec'){echo $dog_owner.$sessionLevel;}
} else {
    if ($watch == 'yes' || $publish_status != 'publish') {
        header("Location: https://m.dogspot.in/login.php?refurl=/dogs/" . $dog_nicename);
    }
}
if (!$breed_nicename) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();
}

//--------------------------------------wag tag code-------------------------------------------------------
$active_tag = mysql_query("SELECT * FROM wag_tag WHERE dog_id='$dog_id' AND active_status='1'");
$hastag     = mysql_num_rows($active_tag);
if ($dog_owner == $userid) {
    $wagtagdet   = mysql_query("SELECT * FROM wag_tag WHERE dog_id='$dog_id' AND userid='$userid' AND active_status='1'");
    $countwagtag = mysql_num_rows($wagtagdet);
    $match       = '1';
}
if (!$dog_nicename) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();
}
if ($dog_sex == 'M') {
    $sex = 'his';
} else {
    $sex = 'her';
}
if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
    $summary12 = 'My ' . $dog_NAME . ' got ' . $sex . ' very own social profile on Wagclub. Does your dog have an exclusive social identity?';
} else {
    $summary12 = $dog_NAME . ' got ' . $sex . ' very own social profile on Wagclub. Does your dog have an exclusive social identity?';
}
$title12 = 'Wag Club';
$url12   = urlencode('https://www.dogspot.in/dogs/' . $dog_nicename.'/');

$activities1 = mysql_query("SELECT DISTINCT da.image,da.activity1,da.id,da.user_activity FROM dogs_activity as da,dogs_available as das WHERE da.dog_id='$dog_id' AND da.dog_id=das.dog_id AND das.userid='$dog_owner' AND (da.publish_status='publish' OR das.userid='$userid' ) ORDER BY da.id DESC ");
$activities2 = mysql_query("SELECT DISTINCT da.image,da.activity1,da.id,da.publish_status,da.user_activity FROM dogs_activity as da,dogs_available as das WHERE da.dog_id='$dog_id' AND da.dog_id=das.dog_id AND das.userid='$dog_owner' AND (da.publish_status='publish' OR das.userid='$userid' ) ORDER BY da.id DESC LIMIT 0,3 ");
?>

<!DOCTYPE html>
<html>
<head>

<meta name="robots" content="noindex, nofollow">
<title><? echo "$dogDet[dog_name], $Dogbreed, Dogs in $city, ";
if ($state) { echo "$state, "; }
print(dispUname($dog_owner));
?>|<?=$dog_id?>| DogSpot</title>
<meta name="keywords" content="<?
echo "$dog_name1 ,$dogDet[dog_breed], $dogDet[dog_color] , $city , $state , $country_name , $dog_nicename ";
?>Dogs , Dog Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog: DogSpot" />
<meta name="description" content="<?
echo "$dog_name1 ,$dogDet[dog_breed], $dogDet[dog_color] , $dogDet[dog_name] , $city , $state , $country_name, $dog_nicename";
?> Dogs : Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog , DogSpot" />
  <meta property="fb:app_id" content="1677990482530277" />
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.dogspot.in/dogs/<?= $dog_nicename ?>/" /> 
<meta property="og:title" content="<?= $dogDet['dog_name'] ?>" /> 
<meta property="og:description" content="<?= $summary12 ?>" />

<meta property="og:image" content="<?= "https://www.dogspot.in/dogs/images/" . $dog_image;?>" />
<link rel="canonical" href="https://www.dogspot.in/dogs/<?= $dog_nicename ?>/" />

<link type="text/css" rel="stylesheet" href="/wag_club/css/dg_style.css?h=14">
<script type="text/javascript">
function wagfuncount()
{
var user=$("#user_id").val();
	var dog_id=$("#dog_id").val();
 ShaAjaxJquary("/wag_club/wag_insert.php?user="+user+"&dog_id="+dog_id+"", "#wag", '',  '', 'POST', "", '','REP');
location.reload();
}
function wagcount()
{
var user=$("#user_id").val();
var dog_id=$("#dog_id").val();
ShaAjaxJquary("/wag_club/wag_insert.php?user="+user+"&dog_id="+dog_id+"&update=1", "#wag", '',  '', 'POST', "", '','REP')
location.reload();
 }
</script>
<script>
var cat_nice='';
 $(document).ready(function() { 
 //alert('233');
 var dog_id=$("#dog_id").val();
	var dog_owner=$("#dog_owner").val();
 cat_nice=document.getElementById('txt1').innerHTML;
 var c=0;
 countr=document.getElementById('txt2').innerHTML;
//alert ( $(".imgtxtcontwag:last").attr('id'));
if(countr!='0' && dog_id !='' && dog_owner!=''){
$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	
	if(c!=2){
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/wag_club/loadmore1_test1.php?dog_id="+dog_id+"&dog_owner="+dog_owner+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<a href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore'  >See More </div></a>");
		
	c=0;}$('#loadMoreComments').hide();
	} // c condition close
	}

	});
}
	});
</script>
<script>
function get(rt){
	var dog_id=$("#dog_id").val();
	var dog_owner=$("#dog_owner").val();
	//alert('fdwef');
	$('#divw'+rt).hide();
	c=0;

	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c!=3){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/wag_club/loadmore1_test1.php?dog_id="+dog_id+"&dog_owner="+dog_owner+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
		c=0;}$('#loadMoreComments').hide();
	
	} // c condition close-->
	}
	
	}

</script>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1677990482530277";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style>
.postIcon_wc_new a{ color:#fff; font-size:14px;}
.open_detailBox_dg .ui-collapsible-heading a{width: 100%;
    float: left;
    text-align: left;
    padding: 10px;
    color: #333;
    font-size: 14px;     border: 0px !important;
    margin-bottom: 3px;}
	.open_detailBox_dg .ui-collapsible-content{width: 100%;
    float: left;
    border: 0px;
    border-top: 0px}
	
	.open_detailBox_dg .ui-btn-icon-left:after {
   
    content: "" !important;
}
	.open_detailBox_dg .ui-icon-plus:after{
		-webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
    margin-right: 10px;}
	.open_detailBox_dg .ui-icon-minus:after{-webkit-transform: rotate(90deg);
    -moz-transform: rotate(180deg);
    -ms-transform: rotate(180deg);
    -o-transform: rotate(180deg);
    transform: rotate(180deg);
    margin-right: 10px;
    margin-top: -30px !important;}
h1.ui-collapsible-heading a{    background: rgba(255, 255, 255, 0.9);}
.details_box_dg {
    position: absolute;
	width:100%;
   
}
.open_detailBox_dg {
    position: absolute;
    bottom: 0px;
    text-align: left;
    display: block;
    width:100%;
    border-radius: 2px 2px;
    margin-bottom: 3px;
    float: left;
    padding: 10px 0px;
}
.open_detailBox_dg ul {
    float: left;
    margin: 0px;
    padding: 0px;
	width:100%;
}
.open_detailBox_dg ul li.firstLi_dg.wt_firstLi_dg {
    background: #668000;
    color: #fff;
}
.open_detailBox_dg ul li.firstLi_dg {
    border-radius: 2px 2px 0 0;
}
.open_detailBox_dg ul li {
    float: left;
    width:100%;
    background: rgba(255, 255, 255, 0.9);
    margin-bottom: 3px;
    padding: 5px 20px;
    border-bottom: 0px solid #666;
    list-style: none;
    font-size: 13px;
}
.wt_verifiedTxt {
    position: absolute;
    top: -14px;
    background: #333;
    color: #fff;
    padding: 5px;
    left: 0px;
    border-radius: 3px;
}
.wt_stampPaw {
    float: left;
    position: relative;
    margin: -2px 5px -2px 0px;
}
</style>
</head>
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>

<div style="float:left; width:100%;">
<?
if ($publish_status != 'publish') {
?>
<div class="" style="color: #668000;background: #f7ffd8;float: left;border: 1px solid #668000;text-align: center;padding: 10px 0;width: 99.86%;font-size: 14px;">
Your profile will be visible soon after it is reviewed.</div>
<?
}
?>  
<? $imm = $imageBasePath . "/dogs/images/".$dog_image;

if (!file_exists($imm) || $dog_image == '') {
	$imageofdog="https://www.dogspot.in/wag_club/images/no_imgDog.jpg";
}else
{
$imageofdog="https://www.dogspot.in/dogs/images/".$dog_image;	
}
  ?>
<div class="dog_profileImg_dg" id="shoppro" style=" float:left;width:100%;">

<img src="<?= $imageofdog; ?>" width="1354" style="margin-top:" id="imagepicchange" alt="<?= $dog_name ?>" title="<?= $dog_name ?>">


  <div class="details_box_dg">
<div class="open_detailBox_dg"> 
 <div data-role="none" class="ui-content"  >
          <div data-role="collapsible" data-collapsed="false">
          <h1 class="name_box_dg"><?= ucwords($dog_NAME) ?>
          <span style="display:none"><?=$dog_id?></span> </h1>
<ul>                
<?
if ($hastag == '1') {
?>
<label class="wt_verifiedTxt" style="display:none" id="wag_tag_verified_icon">Wag tag Verified
  <span class="wt_arwDwn"></span>
</label>
<span class="wt_stampPaw" id="ver-tag" style="cursor:pointer;" onmouseover="show_image_verified()" onmouseout="hide_image_verified()"><img src="https://www.dogspot.in/wag_club/images/Verified-Icons.png"></span>
<?
} elseif (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
	?><a href="/wagtagpage.php?dog_id=<?=$dog_id?>"><li>Get your Wag tag</li></a>

<label class="wt_verifiedTxt" style="display:none"id="wag_tag_verified_icon">Wag tag Verified
  <span class="wt_arwDwn"></span>
</label>                    
<span class="wt_stampPaw" id="ver-tag" style="cursor:pointer;display:none" onmouseover="show_image_verified()" onmouseout="hide_image_verified()">
<img src="/wag_club/images/tag-stamp.png"></span>
<?
}?>
 <li><span style="cursor:pointer;" link-href-url="/dogs/breed/<?= $breed_nicename ?>/" target="_blank"><?= ucwords($Dogbreed) ?> Dogs Group</span></li>
 
 <?
if ($day != '0' || $month != '0') {
?>
                   <li id="dog_age"><?
    print(pupDOB1($day, $month, $year));
?></li>
                   <?
}
?>
                   <li> <?
$dogs = $dogDet[dog_sex];
echo "$DogSex[$dogs]";
?></li>
    <li><? if($city_nicename){?><a href="/dogs/citysearch/<?= $city_nicename ?>/"><? }?><?= ucwords($city) ?>
                   <? if($city_nicename){?></a><? }?></li>
                   <?
$wag_dog_brrrd = query_execute_row("SELECT breed_engine FROM dog_breeds WHERE nicename='$breed_nicename'");
if ($wag_dog_brrrd['breed_engine'] == '1') {
?>
                    <li><?= ucwords($Dogbreed) ?></li>
                   <?
}
?>

									   
 

	</ul>
    <form method="post" name="frm" id="frm" action="">

<input type="hidden" name="user_id" id="user_id" value="<?= $userid ?>"/>
<input type="hidden" name="dog_id" id="dog_id" value="<?= $dog_id ?>"/>
<input type="hidden" name="dog_nicename" id="dog_nicename" value="<?= $dog_nicename ?>"/>
<input type="hidden" name="txt1" id="txt1" value="<?=$dog_nicename?>" >
  <input type="hidden" name="txt2" id="txt2" value="<? echo '3';?>" >
</form>
<div id="show" style="display:none"></div>
    </div>
    </div>
    </div>
 </div>
   </div>

<div style="text-align:center;display:inline-block;">
<div id='div-gpt-ad-1552408890928-0' style='height:250px; width:300px;margin: 15px 24px 29px 42px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552408890928-0'); });
</script>
</div>
 </div>

  
  <!-- dog list start-->
  <div class="wag_club_list">
    <form name="wag34" id="wag34" method="post">
      
   <?php require_once($DOCUMENT_ROOT .'/wag_club/activits.php'); ?>
        
        
    </form>
  </div>
  <!-- dog list end--> 
</div>
<form name="wagDog" id="wagDog" method="post">
<div id="wag"></div>
<div data-role="footer" data-position="fixed" class="dog_article_sharefooter ui-footer ui-bar-inherit ui-footer-fixed slideup" role="contentinfo">
    	<div class="ds_social_cont" style="margin-bottom:0px;">
      		<ul class="ds_social_buttons">
    <? $url12   = urlencode('https://www.dogspot.in/dogs/' . $dog_nicename);?>    		
        		<li class="ds_social_list social" style=" background: rgba(0, 0, 0, 0.77);">
                 <a onClick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[url]=<?php
        echo $url12;
?>','sharer','toolbar=0,status=0,width=548,height=325,top=100,left=250');" href="javascript: void(0)">
                <img src="https://m.dogspot.in/contest/img/facebook-icon1.png" width="24" height="24" alt="Share" title="Share">
                
                </a></li>
             <li class="ds_social_list" style=" background: rgba(0, 0, 0, 0.77);"> 
                   <? 
//echo "SELECT * FROM dog_wag WHERE dog_id='$dog_id' AND userid='$userid'";
$wag_r = mysql_query("SELECT * FROM dog_wag WHERE dog_id='$dog_id' AND userid='$userid'");

$wag_dog     = mysql_query("SELECT * FROM dog_wag WHERE dog_id='$dog_id'");
$wag_like    = mysql_num_rows($wag_r);
$wag_count12 = mysql_num_rows($wag_dog);

if($wag_like==0){
	

	if($userid=='Guest'){?>
    <div id="postwagbeforeq<?=$dog_id?>"><a href="/login.php?refUrl=<?=$refUrl?>" rel="nofollow">
<img src="https://m.dogspot.in/contest/img/wag_new-like.png" width="28" height="26" alt="Like" title="Like"></a></div>
<? }else{?>

<div  id="postwagbeforeq<?=$dog_id?>">
<a onclick="wagfuncount();"><img src="https://m.dogspot.in/contest/img/wag_new-like.png" width="28" height="26" alt="Like" title="Like">
<? if($wag_like!='0'){?><span id="acti_wagq<?=$dog_id?>"><?=$wag_count12?></span>
<? }else{?><span id="acti_wagq<?=$dog_id?>"><?=$wag_count12?></span><? }?>
</a>
</div>

<? } }else
{
	?>
	
<div id="postwagafterq<?=$dog_id?>">
<a href="javascript:void(0)" onclick="wagcount();" style="background: #8dc059;"><img src="https://m.dogspot.in/contest/img/wag_new-like.png" width="24" height="24" alt="Unlike" title="Unlike">
<? if($wag_like!='0'){?><span id="acti_wagqq<?=$dog_id?>"><?=$wag_count12?></span>
<? }else{?><span id="acti_wagqq<?=$dog_id?>" ><?=$wag_count12?></span><? }?>
</a></div>
<?
}?>

<div  id="postwagafterq<?=$dog_id?>" style="display:none">
<a onclick="wagcount();"><img src="/contest/img/wag_new-like.png" width="24" height="24" alt="Unlike" title="Unlike">
<? if($wag_like!='0'){?><span id="acti_wagqq<?=$dog_id?>"><?=$wag_count12?></span>
<? }else{?><span id="acti_wagqq<?=$dog_id?>" ><?=$wag_count12?></span><? }?>
</a></div>

<div id="postwagbeforeq<?=$dog_id?>" style="display:none">
<a onclick="wagfuncount();"><img src="/contest/img/wag_new-like.png" width="24" height="24" alt="Like" title="Like">
<? if($wag_like!='0'){?><span id="acti_wagq<?=$dog_id?>"><?=$wag_count12?></span>
<? }else{?><span id="acti_wagq<?=$dog_id?>"><?=$wag_count12?></span><? }?>
</a></div>
<span id="wag<?=$dog_id?>"></span>
                
                </li>
                
                 <? 
if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
?>
               <li class="ds_social_list social" style=" background: rgba(0, 0, 0, 0.77);">
 <a href="/dogs/<?=$dog_nicename?>/Edit/">Edit</a></li>
                <? }?>
      		</ul>
		</div>
  	</div>
    </form>
     <script type="text/javascript">
$(function(){$(document).on("click","span",function(){var n=$(this).attr("link-href-url");n&&(location.href=n)})});
</script>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
