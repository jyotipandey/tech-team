<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
//------------Show


$sitesection = "add-events";
$user_ip = ipCheck();


$sel=query_execute("SELECT * FROM events WHERE event_nice_name = '$event_nice_name'");
 $selget=mysql_fetch_array($sel);
 $date=$selget['start_date'];
 $edate=$selget['end_date'];
 $organized_by=$selget['organized_by'];
 $member_club=$selget['member_club'];
 $member_sec=$selget['member_sec'];
 $map=$selget['map'];
 $event_name=$selget['event_name'];
 $show_id=$selget['link_id'];
 $location=$selget['venue'];
 $event_id=$selget['event_id'];
 $pdf=$selget['pdf'];
 $pointed=$selget['pointed'];
 $online_close_date=$selget['online_close_date'];
 $seljud=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' ORDER BY judge_id DESC");
 $judge=$seljud['judge'];
 $judge_id=$seljud['judge_id'];
 $seljud1=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id' ORDER BY judge_id DESC");
 $judge1=$seljud1['judge'];
 $judge_id1=$seljud1['judge_id'];
 $seljud2=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id1' AND judge_id!='$judge_id' ORDER BY judge_id DESC");
 $judge2=$seljud2['judge'];
 $judge_id2=$seljud2['judge_id'];
 $seljud3=query_execute_row("SELECT judge_id, judge, group_judge FROM events_judges WHERE event_id='$event_id' AND judge_id!='$judge_id2' AND judge_id!='$judge_id' AND judge_id!='$judge_id1' ORDER BY judge_id DESC");
 $judge3=$seljud3['judge'];

 $selk=query_execute_row("SELECT kennel_name, kennel_nicename, contact_detail FROM kennel_club WHERE kennel_id='$organized_by'");
 $kname=$selk['kennel_name'];
 $contact_detail=$selk['contact_detail'];
 $quotes_event_name = str_replace('"', "", $event_name);
?>

<!DOCTYPE html>
<html>
<title>Dog Event Information organized by <?=$kname.", ".$quotes_event_name?> <?=showdate($date, "d M Y")?></title>
<meta name="keywords" content="Dog Event Information, <? echo $kname.", ".$quotes_event_name?> <?=showdate($date, "d M Y")?>" />
<meta name="description" content="Get Dog Event Information organized by <?=$kname.", ".$quotes_event_name?> held on  <? print(showdate($date, "d M Y"));?>" />

<? if($show == '1'){ ?>
<meta name="robots" content="noindex, follow">
<? }?>
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.dogspot.in/<?=$album_nicename?>/" /> 
<meta property="og:title" content="<? echo "$album_name1, ".$tDate.' '; if ($titlepage > 0) { echo " | " . $titlepage;}?>" /> 
<meta property="og:description" content="<? echo "$album_name1 ,".$tDate.' ';?> Photos, You Can See more Dogs Pictures, Dog Photos & Dog Images at DogSpot. DogSpot.in is no 1 Online Dog Lover Portal <?php if ($titlepage > 0) { echo " , " . $titlepage;}?>" />

<meta property="og:image" content="https://www.dogspot.in/new/pix/dog-show-image.jpg" />
<link type="text/css" rel="stylesheet" href="/dog-events/css/dog-events.css?j=13">
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
</html>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<style>
.dog-show-sec{ width:96%; margin:0px 2%;}
tr:nth-of-type(odd){ background:#fff;}
table td{    border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
    padding-top: 10px;}
#dog_schedule_start h1{ margin:10px 0px;}	
</style>
<div class="dogshow-slider-sec">
 <div id="dogshow_banner_holder">
 
      <div class="dog-show-banner"> <img src="/dog-events/images/show-schedules.jpg" alt="Show Schedules" height="" width=""> </div>
<div id="showsechedule_text">
  <h3>Shows Schedules</h3>
</div>
</div></div>

<div class="dog-show-sec">
 <div id="show_results_full_column">
        <div id="dog_schedule_start">
          <h1><? echo $kname."</br>".$event_name ?><? if($pointed=='Y'){ echo "(Pointed)";}?></h1>
        </div>
 </div>
 <div style="text-align:center;display:inline-block;">
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Category-Listing-4 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="7301949999"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
 </div>
<div id="schedule_col_left">
          <table id="tbl_data" border="0" width="100%">
            <tbody><tr>
              <td width="25"><img src="https://www.dogspot.in/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="16" width="16"></td>
              <td width="597"><strong>Show date:</strong> <? print(showdate($date, "d M Y"));?></td>
            </tr>
            <? 
			if($online_close_date!='0000-00-00 00:00:00'){?>
            <tr>
              <td width="25"><img src="https://www.dogspot.in/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="16" width="16"></td>
              <td width="597"><strong>Online entry closing on:</strong> <? print(showdate($online_close_date, "d M Y"));?></td>
            </tr>
            <? }?>



<tr id="">
	<td><img src="https://www.dogspot.in/dog-show/Images/buttons/organized by.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Organized By:</strong><a href="/<?=$selk['kennel_nicename']?>/"> <?=$selk['kennel_name'];?></a></td>
</tr>

<tr id="">
	<td><img src="https://www.dogspot.in/dog-show/Images/buttons/judges.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Judges:</strong> <?=$judge?><? if($judge1!=''){ echo ", ".$judge1;} if($judge2!=''){ echo ", ".$judge2;} if($judge3!=''){ echo ", ".$judge3;} ?></td>
</tr>

<? if($location!=''){ ?>
<tr id="">
	<td><img src="https://www.dogspot.in/dog-show/Images/buttons/venue.png" alt="Globe" height="16" width="16"></td>
	<td><strong>Venue:</strong> <?=stripcslashes(htmlspecialchars_decode($location))?></td>
</tr>
<? }?>
          </tbody></table>
          <font><p></p>
            
          <?=stripcslashes(htmlspecialchars_decode($map));?>	
        </font>
        <div >&nbsp;</div>
        <table id="tbl_data" border="0" width="100%">
        <tr id="">
	<td><img src="https://www.dogspot.in/dog-show/Images/buttons/Contact-icon.jpg" alt="Globe" height="16" width="16"></td>
    
	<td style="width:380px; float:left;"><strong>Contact Detail:</strong><?=stripcslashes(htmlspecialchars_decode($contact_detail))?></td>
</tr>
</table>
        </div>


  </div>
  
</div>
 
  <?php require_once($DOCUMENT_ROOT.'/common/bottom.php');exit; ?>