<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<title>Dog Show | Dog Events | Dog Championship | Kennel Club India</title>
<meta name="keywords" content="Dog Show, Dog Events, Dog Championship, dog show albums, KCI, Kennel Club India events." />
<meta name="description" content="Find the Dog show results, online show entry details, recent articles on dogs as well as old events albums." />
<link type="text/css" rel="stylesheet" href="/dog-events/css/dog-events.css?j=18">
<link rel="canonical" href="https://www.dogspot.in/dog-events/" />
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<script>$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#dogshow_nav_header').offset().top - parseFloat($('#dogshow_nav_header').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#dogshow_nav_header').addClass('fixed');
     } else {
       // otherwise remove it
       $('#dogshow_nav_header').removeClass('fixed');
     }
   });
 }  
});</script>
<script type="text/javascript">
var cat_nice='';
 $(document).ready(function() { 
 //alert('233');
 cat_nice='tt';
 var c=0;
 //alert('j');
 countr=document.getElementById('txt2').innerHTML;
 //alert(2);
//alert ( $(".imgtxtcontwag:last").attr('id'));
$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < 200) {	
	$('#loadMoreComments').show();
	
	$(window).data('ajaxready', false);
	if(c!=2){
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/dog-events/loadmore.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<a href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore' >See More </div></a>");
		
	c=0;}$('#loadMoreComments').hide();
	} // c condition close
	}

	});
	});
</script>
<script>
function get(rt){
	$('#divw'+rt).hide();
	c=0;

	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < 200) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c!=3){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/dog-events/loadmore.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
		c=0;}$('#loadMoreComments').hide();
	
	} // c condition close-->
	}
	
	}

</script>

<style>
.box_list ul li{ padding-top:10px; float:left; width:100%;  font-size:14px;}
.box_list ul li:last-child{ padding-bottom:10px; margin-bottom:10px;}
.box_list{width: 96%;
    float: left;
    margin: 20px 2%;
    border: 1px solid #ecdcc5; margin-bottom:0px;}
	.dog_name a, .dog_date a{ text-decoration:none; color:#333; font-size:12px;}
	.ViewMore{float: left;
    width: 96%;
    text-align: center;
    border: 1px solid #668000;
    color: #668000;
    margin: 0px 2% 20px 2%;
    border-radius: 3px;
    padding: 5px 0px;}
</style>

</head>

<div class="dog-show-sec">
  <div class="dog-show-banner"> <img src="/dog-events/images/dog-show-banner.jpg" title="Dog Show Banner" alt="Dog Show Banner" height="" width=""> </div>
  <div class="dogshow_nav_header" id="dogshow_nav_header">
    <div id="wrapper" class="clearfix">
      <div id="ds_top_nav">
        <ul id="nav">
          <li><a href="/dog-events/about-us/"  data-ajax="false">About us</a> </li>
          <li><a href="/dog-events/show-schedules/" data-ajax="false">Show schedules</a></li>
          <li><a href="/show-results/"  data-ajax="false">Show results</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="box_list">
  <div class="box_show_title">Show Results</div>
  <ul style="margin:0px 2%;">
    <?
$selectShow = mysql_query("SELECT * FROM show_description WHERE show_id != '2' AND show_id != '123' AND show_id != '124' ORDER BY date DESC LIMIT 55");
 if(!$selectShow){	die(mysql_error());	}
	$dit=0;
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT * FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 
	 if($ring_id){ 
	 	$dit=$dit+1;
?>
    <li>
      <? if($ring_id ){?>
      <a id="" href="/<?=$show_nicename?>/" style="color:#162E44;" title="<?=$show_name?> Dog Show Results" data-ajax='false'>
      <?=$show_name?>
      , <span class="date"><? print(showdate($date, "d M Y")); ?></span></a>
      <? }?>
    </li>
    <? }if($dit==5){break;}}?>
   <li> <a href="/show-results/" class="view-more" data-ajax="false">view more &raquo;</a></li>
  </ul>
</div>
<div style="text-align:center;display:inline-block;">
<div id='div-gpt-ad-1552408890928-0' style='height:250px; width:300px;margin: 15px 24px 29px 12px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552408890928-0'); });
</script>
</div>
 </div>
<div id="box_news" >
  <?php /*?>#BeginLibraryItem "/Library/recentnews.lbi"<?php */?>
  <!--<div class="box_show_title">Recent Articles</div>-->
  <?php /*?><marquee id="box_news" direction="up" onmouseover="this.stop();" onmouseout="this.start();"><?php */?>
  <?
						$selectMC = mysql_query("SELECT * FROM articles WHERE (art_body like '%dog shows%' OR art_subject like '%dog shows%') AND publish_status='publish' ORDER BY c_date DESC limit 2");
						
					  if(!$selectMC){	die(mysql_error());	} ?>
  <!-- content -->
  <? $i=0;
							  while($rowArt = mysql_fetch_array($selectMC)){
								$i=$i+1;  
								$articlecat_id = $rowArt["articlecat_id"];
								$article_id = $rowArt["article_id"];
								$art_subject = $rowArt["art_subject"];
								$art_body = $rowArt["art_body"];
								$c_date = $rowArt["c_date"];
								$artuser = $rowArt["userid"];
								$art_name = $rowArt["art_name"];
								  
						$art_subject = stripslashes($art_subject);
						$art_subject = breakLongWords($art_subject, 12, " ");
					
					// Get Post Teaser
						$art_body = stripslashes($art_body);
						$art_body = strip_tags($art_body);
						$art_body = trim($art_body);
						$art_body = substr($art_body,0,100);
					
						$art_body = stripslashes($art_body);
						$art_body = breakLongWords($art_body, 30, " ");
					 
					  ?>
  <div class="dog_show_recentNews">
    <p class="recent_news_title"><a href="<? echo"/$art_name/";?>"><? echo"$art_subject"; ?></a></p>
    <p><? echo"$art_body...";?><? echo " <a href='/$art_name/' class='recent_news_readmore'> Read More</a>"; ?></p>
    <?php /*?>    <br />
          <span style="color:#666; font-style:italic;"><? if(($artuser == $userid || $sessionLevel == 1 ) && $userid != "Guest"){ echo"<a   href='/new/articles/articles_new.php?article_id=$article_id'>Edit</a>";?> | <a href="<? echo"javascript:confirmDelete('/new/articles/articles.php?article_id=$article_id&del=del&reDirURL=$refUrl')"; ?>" style="text-decoration:none;">Delete</a> | <? } ?> Posted by :<a href="/profile/<? echo"$artuser"; ?>/" style="color:#a33b00;"> <? print getTeaser(dispUname($artuser),10); ?> </a>| <? print(showdate($c_date, "l, d M, o")); ?></span></p><?php */?>
  </div>
  <!-- class="imgtxtcont"  above div-->
  
  <? } ?>
  <!--<div class="recent_news_viewmore"><a href="/dog-blog/events/" class="view-more">view more &raquo;</a></div>-->
  <?php /*?></marquee><?php */?>
  <!-- #EndLibraryItem --> </div>
<div  id="box_show" class="margin_left">
  <?php /*?>#BeginLibraryItem "/Library/showentry.lbi" <?php */?>
  <div id="box_show">
    <!--<div class="box_show_title">Show entry</div>
    <a href="/new/dog-events/enter-online.php" class="dogshow_enterbtn">Enter <br />
    on-line</a> </div>-->
  <!-- #EndLibraryItem --> </div>
<? 
				$surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				include($DOCUMENT_ROOT."/Adoption1/social1_article.php"); 
			?>
<div class="recent_show_albem">
  <h2>Recent Albums </h2>
</div>
<!-- recent dog shows start-->
<?  $iid=0;?>
<div class="rytPost_list" id="rytPost_list">
  <div class="dog-show-list imgtxtcontwag" id='<?=$iid?>'>
    <?php
$qItem1=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name, album_link_id FROM photos_album
WHERE  album_link_name ='event' order by cdate desc limit 0,6");
$qItem1all=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name FROM photos_album
WHERE  album_link_name ='event' order by cdate desc");
$totrecord = mysql_num_rows($qItem1all);
$i=0;
		$div_count='0';
while($rowItem = mysql_fetch_array($qItem1)){
				$iid++;
		$div_count++;
	$album_id=$rowItem["album_id"];
	$album_link_name=$rowItem["album_link_name"];
	$i=$i+1;
	$album_name=$rowItem["album_name"];
	$nice_name=$rowItem["album_nicename"];
	
	$qdataM12=query_execute_row("SELECT cover_img FROM photos_image where album_id='$album_id' AND cover_img =  '1'");
	//$cover_image=$qdataM12['cover_img'];
	if($qdataM12) {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id' AND cover_img='1'");
	//echo "SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where    album_id='$album_id'";
	}
	else {
	//	echo "SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where    album_id='$album_id' AND cover_img='1'";
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id'");	
	}
	$sqldatsh=query_execute_row("SELECT date FROM show_description WHERE show_id='".$rowItem['album_link_id']."'");
		$image=$qdataM["image"];
		$imagenice_name=$qdataM["image_nicename"];
		$title=$qdataM["title"];
		if($sqldatsh['date']!=''){ $date=showdate($sqldatsh["date"], "d M o"); }else{ $date=showdate($qdataM["cdate"], "d M o");}
	if($image){
	$src = $imageBasePath.'/photos/images/'.$image;
	$destm = $imageBasePath.'/imgthumb/200x134-'.$image;
	//echo $src;
	
	createImgThumbIfnot($src,$destm,'200','134','ratiowh');

 ?>
    <div class="dogshows_wrapperBox">
      <div class="dog_shows_thumbnail"> <a href="https://m.dogspot.in/photos/album/<?=$nice_name?>/" target="_blank" data-ajax="false"> <img src="https://www.dogspot.in/imgthumb/200x134-<?=$image?>" alt="<?=$title?>" title="<?=$title?>" height="134" width="200" align="middle"></a> </div>
      <div class="dog_shows_title">
        <p class="dog_name"><a href="https://m.dogspot.in/photos/album/<?=$nice_name?>/" data-ajax="false"  target="_blank"><? echo snippetwop($album_name, 26, '');
?></a></p>
        <?
 $qcount= query_execute_row("SELECT count(*) as image from photos_image where album_id='$album_id'");?>
        <p class="dog_date"><? echo $qcount["image"]." "."Photos";?> |<?=$date?></p>
      </div>
    </div>
    <? if($div_count=='2' && $iid !='6'){ 
			$div_count='0';
			?>
  </div><? if($iid==4) {?>
  <div style="text-align:center;display:inline-block;">
 <div id='div-gpt-ad-1552391467223-0' style='height:50px; width:320px;margin: 24px 24px 29px 22px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552391467223-0'); });
</script>
</div></div><? }?> 
  <div class="dog-show-list imgtxtcontwag" id='<?=$iid?>'>
<? }}}?>
  </div>
  <input type="hidden" name="txt2" id="txt2" value="<?=$totrecord?>">
  <div id='loadMoreComments' style='display:none'><img src="https://www.dogspot.in/new/pix/loading2.gif" /></div>
  <!-- recent dog show end--> 
</div>
<?php require_once($DOCUMENT_ROOT .'/common/bottom-new.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
<!-- cart page start--> 

