<?php
    /*ini_set('memory_limit','512M');*/
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
//print_r($section);
	if($section[1] == 'amp'){
		require_once("constants.php");
	}else{
		require_once("../constants.php");
	}
	require_once(SITEMAIN_URL."/database.php");
	require_once(SITEMAIN_URL."/functions.php");
	require_once(SITEMAIN_URL."/shop/functions.php");
	require_once(SITEMAIN_URL."/functions2.php");
	require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
	require_once(SITEMAIN_URL.'/session.php');
	
	$sitesection='adoption';
//echo 'jyoti'.$section[1];
//exit;
if ($section[1]=='amp')
		{
		//$article_id = $rowarticle['ID'];
		include 'amp/index-adoption.php';

		exit();
		}
?>
<!DOCTYPE html>
<html>
<head>
<title>Pet Adoption | Dog Adoption | Free Adoption in India | DogSpot.in   </title>
<meta name="keywords" content="Pets Adoption India, Search Free Pets for Adoption & Adopt a Pet " />
<meta name="description" content="Pet Adoption India an initiative by DogSpot.in - find pets for free adoption rescued by animal shelters and pet lovers in India. Adopt a Pet and show that you care " />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="adoption" />
<meta property="og:url" content="https://www.dogspot.in/adoption/" /> 
<meta property="og:title" content="Pet Adoption India, Search Free Pets for Adoption & Adopt a Pet" /> 
<meta property="og:description" content="Pet Adoption India an initiative by DogSpot.in - find pets for free adoption rescued by animal shelters and pet lovers in India. Adopt a Pet and show that you care" />
<link rel="canonical" href="https://www.dogspot.in/adoption/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/adoption/" />
<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>


    <?php require_once($DOCUMENT_ROOT .'/common/header.php'); ?>
     <script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script>

var adslot0;

 googletag.cmd.push(function() {

   // Declare any slots initially present on the page
   adslot0 = googletag.defineSlot('/21630298032/Mobile', [300, 250], 'div-gpt-ad-1554034021305-0').
       setTargeting("test","infinitescroll").
       addService(googletag.pubads());

   // Infinite scroll requires SRA
   googletag.pubads().enableSingleRequest();

   // Disable initial load, we will use refresh() to fetch ads.
   // Calling this function means that display() calls just
   // register the slot as ready, but do not fetch ads for it.
 //  googletag.pubads().disableInitialLoad();

   // Enable services
   googletag.enableServices();
 //   googletag.pubads().refresh([adslot0]);
 });

 // Function to generate unique names for slots
 var nextSlotId = 1;
 function generateNextSlotName() {
   var id = nextSlotId++;
   return 'adslot' + id;
 }

 // Function to add content to page, mimics real infinite scroll
 // but keeps it much simpler from a code perspective.
 function moreContent(iid) {
  // ids=ids+1;
   // Generate next slot name
   var slotName = generateNextSlotName();

   // Create a div for the slot
   var slotDiv = document.createElement('div');
   slotDiv.id = slotName; // Id must be the same as slotName
  // ('.dfpAd:last').append(slotDiv);
  console.log(slotDiv);
  // console.log($('.dfpAd'+ids+'').last().append(slotDiv));
  
 // var ty=$(".dfpAd:last").attr('id');
   //console.log(ty);
  $(".dfpAd"+iid+"").append(slotDiv);
 //sssss  $(".dfpAd").last().append(slotDiv);
   // Create and add fake content 1
  <?php /*?> var h1=document.createElement("H2")
   var text1=document.createTextNode("Dynamic Fake Content 1");
   h1.appendChild(text1);
   document.body.appendChild(h1);

   // Create and add fake content 2
   var h2=document.createElement("H2")
   var text2=document.createTextNode("Dynamic Fake Content 2");
   h2.appendChild(text2);
   document.body.appendChild(h2);
<?php */?>
   // Define the slot itself, call display() to 
   // register the div and refresh() to fetch ad.
   googletag.cmd.push(function() {
     var slot = googletag.defineSlot('/21630298032/Mobile', [300, 250], slotName).
         setTargeting("test","infinitescroll").
         addService(googletag.pubads());

     // Display has to be called before
     // refresh and after the slot div is in the page.
     googletag.display(slotName);
     googletag.pubads().refresh([slot]);
   });
 }
</script>
	<style>
#google_pedestal_container{display:none;}.sticky-bar,.sticky-height{height:30px}.sale-text,.start-countdown{display:inline-block}.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333}.sticky-bar a,.sticky-bar a:hover{text-decoration:underline}#header div,#morepopup li a,.adoption_ok,.sale-text a,.ui-link,ins{text-decoration:none}.sticky-left{width:96%;float:left}.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px}.sale-text{font-size:12px}.sale-text a{color:#fc3}.ds-home-link{padding-top:30px!important}.adoption_blk h1,.adoption_ok{text-transform:uppercase}body,button,input,select{font-size:1em;line-height:1.3;font-family:lato,sans-serif}.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em;-webkit-background-clip:padding;background-clip:padding-box}.ui-body-a{border-width:1px;border-style:solid;background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3}.ui-content,.ui-mobile a img{border-width:0}.ui-mobile body{height:99.9%}.ui-page{padding:0;margin:0;outline:0}.ui-mobile-viewport{margin:0;overflow-x:visible;-webkit-text-size-adjust:100%;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent}body.ui-mobile-viewport{overflow-x:hidden}.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0}.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden}@media screen and (orientation:portrait){.ui-mobile .ui-page{min-height:420px}}@media screen and (orientation:landscape){.ui-mobile .ui-page{min-height:300px}}.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0}.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em}.ui-content{overflow-x:hidden}.ui-screen-hidden{display:none!important}.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%}.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099}.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important}.ui-popup{position:relative;padding:10px 10px 0}.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff}.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden}.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px)}.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block}#header,#morepopup li:last-child{border-bottom:0}.ui-panel-page-container{overflow-x:visible}.ui-panel-wrapper{overflow-x:hidden;position:relative;border:0;z-index:999}.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none}.panelicon,.purchase-product-cart{position:relative}.ui-panel-dismiss-open{display:block}.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease}.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0)}.ui-panel-position-left{left:-17em}.ui-panel-open.ui-panel-position-left{left:-1px}.ui-panel-page-content-position-left{left:17em;right:-17em}.ui-panel-animate.ui-panel-page-content-position-left{left:0;right:0;-webkit-transform:translate3d(17em,0,0);-moz-transform:translate3d(17em,0,0);transform:translate3d(17em,0,0)}.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em}#header{text-align:left;height:50px;padding:12px;border-top:0;background:#6c9d06}#header .fa{font-size:18px;color:#fff}#header div,.panelicon{display:inline-block}#header div{color:#f7422d}.panelicon{height:19px;left:0;margin-top:0!important;top:0;width:18px}.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px}.ui-link{color:#333}.green_clr{font-family:gobold;margin-left:8px;color:#fff!important;font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335}#morepopup li,#morepopup li a{color:#323232}#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px}#morepopup li img{margin-right:8px;vertical-align:top}#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px}#morepopup-screen{background:0 0}.content-search{padding-bottom:20px}*,.art-content{padding:0}.content-search form{margin:0 auto;position:relative;top:5px;width:95%}.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%}*{margin:0;font-family:lato,sans-serif;box-sizing:border-box}.adoption_ok,ul.adoption_sec{display:inline-block;width:100%}.adoption_blk{width:100%;text-align:center}.adoption_header{background:#f2f2f2;padding:10px 15px;margin:5px 5px 25px;border-radius:5px}.adoption_blk h1,.adp_m_b{margin-bottom:10px}.adoption_blk h1{text-align:center;width:100%;color:#000;font-size:16px;opacity:.6;margin-top:15px}.dog_name{padding-bottom:7px;margin-top:7px;border-bottom:1px solid #ddd;margin-bottom:7px;font-size:25px;color:#8dc059}.adoption_div{padding-bottom:5px;margin-top:5px;margin-bottom:5px;font-size:14px}ul.adoption_sec{list-style:none;padding:0;margin-bottom:25px}ul.adoption_sec li{margin:auto;text-align:center;float:left;width:47.5%;overflow:hidden;border-radius:5px;border-top:1px solid #ddd;box-shadow:1px 4px 5px #aaa}ul.adoption_sec li:last-child{margin-left:5%}ul.adoption_sec li img{max-width:100%;width:auto;height:160px}.ui-content{clear:both;overflow:hidden}.cartsearch_blk a img{width:18px;height:19px}.content-search,.ui-content,.ui-panel-wrapper{width:100%;float:left}.content-search{margin-top:0!important;padding:5px 0;background:#6c9d06}.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0}#header img{vertical-align:middle}#defaultpanel4{z-index:99999}div.dsdropdown{position:relative}.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px}.m-sprite{background:url(https://m.dogspot.in/images/msprite.png) no-repeat;margin:auto}.m-user{float:left;background-position:-166px -40px}.m-orders{float:left;background-position:-33px -72px}.m-wishlist{float:left;background-position:-6px -72px}.m-logout{float:left;background-position:-78px -40px}.m-setting{float:left;background-position:-107px -40px}.m-contact-us{float:left;background-position:-139px -40px}ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc}ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc}ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%}ul.dsdropdown-menu{display:none}ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important}ul.dsdropdown-menu li:last-child{font-size:16px;border:0}ul.dsdropdown-menu li:hover{background:#f9f9f9}.nf-img{float:left;width:20%}.nf-text{float:left;width:80%}.nf-no{position:absolute;width:17px;border-radius:17px;font-size:11px;height:17px;line-height:16px;background:red;color:#fff!important;border:1px solid red;top:-8px;right:-7px;text-align:center}.nf-img img{width:50px!important;height:auto!important}.nf-text h4{font-size:14px;padding-bottom:1px;color:#333}.nf-text p{color:#888;font-size:13px}.nf-active-bg{background:#f2f2f2}.cat-section{float:left;width:100%;letter-spacing:.7px;margin-top:10px}.cat-section .cat-gallery{overflow:scroll;overflow-y:hidden;white-space:nowrap;width:100%;border-bottom:1px solid #f2f2f2;padding-bottom:20px}.cat-section .cat-gallery a{font-size:12.3px;font-weight:700;letter-spacing:0;color:#555;text-decoration:none}.cat-section .cat-gallery div{margin-top:2px}.cat-section .cat-gallery ul{list-style:none;padding:0;margin:0}.cat-section .cat-gallery ul li{width:120px;text-align:center;border-left:1px solid #f4f4f4;position:relative;display:inline-block;align-self:flex-start;border-radius:0;background:#fff;cursor:pointer;margin-left:10px}.cat-gallery::-webkit-scrollbar{display:none}.cat-section .cat-gallery ul li:first-child{border:0}a,body,div,em,form,h1,h4,i,iframe,img,ins,li,p,span,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline}#defaultpanel4,.whitewrapper{background-color:#fff}ul{list-style-type:none}:focus{outline:0}.cartsearch_blk a{margin-left:12px}.cartsearch_blk a:first-child{margin-left:0}#defaultpanel4{border-right:1px solid #ccc}.breadcrumb{overflow:hidden;padding:8px;margin-bottom:0; font-size:12px;color:#555;}.breadcrumb a{color:#4b3a15;text-decoration:none;}.ds-nav-click a{text-decoration:none}.ui-link img{vertical-align:middle}.add-sans-sapce{float:left;width:100%;text-align:center;margin:auto;max-height:120px}.add-sans-sapce .add-banner{margin:auto}.panel div a{color:#333}.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none}.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4}.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4}.accordion:hover,.active{background-color:#f8f8f8}.panel{padding:0;display:none;background-color:#fff;overflow:hidden}.panel div{padding:10px 25px}.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px}.accordion.active:after{content:"\2212"}.ds-home-link{background:#6c9d06;padding:8px 10px}.ds-home-link a{color:#fff}.ds-nav-click{padding:12px}.ds-nav-click a{color:#333}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-bars:before{content:"\f0c9"}.fa-bell-o:before{content:"\f0a2"}.fa-shopping-cart:before{content:"\f07a"}.fa-user-o:before{content:"\f2c0"}.fa-home:before{content:"\f015"}.fa-search:before{content:"\f002"}.adp_m_b select{width:100%;border:1px solid #ddd;border-radius:3px;height:35px}.adoption_ok{height:auto;padding:8px 0;border-radius:3px;border:0;background:#ccc;color:#555;cursor:pointer}body{margin:0;font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height:1.5}@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(https://m.dogspot.in/css/font-awesome/fonts/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal}
	</style>
 </head>

<?php /*?><div class="adp-header-banner"><a href="http://bit.ly/2jUs6O7" onClick="ga('send','event','Christmas Contest','Click','mobile')">
    <img alt="Christmas Contest" title="Christmas Contest" src="https://m.dogspot.in/adoption/christmas-contest-banner.jpg" onload="ga('send', 'event','Christmas Contest','Impression','mobile',{'nonInteraction': 1})" ></a>
 
    <div class="banner-bottom-text">*T&C: Winner will be announced on 25th December at 6:00pm by email.</div>
</div><?php */?>
<?php require_once($DOCUMENT_ROOT .'/category-common.php'); ?>
    <div data-role="content" class="art-content">
    <div class="breadcrumb">
<div itemscope itemtype="http://schema.org/Breadcrumb"> 
<div itemscope itemtype="http://schema.org/BreadcrumbList">
<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<a href="/" itemprop="item"  data-ajax="false" ><span itemprop="name">Home</span></a>
<meta itemprop="position" content="1" /> </span>
<span> / </span>       
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <a href="/adoption/" itemprop="item"  data-ajax="false" ><span itemprop="name">Pet Adoption</span></a>
       <meta itemprop="position" content="2" /> </span>           
             
</div>
</div>
</div>


<?php /*?><div class="mobile-add-banner">
 <a href="/petspot-chicken-sandwich-bits-70-gm/" data-ajax="false"  onClick="ga('send','event','Adoption Home','Click','Adoption Home')">
 <img  src="https://www.dogspot.in/allbanners/5a0ecdcc923f8.jpg" onload="ga('send', 'event','Adoption Home','Impression','Adoption Home')">
 </a>  
</div><?php */?>

<?php /*?><? echo addBannerProductM($_SERVER['REQUEST_URI']); ?><?php */?>

    <div class="adoption_blk" style="float:left; width:100%;" >
    

   <?php /*?><div class="widget-pup-new Label banner-card">
                 
      <? $getItems=query_execute("SELECT si.name,si.nice_name,si.item_id,si.price,si.selling_price,si.item_parent_id FROM shop_items as si WHERE banner_type='yes' ORDER By rand() LIMIT 3 ");
				 while($fetchaffiliate=mysql_fetch_array($getItems)){
			 $name=$fetchaffiliate['name'];
			 $item_id=$fetchaffiliate['item_id'];
			  $item_parent_id=$fetchaffiliate['item_parent_id'];
			   $nice_name=$fetchaffiliate['nice_name'];
			    $price=$fetchaffiliate['price'];
			   $mrp_price=$fetchaffiliate['selling_price'];
		 $rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
		 if(!$rowdatM['media_file'])
		 {
		$rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
	     }
	 if($rowdatM["media_file"]){
				$imglink='/imgthumb/197x197-'.$rowdatM["media_file"];
				$src=$DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
				}else{
				$imglink='/imgthumb/197x197-no-photo.jpg';
				$src=$DOCUMENT_ROOT.'/imgthumb/no-photo.jpg';
				}
				$dest = $DOCUMENT_ROOT.$imglink;	
				createImgThumbIfnot($src,$dest,'197','197','ratiowh');
//$compressed = compress_image($dest,$dest, 80);
		 ?>
                       <div class="banner-wrap"> 
        	<a data-ajax="false" href="/<?=$nice_name?>/?UTM=bannerrepublic">
                <div class="banner-img"> <img src="https://www.dogspot.in<?=$imglink?>" width="100" height="93" style="margin-top:0px;"> </div>
                <div class="banner-text">
                    <div class="gurgaon_offers_pn"><?=$name?></div>
                                        <div class="gurgaon_offers_pr">
                                        <? if($mrp_price>$price){?>
                                        <span>Rs. <del><?=number_format($mrp_price,0)?></del> 
                                        </span> &nbsp; &nbsp; <? }?>
                                        <span style="color: #f00;">
                                        Rs. <?=number_format($price,0)?></span>
                                        </div>
                                    </div>
          	</a> 
		</div>
                
                
                
                
      
                <? }?>
                 </div><?php */?>
                 
                 <div class="header-banner" >
 <?php require_once($DOCUMENT_ROOT .'/header-banner.php'); ?> 
   
</div>
<!-- /21630298032/testing -->

    	<h1 style="float:left; width:100%;"> Pet For Adoption</h1>
        

		<?php 
            ////////// Select All breeds
            $breed=mysql_query("SELECT distinct pa.breed_nicename, db.breed_name FROM puppies_available as pa, dog_breeds as db WHERE pa.adoption='yes'  AND pa.breed_nicename=db.nicename ORDER BY db.breed_name ");
            //End Breed 
            
            /// Select Location 
            $location = mysql_query("SELECT distinct(city_id),city_nicename,city FROM puppies_available WHERE adoption='yes' AND publish_status !='markdel' ORDER BY city  ");
            // End Location
            
			 
			
				$selectart5t = query_execute_row("SELECT city_name, city_nicename FROM city WHERE city_id = '$citySearch' ");
				//echo"SELECT breed_name FROM dog_breeds WHERE nicename = '$dog_breedSearch' ";
	$selectdog_breedSearch = query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename = '$dog_breedSearch' ");
	$city_name=$selectart5t['city_name'];
	$city_nicename=$selectart5t['city_nicename'];
	$breed_name=$selectdog_breedSearch['breed_name'];
				if($type=='dog'){
			  $pets="Dogs";
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
				  
				//  echo $dog_breedSearch.$citySearch;
				if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
				$pageh=ucwords($breed_name).' Dogs for Adoption';
				$title='Adopt a '.ucwords($breed_name).' Dogs | '.ucwords($breed_name).' Dogs for Adoption in India ';
				$keyword='Adopt a '.ucwords($breed_name).' Dogs, '.ucwords($breed_name).' Dogs for Adoption in India ';
				$desc='List of '.ucwords($breed_name).' dogs and puppies for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers';	
  				$sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  				$sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){


				$pageh='Dogs for Adoption in '.$city_name;
				$title='Dogs for Adoption in '.$city_name.' | Adopt a Dog for free in '.$city_name;
				$keyword='Dogs for Adoption in '.$city_name.' , Adopt a Dog for free in '.$city_name;
				$desc='Listings for dog adoption in Delhi '.$city_name.'. Get information on free dog adoption from shelters in '.$city_name.' or adopt dogs from a dog lover';

  				$sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) )  AND breed_nicename!='cats' ORDER BY status ASC ";
  				$sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
			}
			elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){



	$pageh=ucwords($breed_name).' Dogs for Adoption in '.$city_name;
	$title=ucwords($breed_name).' for Adoption in '.$city_name.' | Adopt '.ucwords($breed_name).' Dog in '.$city_name;
	$keyword=ucwords($breed_name).' for Adoption in '.$city_name.' , Adopt '.ucwords($breed_name).' Dog in '.$city_name.' adopt '.ucwords($breed_name).' in'.ucwords($breed_name);
	$desc='Local listings for '.ucwords($breed_name).' adoption in '.$city_name.'. Information on where and how to adopt '.ucwords($breed_name).' dogs in '.$city_name;
	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
$pageh=ucwords($breed_name).' Dogs for Adoption';
$title=ucwords($breed_name).' Dogs for Adoption  | Find information on '.ucwords($breed_name).' Dogs for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Dogs for Adoption , Find information on '.ucwords($breed_name).' Dogs for Adoption , DogSpot.in ';
	$desc='Find information on '.ucwords($breed_name).' Dogs for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';	  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Puppies for Adoption';
	$title='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$keyword='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$desc='Find information on Puppies for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

   $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE adoption='yes'  AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
	$pageh='Dogs for Adoption in '.$city_name;
	$title='Dogs for Adoption in '.$city_name.' | Free Dog Adoption | Pet Adoption | DogSpot.in';
	$keyword=' Dogs for Adoption in '.$city_name.', Free Dog Adoption,Pet Adoption,DogSpot.in ';
	$desc='Find Dog for Adoption in '.$city_name.' at Dogspot.in. See Dog for Adoption, Pet for Adoption at DogSpot.in';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif(!$citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Dogs for Adoption';
$title='Adopt a '.ucwords($breed_name).' Dogs | '.ucwords($breed_name).' Dogs for Adoption in India ';
	$keyword='Adopt a '.ucwords($breed_name).' Dogs, '.ucwords($breed_name).' Dogs for Adoption in India ';
	$desc='List of '.ucwords($breed_name).' dogs and puppies for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers';	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}else{
	$pageh=ucwords($breed_name).' Dogs for Adoption'.$city_name;
$title=ucwords($breed_name).' Dogs for Adoption in '.$city_name.' | Find '.ucwords($breed_name).' Dogs for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Dogs for Adoption in '.$city_name.' , Find '.ucwords($breed_name).' Dogs for Adoption , DogSpot.in ';
	$desc='Find '.ucwords($breed_name).' Dogs for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
 
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
//echo $sql;
$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Dogs for Adoption';
	$title='Dogs for Adoption | Adopt a Dog | Free Dog Adoptions India';
	$keyword='Dogs for Adoption , Adopt a Dog , Free Dog Adoptions India';
	$desc='Looking for a dog to adopt? Select from a list of dogs available for free adoption from shelters and dog lovers in India';
//echo "SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
   $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE adoption='yes'  AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {
	$totrecord=0;
  
}
		}elseif($type=='puppy')
		{
			 $pets="Puppies";
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Puppies for Adoption';
	$title='Adopt a '.ucwords($breed_name).' Puppies | '.ucwords($breed_name).' Puppies for Adoption in India';
	$keyword='Adopt a '.ucwords($breed_name).' Puppies , '.ucwords($breed_name).' Puppies for Adoption in India';
	
	$desc='List of '.ucwords($breed_name).' puppies and puppies for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers.';
	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){
	

	$pageh='Puppies for Adoption in '.$city_name;
	$title='Puppies for Adoption in '.$city_name.' | Adopt a Puppies for free in '.$city_name;
	$keyword='Puppies for Adoption in '.$city_name.' , Adopt a Puppies for free in '.$city_name;
	$desc='Listings for Puppies adoption in '.$city_name.'. Get information on free Puppies adoption from shelters in '.$city_name.' or adopt Puppies from a Puppies lover';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) )AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Puppies for Adoption in '.$city_name;
	$title=ucwords($breed_name).' for Adoption in '.$city_name.' | Adopt a '.ucwords($breed_name).' Puppies in '.$city_name;
	$keyword=ucwords($breed_name).' for Adoption in '.$city_name.' , Adopt '.ucwords($breed_name).' Puppies in '.$city_name.' , Adopt '.ucwords($breed_name).' in '.$city_name;
	$desc='Local listings for '.ucwords($breed_name).' adoption in '.$city_name.'. Information on where and how to adopt '.ucwords($breed_name).' Puppies in '.$city_name.'. ';
	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) )AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
	$pageh=ucwords($breed_name).' Puppies for Adoption';
	$title=ucwords($breed_name).' Puppies for Adoption  | Find information on '.ucwords($breed_name).' Puppies for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Puppies for Adoption , Find information on '.ucwords($breed_name).' Puppies for Adoption , DogSpot.in ';
	$desc='Find information on '.ucwords($breed_name).' Puppies for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Puppies for Adoption';
	$title='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$keyword='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$desc='Find information on Puppies for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

  $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}else{
	$pageh=ucwords($breed_name).' Puppies for Adoption in '.$city_name;
	$title=ucwords($breed_name).' Puppies for Adoption in '.$city_name.' | Find '.ucwords($breed_name).' Puppies for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Puppies for Adoption in '.$city_name.' , Find '.ucwords($breed_name).' Puppies for Adoption , DogSpot.in ';
	$desc='Find '.ucwords($breed_name).' Puppies for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}

$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Puppies for Adoption';
	$title='Adopt Puppies | Free Puppy Adoption in India';
	$keyword='Adopt Puppies , Free Puppy Adoption in India';

	$desc='Find yourselves a puppy to adopt from a list of dog puppies up for adoption from puppy care-takers and pup shelters in India.';

  $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {
	$totrecord=0;
  
}
		}elseif($type=='cat')
		{
			
			$pets="Cats";
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
	
	$pageh='Cats for Adoption';
		$title='Cats for Adoption | Adopt a Cat | Free Cat Adoptions India';
	$keyword='Cats for Adoption , Adopt a Cat , Free Cat Adoptions India ';
	$desc='Looking for a cat to adopt? Select from a list of cats available for free adoption from shelters and cat lovers in India.';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' ORDER BY status ASC LIMIT 0,4  ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  ORDER BY status ASC";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Find Cats for Adoption | DogSpot.in ';
	$keyword='Cats for Adoption in '.$city_name.' , Find Cats for Adoption , DogSpot.in ';
	$desc='Find Cats for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC";
}
elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){
	
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Adopt a Cats in '.$city_name;
	$keyword='Cats for Adoption in '.$city_name.' , Adopt Cat in '.$city_name.' , Adopt Cats in '.$city_name;
	$desc='Local listings for Cats adoption in '.$city_name.'. Information on where and how to adopt Cats in '.$city_name;
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status ASC";}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
	$pageh='Cats for Adoption';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status ASC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Find Cats for Adoption | DogSpot.in ';
	$keyword='Cats for Adoption in '.$city_name.' , Find Cats for Adoption , DogSpot.in ';
	$desc='Find Cats for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish'  ORDER BY status DESC";
}
elseif(!$citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh='Cats for Adoption';
		$title='Cats for Adoption | Adopt a Cat | Free Cat Adoptions India';
	$keyword='Cats for Adoption , Adopt a Cat , Free Cat Adoptions India ';
	$desc='Looking for a cat to adopt? Select from a list of cats available for free adoption from shelters and cat lovers in India.';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch'  AND adoption='yes' AND publish_status ='publish' ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch'  AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}
elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}else{
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Find Cats for Adoption | DogSpot.in ';
	$keyword='Cats for Adoption in '.$city_name.' , Find Cats for Adoption , DogSpot.in ';
	$desc='Find Cats for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}

$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {
	$totrecord=0;
  
}
		
		
		}else
		{
		
		
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
				  
			$pets="Pets";
if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Pets for Adoption';
	$title=ucwords($breed_name).' Pets for Adoption | '.ucwords($breed_name).' Puppies for Adoption in India';
	$keyword=ucwords($breed_name).' Pets for Adoption , '.ucwords($breed_name).' Puppies for Adoption in India';
	
	$desc='List of '.ucwords($breed_name).' and '.ucwords($breed_name).' for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers.';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC LIMIT 0,4 ";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Pet Adoption '.$city_name;
	$title='Pet Adoption '.$city_name.' | Adopt Pets up for free Adoption in '.$city_name;

	$keyword='pets adoption '.$city_name.', pet adoptions '.$city_name.', pets for adoption '.$city_name.', free pet adoption '.$city_name.', adopt pets in '.$city_name.'';
		$desc='Local listings for pet adoption in '.$city_name.'. Adopt a pet rescued by pet shelters or up for adoption by other pet lovers in '.$city_name.'.';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
}
elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' for Adoption in '.$city_name;
	$title=ucwords($breed_name).' for Adoption in '.$city_name.' | Adopt a '.ucwords($breed_name).' in '.$city_name;
	$keyword=ucwords($breed_name).' for Adoption in '.$city_name.', Adopt '.ucwords($breed_name).'  in '.$city_name.' , Adopt '.ucwords($breed_name).' in '.$city_name;
	$desc='Local listings for '.ucwords($breed_name).' adoption in '.$city_name.'. Information on where and how to adopt '.ucwords($breed_name).'  in '.$city_name.'. ';
	
	$sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status DESC LIMIT ";
}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status DESC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish'  ORDER BY status DESC";
}

elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}else{
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}

$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC LIMIT 0,4 ";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {
	$totrecord=0;
  
}
		
			
		}
			
            // Select puppies
            if(isset($_POST['search'])){
$citySearch=$_POST['city'];
$dog_breedSearch=$_POST['breed'];
$dog_statusSearch=$_POST['status'];
				
				
				if($_POST['breed']!=""){
                    $sql_breed = " AND breed_nicename='".$_POST['breed']."'";
                }
                if($_POST['city']!=""){
                    $sql_city = " AND city_id='".$_POST['city']."'";
                }
                if($_POST['status']!=""){
                    $sql_status = " AND status='".$_POST['status']."'";
                }
                $sql="SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' $sql_breed $sql_city $sql_status ORDER BY status ASC LIMIT 0,100";
		}else{
                $sql="SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' ORDER BY status ASC LIMIT 0,4";
            
			}
			
            // End Puppies data
        ?>
    	<form action="" method="post" name="filter" style="float:left; width:100%;">
        	<div class="adoption_header">
            	<div class="adp_m_b">
                	<select data-role="none" name="breed" id="breed" >
                    	<option value="">Select Breed</option>
					<?php while($breed_detail=mysql_fetch_array($breed)){?>
 <option value="<?=$breed_detail['breed_nicename']; ?>" <?php if($dog_breedSearch==$breed_detail['breed_nicename']){ ?>selected="selected"<?php }?>><?=$breed_detail['breed_name']; ?></option>   
                    <?php } ?>
                    </select>
	            </div>
    	        <div class="adp_m_b">
        	        <select  data-role="none" name="city" id="city">
            	        <option value="">Select Current Location</option>
                	     <?php while($location_details=mysql_fetch_array($location)){?>
 <option value="<?=$location_details['city_id']; ?>" <?php if($citySearch==$location_details['city_id']){ ?>selected="selected"<?php }?>><?=$location_details['city']; ?></option>   
	                    <?php } ?>
    	            </select>
        	    </div>
            	<div>
        	    	<input data-role="none" type="submit" name="search" id="search" value="GO" class="adoption_ok"/>
				</div>
	        </div>
    	</form>
        <div style="margin: 10px;" class="rytPost_list" id="rytPost_list">
         <? $iid=0;
 ?>       
		<!-- adoption list start-->
        <ul class="adoption_sec imgtxtcontwag" id='<?=$iid?>'>
    	<?php  $div_count='0';
			$adoptionData = mysql_query($sql);
			$perRow = 2; $i=1; $jk=0;
			$count = mysql_num_rows($adoptionData);
			if($count > 0){
				while($getRow = mysql_fetch_array($adoptionData)){
						$iid++;
		            $div_count++;
					$jk++;
					if($getRow['puppi_img']){
						$src = '/home/dogspot/public_html/puppies/images/'.$getRow['puppi_img'];
						$imageURL1='/puppies/images/400x300_'.$getRow['puppi_img'];
					}else{
						$src = '/home/dogspot/public_html/dogs/images/no-photo-t.jpg';
						$imageURL1='/dogs/images/no-photo-t.jpg';
					}
					$dest = "/home/dogspot/public_html".$imageURL1;
					createImgThumbIfnot($src,$dest,'400','300','ratiowh'); 
		?>
    	<?php if($i%$perRow!=0){?>
        
    		<ul class="adoption_sec imgtxtcontwag" id='<?=$iid?>'>
	    <?php }?>
            <li>
            <? if($getRow['status']=='rescue' || $getRow['status']=='foster'){
					 $colr='color:green;';
					 $status='Available';
					 }else
					 {
					 $colr='color:#f00;';
					 $status='Adopted';
					 
				     }?>
                <a href="<?=$getRow['puppy_nicename']; ?>/" data-ajax="false"><div><img alt="<?=$getRow['puppi_name']; ?>" title="<?=$getRow['puppi_name']; ?>" width="400px" height="300px" src="https://www.dogspot.in/puppies/images/400x300_<?=$getRow['puppi_img']; ?>"></div>
                <div class="dog_name"><?=$getRow['puppi_name']; ?></div>
                <div class="adoption_div">Gender: <?php if($getRow['puppi_sex']=='M'){ echo "Male"; }else{ echo "Female";} ?></div>
                <div class="adoption_div">Breed: <?= $getRow['puppi_breed'];?></div>
                <div class="adoption_div">City: <?=$getRow['city']; ?></div>
                <div class="adoption_div">Status: <span style=" <?=$colr?>" ><?=$status; ?></span></div>
                </a>
            </li>	
            
                       
    	<?php if($i%$perRow==0){ $j=$i; $i=0;?>
        </ul>
        <?  if($jk==4){ $jk=0;?>
        <div class="dfpAd<?=$iid?>" id='div-gpt-ad-1554034021305-0' style="text-align:center;padding-left:10px;padding-top:10px;padding-bottom:10px;">
        <? if($iid==4){?><script>
googletag.cmd.push(function() {
         googletag.display('div-gpt-ad-1554034021305-0');
 googletag.pubads().refresh([adslot0]);
        });

</script>
<? }else{?>
<script>
moreContent('<?=$iid?>');
</script>
<? }?>
</div>
      <? }  ?>  
                    <!--Recently Viewed item container start-->
		
			 <!--Recently Viewed item container end-->
        <?php }?>
         
        <?php $i++;} }else{?>
        <div style="font-size:14px; color:#668000; text-align:left;">
		No Pets were found suiting the selected criteria, kindly search again with a different search combination. 
		<a href="/adoption/" style="font-size:14px;" data-ajax="false"> Reset your search</a>
		</div>
        <?php }?>
        
        	<?php /*<div style="float:left; width:100%; text-align:center; margin-bottom:10px;">
  <script type="text/javascript" language="javascript">
     var aax_size='300x250';
     var aax_pubname = 'dog0bd-21';
     var aax_src='302';
   </script>
  <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>
</div> */?>
        <div id='loadMoreComments' style='display:none'><img src="https://www.dogspot.in/new/pix/loading2.gif" alt="Loading" title="Loading" width="32" height="32"/></div>
		<div id="txt1" style='display:none; cursor:pointer' >Adoption</div>
<div id="txt2" style='display:none'><? echo $totrecord-1; ?></div>		</div>
        </div>
	
        <!-- adoption end-->


    <script>jQuery(document).ready(function ($) {

    setInterval(function (){
        moveRight();
    }, 7000);

  
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider').css({ width: slideWidth, height: slideHeight });
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
		$('#slider').css('display','block');
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

});    
</script>     
<script type="text/javascript">
var cat_nice='';
 $(document).ready(function() { 
 //alert('233');
 cat_nice=document.getElementById('txt1').innerHTML;
 var c=0;
 //alert('j');
 countr=document.getElementById('txt2').innerHTML;
 //alert(2);
//alert ( $(".imgtxtcontwag:last").attr('id'));
$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < 400) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	
	if(c!=2){
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/adoption/loadmore.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<a style='text-decoration:none;' href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='background:#ddd; padding:5px; border-radius:5px; margin-bottom:20px;' >See More </div></a>");
		
	c=0;}$('#loadMoreComments').hide();
	} // c condition close
	}

	});
	});
</script>
<script>
function get(rt){
	$('#divw'+rt).hide();
	c=0;

	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < 400) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c!=3){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/adoption/loadmore.php?cat_nice="+cat_nice+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
		c=0;}$('#loadMoreComments').hide();
	
	} // c condition close-->
	}
	
	}

</script>     

<script>
        var prevSelection = "tab1";
        $("#navbar ul li").live("click",function(){
            var newSelection = $(this).children("a").attr("data-tab-class");
            $("."+prevSelection).addClass("ui-screen-hidden");
            $("."+newSelection).removeClass("ui-screen-hidden");
            prevSelection = newSelection;
        });
		$( function() {
			$( ".pagination span:first-child" ).addClass('swiper-visible-switch swiper-active-switch');
		//	$( "#purchase-product" ).enhanceWithin().popup();
			$( "#purchaseproductdetail" ).enhanceWithin().popup();
			$( "#purchaseproductdetail1" ).enhanceWithin().popup();
			$( "#selectsize_popup" ).enhanceWithin().popup();
			$( "#morepopup" ).enhanceWithin().popup();
			$( "#buynowpopup" ).enhanceWithin().popup();
			$( "#subscription_offer" ).enhanceWithin().popup();
			//$( "#defaultpanel" ).enhanceWithin().panel();
		});
    </script>   

<?php /*?><script type="text/javascript">
    $(document).ready(function () {
	var interval = setInterval(changewidth,3000);	
	  });
	function changewidth()
	{
document.getElementById("div-gpt-ad-1507785198319-0").style.height = "120px";
document.getElementById("google_ads_iframe_/21630298032/Adoptionmobile_0__container__").style.height = '120px';

	}
</script><?php */?>

	    <?php require_once(SITEMAIN_URL .'/common/bottom-final.php'); ?>
		<!-- cart page start-->
        
     