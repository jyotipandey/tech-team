<?php 

include_once("constants.php");
include_once("database.php");
//echo SITEMAIN_URL;
include_once(SITEMAIN_URL."/shop/functions.php");
include_once(SITEMAIN_URL."/shop/arrays/arrays.php");
include_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
include_once(SITEMAIN_URL."/functions.php");
include_once(SITEMAIN_URL."/functions2.php");
include_once(SITEMAIN_URL.'/session.php');
$sitesection = 'HOME';
if($utm_source=="sbibuddy")
{
 setcookie("sbibuddy", 'sbi', time()+3600*24*30, "/"); /* expire in 30 Days */
}

if($userid=='94048')
{
require_once('new-home.php');
exit;	
}
$ip=ipCheck();
?>
<?php require_once('common/script.php'); ?>
<!DOCTYPE html>
<html>
<head>
<title>DogSpot.in-Online Pet Supplies Store | Shop for Dog,Cat,Birds Products</title>

<meta name="keywords" content="DogSpot.in, Online Pet Supplies Store, Dog Store, Cat shop, Birds products, pet shopping India, puppies shop, cat supplies, dog shop online." />
<meta name="description" content="DogSpot.in Pet Supplies Store - India's best online shopping site for pets supplies, accessories & products for pets. Shop for Dog, Cat, Birds and small pets at great price from anywhere." />
<link href="https://www.dogspot.in" rel="canonical">
<style>.discount-stickerh{position: absolute;top: 6px;font-size: 12px; line-height: 12px; text-align: center;color: #fff; border-radius: 20px; background: #a50200;margin-left: 90px;padding: 4px;}</style>
	<?php include_once('common/top.php');
     //  $sessionUserCity=$_COOKIE['sessionUserCity'];
	  ?>
		<div data-role="content">
		<!-- homepage banner container start  -->
			<div class="device">
				<div class="slider1-container">
                <?php  include('includes/home/home-banner-images.php');  ?>
				</div>
                
			</div>
            <div class="pagination"></div>
			<!-- homepage banner container end  -->
              <style>
		    .new-arv-bc{float:left; width:100%;}
            .new_arv_h{float: left; width: 100%; margin-top: 5px; background: #F4f4f4; padding: 8px 10px; border-bottom: 1px solid #ddd;}
			.swiper-wrapper{ float:left;}
			.banner-cat img{width: 140px !important;height: auto !important }
	.banner-cat-t{position: absolute;margin-top: -52px;font-size: 12px; text-transform:uppercase; padding-left: 5px;}    
	.banner-cat-t .cat-off{ font-size:12px;}
	.banner-cat-t .cat-off span{ color: #ff587c;}
	.product-slide {
    width: 165px !important;
}
.banner-cat{    padding-top: 30px;}
	
            </style>
           
            <!--Recently Viewed item container start-->
          
			<div class="new_arrival_blk new_arv_bc" >
				<div class="new_arrival_header new_arv_h" >
					<h3 class="loadMore" data="youMayLike">Shop by Category</h3>
					</div>
				<div class="new_arrival_product">
				
				</div>
			</div>
			<div class="swiper-container swiper-container1 product-slider new-arrivals" style="height:170px;">
				<div class="swiper-wrapper" style="height:170px;">
                   
<a class="swiper-slide first-slide product-slide" href="/dog-food/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-food-cat.jpg" />
              </div>
      <div class="banner-cat-t" style="margin-left:15px; margin-top:-43spx;"><strong>Food</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div>
	</div>
</a>
<a class="swiper-slide first-slide product-slide" href="/biscuits-treats/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-treats.jpg" />
              </div>
      <div class="banner-cat-t"><strong>DOGS TREATS</strong>
      <div class="cat-off">Upto <span>25%</span>  <br/>OFF</div></div>
	</div>
</a>
<a class="swiper-slide first-slide product-slide" href="/health-wellness/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/health-n-wellness.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Health & <br />Wellness</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div>
	</div>
</a>

<a class="swiper-slide first-slide product-slide" href="/dog-toy/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/toys.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Toys</strong><div class="cat-off">Upto<span> 25%</span>
      <br/>  OFF</div></div>
	</div>
</a>
<a class="swiper-slide first-slide product-slide" href="/collars-leashes/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/collar-n-leash.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Collars & <br />Leashes</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-grooming/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/groming.jpg" />
              </div>
      <div class="banner-cat-t"><strong>GROOMIING</strong></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-bowls/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/bowls-n-fedders.jpg" />
              </div>
      <div class="banner-cat-t"><strong>bowls & <br/ > Feeders</strong></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/flea-ticks/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/flea-ntcks.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Flea & <br/ >Ticks</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/crates-beds/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/crates-n-cages.jpg" />
              </div>
      <div class="banner-cat-t"><strong>crates & <br/ >
Cages
	</strong></div></div>
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-clean-up/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-cleaning.jpg" />
              </div>
      <div class="banner-cat-t"><strong>dog <br>cleaning</strong><?php /*?> UPTO <span>15%</span>  OFF</div></strong><?php */?></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-training-behavior/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/tranning.jpg" />
              </div>
      <div class="banner-cat-t" style="margin-top:-43px;"><strong>Training</strong><?php /*?> UPTO <span>15%</span>  OFF</div></strong><?php */?>
      </div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/clothing-accessories/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/cloth.jpg" />
              </div>
      <div class="banner-cat-t" style="margin-top: -34px;"><strong>clothing</strong> 
     <div class="cat-off">Upto <span>25%</span> <br/> OFF</div></div></div>
	
</a>

				</div>
				<div class="pagination"></div>
			</div>
			 <!--Recently Viewed item container end-->
			   <? ?>
             <!--Recommended Products container start-->
             
           <? ?>
            <!--Recently Viewed item container start-->
          
			<div class="new_arrival_blk new_arv_bc" >
				<div class="new_arrival_header new_arv_h" >
					<h3 class="loadMore" data="youMayLike">Hot Selling Products</h3>
					</div>
				<div class="new_arrival_product">
				
				</div>
			</div>
			<div class="swiper-container swiper-container2 product-slider new-arrivals" >
				<div class="swiper-wrapper" >
       <? $item_idSell=hotSelling();
	   foreach($item_idSell as $item){$qItemq=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$type_id1=$qItemq["type_id"];
					$name=$qItemq["name"];
					$domain_id=$qItemq["domain_id"];
					$stock_status=$qItemq["stock_status"];
					$price=$qItemq["price"];
					$selling_price=$qItemq["selling_price"];
					$discountPer=($selling_price-$price)/$selling_price*100;	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
					$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
					$nice_name=$qItemnice["nice_name"];	
					$rowdatM = mysql_fetch_array($qdataM);
				if($rowdatM["media_file"]){
		//$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		//$imageURL='/imgthumb/250x250-'.$rowdatM["media_file"];
		$src = $imageBasePath.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL=$absPath.'/imgthumb/184x184-'.$rowdatM["media_file"];
		$dest = $imageBasePath.'/imgthumb/184x184-'.$rowdatM["media_file"];
	}else{
		//$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		//$imageURL='/shop/image/no-photo-t.jpg';
		$src = $imageBasePath.'/shop/image/no-photo-t.jpg';
		$imageURL=$absPath.'/shop/image/no-photo-t.jpg';
		$dest = $imageBasePath.'/shop/image/no-photo-t.jpg';
	}
//	$dest = SITEMAIN_URL.$imageURL;
	createImgThumbIfnot($src,$dest,'184','184','ratiowh');

		   ?>            

<a class="swiper-slide first-slide product-slide" href="<?=$nice_name?>/" style="width: 421px;" data-ajax="false">

	<div class="title">
    <?php if($price<$selling_price) { $per= ($selling_price-$price)/$selling_price*100?>
<span class="discount-stickerh" > <?=number_format($per,0)?>% <div style="display: block;">off</div></span>
<? }?>
		<div><img src="<?=$imageURL?>" alt="<?=$name?>" border="0"  align="middle" title="<?=$name?>" height="" width=""/></div>
		<div class="product_desc"><?=$name?> </div>
		<div class="pro_price_det"><span><i class="fa fa-inr"></i></span> <span> <?=number_format($price,0);?></span></div>
		<?php if($price<$selling_price) { ?><p><i class="fa fa-inr"></i><span> <strike><?=number_format($selling_price,0);?></strike></span></p> <?php }?>
	</div>
</a>
<? }?>


				</div>
				<div class="pagination"></div>
			</div>
			 <!--Recently Viewed item container end-->
              <!--Recently Viewed item container start-->
       <?  if($userid=='Guest'){
	    $countuser=query_execute("SELECT si.item_id FROM user_items_history as us,shop_items as si WHERE si.item_id=us.item_id  AND visibility='visible' AND stock_status='instock' AND user_ip='$ip' ORDER BY id DESC LIMIT 10"); 	   }elseif($userid !='Guest')
	   {
		   $countuser=query_execute("SELECT si.item_id FROM user_items_history as us,shop_items as si WHERE si.item_id=us.item_id  AND visibility='visible' AND stock_status='instock' AND userid='$userid' ORDER BY id DESC LIMIT 10");   
	   }
$c_count=mysql_num_rows($countuser);
if($c_count>3)
{
	//echo "SELECT item_id FROM user_items_history WHERE (userid='$userid' OR user_ip='$ip') AND userid !='Guest' ORDER BY id DESC";
	?>
			<div class="new_arrival_blk new_arv_bc" >
				<div class="new_arrival_header new_arv_h" >
					<h3 class="loadMore" data="youMayLike">Recently Viewed</h3>
					</div>
				<div class="new_arrival_product">
				
				</div>
			</div>
			<div class="swiper-container swiper-container4 product-slider new-arrivals" >
				<div class="swiper-wrapper" >
       <? while($item_idS=mysql_fetch_array($countuser))
	   {
		$item_idRec[]= $item_idS['item_id']; 
	   }
	   foreach($item_idRec as $item){$qItemq=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$type_id1=$qItemq["type_id"];
					$name=$qItemq["name"];
					$domain_id=$qItemq["domain_id"];
					$stock_status=$qItemq["stock_status"];
					$price=$qItemq["price"];
					$selling_price=$qItemq["selling_price"];
					$discountPer=($selling_price-$price)/$selling_price*100;	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
					$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
					$nice_name=$qItemnice["nice_name"];	
					$rowdatM = mysql_fetch_array($qdataM);
				if($rowdatM["media_file"]){
		//$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		//$imageURL='/imgthumb/250x250-'.$rowdatM["media_file"];
		$src = $imageBasePath.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL=$absPath.'/imgthumb/184x184-'.$rowdatM["media_file"];
		$dest = $imageBasePath.'/imgthumb/184x184-'.$rowdatM["media_file"];
	}else{
		//$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		//$imageURL='/shop/image/no-photo-t.jpg';
		$src = $imageBasePath.'/shop/image/no-photo-t.jpg';
		$imageURL=$absPath.'/shop/image/no-photo-t.jpg';
		$dest = $imageBasePath.'/shop/image/no-photo-t.jpg';
	}
//	$dest = SITEMAIN_URL.$imageURL;
	createImgThumbIfnot($src,$dest,'184','184','ratiowh');

		   ?>            

<a class="swiper-slide first-slide product-slide" href="<?=$nice_name?>/" style="width: 421px;" data-ajax="false">

	<div class="title">
    <?php if($price<$selling_price) { $per= ($selling_price-$price)/$selling_price*100?>
<span class="discount-stickerh" > <?=number_format($per,0)?>% <div style="display: block;">off</div></span>
<? }?>
		<div><img src="<?=$imageURL?>" alt="<?=$name?>" border="0"  align="middle" title="<?=$name?>" height="" width=""/></div>
		<div class="product_desc"><?=$name?> </div>
		<div class="pro_price_det"><span><i class="fa fa-inr"></i></span> <span> <?=number_format($price,0);?></span></div>
		<?php if($price<$selling_price) { ?><p><i class="fa fa-inr"></i><span> <strike><?=number_format($selling_price,0);?></strike></span></p> <?php }?>
	</div>
</a>
<? }?>


				</div>
				<div class="pagination"></div>
			</div>
			 <!--Recently Viewed item container end-->
			   <? } ?>
             <!--Recommended Products container end-->
             <!--new arrival set start-->
			 <div class="new_arrival_blk">
				<div class="new_arrival_header">
					<h2 class="loadMore" data="newArrival">New Arrivals</h2>
					<form action="/feature_products.php" id="newArrivalForm" method="post">
						<input type="hidden" name="moreType" id="newArrival" value="newArrival">
					</form>
				</div>
				<div class="new_arrival_product">
				</div>
			</div>
			<?php include('includes/home/new_arrival.php');?>
            <!--new arrival set end-->
			 
             <!-- what other are buying -->
			 <div class="new_arrival_blk">
				<div class="new_arrival_header">
					<h2 class="loadMore" data="Other">Popular Products</h2>
					<form action="/feature_products.php" id="youMayLikeForm" method="post">
						<input type="hidden" name="moreType" id="Other" value="Other">
					</form>
				</div>
				<div class="new_arrival_product">
				
				</div>
			</div>
            <?php  include('includes/home/other-buying-products.php');  ?>
             <!-- offer banner start-->
         	<?php //include('includes/home/home-small-banner.php');?>
			<!-- offer banner end--> 
             <!--Recommended Products container end-->
        </div>
 
 		
        
<div style="width:100%; padding-top:15px; padding-bottom:15px; text-align:center; margin-bottom:52px;">
	<span class="ds_desktop_version"><a href="https://www.dogspot.in/index.php?to=dt" style="font-size:14px; color:#79B048; text-decoration:underline;">Desktop version</a></span>
</div>

<?php //if($userid=='umesh3') {?>
<!-- shop with whatsapp-->
	<div class="customer_care_Div"  style="margin-top:15px;position: fixed;bottom: 0px;width:100%;background:#f2f2f2;  z-index: 99999;">
		<div class="phone_circle" style="border:0px;">
			<a href="tel:+919212196633" class="ds_footer_links">
            	<span class="fa fa-whatsapp" style="font-size: 26px;"></span></a>
		</div>
        <div class="custmr_care_blk">
			<div class="custmr_care_no"><div class="custmr_care_no">Shop with Whatsapp
            	<a href="tel:+919599090487" class="ds_footer_links ui-link"><span>+91-9599090487</span></a></div>
			</div>
		</div>
	</div>
<!-- shop with whatsapp end-->


		<?php require_once('common/bottom.php'); ?>