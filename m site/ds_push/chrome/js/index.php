<?php
    ini_set('memory_limit','512M');
	/*ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);*/
	if($section[0]==''){
		require_once("../constants.php");
	}else{
		require_once("../constants.php");
	}
	require_once(SITEMAIN_URL."/database.php");
	require_once(SITEMAIN_URL."/functions.php");
	require_once(SITEMAIN_URL."/shop/functions.php");
	require_once(SITEMAIN_URL."/functions2.php");
	require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
	require_once(SITEMAIN_URL.'/session.php');
	$sitesection='adoption';?>

<!DOCTYPE html>
<html>
<head>
<title>Pet Adoption India, Search Free Pets for Adoption & Adopt a Pet</title>
<meta name="keywords" content="Pet Adoption India, Search Free Pets for Adoption & Adopt a Pet" />
<meta name="description" content="Pet Adoption India an initiative by DogSpot.in - find pets for free adoption rescued by animal shelters and pet lovers in India. Adopt a Pet and show that you care" />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="adoption" />
<meta property="og:url" content="https://www.dogspot.in/adoption/" /> 
<meta property="og:title" content="Pet Adoption India, Search Free Pets for Adoption & Adopt a Pet" /> 
<meta property="og:description" content="Pet Adoption India an initiative by DogSpot.in - find pets for free adoption rescued by animal shelters and pet lovers in India. Adopt a Pet and show that you care" />
<link rel="canonical" href="https://www.dogspot.in/adoption/" />

<style>.ui-select .ui-btn{border: none;
    font-size: 14px;
    border: 1px solid #ddd;
    padding: 10px 10px;
    color: #555;} 
	.ui-input-btn{padding: 10px 0px!important;;
    border-radius: 5px !important;}
	.adoption_header{ background:#fff !important;}
.discount-stickerh{position: absolute;top: 6px;font-size: 12px; line-height: 12px; text-align: center;color: #fff; border-radius: 20px; background: #a50200;margin-left: 90px;padding: 4px;}
 .new-arv-bc{float:left; width:100%;}
            .new_arv_h{float: left; width: 100%; margin-top: 5px; background: #F4f4f4; padding: 8px 10px; border-bottom: 1px solid #ddd;}
			.swiper-wrapper{ float:left;}
			.banner-cat img{width: 140px !important;height: auto !important }
	.banner-cat-t{position: absolute;margin-top: -52px;font-size: 12px; text-transform:uppercase; padding-left: 5px;}    
	.banner-cat-t .cat-off{ font-size:12px;}
	.banner-cat-t .cat-off span{ color: #ff587c;}
	.product-slide {
    width: 165px !important;
}
.banner-cat{    padding-top: 30px;}
.pagination{ display:none !important;}
.swiper-container.product-slider{ float:left;  margin-bottom:20px;}
.product-slide{ border:0px !important;}
</style>
	<script type="text/javascript" src="<?php echo $baseUrl;?>js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUrl;?>js/live-validation.js"></script>
    <script type="text/javascript" src="<?php echo $baseUrl;?>js/jquery.mobile-1.4.4.min.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl;?>js/shaajax.min.2.1.js"></script> 
    <script>jQuery(document).ready(function ($) {

    setInterval(function (){
        moveRight();
    }, 7000);

  
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider').css({ width: slideWidth, height: slideHeight });
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
		$('#slider').css('display','block');
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

});    
</script>
    <?php require_once($DOCUMENT_ROOT .'/common/top-new.php'); ?>
 
    <div data-role="content" class="art-content">
    <div class="adoption_blk" >
   
    	<h1 style="float:left; width:100%;"> Pet For Adoption</h1>
        

		<?php 
            ////////// Select All breeds
            $breed=mysql_query("SELECT distinct pa.breed_nicename, db.breed_name FROM puppies_available as pa, dog_breeds as db WHERE pa.adoption='yes'  AND pa.breed_nicename=db.nicename ORDER BY db.breed_name");
            //End Breed 
            
            /// Select Location 
            $location = mysql_query("SELECT distinct(city_id),city_nicename,city FROM puppies_available WHERE adoption='yes' AND publish_status !='markdel' ORDER BY city  ");
            // End Location
            
            // Select puppies
            if(isset($_POST['search'])){
				if($_POST['breed']!=""){
                    $sql_breed = " AND breed_nicename='".$_POST['breed']."'";
                }
                if($_POST['city']!=""){
                    $sql_city = " AND city_id='".$_POST['city']."'";
                }
                if($_POST['status']!=""){
                    $sql_status = " AND status='".$_POST['status']."'";
                }
                $sql="SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' $sql_breed $sql_city $sql_status ORDER BY e_date DESC LIMIT 0,30";
            }else{
                $sql="SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' ORDER BY e_date DESC LIMIT 0,30";
            }
            // End Puppies data
        ?>
    	<form action="" method="post" name="filter" style="float:left; width:100%;">
        	<div class="adoption_header">
            	<div class="adp_m_b">
                	<select name="breed" id="breed">
                    	<option value="">Select Breed</option>
					<?php while($breed_detail=mysql_fetch_array($breed)){?>
 <option value="<?=$breed_detail['breed_nicename']; ?>" <?php if($_POST['breed']==$breed_detail['breed_nicename']){ ?>selected="selected"<?php }?>><?=$breed_detail['breed_name']; ?></option>   
                    <?php } ?>
                    </select>
	            </div>
    	        <div class="adp_m_b">
        	        <select name="city" id="city">
            	        <option value="">Select Current Location</option>
                	     <?php while($location_details=mysql_fetch_array($location)){?>
 <option value="<?=$location_details['city_id']; ?>" <?php if($_POST['city']==$location_details['city_id']){ ?>selected="selected"<?php }?>><?=$location_details['city']; ?></option>   
	                    <?php } ?>
    	            </select>
        	    </div>
            	<div class="adp_m_b">
                	<select name="status" id="status">
                    	<option value="">Select Status</option>
	                    <option value="rescue" <?php if($_POST['status']=='rescue'){ ?>selected="selected"<?php }?>>Rescue</option>
    	                <option value="foster" <?php if($_POST['status']=='foster'){ ?>selected="selected"<?php }?>>Foster</option>
        	            <option value="adopted" <?php if($_POST['status']=='adopted'){ ?>selected="selected"<?php }?>>Adopted</option>
            	    </select>
	            </div>
    	        <div>
        	    	<input type="submit" name="search" id="search" value="GO" class="adoption_ok"/>
				</div>
	        </div>
    	</form>
        <div style="margin: 10px;">
		<!-- adoption list start-->
        <ul class="adoption_sec">
    	<?php 
			$adoptionData = mysql_query($sql);
			$perRow = 2; $i=1; $jk=0;
			$count = mysql_num_rows($adoptionData);
			if($count > 0){
				while($getRow = mysql_fetch_array($adoptionData)){
					$jk++;
					if($getRow['puppi_img']){
						$src = '/home/dogspot/public_html/puppies/images/'.$getRow['puppi_img'];
						$imageURL1='/puppies/images/400x300_'.$getRow['puppi_img'];
					}else{
						$src = '/home/dogspot/public_html/dogs/images/no-photo-t.jpg';
						$imageURL1='/dogs/images/no-photo-t.jpg';
					}
					$dest = "/home/dogspot/public_html".$imageURL1;
					createImgThumbIfnot($src,$dest,'400','300','ratiowh'); 
		?>
    	<?php if($i%$perRow!=0){?>
    		<ul class="adoption_sec">
	    <?php }?>
            <li>
                <a href="<?=$getRow['puppy_nicename']; ?>" data-ajax="false"><div><img width="400px" height="300px" src="https://www.dogspot.in/puppies/images/400x300_<?=$getRow['puppi_img']; ?>"></div>
                <div class="dog_name"><?=$getRow['puppi_name']; ?></div>
                <div class="adoption_div">Gender: <?php if($getRow['puppi_sex']=='M'){ echo "Male"; }else{ echo "Female";} ?></div>
                <div class="adoption_div">Breed: <?= $getRow['puppi_breed'];?></div>
                <div class="adoption_div">City: <?=$getRow['city']; ?></div>
                <div class="adoption_div">Status: <span style="color:#f00;"><?=$getRow['status']; ?></span></div>
                </a>
            </li>	
    	<?php if($i%$perRow==0){ $j=$i; $i=0;?>
        </ul>
                    <!--Recently Viewed item container start-->
         <? if($jk==2){?>
         <div class="new_arrival_blk new_arv_bc" >
				<div class="new_arrival_header new_arv_h" >
					<h3 class="loadMore" data="youMayLike" style="text-align:left;">Hot Selling Products</h3>
					</div>
				<div class="new_arrival_product">
				
				</div>
			</div>
			<div class="swiper-container swiper-container2 product-slider new-arrivals" >
				<div class="swiper-wrapper" >
       <? $item_idSell=hotSelling();
	   foreach($item_idSell as $item){$qItemq=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$type_id1=$qItemq["type_id"];
					$name=$qItemq["name"];
					$domain_id=$qItemq["domain_id"];
					$stock_status=$qItemq["stock_status"];
					$price=$qItemq["price"];
					$selling_price=$qItemq["selling_price"];
					$discountPer=($selling_price-$price)/$selling_price*100;	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
					$qItemnice=query_execute_row("SELECT nice_name FROM shop_items  WHERE item_id='$item_id'");
					$nice_name=$qItemnice["nice_name"];	
					$rowdatM = mysql_fetch_array($qdataM);
				if($rowdatM["media_file"]){
		//$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		//$imageURL='/imgthumb/250x250-'.$rowdatM["media_file"];
		$src = $imageBasePath.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL=$absPath.'/imgthumb/184x184-'.$rowdatM["media_file"];
		$dest = $imageBasePath.'/imgthumb/184x184-'.$rowdatM["media_file"];
	}else{
		//$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		//$imageURL='/shop/image/no-photo-t.jpg';
		$src = $imageBasePath.'/shop/image/no-photo-t.jpg';
		$imageURL=$absPath.'/shop/image/no-photo-t.jpg';
		$dest = $imageBasePath.'/shop/image/no-photo-t.jpg';
	}
//	$dest = SITEMAIN_URL.$imageURL;
	createImgThumbIfnot($src,$dest,'184','184','ratiowh');

		   ?>            

<a class="swiper-slide first-slide product-slide" href="/<?=$nice_name?>/" style="width: 421px;" data-ajax="false">

	<div class="title">
    <?php if($price<$selling_price) { $per= ($selling_price-$price)/$selling_price*100?>
<span class="discount-stickerh" > <?=number_format($per,0)?>% <div style="display: block;">off</div></span>
<? }?>
		<div><img src="<?=$imageURL?>" alt="<?=$name?>" border="0"  align="middle" title="<?=$name?>" height="" width=""/></div>
		<div class="product_desc"><?=$name?> </div>
		<div class="pro_price_det"><span><i class="fa fa-inr"></i></span> <span> <?=number_format($price,0);?></span></div>
		<?php if($price<$selling_price) { ?><p><i class="fa fa-inr"></i><span> <strike><?=number_format($selling_price,0);?></strike></span></p> <?php }?>
	</div>
</a>
<? }?>


				</div>
				<div class="pagination"></div>
			</div>
           
         <? }elseif($jk==10){?>
          <!--Recently Viewed item container start-->
          
			<div class="new_arrival_blk new_arv_bc" >
				<div class="new_arrival_header new_arv_h" >
					<h3 class="loadMore" data="youMayLike">Shop by Category</h3>
					</div>
				<div class="new_arrival_product">
				
				</div>
			</div>
			<div class="swiper-container swiper-container1 product-slider new-arrivals" style="height:170px;">
				<div class="swiper-wrapper" style="height:170px;">
                   
<a class="swiper-slide first-slide product-slide" href="/dog-food/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-food-cat.jpg" />
              </div>
      <div class="banner-cat-t" style="margin-left:15px; margin-top:-43spx;"><strong>Food</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div>
	</div>
</a>
<a class="swiper-slide first-slide product-slide" href="/biscuits-treats/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-treats.jpg" />
              </div>
      <div class="banner-cat-t"><strong>DOGS TREATS</strong>
      <div class="cat-off">Upto <span>25%</span>  <br/>OFF</div></div>
	</div>
</a>
<a class="swiper-slide first-slide product-slide" href="/health-wellness/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/health-n-wellness.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Health & <br />Wellness</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div>
	</div>
</a>

<a class="swiper-slide first-slide product-slide" href="/dog-toy/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/toys.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Toys</strong><div class="cat-off">Upto<span> 25%</span>
      <br/>  OFF</div></div>
	</div>
</a>
<a class="swiper-slide first-slide product-slide" href="/collars-leashes/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/collar-n-leash.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Collars & <br />Leashes</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-grooming/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/groming.jpg" />
              </div>
      <div class="banner-cat-t"><strong>GROOMIING</strong></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-bowls/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/bowls-n-fedders.jpg" />
              </div>
      <div class="banner-cat-t"><strong>bowls & <br/ > Feeders</strong></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/flea-ticks/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title" style="width:162px;">
            <div class="banner-cat" >
              <img src="https://www.dogspot.in/new/images/flea-ntcks.jpg" />
              </div>
      <div class="banner-cat-t"><strong>Flea & <br/ >Ticks</strong><?php /*?> UPTO <span>20%</span>  OFF<?php */?></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/crates-beds/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/crates-n-cages.jpg" />
              </div>
      <div class="banner-cat-t"><strong>crates & <br/ >
Cages
	</strong></div></div>
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-clean-up/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/dog-cleaning.jpg" />
              </div>
      <div class="banner-cat-t"><strong>dog <br>cleaning</strong><?php /*?> UPTO <span>15%</span>  OFF</div></strong><?php */?></div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/dog-training-behavior/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/tranning.jpg" />
              </div>
      <div class="banner-cat-t" ><strong>Training</strong><?php /*?> UPTO <span>15%</span>  OFF</div></strong><?php */?>
      </div></div>
	
</a>
<a class="swiper-slide first-slide product-slide" href="/clothing-accessories/" style="height: 162px; width: 421px;" data-ajax="false">

	<div class="title">
            <div class="banner-cat">
              <img src="https://www.dogspot.in/new/images/cloth.jpg" />
              </div>
      <div class="banner-cat-t" ><strong>clothing</strong> 
     <div class="cat-off">Upto <span>25%</span> <br/> OFF</div></div></div>
	
</a>

				</div>
				<div class="pagination"></div>
			</div>
			 <!--Recently Viewed item container end-->
         <? }?>
			
			 <!--Recently Viewed item container end-->
        <?php }?> 
        <?php $i++;} }else{?>
        <p style="margin-top: 10px; font-size:18px; color:#668000; text-align:left; ">No Pets were found suiting the selected criteria, kindly search again with a different search combination.<font size="3"><a href="/adoption/" style="font-size:18px;" data-ajax="false">Reset your search</a></font></p>
        <?php }?>
        </div>
        <!-- adoption end-->
        </div>
      </div>
	    <?php require_once(SITEMAIN_URL .'/common/bottom.php'); ?>
		<!-- cart page start-->
        
     