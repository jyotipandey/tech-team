<?php
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/session.php");
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/shop/functions.php");
require_once(SITEMAIN_URL."/shop/arrays/arrays.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Wag Tag Information | Dog Tag | Full Detail | DogSpot.in</title>
<meta name="keywords" content="Wag Tag Information,Full Detail On Wag Tag, Dog Tag" />
<meta name="description" content="Wag Tag, Dog Tag Full Information on Wag Tag like,A unique id for every dog,Activate it easily through your phone or Internet" />

<link rel="canonical" href="https://www.dogspot.in/wagtag-info/" />

<?php require_once('common/script.php'); ?>
<?php require_once('common/top.php'); ?>
</head>

<script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
 
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1552614-5', 'auto');
  ga('require', 'ec');
 
 ga('send', 'pageview'); 
function checksearch()
{
var searchtext=document.getElementById('searchText').value;
if(searchtext=='')
{	
	//document.getElementById('searchText').style.backgroundColor = "red";
	document.getElementById('SrchErr').innerHTML="<font color='red'>This field is required.</font>";
	document.formhgtr.searchText.focus();
	return false;
}
else
{
	return true;
}
}


</script>
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1423135197964769']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>



</head>

<body>
<div class="wagtag_headingTop">
<h1>More about Wag Tag</h1>
</div>
<!--wag tag wrapper-->
<div id="wagtag_wrapper">
 <div class="bellows default">
            <div class="bellows__item ">
                <div class="bellows__header">
                    <h3>How it works?</h3>
                </div>
                <div class="bellows__content">
                    <div class="How_It_works"><img src="images/tag_cycle.png"></div>
                </div>
            </div>
            <div class="bellows__item">
                <div class="bellows__header">
                    <h3>What is Wag Tag</h3>
                </div>
                <div class="bellows__content">
                <!-- what is wag tag text-->
                  <div class="what_is_wagTag">
                  <div class="wagtag_textheading">Why does my dog need a wag tag?</div>
                  <div class="wagtag_tex">Every dog needs a wag tag to ensure its sagety. A wat tag will help your pet to reunite with younin case of any unfortunate and unforeseen circumstances.  The unique id on the tag will help 
the finder to contact your Information. The call will be connected to DogSpot and they will reroute it o the rightful owner.</div>
                  </div>
                  <!-- what is wag tag text-->
                   <!-- what is wag tag text-->
                  <div class="what_is_wagTag">
                  <div class="wagtag_textheading">How much does it cost?</div>
                  <div class="wagtag_tex">The wag tag is worth 300 INR but you can avail it now at rupees 125 by an introductory offer.</div>
                  </div>
                  <!-- what is wag tag text-->
                  <!-- what is wag tag text-->
                  <div class="what_is_wagTag">
                  <div class="wagtag_textheading">Do you charge any monthly fees for wag tag?</div>
                  <div class="wagtag_tex">No, there is no fee charged for using wag tag. There are no hiddem subscriptions involved this product.</div>
                  </div>
                  <!-- what is wag tag text-->
                </div>
            </div>
            <div class="bellows__item">
                <div class="bellows__header">
                    <h3>FAQ</h3>
                </div>
                
               <!-- quest-->
                <div class="bellows__content">
                <!-- Question-->
                
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>Why does my dog need a wag tag?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>Every dog needs a wag tag to ensure its safety. A wag tag will help your pet to reunite with you in case of any unfortunate and unforeseen circumstances. The unique id on the tag will help the finder to contact you while maintaining confidentiality of your information. The call will be connected to DogSpot and they will reroute it to the rightful owner. </p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                            
                           
                              <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>How much does it cost?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>The wag tag is worth 300 INR but you can avail it now at rupees 125 by as an introductory offer.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                            
                                 <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>Do you charge any monthly fees for wag tag?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>No, there is no fee charged for using wag tag. There are no hidden subscriptions involved with this product.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                            <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>I have damaged the activation code that came with the product. What shall I do next?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>Every wag tag is unique in its way. The tag will be activated only with the activation code provided in the pack. If you have accidentally damaged the code then just call our customer service number and they will help you.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                            
                            <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>How secure is your data?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>Your data is absolutely secure and no one will be able to accsess it. In fact to ensure privacy and confidentiality of the owner the number is also not displayed on the tag. The call is routed to the rightful owner through DogSpot.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                              <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>How will the finder contact me?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>The finder will call the number given on the tag and give in the unique wag tag id given on the tag and the call will be routed to the rightful owner.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                            <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>Is it an alternative to micro chipping?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>No, micro chipping and the tag are two different things. A wag tag allows the finder to contact you instantly, whereas in a microchip the individual needs a hand scanner that can scan the information on the microchip. Your pet will require a surgery for a microchip, whereas the tag can be easily attached to any of the commercially available dog collars.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                            
                                <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>I have a tag but do not have a profile on Wag Club?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>To activate your wag tag it is absolutely integral to have a profile of your dog on the wag club. Otherwise you cannot activate the tag.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                             <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>Benefits over Traditional tag?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>A traditional tag will display your information on the tag itself, whereas the wag tag will maintain the privacy for the owner and route the call through DogSpot. wag tag allows you to store your pet’s vital information and if required allows you to update it as well.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                              <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>How do I activate my tag?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>The tag is activated with the help of an activation code provided at the back of the product. You just need to create a profile for your dog. There is an activate tab on the page that has to be clicked and you just have to enter the activation code provided in the pack and the tag is activated with all the required information.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                 <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>What happens if my dog losses my tag?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>if accidentally the tag is lost, you can always buy a new tag. The moment you activate the new tag, the older tag will be de-activated.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                               <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>How many numbers can I add in my wag tag?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>You can add up to three numbers on your wag tag profile.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                             <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>Are there any sizes to chose from?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>No there is a standard size for all your dogs, which suits and fits all.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                            <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>My friends wants a tag, can I give them mine?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>No you cannot share or give your tag to anyone else as the tag has been generated with your personal details and your pets. If your friend wants one then he or she has to buy a new wag tag.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
                              <!-- Question-->
                <div class="bellows faq_div">
                
                        <div class="bellows__item">
                            <div class="bellows__header">
                                <h5>What is the tag made of and how durable is it?</h5>
                            </div>
                            <div class="bellows__content">
                            <p>The tag is made of durable quality stainless steel.</p>
                            </div>
                            </div>
                            
                            </div>
                            <!-- Question-->
            </div></div>
            <!-- Faq-->
            <div class="bellows__item" style="border-bottom: 1px solid #ddd;">
                <div class="bellows__header">
                    <h3>Compare Wag Tag</h3>
                </div>
                <div class="bellows__content">
                   <div class="compare_wagTag">
                   <img src="images/Compare.png">
                   </div>
                </div>
            </div>
        </div>		
</div>
<style>
*{box-sizing:border-box}html{font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;-webkit-text-size-adjust:100%}.bellows{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.bellows__item{border-top:1px solid #ddd} .bellows__content-wrapper{}.bellows.faq_div:last-child .bellows__item{border-bottom:0}.bellows__header{cursor: pointer;position:relative;padding:13px 10px;border-width:0 0 1px;background:#fff;color:#000;-webkit-tap-highlight-color:transparent}.bellows__header:active{background:#fff}.bellows__header::after,.bellows__header::before{content:'';position:absolute;top:50%;right:20px;z-index:2;display:block;width:12px;height:2px;margin-top:-2px;background:#000;pointer-events:none;-webkit-transition:-webkit-transform .25s ease-in-out;transition:transform .25s ease-in-out}.panellisting .bellows__item:first-child .bellows__header::after,.panellisting .panellisting .bellows__header::after,.panellisting .bellows__header::before{content:'';position:absolute;top:50%;right:20px;z-index:2;display:block;height:2px;margin-top:-2px;background:#000;pointer-events:none;-webkit-transition:-webkit-transform .25s ease-in-out;transition:transform .25s ease-in-out}.bellows__header::before{content:'';-webkit-transform:rotate(0deg);-ms-transform:rotate(0deg);transform:rotate(0deg)}.bellows__header::after{-webkit-transform:rotate(90deg);-ms-transform:rotate(90deg);transform:rotate(90deg)}.bellows__item.bellows--is-open>.bellows__header::before,.bellows__item.bellows--is-opening>.bellows__header::before{-webkit-transform:rotate(180deg);-ms-transform:rotate(180deg);transform:rotate(180deg)}.bellows__item.bellows--is-open>.bellows__header::after,.bellows__item.bellows--is-opening>.bellows__header::after{-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}.panellisting .bellows__header h1,.panellisting .bellows__header h2,.panellisting .bellows__header h3,.panellisting .bellows__header h4{margin:0;font-size:12px;color:#333}.panellisting .bellows__item:first-child .bellows__header h3{font-size:12px;color:#333; margin-left:10px; }.panellisting .bellowsheader h3 a{margin:0;font-size:12px;color:#333;text-decoration:none}.panellisting .bellows__item .bellows__header h3 a{color:#333;text-decoration:none} .bellow_listing {border-top:1px solid #e4e4e4;} .bellow_listing li{border-bottom:1px solid #e4e4e4;font-size:12px;padding:8px 36px}.bellow_listing a,.bellow_listing a:hover{text-decoration:none;color:#333}.bellow_listing li:last-child{border-bottom:0}.bellow_listing li a{color:#000;text-decoration:none}.panellisting .bellows__header h3 img,.panellisting .bellowsheader h3 img{margin-right:8px;vertical-align:text-top}.bellows__header h1,.bellows__header h2,.bellows__header h3,.bellows__header h4{margin:0px 0px 0px 0px}.bellows__header h5{margin:0;font-size:12px;font-weight:600;color:#000}.bellows__content{background:#FFF}.bellows__item:not(.bellows--is-open)>.bellows__content{display:none}.bellows__item.bellows--is-closing>.bellows__content-wrapper,.bellows__item.bellows--is-open>.bellows__content-wrapper{display:block}.bellows__content-wrapper{display:none}.How_It_works{text-align:center}.How_It_works img{max-width:100%}.what_is_wagTag{padding:10px;border-bottom:1px solid #ddd}.what_is_wagTag:last-child{border-bottom:0}.wagtag_textheading{font-size:12px;font-weight:700;margin-bottom:5px}.wagtag_tex{font-size:14px}.compare_wagTag{text-align:center;padding:10px}.compare_wagTag img{max-width:100%;height:auto}.bellows__content p{font-size:13px;padding:10px}.bellowsheader{background:#fff;border-width:0 0 1px;color:#000;padding:13px 10px;position:relative} .header_gray_bg{ background:#f4f4f4;} .header_gray_brd{border-bottom:1px solid #ddd;}/****wagtag-accordion.css end */
.un_pub_dog{border: 1px solid #cecece; font-size: 18px; background: #d4d4d4; padding: 10px 18px; position: relative; border-radius: 3px 3px 0px 0px; border-bottom: 0;}
.myDogPic{ float:left; width:40%;}
.myDogPic img { width:80px; height:80px; border-radius:100%; border:1px solid #ddd !important;}
.myDogsName a{float:left; width:60%; text-align:left; font-size:16px; font-weight:bold; text-decoration:none; color:#000; vertical-align:middle; }
.wt_verifyDiv{padding: 10px 5px; height: 20px; float: left; width: 20px;  display: block; float: left; width: 100%; overflow: hidden; padding-bottom: 20px;}
</style>
</body>
<!--wag tag wrapper-->
<?php require_once('common/bottom.php'); ?>

</body>
</html>
