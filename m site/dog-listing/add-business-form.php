<?
	/*ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);*/
	
	$Document = "/home/dogspot/domains/m.dogspot.in/public_html";
	require_once("../constants.php");
	require_once(SITEMAIN_URL . "/database.php");
	require_once(SITEMAIN_URL . "/check_img_function.php");
	require_once(SITEMAIN_URL . "/functions.php");
	require_once(SITEMAIN_URL . "/functions2.php");
	require_once(SITEMAIN_URL . '/shop/arrays/shop-categorys.php');
	require_once(SITEMAIN_URL . '/session.php');

	
	$user_ip1 = ipCheck();			//Returns Double IP 180.151.195.146, 107.167.181.64
	$user_ip_arr = explode(",",$user_ip1);
	$user_ip = $user_ip_arr[0];
	
	if($_POST['submit']){
		$businessName = $_POST['business_name'];
		$bus_nicename = createSlug($businessName);
		$businessCategory = $_POST['business_category'];
		if($businessCategory){
			$businessCatDetails = explode("@@",$businessCategory);
			$bus_cat_nicename = $businessCatDetails[0];
			$bus_cat_name = $businessCatDetails[1];
		}
		$phoneNo = $_POST['phone_no'];
		$altPhoneNo = $_POST['alt_phone_no'];
		$landLineNo = $_POST['land_line_no'];
		$weblink = $_POST['weblink'];
		$emailID = $_POST['emailID'];
		$street = $_POST['street'];
		$address = $_POST['address'];
		$pincode = $_POST['pincode'];
		
		$contact_person = $_POST['contact_person'];
		
		$city = $_POST['city'];
		if($city!='-1'){
			$cityDetails = explode("@@@",$city);
			$city_id = $cityDetails[0];
			$city_name = $cityDetails[1];
			$city_nicename = $cityDetails[2];
		}else{
			$city_id = "-1";
			$city_name = $_POST['cityBox'];
			$city_nicename = createSlug($_POST['cityBox']);	
		}
		
		$state = $_POST['state'];
		if($state!='-1'){
			$stateDetails = explode("@@@",$state);
			$state_id = $stateDetails[0];
			$state_name = $stateDetails[1];
			$state_nicename = $stateDetails[2];
		}else{
			$state_id = "-1";
			$state_name = $_POST['stateBox'];
			$state_nicename = createSlug($_POST['stateBox']);			
		}
		
		$desc = $_POST['desc'];
		
		$start_hrs = $_POST['start_hrs'];
		$start_minutes = $_POST['start_minutes'];
		$bus_time_from = $start_hrs.":".$start_minutes;
		
		$end_hrs = $_POST['end_hrs'];
		$end_minutes = $_POST['end_minutes'];	
		$bus_time_to = $end_hrs.":".$end_minutes;
		$bus_start_day = $_POST['start_day'];
		$bus_end_day = $_POST['end_day'];
		
		$upload_location = $DOCUMENT_ROOT."/dog-listing/images/business_logo/";
		if ($_FILES['upload_logo']['name']) {
			$name = str_replace(' ', '_', $_FILES['upload_logo']['name']);
			$name = strtolower($name);
			$size = $_FILES['upload_logo']['size'];
			$tmp  = $_FILES['upload_logo']['tmp_name'];
			if($name){
				
				$image_info   = getimagesize($_FILES["upload_logo"]["tmp_name"]);
				$image_width  = $image_info[0];
				$image_height = $image_info[1];
				if (move_uploaded_file($tmp, $upload_location.'200x200_'.$name)){
					$file_array = array(
						'file_path' => $upload_location,
						'new_name' => '200x200_'.$name,
						'temp_name' => $_FILES['upload_logo']['tmp_name']
					);
					$img = '200x200_'.$name;
					$return = checkImgType($file_array);
					if ($return['status'] == "Safe") {
						$SQL = "INSERT INTO business_listing(userid, comp_org, bus_nicename, street_address, address, city, city_name, city_nicename, state_id, state_name, state_nicename, zip, mobile_main, mobile_alt, phone_main, email, web_site, about_us, bus_start_day, bus_end_day, bus_time_from, bus_time_to, c_date, cat_nice_name1, category_name1, user_ip, logo, contact_person) VALUES ('$userid', '$businessName', '$bus_nicename', '$street', '$address', '$city_id', '$city_name', '$city_nicename', '$state_id', '$state_name', '$state_nicename', '$pincode', '$phoneNo', '$altPhoneNo', '$landLineNo', '$emailID', '$weblink', '$desc', '$bus_start_day', '$bus_end_day', '$bus_time_from', '$bus_time_to', NULL, '$bus_cat_nicename', '$bus_cat_name', '$user_ip', '$img', '$contact_person')";
						mysql_query($SQL);
						$insertedID = mysql_insert_id();
						header('location:'.$bus_cat_nicename."/".$insertedID);
					}
				}else{
					echo "Error in Uploading Image. Please try again!";
				}
			}
		}
	}	
?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="canonical" href="https://m.dogspot.in/dog-listing/add-business-form.php" />
    <?php require_once(SITEMAIN_URL.'/common/script.php'); ?>
    <title>Add your Dog Business | Dog Business Listing | DogSpot</title>
    <meta name="keywords" content="Add your Dog Business | Dog Listig form" />
    <meta name="description" content=" Pet Business Form, Add your Dog Business, Dog Listig form" />
    <link href="css/classifieds.css?wq=3" type="text/css" rel="stylesheet">
   	<script language="javascript" type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
	<script language="javascript" type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>
    <style>
    	.error{ color:red; font-size:13px !important;}
    </style>
    <script>
	function checkState(){
		var state=document.add_business.state.value;
		//alert(state);
		if(state == "-1"){
			document.getElementById('stateDropDownList').style.display = "none";
			document.getElementById('stateInputBox').style.display = "block";
			$('#stateBox').attr('required', 'required');
		}	
	}
	
	function checkCity(){
		var city=document.add_business.city.value;
		//alert(city);
		if(city == "-1"){
			document.getElementById('cityDropDownList').style.display = "none";
			document.getElementById('cityInputBox').style.display = "block";
			$('#cityBox').attr('required', 'required');
		}	
	}
	
	$(function() {
    	$("#add_business" ).validate({
        	rules: {
				business_name: {
					required: true
				},
				business_category: {
					required: true
				},
				contact_person: {
					required: true
				},
				phone_no: {
					required: true,
					//phoneUK: true,  //or look at the additional-methods.js to see available phone validations
					digits: true, 
					minlength: 10,
                    maxlength: 10,
				},
				emailID: {
					required: true,
					email: true
				},
				street: {
					required: true
				},
				address: {
					required: true
				},
				pincode: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    digits: true
                },
				alt_phone_no: {
                    minlength: 10,
                    maxlength: 10,
                    digits: true
                },
				land_line_no: {
                    minlength: 10,
                    maxlength: 10,
                    digits: true
                },
				state: {
					required: true 
				},
				city: {
					required: true
				},
				desc: {
					required: true
				},
				start_hrs: {
					required: true
				},
				start_minutes: {
					required: true
				},
				end_hrs: {
					required: true
				},
				end_minutes: {
					required: true
				},
				upload_logo: {
					required: true,
					file: true
				},
				country1: {
					required: true,
				},
				start_day: {
					required: true,
				},
				end_day: {
					required: true,
				}
	    	},
    	});
	});
	</script>
    
    <?php require_once(SITEMAIN_URL.'/common/top.php'); ?>
    <div data-role="content" class="ui-content cfieds_form_blk" role="main">
		<div class="myDogsBlock_wrp" style="margin:10px;">
			<div class="myDogText"><h1 style="font-weight:normal;">
            Add your Dog Business</h1><span class="triangle_down"></span></div>
            <form action="" method="post" name="add_business" id="add_business" enctype="multipart/form-data" data-ajax="false">
			<div class="contest_uploadblk myDogText_content">
				<div class="cfieds_form_div">
                	<input type="text" name="business_name" id="business_name"  placeholder="Name of the Business">
                    <span id="name" style="color:red"></span>
                </div>
				<div class="cfieds_form_div">
                	<select name="business_category" id="business_category">
                    	<option value="">Select Category</option>
                        <option value="kennel-owner-breeder@@Breeder/Kennels">Breeder/Kennels</option>
                        <option value="services@@Grooming">Grooming</option>
                        <option value="kennel-club@@Kennel Club">Kennel Club</option>
                        <option value="veterinarian@@Veterinarian">Veterinarian</option>
                        <option value="pet-boarding@@Boarding">Boarding</option>
                        <option value="dog-trainer@@Dog Trainer">Dog Trainer</option>
                        <option value="pet-shop@@Pet Shop">Pet Shop</option>
                        <option value="others@@Other">Other</option>
					</select>
				</div>
				<div class="cfieds_form_div">
                	<input type="text" placeholder="Contact Person's Name" value="" name="contact_person" id="contact_person"/><span id="contact_person"></span>
                </div>
                <div class="cfieds_form_div">
                	<input type="text" placeholder="Mobile" value="" name="phone_no" id="phone_no"/><span id="msg_phone"></span>
                </div>
       		
            	<div class="cfieds_form_div">
                	<input type="text"  placeholder="Alternative Mobile" value="" name="alt_phone_no" id="alt_phone_no"/><span id="msg_phone_alt"></span>
				</div>
                
                <div class="cfieds_form_div">
                	<input type="text"  placeholder="Land Line Number" value="" name="land_line_no" id="land_line_no" /><span id="msg_land"></span>
				</div>
       		
            	<div class="cfieds_form_div">
                	<input type="text"  placeholder="Website link" value="" name="weblink" id="weblink" /><span id="message"></span>
				</div>
       		
            	<div class="cfieds_form_div">
                	<input type="text"  placeholder="Email ID" value="" name="emailID" id="emailID" /><span id="message"></span>
				</div>
			
            	<div class="cfieds_form_div"><input type="text"  placeholder="Street Address" value="" name="street" id="street" /></div>
                
            	<div class="cfieds_form_div"><input type="text"  placeholder="Address" value="" name="address" id="address" /></div>
       		
            	<div class="cfieds_form_div"><input type="text"  placeholder="Pin Code" value="" name="pincode" id="pincode" /><span id="message"></span></div>
         	
            	<div class="cfieds_form_div">
                	<select name="country1" id="country1">
						<option value="">Select Country</option>
                        <option value="IN@@@INDIA@@@india" selected="selected">INDIA</option>
                    </select>
                </div>
 
            	<div class="cfieds_form_div" id="stateDropDownList">
                	<select name="state" id="state" onChange="javascript: checkState();">
						<option value="">Select State</option>
                        	<? 
								$sql_state = mysql_query("SELECT * FROM state ORDER BY state_name ASC");
								if(mysql_num_rows($sql_state) > 0){
							?>
							<? while($rowState = mysql_fetch_array($sql_state)){?>
                            <option value="<?=$rowState["state_id"]."@@@".$rowState["state_name"]."@@@".$rowState["state_nicename"]; ?>"><?=$rowState["state_name"]; ?></option>		
                            <? } } ?>
                            <option value="-1">Other</option>
					</select>
                </div>
                
                  <div class="cfieds_form_div" id="stateInputBox" style="display:none;"><input type="text"  placeholder="Enter Other State" value="" name="stateBox" id="stateBox"/></div>
                  
                <div class="cfieds_form_div" id="cityDropDownList">
                	<select name="city" id="city" onChange="javascript: checkCity();">
						<option value="">Select City</option>
                        <? 
							$sql_city = mysql_query("SELECT * FROM city ORDER BY city_name ASC");
							if(mysql_num_rows($sql_city) > 0){
						?>
                       	<? while($rowCity = mysql_fetch_array($sql_city)){?>
						<option value="<?=$rowCity["city_id"]."@@@".$rowCity["city_name"]."@@@".$rowCity["city_nicename"]; ?>"><?=$rowCity["city_name"]; ?></option>		
						<? } }?>
                        <option value="-1"> Other </option>
                    </select>    
                </div>
                
         		 <div class="cfieds_form_div" id="cityInputBox" style="display:none;"><input type="text"  placeholder="Enter Other City" value="" name="cityBox" id="cityBox"/></div>
                 
                 
                <div class="fileUpload btn btn-primary">
                    <span>Upload Business Logo</span>
                    <input type="file" accept="image/*" name="upload_logo" id="upload_logo" class="upload" value="Upload Business Logo"/>
                </div>
				<div id="image-holder" class="image-holder"></div>
          		
                <div class="cfieds_form_div"><textarea placeholder="About Your Business" name="desc" id="desc" /></textarea></div>
                
                <div><strong>Business Days</strong></div>
                <div>
                	<span style="float:left">From</span>
                	<span style="float:right; margin-right: 50%">To</span>
                </div>
                <div class="clsfeds_time_form">
                    <div class="cfieds_form_div clsfeds_time_form_div">
                        <select name="start_day" id="start_day">
                            <option value="">Days</option>
                            <option value="monday">Monday</option>
                            <option value="tuesday">Tuesday</option>
                            <option value="wednesday">Wednesday</option>
                            <option value="thursday">Thursday</option>
                            <option value="friday">Friday</option>
                            <option value="saturday">Saturday</option>
                            <option value="sunday">Sunday</option>
                        </select>
                    </div>
                    <div class="cfieds_form_div clsfeds_time_form_div">
                        <select name="end_day" id="end_day">
                            <option value="">Days</option>
                            <option value="monday">Monday</option>
                            <option value="tuesday">Tuesday</option>
                            <option value="wednesday">Wednesday</option>
                            <option value="thursday">Thursday</option>
                            <option value="friday">Friday</option>
                            <option value="saturday">Saturday</option>
                            <option value="sunday">Sunday</option>
                        </select>
                    </div>
                </div>
                
                
                <div style="width:100%; float:left"><strong>Business Timing</strong></div>
                <div style="width:100%; float:left"><span>From</span></div>
                <div class="clsfeds_time_form">
                    <div class="cfieds_form_div clsfeds_time_form_div">
                        <select name="start_hrs" id="start_hrs">
                            <option value="">Hours</option>
                            <? for($i=1; $i<=12; $i++){?>
                            <option value="<? printf("%02d", $i);?>"><? printf("%02d", $i);?></option>
                            <? }?>
                        </select>
                    </div>
                    <div class="cfieds_form_div clsfeds_time_form_div">
                        <select name="start_minutes" id="start_minutes">
                            <option value="">Minutes</option>
                            <? for($i=0; $i<=59; $i++){?>
                            <option value="<? printf("%02d", $i);?>"><? printf("%02d", $i);?></option>
                            <? }?>
                        </select>
                    </div>
                </div>
                <div style="width:100%; float:left"><span>To</span></div>
                <div class="clsfeds_time_form">
                    <div class="cfieds_form_div clsfeds_time_form_div">
                        <select name="end_hrs" id="end_hrs">
                            <option value="">Hours</option>
                            <? for($i=1; $i<=12; $i++){?>
                            <option value="<? printf("%02d", $i);?>"><? printf("%02d", $i);?></option>
                            <? }?>
                        </select>
                    </div>
                    <div class="cfieds_form_div clsfeds_time_form_div">
                        <select name="end_minutes" id="end_minutes">
                            <option value="">Minutes</option>
                            <? for($i=0; $i<=59; $i++){?>
                            <option value="<? printf("%02d", $i);?>"><? printf("%02d", $i);?></option>
                            <? }?>
                        </select>
                    </div>
                </div>
          		<div class="cfieds_form_div"><input type="submit" name="submit" id="submit" value="Submit" onClick="return checkSignUP();" /></div>
       		</div>
            </form>
        </div>
	</div>
    <script type="text/javascript">
		$("#upload_logo").on('change', function () {
			//Get count of selected files
			var countFiles = $(this)[0].files.length;
			var imgPath = $(this)[0].value;
			var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
			var image_holder = $("#image-holder");
			image_holder.empty();
			if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg"){
				
				if (typeof (FileReader) != "undefined"){
					//loop for each file selected for uploaded.
					for (var i = 0; i < countFiles; i++){
						var reader = new FileReader();
						reader.onload = function (e){
							$("<img />", {
								"src": e.target.result,
								 "class": "thumb-image"
							}).appendTo(image_holder);
						}
						image_holder.show();
						reader.readAsDataURL($(this)[0].files[i]);
					}
				}else{
					alert("This browser does not support FileReader.");
				}
			}else{
				alert("Pls select only images");
			}
		});
	</script>
<?php require_once(SITEMAIN_URL.'/common/bottom.php'); ?>
<!-- cart page start--> 
<!-- cart page end--> 