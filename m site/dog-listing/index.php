<?php 
if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<title>Dog Business Listing | DogSpot</title>
<meta name="keywords" content="Kennel Owner Breeder, Pet Shop, Dog Trainer, Pet Services, Pet Boarding, Veterinarian, Kennel Club | Pet Business in India | DogSpot" />
<meta name="description" content="Pet Business Find Phone Numbers, Addresses, Best Deals, Latest Reviews & Ratings of Kennel Owner Breeder, Pet Shop, Dog Trainer, Pet Services, Pet Boarding, Veterinarian, Kennel Club | DogSpot" />
<link type="text/css" rel="stylesheet" href="css/classifieds.css?w=2302">
<link rel="canonical" href="https://www.dogspot.in/dog-listing/" />
<?php require_once('../common/script.php'); ?>
<?php require_once('../common/top.php'); ?>
</head>
<script type="application/javascript">
$(document).ready(function(){
    $("#city_search li").click(function(){
    	var city_nicename = this.id; // id of clicked li by directly accessing DOMElement property
		$('#city_name').val(city_nicename);
		$('#filter').attr('action','classifieds-list.php?city_name='+city_nicename);
		$('#filter').submit();
	});
});
</script>
<div data-role="content" class="ui-content" role="main">
<div class="clfieds_blk">
	<div class="clfieds_head">
		<div class="clfieds_search_box">
			<div class="clfieds_heading">
            <h1 style="font-weight:normal;">Find Pet Shops, Veterinarians, Kennels & Many More</h1></div>
			<ul id="city_search" data-role="listview" data-filter="true" data-filter-reveal="true" data-filter-placeholder="Search By City..." data-inset="true">
                <? 
					$sql1=mysql_query("SELECT city_nicename, city_name FROM business_listing WHERE city_name!='' GROUP BY city_name ORDER BY city_name ASC");
					while($row1=mysql_fetch_array($sql1)){
				?>
                    <li id="<?=$row1['city_nicename']?>"><?=ucfirst($row1['city_name']);?></li>
                <? } ?>
            </ul>
            <form name="filter" id="filter" method="post" data-ajax="false">
                <input type="hidden" id="city_name" name="city_name" />
            </form>
		</div>
	</div>
    <div class="header-banner" style="margin-top:10px;" >
<?php require_once($DOCUMENT_ROOT .'/header-banner.php'); ?>
   
</div>
	<!-- classifieds category start-->
	<div class="clfieds_cat">Categories</div>
	<div class="clfieds_sec">
		<div class="clfieds_list"> 
			<a href="/dog-listing/category/kennel-owner-breeder/" data-ajax="false"> <img src="images/dog-breed-icon.png" >
				<div class="clfieds_cat_name">Breeder/Kennels</div>
			</a>
		</div>
        
        <div class="clfieds_list"> 
			<a href="/dog-listing/category/services/" data-ajax="false"> <img src="images/gromming-icons.jpg">
				<div class="clfieds_cat_name">Grooming</div>
			</a>
		</div>
        
        <div class="clfieds_list"> 
			<a href="/dog-listing/category/kennel-club/" data-ajax="false"> <img src="images/kennel-club-icon.png">
				<div class="clfieds_cat_name">Kennel Club</div>
			</a>
		</div>
        
        <div class="clfieds_list"> 
			<a href="/dog-listing/category/veterinarian/" data-ajax="false"> <img src="images/vet-icons.jpg" > 
				<div class="clfieds_cat_name">Veterinarian</div>
			</a>
		</div>
        
        <div class="clfieds_list"> 
			<a href="/dog-listing/category/pet-boarding/" data-ajax="false"> <img src="images/day-care-small.png">
				<div class="clfieds_cat_name">Boarding</div>
			</a>
		</div>
        <div class="clfieds_list"> 
			<a href="/dog-listing/category/dog-trainer/" data-ajax="false"> <img src="images/trainer-small-icon.png" >
				<div class="clfieds_cat_name">Dog Trainer</div>
			</a>
		</div>
        
        <div class="clfieds_list"> 
			<a href="/dog-listing/category/pet-shop/" data-ajax="false"><img src="images/petshop-icons.jpg" >
				<div class="clfieds_cat_name">Pet Shop</div>
			</a>
		</div>
		
		
		<div class="clfieds_list"> 
			<a href="/dog-listing/category/others/" data-ajax="false"> <img src="images/other-icon.png" class="clfieds_icon_other">
				<div class="clfieds_cat_name">Other</div>
			</a>
		</div>
	</div>	
	<?php /*
	<div style="float:left; width:100%; text-align:center; margin-bottom:10px;">
  <script type="text/javascript" language="javascript">
     var aax_size='300x250';
     var aax_pubname = 'dog0bd-21';
     var aax_src='302';
   </script>
  <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>
</div> */ ?>
    
	<!-- classifieds category end--> 
	<!-- banner-->
    <div class="add_your_bs_btn" style="text-align:center; height: 20px; padding-top: 0px;"><a href="add-business-form.php" data-ajax="false">
    Add Your Business</a></div>
	<div style="float:left; width:100%;">
       <?php /*?> <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive-Head -->
<ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-3238649592700932"
    data-ad-slot="6045502591"
    data-ad-format="auto"></ins>
<script>
$(document).ready(function(){(adsbygoogle = window.adsbygoogle || []).push({})})
</script><?php */?>
<div style="text-align:center;">
<div id='div-gpt-ad-1552408890928-0' style='height:250px; width:300px;margin: 15px 24px 29px 42px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552408890928-0'); });
</script>
</div>
 </div>
        </div>
    <!-- banner end--> 
    <!--- best services sec-->
    <div class="best_services_sec" style="background:#f2f2f2; ">
        <div class="best_services_text"><? if($sessionUserCity){?>Best Services in <?=$sessionUserCity;?><? }else{ echo "Best Services"; }?></div>
            <ul class="clfieds_listing" style="text-transform:none;"> 
            <?php 
				if($sessionUserCity){
 	               	$sql = "SELECT * FROM business_listing WHERE city_nicename = '$sessionUserCity' ORDER BY business_id DESC LIMIT 5";
				}else{
					$sql = "SELECT * FROM business_listing ORDER BY business_id DESC LIMIT 5";
				}
				$selectArt = mysql_query("$sql");
                if(!$selectArt){ die(mysql_error()); }
				$ii=0;
                while($rowArt1 = mysql_fetch_array($selectArt)){
                    $ii++;$username=$rowArt1["userid"];
                    $comp_org = $rowArt1["comp_org"];
                    $address = $rowArt1["address"];
                    $address = stripslashes($address);
                    $address =  breakLongWords($address, 30, " ");
                    $city_name = $rowArt1["city_name"];
                    $country_name = $rowArt1["country_name"];
                    $bus_nicename = $rowArt1["bus_nicename"];
                    $mobile_main = $rowArt1["mobile_main"];
					
					$cat_nice_name1 = $rowArt1["cat_nice_name1"];
					$cat_nice_name2 = $rowArt1["cat_nice_name2"];
					$cat_nice_name3 = $rowArt1["cat_nice_name3"];
					$cat_nice_name4 = $rowArt1["cat_nice_name4"];
					
					$rowUser1=query_execute_row("SELECT cat_logo FROM business_category WHERE (cat_nice_name='$cat_nice_name1' OR cat_nice_name='$cat_nice_name2' OR cat_nice_name='$cat_nice_name3' OR cat_nice_name='$cat_nice_name4') LIMIT 1");
                    if($rowUser1["cat_logo"]){
                        $profileimgurl='/dog-listing/images/'.$rowUser1["cat_logo"];
                    }else{
                        $profileimgurl='/dog-listing/images/photo.jpg'; 
                    }
            ?>	 
            <li>
                <a href="/dog-listing/<?=$bus_nicename;?>/" class="ui-link" data-ajax="false">
                    <div class="clfiedsimg_blk"><img src="<?=$profileimgurl; ?>" alt="<?=$city_name; ?>" title="<?=$city_name; ?>" class="clfieds_icon_shop"></div>
                    <div class="clfiedstext_blk">
                        <div class="clfieds_right_icon"><img src="images/right-arrow.png"></div>
                        <div class="clfieds_shop_name"><? print ucfirst(breakLongWords($comp_org, 11, " "));?></div>
                        <div class="clfieds_shop_address"><img src="images/Maps-icon.png" style="margin-right:3px;">
						<? echo trim(trim($address,',')).", ".trim(trim($city_name,',')).", +91-".trim(trim($mobile_main,',')); ?></div>		
                    </div>
                </a>
            </li>
            <? if($ii==2){?>
            <div style="text-align:center">
 <div id='div-gpt-ad-1552391467223-0' style='height:50px; width:320px;margin: 24px 24px 29px 22px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552391467223-0'); });
</script>
</div></div>
            <? } } ?>
            </ul>
        </div>
	    <!-- best services box end-->
	</div>
    </div>
<?php require_once('../common/bottom-new.php'); ?>    
<?php require_once('../common/bottom.php'); ?>
<!-- cart page start--> 
<!-- cart page end--> 