<?php
require_once('constants.php'); 
require_once('database.php');
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/session.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>How Wag Tag Works | Dog Tag | DogSpot.in</title>
<meta name="keywords" content="How Wag Tag Works, Dog Tag, Wag Tag Information, Full Detail On Wag Tag" />
<meta name="description" content="How Wag Tag Works Dog Tag, Full Information on Wag Tag working" />
<?php require_once('common/script.php'); ?>

 <script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
 
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1552614-5', 'auto');
  ga('require', 'ec');
 
 ga('send', 'pageview'); 
function checksearch()
{
var searchtext=document.getElementById('searchText').value;
if(searchtext=='')
{	
	//document.getElementById('searchText').style.backgroundColor = "red";
	document.getElementById('SrchErr').innerHTML="<font color='red'>This field is required.</font>";
	document.formhgtr.searchText.focus();
	return false;
}
else
{
	return true;
}
}
</script>

<script>
(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1423135197964769']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<?php require_once('common/top.php'); ?>
</head>

<body>
<div class="wagtag_headingTop">
<h1>How to activate wag tag </h1>
</div>
<!--wag tag wrapper-->
<div id="wagtag_wrapper">
<!-- content-->
<div class="wagtag_content">
<div class="activate_wagtag">
<a href="/wagtag-info/"><img src="images/wag_tag_activate_png.png" ></a>
</div>
<div class="wagtag_btnsDiv">
<? if($userid=='Guest'){?><a href="/login.php?refUrl=/myDogs/">
<input type="button" value="Activate Wag Tag" class="activate_wagtagBtn"></a>
<? }else{?><a href="/myDogs/">
<input type="button" value="Activate Wag Tag" class="activate_wagtagBtn"></a>
<? }?>

</div>
</div>
<!--wag tag wrapper-->
</div>
<?php require_once('common/bottom.php'); ?>
</body>

</html>