<?
error_reporting(E_ERROR |E_PARSE);

require_once('constants.php'); 
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL."/database.php");
$sitesection = "shop";
$ecomm_pagetype = 'product';

$ant_section = 'Shop';
$ant_page = 'Product';

makeProperURL($requestedUrl, $requested);

$qdata=query_execute("SELECT * FROM shop_items WHERE nice_name='$section[0]' AND item_display_status!='delete'");
$rowTotal = mysql_num_rows($qdata);
if($rowTotal==0){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();
}
$rowdat = mysql_fetch_array($qdata);
$item_id = $rowdat["item_id"];
$tag = $rowdat["tag"];
$feature = $rowdat["feature"];
$type_id = $rowdat["type_id"];
$price = $rowdat["price"];
$weight = $rowdat["weight"];
$brand_id = $rowdat["item_brand"];
$stock_status = $rowdat["stock_status"];
$selling_price=$rowdat["selling_price"];
$visibility=$rowdat["visibility"];
$name=$rowdat["name"];
$item_about=$rowdat["item_about"];
$nice_name=$rowdat["nice_name"];
$item_parent_id=$rowdat["item_parent_id"];
$payment_mode=$rowdat["payment_mode"];
$ecomm_prodid = "$item_id";
$domain_id2=$rowdat["domain_id"];

if($domain_id2=='2'){
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://m.dogspot.in/");
	}

$cemantitemid[]=$rowdat['item_id'];
//$upView=query_execute("UPDATE shop_items SET num_views = num_views+1 WHERE item_id='$item_id'");

$qCat=query_execute("SELECT category_id FROM shop_item_category WHERE item_id='$item_id' AND category_id!=0");
$rowCat = mysql_fetch_array($qCat);
$category_id = $rowCat["category_id"];

$qParentCat=query_execute("SELECT * FROM shop_category WHERE category_id='".$category_id."'"); //NKS
$rowParentCat = mysql_fetch_array($qParentCat); //NKS
$category_parent_id = $rowParentCat["category_parent_id"];
$category_nicename = $rowParentCat["category_nicename"];
$category = $rowParentCat["category_name"];

$qParentCatNice=query_execute("SELECT category_nicename FROM shop_category WHERE category_id='".$category_parent_id."'"); //NKS
$rowParentCatNice = mysql_fetch_array($qParentCatNice); //NKS
$catParentNiceName = $rowParentCatNice["category_nicename"];
//echo 'id:'.$category_id.' pid:'.$category_parent_id.''.$item_id; //NKS
$ant_category = "$category";

$qBrand=query_execute("SELECT brand_name FROM shop_brand WHERE brand_id='$brand_id'");
$rowBrand = mysql_fetch_array($qBrand);
$brandName=$rowBrand["brand_name"];
// Get all images
	$sqlmediaco1 = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
	$trecount1=mysql_fetch_array($sqlmediaco1);   
    if ($trecount1['trecd'] != '0')
    							{
								$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1";
								}
							else{
								$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1";
								}	
$qdataM=query_execute("$sqlmedia");
$rowdatM = mysql_fetch_array($qdataM);
	
	if($rowdatM["media_file"]){
		$src = SITEMAIN_URL.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL='/imgthumb/200x200-'.$rowdatM["media_file"];
	}else{
		$src = SITEMAIN_URL.'/shop/image/no-photo-t.jpg';
		$imageURL='/shop/image/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'200','200','ratiowh');
	$catName=$ArrayShopCategorys[$rowCat["category_id"]];
$pageHone=$rowdat["name"];
$art_body = stripslashes($rowdat["item_about"]);
$art_body = strip_tags($art_body);
$art_body = trim($art_body);
$art_body = substr($art_body,0,240);
$art_body = stripslashes($art_body);

$pageTitle=$pageHone.', '.$catName;
$pageKey=$pageHone.', '.$brandName.', '.$brandName.' '.$catName.', '.$catName;
$pageDesc=$rowdat["short_description"];
$pageDesc=stripslashes($pageDesc);
	$pageDesc = preg_replace('/\"/', ' ', $pageDesc);
	$pageTitle = preg_replace('/\"/', ' ', $pageTitle);
	$pageKeyword = preg_replace('/\"/', ' ', $pageKeyword);
	$pageHone = preg_replace('/\"/', ' ', $pageHone);
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?=$pageTitle?></title>
<meta name="description" content="<? echo 'Buy '. $pageHone. ' online by '. $brandName.' Brand at reasonable Price. Order '. $catName.' from DogSpot.in. Available ✓Cash on Delivery ✓30 Days Return.' ?>" />
<meta name="keywords" content="<?=$pageKey;?>" />
<link href="https://www.dogspot.in/<?=$section[0]?>/" rel="canonical">

<script type="text/javascript">
function qtycheck()
{
	var qty=document.getElementById('item_qty').value;
	if(qty>='1')
	{
		document.getElementById('show').style.display='none';
		return true;
	}else
	{
		document.getElementById('show').value='This field is required';
		document.getElementById('show').style.display='inline';
		return false;
	}
}</script>


<?php require_once('common/top.php'); ?>
<!-- breadcrumbs code start -->
<div class="linkContainer"><a href="/">Online Pet Shop</a> » <a href="/<?=$catParentNiceName?>/"><?=$ArrayShopCategorys[$category_parent_id]?></a><? if($ArrayShopCategorys[$category_parent_id]==''){ echo $ArrayShopCategorys[$category_id]; }?> » <?=ucwords($name);?></div>
<!-- breadcrumbs code end -->
<div class="matterContainer">
<form name="form2" method="post" onSubmit="return qtycheck()" enctype="multipart/form-data" action="/cart.php" >
<div class="productRow"><a href="#"><img src="<?=$imageURL?>" alt="<?=$rowdatM["label"]?>" title="<?=$rowdatM["label"]?>"/></a>
      <p><a href="#"><?=$name?></a></p>
      <span>Price: Rs. <?=number_format($price)?> &nbsp;<del><? if($selling_price>$price)
	  {echo 'Rs. '.number_format($selling_price); }?></del></span>
      <br />
<input name="itembasceprice" type="hidden" id="itembasceprice" value="<?=$price?>" />
<input name="item_id" type="hidden" id="item_id" value="<?=$item_id;?>" />
<input name="item_price" type="hidden" id="item_price" value="<?=$price?>" />
<input name="type_id" type="hidden" id="type_id" value="<?=$type_id?>" />
<input name="ItemOption" type="hidden" id="ItemOption" value="" />
<? if($stock_status=='instock'){
	$ecomm_totalvalue = number_format($price,2);
?>
<span>Qty: </span><input name="item_qty" type="text" class="ShopQty required" style="text-align:center; width:20px;" id="item_qty" value="1" />
<p><span style="float:left;"><font size='2'>Cash On Delivery: <? if($payment_mode!='cod'){?><font color="#FF0000">Not Available</font><? }else{?><font color="#33CC33">Available<? }?>
</font></font>
  </span></p>
     <? if($tag=='1'){
 if($cart_value)
 { 
 $cart=explode("@@",$cart_value);
 
 $cart_show_id=$cart[0];
  $p_det=query_execute("SELECT * FROM shop_cart_meta WHERE cart_id ='$cart_show_id'");
  $count=mysql_num_rows($p_det);
 $show=$cart[1];
 $max=$show-$count;
	// echo "SELECT * FROM shop_cart_meta WHERE cart_id =$cart_value";
 $p_det=query_execute("SELECT * FROM shop_cart_meta WHERE cart_id ='$cart_show_id' AND id BETWEEN $max AND $show");
 $p_deter=query_execute_row("SELECT item_qty FROM shop_cart WHERE cart_id ='$cart_show_id'");
 while($p_det1=mysql_fetch_array($p_det))
 {
	 $p_name[]=$p_det1["cart_meta_name"];
 	 $cart_meta_value[]=$p_det1["cart_meta_value"];
 }
 }?>
     <table>
     <input type="hidden" name="show" id="show"   value="<?=$show?>"  />
     <?  if($feature=='cake'){?>
    <tr><td>Flavour Options:</td><td>
    <select name="flavour" id="flavour" class="ShopQty required" required style="width: 133px;margin-left: 2px;">
    <option value="">Choose Flavour</option>
     <? if($item_id=='8388' || $item_id=='8380'){?>
     <option value="Mango" <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Mango"){  ?> selected="selected"<? }?> >Mango</option>
      <option value="Paw-berry"  <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Paw-berry"){  ?>selected="selected"<? }?>>Paw-berry</option>
       <option value="Pineapple"  <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Pineapple"){  ?>selected="selected"<? }?>>Pineapple</option>
       <option value="Banana"  <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Banana"){  ?>selected="selected"<? }?>>Banana</option> <? }else{?>
     <option value="Apple" <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Apple"){  ?> selected="selected"<? }?> >Apple</option>
      <option value="Banana"  <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Banana"){  ?>selected="selected"<? }?>>Banana</option>
       <option value="Carob"  <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Carob"){  ?>selected="selected"<? }?>>Carob (Looks like chocolate)</option>
       <option value="Pumpkin"  <? 
	 $f=array_search("flavour",$p_name);
	 if($cart_meta_value[$f]=="Pumpkin"){  ?>selected="selected"<? }?>>Pumpkin</option>
       <? }?> 
     </select></td></tr>
     <tr><td>Shape Options:</td><td>
    <select name="shape" id="shape" class="ShopQty required" required style="width: 133px;margin-left: 2px;">
    <option value="">Choose Shape</option>
     <option value="Bone" <? 
	 $f=array_search("shape",$p_name);
	 if($cart_meta_value[$f]=="Bone"){  ?> selected="selected"<? }?>>Bone Shaped</option>
      <option value="Paw" <? 
	 $f=array_search("shape",$p_name);
	 if($cart_meta_value[$f]=="Paw"){  ?> selected="selected"<? }?>>Paw Shaped</option>
       <option value="Circular" <? 
	 $f=array_search("shape",$p_name);
	 if($cart_meta_value[$f]=="Circular"){  ?> selected="selected"<? }?>>Circular</option>
       <option value="Heart" <? 
	 $f=array_search("shape",$p_name);
	 if($cart_meta_value[$f]=="Heart"){  ?> selected="selected"<? }?>>Heart Shaped</option>
     </select></td></tr>
     <? }?>
      <? if($feature=='imagedate'){?>
      <tr><td>Name to be Printed:</td>
     <td><input type="text" name="pet_name" id="pet_name" maxlength="16"  placeholder="e.g. Sahil & Bozo"   value="<? 
	 $f=array_search("NAME",$p_name);
	 if($p_name[$f]=="NAME"){ echo $cart_meta_value[$f];}?>" required onkeyup="javascript:valid1(this);" /></td></tr>
      <? }elseif($feature=='cake'){
		 ?><tr><td>Name:</td>
     <td><input type="text" name="pet_name" id="pet_name" maxlength="16" style="width:132px;"   value="<? 
	 $f=array_search("NAME",$p_name);
	 if($p_name[$f]=="NAME"){ echo $cart_meta_value[$f];}?>"  onkeyup="javascript:valid1(this);" /></td></tr><? 
	 }else{?>
     <tr><td> Pet's Name</td>
     <td><input type="text" name="pet_name" id="pet_name" maxlength="16"  value="<? 
	 $f=array_search("NAME",$p_name);
	 if($p_name[$f]=="NAME"){ echo $cart_meta_value[$f];}?>" required onkeyup="javascript:valid1(this);" /></td></tr>
<? }?>
<? if($feature!='image' && $feature!='imagedate' && $item_id!='8168' && $item_id!='8169' && $item_id!='8170' && $feature!='cake'){?>
     <tr><td>Mobile no.</td><td><input type="text" name="pet_phone" id="pet_phone"  value="<? 
	 $f1=array_search("PHONE",$p_name);
	 if($p_name[$f1]=="PHONE"){ 
echo $cart_meta_value[$f1];
}?>" maxlength="10" minlength="10" 
     onkeyup="javascript:valid(this);" required/></td></tr><? }?>
        <? if($feature=='alpha'){?>
    <tr><td>Alphabet</td><td>
    <select name="alpha" id="alpha" class="ShopQty required" required>
     <option value="">Choose Alphabet</option>
    <? $letters = range('A', 'Z');
	 foreach ($letters as $one){?>
     <option value="<?=$one?>" 
<? 
$f2=array_search("feature",$p_name);
if($p_name[$f2]=="feature"){ 
if($cart_meta_value[$f2]==$one){?>
 selected="selected"
<? } }?>
    ><?=$one?></option><? }?>
     </select></td></tr>
     <? }
	 if($feature=='image' || $feature=='imagedate')
	 { if($feature=='image'){?>
     <tr><td>Size</td><td>
     <select name="product_d" id="product_d" class="ShopQty required" style="width: 133px;margin-left: 2px;">
    <option value="">Choose Layout: </option>
     <option value="best" <? $f3=array_search("Design",$p_name);
	if($cart_meta_value[$f3]=='best'){ echo "checked='checked'";}else{echo "checked='checked'";}?>>Best fit</option>
      <option value="landscape" <? $f3=array_search("Design",$p_name);
	if($cart_meta_value[$f3]=='landscape'){ echo "checked='checked'";}else{echo "checked='checked'";}?>>landscape</option>
       <option value="portrait" <? $f3=array_search("Design",$p_name);
	if($cart_meta_value[$f3]=='portrait'){ echo "checked='checked'";}else{echo "checked='checked'";}?>>portrait</option>
     </select>
    </td></tr><? }else{?>
     <tr><td>Print Date:</td><td><input type="text" name="printeddate" <? $f4=array_search("printdate",$p_name);?>
          value="<?=$cart_meta_value[$f4]?>" id="printeddate" placeholder="e.g. August 2014" required
         /></td></tr>
    <? }?>
     <tr><td>Image</td><td>
	  <input type="file" name="productimage" id="productimage" required />&nbsp;&nbsp;
        <div id="output"></div>
        </td></tr>
	<? }
	 ?>
     <input type="hidden" name="cart_value" id="cart_value" value="<?=$cart_show_id?>"  />
      <input type="hidden" name="user" id="user" value="<?=$userid?>"  />
     </table>
     <? }?> <!--  tag code end -->
<p>&nbsp; </p>
<br>
<div id='show'></div>

<input name="addtoItemCart" type="image" id="addtoItemCart" style="border:0px; margin-top:10px;" src="/images/buy-now.jpg" alt="Submit"/>
<? }else{?><span style="color:#F00; font-size:18px">Sold Out</span><? }?>
<div class="cb"></div>
</div>
</form>
<div class="textWrapper"><?= $item_about?></div>
</div>
 <?php require_once('common/bottom.php'); ?>