<?php
if($section[0]==''){
	require_once("../constants.php");
}
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>What is Wag Tag | Dog Tag | DogSpot.in</title>
<meta name="keywords" content="What is Wag Tag, Dog Tag, Wag Tag Information, Full Detail On what is Wag Tag" />
<meta name="description" content="What is Wag Tag, Dog Tag, know full information about Wag Tag" />
<link rel="canonical" href="https://www.dogspot.in/wagtag/what-is-wag-tag.php" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.dogspot.in/wagpage.php" />
<link rel="stylesheet" href="fonts.css" type="text/css" charset="utf-8" />


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>

<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<link rel="stylesheet" href="css/style.css?=v40100011">
<section class="wagtag-section">	
    <div class="container-fluid" style="padding:0;position: relative;">
			<img src="images/wagtag-banner.png" class="img-responsive hidden-xs tag-icon-main">
			<img src="images/wagtag-banner-1.png" class="img-responsive visible-xs tag-icon-mo">
			<img src="images/tag-icon.png" class="img-responsive hidden-xs tag-icon">
		</div>
        <div class="main-tag-menu">
    <ul class="tag-menu hidden-xs">
      <li><a data-ajax="false" style="font-weight:600;" href="/wagtag/index-new.php"  >Activate WAGTAG</a></li>
      <li><a data-ajax="false"  href="/wagtag/what-is-wag-tag-new.php" >What is WAGTAG?</a></li>
      <li><a data-ajax="false" class="active" href="/wagtag/how-it-works-new.php" >How does it work?</a></li>
      <li><a  data-ajax="false" href="/wagtag/faq-new.php" >FAQs</a></li>
      <li><a data-ajax="false" href="/wag_club/" >Wag Club</a></li>
    </ul>
    <ul class="tag-menu-mo visible-xs">
      <li><a data-ajax="false" href="/wagtag/index-new.php"  >Activate WAGTAG</a></li>
      <li><a data-ajax="false" href="/wagtag/what-is-wag-tag-new.php" >What is WAGTAG?</a></li>
      <li><a data-ajax="false"  href="/wagtag/how-it-works-new.php"  class="active" >How does it work?</a></li>
      <li><a data-ajax="false" href="/wagtag/faq-new.php" >FAQs</a></li>
      <li style="border-right:none;"><a data-ajax="false" href="/wag_club/" >Wag Club</a></li>
    </ul>
  </div>
        
        <div class="container" style="margin-top:10px;">

<h1 style="text-align: center;
    font-size: 26px;
    color: #6c9d06;
    padding: 15px 0">How it works?</h1>
<img src="/wagtag/images/tag-cycle.jpg" alt="how-it-works" title="how-it-works" class="img-responsive" style="margin:auto">
</div>
        
        
        
        
        </section>
        

<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>s