<?php
require_once("../constants.php");
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/session.php');
?>
<!DOCTYPE html>
<html>
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<?php require_once($DOCUMENT_ROOT . '/common/script.php'); ?>
<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
<?php require_once($DOCUMENT_ROOT . '/common/top.php');?>
<script type='text/javascript' src='https://www.dogspot.in/js/shaajax2.1.js'></script>
<script>
//Ask Question Jquery Alert And Function Call By Id By Bharat
$(document).ready(function(e) {
	$("#qnaclickdet").click(function(){
		$( "#qnaopenbox" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});
$("#qtnbox").click(function(){
	$("#userpro").css("display", "none");
});
//Show Validation Errors By Bharat
$("#btsubmit").click(function(){
var qtn = $("#qtnbox").val();
var qna_catid = $("#qna_catid").val();
var cs = $("#qtnbox").val().length;	
if(qtn!=''){
	if(cs<50){
	alert('Please input minimum 50 characters.');
	return false;} }	
if(qtn=='' && qna_catid!=''){
	$("#qtnans").css("display", "block");
	$("#qtnblank").css("display", "none");
	$("#qtncat").css("display", "none");
	$("#qtncharerror").css("display", "none");
	$("#userpro").css("display", "none");
		}else if(qna_catid=='' && qtn!=''){
		$("#qtncat").css("display", "block");
		$("#qtnans").css("display", "none");
		$("#qtnblank").css("display", "none");
		$("#qtncharerror").css("display", "none");
		$("#userpro").css("display", "none");
		}else if(qna_catid=='' && qtn==''){
		$("#qtnblank").css("display", "block");
		$("#qtncat").css("display", "none");
		$("#qtnans").css("display", "none");
		$("#qtncharerror").css("display", "none");
		$("#userpro").css("display", "none");
		}
	else{
	var qna_catid=$("#qna_catid").val();
	var qtnbox=$("#qtnbox").val();
	var qnaopenbox=$("#qnaopenbox").val();
	var user_id=$("#user_id").val();
ShaAjaxJquary('/qna/insert-data.php?qna_catid='+qna_catid+'&qtnbox='+qtnbox+'&qnaopenbox='+qnaopenbox+'&user_id='+user_id+'', "#userpro", '', '', 'POST', "#userpro", 'checking...', 'REP');
		<? setcookie ("qnavallk", "", time() - 3600);?>
		$("#qtnblank").css("display", "none");
		$("#qtncat").css("display", "none");
		$("#qtnans").css("display", "none");
		$("#qtncharerror").css("display", "none");
		$('#qna_catid').val("");
		$('#qtnbox').val("");
		$('#qnaopenbox').val("");
	}
});
$("#qnaopenbox").click(function(){
$("#qtncharerror").css("display", "none");	 
});
});
</script>
<!--character count-->
<script >
(function($){  
	$.fn.textareaCount = function(options, fn) {   
		var defaults = {  
			maxCharacterSize: -1,  
			originalStyle: 'originalTextareaInfo',
			warningStyle: 'warningTextareaInfo',  
			warningNumber: 20,
			displayFormat: '#input characters | #words words'
		};  
		var options = $.extend(defaults, options);
		
		var container = $(this);
		var fgtr = $("#erty");
		
		$("<div class='charleft'>&nbsp;</div>").insertAfter(fgtr);
		
		//create charleft css
		var charLeftCss = {
			'width' : fgtr.width()
		};
		
		var charLeftInfo = getNextCharLeftInformation(fgtr);
		charLeftInfo.addClass(options.originalStyle);
		charLeftInfo.css(charLeftCss);
		
		var numInput = 0;
		var maxCharacters = options.maxCharacterSize;
		var numLeft = 0;
		var numWords = 0;
				
		container.bind('keyup', function(event){limitTextAreaByCharacterCount();})
				 .bind('mouseover', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);})
				 .bind('paste', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);});
		
		
		function limitTextAreaByCharacterCount(){
			charLeftInfo.html(countByCharacters());
			//function call back
			if(typeof fn != 'undefined'){
				fn.call(this, getInfo());
			}
			return true;
		}
		
		function countByCharacters(){
			var content = container.val();
			var contentLength = content.length;
			
			//Start Cut
			if(options.maxCharacterSize > 0){
				if(contentLength > 99){
							$( "#qnaopenbox" ).slideDown( "slow", function() {
    // Animation complete.
  });	
    $("#qtncharerror").css("display", "block");
  		$("#qtnblank").css("display", "none");
		$("#qtncat").css("display", "none");
		$("#qtnans").css("display", "none");		
				}else{
					$("#qnaopenbox").css("display", "none");
					$("#qtncharerror").css("display", "none");
					}
				//If copied content is already more than maxCharacterSize, chop it to maxCharacterSize.
				if(contentLength >= options.maxCharacterSize) {
					content = content.substring(0, options.maxCharacterSize); 				
				}
				
				var newlineCount = getNewlineCount(content);
				
				// newlineCount new line character. For windows, it occupies 2 characters
				var systemmaxCharacterSize = options.maxCharacterSize - newlineCount;
				if (!isWin()){
					 systemmaxCharacterSize = options.maxCharacterSize
				}
				if(contentLength > systemmaxCharacterSize){
					//avoid scroll bar moving
					var originalScrollTopPosition = this.scrollTop;
					container.val(content.substring(0, systemmaxCharacterSize));
					this.scrollTop = originalScrollTopPosition;
				}
				charLeftInfo.removeClass(options.warningStyle);
				if(systemmaxCharacterSize - contentLength <= options.warningNumber){
					charLeftInfo.addClass(options.warningStyle);
				}
				
				numInput = container.val().length + newlineCount;
				if(!isWin()){
					numInput = container.val().length;
				}
			
				numWords = countWord(getCleanedWordString(container.val()));
				
				numLeft = maxCharacters - numInput;
			} else {
				//normal count, no cut
				var newlineCount = getNewlineCount(content);
				numInput = container.val().length + newlineCount;
				if(!isWin()){
					numInput = container.val().length;
				}
				numWords = countWord(getCleanedWordString(container.val()));
			}
			
			return formatDisplayInfo();
		}
		
		function formatDisplayInfo(){
			var format = options.displayFormat;
			format = format.replace('#input', numInput);
			format = format.replace('#words', numWords);
			//When maxCharacters <= 0, #max, #left cannot be substituted.
			if(maxCharacters > 0){
				format = format.replace('#max', maxCharacters);
				format = format.replace('#left', numLeft);
			}
			return format;
		}
		
		function getInfo(){
			var info = {
				input: numInput,
				max: maxCharacters,
				left: numLeft,
				words: numWords
			};
			return info;
		}
		
		function getNextCharLeftInformation(container){
				return container.next('.charleft');
		}
		

		function isWin(){
			var strOS = navigator.appVersion;
			if (strOS.toLowerCase().indexOf('win') != -1){
				return true;
			}
			return false;
		}
		
		function getNewlineCount(content){
			var newlineCount = 0;
			for(var i=0; i<content.length;i++){
				if(content.charAt(i) == '\n'){
					newlineCount++;
				}
			}
			return newlineCount;
		}
		
		function getCleanedWordString(content){
			var fullStr = content + " ";
			var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
			var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
			var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
			var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
			var splitString = cleanedStr.split(" ");
			return splitString;
		}
		
		function countWord(cleanedWordString){
			var word_count = cleanedWordString.length-1;
			return word_count;
		}
	};  
})(jQuery); 
        </script>
<script type="text/javascript">
			$(document).ready(function(){
				var options3 = {
						'maxCharacterSize': 100,
						'originalStyle': 'originalTextareaInfo',
						'warningStyle' : 'warningTextareaInfo',
						'warningNumber': 10,
						'displayFormat' : '#left'
				};
				$('#qtnbox').textareaCount(options3, function(data){
				});
			});
</script>
<style type="text/css">
.ui-select .ui-btn {
	border-radius: 0px!important;
	border: 1px solid #ddd !important;
	padding-top: 5px;
	padding-bottom: 5px;
}
</style>
<div data-role="content" class="art-content">
  <div class="qna_ask_content" data-position="fixed">
    <div class="qna_ask_blk">
      <div class="qna_ask_blk_text">Ask a Question</div>
      <div style="display:none" class="qna-error-box" id="qtnans">Please Enter Question.</div>
      <div style="display:none" class="qna-error-box" id="qtncat">Please Select Category.</div>
      <div style="display:none" class="qna-error-box" id="qtnblank">Field Should Not Be Blank</div>
      <div style="display:none" class="qna-error-box" id="qtncharerror">100 Characters Limit Exceeded. Add More Details in Description.</div>
      <div  id="userpro"></div>
      <div class="">
        <textarea class="ask_ques_text1" placeholder="Write your questions" id="qtnbox" maxlength="100"><?=$text_valkl?>
</textarea>
      </div>
      <div class="">
        <textarea class="ask_ques_text2" placeholder="Description" id="qnaopenbox" style="display:none;"></textarea>
      </div>
      <div class="ask_ques_desc" id="qnaclickdet"> <img src="../images/details.jpg"> <a >Description</a></div>
      <div style="display:none;">
        <textarea placeholder="Write your questions"></textarea>
      </div>
      <div class="qna_select_category">
        <div style="background:#fff;">
          <select name="qna_catid" id="qna_catid">
            <option value="">Select Category</option>
            <? $sqqncatop=query_execute("SELECT cat_desc, cat_nicename, cat_id FROM qna_cat ORDER BY cat_desc ASC");
        while($rowCat = mysql_fetch_array($sqqncatop)){
         $cat_id = $rowCat["cat_id"];
         $cat_desc = $rowCat["cat_desc"];
     ?>
            <option value="<? echo"$cat_id";?>" <? if($cat_id==$qna_Oldcatid){ echo"selected"; } ?>><? echo"$cat_desc";?></option>
            <? } ?>
          </select>
          <input type="hidden" id="user_id" value="<?=$userid;?>"/>
        </div>
        <div>
          <? if($userid != "Guest"){ ?>
          <a href="" id="btsubmit" style="text-decoration:none;" >
          <div class="ask_qna_submit_btn">Submit</div>
          </a>
          <? }else{ ?>
          <a href="/login.php">
          <div class="ask_qna_submit_btn">Submit</div>
          </a>
          <? }?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require_once($DOCUMENT_ROOT . '/common/bottom.php'); ?>
