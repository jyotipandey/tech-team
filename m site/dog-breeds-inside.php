<!DOCTYPE html>
<html>
<head>

 <?php require_once('common/script.php'); ?>
	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
    
		
        <?php require_once('common/top.php'); ?>
        
        <div data-role="content" class="dog_breeds_sec">
				<div class="dogbreeds_content">
			<div class="dogbreeds_blk"><img src="images/dog-breed.png"></div>
            <div class="breed_type">Afghan Hound</div>
            <div>Even Noah found them worth saving</div>
            
            <div><strong>Origin:</strong>  Afghanistan</div>
            <div><strong>Group:</strong>  Hound</div>
            <div><strong>Origin of Name:</strong> The Afghan Hound as evident from the name, has their roots in  Afghanistan. This was the place where the oldest cave paintings were found. In Afghanistan they were used to assist hunters and shepherds.</div>
			<table class="breads_tab">
            <tr>
            <td><strong>Size</strong></td>
            <td>Large</td>
            </tr>
            <tr>
            <td><strong>Efforts</strong></td>
            <td>Regular Grooming & Regular Exercise</td>
            </tr>
            <tr>
            <td><strong>Shedding</strong></td>
            <td>Negligible</td>
            </tr>
            <tr>
            <td colspan="2" style="font-style:italic;" >
            <strong>Monthly keeping cost</strong>
            </td>
            </tr>
            <tr>
            <td>
           <strong> Premium*</strong>
            </td>
             <td>
            <strong>Standard*</strong>
            </td>
            </tr>
             <tr>
            <td>
Rs. 5,840
            </td>
             <td>
            Rs. 3,050
            </td>
            </tr>
            </table>

            </table>	
			</div>
            
            <div class="content white">
	
	<div class="accordion-container">
		<a href="#" class="accordion-toggle">BREED INFO <span class="toggle-icon"><i class="fa fa-plus-circle"></i></span></a>
		<div class="accordion-content">
			<img src="images/italy-thumb_rome.jpg" />
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<!--/.accordion-content-->
	</div>
	<!--/.accordion-container-->
	<div class="accordion-container">
		<a href="#" class="accordion-toggle">MAINTENANCE & EFFORT  <span class="toggle-icon"><i class="fa fa-plus-circle"></i></span></a>
		<div class="accordion-content">
			<img src="images/italy-thumb_florence.jpg" />
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<!--/.accordion-content-->
	</div>
	<!--/.accordion-container-->
	<div class="accordion-container">
		<a href="#" class="accordion-toggle">HAIR & COAT <span class="toggle-icon"><i class="fa fa-plus-circle"></i></span></a>
		<div class="accordion-content">
			<img src="images/italy-thumb_venice.jpg" />
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<!--/.accordion-content-->
	</div>
	<!--/.accordion-container-->
	<div class="accordion-container">
		<a href="#" class="accordion-toggle">HEALTH <span class="toggle-icon"><i class="fa fa-plus-circle"></i></span></a>
		<div class="accordion-content">
			<img src="images/italy-thumb_cinque-terre.jpg" />
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<!--/.accordion-content-->
	</div>
	<!--/.accordion-container-->
	<div class="accordion-container">
		<a href="#" class="accordion-toggle">BEHAVIOR <span class="toggle-icon"><i class="fa fa-plus-circle"></i></span></a>
		<div class="accordion-content">
			<img src="images/italy-thumb_pompeii.jpg" />
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<!--/.accordion-content-->
	</div>
	<!--/.accordion-container-->
	
	<!--/.accordion-container-->
	<div class="accordion-container">
		<a href="#" class="accordion-toggle">APPEARANCE<span class="toggle-icon"><i class="fa fa-plus-circle"></i></span></a>
		<div class="accordion-content">
			<img src="images/italy-thumb_pompeii.jpg" />
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<!--/.accordion-content-->
	</div>
	<!--/.accordion-container-->
</div>
<!--/.content-->
			</div>
            <script type="text/javascript">
$(document).ready(function () {
    $('.accordion-toggle').on('click', function(event){
    	event.preventDefault();
    	// create accordion variables
    	var accordion = $(this);
    	var accordionContent = accordion.next('.accordion-content');
    	var accordionToggleIcon = $(this).children('.toggle-icon');
    	
    	// toggle accordion link open class
    	accordion.toggleClass("open");
    	// toggle accordion content
    	accordionContent.slideToggle(250);
    	
    	// change plus/minus icon
    	if (accordion.hasClass("open")) {
    		accordionToggleIcon.html("<i class='fa fa-minus-circle'></i>");
    	} else {
    		accordionToggleIcon.html("<i class='fa fa-plus-circle'></i>");
    	}
    });
});
</script>
	   <?php require_once('common/bottom.php'); ?>
		<!-- cart page start-->
        
        <!-- cart page end-->
  