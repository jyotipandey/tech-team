<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
ob_start();
//session_start();
//require_once("constants.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
date_default_timezone_set('Asia/Calcutta');

?><meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />	 
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $baseUrl;?>js/jquery.mobile-1.4.4.css" />
	<link rel="stylesheet" href="<?php echo $baseUrl;?>css/idangerous.swiper.css">
	<link rel="stylesheet" href="<?php echo $baseUrl;?>font-awesome-4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo $baseUrl;?>css/wagtag-accordion.css"  type="text/css">
	<link href="<?php echo $baseUrl;?>css/dogspot.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $baseUrl;?>css/main.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $baseUrl;?>css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo $baseUrl;?>js/idangerous.swiper.min.js"></script>
<!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("92f55b2f6a9ff892fefecca00322d0bc");</script><!-- end Mixpanel -->
 	<!--<script type="text/javascript" src="<?php echo $baseUrl;?>js/loader.js"></script>-->   
	<!--  <script>
        //Kill transitions on android
        $(document).bind("mobileinit", function() {
             //alert();
             //$.mobile.ignoreContentEnabled = true;
             $.mobile.defaultPageTransition = "slide";
             
         }); 
      </script> -->
	<!-- <script src="<?php //echo $baseUrl;?>js/slippry.min.js"></script> -->
    <link rel="stylesheet" href="<?php //echo $baseUrl;?>css/slippry.css">
	<!-- <script type="text/javascript" src="<?php //echo $baseUrl;?>js/purl.js"></script> -->	
	<!-- <script type="text/javascript" src="<?php //echo $baseUrl;?>cordova.js"></script> -->
	<!-- <script type="text/javascript" src="<?php //echo $baseUrl;?>js/service.js"></script> -->
    <!-- <script src="<?php echo $baseUrl;?>js/jquery-1.10.1.min.js"></script> -->
    <script src="<?php echo $baseUrl;?>js/highlight.pack.js"></script>
    <script src="<?php echo $baseUrl;?>js/bellows.min.js"></script> 

	<script>
        var prevSelection = "tab1";
        $("#navbar ul li").live("click",function(){
            var newSelection = $(this).children("a").attr("data-tab-class");
            $("."+prevSelection).addClass("ui-screen-hidden");
            $("."+newSelection).removeClass("ui-screen-hidden");
            prevSelection = newSelection;
        });
		$( function() {
		//	$( "#purchase-product" ).enhanceWithin().popup();
			$( "#purchaseproductdetail" ).enhanceWithin().popup();
			$( "#purchaseproductdetail1" ).enhanceWithin().popup();
			$( "#selectsize_popup" ).enhanceWithin().popup();
			$( "#morepopup" ).enhanceWithin().popup();
			$( "#buynowpopup" ).enhanceWithin().popup();
			$( "#subscription_offer" ).enhanceWithin().popup();
			//$( "#defaultpanel" ).enhanceWithin().panel();
		});
    </script>
       <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<!-- Page 32 start :page-36 home page-->
	<div id="homepage_page32" class="whitewrapper" data-role="page" data-theme="none" data-enhance="false" data-transition="slide" style=" 
    height:500px; !important; padding-bottom:0px;">
		<script type="text/javascript">
			$("#homepage_page32").on("pageinit", function() {
				
				$(".backtosetting").click(function() {
					$.mobile.changePage("#setting_page29",{reverse:true});
				});	
				 var mySwiper = new Swiper('.swiper-container',{
					pagination: '.pagination',
					paginationClickable: true,
					slidesPerView: 5
				  });
				  // for new arrival product slider on home page
				  var mySwiper = new Swiper('.swiper-container0',{
					pagination: '.pagination',
					paginationClickable: true,
					slidesPerView: 5
				  });
				  //for recent-products slider
				   var mySwiper = new Swiper('.swiper-container1',{
					pagination: '.pagination1',
					paginationClickable: true,
					slidesPerView: 5
				  });
					  // for recommended_product slider on home page
				   var mySwiper = new Swiper('.swiper-container2',{
					pagination: '.pagination2',
					paginationClickable: true,
					slidesPerView: 5
				  });
					  // for home page main slider 
				 	var mySwiper = new Swiper('.slider1-container',{
				 	pagination: '.pagination',
				 	grabCursor: true,
				 	paginationClickable: true,
				 });
			});
			$("#homepage_page32").on("pageshow", function() {
			});
	$(function(){
			 $('.loadMore').click(function(){
				var input_Type=$(this).attr('data');
				var t=$('#'+input_Type).val();
				window.location.href="/feature_products.php?request_type="+t;
			 });
			});
		</script>
		
		<div id="header" data-role="header" data-position="fixed">
<?php if($userid=='Guest'){
			$session_id = session_id();
					$qGetMyCart2=query_execute("SELECT *  FROM shop_cart where session_id =  '$session_id' and cart_order_status='new'");
					//echo "SELECT *  FROM shop_cart where userid =  '$session_id'";
					}
					else
					{
						$qGetMyCart2=query_execute("SELECT *  FROM shop_cart where userid =  '$userid' and cart_order_status='new'");
					}
					//echo "SELECT *  FROM shop_cart where userid =  '$userid'";
					$totlrecord = mysql_num_rows($qGetMyCart2);
					?>

			<div><a href="#defaultpanel4" data-role="none" data-inline="true" data-icon="bars" class="panelicon"></a></div>
			<div class="green_clr"><a href="<?php echo $baseUrl;?>"  data-ajax="false"><img src="../images/dogspot-logo-1.png"></a></div>
			<div class="cartsearch_blk">
				<a href="#morepopup" data-transition="pop" class="more_icon" data-rel="popup"> 
					<span></span>
					<span></span>
					<span></span>
				</a>
			</div>
		</div>