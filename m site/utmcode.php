<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1552614-5', 'auto');
  ga('create', 'UA-1552614-17', 'auto','mdogspot');
  ga('require', 'ec');
   <?php 
 /********************ecommerce tracking code start ***************************/
 if($shopProductArray){ ?>
  ga('ec:addImpression', {               <? // Provide product details in a productFieldObject.?>
  'id': '<?=addslashes($shopProductArray['id'])?>',<?                    // Product ID (string).?>
  'name': '<?=addslashes($shopProductArray['name'])?>', <? // Product name (string).?>
  'category': '<?=addslashes($shopProductArray['category'])?>',<?             // Product category (string).?>
  'brand': '<?=addslashes($shopProductArray['brand'])?>',<?                // Product brand (string).?>
  'variant': '<?=addslashes($shopProductArray['variant'])?>',<?              // Product variant (string).?>
  'position': '<?=addslashes($shopProductArray['position'])?>',<?                    // Product position (number).?>
  'dimension1': '<?=addslashes($shopProductArray['dimension1'])?>'<?           // Custom dimension (string).?>});
ga('ec:setAction', 'detail');

<?php  } ?> 

<!-- Analytics code for every section starts here -->
<?php
if ($sitesection == "articles1") {
?>
ga('set', 'contentGroup1', 'Articles');
<?php
} elseif ($sitesection == "articles") {
?>
ga('set', 'contentGroup1', 'Articles');
<?php if($article_category!=''){ ?>
ga('set', 'contentGroup2', '<?=$article_category?>');
<? }?>  
<?php
} elseif ($sitesection == "puppie") {
?>
ga('set', 'contentGroup1', 'Adoption');
<?php
} elseif ($sitesection == "puppies") {
?>
ga('set', 'contentGroup1', 'Adoption');
ga('set', 'contentGroup4', '<?=$puppi_breed;?>');
<?php 
} elseif ($sitesection == "qna") {
?>
ga('set', 'contentGroup1', 'qna');
<?php
} elseif ($sitesection == "qnaview") {
?>
ga('set', 'contentGroup1', 'qna');
ga('set', 'contentGroup2', '<?=$AqnaCat[$qna_catid]?>');
<?php  
}elseif ($sitesection == "dog-breeds") {
?>
ga('set', 'contentGroup1', '<?=$ant_section?>');
<?php if($ant_category!=''){ ?>
ga('set', 'contentGroup4', '<?=$ant_category?>');
<? }?>
<?php
} elseif ($sitesection == "dog-business") {
?>
ga('set', 'contentGroup1', 'dog-business');
<?php   
} elseif ($sitesection == "dog-business-view") {
?>
ga('set', 'contentGroup1', 'dog-business');
ga('set', 'contentGroup2', '<?=$category_nameText?>');
<?php
} elseif ($sitesection == "dog-business-listing") {
?>
ga('set', 'contentGroup1', 'dog-business');
ga('set', 'contentGroup2', '<?=$category_name1?>');
<?php  
} elseif ($sitesection == "dog") {
?>
ga('set', 'contentGroup1', 'dog');
<?php
    
} elseif ($sitesection == "dogview") {
?>
ga('set', 'contentGroup1', 'wagclub');
<?php if($Dogbreed!='' || $sqlbreed_gp!=''){ ?>
ga('set', 'contentGroup4', '<?=$sqldog.''.$Dogbreed.''.$sqlbreed_gp?>');
<? }?> 
<?php
} elseif ($sitesection == "dog-events") {
?>
ga('set', 'contentGroup1', 'dog-events');
<?php 
} elseif ($sitesection == "dog-events-view") {
?>
ga('set', 'contentGroup1', 'dog-events');
ga('set', 'contentGroup2', '<?=$venue?>');
<?php  
} elseif ($sitesection == "photos-album") {
?>
ga('set', 'contentGroup1', 'photos-album');
ga('set', 'contentGroup2', '<?= $album_name1 ?>'); 
<?php  
} elseif ($sitesection == "photos") {
?>
ga('set', 'contentGroup1', 'photos');
<?php
} elseif ($sitesection == "photos-view") {
?>
ga('set', 'contentGroup1', 'photos-view');
ga('set', 'contentGroup2', '<?=$rowdogID[dog_breed_name];?>');
<?php
    
} elseif ($sitesection == "show-result") {
?>
ga('set', 'contentGroup1', 'show-result');
ga('set', 'contentGroup2', '<?=$show_name; ?>');
<?php   
} elseif ($sitesection == "club") {
?>
ga('set', 'contentGroup1', 'show');
ga('set', 'contentGroup2', '<?=$club_Name;?>');
ga('set', 'contentGroup7', '<?=stripslashes($rtn_row_Data['section_title_tag'])?>');
<?php  
}elseif ($sitesection == "shop" && $ant_page !='Response') {
?>
ga('set', 'contentGroup1', 'Shop')
ga('set', 'contentGroup2', '<?=str_replace(" ' "," ",$ant_category);?> ');
<?php if($cat_name_bread!=''){ ?>
ga('set', 'contentGroup3', '<?=$cat_name_bread?>');
<?php if($shopProductArray['brand']!=''){?>
ga('set', 'contentGroup5', '<?=stripslashes($shopProductArray['brand']) ?>');
<? }?>
<?php }?>
<?php   
}elseif ($sitesection == "shop-cart") {
?>
ga('set', 'dimension1', '<?=$ant_section?>');
ga('set', 'dimension4', '<?=$ant_page?>');
<?php   
}elseif ($sitesection == "donation") {
?>
ga('set', 'contentGroup1', 'Donation');
<?php   
}elseif ($sitesection == "wag-tag") {
?>
ga('set', 'contentGroup1', 'Wag Tag');
<?php   
}elseif ($sitesection == "animal-activist") {
?>
ga('set', 'contentGroup1', 'Animal Activist');
<?php   
}elseif ($sitesection == "dog-names") {
?>
ga('set', 'contentGroup1', 'Dog Names');
<?php if($sex_bred!='' || $breed){?>
ga('set', 'contentGroup4', '<?=$breed.''.$sex_bred?>');
<? }?>
<?php   
}
?>

 ga('send', 'pageview');
 ga('mdogspot.send', 'pageview');
</script>
<script>function checksearch(){
var searchtext=document.getElementById('searchText').value;
if(searchtext==''){	
	//document.getElementById('searchText').style.backgroundColor = "red";
	document.getElementById('SrchErr').innerHTML="<font color='red'>This field is required.</font>";
	document.formhgtr.searchText.focus();
	return false;
}else{
	return true;
}
}

</script>