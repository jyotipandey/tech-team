<?php
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<link rel="canonical" href="https://m.dogspot.in/contactus.php" />
<title>Contact DogSpot - Online Pet Supply Store</title>
<meta name="keywords" content="Contact Dogspot, Contact Pet supplies store, get in touch with Pet Shop, Dog store." />
<meta name="description" content="Contact DogSpot for any questions regarding pets supplies, accessories & online products purchase. You can get in touch via phone calls or by filling inquiry form." />
<meta itemprop="description" content="DogSpot.in offers pet products including dogs, cats and small pets online.">
<meta itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<meta itemprop="streetAddress" content="Plot no - 545, S.Lal Tower Sector - 20, Dundahera">
<meta itemprop="addressLocality" content="Gurgaon">
<meta itemprop="addressRegion" content="Haryana">
<meta itemprop="postalCode" content="122016">
<meta itemprop="addressCountry" content="India">

 <?php require_once('common/script.php'); ?>
    <?php require_once('common/top.php'); ?>
<div class="ds_contactuscont">
<!--<div class="call_us_blk">
<div class="ds_callus"><h1>Call Us:</h1></div>
<div class="call_us_inside">
<div class="custmr_care_no">Customer Care:
                 <a href="tel:+9212196633" class="ds_footer_links ui-link"><span>+91-9212196633</span></a></div>
<div class="call_time_contact">Mon-Sat (9 AM - 6 PM) </div>
<div class="ds_customersmall ">(Standard Calling Charges Apply)</div>
</div>
</div>-->

<div class="mail_us_blk">

<div class="ds_mailus ">
Mail Us:
</div>
<div class="mail_us_inside">
<div>Marketing Alliances: <a href="mailto:marketing@dogspot.in">marketing@dogspot.in</a></div>

<div class="ds_mailaddress">
Om Sai Complex, First Floor, 
Plot No. 37/19/22, 
Behind IGL CNG Petrol Pump, 
Kapashera, New Delhi-110037</div>
<div class="ds_notetext ds_contborder">Please Note:</div>
<div class="ds_customerdetails">DogSpot.in is an open platform for information sharing and is not involved in the sale or purchase of puppies and dogs. The users can interact with each other directly for adoption, sale or purchase of Puppies and Dogs. DogSpot.in does not undertake any responsibility for Transactions made based on information posted on the website.</div>
</div>
</div>
</div>

<?php require_once('common/bottom.php'); ?>
