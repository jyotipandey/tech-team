<?php
require_once('/home/dogspot/public_html/functions.php'); 
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
//require_once(SITEMAIN_URL.'/functions.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
?><!DOCTYPE html>
<html>
<head>
 <?php require_once('common/script.php'); ?>
	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
	 <?php require_once('common/top.php'); ?>
	  <div class="p-one-em" >
		<div data-role="content">
		<div class="order_blk">
			<div class="order_id">
			<?php
			$order_id=$_GET['order_id'];
			$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id' AND userid='$userid' AND order_type='complete'");
			$order_date = $SQ_shop_order['order_c_date'];
			?>
			<div>Order ID:<span class="order_no"><?=$order_id?></span>
				<p><?=$date?></p>
			</div>
			    <!--<div class="right cross_btn"><a href="#"><span class="fa fa-trash-o"></span></a></div>-->
				</div>
				<div class="order_delivery_detail">
					<ul>
						<li><div>Order Verified</div>
                       		 <?php 
						//Order is in Pending-Dispatch
						$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id' AND userid='$userid' AND order_type='complete'");
						$SQ_courier = query_execute_row("SELECT * FROM shop_courier WHERE courier_id='".$SQ_shop_order['shop_courier_id']."'");		
						// Order is in New
						$order_date = $SQ_shop_order['order_c_date'];
						$date = showdate($order_date, "d M o");
						$awb_no = $SQ_shop_order['order_tracking'];
						$courier_id = $SQ_shop_order['shop_courier_id'];
						$time = date('h:i A', strtotime($order_date));
						$sqlcomplete = $date. ' | ' .$time. ' | ' ."Your order has been placed";
						?>
                        <div><?=$sqlcomplete?></div></li>
						<li><div>Order Dispatched</div>
                        <?php
							$SQ_review = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%pending-dispatch%' AND review_section_id='$order_id'");
							$rev_date = $SQ_review['c_date'];
							$datetimearray1 = explode(" ", $rev_date);
							$date1 = $datetimearray1[0];
							$time1 = $datetimearray1[1];
							$reformatted_date1 = date('d-m-Y',strtotime($date1));
							$reformatted_time1 = date('Gi.s',strtotime($time1));
							if($rev_date==''){
								$sqlcomplete1 = "";
							}else{
								$sqlcomplete1 = $date1. ' | ' .$time1. ' | ' ."Your order has been cofigured";
							}
						?>
                        <div><?php echo $sqlcomplete1;?></div></li>
						<li><div>Out for Delivery</div>
                        <?php
							$sqld1 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched-ready%' AND review_section_id='$order_id'");
							$sqlsdat1 = $sqld1['c_date'];
							$datetimearray2 = explode(" ", $sqlsdat1);
							$date2 = $datetimearray2[0];
							$time2 = $datetimearray2[1];
							$reformatted_date2 = date('d-m-Y',strtotime($date2));
							$reformatted_time2 = date('Gi.s',strtotime($time2));
							if($sqlsdat1 == ''){
								$sqlcomplete2 = "";
							}else{
								$sqlcomplete2 = $date2. ' | ' .$time2. ' | ' .$sqlstate2. ' | ' ."Your order has been packed";
							}
						?>
                        <div><?=$sqlcomplete2?></div></li>
						<!--<li><div>Dispatched From Warehouse</div>
						<?php
							$sqld2 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched%' AND review_section_id='$order_id'");
							$sqlsdat3 = $sqld2['c_date'];
							$datetimearray3 = explode(" ", $sqlsdat3);
							$date3 = $datetimearray3[0];
							$time3 = $datetimearray3[1];
							$reformatted_date3 = date('d-m-Y',strtotime($date3));
							$reformatted_time3 = date('Gi.s',strtotime($time3));
							if($sqlsdat3 == ''){
								$sqlcomplete3 = "";
							}else{
								$sqlcomplete3 = $date3. ' | ' .$time3. ' | ' ."Your order has been dispatched from warehouse";
							}
						?>
                        <div ><?=$sqlcomplete3?></div></li>-->
						
						<li><div>DELIVERED</div>
						<?php
							$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%Delivered%' AND review_section_id='$order_id'");
							if($sqld4['c_date']== ''){
								$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%delivered%' AND review_section_id='$order_id'");
								$sqlsdat4 = $sqld4['c_date'];	
							}
							$sqlsdat4 = $sqld4['c_date'];
							$date7 = showdate($sqlsdat4, "d M o");
							$time7 = date('h:i A', strtotime($sqlsdat4));
							if($sqlsdat4 == ''){
								$sqlcomplete4 = "";
							}else{ 
								$sqlcomplete4 = $date7. ' | ' .$time7. ' | ' .'"Your order has been delivered';
							}
						?>
                        <div><?=$sqlcomplete4?></div></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="customer_care_Div" data-role="footer" data-position="fixed" data-tap-toggle="false">
			<div class="phone_circle">
				<span class="fa fa-phone"></span>
			</div>
			<div class="custmr_care_blk">
				<div class="custmr_care_no"><div class="custmr_care_no">Customer Care: <span>+91-9212196633</span></div>
				<div class="call_time">Mon-Fri &nbsp;(8 AM - 7 PM)</div>
			</div>
		</div>
	</div>	
</div>
		<?php require_once('common/bottom.php'); ?>
	
  