<?php
ini_set('memory_limit', '-1');
include_once("../constants.php");
include_once(SITEMAIN_URL."/functions2.php");
include_once(SITEMAIN_URL."/functions.php");
include_once(SITEMAIN_URL.'/session.php');
require(SITEMAIN_URL."/database.php");

function strip_mailto_tags($post_content_filtered){
	return $post_content_filtered = preg_replace('#<a.+?href="mailto:(.*?)".+?</a>#', "$1",$post_content_filtered);
}
function remove_outlink_without_text($content){
	$pattern = '#<a [^>]*\bhref=([\'"])http.?://((?<!dogspot)[^\'"])+\1 *.*?</a>#i';
	return preg_replace($pattern, '', $content);
}
function new_text($post_content)
{
	$post_content = str_replace('%5C%22', '', $post_content);
	$post_content = str_replace('!important', '', $post_content);
	$post_content = str_replace('<city>', ' ', $post_content);
	$post_content = str_replace('<place>', ' ', $post_content);
	$post_content = str_replace('<state>', ' ', $post_content);
	$post_content = str_replace('</city>', ' ', $post_content);
	$post_content = str_replace('</place>', ' ', $post_content);
	$post_content = str_replace('</state>', ' ', $post_content);
	//$post_content = str_replace('https://www.stechies.com', 'https://m.stechies.com', $post_content);
    $post_content = str_replace($a, $b, $post_content);
	$post_content = preg_replace('/(<font[^>]*>)|(<\/font>)/', '', $post_content);
	$post_content = preg_replace('/(<span[^>]*>)|(<\/span>)/', '', $post_content);
	$post_content = preg_replace('/(<stockticker[^>]*>)|(<\/stockticker>)/', '', $post_content);
	$post_content = preg_replace('/(<smarttagtype[^>]*>)|(<\/smarttagtype>)/', '', $post_content);
	$post_content = preg_replace('/(<placename[^>]*>)|(<\/placename>)/', '', $post_content);
	$post_content = preg_replace('/(<placetype[^>]*>)|(<\/placetype>)/', '', $post_content);
	$post_content = preg_replace('/(<shapetype[^>]*>)|(<\/shapetype>)/', '', $post_content);
	$post_content = preg_replace('/(<line[^>]*>)|(<\/line>)/', '', $post_content);
	$post_content = preg_replace('/(<svg[^>]*>)|(<\/svg>)/', '', $post_content);
	$post_content = preg_replace('/(<date[^>]*>)|(<\/date>)/', '', $post_content);
	$post_content = preg_replace('/(<street[^>]*>)|(<\/street>)/', '', $post_content);
	$post_content = preg_replace('/(<time[^>]*>)|(<\/time>)/', '', $post_content);
	$post_content = preg_replace('/(<personname[^>]*>)|(<\/personname>)/', '', $post_content);
    $post_content = htmlentities(addslashes($post_content));
	return $post_content;
}

function convert_to_amp($post_content) {
    $post_content = preg_replace('/(<[^>]+) border=".*?"/i', '$1', $post_content);
	//$post_content = preg_replace('/(<[^>]+) !important/i', '$1', $post_content);
    $post_content = preg_replace('/(<[^>]+) cellpadding=".*?"/i', '$1', $post_content);
    $post_content = preg_replace('/(<[^>]+) cellspacing=".*?"/i', '$1', $post_content);
    $post_content = preg_replace('/<video/i', '<amp-video', $post_content);
    $post_content = preg_replace('/<iframe/i', '<amp-iframe', $post_content);
   
    $post_content = preg_replace('/<img/i', '<amp-img', $post_content);
    $post_content = preg_replace('/<amp-img(.*?)\/?>/', '<amp-img layout="responsive"$1></amp-img>', $post_content);
    $post_content = preg_replace('/(<meta[^>]*>)|(<\/meta>)/', '', $post_content);
	$post_content = preg_replace('/(<country-region[^>]*>)|(<\/country-region>)/', '', $post_content);
	$post_content = preg_replace('/(<link[^>]*>)|(<\/link>)/', '', $post_content); 
	$post_content = mb_convert_encoding($post_content, 'HTML-ENTITIES', 'UTF-8');
    $doc = new DOMDocument();
    @$doc->loadHTML($post_content);
 // echo  htmlentities($post_content);
	foreach ($doc->getElementsByTagName('p') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	foreach ($doc->getElementsByTagName('p') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	foreach ($doc->getElementsByTagName('p') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	foreach ($doc->getElementsByTagName('p') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	
	foreach ($doc->getElementsByTagName('li') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	foreach ($doc->getElementsByTagName('li') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	foreach ($doc->getElementsByTagName('li') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	
	foreach ($doc->getElementsByTagName('td') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	//foreach ($doc->getElementsByTagName('td') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	foreach ($doc->getElementsByTagName('td') as $tag) {foreach ($tag->attributes as $attr) {$tag->removeAttribute($attr->name);}}
	
	foreach ($doc->getElementsByTagName('amp-img') as $tag) {
		$cont = $tag->getAttribute('style');
        if($cont != '' || $cont != NULL){
            $re = "/width:(\\d+)/";
            preg_match($re, $cont, $matches);
            $tag->setAttribute("width", $matches[1]);
            $re1 = "/height:(\\d+)/";
            preg_match($re1, $cont, $matches1);
            $tag->setAttribute("height", $matches1[1]);
        }
    }
	foreach ($doc->getElementsByTagName('amp-youtube') as $tag) {
		$contsrc = $tag->getAttribute('src');
		$coin=explode('youtube.com/embed/',$contsrc);
		$getif=explode('?',$coin[1]);
		$tag->setAttribute("data-videoid", $getif[0]);
		$tag->removeAttribute("src");
		$tag->removeAttribute("allowfullscreen");
		$tag->removeAttribute("frameborder");
        }	
	foreach ($doc->getElementsByTagName('amp-img') as $tag) {
        $tag->removeAttribute("vspace");
		$tag->removeAttribute("hspace");
		$tag->removeAttribute("align");
		$tag->removeAttribute("name");
		$cont = $tag->getAttribute('height');
		$contwidth = $tag->getAttribute('width');
        if($cont == '' || $cont == NULL || $contwidth == '' || $contwidth == NULL){
			$src = $tag->getAttribute('src');
			$src = str_replace('https://www.dogspot.in','',$src);
			$src = str_replace('https://ik.imagekit.io/2345','',$src);			
			$src = str_replace('%20',' ',$src);
			if(file_exists ('/home/dogspot/public_html'.$src)){
			$imgwh = getimagesize('/home/dogspot/public_html'.$src);
			$tag->setAttribute("width", $imgwh[0]);
			$tag->setAttribute("height", $imgwh[1]);
			}
        }
    }
	foreach ($doc->getElementsByTagName('table') as $tag) {
		$stye= str_replace('!important', '',$tag->getAttribute("style"));
        $tag->setAttribute("style",$stye);
		}
		foreach ($doc->getElementsByTagName('tr') as $tag) {
        $stye= str_replace('!important', '',$tag->getAttribute("style"));
        $tag->setAttribute("style",$stye);
		}
		foreach ($doc->getElementsByTagName('td') as $tag) {
        $stye= str_replace('!important', '',$tag->getAttribute("style"));
        $tag->setAttribute("style",$stye);
		}
		foreach ($doc->getElementsByTagName('tr') as $tag) {
       $stye= str_replace('!important', '',$tag->getAttribute("style"));
        $tag->setAttribute("style",$stye);
		}
		foreach ($doc->getElementsByTagName('th') as $tag) {
       $stye= str_replace('!important', '',$tag->getAttribute("style"));
        $tag->setAttribute("style",$stye);
		}
    foreach ($doc->getElementsByTagName('*') as $tag) { 
        $tag->removeAttribute("style");
		$tag->removeAttribute("garamond");
        if($tag->tagName != 'html' && $tag->tagName != 'body' && $tag->tagName != 'head' && $tag->tagName != 'a'  && $tag->tagName != 'amp-img'){
            foreach ($tag->attributes as $attr) {
                if($attr->name != 'class' && $attr->name != 'id' && $attr->name != 'href'){
                    $tag->removeAttribute($attr->name);
                }
            }
        }
    }
	
	
    $post_content = $doc->saveHTML();
    $post_content = str_replace('<html><body>', '', $post_content);
    $post_content = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">', '', $post_content);
    $post_content = str_replace('</body></html>', '', $post_content);
	 $post_content = str_replace('<table ','<div class="table-responsive"><table ', $post_content);
    $post_content = str_replace('/table>', '/table></div>', $post_content);
   // $post_content = str_replace('<table ','<table class="table table-bordered"', $post_content);
    $post_content = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $post_content);
    $post_content = preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $post_content);
    return $post_content;
}
$date = date('Y-m-d',strtotime("-7 days"));
$date = $date." 00:00:00";

//$db->where('post_status','publish');
//$db->where('post_modified',"$date",'>');
//$db->orderBy('ID','asc');
//$rowPost = $db->get('sap_posts',null,array('ID','post_content'));
$getQt=mysql_query("SELECT event_id,map FROM events WHERE map!='' ");
$a = urldecode("%27%5C%22_blank%5C%22%27");
$b = urldecode("%22_blank%22");
while($rows=mysql_fetch_array($getQt)){
    
	$map = stripslashes($rows['map']);

	$event_id=$rows['event_id'];
	//$db->where('post_id', "$ID");
	//$selRemoveLink = $db->getOne('sap_posts_remove_link', 'post_id');
	//if($selRemoveLink['post_id']){$post_content = strip_a_tags($post_content);}
	
    $map = strip_mailto_tags(stripallslashes(htmlspecialchars_decode($map)));// remove only mailto tag
	$map = convert_to_amp($map);
	$map = remove_outlink_without_text($map);
	$map = new_text($map);
	echo "UPDATE events SET map_amp='$map'  WHERE  event_id='$event_id'";
	$updateU=mysql_query("UPDATE events SET map_amp='$map'  WHERE  event_id='$event_id'");	
	
	//}
	//$db->where('post_id', $rows['ID']);
	//$db->where('meta_key', "post_amp_tags");
	//$rowamp_tags = $db->getOne('sap_postmeta', 'meta_value');
	//if(!$rowamp_tags){
	//$data = array(
      //  'post_amp' => "$post_content"
    //);
    //$db->where('ID',$rows['ID']);
    //if(!$db->update('sap_posts',$data)){
      //  echo 'update failed: ' . $db->getLastError(),"<hr/>";
    //} }   
}