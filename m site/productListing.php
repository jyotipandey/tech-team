<?php 
require_once("constants.php");
require_once("database.php");
require_once(SITEMAIN_URL."/shop/functions.php");
require_once(SITEMAIN_URL."/shop/arrays/arrays.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
require_once(SITEMAIN_URL."/functions.php");
?>
<!DOCTYPE html>

<html>
<head>
	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>

	<?php require_once('common/top.php'); ?>
<div data-role="content">
			<div class="sol_blk">
				<ul class="sol_listing groom">
					<!---<li>
						<select id="select-dog-pro" name="select-choice" data-role="none">
							<option>All Products of Dog Grooming (250) </option>
							<option value="">All Products of Dog Grooming</option>
							<option value="">Dog Deodorizers</option>
							<option value="">Dry Bath</option>
							<option value="">Advance Grooming</option>
							<option value="">Dog Shedding ontrol</option>
							<option value="">Brushes & Combs</option>
							<option value="">Dog Grooming Tools</option>
							<option value="">Ear Care</option>
							<option value="">Eye Care</option>
							<option value="">Dental Care</option>
						</select>
					</li>--->
					<li>
						<div class="img_blk"><img src="images/dummy-image.png"/> </div>
						<div class="txt_blk">
							<a href="#product_detail_page36">Aromatree 2 in 1 deodorant Spray For Dog & Cat - 200ml</a>
							<p><span>Rs.</span> 1,100</p>
							<small><strike>Rs. 2,200</strike> 50% off</small>
						</div>
						<div class="product_list_option">
							<a href=""><div class="hoverinfo"></div></a>
							<a href=""><div class="hovercart"></div></a>
							<a href=""><div class="hovershare"></div></a>
							<a href=""><div class="hoverstar"></div></a>
						</div>
					</li>
					<li class="outofstock">
						<div class="img_blk"><img src="images/dummy1-image.png"/></div>
						<div class="txt_blk">
							<a href="#productnotavaliable_page37">Petkin Valu-Pak Petwipes Vanilla Coconut for Dog & Cat - 40 Pieces</a>
							<p><span>Rs.</span> 1,100</p>
							<small><strike>Rs. 2,200</strike> 50% off</small>
						</div>
						<!--div class="product_list_option">
							<a href=""><div class="hovercart"></div></a>
							<a href=""><div class="hovershare"></div></a>
							<a href=""><div class="hoverstar"></div></a>
						</div-->
					</li>
					<li>
						<div class="img_blk"><img src="images/dummy2-image.png"/></div>
						<div class="txt_blk">
							<a href="#product_detail_page36">All 4 Pet Wipes For Dogs & Cats - 10 Pic</a>
							<p><span>Rs.</span> 1,100</p>
							<small><strike>Rs. 2,200</strike> 50% off</small>
						</div>
						<div class="product_list_option">
							<a href=""><div class="hoverinfo"></div></a>
							<a href=""><div class="hovercart"></div></a>
							<a href=""><div class="hovershare"></div></a>
							<a href=""><div class="hoverstar"></div></a>
						</div>
					</li>
					<li>
						<div class="img_blk"><img src="images/dummy3-image.png"/></div>
						<div class="txt_blk">
							<a href="#product_detail_page36">Ptekin ItchWipes For Dog & Cat - 30 Pieces</a>
							<p><span>Rs.</span> 1,100</p>
							<small><strike>Rs. 2,200</strike> 50% off</small>
						</div>
						<div class="product_list_option">
							<a href=""><div class="hoverinfo"></div></a>
							<a href=""><div class="hovercart"></div></a>
							<a href=""><div class="hovershare"></div></a>
							<a href=""><div class="hoverstar"></div></a>
						</div>
					</li>
					<li>
						<div class="img_blk"><img src="images/dummy4-image.png"/></div>
						<div class="txt_blk">
							<a href="#product_detail_page36">BI Anti Dandruff Lotion For Dog & Cat - 200ml</a>
							<p><span>Rs.</span> 1,100</p>
							<small><strike>Rs. 2,200</strike> 50% off</small>
						</div>
						<div class="product_list_option">
							<a href=""><div class="hoverinfo"></div></a>
							<a href=""><div class="hovercart"></div></a>
							<a href=""><div class="hovershare"></div></a>
							<a href=""><div class="hoverstar"></div></a>
						</div>
					</li>
					<li>
						<div class="img_blk"><img src="images/dummy5-image.png"/></div>
						<div class="txt_blk">
							<a href="#product_detail_page36">Magic White Dog Coat Whitener Cleaner 300ml</a>
							<p><span>Rs.</span> 1,100</p>
							<small><strike>Rs. 2,200</strike> 50% off</small>
						</div>
						<div class="product_list_option">
							<a href=""><div class="hoverinfo"></div></a>
							<a href=""><div class="hovercart"></div></a>
							<a href=""><div class="hovershare"></div></a>
							<a href=""><div class="hoverstar"></div></a>
						</div>
					</li>
				</ul>
			</div>
        </div>
		<div id="grooming-footer" data-role="footer" data-position="fixed">
			<div class="sortby_det"><a href="#sort_by" data-rel="popup"><img src="images/sort.png"/>Sort by</a></div>
			<div class="filters_det"><a href="#filters-page24"><img src="images/filter.png"/>Filters</a></div>
		</div>
		<div data-role="popup" data-transition="fade" id="sort_by" class="sortbypopup" data-position-to="window">
			<ul>
				<li>Sort By</li>
				<li>Popularity</li>
				<li class="active">Price: Low to High</li>
				<li>Price: High to Low</li>
			</ul>
		</div>
            <?php require_once('common/bottom.php'); ?>