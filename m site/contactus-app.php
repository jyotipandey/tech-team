<?php
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<link href="https://m.dogspot.in/css/dogspot.css" rel="stylesheet" type="text/css">
 <?php //require_once('common/script.php'); ?>
 
	<title>Contact Us</title>
    <?php //require_once('common/top.php'); ?>
<div class="ds_contactuscont">
<div class="call_us_blk">
<div class="ds_callus">Call Us:</div>
<div class="call_us_inside">
<div class="custmr_care_no">Customer Care: <a href="tel:+919212196633" class="ds_footer_links ui-link"><span>+91-9212196633</span></a></div>
<div class="call_time_contact">Mon-Fri (9 AM - 6 PM) </div>
<div class="ds_customersmall ">(Standard Calling Charges Apply)</div>
</div>
</div>

<div class="mail_us_blk">

<div class="ds_mailus ">
Mail Us:
</div>
<div class="mail_us_inside">
<div>Marketing Alliances: <a href="mailto:marketing@dogspot.in">marketing@dogspot.in</a></div>

<div class="ds_mailaddress">
2017, Atul Kataria Chowk
Old Delhi Gurgaon Road
Gurgaon -122016, Haryana</div>
<div class="ds_notetext ds_contborder">Please Note:</div>
<div class="ds_customerdetails">DogSpot.in is an open platform for information sharing and is not involved in the sale or purchase of puppies and dogs. The users can interact with each other directly for adoption, sale or purchase of Puppies and Dogs. DogSpot.in does not undertake any responsibility for Transactions made based on information posted on the website.</div>
</div>
</div>
</div>

<?php //require_once('common/bottom.php'); ?>
