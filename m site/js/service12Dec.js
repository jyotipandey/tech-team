$(document).ready(function(){
});

//Save Device ID
//if(localStorage.guid == undefined)
//{
//    localStorage.guid = GUID();
//}https://www.dogspot.in/dogs/mobile-webservice/android/login.php

//	var HOST = "http://27.7.228.32:8080";
//	var HOST = "http://code.serverstaging.com";
	var HOST = "https://www.dogspot.in";

var service = {
	url: "",
	hosturl: HOST + "/mobile-webservice/android",
	deviceToken:"",
	deviceType: "android",
    accessToken:"YnJhamVuZHJhIyMx",
    
    sendRequest: function(targetUrl,SERVICE_NAME, params, result) {
        var data = this.createRequest(params);
        this.url = targetUrl+"/"+SERVICE_NAME;
         
        console.log("Data << " + JSON.stringify(data));
        console.log("URL << " + this.url);
        $.ajax(this.url, {
            crossDomain:false,
            type: 'POST',
            data: data,
            success:function(data,text,xhqr){
                console.log("---> "+JSON.stringify(data));
                result(data);
            },
            error: function (responseData, textStatus, errorThrown) {
                console.log("something went wrong!! "+JSON.stringify(responseData) +", Error: "+textStatus);
            }
        });
    },

    createRequest: function(params) {
        return this.createCommand(params);
        /*
    	return {
        	session: this.sessionRequest(),
        	request : this.createCommand(params)
        };
        */
    },

    createCommand: function(params) {
        var command = new Object();
        if (params != null) {
            params(command);
        }
        return command;
    },
    
    sessionRequest: function() {
        return {
        	email: this.email,
	        password:this.password
        };
    },
    
    signupRequest: function(name,email,password,Youare,result){
    	service.sendRequest(this.hosturl,"register.php", function(args){
    		args.Name = name;
			args.Email_id = email;
    		args.Password = password;
    		args.Request_type = 0;
    		args.Youare = Youare;
    	}, result)
    },
    
    signupRequestFB: function(Name,Email,DOB,City,Phone,County,SEX,result){
    	service.sendRequest(this.hosturl,"register.php", function(args){
    		args.Name  = Name;
			args.Email = Email;
    		args.DOB = DOB;
    		args.Request_type = 1;
    		args.City = City;
    		args.Phone = Phone;
    		args.County = County;
    		args.SEX = SEX;
    	}, result)
    },
    
    signinRequest: function(email,password,result){
    	service.sendRequest(this.hosturl,"login.php", function(args){
    		args.Email_id = email;
    		args.Password = password;
    		args.Request_type=0;
    	}, result)
    },
    signinRequestSocial: function(email,password,request_type,name,result){
        service.sendRequest(this.hosturl,"login.php", function(args){
                            args.Email_id = email;
                            args.Password = password;
                            args.Request_type=request_type;
                            args.name = name;
                            }, result)
    },
    forgetPwdRequest: function(email,result){
    	service.sendRequest(this.hosturl,"forgetpass.php", function(args){
    		args.Email_id = email;
    	}, result)
    },
    
    getMyDogsRequest: function(result){ 
    	service.sendRequest(this.hosturl,"mydogs.php", function(args){
    		args.access_token = service.accessToken;
    	}, result)
    	
    },
    
    getWagTagIdRequest: function(dog_id, result){ 
    	service.sendRequest(this.hosturl,"getid.php", function(args){
    		args.dog_id = dog_id;
    		args.access_token = service.accessToken;
    	}, result)
    	
    }, 
   
    activateWagtagRequest: function (dog_id,wag_tag_id,activate_code , result){
    
    	service.sendRequest(this.hosturl,"activatewagtag.php",function(args){
                        
                        args.access_token = service.accessToken;
                        args.dog_id = dog_id;
                        args.wag_tag_id = wag_tag_id;
                        args.activate_code = activate_code;
                        args.mobile_number = verifi_number;
                        
                        }, result)

	},
	    
	
  
	  varifyCode: function (verify_Code,mobileNo, result){
		    
	      service.sendRequest(this.hosturl,"verifycode.php",function(args){
	    	  				args.access_token = service.accessToken;
	                        args.mobile_number = mobileNo;
	                        args.author_code = verify_Code ;
	                       } , result)
	
	},
	
	getYouAreFeed: function(result){
		 service.sendRequest(this.hosturl,"register-parameter.php",function(args){
			
			 	
			 
            } , result)

	},
	
	
	addPhotosRequest: function (name,Breed,gender,add_new_dog_image,result){
		service.sendRequest(this.hosturl,"add_new.php",function(args){
	    	  				args.access_token = service.accessToken;
	                        args.name = name;
	                        args.breed = Breed ;
	                        args.city = "delhi" ;
	                        args.gender = gender;
	                        args.image = add_new_dog_image;
	                       } , result)
	                       
	},
	
	 getCityList: function(result){
		 service.sendRequest(this.hosturl,"city-list.php",function(args){
			 args.access_token = service.accessToken;
			 args.name = "Hero";
             args.breed = "beagle" ;
             args.city = "Delhi" ;
             args.sex = "M";
             args.photo = "dog1.jpg";
            } , result)

	},
	
	
	getBreedList: function(result){
		 service.sendRequest(this.hosturl,"breed-list.php",function(args){
			 args.access_token = service.accessToken;
			 args.name = "Hero";
             args.breed = "beagle" ;
             args.city = "Delhi" ;
             args.sex = "M";
             args.photo = "dog1.jpg";
            } , result)

	},
}

var img_path ="https://www.dogspot.in/dogs/images/";
var dogs_list = null;
var verifi_number = null;
var verifi_code = null;
var is_verified = false;
var add_new_dog_image;