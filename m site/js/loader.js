var startLoading = function(){
    $("#loading").show();
    $('#loading').html("<center><img src=\"images/ajax_loader.gif\" border=\"0\"  class=\"list-li-thumb\"></center>");  
    myLoadingState = true;
    myLoadingTimer = setTimeout ( "onLoadingTimeout()", 250 );
};

//stops the loading
var stopLoading = function(){
    myLoadingState = false;
    $('#loading').html("");
    $("#loading").hide();  
};

var onLoadingTimeout = function()
{
    if (myLoadingState == true)
    {
        myCurrentLoadImg++;
        if (myCurrentLoadImg > 4) myCurrentLoadImg = 1;
        var curImg = "images/loading" + myCurrentLoadImg + ".gif";
        $('#loading').html("<center><img src=\"" + curImg + "\" border=\"0\"  class=\"list-li-thumb\"></center>");
        myLoadingTimer = setTimeout ( "onLoadingTimeout()", 250 );
    }
};