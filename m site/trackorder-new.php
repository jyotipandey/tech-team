<?php
require_once('/home/dogspot/public_html/functions.php'); 
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
//require_once(SITEMAIN_URL.'/functions.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/shop/arrays/arrays.php');
require_once(SITEMAIN_URL.'/session.php');
?><!DOCTYPE html>
<?php
$user_id = $userid;
if($order_id){
	$shop_address = mysql_query("SELECT * FROM shop_order_address WHERE order_id='$order_id' AND address_type_id='1' AND userid='$user_id' ORDER BY order_address_id DESC LIMIT 1");
	$getAdd = mysql_fetch_array($shop_address);
	//Getting shipping details
	$del_name = $getAdd['address_name'];
	$userid_check = $getAdd['userid'];
	if($userid_check==$user_id){
	$address1 = $getAdd['address_address1'];
	$address2 = $getAdd['address_address2'];
	$city = $getAdd['address_city'];
	$state = $getAdd['address_state'];
	$mobile = $getAdd['address_mobile'];
	$pincode = $getAdd['address_pincode'];
	$full_address = $address1;
	if($address2){
		$full_address .= ", ". $address2;
	}
	$full_address .= ", ".$pincode;
	//End
	
	//Getting order details
	$shop_order = mysql_query("SELECT * FROM shop_order WHERE order_id='$order_id' AND userid='$user_id'");
	$getOrder = mysql_fetch_array($shop_order);
	$order_amount = $getOrder['order_amount'];
	$order_method = $getOrder['order_method'];
	$status = $getOrder['order_status'];
	if($getOrder['delevery_status']=="rto"){
		$order_status = "RTO(Return to Origin)";
	}elseif($getOrder['delevery_status']=="canceled"){
		$order_status = "Cancelled";
	}else{
		$order_status = $getOrder['delevery_status'];
	}
	$order_c_date = $getOrder['order_c_date'];
	//End
	
	//Tracking status
	$SQ_shop_order = query_execute_row("SELECT * FROM shop_order WHERE order_id = '$order_id' AND userid='$user_id' AND order_type='complete'");
	if($SQ_shop_order['order_id']!=""){
		// Get Courier Name and Url
		$SQ_courier = query_execute_row("SELECT * FROM shop_courier WHERE courier_id='".$SQ_shop_order['shop_courier_id']."'");
		
		// Order is in New
		$order_date = $SQ_shop_order['order_c_date'];
		$date = showdate($order_date, "d M o");
		$awb_no = $SQ_shop_order['order_tracking'];
		$courier_id = $SQ_shop_order['shop_courier_id'];
		$time = date('h:i A', strtotime($order_date));
		$msg = "Your order has been placed";
		$time_sqlcomplete = $date." | ".$time;
		$sqlcomplete = "1";
		
		
		// Order is in Dispatched-Ready
		if($sqlcomplete){
			$sqld1 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched-ready%' AND review_section_id='$order_id'");
			$sqlsdat1 = $sqld1['c_date'];
			$datetimearray2 = explode(" ", $sqlsdat1);
			$date2 = $datetimearray2[0];
			$time2 = $datetimearray2[1];
			$reformatted_date2 = date('d-m-Y',strtotime($date2));
			$reformatted_time2 = date('Gi.s',strtotime($time2));
			if($sqlsdat1 == ''){
				$sqlcomplete1 = "";
			}else{
				$msg = "Your order has been packed";
				$time_sqlcomplete =  $date2. ' | ' .$time2;
				$sqlcomplete1 = "1";
			}
		}
		
		
		//Order is in Dispatched
		if($sqlcomplete1){
			$sqld2 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%dispatched%' AND review_section_id='$order_id'");
			$sqlsdat3 = $sqld2['c_date'];
			$datetimearray3 = explode(" ", $sqlsdat3);
			$date3 = $datetimearray3[0];
			$time3 = $datetimearray3[1];
			$reformatted_date3 = date('d-m-Y',strtotime($date3));
			$reformatted_time3 = date('Gi.s',strtotime($time3));
			if($sqlsdat3 == ''){
				$sqlcomplete2 = "";
			}else{
				$msg = "Your order has been dispatched from warehouse";
				$time_sqlcomplete =   $date3. ' | ' .$time3;
				$sqlcomplete2 = "1";
			}
		}
		
		// Courier status
		if($sqlcomplete2){
			$courrier_sql = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%rto-transit%' AND review_section_id='$order_id'");
			if($courrier_sql['c_date']==""){
				$courrier_sql = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%delivered%' AND review_section_id='$order_id'");
				$sqlsdat_rto = $courrier_sql['c_date'];
				$date4 = showdate($sqlsdat_rto, "d M o");
				$time4 = date('h:i A', strtotime($sqlsdat_rto));
				if($sqlsdat_rto == ''){
					$sqlcomplete3 = "";
				}else{
					$msg = "Your order has been delivered";
					$time_sqlcomplete =   $date4. ' | ' .$time4;
					$sqlcomplete3 = "1";
				}
			}else{
				$sqlsdat_rto = $courrier_sql['c_date'];
				$date4 = showdate($sqlsdat_rto, "d M o");
				$time4 = date('h:i A', strtotime($sqlsdat_rto));
				if($sqlsdat_rto == ''){
					$sqlcomplete3 = "";
				}else{
					$msg = "Your order has been return to origin";
					$time_sqlcomplete =   $date4. ' | ' .$time4;
					$sqlcomplete3 = "1";
				}
			}
			
		}
		//End
		
		// Order is in Delivered
		if($sqlcomplete3){
			$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%Delivered%' AND review_section_id='$order_id'");
			if($sqld4['c_date']== ''){
				$sqld4 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%delivered%' AND review_section_id='$order_id'");
				$sqlsdat4 = $sqld4['c_date'];	
			}
			$sqlsdat4 = $sqld4['c_date'];
			$date5 = showdate($sqlsdat4, "d M o");
			$time5 = date('h:i A', strtotime($sqlsdat4));
			if($sqlsdat4 == ''){
				$sqlcomplete4 = "";
			}else{
				$msg = "Your order has been delivered";
				$time_sqlcomplete =   $date5. ' | ' .$time5;
				$sqlcomplete4 = "1";
			}
		}

		
		//Order is Cancelled
		$sqld5 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%cancelled%' AND review_section_id='$order_id'");
		if($sqld5['c_date'] ==''){
			$sqld5 = query_execute_row("SELECT c_date FROM section_reviews WHERE review_name LIKE '%canceled%' AND review_section_id='$order_id'");
		}
		$sqlsdat5 = $sqld5['c_date'];
		$datetimearray5 = explode(" ", $sqlsdat5);
		$date7 = $datetimearray5[0];
		$time7 = $datetimearray5[1];
		$reformatted_date5 = date('d-m-Y',strtotime($date7));
		$reformatted_time5 = date('Gi.s',strtotime($time7));
		if($sqlsdat5 == ''){
			$status_can = "";
		}else{
			$msg = "Your order has been cancelled";
			$time_sqlcomplete =   $date7. ' | ' .$time7;
			$status_can = "1";
		}
	}
	//End
	}else{
	header("location: $baseUrl");
}
}else{
	header("location: $baseUrl");
}
?>
<html>
<head>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type='text/javascript' src='https://www.dogspot.in/js/shaajax2.js'></script>
<script>
$(document).ready(function() {
	$('#myCircle li').click(function() {
		var id = $(this).attr("id");
		var orderId = "<?=$order_id?>";
		var url='https://m.dogspot.in/ajax-track-order.php?order_id='+orderId+'&id='+id;
		console.log(url);
		$.ajax({
			url:url,
			data:'',
			type:"Post",
			success:function(data){
						console.log(data);
						if(data.status==true){
							$(".track_order_message_green").html(data.msg);
							$(".timetest").html(data.time_sqlcomplete);
							$(".order_tracking_process_aero li").remove();
							$(".order_tracking_process_aero").html(data.arrow).append();
						}else{
							$(".track_order_message_green").html(data.msg);
							$(".timetest").html('');
							$(".timetest").html(data.time_sqlcomplete);
							$(".order_tracking_process_aero li").remove();
							$(".order_tracking_process_aero").html(data.arrow).append();
						}
					},
			error:function(xhr, ajaxOptions, thrownError){
						console.log("error");
						console.log(xhr);
					},
			complete:function()	{
						// complete
					},
					dataType:"json",
			});
		});
	});
</script>
 <?php require_once('common/script.php'); ?>
	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
	 <?php require_once('common/top.php'); ?>
      <div class="content_trackorder">
		<div class="track_order_blk">
        <div id="status_msg1"></div>
		<div class="order_summery_box">Order Summary</div>
        <ul class="track_order_list">
        <li>
			<div class="track_order_col1">Delivery Address</div>
			<div class="track_order_col2">
			<div><strong><?=$del_name;?></strong></div>
			<div><?=$full_address;?></div>
			<div><?=$city;?>,</div>
			<div><?=$state;?></div> 
			<div><?=$mobile;?></div> 
			</div>
        </li>
		<li>
			<div class="track_order_col1">Grand Total</div>
			<div class="track_order_col2">
			<div><strong>Rs. <?=number_format($order_amount,0); ?></strong><? if($status==0){?> paid <? }else{?> unpaid<? }?> through <strong><?=$AOrderMethod[$order_method]; ?></strong></div>
			</div>
        </li>
		<!--<li>
			<div class="track_order_col1">Status</div>
			<div class="track_order_col2">
				<div><?php //$order_status; ?> </div>
			</div>
        </li>-->
		<li>
			<div class="track_order_col1">Order ID</div>
			<div class="track_order_col2">
				<div><?=$order_id; ?></div>
			</div>
        </li>
		<li>
			<div class="track_order_col1">Order Date</div>
			<div class="track_order_col2">
				<div><?=$order_c_date; ?></div>
			</div>
        </li>
        </ul>
        </div>	
		<div class="track_order_blk">
        <div class="order_summery_box">Track Order</div>
        <ul class="order_tracking_process">
			<li>Approval</li>
			<li>Processing</li>
			<li>Dispatched</li>
			<li>In Transit</li>
			<li>Delivered</li>
       </ul>
		<ul class="order_tracking_process track_order" id="myCircle">
			<li id="approval"><label <?php if($sqlcomplete!="" && $sqlcomplete1=="" && $status_can==""){ ?>class="track_circle_img track_circle_big"<?php }else{ ?>class="track_circle_img track_circle_green"<? }?>></label></li>
			<li <? if($status_can!=""){?>id="cancelled"<? }else{?>id="processing"<? } ?>><label <?php if($sqlcomplete1!="" && $sqlcomplete2=="" && $status_can==""){ ?>class="track_circle_img track_circle_big"<?php }elseif($status_can!=""){ ?>class="track_circle_img track_circle_red"<?php }elseif($sqlcomplete1==""){?>class="track_circle_img track_circle_gray"<?php }else{?>class="track_circle_img track_circle_green"<? }?>></label></li>
			<li id="dispatched"><label <?php if($sqlcomplete2!="" && $sqlcomplete3=="" && $status_can==""){ ?>class="track_circle_img track_circle_big"<?php }elseif($sqlcomplete2==""){?>class="track_circle_img track_circle_gray"<?php }else{?>class="track_circle_img track_circle_green"<? }?>></label></li>
			<li id="inTransit"><label <?php if($sqlcomplete3!="" && $sqlcomplete4=="" && $status_can==""){ ?>class="track_circle_img track_circle_big"<?php }elseif($status_can!=""){ ?>class="track_circle_img track_circle_gray"<?php }else{?>class="track_circle_img track_circle_gray"<? }?>></label></li>
			<li id="delivered"><label <?php if($sqlcomplete4!=""){ ?>class="track_circle_img track_circle_big"<?php }else{ ?>class="track_circle_img track_circle_gray"<?php }?>></label></li>
		</ul>
		<ul class="order_tracking_process_aero">
			<li><label <?php if($sqlcomplete!="" && $sqlcomplete1=="" && $status_can==""){ ?>class="track_arrow_active"<?php }elseif($changeActiveState){ ?>class="track_arrow_active"<?php }else{?>class="track_arrow"<? } ?>></label></li>
			<li><label <?php if($sqlcomplete1!="" && $sqlcomplete2=="" && $status_can==""){ ?>class="track_arrow_active"<?php }elseif($status_can!=""){ ?>class="track_arrow_active"<?php }else{?>class="track_arrow"<?php }?>></label></li>
			<li><label <?php if($sqlcomplete2!="" && $sqlcomplete3=="" && $status_can==""){ ?>class="track_arrow_active"<?php }else{ ?>class="track_arrow"<?php }?>></label></li>
			<li><label <?php if($sqlcomplete3!="" && $sqlcomplete4=="" && $status_can==""){ ?>class="track_arrow_active"<?php }else{ ?>class="track_arrow"<?php }?>></label></li>
			<li><label <?php if($sqlcomplete4!=""){ ?>class="track_arrow_active"<?php }else{ ?>class="track_arrow"<?php }?>></label></li>
		</ul>
		<ul class="track_order_message">
			<!--<li><p class="track_order_message_green">Your Item has been placed</p><small>Thu, Jun 04, 2015 10:50 AM </small></li>-->
			<li id="status_msg"><p class="track_order_message_green"><?=$msg;?></p><small><span class="timetest"><?=$time_sqlcomplete;?></span></small></li>
		</ul>
		</div>
    </div>
      
	<?php require_once('common/bottom.php'); ?>