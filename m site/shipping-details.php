  <?php 
 /* ini_set("display_errors", "1");
  error_reporting(E_ALL);*/
require_once('constants.php'); 
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');?>
  <!DOCTYPE html>
<html>
<head>
 <?php require_once('common/script.php'); ?>
<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
<meta property="og:title" content="<?=$pageDesc?>"/>
<meta property="og:type" content="website"/>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

      <?php require_once('common/top.php'); ?>
      <div class="product_content">
<div><strong>Shipping Charges</strong></div>
<div>Free Shipping on all orders above Rs. 500*</div>
<div><strong>*Terms and Conditions</strong></div>
<div>1. Dog food products would be charged itemized shipping charge of Rs. 75/quantity</div>
<div>2. If the order value is more than or equal to Rs. 500 with no dog food and cat litter then shipping would be FREE</div>
<div>3. If the order value is less then Rs. 500 with no dog food or cat litter in the order then shipping would be Rs. 49</div>
<div>4. If the order has dog food or cat litter + any other items then the itemized shipping will be applicable Rs. 75/quantity of dog food and cat litter</div>
		
		<div data-role="footer" data-position="fixed" class="ui-footer ui-footer-fixed ui-bar-inherit slideup" style="background:#f2f2f2;" role="contentinfo">
						<a href="cart.php" data-ajax="false" style="width: 100%; border: none;" class="ui-link ui-btn ui-shadow ui-corner-all" data-role="button" role="button">
						                        <div class="green_button" id="placeOrder" style="margin: 0px; width:99%;">Back</div></a>
                        </div>
		<?php require_once('common/bottom.php');  ?>
	  