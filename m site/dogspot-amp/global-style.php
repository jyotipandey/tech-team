<style amp-custom>
        /* GLOBAL STYLES */
		/**---------------------
		  *
		  * Resets
		  *
		  *---------------------**/
		figure{margin: 0}
		fieldset{border:none;padding:0;margin: 0}
		*{box-sizing: border-box}
		button{background: none; border: none}
		a{text-decoration: none}
		:focus{outline: 0;}
		ul{padding-left: 20px}

		/**---------------------
		  *
		  * Global Styles
		  *
		  *---------------------**/
		html{font-size: 62.5%; box-sizing: border-box;}
		body{font-size: 1.3rem; line-height: 1.8; -webkit-font-smoothing: antialiased; color: #3e3e3e;}

		.font-1, html{font-family: 'Roboto', serif; font-weight: 300}
		.font-2{font-family: 'Oswald', sans-serif;}

		.text-center{text-align: center}
		.margin-0{margin: 0}
		.margin-top-0{margin-top: 0}
		.margin-bottom-0{margin-bottom: 0}
		.margin-left-0{margin-left: 0}
		.margin-right-0{margin-right: 0}
		.padding-left-0{padding-left: 0}
		.padding-right-0{padding-right: 0}
		.minus-margin-top-bottom-15{margin-top: -15px; margin-bottom: -15px}

		[class*="col-"].margin-bottom-0{margin-bottom: 0}

		.margin-top-80{margin-top: 80px}
		.margin-top-50{margin-top: 50px}
		.margin-top-minus-30{margin-top: -30px}

		.space{height: 10px;}
		.space-2{height: 20px;}
		.space-3{height: 30px;}

		.divider{margin: 13px 0;}
		.divider-30{margin: 30px 0;}
		.divider.colored{height: 1px; background: rgba(0,0,0,.12)}
		.divider-30.colored{height: 1px; background: rgba(0,0,0,.12)}

		.width-100{width: 100%}
		.width-90{width: 90%}
		.width-80{width: 80%}
		.width-70{width: 70%}

		.pull-left{float: left}
		.pull-right{float: right}

		.d-block{display: block}
		.d-inline-block{display: inline-block}
		.d-inline{display: inline}

		.clear-both{clear:both}

		.clearfix:after,
		.clearfix:before {
			display: table;
			content: "";
			line-height: 0
		} .clearfix:after {clear: both}

		h2{margin-bottom: 7.5px}
		p{margin: 7.5px 0 0;}
		small{font-size: 1rem; line-height: 1}
		strong,b{font-weight: 700}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6{
			font-weight: 400;
			color: #212121;
			font-family: 'Oswald', sans-serif;
		}

		.thin{font-weight: 300}
		.thicc{font-weight: 700}

		h1{font-size: 2.7rem}
		h2{font-size: 2.1rem}
		h3{font-size: 1.7rem}
		h4{font-size: 1.4rem}
		h5{font-size: 1.2rem}
		h6{font-size: 1rem}

		.h1{font-size: 2.7rem}
		.h2{font-size: 2.1rem}
		.h3{font-size: 1.7rem}
		.h4{font-size: 1.4rem}
		.h5{font-size: 1.2rem}
		.h6{font-size: 1rem}

		a,
		.primary-color{color: #f82e56}
		.secondary-color{color: #e7a900}
		.light-color{color: #FFF}
		.light-color-2{color: rgba(255,255,255,.54)}
		.dark-color{color: #333030;}
		.ocean-color{color: #2b90d9;}
		.grass-color{color: #3ac569;}
		.salmon-color{color: #ff7473;}
		.sun-color{color: #feee7d;}
		.alge-color{color: #79a8a9;}
		.flower-color{color: #353866;}
		.grey-color{color: #c4c4c4;}

		.primary-bg{background: #f82e56}
		.secondary-bg{background: #e7a900}
		.light-bg{background: #fff;}
		.dark-bg{background: #333030;}
		.ocean-bg{background: #2b90d9;}
		.grass-bg{background: #3ac569;}
		.salmon-bg{background: #ff7473;}
		.sun-bg{background: #feee7d;}
		.alge-bg{background: #79a8a9;}
		.flower-bg{background: #353866;}
		.grey-bg{background: #c4c4c4;}

		.circle{border-radius: 50%}

		[dir="rtl"] .pull-left{float: right}
		[dir="rtl"] .pull-right{float: left}
		body {text-align: left}
		body[dir="rtl"] {text-align: right}

		.text-center{text-align: center}

		code {
			padding: .2rem .4rem;
			font-size: 90%;
			color: #bd4147;
			background-color: #f7f7f9;
			border-radius: .25rem;
		}

		h2:only-child{margin: 0}

		/**---------------------
		  *
		  * Header Styles
		  *
		  *---------------------**/
		  .head-nav .fa{ margin-right:20px}
		header{
			position: relative;
			min-height: 51px;
			padding: 0 5px;
			background: #fff;
		}

		header .fa{
			color: #333;
			opacity: .87;
			font-size: 17px;
			line-height: 52px;
			height: 51px;
			padding: 0 15px;
			margin: 0;
		}

		header a:last-child{
			position: relative;
		}

		header a:last-child span{
			position: absolute;
			top: 3px;
			right: 9px;
			font-size: 13px;
			color: #333;
			font-weight: 400;
		}

		.toast {
			display: block;
			position: relative;
			height: 51px;
			padding-left: 15px;
			padding-right: 15px;
			width: 49px;
		}

		.toast:after,
		.toast:before,
		.toast span {
			position: absolute;
			display: block;
			width: 19px;
			height: 3px;
			border-radius: 2px;
			background-color: #555;
			-webkit-transform: translate3d(0,0,0) rotate(0deg);
			transform: translate3d(0,0,0) rotate(0deg);
		}

		.toast:after,
		.toast:before {
			content: '';
			width: 16px;
			left: 15px;
			-webkit-transition: all ease-in .4s;
			transition: all ease-in .4s;
		}

		.toast span {
			opacity: 1;
			top: 24px;
			-webkit-transition: all ease-in-out .4s;
			transition: all ease-in-out .4s;
		}

		.toast:before {
			top: 17px;
		}

		.toast:after {
			top: 31px;
		}

		#logo{
			display: block;
            line-height: 1;

			position: absolute;
			top: 50%;

			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
	margin-left: 90px;
    margin-top: 3px;
		}

		#logo img{
			display: block;
		}

		/**---------------------
		  *
		  * Sidebar Styles
		  *
		  *---------------------**/
		#mainSideBar{
			min-width: 300px;
			padding-bottom: 30px;
		}

		#mainSideBar > div:not(.divider){
			padding: 17px 20px;
		}

		#mainSideBar figure{
			width: 300px;
			max-width: 100%;
			padding: 20px;
			position: relative;
		}

		#mainSideBar button{
			position: absolute;
			right: 20px;
			top: 20px;
		}

		#mainSideBar amp-img{
			margin-bottom: 5px;
		}

		#mainSideBar h3,
		#mainSideBar h5{
			margin: 0;
			line-height: 1.5;
		}

		#menu{margin-top: 15px}
		#menu div{padding: 0}

		#menu h6,
		#menu a{
			color: inherit;
			font-size: 1.3rem;
			font-weight: 300;
			padding:0;
			border: none;
		}

		#menu a,
		#menu span{
			padding: 14px 20px 14px 53px;
			display: block;
			color: inherit;
			position: relative;

			-webkit-transition: all ease-in-out .2s;
			transition: all ease-in-out .2s;
		}

		#menu section[expanded] > h6 span{
			background-color: rgba(0,0,0,.06);
			color: #f82e56;
		}

		#menu h6 span:after{
			position: absolute;
			right: 20px;
			top: 0;
			font-family: 'FontAwesome';
			font-size: 12px;
			line-height: 47px;
			content: '\f0dd';
		}

		#menu i,
		#mainSideBar li i{
			font-size: 1.7rem;
			position: absolute;
			left: 20px;
		}

		.social-ball{
			font-size: 1.6rem;
			display: inline-block;
			text-align: center;
			line-height: 30px;
			height: 30px;
			width: 30px;
			border-radius: 50%;
			color: #FFF;
			margin-right: 5px;
		}

		.sample-icons [class*="shopping-"]{
			font-size: 2.7rem;
		}

		.sample-icons .class-name{
			font-size: 1.1rem;
		}

		.sample-icons .col-xs-3:nth-child(4n + 1){
			clear: left
		}

		.social-ball.fa-facebook{background-color: #4867AA}
		.social-ball.fa-twitter{background-color: #00ACED}
		.social-ball.fa-linkedin{background-color: #0177B5}
		.social-ball.fa-behance{background-color: #010103}
		.social-ball.fa-dribbble{background-color: #E04C86}

		/**---------------------
		  *
		  * Badges
		  *
		  *---------------------**/
		.badge{
			font-weight: 400;
			font-size: 1.2rem;
			color: #FFF;
			line-height: 1.8rem;
			height: 1.8rem;
			padding: 0 5px;
			display: inline-block;
		}


		/**---------------------
		  *
		  * Background Row
		  *
		  *---------------------**/
		.background-row{
			background-image: url('https://img.mobius.studio/themes/bones/LTR/assets/img/row_bg_375x250.jpg');
			background-size: cover;
			background-position: center center;
			padding: 60px 0;
		}

		.colored-row{
			background-color: #f4f2f3
		}

		.inside-out-long-img{
			margin-right: 25px;
		}


		.inside-out-long-img amp-img{
			margin-bottom: -90px;
            margin-top: 10px;
		}

		.background-row-content{
			background-color: rgba(255,255,255,.87);
			display: inline-block;
			outline: 1px solid rgba(255,255,255,.87);
		    outline-offset: 3px;
            padding: 25px;
		}

		.background-row-content .row-1,
		.background-row-content .row-2,
		.background-row-content .row-3{
			margin: 0;
			font-weight: 600;
            line-height: 1;
		}

		.background-row-content .row-1{
			font-size: 17px;
		}

		.background-row-content .row-2{
			font-size: 25px;
			margin: 20px 0;
		}

		.background-row-content .row-3{
			font-size: 12px;
			font-weight: 400
		}


		/**---------------------
		  *
		  * Shopping Line Icons
		  *
		  *---------------------**/
		@font-face{font-family:shopping;src:url(assets/fonts/shopping.eot);src:url(assets/fonts/shopping.eot?#iefix) format("embedded-opentype"),url(assets/fonts/shopping.woff) format("woff"),url(assets/fonts/shopping.ttf) format("truetype"),url(assets/fonts/shopping.svg#shopping) format("svg");font-weight:400;font-style:normal}@media screen and (-webkit-min-device-pixel-ratio:0){@font-face{font-family:shopping;src:url(assets/fonts/shopping.svg#shopping) format("svg")}}[class*=" shopping-"]:before,[class^=shopping-]:before{font-family:shopping;font-size:inherit;font-style:normal}.shopping-alcohol-bottle:before{content:"\f100"}.shopping-ascending-line-chart:before{content:"\f101"}.shopping-atm-machine:before{content:"\f102"}.shopping-bag-for-woman:before{content:"\f103"}.shopping-baseball-cap:before{content:"\f104"}.shopping-big-barcode:before{content:"\f105"}.shopping-big-cargo-truck:before{content:"\f106"}.shopping-big-diamond:before{content:"\f107"}.shopping-big-diamond-1:before{content:"\f108"}.shopping-big-gift-box:before{content:"\f109"}.shopping-big-glasses:before{content:"\f10a"}.shopping-big-license:before{content:"\f10b"}.shopping-big-paper-bag:before{content:"\f10c"}.shopping-big-piggy-bank:before{content:"\f10d"}.shopping-big-search-len:before{content:"\f10e"}.shopping-big-shopping-trolley:before{content:"\f10f"}.shopping-big-television-with-two-big-speakers:before{content:"\f110"}.shopping-big-wall-clock:before{content:"\f111"}.shopping-blank-t-shirt:before{content:"\f112"}.shopping-bra:before{content:"\f113"}.shopping-calculator-symbols:before{content:"\f114"}.shopping-car-facin-left:before{content:"\f115"}.shopping-cash-bill:before{content:"\f116"}.shopping-cash-machine:before{content:"\f117"}.shopping-cash-machine-open:before{content:"\f118"}.shopping-check-machine:before{content:"\f119"}.shopping-checklist-and-pencil:before{content:"\f11a"}.shopping-cloakroom:before{content:"\f11b"}.shopping-closed-package:before{content:"\f11c"}.shopping-consultation:before{content:"\f11d"}.shopping-credential:before{content:"\f11e"}.shopping-cut-credit-card:before{content:"\f11f"}.shopping-delivery-trollery:before{content:"\f120"}.shopping-delivery-truck-facing-right:before{content:"\f121"}.shopping-disco-light:before{content:"\f122"}.shopping-discount-badge:before{content:"\f123"}.shopping-dollar-bag:before{content:"\f124"}.shopping-dollar-bill-from-automated-teller-machine:before{content:"\f125"}.shopping-electric-scalator:before{content:"\f126"}.shopping-fork-plate-and-knife:before{content:"\f127"}.shopping-gold-stack:before{content:"\f128"}.shopping-hambuger-with-soda:before{content:"\f129"}.shopping-hanger-with-towel:before{content:"\f12a"}.shopping-heart-with-line:before{content:"\f12b"}.shopping-high-hills-shoe:before{content:"\f12c"}.shopping-horn-with-handle:before{content:"\f12d"}.shopping-inclined-comb:before{content:"\f12e"}.shopping-inclined-open-umbrella:before{content:"\f12f"}.shopping-ladies-purse:before{content:"\f130"}.shopping-lipstick-with-cover:before{content:"\f131"}.shopping-long-dress:before{content:"\f132"}.shopping-long-pants:before{content:"\f133"}.shopping-manager-profile:before{content:"\f134"}.shopping-mannequin-with-necklace:before{content:"\f135"}.shopping-map-with-placeholder-on-top:before{content:"\f136"}.shopping-market-shop:before{content:"\f137"}.shopping-megaphone-facing-right:before{content:"\f138"}.shopping-men-shorts:before{content:"\f139"}.shopping-necklace:before{content:"\f13a"}.shopping-one-shoe:before{content:"\f13b"}.shopping-open-box:before{content:"\f13c"}.shopping-open-sign:before{content:"\f13d"}.shopping-parking-sign:before{content:"\f13e"}.shopping-perfume-bottle-with-cover:before{content:"\f13f"}.shopping-phone-ringing:before{content:"\f140"}.shopping-purse-with-bills:before{content:"\f141"}.shopping-purse-with-bills-1:before{content:"\f142"}.shopping-recepcionist:before{content:"\f143"}.shopping-recorder-machine:before{content:"\f144"}.shopping-road-banner:before{content:"\f145"}.shopping-round-bag:before{content:"\f146"}.shopping-round-bracelet:before{content:"\f147"}.shopping-sale-label:before{content:"\f148"}.shopping-security-cam:before{content:"\f149"}.shopping-shirt-and-tie:before{content:"\f14a"}.shopping-shopping-on-computer:before{content:"\f14b"}.shopping-shopping-on-smarphone:before{content:"\f14c"}.shopping-shopping-on-tablet:before{content:"\f14d"}.shopping-shopping-paper-bag:before{content:"\f14e"}.shopping-shopping-trolley-full:before{content:"\f14f"}.shopping-short-bag:before{content:"\f150"}.shopping-short-skirt:before{content:"\f151"}.shopping-sign-check:before{content:"\f152"}.shopping-small-sofa:before{content:"\f153"}.shopping-store-purchase:before{content:"\f154"}.shopping-supermarket-basket:before{content:"\f155"}.shopping-three-alcoholic-bottles:before{content:"\f156"}.shopping-three-coins:before{content:"\f157"}.shopping-three-ties:before{content:"\f158"}.shopping-two-boots:before{content:"\f159"}.shopping-two-credit-cards:before{content:"\f15a"}.shopping-two-earrings:before{content:"\f15b"}.shopping-two-hangers:before{content:"\f15c"}.shopping-two-rings:before{content:"\f15d"}.shopping-two-socks:before{content:"\f15e"}.shopping-wc-sign:before{content:"\f15f"}.shopping-wide-calculator:before{content:"\f160"}.shopping-women-hat:before{content:"\f161"}.shopping-women-shirt-with-sun:before{content:"\f162"}.shopping-women-swimsuit:before{content:"\f163"}


		/**---------------------
		  *
		  * Products Grid
		  *
		  *---------------------**/
		.bones-products-grid{
			margin: -7.5px;
			position: relative;
		}

		.bones-products-grid:after{
			content: '';
			display: table;
			clear: both;
		}

		.bones-products-grid > div{
			float: left;
			padding: 7.5px;
		}

		.bones-products-grid.cols-2 > div{width: 50%;}
		.bones-products-grid.cols-3 > div{width: 33.333333333333333%;}
		.bones-products-grid.cols-4 > div{width: 25%;}

		@media all and (min-width: 376px) {
			.bones-products-grid.cols-2 > div{width: 33.333333333333333%;}
			.bones-products-grid.cols-3 > div{width: 25%;}
			.bones-products-grid.cols-4 > div{width: 20%;}
		}

		@media all and (min-width: 768px) {
			.bones-products-grid.cols-2 > div{width: 25%;}
			.bones-products-grid.cols-3 > div{width: 20%;}
			.bones-products-grid.cols-4 > div{width: 16.666666666666667%;}
		}

		/**---------------------
		  *
		  * Grid
		  *
		  *---------------------**/
		[class*="col-"]{margin-bottom: 30px;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{margin-right:-15px;margin-left:-15px}.row:after,.row:before{display:table;content:" "}.row:after{clear:both}.container-full,.container-full [class*="col-"]{padding-left: 0; padding-right: 0;}.container-full .row{margin-left:0; margin-right:0;}.no-gap [class*="col-"]{padding-right: 0;padding-left: 0;margin-bottom: 0;}.no-gap.row{margin-right: 0; margin-left: 0;}.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{float:left}.col-xs-12{width:100%}.col-xs-11{width:91.66666667%}.col-xs-10{width:83.33333333%}.col-xs-9{width:75%}.col-xs-8{width:66.66666667%}.col-xs-7{width:58.33333333%}.col-xs-6{width:50%}.col-xs-5{width:41.66666667%}.col-xs-4{width:33.33333333%}.col-xs-3{width:25%}.col-xs-2{width:16.66666667%}.col-xs-1{width:8.33333333%}@media (min-width:768px){.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-11{width:91.66666667%}.col-sm-10{width:83.33333333%}.col-sm-9{width:75%}.col-sm-8{width:66.66666667%}.col-sm-7{width:58.33333333%}.col-sm-6{width:50%}.col-sm-5{width:41.66666667%}.col-sm-4{width:33.33333333%}.col-sm-3{width:25%}.col-sm-2{width:16.66666667%}.col-sm-1{width:8.33333333%}}



		/**---------------------
		  *
		  * AMP Lightbox
		  *
		  *---------------------**/
		amp-lightbox{background-color: rgba(0,0,0,.87)}
		amp-lightbox.light{background-color: rgba(255,255,255,.87)}

		amp-lightbox > .lightbox{
			position: absolute;
			bottom: 0;
			top: 0;
			right: 0;
			left: 0;
		}

		amp-lightbox .middle{
			width: 80%;
			position: absolute;
			top: 50%;
			left: 50%;

			-webkit-transform: translate(-50%, -50%);
			-moz-transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			-o-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
		}

		amp-lightbox .message{
			text-align: center;
		}

		amp-lightbox h1,
		amp-lightbox small{
			color: #FFF
		}

		amp-lightbox.light h1,
		amp-lightbox.light small{
			color: #212121
		}

		amp-lightbox small{
			font-size: 1.2rem;
		}

        /* /GLOBAL STYLES */

        /* INDEX PAGE STYLES */

        /**---------------------
		  *
		  * Blog Item
		  *
		  *---------------------**/
		.blog-item .preview {
			display: block;
			margin-bottom: 15px
		}

		.subtitle{font-size: 1.2rem}

        /**---------------------
		  *
		  * Pagination
		  *
		  *---------------------**/
		.pagination {
			text-align: center;
			margin: 15px 0 0
		}

		.pagination a {
			display: inline-block;
			margin: 0;
			border-radius: 50%;
			color: #212121;
			min-width: 30px;
			min-height: 30px;
			line-height: 30px;
		}

		.pagination a.active {
			color: #FFF;
			background-color: #f82e56
		}

        /**---------------------
		  *
		  * Sidebar Box
		  *
		  *---------------------**/
		.blog-sidebar-box{margin-bottom: 30px;}

        /**---------------------
		  *
		  * Photo Row
		  *
		  *---------------------**/
		.photo-row {
			margin: 15px -2.5px 0
		}

		.photo-row a {
			width: 33.33333333333333%;
			padding: 0 2.5px;
			float: left;
			display: block
		}

		.media-list{
			list-style: none;
			padding: 0;
			border-bottom: 1px solid rgba(0,0,0,.06);
		}

		.media-list a{
			position: relative;
			display: block;
		}

		.media-list i{
			position: absolute;
			right: 5px;
			top: 50%;
			line-height: 10px;
			margin-top: -17px;
			display: block;
		}

		.media-list amp-img{
			display: block;
		}

		.media-list div{
			margin-left: 80px;
			padding-bottom: 15px;
			margin-bottom: 15px;
		}

		.media-list li:not(:last-child) div{
			border-bottom: 1px solid rgba(0,0,0,.06);
		}

        
		.bordered-list{
			padding-left: 0;
			list-style: none;
		}

		.bordered-list a{
			color: inherit;
			display: block;
			position: relative;
			padding: 10px 15px 8px 0;
			border-bottom: 1px solid rgba(0,0,0,.06);
		}

		.bordered-list a:after{
			position: absolute;
			right: 5px;
			top: 0;
			font-size: 12px;
			line-height: 47px;
			font-weight: 500;
			content: '+';
		}

        

        /* /INDEX PAGE STYLES */
		/*font awsome*/
		@font-face {
    font-family: FontAwesome;
    src: url(//use.fontawesome.com/releases/v4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0);
    src: url(//use.fontawesome.com/releases/v4.7.0/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'), url(//use.fontawesome.com/releases/v4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'), url(//use.fontawesome.com/releases/v4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'), url(//use.fontawesome.com/releases/v4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'), url(//use.fontawesome.com/releases/v4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');
    font-weight: 400;
    font-style: normal
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale
}
.fa-facebook-f:before,
.fa-facebook:before {
    content: "\f09a"
}

.fa-book:before {
    content: "\f02d"
}

.fa-calendar-o:before {
    content: "\f133"
}

.fa-comment:before {
    content: "\f075"
}

.fa-twitter:before {
    content: "\f099"
}

.fa-linkedin:before {
    content: "\f0e1"
}

.fa-google-plus:before {
    content: "\f0d5"
}

.fa-caret-left:before {
    content: "\f0d9"
}

.fa-home:before {
    content: "\f015"
}

.fa-inr:before,
.fa-rupee:before {
    content: "\f156"
}

.fa-star:before {
    content: "\f005"
}

.fa-map-marker:before {
    content: "\f041"
}
.fa-shopping-cart:before {
    content: "\f07a";
}
.fa-paw:before {
    content: "\f1b0";
}
.fa-caret-left:before {
    content: "\f0d9";
}

    </style>