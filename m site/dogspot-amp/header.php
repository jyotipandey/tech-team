<header itemscope itemtype="https://schema.org/WPHeader">
        <button class="toast pull-left" on="tap:mainSideBar.toggle"><span></span></button>
        <a id="logo" href="index.html"><amp-img src="https://m.dogspot.in/dogspot-amp/assets/img/logo.png" width="100" height="25"></amp-img></a>
        <a class="pull-right" href="cart.html"><i class="fa fa-shopping-cart"></i><span>3</span></a>
    </header>