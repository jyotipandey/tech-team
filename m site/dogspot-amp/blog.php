<?php
$baseURL='/home/dogspot/public_html';
include("../constants.php");
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/functions2.php');
require_once(SITEMAIN_URL.'/banner1.php');

header("Content-type: text/html; charset=iso-8859-1"); 
$ecomm_pagetype='articles';  
$filter=$section[1];
$sel_drop_categories_check=query_execute_row("SELECT DISTINCT(wptr.term_taxonomy_id) , wtt.term_id , wt.name , wt.slug FROM wp_term_relationships as wptr , wp_term_taxonomy as wtt , wp_terms as wt WHERE wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wt.term_id=wptr.term_taxonomy_id AND slug='$filter'");
$articlecat_id=$sel_drop_categories_check['term_id'];			   
if ($filter) {
    if ($filter == 'justin') {  
	    $pageT  = 'justin';
        $pageT1 = 'justin';
        
        $pageh1    = "Just in - Dog Blog & Articles";
        $pageKey   = "$pageT, Dog Blog, & Articles, Dogs India";
        $pageTitle = "$pageT, Dog Blog, Dog Article, Dogs India DogSpot.in";
		$pageDesc  = "Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in";    
        $sql       = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_author=wu.ID GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,10";
    } else if ($filter == 'popular') {
		 $pageT  = 'popular';
        $pageT1 = 'popular';
        
        $pageh1      = "Most Popular - Dog Blog & Articles";
        $pageKey     = "$pageT, Dog Blog, & Articles, Dogs India";
        $pageTitle   = "$pageT, Dog Blog, Dog Article, Dogs India DogSpot.in";
		$pageDesc  = "Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in";
        $sql         = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_author=wu.ID GROUP BY wpp.ID ORDER BY wpp.views DESC LIMIT 0,10";		
    } elseif($articlecat_id!=''){
	$filter = preg_replace('/\-/', ' ', $filter);
	$pageTitle = "$filter, Dog Blog, Dog Article, Dogs India DogSpot.in";
    $pageh1    = ucwords($filter)." - Dog Blog & Articles";
    $pageKey   = "$filter, Dog Blog, & Articles, Dogs India";
    $pageDesc  = "$filter Dog Blog, & Articles from Dogs Experts Dogs India DogSpot.in";	
 $sql   = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wptr.term_taxonomy_id='$articlecat_id' AND wpp.post_status = 'publish' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_author=wu.ID GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,10";		
		}else {
        header("HTTP/1.0 404 Not Found");
        require_once($DOCUMENT_ROOT . '/404.php');
        exit();
    }
}else{
     $pageTitle = "Dog Blogs and Articles for Every Pet Lover | DogSpot.in";
    $pageh1    = "Dog Blog & Articles";
    $pageKey   = "Dog Blog Category, & Articles, Dogs India";
    $pageDesc  = "Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in";
				$sql="SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wpp.post_name, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wpp.post_author=wu.ID AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC limit 0,10";
}
?>
<!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title><?
echo "$pageTitle";if ($artuser) {echo "," . $artuser;}?></title>

    <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1,initial-scale=1">
    <meta name="keywords" content="<? $pageKey = str_replace('"', '', $pageKey);echo "$pageKey";if($artuser){echo ",". $artuser;}?>" />
<meta name="description" content="<? $pageDesc = str_replace('"', '', $pageDesc); echo "$pageDesc";if($artuser){echo ",". $artuser;}?>" />
    <? if($section[1]==''){?>
<link href="https://www.dogspot.in/dog-blog/" rel="canonical">
<? }else{?>
<link href="https://www.dogspot.in/dog-blog/<?=$section[1];?>/" rel="canonical">
<? }?>
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700|Roboto:300,400">

       <script async custom-element="amp-install-serviceworker" src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js">       </script>
	   <script async custom-element="amp-twitter" src="https://cdn.ampproject.org/v0/amp-twitter-0.1.js"></script>
        
        <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
        
        <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
        
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        
        <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
        <script async src="https://cdn.ampproject.org/v0.js"></script>

    
     <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<?php require_once($DOCUMENT_ROOT . '/dogspot-amp/global-style.php'); ?>
  </head>
<body dir="ltr">
    <!-- header-->
<?php require_once($DOCUMENT_ROOT . '/dogspot-amp/header.php'); ?>
    <!-- header-->
    <!-- breadcrum-->
    <div class="breadcrumb">
<div itemscope itemtype="http://schema.org/Breadcrumb"> 
<div itemscope itemtype="http://schema.org/BreadcrumbList">
<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<a href="/" itemprop="item"  data-ajax="false" ><span itemprop="name">Home</span></a>
<meta itemprop="position" content="1" /> </span>
<span> > </span>       
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <a href="/dog-blog/" itemprop="item"  data-ajax="false" ><span itemprop="name">Dog Blog</span></a>
       <meta itemprop="position" content="2" /> </span>           
             
</div>
</div>
</div>
    <!-- breadcrum-->
    <!-- CONTAINER-FLUID START -->

    <div class="container-fluid">
		<div class="space-2"></div>

		<div class="row">
			<div class="col-sm-12">
            <h1><?=$pageh1;?></h1>
				<div class="blog-item clearfix">
					<a href="blog-detail-sample-post.html" class="preview">
						<amp-img layout="responsive" src="https://img.mobius.studio/themes/bones/LTR/assets/img/blog_1_690x388.jpg" width=690 height=388></amp-img>
					</a>
					<a href="blog-detail-sample-post.html"><h3 class="margin-0">Sample Post</h3></a>

					<div class="subtitle">Posted on <a href="#">10 January 2015</a> in <a href="#">Web Design</a> by <a href="#">Mobius Studio</a></div>

					<div class="space"></div>

					<div class="details">
						<p>Compellingly incentivize intuitive infomediaries with reliable networks. Interactively incubate inexpensive sources rather than high-payoff networks. Competently cultivate high...</p>
					</div>

					<div class="space-2"></div>

					<div class="pull-right">
						<a href="blog-detail-sample-post.html">Go to Post&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>
					</div>
				</div><!-- BLOG-ITEM ENDS -->

				<div class="divider-30 colored"></div>

				<div class="blog-item clearfix">
					<amp-carousel class="preview" layout="fixed-height" height=194>
						<amp-img src="https://img.mobius.studio/themes/bones/LTR/assets/img/blog_2_690x388.jpg" width=345 height=194></amp-img>
						<amp-img src="https://img.mobius.studio/themes/bones/LTR/assets/img/blog_3_690x388.jpg" width=345 height=194></amp-img>
					</amp-carousel><!-- CAROUSEL ENDS -->

					<a href="blog-detail-carousel-post.html"><h3 class="margin-0">Carousel Post</h3></a>

					<div class="subtitle">Posted on <a href="#">10 January 2015</a> in <a href="#">Web Design</a> by <a href="#">Mobius Studio</a></div>

					<div class="space"></div>

					<div class="details">
						<p>Compellingly incentivize intuitive infomediaries with reliable networks. Interactively incubate inexpensive sources rather than high-payoff networks. Competently cultivate high...</p>
					</div>

					<div class="space-2"></div>

					<div class="pull-right">
						<a href="blog-detail-sample-post.html">Go to Post&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>
					</div>
				</div><!-- BLOG-ITEM ENDS -->

				<div class="divider-30 colored"></div>

				<div class="blog-item clearfix">
					<amp-youtube
							class="preview"
							data-videoid="ZHVJVQzHv5Q"
							layout="responsive"
							width="345" height="194"></amp-youtube>

					<a href="blog-detail-video-post.html"><h3 class="margin-0">Video Post</h3></a>

					<div class="subtitle">Posted on <a href="#">10 January 2015</a> in <a href="#">Web Design</a> by <a href="#">Mobius Studio</a></div>

					<div class="space"></div>

					<div class="details">
						<p>Compellingly incentivize intuitive infomediaries with reliable networks. Interactively incubate inexpensive sources rather than high-payoff networks. Competently cultivate high...</p>
					</div>

					<div class="space-2"></div>

					<div class="pull-right">
						<a href="blog-detail-sample-post.html">Go to Post&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>
					</div>
				</div><!-- BLOG-ITEM ENDS -->

				<div class="divider-30 colored"></div>

				<div class="blog-item clearfix">
					<amp-twitter class="preview"
					             width="345"
					             height="194"
					             layout="responsive"
					             data-tweetid="682699759069560834">
					</amp-twitter>

					<a href="blog-detail-sample-post.html"><h3 class="margin-0">Twitter Post</h3></a>

					<div class="subtitle">Posted on <a href="#">10 January 2015</a> in <a href="#">Web Design</a> by <a href="#">Mobius Studio</a></div>

					<div class="space"></div>

					<div class="details">
						<p>Compellingly incentivize intuitive infomediaries with reliable networks. Interactively incubate inexpensive sources rather than high-payoff networks. Competently cultivate high...</p>
					</div>

					<div class="space-2"></div>

					<div class="pull-right">
						<a href="blog-detail-sample-post.html">Go to Post&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>
					</div>
				</div><!-- BLOG-ITEM ENDS -->

				<div class="divider-30 colored"></div>

				<div class="pagination">
					<a href="blog.html" rel="prev" class="fa fa-angle-left"></a>
					<a href="blog.html" class="active">1</a>
					<a href="blog.html">2</a>
					<a href="blog.html">3</a>
					<a href="blog.html">4</a>
					<a href="blog.html" rel="next" class="fa fa-angle-right"></a>
				</div>
			</div><!-- COL-SM-9 ENDS -->

			<!-- COL-SM-3 ENDS -->
		</div><!-- ROW ENDS -->

	</div><!-- CONTAINER-FLUID ENDS -->

	<div class="container-fluid">
		
<div class="divider colored"></div>

            <div class="space-2"></div>

            <div class="text-center">
                <a href="#" class="social-ball fa fa-facebook"></a>
                <a href="#" class="social-ball fa fa-twitter"></a>
                <a href="#" class="social-ball fa fa-linkedin"></a>
                <a href="#" class="social-ball fa fa-behance"></a>
                <a href="#" class="social-ball fa fa-dribbble"></a>
            </div><!-- TEXT-CENTER/SOCIAL-ICONS ENDS -->

            <div class="space"></div>

            <div class="text-center">
                <small>Copyright © 2016 by Mobius Studio</small>
            </div><!-- TEXT-CENTER/COPYRIGHT ENDS -->

            <div class="space-2"></div>
	</div><!-- CONTAINER-FLUID ENDS -->

    <amp-sidebar id='mainSideBar' layout='nodisplay'>
        <figure class="light-bg head-nav" >
             <a href="index.html"><i class="fa fa-home" ></i>Home</a>
            

            <button on='tap:mainSideBar.toggle' class="fa fa-caret-left "></button>
        </figure><!-- NAVBAR USER CARD ENDS -->

        <nav id="menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
           <a href="/dog-breeds/"><i class="fa fa-paw"></i>Dog Breeds</a> 
<a href="/dog-blog/"> 
<i class="fa fa-paw"></i>Dog Blogs</a> 
<a  href="/dog-names/" > <i class="fa fa-paw"></i> Dog Names</a> 
<a href="/photos/" ><i class="fa fa-paw"></i> Dog Images</a> 
<a  href="/adoption/"> <i class="fa fa-paw"></i> Adoption</a>
           
            <amp-accordion>
                <section>
                    <h6><span><i class="fa fa-paw"></i>Community</span></h6>
                    <div>
                        <a href="/dog-listing/">Classifieds</a>
     <a  href="/dog-events/"> Dog Show</a>
     <a href="/qna/" >Q&amp;A  </a>
     <a href="/wagtag-info/">Wag Tag</a>
     <a href="/wag_club/"> Wag Club</a>
     
                    </div>
                </section>
            </amp-accordion>
            <amp-accordion>
                <section>
                    <h6><span><i class="fa fa-paw"></i>Shop</span></h6>
                    <div>
                         <a href="/dog-store/">Dog Store</a>
            <a href="/cats/">Cat Store </a>
          
        <a href="/birds/" >Bird Store</a>
        
        <a href="/small-pets/" >Small Pet</a>
        <a href="/monthly-essential-packs/" >Smart Deals</a>
         <a href="/sales/" >Sale</a>
                    </div>
                </section>
            </amp-accordion>
            
            
        </nav>

        <div class="divider colored"></div>

        <!-- CONTACT INFORMATION ENDS -->

        <!-- SOCIAL ICONS ENDS -->
    </amp-sidebar><!-- SIDEBAR ENDS -->

</body>
</html>