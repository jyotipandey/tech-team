<?php
header('Access-Control-Allow-Credentials: true');
header('AMP-Access-Control-Allow-Source-Origin: http://localhost');

if( isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) ){

	// $_POST['email'] is the users E-Mail address sent from AMP page
	// You can register this into your database or send it with email
	// Use it as you need

	header('Content-Type: application/json'); // Response content type must be json
	if( 1 == 1 ){
		http_response_code(200); // Success code for successful response is needed for proper message on AMP page
		echo json_encode(['subscription' => 'complete']); // You can use this json encoded response with Mustache
	}else{
		http_response_code(400); // Failure code for failed response is needed for proper message on AMP page
	}
}

