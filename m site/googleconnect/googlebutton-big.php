<?	
require_once("./constants.php");
require_once ('googleconnect/openid.php');
$openid = new LightOpenID("https://www.dogspot.in");
$openid->identity = 'https://www.google.com/accounts/o8/id';
$openid->required = array(
	'namePerson/first',
	'namePerson/last',
	'contact/email',
	'birthDate',
	'person/gender', 
 	'contact/postalCode/home',
	'contact/country/home',
	'pref/language',  
	'pref/timezone',
);
$openid->returnUrl = 'googleconnect/glogin.php'
?>
<? if($sitesection!='wag-log'){?>
<a href="<?php echo $openid->authUrl() ?>"><img src="/googleconnect/signin-google.png"></a>
<? }else{?>
<a href="<?php echo $openid->authUrl() ?>"><img src="/images/Google-Login.png"></a>
<? } ?>