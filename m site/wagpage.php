<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/session.php");
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/shop/functions.php");
require_once(SITEMAIN_URL."/shop/arrays/arrays.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Wag Tag Information | Dog Tag | Full Detail | DogSpot.in</title>
<meta name="keywords" content="Wag Tag Information,Full Detail On Wag Tag, Dog Tag, DogSpot Wag Tag" />
<meta name="description" content="Wag Tag, Dog Tag Full Information on Wag Tag like,A unique id for every dog,Activate it easily through your phone or Internet" />
<link rel="canonical" href="https://www.dogspot.in/wagtag-info/" />
<meta name="viewport" content="width=device-width, initial-scale=1">


 <?php require_once('common/script.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<link href="/css/main1.css" rel="stylesheet" type="text/css">
<?php require_once('common/top.php'); ?>
</head>
<div id="wagtag_wrapper">
<!-- content-->
<div class="wagtag_content">
<div class="wagtag_logo">
<img src="/images/wt-logo.png">
</div>
<div class="wagtag_btnsDiv"><a href="/how_to_activate.php">
<input type="button" value="How to Activate the Tag" class="wagtag_btn" ></a>
</div>
<div class="wagtag_btnsDiv">
<? if($userid=='Guest'){?><a href="/login.php?refUrl=/myDogs/">
<input type="button" value="Activate Wag Tag" class="wagtag_btn" ></a>
<? }else{?><a href="/myDogs/">
<input type="button" value="Activate Wag Tag" class="wagtag_act_btn" ></a>
<? }?>
</div>
<? $getPrice=query_execute_row("SELECT price,selling_price FROM shop_items WHERE item_id='8253'");?>
<div class="wagtag_purchaseDiv">
<div class="wagtag_price"><span class="rs_del">Rs. </span><del><span class="rs_delText"><?=number_format($getPrice['selling_price'],0)?></span></del></div>
<div class="wagtag_pricedisc"><span class="rs">Rs. </span><span class="rs_Text"><?=number_format($getPrice['price'],0)?></span></div>
				<div class="buy_now">
					 <a href="/wag-tag/">Buy Now</a> </div>
			</div>

<h1 class="wagtag_heading" style="font-size:20px; font-weight:normal; color:#333;">
What is Wag Tag?
</h1>
<div class="wagtag_text">
<p>Wag Tag is a high grade stainless steel tag that can be easily attached to your pet’s collar and will ensure safety of your pets. Giving you respite from sleepless nights.The Wag Tag helps you to not only get your pet back but maintains confidentiality for the pet owner.</p>

<p>Each of the tag comes with a unique code, if your pet gets lost the finder will call the number given on the tag and it will be connected to DogSpot. Our customer care will in turn connect the call to the rightful owner.</p>
</div>

<!-- headline-->
<div class="wagtag_headlineBlock">
<div class="headline_1">A unique id for every dog </div>
<div class="headline_2">Tag can be transferred with the dog</div> 
<div class="headline_3">Activate easily from your phone or internet</div> 

<div class="headline_4">Pet owner's information is kept confidential </div>

</div>
<!--headline-->
<div class="wagtag_product">
<div class="wagtag_product_img">
<img src="/images/wag_tag.png">
</div>
<div class="wagtag_product_text">
Wag Tag is a high grade statinless steel tag 
that will ensure your pet's safety. It is easy to attach to the collar.
</div>
</div>
<!-- content-->
<div class="wagtag_btnsDiv">
<a href="/more_about_wag_tag.php"><input type="button" value="Know More >>" class="wagtag_btn" ></a>
</div>

</div>

<!--wag tag wrapper-->
</div>
 <?php require_once('common/bottom-new.php'); ?>
 <?php require_once('common/bottom.php'); ?>