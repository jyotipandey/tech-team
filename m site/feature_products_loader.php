<?
$ch = curl_init();
$page=$_REQUEST['group_no']+1;
$type=$_REQUEST['type'];
$url='https://www.dogspot.in/mobile-webservice/android/native-api/getTopProductsListing.php';
if($_REQUEST['moreType']=='newArrival'){
	$DATA='mode=DogSpotAppAuth&product_type=all_new_arrival&product_type_id=2&page='.$page;
}else if($_REQUEST['moreType']=='youMayLike'){
	$DATA='mode=DogSpotAppAuth&product_type=all_feature_product&product_type_id=4&page='.$page;
}else if($_REQUEST['moreType']=='sale'){
	$DATA='mode=DogSpotAppAuth&product_type=all_sale_item&product_type_id=6&page='.$page;
}else{
	$DATA='mode=DogSpotAppAuth&product_type=all_new_arrival&product_type_id=2&page='.$page;
}

if (!$ch){
	die("Couldn't initialize a cURL handle");
}
$ret = curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt ($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt ($ch, CURLOPT_POSTFIELDS,"$DATA");
$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$curlresponse = curl_exec($ch);
$allData=json_decode($curlresponse);
$data=$allData->data;
if(sizeof($data)>0 && $allData->status=='success'){ $i=0; foreach($data as $key=>$val){ ?>
<div data-role="content" class="ui-content" role="main">
				<div class="sol_blk">
					<ul class="sol_listing groom">
						<li>
							<div class="img_blk"><img title="" alt="" src="<?=$val->product_image?>"></div>
							<div class="txt_blk">
								<a href="<? echo $baseUrl.$val->product_nicename;?>" class="ui-link"><?=$val->product_name?></a>
								<p><span><i class="fa fa-inr"></i></span> <?=$val->retail_price?></p>
								<? 
								$mrp=$val->retail_price;
								$price=$val->sale_price;
								if($price>$mrp){?>
								<small>
						        <i class="fa fa-inr"></i><strike><?=$val->sale_price?></strike>&nbsp;<?=$val->saving_percentage?>% off</small>
								<?} ?>
							</div>
						</li>
					</ul>
				</div>
</div>
<? $i++;}}else{
	echo '';
} ?>