<?php
require_once('constants.php');
require_once('functions.php'); 
require_once(SITEMAIN_URL.'/database.php');
//require_once(SITEMAIN_URL.'/functions.php');
require_once(SITEMAIN_URL.'/functions2.php');
require_once(SITEMAIN_URL.'/shop/functions.php');
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/arrays.php');
require_once(SITEMAIN_URL.'/session.php');

$_SESSION['useridNew']=$_GET['useridNew'];
$useridNew=$_SESSION['useridNew'];
?>
<!DOCTYPE html>
<html>
<head>
<title>Address Book</title>
 <?php require_once('common/script.php'); ?>
<style>
.LV_invalid{ color:#f00; }
</style>
 <?php require_once('common/top.php'); ?>
 <div id="addnewaddress_page44" class="p-one-em" >
 
		<script type="text/javascript">
			$("#addnewaddress_page44").on("pageinit", function() {
				$("#backstep2").click(function() {
					$.mobile.changePage("#step2_placeorder_page41",{reverse:true});
				});
				$("#redirecttopage41").click(function() {
					$.mobile.changePage("#step2_placeorder_page41");
				});
			});
			$("#addnewaddress_page44").on("pageshow", function() {

			});
			
			function validateForm() {
				var name = document.forms["formshop_add"]["address_name"].value;
				var address_zip = document.forms["formshop_add"]["address_zip"].value;
				var address_phone1 = document.forms["formshop_add"]["address_phone1"].value;
				var address_address1 = document.forms["formshop_add"]["address_address1"].value;
				var address_address2 = document.forms["formshop_add"]["address_address2"].value;
				var address_city = document.forms["formshop_add"]["address_city"].value;
				var address_state = document.forms["formshop_add"]["address_state"].value;
				
				if (name.trim()==null || name.trim()==""|| name===" ") {
					alert("First name must be filled out");
					document.formshop_add.address_name.focus();
					return false;
				}else if (address_zip.trim()==null || address_zip.trim()==""|| address_zip===" ") {
					alert("Pincode must be filled out");
					document.formshop_add.address_zip.focus();
					return false;
				}else if (address_phone1.trim()==null || address_phone1.trim()==""|| address_phone1===" ") {
					alert("Mobile No. must be filled out");
					document.formshop_add.address_phone1.focus();
					return false;
				}else if (address_address1.trim()==null || address_address1.trim()==""|| address_address1===" ") {
					alert("Address must be filled out");
					document.formshop_add.address_address1.focus();
					return false;
				}else if (address_city.trim()==null || address_city.trim()==""|| address_city===" ") {
					alert("City must be filled out");
					document.formshop_add.address_city.focus();
					return false;
				}else if (address_state.trim()==null || address_state.trim()==""|| address_state===" ") {
					alert("State must be filled out");
					document.formshop_add.address_state.focus();
					return false;
				}
				
			}

		</script>
		<div data-role="content">
			<div class="myDogsBlock_wrp">
				<div class="myDogText">Add New Address<span class="triangle_down"></span></div>
				<div class="myDogText_content">
			<?php
				if(isset($_POST['submit']) || $_POST['update']){
				 $userid=$userid;
				 $address_name=$_POST['address_name'];
				 $address_email=$sessUserEmail;
				 $address_address1=mysql_real_escape_string($_POST['address_address1']);
				 $address_address2=$_POST['address_address2'];
				 $address_city=$_POST['address_city'];
				 $SQ_city = query_execute_row("SELECT * FROM city WHERE city_name='$address_city'");
				 $city_id = $SQ_city[city_id];
				 $city_nicename = $SQ_city[city_nicename];
				 $address_state=$_POST['address_state'];
				 $SQ_state = query_execute_row("SELECT * FROM state WHERE state_name='$address_state'");
				 $address_state_id = $SQ_state['state_id'];
				 $address_state_nicename = $SQ_state['state_nicename'];
				 $address_zip=$_POST['address_zip'];
				 $address_phone1=$_POST['address_phone1'];
				}
				if($userid !='Guest'){
				if(isset($_POST['submit'])){
					query_execute("INSERT INTO shop_order_more_address (userid, address_name, address_email, address_address1, address_address2, address_city, address_city_id, address_city_nicename, address_state,address_state_id,address_state_nicename,address_zip, address_phone1) VALUES('$userid','$address_name', '$address_email', '$address_address1', '$address_address2', '$address_city', '$city_id', '$city_nicename', '$address_state','$address_state_id','$address_state_nicename','$address_zip', '$address_phone1')");
					$get_id        = mysql_insert_id();
					$_SESSION['userShopAddressId']=$get_id;			
					if(isset($_REQUEST['refurl'])){
						echo '<script>
							$(location).attr("href","/myaddressbook.php");
						</script>';
					}else{
						echo '<script>
						$(location).attr("href","/order-summary.php");
						</script>';
					}
				}
				if(isset($_POST['update'])){
					$id=$_POST['more_address_id'];
					query_execute("UPDATE shop_order_more_address SET address_name='$address_name', address_address1='$address_address1', address_address2='$address_address2', address_city='$address_city', address_state='$address_state', address_zip='$address_zip', address_phone1='$address_phone1' WHERE more_address_id='$id'");
					
					echo"<script>alert('Your address has been successfully updated');
							$(location).attr('href','/myaddressbook.php');
						</script>";
				}
				$action=$_GET['action'];
				$state_id=$_GET['state_id'];
				if($action=='edit'){
					$id=$_GET['id'];
					$getaddress = mysql_query("SELECT * FROM shop_order_more_address WHERE userid='$userid' && more_address_id='$id'");
					$ship_add=mysql_fetch_array($getaddress);
					$id=$ship_add['more_address_id'];
					$address_name=$ship_add['address_name'];
					$address_address1=$ship_add['address_address1'];
					$address_address2=$ship_add['address_address2'];
					$address_city=$ship_add['address_city'];
					$address_state=$ship_add['address_state'];
					$address_state_id=$ship_add['address_state_id'];
					$address_zip=$ship_add['address_zip'];
					//$address_phone=explode('+91',$ship_add['address_phone1']);
					$address_phone1=$ship_add['address_phone1'];
				}
				}else
				{/*
				query_execute("INSERT INTO shop_order_more_address (userid, address_name, address_email, address_address1, address_address2, address_city, address_city_id, address_city_nicename, address_state,address_state_id,address_state_nicename,address_zip, address_phone1) VALUES('$userid','$address_name', '$address_email', '$address_address1', '$address_address2', '$address_city', '$city_id', '$city_nicename', '$address_state','$address_state_id','$address_state_nicename','$address_zip', '$address_phone1')");
					$get_id        = mysql_insert_id();
					$_SESSION['userShopAddressId']=$get_id;	
					header("Location: /order-summary.php?useridNew=$useridNew");
				*/}
			?>
				
				<form  id="formshop_add" name="formshop_add" method="post" action="" onSubmit="return validateForm()">
                <input type="hidden" name="more_address_id" value="<?=$id?>">
                <input type="hidden" name="useridNew" value="<?=$useridNew?>">
					<div class="form_style">
						<div class="input_field">
                        <input data-role="none" type="text" 
						<?php if($action == 'edit'){echo "value='".$address_name."'";} ?> name="address_name" placeholder='Name'/>
						</div>
						<div class="input_field">
							<input data-role="none" type="text" <?php if($action == 'edit'){echo "value='".$address_zip."'";} ?> name="address_zip"  placeholder='Pincode' id="address_zip"/>
                              <script>
      						 var address_zip = new LiveValidation('address_zip');
							address_zip.add( Validate.Numericality, { onlyInteger: true } );
							address_zip.add( Validate.Length, { is: 6 } );
        					</script>
						</div>
						<div class="input_field input_field_mobile">
							<div class="inputtext">
								<span>Mobile</span>
								<span>+91</span>
							</div>
							<input data-role="none" type="text" name="address_phone1"  <?php if($action == 'edit'){echo "value='".$address_phone1."'";}?> id="address_phone1"/>
                          <script>
      						 var address_phone1 = new LiveValidation('address_phone1');
							address_phone1.add( Validate.Numericality, { onlyInteger: true } );
							address_phone1.add( Validate.Length, { is: 10 } );
        					</script>
						</div>
						<div class="input_field">
						<textarea rows="4" data-role="none" placeholder="Address"  name="address_address1"><?php if($action == 'edit'){echo $address_address1;}?></textarea>
						</div>
						<div class="input_field">
							<input data-role="none" type="text" name="address_address2" <?php if($action == 'edit'){echo "value='".$address_address2."'";}?> placeholder="Landmark"/>
						</div>
						<div class="input_field">
							<div class="col2"><input data-role="none" type="text" name="address_city"  <?php if($action == 'edit'){echo "value='".$address_city."'";}?> placeholder="City"/></div>
							 <div class="col2 input_field">
                             <select name="address_state"><option value="">Select State</option>
	 								<?php
        								$queryState = mysql_query ("SELECT * FROM state ORDER BY state_name ASC");
        								if (!$queryState){	die(mysql_error());	}
        								while($rowState = mysql_fetch_array($queryState)){
        								$state_id_db = $rowState["state_id"];
        								$state_name = $rowState["state_name"];
        								$state_nicename = $rowState["state_nicename"];
										if($action=='edit'){
										$address_state=$_GET['state'];
										$SQ_state = query_execute_row("SELECT * FROM state WHERE state_name='$address_state'");
										$address_state_id = $SQ_state['state_id'];
										 $address_state_nicename = $SQ_state['state_nicename'];?>
										<option <? if($address_state_id==$state_id_db){echo 'selected';}?>><?=$state_name?></option>
                                        <?
										}else{
										echo "<option>$state_name</option>";

											}
         								}
     								?>
								 </select>
                             </div>
							
						</div>
					</div>
          <? if($action=='edit'){		
		  echo '<input data-role="none" class="btnstyle4" id="form-submit" name="update" type="submit" value="update">';
}else { ?>
		<input data-role="none" class="btnstyle4" id="form-submit" name="submit" type="submit" value="submit">
		<? }?>
        		</form>
				</div>	
			</div>	
		</div>
        </div>
  
	
 <?php require_once('common/bottom.php'); ?>
<script>
// $(document).ready(function(e) {
    // $('#form-submit').click(function(){
		// $("#formshop_add").submit();
		// });
// });
</script>