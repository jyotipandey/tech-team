<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
if($section[0]==''){
require_once("../constants.php");
}
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$breedid = query_execute_row("SELECT * FROM dog_breeds WHERE nicename='$breed_nice' AND breed_engine='1'");
$breed_id=$breedid['breed_id'];
if(!$breed_id){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();	
	}
$breednameselect = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$breed_id' AND breed_engine='1'");
$be_name       = $breednameselect['be_name'];
$img		   = $breednameselect['image_name'];
$selgroup        = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='35'");
$selgroup1       = $selgroup['value'];
$selrelated      = mysql_query("SELECT bev.breed_id FROM breed_engine_values as bev , dog_breeds as db WHERE bev.breed_id=db.breed_id AND bev.att_id='35' AND bev.value='$selgroup1' AND bev.breed_id !='$breed_id' AND db.breed_engine='1' LIMIT 5");
	if($img){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$img;
		$imageURL='/new/breed_engine/images/dog_images/120-114-'.$img;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,'120','114','ratiowh');

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="/dog-breeds/css/dog-breeds.css?v17">
<title>Compare <?=$be_name ?> with Related Breeds | Dogspot.In</title>
<meta name="keywords" content="Compare <?=$be_name ?> with Related Breeds, Difference <?=$be_name ?> with Related Breeds" />
<meta name="description" content="Compare <?=$be_name ?> with Related Breeds and Difference <?=$be_name ?> with Related Breeds at DogSpot.in" />
 <?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
  </head>  
   <?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
 <!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980">
                
                    <div class="fl" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666;"> <a href="/shop/">Home</a> » <a href="/dog-breeds/">Dog Breeds</a> » More Comparison
                     </div>
                 
                     <div class="fr"> <a href="javascript:window.print()"><img src="/new/pix/bdcmb_printicon.gif" alt="print" title="print"  /></a>
                        
                       <a href="https://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a>
      <a href="https://twitter.com/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a>                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->   
<div class="cont980">
<div class="be_comBreedBox">
<h1>Related Comparisons for <a href="/<?=$breed_nice;?>/"><span style="color:#668000"><?= $be_name;?></span></a></h1>
<div class="be_comBreed">
<? while($selrelated1=mysql_fetch_array($selrelated)){
$relbrd=$selrelated1['breed_id'];
$rltdbrdname = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$relbrd' AND breed_engine='1'");
$rltdbreedname       = $rltdbrdname['be_name'];
$rltdbreednice       = $rltdbrdname['nicename'];
$rltdimg             = $rltdbrdname['image_name'];
$checkstring=$breed_nice>$rltdbreednice;
?>
<a <? if($checkstring>0){?>href="/<?=$rltdbreednice;?>-vs-<?=$breed_nice;?>-compare/"<? }else{?>href="/<?=$breed_nice;?>-vs-<?=$rltdbreednice;?>-compare/"<? }?>>
<div class="be_comBreed_list">
<div class="be_relatedCom">
  <img src="/new/breed_engine/images/dog_images/120-114-<?= $img ?>" width="120" height="114" alt="<?=$rltdbreedname?>" title="<?=$rltdbreedname?>" /> 
  <label><?=$be_name; ?></label>
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 50px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
<?
	if($rltdimg){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$rltdimg;
		$imageURL='/new/breed_engine/images/dog_images/120-114-'.$rltdimg;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,'120','114','ratiowh');
?>
  <img src="/new/breed_engine/images/dog_images/120-114-<?= $rltdimg ?>" width="120" height="114" alt="<?=$rltdbreedname?>" title="<?=$rltdbreedname?>" /> 
 <label> <?=$rltdbreedname ?></label>
</div>
</div></a>
<? } ?>
</div>
</div>
</div>
  <?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>