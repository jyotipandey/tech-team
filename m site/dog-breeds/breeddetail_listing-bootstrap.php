<?php

require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = '';
$ant_category =ucwords(str_replace("-"," ",$section[0]));
$seltitle=query_execute_row("SELECT title FROM  breed_engine_traits WHERE id='$att_id'");
if($att_id=='999'){
	$selectdata=mysql_query("SELECT breed_id FROM  dog_breeds WHERE breed_engine='1' ORDER BY height DESC");
	}else{
	//	echo "SELECT breed_id,value FROM breed_engine_values WHERE att_id='$att_id' ORDER BY value DESC";
$selectdata=mysql_query("SELECT breed_id,value FROM breed_engine_values WHERE att_id='$att_id' ORDER BY value DESC");
}
	$sel_filter=mysql_query("SELECT monthly_keeping_cost_premium,monthly_keeping_cost_standard FROM  dog_breeds WHERE breed_engine='1'");
	while($selectfil = mysql_fetch_array($sel_filter)){
		if($selectfil['monthly_keeping_cost_premium']){
	$mkcp[]=$selectfil['monthly_keeping_cost_premium'];
	$mkcs[]=$selectfil['monthly_keeping_cost_standard'];}
	}
	$Amkcp=array_unique($mkcp);
	$Amkcs=array_unique($mkcs);
	asort($Amkcp);asort($Amkcs);
	$Amkcpmin=min($Amkcp);$Amkcpmax=max($Amkcp);
	$Amkcsmin=min($Amkcs);$Amkcsmax=max($Amkcs);
	
$selectfiler=mysql_query("SELECT * FROM  breed_engine_values");
while($selectf = mysql_fetch_array($selectfiler)){
	
	if($selectf['value']>'9'){
	$trait_value[]=$selectf['value'];
	$trait_breed_id[]=$selectf['breed_id'];
	}else{
	$rating_array[]=$selectf['att_id'];	
	$rating_value[]=$selectf['att_id'].'-'.$selectf['value'];
	}
	}
	$Atrait_breed_id=array_unique($trait_breed_id);
	$Atrait_value=array_unique($trait_value);
	$Arating_value=array_unique($rating_value);
	$Arating_array=array_unique($rating_array);
	asort($Atrait_value);
	asort($Arating_value);
	asort($Arating_array);
	//print_r($Arating_array);
	//print_r($Arating_value);
	foreach($Arating_array as $ke){//1 to 19

	foreach($Arating_value as $keyArating_value){
		$key12=explode("-",$keyArating_value);
		$keyname12=$key12[0];
		$keyvalue12=$key12[1];
		if($keyname12=='1'){
		$firstfilter[]=$keyvalue12;
		}
		
		if($keyname12=='2'){ //Affection
		$Affection[]=$keyvalue12;
		}
		if($keyname12=='3'){ //Availibility
		$Availibility[]=$keyvalue12;
		}
		if($keyname12=='4'){ //Behavior With Other Dog
		$BehaviorD[]=$keyvalue12;
		}
		if($keyname12=='5'){ //Behavior With Other Pets
		$BehaviorP[]=$keyvalue12;
		}
		if($keyname12=='6'){ //Cuteness
		$Cuteness[]=$keyvalue12;
		}
		if($keyname12=='7'){ //Exercise Required
		$Exercise[]=$keyvalue12;
		}
		if($keyname12=='8'){ //Fluffyness
		$Fluffyness[]=$keyvalue12;
		}
		if($keyname12=='9'){ //Hair Density
		$Hair_Density[]=$keyvalue12;
		}
		if($keyname12=='10'){ //Hair Length
		$Hair_Length[]=$keyvalue12;
		}
		if($keyname12=='11'){ //Maintinance High/low
		$Maintinance[]=$keyvalue12;
		}
		if($keyname12=='12'){ //Noisy
		$Noisy[]=$keyvalue12;
		}
		if($keyname12=='13'){ //Playfullness
		$Playfullness[]=$keyvalue12;
		}
		if($keyname12=='14'){ //Popularity
		$Popularity[]=$keyvalue12;
		}
		if($keyname12=='15'){ //Shedding
		$Shedding[]=$keyvalue12;
		}
		if($keyname12=='16'){ //Tolerance To Cold
		$ToleranceC[]=$keyvalue12;
		}
		if($keyname12=='17'){ //Tolerance To Heat
		$ToleranceH[]=$keyvalue12;
		}
		if($keyname12=='18'){ //Trainibility
		$Trainibility[]=$keyvalue12;
		}
		if($keyname12=='19'){ //Guarding Potential
		$Guarding_Potential[]=$keyvalue12;
		}
				
	}
	}
	
	$Afirstfilter1=$Afirstfilter=array_unique($firstfilter);asort($Afirstfilter);
	$AMaintinance1=$AMaintinance=array_unique($Maintinance);asort($AMaintinance);
	$AAffection1=$AAffection=array_unique($Affection);asort($AAffection);
	$AAvailibility1=$AAvailibility=array_unique($Availibility);asort($AAvailibility);
	$ABehaviorD1=$ABehaviorD=array_unique($BehaviorD);asort($ABehaviorD);
	$ABehaviorP1=$ABehaviorP=array_unique($BehaviorP);asort($ABehaviorP);
	$ACuteness1=$ACuteness=array_unique($Cuteness);asort($ACuteness);
	$AExercise1=$AExercise=array_unique($Exercise);asort($AExercise);
	$AFluffyness=$AFluffyness=array_unique($Fluffyness);asort($AFluffyness);
	$AHair_Density1=$AHair_Density=array_unique($Hair_Density);asort($AHair_Density);
	$AHair_Length1=$AHair_Length=array_unique($Hair_Length);asort($AHair_Length);
	$ANoisy1=$ANoisy=array_unique($Noisy);asort($ANoisy);
	$APlayfullness1=$APlayfullness=array_unique($Playfullness);asort($APlayfullness);
	$APopularity1=$APopularity=array_unique($Popularity);asort($APopularity);
	$AShedding1=$AShedding=array_unique($Shedding);asort($AShedding);
	$AToleranceC1=$AToleranceC=array_unique($ToleranceC);asort($AToleranceC);
	$AToleranceH1=$AToleranceH=array_unique($ToleranceH);asort($AToleranceH);
	$ATrainibility1=$ATrainibility=array_unique($Trainibility);asort($ATrainibility);
	$AGuarding_Potential1=$AGuarding_Potential=array_unique($Guarding_Potential);asort($AGuarding_Potential);




	foreach($Atrait_value as $keys){
		$selectfilername=query_execute_row("SELECT * FROM  breed_engine_att_att WHERE att_att_id='$keys'");
		$keyatt_id[]=$selectfilername['att_id']."-".$selectfilername['value']."@".$selectfilername['att_att_id'];
		$attnames[]=$selectfilername['att_id'];	
		}
	$Akeyatt_id=array_unique($keyatt_id);
	asort($Akeyatt_id);
	$attnames=array_unique($attnames);
	asort($attnames);
	//print_r($Akeyatt_id);
	while($selectdata1=mysql_fetch_array($selectdata)){
		$selectf=query_execute_row("SELECT count(*) as cd FROM  dog_breeds WHERE breed_engine='1' AND breed_id='$selectdata1[breed_id]'");
	if($selectf['cd']>0){
	$unique[]=$selectdata1['breed_id'];}
}

?>

<!DOCTYPE html>
<html>
<head>
 <title><?=$brdtitle?></title>
    <meta name="keywords" content="<?=$brdkey?>" />
    <meta name="description" content="<?=$brddesc?>" />
    <link rel="canonical" href="https://www.dogspot.in/<?= $breednice ?>/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/<?= $breednice ?>/" />

        
    <meta property="fb:app_id" content="119973928016834" /> 
    <meta property="og:site_name" content="DogSpot"/>
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.m.dogspot.in/<?= $breednice ?>/" /> 
    <meta property="og:title" content="<?=$selecttitle['title']?>" /> 
    <meta property="og:description" content="<?=$selecttitle['description']?>" />
    <meta property="og:image" content="https://www.dogspot.in/new/breed_engine/images/dog_images/<?= $img ?>" /> 
    
    <meta name="twitter:card" content="photo">
    <meta name="twitter:site" content="@indogspot">
    <meta name="twitter:url" content="https://m.dogspot.in/<?= $breednice ?>/">
    <meta name="twitter:title" content="<?=$selecttitle['title']?>">
    <meta name="twitter:description" content="<?=$selecttitle['description']?>">
    <meta name="twitter:image" content="https://www.dogspot.in/new/breed_engine/images/dog_images/<?= $img ?>">


    

<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<script type="text/javascript">

$(function(){
	
	$('#restbutton').hide();
	// coding for dispatch ready to dispathced
  $('#nxtbtn').click(function(){
	
    var val12 = [];
	
    $(':checkbox:checked').each(function(i){
      val12[i] = $(this).val();
    });
	
	if(val12!=1){
		
	window.location="/dog-breeds/breed_engine3.php?breeds="+val12;
	}else{alert("Please Select CheckBox");}
	});
});


</script>
<?php /*?><script type="text/javascript">
$(function () {
  
  var msie6 = $.browser == 'msie' && $.browser.version < 7;
  
  if (!msie6) {
    var top = $('#show_breed').offset().top - parseFloat($('#show_breed').css('margin-top').replace(/auto/, 0));
    $(window).scroll(function (event) {
      // what the y position of the scroll is
      var y = $(this).scrollTop();
      

      // whether that's below the form
      if (y >= top) {
        // if so, ad the fixed class
        $('#show_breed').addClass('fixed');
      } else {
        // otherwise remove it
        $('#show_breed').removeClass('fixed');
      }
    });
  }  
});

// This function using for click for compair breed
	function abcd(){
		
	var val1232 = [];	
	var val123 = [];var fields = [];
	var input_obj_breed1 = document.getElementsByName('chkbx');
	var ccheck=0,listb=0,listb1=0;
	for (i1 = 0; i1 < input_obj_breed1.length; i1++) {
		if (input_obj_breed1[i1].checked === true) {
			val123[i1] = input_obj_breed1[i1].value;
			ccheck++;
		}
	}
	
	//alert(val123);alert(ccheck);
	var search_brand_life123 = document.getElementsByName('CC');
	for (i11 = 0; i11 < search_brand_life123.length; i11++) {
		val1232[i11] = search_brand_life123[i11].value;
		listb++;
	}
	if(listb==1){
	var input_obj_compair = document.getElementsByName('CC');
		for (i12 = 0; i12 < input_obj_compair.length; i12++) {
		if (input_obj_compair[i12].checked === true) {
			if(input_obj_compair[i12].value!=''){
			fields[i12] = input_obj_compair[i12].value;
			listb1++;}
			}
		}
	}
	if(ccheck < listb1 || ccheck===listb1 || ccheck==0){
		var input_obj_compair1 = document.getElementsByName('CC');
		var input_obj_breed11 = document.getElementsByName('chkbx');
		for (ii = 0; ii < input_obj_compair1.length; ii++) {
		if (input_obj_breed11[ii].checked === true) {
			//alert(input_obj_compair1[ii].value);
		if (input_obj_compair1[ii].value != input_obj_breed11[ii].value) {
			crosscompair(input_obj_compair1[ii].value);exit();	
			//alert('delete'+input_obj_compair1[ii].value);
			}}
		}
		if(ccheck==0){document.getElementById('be_compareBox').style.display="none";
		crosscompair(input_obj_compair1[0].value);
		
		exit();
		
		}
		}
	if(ccheck>='5'){
		var obj_compair = document.getElementsByName('CC');
		var obj_breed = document.getElementsByName('chkbx');
		for (j = 0; j < obj_breed.length; j++) {
			if (obj_breed[j].checked!= true) {
					maindivs=obj_breed[j].value;
				//$('#d_'+maindivs).css('display','none');
			}
		}
		exit();
	}
		
	//alert(fields);alert(listb1);
	//alert(val1232);alert(listb);
	if(listb==1){var breeds2 = $.merge( val123, val1232 );}
	
	//alert("total:"+val123);
	//alert(val123.unique());
	if(val123==''){$('#be_compareBox').hide();}else{
	ShaAjaxJquary('/dog-breeds/compair-show-bootstrap.php?val123='+val123+'', '#show_breed', '', '', 'POST', '#show_breed', '<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />', 'REP');}
	//xxxx();
	}
	// This property use for bydefault compair div hide
	$(document).ready(function(){
		$('#be_compareBox').hide();
	});
	// This property use for click in compair filter button
function crosscompair(br_breed){
	var val1232=[]; var coi=0;
	var input_obj_breed = document.getElementsByName('chkbx');
	for (i = 0; i < input_obj_breed.length; i++) {
		if (input_obj_breed[i].checked === true) {
			if(br_breed==input_obj_breed[i].value){
				 input_obj_breed[i].checked = false
				 }
		}
	}
	var search_brands_cc = document.getElementsByName('CC');
	for (k = 0; k < search_brands_cc.length; k++) {
		val1232[k] = search_brands_cc[k].value;coi++;
	}
	
	//alert(coi);
	//if(coi<3){$('#compairbutton').css('display','none');}
	ShaAjaxJquary('/dog-breeds/compair-show-bootstrap.php?val123='+val1232+'&delbreed='+br_breed+'', '#show_breed', '', '', 'POST', '#show_breed', '<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />', 'REP');
	
	//for (ig = 0; ig < input_compair1.length; ig++) {		}
	//abcd();
	}	
	function exitsc(){
	//alert("s");
	$(".cmpchk").removeAttr('checked');
	$('#be_compareBox').css('display','none');
	
	}
function xxxx(){
	//var img = item.find('div.be_thumbImg img');//http://dogspot.loc/new/breed_engine/images/dog_images/great-dan.jpg
	var img= "/images/dog_images/great-dan.jpg";
	var imagey = $("#be_compareBox").offset().top-20;
    var imagex = $("#be_compareBox").offset().left-90;
        img.clone().prependTo("body")
        .css("z-index", "10000") .css("position", "absolute").offset(img.offset())
        .animate({
            queue: true,
            opacity: 1,
            top: imagey,
            left: imagex,
            height: 50,
            width: 50
        }, 800, function(){
		
		});
	
	}	
    </script><?php */?>
<script type="text/javascript">
function showhide(iddiv,hidediv,showdiv){
	document.getElementById(iddiv).style.display="block";
		document.getElementById(showdiv).style.display="block";
			document.getElementById(hidediv).style.display="none";
}
function showhide1(iddiv,hidediv,showdiv){
	document.getElementById(iddiv).style.display="none";
		document.getElementById(showdiv).style.display="none";
			document.getElementById(hidediv).style.display="block";
}
function loadpage(){
	var location=document.getElementById("top_sort").value;
	window.location.href='https://m.dogspot.in'+location;
}
function google_track(second,third){
_gaq.push(['_trackEvent', 'Dog Breeds » <?=$section[0] ?>',second,third]);
}
</script>
<script>
	<?php /*?>$(function() {
        $( "#Amkcp-range" ).slider({
            range: true,
            min: <?=$Amkcpmin; ?>,
            max: <?=$Amkcpmax;?>,
            values: [ <?= $Amkcpmin ?>, <?=$Amkcpmax?> ],
            slide: function( event, ui ) {
                $( "#Amkcp_txt" ).val( "Rs" + ui.values[ 0 ] + "-Rs" + ui.values[ 1 ] );
				$( "#Amkcp_min" ).val( "Rs. " + ui.values[ 0 ]);
				$( "#Amkcp_max" ).val( "Rs. " + ui.values[ 1 ]);
            },
			change: function (event, ui) {
				
				searchFilter();
			}
        });
        $( "#Amkcp_txt" ).val( "Rs" + $( "#Amkcp-range" ).slider( "values", 0 ) +
            "-Rs" + $( "#Amkcp-range" ).slider( "values", 1 ) );
			//alert($( "#slider-range" ).slider( "values", 0 ));
			$( "#Amkcp_min" ).val( "Rs. " + $( "#Amkcp-range" ).slider( "values", 0 ));
		$( "#Amkcp_max" ).val( "Rs. " + $( "#Amkcp-range" ).slider( "values", 1 ));	
		});<?php */?>		

		<?php /*?>$(function() {
        $( "#Amkcs-range" ).slider({
            range: true,
            min: <?=$Amkcsmin; ?>,
            max: <?=$Amkcsmax;?>,
            values: [ <?= $Amkcsmin ?>, <?=$Amkcsmax?> ],
            slide: function( event, ui ) {
                $( "#Amkcs_txt" ).val( "Rs" + ui.values[ 0 ] + "-Rs" + ui.values[ 1 ] );
				$( "#Amkcs_min" ).val( "Rs. " + ui.values[ 0 ]);
				$( "#Amkcs_max" ).val( "Rs. " + ui.values[ 1 ]);
            },
			change: function (event, ui) {
				
				searchFilter();
			}
        });
        $( "#Amkcs_txt" ).val( "Rs" + $( "#Amkcs-range" ).slider( "values", 0 ) +
            "-Rs" + $( "#Amkcs-range" ).slider( "values", 1 ) );
			//alert($( "#slider-range" ).slider( "values", 0 ));
			$( "#Amkcs_min" ).val( "Rs. " + $( "#Amkcs-range" ).slider( "values", 0 ));
		$( "#Amkcs_max" ).val( "Rs. " + $( "#Amkcs-range" ).slider( "values", 1 ));	
		});<?php */?>	
	
	
	//--------------------------------------------------------------------------------------------------------
	
	function callattribute(brs1){
		//alert(brs1);
		var search_check_a1 = document.getElementsByName('typecheck');
		//alert(search_check_a1.length);	
		for (br = 0; br < search_check_a1.length; br++) {
			//alert(search_check_a1[br].value+' - '+brs1);
				if(search_check_a1[br].value===brs1){
				search_check_a1[br].checked = false;
				//alert(search_check_a1[br].value+' - '+brs1);
				}
				}
		searchcheck();
	}
	
	function callparentattri(bs){
		//alert(brs1);
		bs=bs.replace('A','-');
		var search_check_a11 = document.getElementsByName('typebreed');
		//alert(search_check_a1.length);	
		for (br1 = 0; br1 < search_check_a11.length; br1++) {
			//alert(search_check_a1[br].value+' - '+brs1);
				if(search_check_a11[br1].value===bs){
				search_check_a11[br1].checked = false;
				//alert(search_check_a1[br].value+' - '+brs1);
				}
				}
		searchcheck();
	}
	
	function callbrand12(brs){
		//alert(brs);
	$("#dog_size").val(brs);	
	searchFilter();
	}
	
	
	
	
    function searchFilter(){
	$('#restbutton').show();
   $('html,body').animate({
       scrollTop: $("#refine").offset().top},
       'slow');
	var type_breed;
	var input_obj_typebreed = document.getElementsByName('typebreed');
	for (i = 0; i < input_obj_typebreed.length; i++) {
		if (input_obj_typebreed[i].checked === true) {
			type_breed = type_breed + input_obj_typebreed[i].value + '|';
		}
	}
	
	var type_breedlife;
	var br_breed1=$("#dog_size").val();
	var input_obj_typelife = document.getElementsByName('life_stage');
	for (i = 0; i < input_obj_typelife.length; i++) {
		if(br_breed1==input_obj_typelife[i].value){input_obj_typelife[i].checked = false;}
		if (input_obj_typelife[i].checked === true) {
			if(br_breed1!=input_obj_typelife[i].value){
			type_breedlife = type_breedlife + input_obj_typelife[i].value + '|';
		}else { input_obj_typelife[i].checked = false;}
		}
	}
	
	var vurl='';
	var s_price1=$('#Amkcp_txt').val();
	var minprice=$('#Amkcp_min').val();
	var maxprice=$('#Amkcp_max').val();
	minprice=minprice.replace('Rs. ',"");
	maxprice=maxprice.replace('Rs. ',"");
	var m1=<?=$Amkcpmin; ?>;var m2=<?=$Amkcpmax;?>;
	if(m1!=minprice || m2!=maxprice){vurl =vurl + '&s_price1='+s_price1; }
	
	var s_price2=$('#Amkcs_txt').val();
	var minprice1=$('#Amkcs_min').val();
	var maxprice1=$('#Amkcs_max').val();
	minprice1=minprice1.replace('Rs. ',"");
	maxprice1=maxprice1.replace('Rs. ',"");
	var m11=<?=$Amkcsmin; ?>;var m21=<?=$Amkcsmax;?>;
	if(m11!=minprice1 || m21!=maxprice1){vurl =vurl + '&s_price2='+s_price2; }
	
	
	window.history.pushState('page2', 'Title', '?filter=filter&att_id=<?=$att_id?>&type_breed11='+type_breed+'&type_breedlife='+type_breedlife+'&vurl='+vurl+'');
	//alert(typeof(type_breed));
	//if(typeof(type_breed)){	
	ShaAjaxJquary('/dog-breeds/breeddetail-listing-search-bootstrap.php?filter=filter&att_id=<?=$att_id?>
	&type_breed11='+type_breed+'&type_breedlife='+type_breedlife+'&vurl='+vurl+'', '#productFilter', '', '', 'POST', '.productListing542Load', '<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />', 'REP');
	setTimeout('assignfiltervalue()',500);
	//}
}
function assignfiltervalue(){
		var search_brands_life12 = document.getElementById('listbreed').value;
		var input_obj_lifebreed_test = document.getElementsByName('chkbx');
		var x,fields,fi;
		fi=search_brands_life12.split(/,/);
		for(var v=0;v<fi.length;v++){	
		fields = fi[v];
		
			if(fields!=','){		
			for (i = 0; i < input_obj_lifebreed_test.length; i++) {
				//alert(input_obj_lifebreed_test[i].value);
			if(input_obj_lifebreed_test[i].value===fields){
			input_obj_lifebreed_test[i].checked = true;
					}
				}
			}
		}
}

	function searchcheck(){
		var search_check = document.getElementsByName('typecheck');
		var lifebreed_test = document.getElementsByName('typebreed');
		var lifebreed = document.getElementsByName('typebreed');
		var x,fields3,f;
		
		for(var v6=0;v6<search_check.length;v6++){
			if (search_check[v6].checked === true) {	
				fields3 = search_check[v6].value;
				//alert(fields3);
				x=fields3.split('|');
				x1=x[0];
				$.each(x, function changi(i) {
				//alert(x[i]);
				fieldvalue=x[i];
			for (b = 0; b < lifebreed_test.length; b++) {
				
				if(lifebreed_test[b].value===fieldvalue){
				lifebreed_test[b].checked = true;
				//alert(lifebreed_test[b].value+' VS '+fieldvalue);
				}
				}
				});//each function 
			}else{
				//alert(search_check[v6].value);
				var fields33 = search_check[v6].value;
				//alert(fields3);
				var xx=fields33.split('|');
				//x11=xx[0];
				$.each(xx, function chan(j) {
				//alert(x[i]);
				fieldvalue1=xx[j];
				//alert(fieldvalue1);
			for (b1 = 0; b1 < lifebreed.length; b1++) {
				if(lifebreed[b1].value===fieldvalue1){
				lifebreed[b1].checked = false;
				//alert(lifebreed[b1].value+' - '+fieldvalue1);
				}
				}
				});//each function 
				//search_check[v6].checked = false;
				}
		}
		searchFilter();
	}

    </script>
    <script type="text/javascript">
    function compairbreed(){
		
		var val124 = [];
		var search_brands_life123 = document.getElementById('listbreed').value;
		
		fii=search_brands_life123.split(/,/);
		for(var v1=0;v1<fii.length;v1++){
		if(fii[v1]!=''){	
		//alert(fii[v1]);	
		val124[v1] = fii[v1];}
		}
		val125="1"+val124;
		window.location="/dog-breeds/compare-page.php?breeds="+val125;
		}
    </script>
<link type="text/css"  rel="stylesheet" href="https://www.dogspot.in/bootstrap/css/bootstrap.min.css" />

<link type="text/css"  rel="stylesheet" href="/dog-breeds/css/dog-breeds-bootstrap.css?v=13" />

</head>

<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<body>
<div id="refine"></div>
<div class="breadcrumbs">
  <div class="container">
    <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
      <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a href="/" itemprop="item"><span itemprop="name">Home</span></a>
        <meta itemprop="position" content="1" />
        </span> <span> / </span> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <span itemprop="item"> <span itemprop="name" class="active-bread">
        <?= ucwords(str_replace("-"," ",$section[0]));?>
        </span></span>
        <meta itemprop="position" content="2" />
        </span> </div>
    </div>
  </div>
</div>

<section class="breeds-section breed-tpe-sc">
<div class="container">
<div class="row">

<div class="col-sm-9">
  <div class="title">
    <h1>
      <? 
if($att_id=='999'){echo "Choose the size that suits you the best";}else{
echo $seltitle['title'];
}

?>
    </h1>
  </div>
  <div id="show_breed"></div>
  <div class="be_rytDetail" id="productFilter">
  
   <?php require_once($DOCUMENT_ROOT .'/dog-breeds/breeddetail-listing-search-bootstrap.php'); ?>
  </div>
</div>
</div>
</div>
</section>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>

  
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
<!-- cart page start--> 
