<?php
require_once("../constants.php");
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');

?>

<div class="com-breed-sec"  id="ajaxbreed">
  <div class="com-breed-text"> You have not selected a breed to compare</div>
  <div class="be_breedBox"> <img src="https://www.dogspot.in/new/breed_engine/images/save.png" width="13" height="12" alt="Save" title="Save" /> <span style="color:#668000">Add</span> another Dog Breed to your comparison:
</div>
<div class="comp-b-select">
    <select id="breed_id_add" name="breed_id_add" onchange="addbreedchange(this.value);">
      <option value='0'>Select Breed</option>
      <?php
			$query_domain = query_execute("SELECT * FROM dog_breeds WHERE breed_engine='1'");
		
			while($query_domain1 = mysql_fetch_array($query_domain)){
			$breed_id12 = $query_domain1["breed_id"];
			$breed_name = $query_domain1["breed_name"];
			$be_name = $query_domain1["be_name"];
			$nicename=$query_domain1['nicename'];
			  print "<option value='$breed_id12|$nicename'";
			  if($breed_id12 == $breed_id){   echo "selected=='selected'";  }  print ">$be_name</option>";
			 }
		  ?>
    </select>
</div>
    <div id="ajaxbreed_load"></div>
 
 
</div>
<input type="hidden" id="prv_breed" name="prv_breed" value="<?=$breeds ?>" />
<div class="breedComName_be" style="border-top:0px;">
  <h3>Top Breed Comparison</h3>
  <div class="be_comBrdNames">
    <ul>
      <li><a href="/german-shepherd-dog-alsatian-vs-labrador-retriever-compare/" data-ajax="false">German Shepherd Dog (Alsatian) vs Labrador Retriever</a></li>
      <li><a href="/pit-bull-terrier-american-vs-rottweiler-compare/" data-ajax="false">Rottweiler vs Pit Bull Terrier (American)</a></li>
      <li><a href="/rottweiler-vs-tibetan-mastiff-compare/" data-ajax="false">Tibetan Mastiff vs Rottweiler</a></li>
      <li><a href="/golden-retriever-vs-labrador-retriever-compare/" data-ajax="false">Golden Retriever vs Labrador Retriever</a></li>
    </ul>
  </div>
</div>
</div>
