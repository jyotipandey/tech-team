<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
if($section[0]==''){
require_once("../constants.php");
}
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
$ant_category = "";
$sel_email=query_execute_row("SELECT * FROM users WHERE userid='$userid'");?>

<!DOCTYPE html>
<html>
<head>
<title>Dog Breeds | Complete Information of dogs | Dogspot.in</title>
<meta name="keywords" content="Dog breed name, Pet breeds in India, Indian dog breeds, Dog breed info, Dog breed types" />
<meta name="description" content="It will give you a comprehensive view for every individual breed; this includes the cost involved in getting a puppy home to the monthly expenditure" />
<link rel="canonical" href="https://www.dogspot.in/dog-breeds/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/dog-breeds/" />

<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top-home-test.php'); ?>
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.dogspot.in/dog-breeds/" /> 
<meta property="og:title" content="Dog Breeds | Complete Information On Dog Breeds | Pictures | Types | Temperament | DogSpot.in" /> 
<meta property="og:description" content="Find Complete Information On Dog Breeds,Types, Pictures, Care, Diet. and this helps you determine which type of dog you should get at Dog Breeds Section with DogSpot.in" />
<meta property="og:image" content="https://www.dogspot.in/new/breed_engine/images/logo-300x300.jpg" />

<meta name="twitter:card" content="photo">
<meta name="twitter:site" content="@indogspot">
<meta name="twitter:url" content="http://www.dogspot.in/dog-breeds/">
<meta name="twitter:title" content="Dog Breeds | Complete Information On Dog Breeds | Pictures | Types | Temperament | DogSpot.in">
<meta name="twitter:description" content="Find Complete Information On Dog Breeds,Types, Pictures, Care, Diet. and this helps you determine which type of dog you should get at Dog Breeds Section with DogSpot.in">
<!--<meta name="twitter:image" content="https://www.dogspot.in/new/common/images/logo-300x300.jpg">-->
<meta name="twitter:image:width" content="610">
<meta name="twitter:image:height" content="610">


<style>
/*! CSS Used from: Embedded */
.sticky-bar,.sticky-height{height:30px;}
.sale-text,.start-countdown{display:inline-block;}
.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333;}
.sticky-bar a,.sticky-bar a:hover{text-decoration:underline;}
.sticky-left{width:96%;float:left;}
.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px;}
.sale-text{font-size:12px;}
.sale-text a{color:#fc3;text-decoration:none;}
.ds-home-link{padding-top:30px!important;}
body{font-family:lato,sans-serif;}
body,button,input{font-size:1em;line-height:1.3;font-family:lato,sans-serif;}
.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em;}
.ui-corner-all{-webkit-background-clip:padding;background-clip:padding-box;}
.ui-body-a{border-width:1px;border-style:solid;}
.ui-body-a{background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3;}
.ui-mobile body{height:99.9%;}
.ui-page{padding:0;margin:0;}
.ui-mobile a img{border-width:0;}
.ui-mobile-viewport{margin:0;overflow-x:visible;-webkit-text-size-adjust:100%;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent;}
body.ui-mobile-viewport{overflow-x:hidden;}
.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0;}
.ui-page{outline:0;}
.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden;}
@media screen and (orientation:portrait){
.ui-mobile .ui-page{min-height:420px;}
}
@media screen and (orientation:landscape){
.ui-mobile .ui-page{min-height:300px;}
}
.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0;}
.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em;}
.ui-content{border-width:0;overflow-x:hidden;}
.ui-screen-hidden{display:none!important;}
.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%;}
.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099;}
.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important;}
.ui-popup{position:relative;padding:10px 10px 0;}
.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff;}
.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden;}
.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px);}
.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block;}
.ui-panel-page-container{overflow-x:visible;}
.ui-panel-wrapper{overflow-x:hidden;}
.ui-panel-wrapper{position:relative;border:0;z-index:999;}
.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none;}
.ui-panel-dismiss-open{display:block;}
.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease;}
.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0);}
.ui-panel-position-left{left:-17em;}
.ui-panel-open.ui-panel-position-left{left:-1px;}
.ui-panel-page-content-position-left{left:17em;right:-17em;}
.ui-panel-animate.ui-panel-page-content-position-left{left:0;right:0;-webkit-transform:translate3d(17em,0,0);-moz-transform:translate3d(17em,0,0);transform:translate3d(17em,0,0);}
.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em;}
body{margin:0;padding:0;background:#F8F8F8;}
div,form,h1,h3,h4,img,p,ul{margin:0;padding:0;border:0;}
#header{text-align:left;height:50px;padding:12px;border-top:0;border-bottom:0;background:#6c9d06;}
#header .fa{font-size:18px;color:#fff;}
#header div,#morepopup li a,.dogbreeds_listing li a,.product-slide,.ui-link{text-decoration:none;}
#header div,.panelicon{display:inline-block;}
#header div{color:#f7422d;}
.panelicon{height:19px;left:0;margin-top:0!important;position:relative;top:0;width:18px;}
.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px;}
.ui-link{color:#333;}
.green_clr{font-family:gobold;margin-left:8px;}
.green_clr{color:#fff!important;}
.green_clr{font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335;}
.product-slide{border-right:1px solid #E7E7E7;color:#333;}
#morepopup li,#morepopup li a{color:#323232;}
#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px;}
#morepopup li img{margin-right:8px;vertical-align:top;}
#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px;}
#morepopup li:last-child{border-bottom:0;}
#morepopup-screen{background:0 0;}
.content-search{padding-bottom:20px;}
.content-search form{margin:0 auto;position:relative;top:5px;width:95%;}
.purchase-product-cart{position:relative;}
.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%;}
.new-arrivals .swiper-wrapper{width:45000!important;}
.dogbreedstext_blk{font-size:14px;font-weight:700;}
.dog_breeds_sec{padding:0;margin-top:10px;}
.dogbreeds_blk{width:100%;text-align:center;}
.dogbreeds_listing li{border-bottom:1px solid #D5D5D5;margin-bottom:10px;padding-bottom:10px;}
.dogbreeds_listing li div{display:inline-block;vertical-align:middle;padding-left:10px;}
.dogbreedsimg_blk{width:30%;margin-right:10px;}
.dogbreedsimg_blk img{max-width:100%!important;overflow:hidden;border:1px solid #fff!important;border-radius:3px;}
.dogbreedstext_blk{color:#8DC059;width:65%;}
*{margin:0;padding:0;font-family:lato,sans-serif;}
.clear,.ui-content{clear:both;}
.cartsearch_blk a img{width:18px;height:19px;}
.content-search{width:100%;float:left;}
.content-search{margin-top:0!important;padding:5px 0;background:#6c9d06;}
.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0;}
#header img{vertical-align:middle;}
.ui-content{overflow:hidden;}
#defaultpanel4{z-index:99999;}
.ui-content,.ui-panel-wrapper{float:left;width:100%;}
.swiper-container.product-slider{margin:0!important;background:#6c9d06;}
div.dsdropdown{position:relative;}
.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px;}
.m-sprite{background:url(https://m.dogspot.in/images/msprite.png) no-repeat;margin:auto;}
.m-user{float:left;background-position:-166px -40px;}
.m-orders{float:left;background-position:-33px -72px;}
.m-wishlist{float:left;background-position:-6px -72px;}
.m-logout{float:left;background-position:-78px -40px;}
.m-setting{float:left;background-position:-107px -40px;}
.m-contact-us{float:left;background-position:-139px -40px;}
ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc;}
ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc;}
ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%;}
ul.dsdropdown-menu{display:none;}
ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important;}
ul.dsdropdown-menu li:last-child{font-size:16px;border:0;}
ul.dsdropdown-menu li:hover{background:#f9f9f9;}
.nf-img{float:left;width:20%;}
.nf-no {
    position: absolute;
    width: 17px;
    text-align: center;
    border-radius: 17px;
    font-size: 11px;
    height: 17px;
    line-height: 16px;
    background: red;
    color: #fff!important;
    border: 1px solid red;
    top: -8px;
    right: -7px;
}
.nf-text{float:left;width:80%;}
.nf-img img{width:50px!important;height:auto!important;}
.nf-text h4{font-size:14px;padding-bottom:1px;color:#333;}
.nf-text p{color:#888;font-size:13px;}
.nf-active-bg{background:#f2f2f2;}
.cat-section{float:left;width:100%;letter-spacing:.7px;margin-top:10px;}
.cat-section .cat-gallery{overflow:scroll;overflow-y:hidden;white-space:nowrap;width:100%;border-bottom:1px solid #f2f2f2;padding-bottom:20px;}
.cat-section .cat-gallery a{font-size:12.3px;font-weight:700;letter-spacing:0;color:#555;text-decoration:none;}
.cat-section .cat-gallery div{margin-top:2px;}
.cat-section .cat-gallery ul{list-style:none;padding:0;margin:0;}
.cat-section .cat-gallery ul li{width:120px;text-align:center;border-left:1px solid #f4f4f4;}
.cat-section .cat-gallery ul li{position:relative;display:inline-block;align-self:flex-start;border-radius:0;background:#fff;cursor:pointer;margin-left:10px;}
.cat-gallery::-webkit-scrollbar{display:none;}
.cat-section .cat-gallery ul li:first-child{border:0;}
a,body,div,em,form,h1,h3,h4,i,iframe,img,ins,li,p,span,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline;}
body{line-height:1.3;}
ul{list-style-type:none;}
:focus{outline:0;}
ins{text-decoration:none;}
.cartsearch_blk a{margin-left:12px;}
.cartsearch_blk a:first-child{margin-left:0;}
#defaultpanel4{border-right:1px solid #ccc;background-color:#fff;}
.breadcrumb{overflow:hidden;padding:8px;margin-bottom:0;}
.breadcrumb a{font:12px Arial,Helvetica,sans-serif;color:#4b3a15;text-decoration:underline;}
.breadcrumb a:hover{text-decoration:none;}
.whitewrapper{background-color:#fff;}
.ui-link img{vertical-align:middle;}
*{box-sizing:border-box;}
.add-sans-sapce{float:left;width:100%;text-align:center;margin:auto;max-height:120px;}
.add-sans-sapce .add-banner{margin:auto;}
.panel div a{color:#333;}
.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none;}
.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4;}
.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4;}
.accordion:hover,.active{background-color:#f8f8f8;}
.panel{padding:0;display:none;background-color:#fff;overflow:hidden;}
.panel div{padding:10px 25px;}
.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px;}
.accordion.active:after{content:"\2212";}
.ds-home-link{background:#6c9d06;padding:8px 10px;}
.ds-home-link a{color:#fff;}
.ds-nav-click{padding:12px;}
.ds-nav-click a{color:#333;text-decoration:none;}
.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.fa-bars:before{content:"\f0c9";}
.fa-bell-o:before{content:"\f0a2";}
.fa-shopping-cart:before{content:"\f07a";}
.fa-user-o:before{content:"\f2c0";}
.fa-home:before{content:"\f015";}
.fa-search:before{content:"\f002";}
.dogbreedstext_blk{color:#333!important;}
h3{font-size:18px;}
.breedComName_be{border-top:1px solid #ccc;float:left;}
.breedComName_be h3{color:#444;padding-bottom:10px;margin-bottom:5px;text-align:left;border-bottom:1px solid #bfbfbf;}
.breedComName_be{width:96%;margin:0 2% 20px;padding-top:10px;}
.be_comBrdNames li,.be_comBrdNames li a{width:100%;list-style:none;font-size:14px;vertical-align:middle;float:left;}
.be_comBrdNames li a{background:#eee;padding:10px;margin:10px 0;}
/*! CSS Used from: Embedded */
.new-arrivals .swiper-wrapper{width:22542px!important;}
.pagination{display:none!important;}
.swiper-slide{width:auto!important;margin-right:20px;margin-left:10px;}
.product-slide{border:none;}
.swiper-container.product-slider{height:25px!important;float:left;}
.swiper-wrapper a{text-decoration:none;color:#fff;font-size:14px;text-transform:uppercase;}
/*! CSS Used fontfaces */
@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'), url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'), url(https://m.dogspot.in/css/font-awesome/fonts/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'), url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'), url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal;}
 </style>
</head>
  <?  $surl= "https://www.dogspot.in$_SERVER[REQUEST_URI]";
   $getDataCOunt=query_execute_row("SELECT SUM(share_value) as val FROM share_count WHERE share_url='$surl'");
$sumT=$getDataCOunt['val'];
if($sumT==''){
$sumT=0;	
}
$sum=format_num($sumT,1); 

?>  
<? if($userid=='95133') { ?>

   
   <div class="swiper-container swiper-container0 product-slider new-arrivals" >
  <div class="swiper-wrapper" >
   
   <a class="swiper-slide first-slide product-slide " data-ajax="false" href="/big-dog-breeds/">big 
dog</a>
   <a class="swiper-slide first-slide product-slide " data-ajax="false" href="/therapy-dog-breeds/">stress 
buster</a>
<a class="swiper-slide first-slide product-slide " data-ajax="false" href="/friendly-dog-breeds/">just a
Friend</a>
<a class="swiper-slide first-slide product-slide " data-ajax="false" href="/cute-dog-breeds/">cute &
furry buddy
</a>

<a class="swiper-slide first-slide product-slide " data-ajax="false" href="/kid-friendly-dog-breeds/">for
kids</a>
<a class="swiper-slide first-slide product-slide " data-ajax="false" href="/guard-dog-breeds/">guard
dog</a>
  </div>
  <div class="pagination" style="display:none;"></div>
  <div class="clear"></div>
</div>

   
  
  


<?php }?>

    
 
 

    
<div data-role="content" ><?php require_once($DOCUMENT_ROOT .'/category-common.php'); ?>
<h1 style="display:none">Dog Breeds</h1>
<div class="breadcrumb">
<div itemscope itemtype="http://schema.org/Breadcrumb"> 
<div itemscope itemtype="http://schema.org/BreadcrumbList">
<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<a href="/" itemprop="item"  data-ajax="false" ><span itemprop="name">Home</span></a>
<meta itemprop="position" content="1" /> </span>
<span> / </span>       
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <span  itemprop="item" ><span itemprop="name">Dog Breeds</span></span>
       <meta itemprop="position" content="2" /> </span>           
             
</div>
</div>
</div>
 <div class="dog_breeds_sec"> 
				<div class="dogbreeds_blk">
				<ul class="dogbreeds_listing"><?
				$iid = 0;
                $j = 0;
	$breednameselect=mysql_query("SELECT * FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC");
while($breeddetails=mysql_fetch_array($breednameselect)){
	 $iid           = $iid + 1;
	$j           = $j + 1;
$breedid=$breeddetails['breed_id'];
$breedname=$breeddetails['breed_name'];
$breednice=$breeddetails['nicename'];
$imgname=$breeddetails['image_name'];
$icon=$breeddetails['icon'];
$ht=$breeddetails['height'];
$wt=$breeddetails['weight'];
$be_name=$breeddetails['be_name'];
$breedadjselect=mysql_query("SELECT * FROM breed_adj WHERE breed_id='$breedid' ORDER BY adj_name ASC LIMIT 4");
while($breedadjselect1=mysql_fetch_array($breedadjselect)){
$adjname[]=$breedadjselect1['adj_name'];
}
			$imageURL = $absPath.'/new/breed_engine/images/dog_images/m/'.$imgname;
		//	$dest=$imageBasePath.'/new/breed_engine/images/dog_images/M-'.$imgname;
		//$imageURL=$absPath.'/new/breed_engine/images/dog_images/M-'.$imgname;
		//$compressed=image_compression($src,$dest,20);

		
?>
                 <li>
    					<a href="/<?=$breednice?>/" data-ajax="false">
						<div class="dogbreedsimg_blk"><img src="<?=$imageURL?>" alt="<?=$breedname?>" title="<?=$breedname?>" ></div>
						<div class="dogbreedstext_blk">
							<?=$breedname?>
						</div>
                        </a>
                      </li>
					  <? if($j==15){ $j=0;?>
						<div style="float:left; width:100%; height:auto; margin:10px auto;">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive-Head -->
<ins class="adsbygoogle"
    style="display:block"
    data-ad-client="ca-pub-3238649592700932"
    data-ad-slot="6045502591"
    data-ad-format="auto"></ins>
<script>
$(document).ready(function(){(adsbygoogle = window.adsbygoogle || []).push({})})
</script>

        </div>  
                      
<? } } ?>
					  	   
                       
				</ul>
				    
				
			</div>
			
            
			</div>
		
            <div class="breedComName_be" style="border-top:0px;">
  <h3>Top Breed Comparison</h3>
  <div class="be_comBrdNames">
    <ul>
      <li><a href="/german-shepherd-dog-alsatian-vs-labrador-retriever-compare/" data-ajax="false">German Shepherd Dog (Alsatian) vs Labrador Retriever</a></li>
      <li><a href="/pit-bull-terrier-american-vs-rottweiler-compare/" data-ajax="false">Rottweiler vs Pit Bull Terrier (American)</a></li>
      <li><a href="/rottweiler-vs-tibetan-mastiff-compare/" data-ajax="false">Tibetan Mastiff vs Rottweiler</a></li>
      <li><a href="/golden-retriever-vs-labrador-retriever-compare/" data-ajax="false">Golden Retriever vs Labrador Retriever</a></li>
    </ul>
  </div>
</div> 
            


     
    
   
            <?php require_once($DOCUMENT_ROOT .'/common/bottom-home-test.php'); ?>
