<?php
$baseURL='/home/dogspot/public_html';
include("../constants.php");
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/functions2.php');
//require_once(SITEMAIN_URL.'/banner1.php');
$ecomm_pagetype='articles'; 
$sitesection='articles'; 
header("Content-type: text/html; charset=iso-8859-1"); 
makeProperURL($requestedUrl, $requested);
if ($section[1]=='amp')
		{
		//$article_id = $rowarticle['ID'];
		include 'amp/blog-details.php';

		exit();
		}
?>
<!DOCTYPE html>
<html>
<head>
<?php 
	
		$sqlall="SELECT wpp.ID , wpp.post_author,wpp.views , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user,wpp.atag FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt,wp_users as wu WHERE wptr.object_id=wpp.ID  AND wpp.post_type='post' AND wu.ID=wpp.post_author AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.ID='$article_id'";
		$post_detail=mysql_query($sqlall);
		$rowpost_detail=mysql_fetch_array($post_detail);
		$article_id=$rowpost_detail['ID'];
		$sqlGetMax=query_execute_row("SELECT DISTINCT(wpp.ID) as ID FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wpp.post_author=wu.ID AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC limit 0,1");
	    $artuser_wp        = $rowpost_detail["post_author"];
        $rowUser_id  = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$artuser_wp'");
        $artuser = $rowUser_id['user_login'];
		$date=$rowpost_detail['post_date'];
		$art_name=$rowpost_detail['post_name'];
		$removelink=$rowpost_detail['atag'];
		$reformatted_date = showdate($date, "d M y");
		$post_title=$rowpost_detail['post_title'];
		$author=$rowpost_detail['display_name'];
		$post_content=stripcslashes($rowpost_detail['post_content']);
		$articlecat_id  = $rowpost_detail["term_id"];
		$sel_cat=query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
		$cat_name=$sel_cat['name'];
		$cat_nice=$sel_cat['slug'];
		$post_title = stripslashes($post_title);
		$post_title = breakLongWords($post_title, 30, " ");
		$whatapp=$post_title;
		$post_content = stripslashes($post_content);
		$post_content = breakLongWords($post_content, 200, " ");

		if($title_tag){
    $title = $title_tag;
}else{
    $title = $post_title;
}

if($art_tag){
    $keyword = $art_tag;
}else{
	$title = str_replace( array( '\'', '"', ',' , ';', '<', '>' ), ' ', $post_title);
    $keyword = $title;
}

		if ($post_content) {
			$pageDesc = $post_content;
			$pageDesc = html_entity_decode(strip_tags($pageDesc));
			$pageDesc = myTruncate($pageDesc, 200); // funtion to split 200 words
			
		}				
		$feat_imgURL    = get_first_image($post_content, '/home/dogspot/domains/m.dogspot.in/public_html');
		$imgURLAbs = make_absolute($feat_imgURL, 'https://m.dogspot.in/');
		if (!$article_id) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();

}
$trend_Desc=query_execute_row("SELECT wpp.ID , wpp.post_title, wpp.post_name , wpp.post_content , wpm.meta_value FROM wp_posts as wpp , wp_postmeta as wpm WHERE wpp.post_status = 'publish' AND wpp.ID='$article_id' AND wpp.domain_id='1' AND wpp.post_type='post' AND wpm.post_id=wpp.ID AND wpm.meta_key like 'Description' ORDER BY wpm.meta_id DESC LIMIT 1");
$trend_page_Desc=$trend_Desc['meta_value'];

?>

<title><?=$title?> | Dogspot.in</title>
<meta name="keywords" content="<?= $keyword ?>" />
<? if($trend_page_Desc){?>
<meta name="description" itemprop="description" content='<?=preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $trend_page_Desc)?>' />
<? }else{?>
<meta name="description" itemprop="description" content="<? $pageDesc = strip_tags($pageDesc); echo preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $pageDesc); ?>" />
<? }?>
<link rel="canonical" href="https://www.dogspot.in/<?=$section[0];?>/">
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/<?=$section[0];?>/" />

<!------Twitter--------->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@indogspot">
<meta name="twitter:title" content="<?= $title ?>">
<meta name="twitter:description" content="<?= trim($pageDesc) ?>">
<meta name="twitter:url" content="<?=$url ?>">
<meta name="twitter:image" content="<?= $imgURLAbs ?>" />
<!----------->

<!--------Facebook----->
<meta property="fb:app_id" content="119973928016834" />
<meta property="og:site_name" content="DogSpot" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?=$surl ?>" />
<meta property="og:title" content="<?= $title ?>" />
<meta property="og:description" content="<?= trim($pageDesc) ?>" />
<meta property="og:image" content="<?= $imgURLAbs ?>" />
<? //if($art_name=='amen-sending-these-angels'){
//http://www.youtube.com/embed/WV2nwTAU2do
?>
<meta property="og:video" content="http://youtube.googleapis.com/v/WV2nwTAU2do"/>
<meta property="og:video:type" content="application/x-shockwave-flash"/>

<script type="text/javascript" src="https://m.dogspot.in/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="https://m.dogspot.in/js/jquery.mobile-1.4.4.min.js"></script> 

<?php //require_once($DOCUMENT_ROOT . '/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT . '/common/header.php');
$iid=1; ?>
 </head>
<!--------------End-------->
<? //echo addBannerProductM($_SERVER['REQUEST_URI']); ?>

<?php require_once($DOCUMENT_ROOT .'/category-common.php'); ?>
<div data-role="content" class="art-content" style="background-color:#fff;">

<div class="breadcrumb" >
 
  <div class="cont980" itemscope itemtype="http://schema.org/Breadcrumb">
    <div class="breadcrumb_cont" itemscope itemtype="http://schema.org/BreadcrumbList"> 
    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    
    <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<a href="/" itemprop="item" data-ajax="false" class="ui-link"><span itemprop="name">Home</span></a>
<meta itemprop="position" content="1"> </span>
<span> / </span>
      <a data-ajax="false" href="/dog-blog/" itemprop="item"><span itemprop="name">Dog Blog</span></a>
       <meta itemprop="position" content="2" /> </span>
     <span> / </span>
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> 
   
     <span itemprop="name" class="brd_font_bold"><?=$post_title?></span>
      <meta itemprop="position" content="3" /> </span></div>
    <div class="cb"></div>
    
  </div>
</div>
<div class="header-banner" >
<?php include($DOCUMENT_ROOT .'/header-banner.php'); ?> 
</div>
 <form name="formsub" id="formsub" method="post">
 <div class="art_blk rytPost_list" id="rytPost_list">
    <div class="art_view_blk imgtxtcontwag" id='<?=$iid?>'>
      <h1><?=$post_title?></h1>
      <div class="art_post_detail"><a href="/dog-blog/<?=$cat_nice?>/" class="art_cat_name" data-ajax="false"><?=$cat_name?></a>,
        <?=$reformatted_date?>, By <?=$author?></div>
        
       <input type="hidden" name="txt1" id="txt1" value="article">
        <input type="hidden" name="txt2" id="txt2" value="5">
         <input type="hidden" name="urlpara<?=$iid?>" id="urlpara<?=$iid?>" value="<?=$art_name?>" />
             <input type="hidden" name="urltitle<?=$iid?>" id="urltitle<?=$iid?>" value="<?=$post_title;?> | <?=$cat_name?> | DogSpot.in" />
              <input type="hidden" name="title<?=$iid?>" id="title<?=$iid?>" value="<?=$post_title;?>" />
              <input type="hidden" name="htmlHidden<?=$iid?>" id="htmlHidden<?=$iid?>" value="<?=$sum?>"/>
    <p>
	<? if($removelink=='yes'){ 
	echo htmlspecialchars_decode(stripcslashes($rowpost_detail['post_content']));
	 }else{ echo strip_a_tags(htmlspecialchars_decode(stripcslashes($rowpost_detail['post_content'])));} ?>
</p>
<div style="text-align:center;">
<div id='div-gpt-ad-1552408890928-0' style='height:250px; width:300px;margin: 10px 24px 29px 22px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552408890928-0'); });
</script>
</div>
 </div>      <input type="hidden" name="articlecat_id" id="articlecat_id" value="<?=$articlecat_id?>"/>
    <input type="hidden" name="article_id<?=$iid?>" id="article_id<?=$iid?>" value="<?=$article_id?>"/>
    </div>
   
    <!--------------------------------------------------------------ends here---------------------------------------------------------------> 

     <!-------------------------------------------------------------about author------------------------------------------------------------->
            <? 
			if($GetUser['about_me']!=''){ 
			$GetUser = query_execute_row("SELECT about_me,f_name,image,userlevel FROM users WHERE userid='".$artuser."'");
    if ($GetUser['userlevel'] == '8' || $artuser=='sunur' || $GetUser['userlevel'] == '2') {
        if ($GetUser["image"]) {
            $profileimgurl = "https://www.dogspot.in/profile/images/" . $GetUser["image"];
        } else {
            $profileimgurl = 'https://www.dogspot.in/images/noimg.gif';
        }
    	 $imgWH[0] =100;
		$imgWH[1] =100;
?>
            <div class="authorContainer" style="float:left;width: 100%;background: #fff;padding: 0px 10px;">
              <h3 class="ds_about_author_text" style="text-transform: uppercase;font-size: 14px; margin-bottom:10px;">About the Author</h3>
              <div style="">
                <? guestredirectimage($userid, $GetUser['f_name'], $artuser, $profileimgurl, $imgWH[0], $imgWH[1]);?>
              </div>
              <h4 style="margin-bottom:5px;
    text-align: center;
}"><?=$author?></h4>
              <p style="font-size:14px; margin-bottom:20px;">
                <?=stripslashes($GetUser['about_me']); ?>
                 <div align="center">
<div id='div-gpt-ad-1552468463026-0' style='height:100px; width:300px;display:inline-block;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1552468463026-0'); });
</script>
</div></div>
                <? $selectuset = query_execute_row("select meta_value from users_meta where meta_key='google_userid' AND userid='".$artuser."'");
				if($selectuset['meta_value']){?>
                <br />
              <p style="padding-left:110px">Connect with the author via: <a href="https://plus.google.com/<?=$selectuset['meta_value']?>" target="_blank" rel="author nofollow">Google+</a></p>
              
              <? }?>
            </div>
         
          <? }?>
          <div class="cb"></div>
          <? } ?>
		  <?php /* 
     	<div style="float:left; width:100%; text-align:center; margin-bottom:10px;">
  <script type="text/javascript" language="javascript">
     var aax_size='300x250';
     var aax_pubname = 'dog0bd-21';
     var aax_src='302';
   </script>
  <script type="text/javascript" language="javascript" src="https://c.amazon-adsystem.com/aax2/assoc.js"></script>
</div> */?>
          <!-------------------------------------------------------------comment box-------------------------------------------------------------->
          <div id="facebook-comment-div" name="facebook-comment-div"></div>
          <div  class="fb-comments" data-href="<? echo"https://www.dogspot.in/$section[0]/";?>" data-num-posts="5" data-width="100%"></div>
          
          <!--------------------------------------------------------------ends here-------------------------------------------------------------->
          

    <? 
/*		$sql_prev = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MAX(ID) FROM wp_posts WHERE ID < '$article_id' AND post_status='publish')");
		$sql_next = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MIN(ID) FROM wp_posts WHERE ID > '$article_id' AND post_status='publish')");
	if($sql_next['ID']=='')
	{
	$show=$current_art_id-100;	
	$sql_next = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MIN(ID) FROM wp_posts WHERE ID > '$show' AND post_status='publish')");	
	}
	if($sql_prev['ID']=='')
	{
	$show=$current_art_id+100;	
	$sql_prev = query_execute_row("SELECT ID, post_title, post_name FROM wp_posts WHERE ID =(SELECT MIN(ID) FROM wp_posts WHERE ID > '$show' AND post_status='publish')");	
	}
$riO=$iid+1;
$selectArt = mysql_query("SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id , wu.display_name,wpp.first_image_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wptr.term_taxonomy_id = '$articlecat_id' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post'  AND wpp.post_author=wu.ID AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.ID!='$article_id' AND wpp.ID!='112'  AND wpp.ID!='95'  AND wpp.ID!='243'  AND wpp.ID!='74'  AND wpp.ID!='231' AND wpp.ID !='426' AND wpp.post_status='publish'  ORDER BY wpp.ID DESC LIMIT $riO,1");

  $getName=mysql_fetch_array($selectArt);?>
    <div data-articlepage-start=""><div class="top_banner"><div><div id="" style="border: 0pt none;">

</div></div></div>*/
?><?
 if($art_name !='8-facts-about-rottweilers-that-everybody-needs-to-know-about-this-awesome-breed-dogspot-in' && $art_name !='8-budget-friendly-dog-breeds-in-india'){?>

<div class="next_article_sce">
<div class="next_article_img"><img src="/dog-blog/images/next-story-icon2.jpg" alt="next story" title="next story"></div>
<div class="next_article_head"><?=$getName['post_title']?></div>
       	   
       	</div> 
         <!--------------------------------------------------------------ends here---------------------------------------------------------------> 
              
   	<div class="article_divider">
       	   &nbsp;
       	</div>
		<? }else{?>
        <div class="container text-center my-3">
        <div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner" role="listbox" style="border-style: groove;border-width: 3px;">
                 <span style="padding-top:30px;"><h2>Latest Article</h2></span>
                   <div class="carousel-item py-5 active" style="margin-top:-32px;">
                    <div class="row">
                <?   $sql2="SELECT DISTINCT(wpp.ID) , wpp.post_content , wpp.post_title , wpp.post_name,first_image_name  FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wpp.ID !='$article_id' AND wpp.post_author=wu.ID AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC limit 1,9";
	$postlist2=mysql_query($sql2);
	$ij=0;$ik=0;
	while($rowpostlist2=mysql_fetch_array($postlist2))
	{$ij++;$ik++;
	$article_id1  = $rowpostlist2["ID"];
		 $first_img_name  = $rowpostlist2["first_image_name"];
		 $post_title=$rowpostlist2["post_title"];
          $post_name_21  = $rowpostlist2["post_name"];
		$main_fea_art_body1=$rowpostlist2['post_content'];
		$feat_imgURL    = get_first_image($main_fea_art_body1, '/home/dogspot/domains/m.dogspot.in/public_html');
		$imgURLAbs = make_absolute($feat_imgURL, 'https://m.dogspot.in/');
				$check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id1' AND post_type='attachment' AND post_mime_type like '%image%' ORDER BY ID DESC");
						
				 if (strpos($imgURLAbs,'wordpress') !== false) {
				$exp=explode("https://www.dogspot.in",$imgURLAbs);
				$src = $DOCUMENT_ROOT.$exp[1];
				$pos = strrpos($imgURLAbs,'/');
				$image_name = substr($imgURLAbs,$pos+1);
				$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
				$imageURL1='/imgthumb/190x127-'.$image_name;
				}else if($check_img['guid'] !=''){
				$imgURLAbs=$check_img['guid'];
				$exp=explode("https://www.dogspot.in",$imgURLAbs);
				$src = $DOCUMENT_ROOT.$exp[1];
				$pos = strrpos($imgURLAbs,'/');
				$image_name = substr($imgURLAbs,$pos+1);
				$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
				$imageURL1='/imgthumb/190x127-'.$image_name;
				}
				else if ($imgURLAbs!='https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
			    $URL=$imgURLAbs;
			    $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
			    $pos = strrpos($image_name,'/');
			    $image_name = substr($image_name,$pos+1);
				$extension = stristr($image_name,'.');
				if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
				$image_name;
				}
				$src = $baseURL.'/userfiles/images/'.$image_name;
				$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
				$imageURL1='/imgthumb/190x127-'.$image_name;
				} 
				else if($rowUser1["image"]) {
				$src = $baseURL.'/profile/images/'.$rowUser1["image"];
				$imageURL = 'https://www.dogspot.in/imgthumb/190x127-'.$rowUser1["image"];
				$imageURL1 = '/imgthumb/190x127-'.$rowUser1["image"];
				} 
				else  {
				$src = $baseURL.'/images/noimg.gif';
				$imageURL = 'https://www.dogspot.in/imgthumb/190x127-noimg.gif';
				$imageURL1 = '/imgthumb/190x127-noimg.gif';
				}
				$dest = $baseURL.$imageURL1;
				//echo $dest;
				createImgThumbIfnot($src,$dest,'190','127','ratiowh');
   ?>
                        <div class="col-sm-3">
                            <div class="card">
                              <div class="card-body">
                               <a href="/<?=$post_name_21;?>/" data-ajax="false"><img src="<?=$imageURL;?>" alt="<?=$post_title?>"  title="<?=$post_title?>"/></a><h4 class="card-title" style="font-size: medium;
    padding-top: inherit;"><?=$post_title?></h4>
                                
                              </div>
                            </div>
                        </div>
                        <? if($ij==3 && $ik<9){ 
							$ij=0;?>
                        </div>
                        </div>
                    <div class="carousel-item py-5" style="margin-top:-32px;">
                    <div class="row">
                        <? }?>
                       <? }?>
                        </div>
                        </div>
                        <div class="row">
        <div class="col-1" style="margin-left: 35px;margin-top: -50px;" id="dvControls">
            <a class="carousel-control-prev text-dark" href="#myCarousel" role="button" data-slide="prev">
            <img src="https://m.dogspot.in/images/right.png" title="arrorw" alt="arrorw" width="15px" height="15px;">
            </a>
            <a class="carousel-control-next text-dark" href="#myCarousel" role="button" data-slide="next">
             <img src="https://m.dogspot.in/images/left.png" title="arrorw" alt="arrorw" width="15px" height="15px;">
            </a>
        </div>
    </div></div></div>
                        </div>
                         

        <? }?>

 <!--about the end-->
</div></form>
</div>
<? if($art_name !='8-facts-about-rottweilers-that-everybody-needs-to-know-about-this-awesome-breed-dogspot-in' && $art_name !='8-budget-friendly-dog-breeds-in-india'){?>

<script type="text/javascript">var urlload='/dog-blog/loadmore.php';var track_load=1;var loading=!1;$(document).ready(function(){$(window).scroll(function(){var height=$(window).height();var pageheight=$(document).height();var articlecat_id=$('#articlecat_id').val();var art_id=$(".imgtxtcontwag:last").attr('id');var article_id=$('#article_id'+art_id).val();var top=$(window).scrollTop()+5000;if(top+height>=pageheight){if(!loading){loading=!0;$('#wait_section').show();$.post(urlload,{'articlecat_id':articlecat_id,'article_id':article_id,'lastComment':$(".imgtxtcontwag:last").attr('id')},function(data){if($.trim(data)!=''){$("#rytPost_list").append(data);track_load++;loading=!1}else{$('#wait_section').hide()}}).fail(function(xhr,ajaxOptions,thrownError){$('#wait_section').hide();loading=!1})}}})});</script>
<? }?>
<script type="text/javascript" language="javascript">$(function(){var $win=$(window);$win.scroll(function(){var i=0;var sticky=$('.sticky'),scroll=$(window).scrollTop();if(scroll>=100){sticky.addClass('fixed')}
else{sticky.removeClass('fixed')};$('#rytPost_list .imgtxtcontwag').each(function(){i++;var visible=isScrolledIntoView(this);if(visible==!0){var id=this.id;var urlP="/"+$('#urlpara'+id).val()+"/";var urlT=$('#urltitle'+id).val();var Title=urlT.split('|');var pathname=window.location.pathname;var pathtitle=$(document).find("title").text().split('|');var curT=pathtitle[0];if(urlP!="/undefined/"){if(pathname!=urlP){var url="https://m.dogspot.in"+urlP+"";var data1=$('#htmlHidden'+id).val();$(".ds_social_number_cont_num").text(data1);$('.social').find("a").each(function(){var href=$(this).attr('href');url2=href.split('www.dogspot.in/');url2[1]=urlP;var arra=[url2[0],'www.dogspot.in',url2[1]];var val=arra.join('');$(this).attr('href',val)});(function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('send','event','load more article','mobile',urlT);ga('send',{'hitType':'pageview','page':''+urlP+'','title':''+urlT+''});window.history.pushState({path:url},'',url);document.title=urlT;return!1}}
return!1}})})});function isScrolledIntoView(elem)
{var $elem=$(elem);var $window=$(window);var docViewTop=$window.scrollTop();var docViewBottom=docViewTop+$window.height();var elemTop=$elem.offset().top;var elemBottom=elemTop+$elem.height();if(elemTop>=docViewTop)
{return(elemTop>=docViewTop)}
else if(elemBottom>=docViewBottom)
{return(elemBottom>=docViewBottom)}}</script>




<style>
#google_pedestal_container{display:none;}.sticky-bar,.sticky-height{height:30px}.sale-text,.start-countdown{display:inline-block}.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333}.sticky-bar a,.sticky-bar a:hover{text-decoration:underline}#header div,#morepopup li a,.sale-text a,.ui-link,ins{text-decoration:none}.sticky-left{width:96%;float:left}.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px}.sale-text{font-size:12px}.sale-text a{color:#fc3}.ds-home-link{padding-top:30px!important}body,button,input{font-size:1em;line-height:1.3;font-family:lato,sans-serif}.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em;-webkit-background-clip:padding;background-clip:padding-box}.ui-body-a{border-width:1px;border-style:solid;background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3}.ui-content,.ui-mobile a img{border-width:0}.ui-mobile body{height:99.9%}.ui-page{padding:0;margin:0;outline:0}.ui-mobile-viewport{margin:0;overflow-x:visible;-webkit-text-size-adjust:100%;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent}body.ui-mobile-viewport{overflow-x:hidden}.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0}.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden}@media screen and (orientation:portrait){.ui-mobile .ui-page{min-height:420px}}@media screen and (orientation:landscape){.ui-mobile .ui-page{min-height:300px}}.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0}.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em}.ui-content{overflow-x:hidden}.ui-screen-hidden{display:none!important}.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%}.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099}.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important}.ui-popup{position:relative;padding:10px 10px 0}.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff}.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden}.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px)}.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block}#header,#morepopup li:last-child{border-bottom:0}.ui-panel-page-container{overflow-x:visible}.ui-panel-wrapper{overflow-x:hidden;position:relative;border:0;z-index:999}.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none}.panelicon,.purchase-product-cart{position:relative}.ui-panel-dismiss-open{display:block}.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease}.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0)}.ui-panel-position-left{left:-17em}.ui-panel-open.ui-panel-position-left{left:-1px}.ui-panel-page-content-position-left{left:17em;right:-17em}.ui-panel-animate.ui-panel-page-content-position-left{left:0;right:0;-webkit-transform:translate3d(17em,0,0);-moz-transform:translate3d(17em,0,0);transform:translate3d(17em,0,0)}.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em}#header{text-align:left;height:50px;padding:12px;border-top:0;background:#6c9d06}#header .fa{font-size:18px;color:#fff}#header div,.panelicon{display:inline-block}#header div{color:#f7422d}.panelicon{background:url(https://m.dogspot.in/images/mobile-panel-icon-white.png);height:19px;left:0;margin-top:0!important;top:0;width:18px}.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px}.ui-link{color:#333}.green_clr{font-family:gobold;margin-left:8px;color:#fff!important;font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335}#morepopup li,#morepopup li a{color:#323232}#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px}#morepopup li img{margin-right:8px;vertical-align:top}#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px}#morepopup-screen{background:0 0}.content-search{padding-bottom:20px}.content-search form{margin:0 auto;position:relative;top:5px;width:95%}.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%}.art-content{padding:0}.art_blk{width:100%}.art_view_blk h1{font-size:16px}.art_view_blk{width:100%;float:left;padding:10px}.art_post_detail{color:#555;font-size:12px;margin-bottom:15px}.art_view_blk img{max-width:100%;height:auto;margin-bottom:15px}.art_view_blk p{font-size:14px;color:#555;padding:0;margin:0 0 15px}.art_cat_name{color:#fd5a5d!important;font-weight:400!important}.slide-banner{padding:10px 5px!important}.w-article-cat{width:100%;float:left;background:#fff;padding-top:15px}*{margin:0;padding:0;font-family:lato,sans-serif;box-sizing:border-box}.ui-content{clear:both;overflow:hidden}.cartsearch_blk a img{width:18px;height:19px}.content-search{margin-top:0!important;padding:5px 0;background:#6c9d06}.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0}#header img{vertical-align:middle}#defaultpanel4{z-index:99999}.ui-content,.ui-panel-wrapper{float:left;width:100%}div.dsdropdown{position:relative}.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px}.m-sprite{background:url(https://m.dogspot.in/images/msprite.png) no-repeat;margin:auto}.m-user{float:left;background-position:-166px -40px}.m-orders{float:left;background-position:-33px -72px}.m-wishlist{float:left;background-position:-6px -72px}.m-logout{float:left;background-position:-78px -40px}.m-setting{float:left;background-position:-107px -40px}.m-contact-us{float:left;background-position:-139px -40px}ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc}ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc}ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%}ul.dsdropdown-menu{display:none}ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important}ul.dsdropdown-menu li:last-child{font-size:16px;border:0}ul.dsdropdown-menu li:hover{background:#f9f9f9}.nf-img{float:left;width:20%}.nf-text{float:left;width:80%}.nf-no{text-align:center;position:absolute;width:17px;border-radius:17px;font-size:11px;height:17px;line-height:16px;background:red;color:#fff!important;border:1px solid red;top:-8px;right:-7px}.nf-img img{width:50px!important;height:auto!important}.nf-text h4{font-size:14px;padding-bottom:1px;color:#333}.nf-text p{color:#888;font-size:13px}.nf-active-bg{background:#f2f2f2}.cat-section{float:left;width:100%;letter-spacing:.7px;margin-top:10px}.cat-section .cat-gallery{overflow:scroll;overflow-y:hidden;white-space:nowrap;width:100%;border-bottom:1px solid #f2f2f2;padding-bottom:20px}.ac-results li,.ac_results,.breadcrumb,.panel{overflow:hidden}.cat-section .cat-gallery a{font-size:12.3px;font-weight:700;letter-spacing:0;color:#555;text-decoration:none}.cat-section .cat-gallery div{margin-top:2px}.cat-section .cat-gallery ul{list-style:none;padding:0;margin:0}.cat-section .cat-gallery ul li{width:120px;text-align:center;border-left:1px solid #f4f4f4;position:relative;display:inline-block;align-self:flex-start;border-radius:0;background:#fff;cursor:pointer;margin-left:10px}.cat-gallery::-webkit-scrollbar{display:none}.cat-section .cat-gallery ul li:first-child{border:0}.content-search{width:100%;float:left}a,body,div,em,form,h1,h4,i,iframe,img,ins,li,p,span,strong,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline}#defaultpanel4,.whitewrapper{background-color:#fff}ul{list-style-type:none}:focus{outline:0}.cb{clear:both}.cartsearch_blk a{margin-left:12px}.cartsearch_blk a:first-child{margin-left:0}#defaultpanel4{border-right:1px solid #ccc}.breadcrumb{padding:8px;margin-bottom:0; font-size:12px; color:#555}.breadcrumb a{color:#555text-decoration:none;}.art_view_blk a,.breadcrumb a:hover,.ds-nav-click a{text-decoration:none}.ui-link img{vertical-align:middle}.add-sans-sapce{float:left;width:100%;text-align:center;margin:auto;max-height:120px}.add-sans-sapce .add-banner{margin:auto}.panel div a{color:#333}.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none}.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4}.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4}.accordion:hover{background-color:#f8f8f8}.panel{padding:0;display:none;background-color:#fff}.panel div{padding:10px 25px}.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px}.accordion.active:after{content:"\2212"}.ds-home-link{background:#6c9d06;padding:8px 10px}.ds-home-link a{color:#fff}.ds-nav-click{padding:12px}.ds-nav-click a{color:#333}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-bars:before{content:"\f0c9"}.fa-bell-o:before{content:"\f0a2"}.fa-shopping-cart:before{content:"\f07a"}.fa-user-o:before{content:"\f2c0"}.fa-home:before{content:"\f015"}.fa-search:before{content:"\f002"}body{margin:0;font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height:1.5}.fa{font-size:16px!important}.art_view_blk{background:#fff}.art-content{background:#f2f2f2}.next_article_sce{width:100%;float:left;background:#fFf;border-top:1px solid #888;border-bottom:1px solid #888;padding:10px;font-size:14px;color:#555}.next_article_img{float:left;width:25%;margin-right:10px}.next_article_head{float:left;width:69%}.article_divider{border-top:5px solid #ccc;font-size:15px;color:#555;font-family:sans-serif;text-align:center;background:#fff;width:100%;float:left;font-weight:700}.w-article-cat{margin-bottom:0;padding:0!important}.article_divider:before{content:"";display:block;margin-bottom:10px;width:0;height:0;border-left:10px solid transparent;border-right:10px solid transparent;border-top:10px solid #ccc;margin-left:8px}.art_view_blk a{color:#6c9d06}.header-banner{width:96%;margin:15px 2% 10px;float:left;text-align:center}.header-banner img{max-width:100%;height:auto}.ac_results{padding:2px;border:1px solid #bfbfbf;background-color:#fff;z-index:999;text-align:left;top:205px;width:374px!important;border-radius:2px}.ac-results ul{width:100%;padding:0;margin:0 0 5px;font-size:16px}.highlight-suggestion{color:#333}.hl-sugg-vtcl{font-weight:700;color:#6C9D06}.ac-results li{margin:0;padding:2px 5px 2px 10px;cursor:default;display:block;font-size:13px;line-height:18px}.ac_over{background-color:#f2f2f2;color:#333}.ac-results .header{margin-top:12px;overflow:visible;background:#6C9D06;margin-bottom:5px}a .ac-results .header .text{display:inline-block;position:relative;color:#fff;padding:0 5px}@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(https://m.dogspot.in/css/font-awesome/fonts/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal} table{ border:1px solid #ddd; border-collapse:collapse;} table td{ border:1px solid #ddd; padding:2px 0px;}tr:nth-of-type(odd){ background:#fff;}</style>
<? if($art_name =='8-facts-about-rottweilers-that-everybody-needs-to-know-about-this-awesome-breed-dogspot-in' || $art_name =='8-budget-friendly-dog-breeds-in-india'){?>

<script type="text/javascript">
  
    // prevent href=# click jump
    document.addEventListener("DOMContentLoaded", function() {
		var controlsDiv=document.getElementById("dvControls");
      var links = controlsDiv.getElementsByTagName("a");
      for(var i=0; i < links.length; i++) {
        if(links[i].href.indexOf('#')!=-1) {
          links[i].addEventListener("click", function(e) {
          console.debug("prevent href=# click");
              if (this.hash) {
                if (this.hash=="#") {
                  e.preventDefault();
                  return false;
                }
                else {
                  /*
                  var el = document.getElementById(this.hash.replace(/#/, ""));
                  if (el) {
                    el.scrollIntoView(true);
                  }
                  */
                }
              }
              return false;
          })
        }
      }
    }, false);
  </script>


<script type="text/javascript"  src="https://m.dogspot.in/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://m.dogspot.in/css/bootstrap.min.css">

<script>
  $('#myCarousel').carousel({
  	interval: 5000
  })


  </script>
  <? }?>
<?php require_once($DOCUMENT_ROOT . '/common/bottom-final.php'); ?>
