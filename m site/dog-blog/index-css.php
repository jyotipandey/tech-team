<?php
$baseURL='/home/dogspot/public_html';
include("../constants.php");
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/functions2.php');
//require_once(SITEMAIN_URL.'/banner1.php');

header("Content-type: text/html; charset=iso-8859-1"); 
$ecomm_pagetype='articles';  
$filter=$section[1];
$sel_drop_categories_check=query_execute_row("SELECT DISTINCT(wptr.term_taxonomy_id) , wtt.term_id , wt.name , wt.slug FROM wp_term_relationships as wptr , wp_term_taxonomy as wtt , wp_terms as wt WHERE wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wt.term_id=wptr.term_taxonomy_id AND slug='$filter'");
$articlecat_id=$sel_drop_categories_check['term_id'];			   
if ($filter) {
    if ($filter == 'justin') {  
	    $pageT  = 'justin';
        $pageT1 = 'justin';
        
        $pageh1    = "Just in - Dog Blog & Articles";
        $pageKey   = "$pageT, Dog Blog, & Articles, Dogs India";
        $pageTitle = "$pageT, Dog Blog, Dog Article, Dogs India DogSpot.in";
		$pageDesc  = "Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in";    
        $sql       = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_author=wu.ID GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,4";
    } else if ($filter == 'popular') {
		 $pageT  = 'popular';
        $pageT1 = 'popular';
        
        $pageh1      = "Most Popular - Dog Blog & Articles";
        $pageKey     = "$pageT, Dog Blog, & Articles, Dogs India";
        $pageTitle   = "$pageT, Dog Blog, Dog Article, Dogs India DogSpot.in";
		$pageDesc  = "Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in";
        $sql         = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_author=wu.ID GROUP BY wpp.ID ORDER BY wpp.views DESC LIMIT 0,4";		
    } elseif($articlecat_id!=''){
	$filter = preg_replace('/\-/', ' ', $filter);
	$pageTitle = "$filter, Dog Blog, Dog Article, Dogs India DogSpot.in";
    $pageh1    = ucwords($filter)." - Dog Blog & Articles";
    $pageKey   = "$filter, Dog Blog, & Articles, Dogs India";
    $pageDesc  = "$filter Dog Blog, & Articles from Dogs Experts Dogs India DogSpot.in";	
 $sql   = "SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wptr.term_taxonomy_id='$articlecat_id' AND wpp.post_status = 'publish' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_author=wu.ID GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC LIMIT 0,4";		
		}else {
        header("HTTP/1.0 404 Not Found");
        require_once($DOCUMENT_ROOT . '/404.php');
        exit();
    }
}else{
     $pageTitle = "Dog Blogs and Articles for Every Pet Lover | DogSpot.in";
    $pageh1    = "Dog Blog & Articles";
    $pageKey   = "Dog Blog Category, & Articles, Dogs India";
    $pageDesc  = "Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in";
				$sql="SELECT DISTINCT(wpp.ID) , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wpp.post_name, wu.display_name,wu.ID as article_user FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt, wp_users as wu WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wpp.post_author=wu.ID AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date_gmt DESC limit 0,4";
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?
echo "$pageTitle";if ($artuser) {echo "," . $artuser;}?></title>
<meta name="keywords" content="<? $pageKey = str_replace('"', '', $pageKey);echo "$pageKey";if($artuser){echo ",". $artuser;}?>" />
<meta name="description" content="<? $pageDesc = str_replace('"', '', $pageDesc); echo "$pageDesc";if($artuser){echo ",". $artuser;}?>" />
<? if($section[1]==''){?>
<link href="https://www.dogspot.in/dog-blog/" rel="canonical">
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/dog-blog/" />
<? }else{?>
<link href="https://www.dogspot.in/dog-blog/<?=$section[1];?>/" rel="canonical">
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/<?=$section[1];?>/" />
<? }?>
<?php require_once($DOCUMENT_ROOT . '/common/script.php'); 
  require_once($DOCUMENT_ROOT . '/common/top-home-test.php');?>
<?php /*?><script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21630298032/Articlemo', [320, 50], 'div-gpt-ad-1507722272541-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script><?php */?>
    <script>
	$(document).ready(function(){
	var searchkeyword=$("#searchkeyword").val();
    $("#searchkeyword").keyup(function(e){
      if (e.keyCode === 13) {
	var searchkeyword=$("#searchkeyword").val();
	if(searchkeyword!=''){
	$.ajax({
    type: 'POST',
    url : '/dog-blog/search.php', 
    data: "search="+searchkeyword,
    success: function(data) {  // returns data
	$('#default').hide();
	$('#filter-sort').hide();
	$('#filter-category').hide();
	$('#filter-search').show();
	$('#filter-search').html(data);
		 }
	});
	}
    }
    });
});
</script>

<style>
.ui-btn:focus,.ui-page{outline:0}#header img,.art_listing li div{vertical-align:middle}.ui-mobile-viewport,html{-webkit-text-size-adjust:100%}.sticky-bar,.sticky-height{height:30px}.sale-text,.start-countdown{display:inline-block}.sticky-bar{position:fixed;width:100%;font-family:lato,sans-serif;left:0;top:0;z-index:100000;border-top:0;line-height:30px;letter-spacing:.5px;color:#fff;text-align:center;background:#333}#header,.ui-btn{text-align:left}.sticky-bar a,.sticky-bar a:hover{text-decoration:underline}.sticky-left{width:96%;float:left}.sticky-right{max-width:22px;float:right;padding:0;margin-right:5px}.sale-text{font-size:12px}.sale-text a{color:#fc3;text-decoration:none}.ds-home-link{padding-top:30px!important}.ui-btn,body,button,input,select{font-size:1em;line-height:1.3;font-family:lato,sans-serif}.ui-mobile label{font-weight:400;font-size:16px}.ui-btn,label.ui-btn{border:1px solid #ccc;padding-top:5px;padding-bottom:5px;border-radius:0!important}.ui-btn.ui-corner-all,.ui-corner-all{-webkit-border-radius:.3125em;border-radius:.3125em}.ui-corner-all{-webkit-background-clip:padding;background-clip:padding-box}.ui-btn.ui-checkbox-off:after{display:block;width:18px;height:18px;margin:-9px 2px 0;-webkit-border-radius:.1875em;border-radius:.1875em;background-color:red;background-color:rgba(0,0,0,.3)}.ui-checkbox-off:after{filter:Alpha(Opacity=30);opacity:.3}.ui-body-a{border-width:1px;border-style:solid;background-color:#fff;border-color:#ddd;color:#333;text-shadow:0 1px 0 #f3f3f3}.ui-content,.ui-mobile a img{border-width:0}.ui-mobile,.ui-mobile body{height:99.9%}.ui-page{padding:0;margin:0}.ui-mobile-viewport{margin:0;overflow-x:visible;-ms-text-size-adjust:none;-webkit-tap-highlight-color:transparent}body.ui-mobile-viewport{overflow-x:hidden}.ui-mobile [data-role=page],.ui-page{top:0;left:0;width:100%;min-height:100%;position:absolute;display:none;border:0}.ui-mobile .ui-page-active{display:block;overflow:visible;overflow-x:hidden}@media screen and (orientation:portrait){.ui-mobile .ui-page{min-height:420px}}@media screen and (orientation:landscape){.ui-mobile .ui-page{min-height:300px}}.ui-loader{display:none;z-index:9999999;position:fixed;top:50%;left:50%;border:0}.ui-loader-default{background:0 0;filter:Alpha(Opacity=18);width:2.875em;height:2.875em;margin-left:-1.4375em;margin-top:-1.4375em}.ui-content{overflow-x:hidden}.ui-btn{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;text-decoration:none!important;font-size:13px;position:relative;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;cursor:pointer;user-select:none;padding-left:5px}#header div,#morepopup li a,.art_txt_blk a,.ui-link,ins{text-decoration:none}.ui-btn-icon-left{padding-left:2.5em}.ui-btn-icon-left:after{content:"â–¼";position:absolute;display:block;color:#999;top:50%;margin-top:-8px;right:0}.ui-mobile label{display:block;margin:0 0 .4em}.ui-screen-hidden{display:none!important}.pop{-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;transform-origin:50% 50%}.ui-popup-screen{background-color:#000;height:100%;left:0;position:fixed;top:0;width:100%;z-index:1099}.ui-popup-container{z-index:1100;display:inline-block;position:absolute;padding:0;outline:0;right:0!important;left:auto!important}.ui-popup{position:relative;padding:10px 10px 0}.ui-popup.ui-body-inherit{border:1px solid #83929f;background-color:#fff}.ui-popup-hidden{left:0;top:0;position:absolute!important;visibility:hidden}.ui-popup-truncate{height:1px;width:1px;margin:-1px;overflow:hidden;clip:rect(1px,1px,1px,1px)}.ui-checkbox{margin-right:5px;position:relative}.ui-checkbox .ui-btn{margin:0;text-align:left;white-space:normal;z-index:2}.ui-checkbox input{position:absolute;left:0;top:50%;width:12px;height:12px;outline:0!important;z-index:1}.ui-checkbox input:disabled{position:absolute!important;height:1px;width:1px;overflow:hidden;clip:rect(1px,1px,1px,1px)}.ui-panel{width:17em;min-height:100%;max-height:none;border-width:0;position:absolute;top:0;display:block}#header,#morepopup li:last-child{border-bottom:0}.ui-panel-page-container{overflow-x:visible}.ui-panel-wrapper{overflow-x:hidden;position:relative;border:0;z-index:999}.ui-panel-dismiss{position:absolute;top:0;left:0;right:0;height:100%;z-index:1002;display:none}.panelicon,.purchase-product-cart{position:relative}.ui-panel-dismiss-open{display:block}#header div,.art_listing li div,.panelicon{display:inline-block}.ui-panel-animate{-webkit-transition:-webkit-transform .3s ease;-webkit-transition-duration:.3s;-moz-transition:-moz-transform .3s ease;transition:transform .3s ease}.ui-panel-animate.ui-panel:not(.ui-panel-display-reveal){-webkit-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0)}.ui-panel-position-left{left:-17em}.ui-panel-open.ui-panel-position-left{left:-1px}.ui-panel-page-content-position-left{left:17em;right:-17em}.ui-panel-animate.ui-panel-page-content-position-left{left:0;right:0;-webkit-transform:translate3d(17em,0,0);-moz-transform:translate3d(17em,0,0);transform:translate3d(17em,0,0)}.ui-panel-dismiss-open.ui-panel-dismiss-position-left{left:17em}#header{height:50px;padding:12px;border-top:0;background:#6c9d06}#header .fa{font-size:18px;color:#fff}#header div{color:#f7422d}.panelicon{background:url(https://m.dogspot.in/images/mobile-panel-icon-white.png);height:19px;left:0;margin-top:0!important;top:0;width:18px}.cartsearch_blk{float:right;text-align:right;width:50%;margin-top:1px}.ui-link{color:#333}.green_clr{font-family:gobold;margin-left:8px;color:#fff!important;font-size:22px;line-height:14px;text-shadow:-1px 1px 1px #659335}#morepopup li,#morepopup li a{color:#323232}#morepopup{background:#fff;border-radius:0;margin-top:20px;border:1px solid #CCC6B1;padding:0;right:-15px;width:183px}#morepopup li img{margin-right:8px;vertical-align:top}#morepopup li{border-bottom:1px solid #CCC6B1;font-size:14px;padding:10px;width:180px}#morepopup-screen{background:0 0}.content-search{padding-bottom:20px}.content-search form{margin:0 auto;position:relative;top:5px;width:95%}.article_blk,.input_field{margin-bottom:5px;float:left}.purchase-product-cart.ui-link span{background:#fff;border:1px solid #bfbfbf;border-radius:50%;color:#000;font-size:11px;font-weight:700;height:17px;padding:0;position:absolute;right:-5px;text-align:center;top:-18px;width:64%}.input_field{width:100%}.col2{float:left;margin-right:12px;width:47.5%}.col2:last-child{margin-right:0}.art-content{padding:0}.article_blk{background:#fff;color:#333;width:100%}.article_id{border-bottom:1px solid #ccc;color:#333;padding:0 0 8px 10px;font-weight:700;font-size:16px}.art_search{padding:14px 10px 4px}.art_blk{width:100%}ul.art_listing small{color:#555;font-size:12px;font-weight:400!important}.art_img_blk{width:25%;margin-right:10px}.art_img_blk img{width:100%}ul.art_listing{float:left;width:100%}.art_listing li:first-child{border-top:1px solid #d5d5d5}.art_listing li{border-bottom:1px solid #d5d5d5;padding:15px 10px}.art_txt_blk{color:#333;font-weight:700;font-size:14px;width:70%}.art_txt_blk a{color:#333}.art_cat_name{color:#fd5a5d!important;font-weight:400!important}.art_auth_name{color:#555!important;font-weight:400!important}.art_search{float:left;width:100%;background:#f8f8f8;margin-top:-5px}*{margin:0;padding:0;font-family:lato,sans-serif;box-sizing:border-box}.ui-content{clear:both;overflow:hidden}.cartsearch_blk a img{width:18px;height:19px}.content-search,.ui-content,.ui-panel-wrapper{width:100%;float:left}.content-search{margin-top:0!important;padding:5px 0;background:#6c9d06}.content-search .ui-filterable input{border:1px solid #fff;padding:10px;float:left;font-family:lato,sans-serif;letter-spacing:.7px;color:#333;background:#fff;margin-bottom:10px!important;border-radius:0}label.ui-btn{border:none!important}small{color:#666}#defaultpanel4{z-index:99999}div.dsdropdown{position:relative}.m-contact-us,.m-logout,.m-orders,.m-setting,.m-user,.m-wishlist{width:25px;height:24px}.m-sprite{background:url(https://m.dogspot.in/images/msprite.png) no-repeat;margin:auto}.m-user{float:left;background-position:-166px -40px}.m-orders{float:left;background-position:-33px -72px}.m-wishlist{float:left;background-position:-6px -72px}.m-logout{float:left;background-position:-78px -40px}.m-setting{float:left;background-position:-107px -40px}.m-contact-us{float:left;background-position:-139px -40px}ul.dsdropdown-menu{text-align:left;background:#fff;border:1px solid #e0e4e7;color:#000;box-shadow:0 4px 8px -3px #555454;position:absolute;width:320px;right:-93px;z-index:9999;border-top:4px solid #ccc}ul.dsdropdown-menu::before{width:0;height:0;content:'';top:-11px;right:14px;position:absolute;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc}ul.dsdropdown-menu li{display:block!important;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%}ul.dsdropdown-menu{display:none}ul.dsdropdown-menu li:first-child{font-size:16px;background:#fff!important}ul.dsdropdown-menu li:last-child{font-size:16px;border:0}ul.dsdropdown-menu li:hover{background:#f9f9f9}.nf-img{float:left;width:20%}.nf-text{float:left;width:80%}.nf-no{position:absolute;width:17px;border-radius:17px;font-size:11px;height:17px;line-height:16px;background:red;color:#fff!important;border:1px solid red;top:-8px;right:-7px}.nf-img img{width:50px!important;height:auto!important}.nf-text h4{font-size:14px;padding-bottom:1px;color:#333}.nf-text p{color:#888;font-size:13px}.nf-active-bg{background:#f2f2f2}.cat-section{float:left;width:100%;letter-spacing:.7px;margin-top:10px}.cat-section .cat-gallery{overflow:scroll;overflow-y:hidden;white-space:nowrap;width:100%;border-bottom:1px solid #f2f2f2;padding-bottom:20px}.ac-results li,.ac_results,.breadcrumb,.panel{overflow:hidden}.cat-section .cat-gallery a{font-size:12.3px;font-weight:700;letter-spacing:0;color:#555;text-decoration:none}.cat-section .cat-gallery div{margin-top:2px}.cat-section .cat-gallery ul{list-style:none;padding:0;margin:0}.cat-section .cat-gallery ul li{width:120px;text-align:center;border-left:1px solid #f4f4f4;position:relative;display:inline-block;align-self:flex-start;border-radius:0;background:#fff;cursor:pointer;margin-left:10px}.cat-gallery::-webkit-scrollbar{display:none}.cat-section .cat-gallery ul li:first-child{border:0}.col2 #drop_category,.col2 #drop_sort{border:1px solid #ddd;width:100%;height:35px;border-radius:3px}a,body,div,em,form,h1,h4,html,i,iframe,img,ins,label,li,p,small,span,ul{background:0 0;border:0;font-size:100%;margin:0;outline:0;padding:0;vertical-align:baseline}#defaultpanel4,.whitewrapper{background-color:#fff}ul{list-style-type:none}:focus{outline:0}.cartsearch_blk a{margin-left:12px}.cartsearch_blk a:first-child{margin-left:0}#defaultpanel4{border-right:1px solid #ccc}.breadcrumb{padding:8px;margin-bottom:0}.breadcrumb a{font:12px Arial,Helvetica,sans-serif;color:#4b3a15;text-decoration:underline}.breadcrumb a:hover,.ds-nav-click a{text-decoration:none}.ui-link img{vertical-align:middle}html{font-family:'Helvetica Neue',Helvetica,Arial,sans-serif}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-bars:before{content:"\f0c9"}.fa-bell-o:before{content:"\f0a2"}.fa-shopping-cart:before{content:"\f07a"}.fa-user-o:before{content:"\f2c0"}.fa-home:before{content:"\f015"}.fa-search:before{content:"\f002"}body{margin:0;font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height:1.5}.add-sans-sapce{float:left;width:100%;text-align:center;margin:auto;max-height:120px}.add-sans-sapce .add-banner{margin:auto}.panel div a{color:#333}.ds-nav-new a,.panel div a{font-size:14px;text-decoration:none}.accordion{cursor:pointer;padding:12px;width:100%;border:none;text-align:left;outline:0;font-size:14px;transition:.4s;border-bottom:1px solid #f4f4f4}.ds-nav-click,.panel div{border-bottom:1px solid #f4f4f4}.accordion:hover,.active{background-color:#f8f8f8}.panel{padding:0;display:none;background-color:#fff}.panel div{padding:10px 25px}.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px}.accordion.active:after{content:"\2212"}.ds-home-link{background:#6c9d06;padding:8px 10px}.ds-home-link a{color:#fff}.ds-nav-click a,.highlight-suggestion{color:#333}.ds-nav-click{padding:12px}.ui-btn{margin:0!important}.header-banner{width:96%;margin:0 2% 10px;float:left;text-align:center}.header-banner img{max-width:100%;height:auto}.ac_results{padding:2px;border:1px solid #bfbfbf;background-color:#fff;z-index:999;text-align:left;top:205px;width:374px!important;border-radius:2px}.ac-results ul{width:100%;padding:0;margin:0 0 5px;font-size:16px}.hl-sugg-vtcl{font-weight:700;color:#6C9D06}.ac-results li{margin:0;padding:2px 5px 2px 10px;cursor:default;display:block;font-size:13px;line-height:18px}.ac_over{background-color:#f2f2f2;color:#333}.ac-results .header{margin-top:12px;overflow:visible;background:#6C9D06;margin-bottom:5px}.ac-results .header .text{display:inline-block;position:relative;color:#fff;padding:0 5px}@font-face{font-family:FontAwesome;src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?v=4.7.0);src:url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'),url(https://m.dogspot.in/css/font-awesome/fonts/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'),url(https://m.dogspot.in/css/font-awesome/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');font-weight:400;font-style:normal}</style>
	
<script type="text/javascript">
  var urlload='/dog-blog/article_load_more.php';
  var track_load=1;
  var loading  = false;
	$(document).ready(function(){
	 $(window).scroll(function() {
			var height = $(window).height();
			var pageheight = $(document).height();
			var top = $(window).scrollTop() + 3000;
			if(top + height >= pageheight){
				  if(!loading){ 
						 loading = true;			
						 $('#wait_section').show();
						 $.post(urlload,{'group_no': track_load}, function(data){									
							 if($.trim(data)!= ''){	
								$("#loader_result").append(data);
								track_load++; 
								loading = false; 	
							 }else{					 	
								$('#wait_section').hide();
							 }						 
						 }).fail(function(xhr, ajaxOptions, thrownError) {
							  $('#wait_section').hide();
							  loading = false;				
						 });				
				 }
			 } 
	  });
	});
function reloadPage(id) {
	var sort_type=$("#drop_sort").val();
	if(sort_type!=''){	
	window.location='/dog-blog/'+ id.value + "/";
	}
}
function reloadPage_cat(id) {
	var cat_id=$("#drop_category").val();
	if(cat_id!=''){	
	window.location='/dog-blog/'+ id.value + "/";
	}
}	
function c_aja_fu(){
	var searchkeyword=$("#searchkeyword").val();
	if(searchkeyword!=''){
	$.ajax({
    type: 'POST',
    url : '/dog-blog/search.php', 
    data: "search="+searchkeyword,
    success: function(data) {  // returns data
	$('#default').hide();
	$('#filter-sort').hide();
	$('#filter-category').hide();
	$('#filter-search').show();
	$('#filter-search').html(data);
		 }
	});
	}	
	}
</script>
<script>jQuery(document).ready(function ($) {

    setInterval(function (){
        moveRight();
    }, 7000);

  
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider').css({ width: slideWidth, height: slideHeight });
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
		$('#slider').css('display','block');
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

});    
</script>
	</head>
     
     
      
      <? echo addBannerProductM($_SERVER['REQUEST_URI']); ?>
        <div data-role="content" class="art-content">
        <?php require_once($DOCUMENT_ROOT .'/category-common.php'); ?>

       <div class="breadcrumb">
<div itemscope itemtype="http://schema.org/Breadcrumb"> 
<div itemscope itemtype="http://schema.org/BreadcrumbList">
<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<a href="/" itemprop="item"  data-ajax="false" ><span itemprop="name">Home</span></a>
<meta itemprop="position" content="1" /> </span>
<span> / </span>       
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <span  itemprop="item"  ><span itemprop="name">Dog Blog</span></span>
       <meta itemprop="position" content="2" /> </span>           
             
</div>
</div>
</div>

<!-- /21630298032/Articlemo -->
<?php /*?><div id='div-gpt-ad-1507722272541-0' style='height:60px; text-align:center;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1507722272541-0'); });
</script>
</div><?php */?>
<div class="header-banner" >
<?php require_once($DOCUMENT_ROOT .'/header-banner.php'); ?> 
   
</div>
			<div class="article_blk">
				<div class="article_id">
					<h1><?=$pageh1;?></h1>
				</div>
                </div>

                
                <div class="art_search">
                <div class="input_field">
							<div class="col2">
                              
        <select name="drop_sort" data-role="none" id="drop_sort" onChange="javascript:reloadPage(this)">
        <option value="">Sort By</option>
        <option value="justin" <? if($section[1]=='justin'){echo'selected="selected"';}?>>Recent Articles</option>
        <option value="popular" <? if($section[1]=='popular'){echo'selected="selected"';}?>>Most Popular</option>
        </select>

                            </div>
                            
							 <div class="col2 input_field">
                                        
       
        
        <?php
                $sel_drop_categories=mysql_query("SELECT DISTINCT(wptr.term_taxonomy_id) , wtt.term_id , wt.name , wt.slug FROM wp_term_relationships as wptr , wp_term_taxonomy as wtt , wp_terms as wt WHERE wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wt.term_id=wptr.term_taxonomy_id");?>               
        <select data-role="none" name="drop_category" id="drop_category" onChange="javascript:reloadPage_cat(this)" >
        <option value="">Select Category </option>
        <? while($sel_drop_categories1=mysql_fetch_array($sel_drop_categories)){
		$cat_drop_name=$sel_drop_categories1['name'];
		$cat_drop_nice=$sel_drop_categories1['slug'];
		$cat_id=$sel_drop_categories1['term_id'];
		?>
        <option value="<?=$cat_drop_nice?>" <? if($cat_drop_nice==$section[1]){echo'selected="selected"';}?> ><?=$cat_drop_name ?></option>
		<? } ?>

        </select>

                            
                             </div>	
						</div>
                </div>
               
             <!--search  code-->     
            <div class="art_blk" id="filter">
            <ul class="art_listing" id="filter-search">
					
				</ul>
            </div>
			<!--search code end -->
            <!--filter category code-->    
           
           
            <div class="art_blk" id="filter">
            <ul class="art_listing" id="filter-category">
					
				</ul>
            </div>
			<!--filter category code end -->
           <!--filter sort code-->     
            <div class="art_blk" id="filter">
            <ul class="art_listing" id="filter-sort">
					
				</ul>
            </div>
			<!--filter sort code end -->              
			<div class="art_blk" id="default">			
			 <ul class="art_listing" id="loader_result">
               <?php
				$postlist=mysql_query($sql);
				$iid = 0;
                $j = 0;	
				while($rowpostlist=mysql_fetch_array($postlist))
				{
				 $iid = $iid + 1;
	             $j = $j + 1;
				$article_id=$rowpostlist['ID'];
				$artuser=$rowpostlist['article_user'];
				$date=$rowpostlist['post_date'];
				$reformatted_date = showdate($date, "d M o");
				$post_title=$rowpostlist['post_title'];
				$author=$rowpostlist['display_name'];
				$articlecat_id  = $rowpostlist["term_id"];
				$post_name  = $rowpostlist["post_name"];
				$main_fea_art_id=$rowpostlist['ID'];
                $main_fea_art_body=$rowpostlist['post_content'];
				$sel_cat=query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
				$cat_name=$sel_cat['name'];
				$cat_nice=$sel_cat['slug'];
				$feat_imgURL    = get_first_image($main_fea_art_body, $baseURL);
	            $imgURLAbs = make_absolute($feat_imgURL, 'https://www.dogspot.in');
				$check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id' AND post_type='attachment' AND post_mime_type like '%image%' ORDER BY ID DESC");
						
				 if (strpos($imgURLAbs,'wordpress') !== false) {
				$exp=explode("https://www.dogspot.in",$imgURLAbs);
				$src = $DOCUMENT_ROOT.$exp[1];
				$pos = strrpos($imgURLAbs,'/');
				$image_name = substr($imgURLAbs,$pos+1);
				$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
				$imageURL1='/imgthumb/190x127-'.$image_name;
				}else if($check_img['guid'] !=''){
				$imgURLAbs=$check_img['guid'];
				$exp=explode("https://www.dogspot.in",$imgURLAbs);
				$src = $DOCUMENT_ROOT.$exp[1];
				$pos = strrpos($imgURLAbs,'/');
				$image_name = substr($imgURLAbs,$pos+1);
				$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
				$imageURL1='/imgthumb/190x127-'.$image_name;
				}
				else if ($imgURLAbs!='https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
			    $URL=$imgURLAbs;
			    $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
			    $pos = strrpos($image_name,'/');
			    $image_name = substr($image_name,$pos+1);
				$extension = stristr($image_name,'.');
				if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
				$image_name;
				}
				$src = $baseURL.'/userfiles/images/'.$image_name;
				$imageURL='https://www.dogspot.in/imgthumb/190x127-'.$image_name;
				$imageURL1='/imgthumb/190x127-'.$image_name;
				} 
				else if($rowUser1["image"]) {
				$src = $baseURL.'/profile/images/'.$rowUser1["image"];
				$imageURL = 'https://www.dogspot.in/imgthumb/190x127-'.$rowUser1["image"];
				$imageURL1 = '/imgthumb/190x127-'.$rowUser1["image"];
				} 
				else  {
				$src = $baseURL.'/images/noimg.gif';
				$imageURL = 'https://www.dogspot.in/imgthumb/190x127-noimg.gif';
				$imageURL1 = '/imgthumb/190x127-noimg.gif';
				}
				$dest = $baseURL.$imageURL1;
				//echo $dest;
				createImgThumbIfnot($src,$dest,'190','127','ratiowh');
				?>	
					<li>
						<div class="art_img_blk">
						<a href="/<?=$post_name;?>/" data-ajax="false"><img src="<?=$imageURL;?>" alt="<?=$post_title?>"  title="<?=$post_title?>"/></a></div>
						<div class="art_txt_blk">
							<a href="/<?=$post_name;?>/" data-ajax="false"><?=$post_title?></a>
							<p><a href="/dog-blog/<?=$cat_nice?>/" class="art_cat_name" data-ajax="false"><?=$cat_name?></a></p>
							<small><?=$reformatted_date?>, By <?=$author?></small>
						</div>		
					</li>
                
			 
                      
<? }  ?>
		       
				</ul>

            <?
			 if($searchkeyword!=''){ ?>
       	 <div style="text-align:center; display:none;" id="wait_section"><img src="/images/ajax_loader.gif" width="100" height="100"></div>
		 <? }?>
         
			</div>
<? if($userid=='95133'){?>

<script type="text/javascript">
 <?php /*?>   $(document).ready(function () {
	var interval = setInterval(changewidth,3000);	
	  });
	function changewidth()
	{
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0__container__").style.position = "relative";
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0__container__").style.paddingBottom = "59%";
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0__container__").style.height = "0";
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0__container__").style.overflow = "hidden";

   	
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0").style.width = '100%';
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0").style.height = "100%";
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0").style.top = "0";
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0").style.left = "0";
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0").style.position = "absolute";
$('#article_dfp').css('width','100%');
$('#article_dfp').css('height','auto');
	}<?php */?>
</script>
<? }?>

<?php /*?><script type="text/javascript">
    $(document).ready(function () {
	var interval = setInterval(changewidth,3000);	
	  });
	function changewidth()
	{
document.getElementById("div-gpt-ad-1507722272541-0").style.height = "60px";
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0").style.width = '320px';
document.getElementById("google_ads_iframe_/21630298032/Articlemo_0").style.height = '60px';
	}
</script><?php */?>


	   <?php require_once($DOCUMENT_ROOT . '/common/bottom-home-test.php'); ?>
  