<?php
if($section[0]==''){
	require_once("../constants.php");
}
require_once(SITEMAIN_URL."/database.php");

$data['defaultpanel4']='
<div class="bellows panellisting">
<a href="/dog-breeds/" data-ajax="false" style="text-decoration: none;" name="Dog Breeds">
<div class="bellows__item">
  <div class="bellowsheader header_gray_bg">
    <h3>Dog Breeds</h3>
  </div>
</div>
</a> <a name="Dog Blog" href="/dog-blog/" data-ajax="false" style="text-decoration: none;">
<div class="bellows__item " >
  <div class="bellowsheader header_gray_bg">
    <h3>Dog Blogs</h3>
  </div>
</div>
</a> <a name="Dog Names" href="/dog-names/" data-ajax="false" style="text-decoration: none;">
<div class="bellows__item" >
  <div class="bellowsheader header_gray_bg">
    <h3>Dog Names</h3>
  </div>
</div>
</a> <a name="Dog Photos" href="/photos/" data-ajax="false" style="text-decoration: none;">
<div class="bellows__item" >
  <div class="bellowsheader header_gray_bg">
    <h3>Dog Images</h3>
  </div>
</div>
</a> <a name="Adoption" href="/adoption/" data-ajax="false" style="text-decoration: none;">
<div class="bellows__item" >
  <div class="bellowsheader  header_gray_bg">
    <h3>Adoption</h3>
  </div>
</div>
</a>
<div class="bellows__item">
  <div class="bellows__header header_gray_bg header_gray_brd" >
    <h3><a href="#">Community</a></h3>
  </div>
  <div class="bellows__content">
    <ul class="bellow_listing">
      <a name="Dog Classifieds" href="/dog-listing/" data-ajax="false" style="text-decoration: none;">
      <li>Classifieds </li>
      </a>
    </ul>
    <ul class="bellow_listing">
      <a name="Dog Show" href="/dog-events/" data-ajax="false" style="text-decoration: none;">
      <li>Dog Show</li>
      </a>
    </ul>
    <ul class="bellow_listing">
      <a name="Qna" href="/qna/" data-ajax="false"  style="text-decoration: none;">
      <li>Q&A </li>
      </a>
    </ul>
    <ul class="bellow_listing">
      <a name="Wag Tag" href="/wagtag-info/" data-ajax="false" style="text-decoration: none;">
      <li>Wag Tag</li>
      </a>
    </ul>
    <ul class="bellow_listing">
      <a name="Wag Club" href="/wag_club/" data-ajax="false" style="text-decoration: none;">
      <li>Wag Club</li>
      </a>
    </ul>
    <?php /*?><ul class="bellow_listing"> 
        <a href="/food-subscription/home/"  data-ajax="false">
          <li> Subscription</li>
          </a>
        </ul><?php */?>
  </div>
</div>
<!--- shop section-->
   <div class="bellows__item">
      <div class="bellows__header header_gray_bg header_gray_brd"  >
        <h3><a name="Shop" href="#">Shop</a></h3>
      </div>
      <div class="bellows__content">
        
        
        <ul class="bellow_listing">
          <a name="Dog Store" href="/dog-store/" data-ajax="false" style="text-decoration: none;">
          <li>Dog Store </li>
          </a>
        </ul>
        
        
        <ul class="bellow_listing">
          <a name="Cat Store" href="/cats/" data-ajax="false"  style="text-decoration: none;">
          <li>Cat Store</li>
          </a>
        </ul>
        <ul class="bellow_listing">
          <a name="Birds" href="/birds/" data-ajax="false" style="text-decoration: none;">
          <li> Bird Store</li>
          </a>
        </ul>
         <ul class="bellow_listing">
          <a name="Small Pets" href="/small-pets/" data-ajax="false" style="text-decoration: none;">
          <li>Small Pet</li>
          </a>
        </ul>
        <ul class="bellow_listing">
          <a namey="Smart Deals" href="/monthly-essential-packs/" data-ajax="false" style="text-decoration: none;">
          <li>Smart Deals</li>
          </a>
        </ul>
        
        
          <ul class="bellow_listing">
          <a name="Sales" href="/sales/" data-ajax="false" style="text-decoration: none;">
          <li>Sale</li>
          </a>
        </ul>
        
      </div>
    </div>
   <!-- shop section end-->
</div>';
?>