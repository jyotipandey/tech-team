<?php
/*ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);*/

ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "512M");

if ($section[0] == '') {
    require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL . "/database.php");
require_once(SITEMAIN_URL . "/check_img_function.php");
require_once(SITEMAIN_URL . "/functions.php");
require_once(SITEMAIN_URL . "/functions2.php");
require_once(SITEMAIN_URL . '/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL . '/session.php');
$sitesection  = 'dog-breeds';
$ant_section  = 'Dog Breeds';
$ant_page     = 'breedhome';
$ant_category = "";
$sel_email    = query_execute_row("SELECT * FROM users WHERE userid='$userid'");
?>

<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="<?php echo $baseUrl;?>js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl;?>js/live-validation.js"></script>
<?php /*?><script type="text/javascript" src="<?php echo $baseUrl;?>js/jquery.mobile-1.4.4.min.js"></script> <?php */?>
<script type="text/javascript" src="<?php echo "https://www.dogspot.in";?>/js/shaajax.min.2.1.js"></script>
<link rel="stylesheet" href="https://www.dogspot.in/wag_club/css/dg_style.css" />
<script type="text/javascript" src="https://www.dogspot.in/wag_club/file_uploads.js"></script>
<title>Dog Breeds | Complete Information On Dog Breeds | Wagpedia | Dog Breed Types</title>
<meta name="keywords" content=" Dog Breeds, Dog Breeds List, Dogs Breed Selector, Best Dog Breeds, Top Dog Breeds" />
<meta name="description" content="WagPedia - Complete Information On Dog Breeds, Temperament,Types, Pictures, Care, Diet,Training to help you determine which type of dog you should get." />
<meta property="fb:app_id" content="119973928016834" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:title" content="Dog Breeds | Complete Information On Dog Breeds | Pictures | Types | Temperament | DogSpot.in" /> 
<meta property="og:description" content="Find Complete Information On Dog Breeds,Types, Pictures, Care, Diet. and this helps you determine which type of dog you should get at Dog Breeds Section with DogSpot.in" />
<style>
.fileUpload {
    position: relative;
    overflow: hidden;

}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.btn-primary {
  
    background-color:#d1d1d1;
    
}
.btn {
    display: block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
.contest_uploadblk{background:#f2f2f2; margin:10px; padding:15px;}
.ui-btn{    border: none !important;
    border-bottom: 1px solid #ddd;
    border-radius: 0px !important;
    padding-top: 10px !important;
    padding-bottom: 10px!important;
    color: #999;
    font-size: 14px !important;
    border-bottom: 1px solid #ccc !important;
}
.ui-input-btn{background-color:#d1d1d1 !important;  }
#image-holder{ margin-top:15px;}
.ui-select .ui-btn {
    
    border-radius: 0px !important;
}
.mrt{margin-top:15px !important;}
.dog_nameinput{background: #fff;border: none;border-bottom: 1px solid #ccc; padding-top: 10px !important;
    padding-bottom: 10px!important;}
</style>

<body>

<?php
require_once($DOCUMENT_ROOT . '/common/top.php');
?>
 <form id="vasPLUS_Programming_Blog_Form" method="post" enctype="multipart/form-data" action="submit.php">
<div class="contest_uploadblk">

<div class="fileUpload btn btn-primary">
 <span>Upload</span>
  <input name="vasPhoto_uploads" id="vasPhoto_uploads" type="file" class="upload" />
</div>
  
    <div id="vasPhoto_uploads_Status"></div>
    <div class="mrt">
<input type="text" name="dog_name" id="dog_name" required placeholder="Dog Name" class="dog_nameinput" >
</div>

    <div class="mrt">
<select name="breed" id="breed" required>
<option>Select Dog Breed</option>
 <?php
$queryBreed = mysql_query("SELECT * FROM dog_breeds ORDER BY breed_name ASC");
while ($rowBreed = mysql_fetch_array($queryBreed)) {
?>
     <option value="<?= $rowBreed['breed_id'] ?>"><?= $rowBreed['breed_name'] ?></option>
                  
                  <?php
}
?>
</select>
</div>
<div class="mrt">
<input type="Submit" name="acttag" id="acttag" value="Save">

</div>
<div  class="mrt">
<input type="reset" value="Reset">

</div>
</div>
</form>

     
<?php
require_once($DOCUMENT_ROOT . '/common/bottom.php');
?>
