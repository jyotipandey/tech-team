<?php 
	require_once('constants.php');
	require_once('functions.php'); 
	require_once(SITEMAIN_URL.'/database.php');
	require_once(SITEMAIN_URL.'/functions2.php');
	require_once(SITEMAIN_URL.'/shop/functions.php');
	require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
	require_once(SITEMAIN_URL.'/arrays.php');
	require_once(SITEMAIN_URL.'/session.php');
	require_once('functions_gift.php');
	
	if(!$_REQUEST){
		header("location: https://m.dogspot.in/");	
	}
	
	//------------Show
	
?>
<!DOCTYPE html>
<html>
<head>
 	<title>DogSpot.in | Online Pet Products Shopping India | Dog Breeds</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script type="text/javascript">
		function sortBy(){
			var val = $('input[type="radio"]:checked').val();
			//alert(val);
			document.forms["frmsort"].submit();
		}
	</script>
    
    <?php require_once('common/script.php'); ?>
  	<div id="filters-page24" class="whitewrapper">
    <?php require_once('common/top.php'); ?>
    
    <style>
	.filters_det, .sortby_det {
	background: #eaeaea!important
}
.filters_det>a, .sortby_det>a {
	color: #333!important;
	text-transform: uppercase
}
.sortby_det {
	border-right: 1px solid #ddd
}
.recentView .carousel ul li {
	width: 100px!important
}.product-listing-sec ul li{min-height: 240px;border-bottom: 1px solid #ddd;}
#positionWindow-popup{    max-width: 100% !important; left:0px !important; width:100% !important;     margin-top: -23px;}
.share_fb label{font-size: 14px;}
	</style>
    <div data-role="content">
		<? if($_REQUEST['cat_id']){
			$SQ_cat_name=query_execute_row("SELECT category_name, category_nicename FROM shop_category WHERE category_id='".$_REQUEST['cat_id']."'");
			$category_name=$SQ_cat_name['category_name'];
			$category_nicename=$SQ_cat_name['category_nicename'];	
		?>
        <div class="linkContainer"><h1 style="border-top: 1px solid #ddd;background-color: #337ab7"><?=$category_name?></h1></div>	
        <? }?>
    	<div class="product-listing-sec">
			<ul>
    <?php 
		//print_r($_REQUEST);
		if($_REQUEST){
			if(isset($_REQUEST['cat_id'])){
				$cat_id = $_REQUEST['cat_id'];
				$attribute_value = $_REQUEST['type_breed11'];
				if($attribute_value){
					$attribute_value = str_replace("undefined", "", $attribute_value);
					if($attribute_value){
						foreach($attribute_value as $sbrt1){
							$sbreedtype = explode('-',$sbrt1);
							$keyss[] = $sbreedtype[0];
							$sbreedt12[] = $sbrt1;
						}
						$keyss = array_unique($keyss);
						$breedtsql = '';
						if($sbreedt12[0]){
							foreach($keyss as $key1){
								foreach($sbreedt12 as $sbrt){
									$sbreedtypeT = explode('-',$sbrt);	
									$sbrt = str_replace('-','A',$sbrt);
									if($key1 == $sbreedtypeT[0]){
										if($c==1){
											$breedtsql.= " item_attribute:$sbrt";
											$c=0;
										}else{
											$breedtsql.= " OR item_attribute:$sbrt";
										}
										$printattrib_value[]=$sbrt;
									}
								}
								$c=1;
								$breedtsql= trim($breedtsql,' OR ');
								$breedtsql= "($breedtsql) AND (";
							}
							$breedtsql= trim($breedtsql,' AND ()');
							if($c==1){
								$breedtsql= " AND ($breedtsql)";
							}else{
								$breedtsql= " AND ($breedtsql))";
							}
						}
					}
				}
				
				if($_REQUEST['brand_id']){
					
					$brand_id = str_replace("undefined", "", $brand_id);
					$brand_id=trim($brand_id,'-');
					$sbrands=explode('-',$brand_id);
					if($sbrands[0]){
						if($cat_id==15){
							foreach($sbrands as $sb){
								$bsql.=" OR i.item_brand=$sb ";
								$printbrand_name[]=$sb;	
							}
							$bsql = trim($bsql,' OR ');
							$bsql = " AND ($bsql)";
						}else{
							foreach($sbrands as $sb){
								$bsql.=" OR item_brand:$sb ";
								$printbrand_name[]=$bsql;
							}
							$bsql = trim($bsql,' OR ');
							$bsql = "&fq=($bsql)";
						}
					}
				}
				
				if($_REQUEST['type_breed11']){
					$type_breed11 = $_REQUEST['type_breed11'];
					$type_breed11 = str_replace("undefined", "", $type_breed11);
					$type_breed11=trim($type_breed11,'-');
					$sbreedt=explode('-',$type_breed11);
					if($sbreedt[0]){
					foreach($sbreedt as $sbrt){
						$breedtsql.=" OR j.breed_type='$sbrt' ";
						$printbreed_type[]=$sbrt;
					}
					$breedtsql = trim($breedtsql,' OR ');
					$breedtsql = " AND ($breedtsql)";
					}
				}

				if($_REQUEST['min_price'] && $_REQUEST['max_price']){
					$sqlpp=" AND price:[$min_price TO $max_price]";
				}
				
				if($_REQUEST['type_life']){ //undefinedmature-
					$type_life = $_REQUEST['type_life'];
					$type_life = str_replace("undefined", "", $type_life);
					$type_life=trim($type_life,'-');
					$sbreedlife=explode('-',$type_life);
					if($sbreedlife[0]){
						foreach($sbreedlife as $sblife){
							$breedlsql.=" OR j.life_stage='$sblife' ";
							$printbreed_life[]=$sblife;
						}
						$breedlsql = trim($breedlsql,' OR ');
						$breedlsql = " AND ($breedlsql)";
					}
					
				}
				//check for category id sale
if($_REQUEST['categoryid23']){


    
	foreach($_REQUEST['categoryid23'] as $sb5){
		$bcatsql.=" OR item_category_id:$sb5 ";
	}
	$bcatsql = trim($bcatsql,' OR ');
	$bcatsql = "&fq=($bcatsql)";
	
}

				if($_REQUEST['breedname']){
					$breedname = $_REQUEST['breedname'];
					$breedname = str_replace("undefined", "", $breedname);
					$breedname=trim($breedname,'-');
					$sbreeds=explode('-',$breedname);
					if($sbreeds[0]){
						foreach($sbreeds as $sbre){
							$breedsql.=" OR j.breed_id='$sbre' ";
							$printbreed_name[]=$sbre;
						}
						$breedsql = trim($breedsql,' OR ');
						$breedsql = " AND ($breedsql)";
					}
				}
				
				if($_REQUEST['dog_breed_type']){
					$dog_breed_type = $_REQUEST['dog_breed_type'];
					$dog_breed_type = str_replace("undefined", "", $dog_breed_type);
					$dog_breed_type=trim($dog_breed_type,'-');
					$sbreedt=explode('-',$dog_breed_type);
					if($sbreedt[0]){
						foreach($sbreedt as $sbrt){
							$breedtsql1.=" OR j.breed_type='$sbrt' ";
							$printbreed_type[]=$sbrt;
						}
						$breedtsql1 = trim($breedtsql1,' OR ');
						$breedtsql1 = " AND ($breedtsql1)";
					}
				}
				
				if($_REQUEST['sortbypirce']=='1'){
					if($cat_id=='15'){
						$s = " ORDER BY i.price ASC ";	
					}else{
						$s = " price ASC ";
					}
				}
				
				if($_REQUEST['sortbypirce']=='2'){
					if($cat_id=='15'){
						$s = " ORDER BY i.price DESC ";
					}else{
						$s = " price DESC ";
					}
				}
				if($cat_id=='15'){
					$SQ_shop_item = "SELECT i.item_id, i.name, i.nice_name, i.price, i.item_brand,i.weight_price, i.item_parent_id, i.stock_status, i.created_at, j.breed_id, j.breed_type, j.life_stage, i.weight, i.item_display_order FROM shop_items as i, breed_shop_items as j WHERE j.item_category_id = '$cat_id' AND i.type_id!='configurable' AND stock_status!='outofstock' AND i.visibility='visible' AND i.item_display_status!='delete' AND j.item_id=i.item_id $breedtsql1 $breedsql $breedtsql $breedlsql $breedlsql1 $bsql $psql1 $wsps1 $wsql1 $s";
					$qItemAll=query_execute($SQ_shop_item);
					$totrecord = mysql_num_rows($qItemAll);
					//echo $totrecord."<br/>".$SQ_shop_item;
					while($rowItemall = mysql_fetch_array($qItemAll)){
						$Aitem_Itemid[]=$rowItemall["item_id"];
						$Aitem_brand[]=$rowItemall["item_brand"];
						$Aitem_price[]=$rowItemall["price"];
						$Abreed_id[]=$rowItemall["breed_id"];
						$Abreed_type[]=$rowItemall["breed_type"];
						$Alife_stage[]=$rowItemall["life_stage"];
						$Aweight[]=$rowItemall["weight"];
						$Aweight_price[]=$rowItemall["weight_price"];
					}
				}else{
					if($cat_id=='500'){
					$cat_id='';	
					$sal=" AND item_sale:1" ;
					}
					if($cat_id=='600'){
					$cat_id='';	
					$sal=" AND item_sale:1 AND domain_id:1" ;
					}
					if($cat_id=='700'){
					$cat_id='';	
					$sal=" AND item_sale:1 AND domain_id:3" ;
					}
$salinstock = "item_category_id:\"$cat_id\" AND NOT type_id:configurable AND visibility:visible AND NOT item_display_status:delete AND NOT item_id:805 AND NOT item_id:1283 AND NOT price:0 AND stock_status:instock $breedtsql$breedlsql$bsql$bssql$bnsql$sqlpp$btsql$bcatsql$wsql1$wsps1$sal$st&version=2.2&rows=500&sort=$s&qf=name^2&df=text&wt=xml&indent=true";
	               
					$url = "http://101.53.137.39/solr/dogspotshopsolr/select/?q=$salinstock";
	
					$url = str_replace(" ","%20",$url);
					$resultsolr = get_solr_result($url);
					$totrecord = $resultsolr['TOTALHITS'];
					?><input type="hidden" name="hu" value='<?=print_r($_REQUEST['categoryid23'])?>' ><?
					foreach($resultsolr['HITS'] as $rowItemall){
						$Aitem_Itemid[]=$rowItemall["item_id"];
						$Aitem_brand[]=$rowItemall["item_brand"];
						$Aitem_price[]=$rowItemall["price"];
						$Aitem_attribute[]=$rowItemall["item_attribute"];
						$Aitem_life_stage[]=$rowItemall["item_life_stage"];
						$Aitem_breed_id[]=$rowItemall["item_breed_id"];
						$Aitem_breed_type[]=$rowItemall["item_breed_type"];
						$Aitem_category[]=$rowItemall["item_category_id"];//add code
						$Aitem_weight[]=$rowItemall["weight"];
						$A1weight_price[]=round($rowItemall["price"]/$rowItemall["weight"],3);
					}
				}
	
				if($totrecord>0){
					/*foreach($resultsolr['HITS'] as $rowItemP11){
						$Aitem_ItemidP3[]=$rowItemP11['item_id'];
					}*/
					$unique = array_map("unserialize", array_unique(array_map("serialize", $Aitem_Itemid)));
					
					foreach($unique as $rowItemitem ){
						
						$rowItem=query_execute_row("SELECT tag, item_id, name, nice_name, description , selling_price, item_parent_id, price, type_id, weight_price, item_brand, stock_status, created_at, weight, item_display_order FROM shop_items WHERE item_id='$rowItemitem'");
						$item_id=$rowItem["item_id"];
			
						$tag=$rowItem["tag"];
						$name=stripslashes($rowItem["name"]);
						$name=stripslashes($name);
						$nice_name=$rowItem["nice_name"];
						$type_id=$rowItem["type_id"];
						$price=$rowItem["price"];
						$selling_price=$rowItem["selling_price"];
						$item_parent_id=$rowItem["item_parent_id"];
						$stock_status=$rowItem["stock_status"];
						$brandd=$rowItem["item_brand"];
						$description=$rowItem["description"];
						if($item_id){
							$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
						}
						if(mysql_num_rows($qdataM) < 1){
							$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
						}
						$rowdatM = mysql_fetch_array($qdataM);
			
						if($rowdatM["media_file"]){
							$imglink='/imgthumb/150x160-'.$rowdatM["media_file"];
							$src = '/home/dogspot/public_html/shop/item-images/orignal/'.$rowdatM["media_file"];
						}else{
							$imglink='/imgthumb/150x160-no-photo.jpg';
							$src = '/home/dogspot/public_html/shop/image/no-photo.jpg';
						}
						$dest = "/home/dogspot/public_html".$imglink;
						createImgThumbIfnot($src,$dest,'150','160','ratiowh');
			
						//get today's savings
						if($rowItem["selling_price"] > $rowItem["price"]){
							$item_discount_per1 = (($rowItem["selling_price"]-$rowItem["price"])*100)/$rowItem["selling_price"];
							$item_discount_per=number_format($item_discount_per1,0);
						}else{
							$item_discount_per="0";
						}
						?>
						
						<li class="item<?=$item_id?>">
						<a href="<?php echo $baseUrl.$nice_name;?>/" data-ajax="false"><img src="<?=$absPath."/".$imglink?>" alt="<?=$rowdatM["label"]?>" title="<?=$rowdatM["label"]?>" /></a>
						
							   <p>
                               <a href="/<?=substr($nice_name,0,40)?>/" data-ajax="false">
                             <?=substr($name,0,37); ?>
      <?	
	  if(strlen($name)>40)	
	 {
	echo'...';	 
	 } 
	  ?>
                               
                               </a>
                               </p>
							<div class="price-list-tag"><i class="fa fa-inr"></i> <?=number_format($price)?></div>
							<small><?php $diff=$selling_price-$price; $perc=($diff/$selling_price)*100; if($selling_price > $price){ ?><span class="price-crosss"><del> <?php echo 'Rs.'.number_format($selling_price); ?> </del>
                            </span> 
							<?php echo number_format($perc);?>% off <?php }?></small>
						
                     </li>   <?
						//end
					}
					
				}else{?>
					<li><p style="text-align:center; color:red"><strong>Items are not available.</strong></p></li>
				<? }
			}
		}
		
	?>
    </ul>
			</div>
        </div>
        <div class="cb vs10"></div>
        
        
        <div id="positionWindow" data-role="popup" class="ui-content sor-bye-line ui-popup-container" data-theme="a" style="border-radius:0px;">
          <div class="social_block" style="padding-bottom: 45px;">  <div style="background:#fff;"><strong style="color:#666;">Sort by</strong></div>
                <form action="" method="POST" name="frmsort" id="frmsort">
                    <input type="hidden" name="cat_id" id="cat_id" value="<?=$_REQUEST['cat_id']?>" />
                    <?
                    if($attribute_value){
						foreach($attribute_value as $getData){?>
						 <input type="hidden" name="type_breed11[]" id="type_breed11[]" value="<?=$getData;?>" />
					<?	}	
					}
					?>
                    <input type="hidden" name="type_breed11" id="type_breed11" value="<? print_r();?>" />
                    <input type="hidden" name="brand_id" id="brand_id" value="<?=$_REQUEST['brand_id']?>" />
                    <input type="hidden" name="type_life" id="type_life" value="<?=$_REQUEST['type_life']?>" />
                    <input type="hidden" name="min_price" id="min_price" value="<?=$_REQUEST['min_price']?>" />
                    <input type="hidden" name="max_price" id="max_price" value="<?=$_REQUEST['max_price']?>" />
                    <input type="hidden" name="dog_breed_type" id="dog_breed_type" value="<?=$_REQUEST['dog_breed_type']?>" />
                    <input type="hidden" name="breedname" id="breedname" value="<?=$_REQUEST['breedname']?>" />
                    <div class="share_fb" style="padding:7px 0px;"><input data-role="none" type="radio" name="sortbypirce" id="sortbypirce" value="1" onClick="sortBy();"style="position:relative; display:none; " /><label for="sortbypirce">Price: Low to High</label></div>
                    <div class="share_fb" style="padding:7px 0px; border-top:1px solid #ddd;"><input data-role="none" type="radio" name="sortbypirce" id= "sortbypircen" id="sortbypirce" value="2" onClick="sortBy();" style="position:relative; display:none;" /><label for="sortbypircen" data-role="none">Price: High to Low</label></div>
                </form>
            </div></div>
        </div>
        
	<div id="grooming-footer" data-role="footer" data-position="fixed" style="bottom: 0px;z-index: 999999;position: fixed;width: 100%;">
    	<div class="sortby_det"><a href="#positionWindow" data-rel="popup" data-transition="pop" data-ajax="true"><img src="https://m.dogspot.in/images/sortby-new.png" style="vertical-align:middle;">Sort by</a></div>
        <div class="filters_det"><a href="https://m.dogspot.in/filters.php?catid=<?=$cat_id; ?>" data-ajax="false"><img src="https://m.dogspot.in/images/filter-by-new.png" style="vertical-align:middle;">Filters</a></div>
	</div>
<?php require_once('common/bottom.php'); ?>
