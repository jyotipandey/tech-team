<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);

if($section[0]==''){
	require_once("../constants.php");
}
//echo SITEMAIN_URL;
require_once(SITEMAIN_URL."/database.php");
require_once(SITEMAIN_URL."/functions.php");
require_once(SITEMAIN_URL."/functions2.php");
require_once(SITEMAIN_URL.'/shop/arrays/shop-categorys.php');
require_once(SITEMAIN_URL.'/session.php');
?>

<!DOCTYPE html>
<html>
<head>
<title>Dogs Images | Dog Pictures | Dogs Photos | Dog Wallpaper | Dog Photo Album | Puppy Pictures</title>
<link type="text/css" rel="stylesheet" href="/photos/css/photos.css?v2">
  <link rel="stylesheet" type="text/css" href="/photos/css/slick.css?=b8">
  <style type="text/css">
    .slick-prev:before,
    .slick-next:before {
        color: black;}
    
  </style>
<link rel="canonical" href="" />

<?php require_once($DOCUMENT_ROOT .'/common/script.php'); ?>
<?php require_once($DOCUMENT_ROOT .'/common/top.php'); ?>
<!-- photos section start-->
<div class="photos-section">
  
  
  <section class="slider-for slider" >
    <div>
      <img data-lazy="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_562.jpg" data-srcset="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-sizes="100vw">
    </div>
    <div>
 <img data-lazy="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_561.jpg" data-srcset="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-sizes="100vw">
    </div>
    <div>
 <img data-lazy="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-srcset="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-sizes="100vw">
    </div>
    <div>
 <img data-lazy="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_560.jpg" data-srcset="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-sizes="100vw">
    </div>
    <div>
 <img data-lazy="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_559.jpg" data-srcset="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-sizes="100vw">
    </div>
    <div>
      <!-- this slide should inherit the sizes attr from the parent slider -->
      <img data-lazy="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-srcset="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg" data-sizes="100vw">
    </div>
  </section>
<section class="slider-nav slider">
    <div>
    <div class="silk-box">  <img src="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_562.jpg?text=1">
            <div class="dogshow_thumbscaption">13 Jan 2015</div>	</div>
    </div>
    <div>
     <div class="silk-box"> <img src="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_561.jpg?text=2">
      <div class="dogshow_thumbscaption">13 Jan 2015</div>				
		</div>					
    </div>
    <div><div class="silk-box">
      <img src="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg?text=3">
            <div class="dogshow_thumbscaption">13 Jan 2015</div></div>	
    </div>
    <div><div class="silk-box">
      <img src="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_560.jpg?text=4">
            <div class="dogshow_thumbscaption">13 Jan 2015</div>	
            </div>
    </div>
    <div>
    <div class="silk-box">
      <img src="https://www.dogspot.in/event_images/722x485-shepherd-club-delhi_559.jpg?text=5">
            <div class="dogshow_thumbscaption">13 Jan 2015</div>
            </div>	
    </div>
    <div>
      <div class="silk-box"><img src="https://www.dogspot.in/event_images/722x485-gurgaon-dog-show-2-feb-2014_379.jpg?text=6">   
         <div class="dogshow_thumbscaption">13 Jan 2015</div>	
    </div>
    </div>
  </section>
  


  
  
  
</div>
<!-- photos list start--> 

<!-- photos list end--> 
<!-- photos section end-->
 <!--<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>-->
  <script src="/photos/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
   
	$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: false,
  focusOnSelect: true,
  arrows: false,
  
});
  </script>
<?php require_once($DOCUMENT_ROOT .'/common/bottom.php'); ?>
<!-- cart page start--> 

