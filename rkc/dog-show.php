<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows</title> 
<meta content="Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows" name="keywords">
<meta content="Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows" name="description">
<?php require_once('../common/top.php'); ?>     
<div id="pagebody">      
<h1>Dog Show Registration 2007- 08</h1>      

<h2>Rohilkhand Kennel Club- 25th & 26th All India All Breed championship KCI Dog Show <br>
Date: 18th November 2007 (Sunday)<br>
Venue: Police Lines, Chopla, Bareilly</h2>
<h2>Entry Fee</h2>
<p> Rs. 300/- per Dog per Class for both shows plus Rs.50/- of catalogue (Catalogue is compulsory). <strong>Online entries closes on 14th of November.</strong></p>
<p> Free Entry: Rampur Hound breed is exempt from entry fee. Free entry will not be taken online.</p>
<hr />
<p>&nbsp;</p>
<h2>Dog Show Rules</h2>
<p>1. Elligibility: Only dogs registered with Kennel Club of India (KCI) may compete. Cryptorchid and monorchid dogs will not be allowed to compete.</p>
<p>2. Caution: Exhibitors should make themselves familiar with the KCI Rules and Regulations for Shows. Copies of which may be obtained from secretary KCI on Payment of Rs. 25/- </p>
<p>3. Entry in the wrong class or breed will automatically disqualify the exhibit.</p>
<p>4. The show committee reserves the right to nominate a new judge/judges in the event of any of the above mentioned judges not being able to fulfill their obligation.</p>
<p>5. Exhibitors will have to follow the order of judging and make themselves present in the waiting ring at the time of judging.</p>
<p>6. The club shall not be responsible if exhibitors are not present in the ring when their class is being judged and their exhibits have been marked absent. Creation of any sort of commotion or unpleasantness at the ring side, if a class is missed, will automatically call for stringent action against the handler / owner. </p>
<p>7. Online ENTRY closes on 14TH November 2007. Offline ENTRY strictly closes on 12TH November 2007. No entries will be taken on the ground.</p>
<p>8. The decision of the show committee is final subject only to appeal to KCI.</p>
<p>9. Vet Examination: All Dogs must be passed by the honorary veterinary surgeon present at the gate and be certified fit by him before admission. The Vet certificate should be produced on demand.</p>
<p>10. Challenge Certificate: Challenge Certificates (CC) will be awarded at the show. All Indian breeds are eligible for CCs as per the KCI rules. </p>
<p>11. Penalty: Any Willful misrepresentation regarding ownership, age, name, pedigree etc of dogs entered and/or prepared for exhibition in contravention of KCI regulations, will be liable for disciplinary action by the committee.</p>
<p>12. Objections: objections will be decided by the KCI committee in accordance with the KCI regulations. The objection fee is Rs. 1000/- Refundable if the objection is held valid by the show committee, if turned down the fee will be forfeited.</p>
<p>13. Exhibitors or handlers are hereby warned that any one found drinking alcohol or have consumed alcohol in side the show grounds while handling the exhibit in the show ring will be reported to the KCI. Such persons are liable to be suspended for a minimum period of two years and shall be fined Rs.1000/- or more as committee may decide.</p>
<p>14. Double handling is strictly prohibited. Any exhibit double handled is liable to be disqualified for the show.</p>
<p>15. The exhibitors are requested not to carry any logo or name of their kennels on their clothing or else where while exhibiting their dogs.</p>
<hr />
<br />
<h2>Definition of Classes</h2>
<p>1. Minor Puppy Class: Puppies over 4 months and not exceeding 6 months.</p>
<p>2. Puppy Class: Puppies over 6 months and not exceeding 12 months</p>
<p>3. Junior Class: Dogs over 12 months and under 18 months</p>
<p>4. Intermediate Class: Dogs over 18 months and under 36 months</p>
<p>5. Bred in India Class: Dogs bred and born in India. Dogs bred abroad and born in India will be considered as imported class.</p>
<p>6. Open Class: All dogs of that breed.</p>
<p>&nbsp;</p>
<?php require_once('../common/bottom.php'); ?></div>