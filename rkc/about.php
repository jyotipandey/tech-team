<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows</title> 
<meta content="Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows" name="keywords">
<meta content="Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows" name="description">
<?php require_once('../common/top.php'); ?> 
<div id="pagebody">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="160" valign="top"><div id="leftnav"> <a href="#blog"></a> 
    <a href="index.php" class="userbutton">Dog Show 2008</a> 
    
    <a href="about.php" class="userbutton">Rohilkhand Kennel Club</a> 
    <a href="https://www.dogspot.in/dog-listing/rohilkhand-kennel-club/" class="userbutton">Contacts</a> </div></td>
    <td valign="top"><div id="inbody">
      <h1>Dog Show Registration 2007- 08</h1>
      <h2>About Rohilkhand Kennel Club</h2>
      <p> Rohilkhand Kennel Club (RKC) was found in 1994 under the guidelines of <a href="http://www.thekci.org"> Kennel Club of India (KCI)</a>. Since then the show is held every year. In past shows several renowned judges viz. Nawab Nazir Yar Jung, Raja GVN Krishna Rao, Mr. C.V. Sudarshan, Mr. N. Radhakrishnan, Mr. P.S. Chatterjee, Dr. Mathew C John, Mr. R.V.Salvi &amp; Mr. Jogeshwar Singh have judged the show. </p>
      <p> The club has on roll 23 founder members, and 93 life members. The executive body consists of 14 members.</p>
      <p> Apart from the show the club helps in getting the pure bred dogs registered with the KCI. RKC has initiated various programs to microchip the dogs for ultimate safety and identity. The RKC also organizes various voluntary programs for the good will of animals.</p>
      <p> </p>
      <hr />
      <h2><br />
        Patrons </h2>
      <ul>
        <li>Mr. K.K. Trivedi</li>
        <li> IG bareily zone</li>
        <li> GOC  UP  Area</li>
        <li> Director IVRI</li>
        <li> Director AH UP </li>
      </ul>
      <h2>Rohilkhand Kennel Club Members</h2>
      <table width="400" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td><strong>President</strong></td>
          <td>Mr. Dileep Tandon</td>
        </tr>
        <tr>
          <td><strong>Vice President</strong></td>
          <td>Dr. C.K. Malhotra, Mr. Manoj Jaiswal</td>
        </tr>
        <tr>
          <td><strong>Honorary Secretary</strong></td>
          <td>Mr. Hoshiar Singh (baba)</td>
        </tr>
        <tr>
          <td><strong>Joint Secretary</strong></td>
          <td>Mr. Rakesh Bhandari</td>
        </tr>
        <tr>
          <td><strong>Treasurer</strong> </td>
          <td>Dr. Abhai Tilak </td>
        </tr>
        <tr>
          <td valign="top"><strong>Members</strong></td>
          <td><p>Dr. Abhijit Pawde<br />
            Mr. Amit Yadav<br />
            Mr. Pawan Basin<br />
            Mr. Rahul Bindal<br />
            Mrs. Ruchita Agrawal<br />
            Lt.Col. Gurdeep Singh<br />
            Mr. Jaspal Singh Gujral</p></td>
        </tr>
      </table>
      <br />
      <br />
      <hr />
      <h2><br />
        Staying in Bareilly</h2>
      <p> Participants can stay in Uberai Anand Hotel. Uberai Anand Hotel is a dog friendly hotel in Bareilly.<br />
      </p>
      <p><strong>Hotel Uberai Anand </strong><br />
          <strong>Contact Numbers</strong>: 0571-2570838, 2570839, 2570840 Civil Lines Bareilly <br />
          <strong>Rates</strong>: Starting from Rs. 750 upto Rs. 3000 <br />
          <strong>Location Location</strong>: 46- Civil Lines Bareilly<br />
          <strong>Total Rooms</strong> :88 Rooms<br />
          <strong>Check-in Time is</strong>: 12:00 AM / Check-Out Time is: 12:00 AM<br />
          <strong><br />
            Accessibility :<br />
          </strong>Airport : 85 kms.<br />
        Railway Station : 1.5 kms.<br />
        Bus Station : 0.2 kms</p>
      <hr />
    </div></td>
  </tr>
</table>    
</div> 

<?php require_once('../common/bottom.php'); ?></div>