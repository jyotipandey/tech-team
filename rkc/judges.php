<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows</title> 
<meta content="Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows" name="keywords">
<meta content="Bareilly Dog Show 2008 - Registration | Dog lovers Community, dogs, dogs india, dogs world, puppies, dog, rohilkhand kennel club, rohilkhand, dog show, dog shows" name="description">
<?php require_once('../common/top.php'); ?>     
<div id="pagebody">
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td width="160" valign="top"><table width="100%" border="0" cellpadding="3" cellspacing="0" id="miniNavi">
      
      <tr>
        <td><strong><a href="dog-show.php">Dog Show 2008</a></strong></td>
      </tr>
      <tr>
        <td><a href="about.php">Rohilkhand Kennel Club</a></td>
      </tr>
      
     
      <tr>
        <td><a href="judges.php">Show Judges</a></td>
      </tr>
      <tr>
        <td><a href="https://www.dogspot.in/dog-listing/rohilkhand-kennel-club/">Contacts</a></td>
      </tr>
    </table></td>
    <td valign="top"><h1>Bareilly Dog Show 2008</h1>
      <h2>Mr. N.Radhakrishnan </h2>
      <p align="justify"><img src="http://www.thekci.org/judges%20and%20comm/radakrishnan.jpg" width="122" height="153" hspace="10" vspace="5" align="left" />Started             taking active part in Dog game in 1968 with a Labrador Retriever. Slowly             increased the stocks by adding different breed. Uptil reared bred many             champions by showing Lhasa Apso, G.S.D., Dobermann, Irish Setter, Whippet,             Italian Grey Hound, Dachshund, Beagle, German Spitz etc.</p>
        <p>Worked in Poona Kennel Club as President, in B.P.K.C. as Managing Committee             member and Founder Patron of Mumbai Canine Club and working as Managing             Committee member of Kennel Club of India for the last 15 years. Represented             KCI as a delegate in AKU conferences in Japan and Bangkok, shown different             breeds in many shows from Cochin to Dehradun and collected many CCs             and Board Placings. Judged about 20 shows in Australia and New Zealand             in 1992 and 1996 including the prestigious Australasian show and many             shows in India. Actively involved in various commercial and social organizations.<br />
        </p>
      <hr /></td>
    </tr>
</table>
<br />
<h1>&nbsp;</h1>
<?php require_once('../common/bottom.php'); ?></div>