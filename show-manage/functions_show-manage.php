<?
// get Dog Month
function dogMonth($gd,$gm,$gy,$cd,$cm,$cy){
/*
     * Usage: -- echo curage(m,d,y); -- 
     * Where 'm' is birth month, 'd' is birth day 
     * and 'y' is birth year 
    **/ 

    $r = ''; 

    /** 
     * days of each month 
    **/ 

    for($x=1 ; $x<=12 ; $x++){ 

        $dim[$x] = date('t',mktime(0,0,0,$x,1,date('Y')));

    } 


    /** 
     * vars for current date 
    **/ 
//    $cm = 11;
//    $cd = 16; 
//    $cy = 2008; 
/*
    $cm = date('n'); 
    $cd = date('j'); 
    $cy = date('Y'); 
*/

    /** 
     * calculate differences 
    **/ 

    $m = $cm - $gm; 
    $d = $cd - $gd; 
    $y = $cy - $gy; 


    /** 
     * if the given day is ahead of the current day 
    **/ 

    if($d < 0) { 
      
        $td = $cd + $dim[$cm]; 
        $tm = $cm - 1; 
        $d = $td - $gd; 
        $m = $tm - $gm; 
        if(($tm - $gm) < 0) { 

            $tm += 12; 
            $ty = $cy - 1; 
            $m = $tm - $gm; 
            $y = $ty - $gy; 

        } 

    } 


    /** 
     * if the given month is ahead of the current month 
    **/ 

    if($m < 0) { 

        $tm = $cm + 12; 
        $ty = $cy - 1; 
        $m = $tm - $gm; 
        $y = $ty - $gy; 

} 


    /** 
     * the processing area 
    **/ 

    if($y < 0) { 

        die("That date is in the future."); 

    } else { 

        switch($y) { 

            case 0 : $yr .= ''; break; 
            case 1 : $yr .= $y.($m == 0 && $d == 0 ? '' : ''); break; 
            default : $yr .= $y.($m == 0 && $d == 0 ? '' : ''); 

        } 


        switch($m) { 

            case 0: $mr .= ''; break; 
            case 1: $mr .= ($y == 0 && $d == 0 ? $m.'' : ($y == 0 && $d != 0 ? $m.'' : ($y != 0 && $d == 0 ? ''.$m.'' : ''.$m.''))); break; 
            default: $mr .= ($y == 0 && $d == 0 ? $m.'' : ($y == 0 && $d != 0 ? $m.'' : ($y != 0 && $d == 0 ? ''.$m.'' : ''.$m.''))); break; 

        } 

    }
	$yy=$yr*12+$mr;
	//$rr="$y;$mr";
    return $yy;
}// END 


function dogClassM($gd,$gm,$gy, $dmonth){
	/* Date of birth compaire on show date, which is call frm database */
if($dmonth < 4){
echo"<div align='center'><b>Your Dog is not Eligible</b><div>";
}elseif($dmonth > 3 && $dmonth <= 6){
	$Dclass = array('A');
}elseif($dmonth > 6 && $dmonth <= 9){
	$Dclass=array('B');
}elseif($dmonth >= 10 && $dmonth <= 15){
	$Dclass=array('C');
}elseif($dmonth > 15 && $dmonth <= 18){
	$Dclass=array('C','D','E','F');
}elseif($dmonth >= 19 && $dmonth <= 24){
	$Dclass=array('D','E','F');
}elseif($dmonth > 24 && $dmonth <= 95){
	$Dclass=array('E','F');
}elseif($dmonth >= 96){
	$Dclass=array('H');
}
return $Dclass;
}// End Function 

function dogClassF($gd,$gm,$gy, $dmonth){
if($dmonth < 4){
echo"<div align='center'><b>Your Dog is not Eligible</b><div>";
}elseif($dmonth > 3 && $dmonth <= 6){
	$Dclass = array('I');
}elseif($dmonth > 6 && $dmonth <= 9){
	$Dclass=array('J');
}elseif($dmonth >= 10 && $dmonth <= 15){
	$Dclass=array('K');
}elseif($dmonth > 15 && $dmonth <= 18){
	$Dclass=array('K','L','M','N');
}elseif($dmonth >= 19 && $dmonth <= 24){
	$Dclass=array('L','M','N');
}elseif($dmonth > 24 && $dmonth <= 95){
	$Dclass=array('M','N');
}elseif($dmonth >= 96){
	$Dclass=array('P');
}
return $Dclass;
}// End Function 

//------------------------------FCI-------------------------------------------
function dogClassFCIM($gd,$gm,$gy, $dmonth){
	/* Date of birth compaire on show date, which is call frm database */
if($dmonth < 4){
echo"<div align='center'><b>Your Dog is not Eligible</b><div>";
}elseif($dmonth >= 4 && $dmonth <= 6){
	$Dclass = array('A','B','C','D','E','F','G');
}elseif($dmonth > 6 && $dmonth <= 12){
	$Dclass = array('A','B','C','D','E','F','G');
}elseif($dmonth > 12 && $dmonth <= 18){
	$Dclass = array('A','B','C','D','E','F','G');
}elseif($dmonth > 18 && $dmonth <= 36){
	$Dclass = array('A','B','C','D','E','F','G');
}else{
	$Dclass = array('A','B','C','D','E','F','G');
}
return $Dclass;
}// End Function 

function dogClassFCIF($gd,$gm,$gy, $dmonth){
if($dmonth < 4){
echo"<div align='center'><b>Your Dog is not Eligible</b><div>";
}elseif($dmonth >= 4 && $dmonth <= 6){
	$Dclass = array('H','I','J','K','L','M','N');
}elseif($dmonth > 6 && $dmonth <= 12){
	$Dclass = array('H','I','J','K','L','M','N');
}elseif($dmonth > 12 && $dmonth <= 18){
	$Dclass = array('H','I','J','K','L','M','N');
}elseif($dmonth > 18 && $dmonth <= 36){
	$Dclass = array('H','I','J','K','L','M','N');
}else{
	$Dclass = array('H','I','J','K','L','M','N');
}
return $Dclass;
}// End Function 
//------------------------------FCI-------------------------------------------
?>