<?
include($DOCUMENT_ROOT."/session.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/functions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apply for Free to use Catalog Builder</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
<script src="http://jquery.bassistance.de/validate/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
  $().ready(function() {
	  $("#applycb").validate({
		  rules: {
			name: "required",
			clubname:"required",
			email: {
				required: true,
				email: true
			},
			mobile:	"required",
			designation: "required"
		  },
		  messages: {
			name: "",
			clubname:"",
			email: "",
			mobile:"",
			designation: ""
		  }
	  });
});	
</script>
<?php require_once($DOCUMENT_ROOT.'/common/top.php'); ?>
<link href="css/cb.css" rel="stylesheet" type="text/css" />
<div id="pagebody">
<div id="greedBox">
<h1>Free to use Catalog Builder</h1>
<ul>
  <li><strong>Full catalog sorting as per KCI Rules</strong></li>
  <li><strong>No Loss of data back up online</strong></li>
  <li><strong>Different Catalog Formats for printing</strong></li>
</ul>
<?
if (isset($_POST['apply'])){
	$mailSubject = "DogSpot: Apply for Catalog Builder";
	$mailBody = '
<table width="100%" border="0" cellpadding="3" cellspacing="0">
  <tr> 
    <td><div align="center"><strong>Apply for </strong></div></td>
  </tr>
  <tr>
    <td>DogSpot ID: '.$userid.'</td>
  </tr>
  <tr> 
    <td>Name:'.$name.'</td>
  </tr>
  <tr> 
    <td>Name of the Club:'.$clubname.'</td>
  </tr>
  <tr>
    <td>Email:'.$email.'</td>
  </tr>
  <tr>
    <td>Mobile Number:'.$mobile.'</td>
  </tr>
  <tr>
    <td>Your Designation with Club:'.$designation.'</td>
  </tr>
</table>
';
 
    $mailBody .= $mailBottom;
	
	$toEmail = 'DogSpot <contact@dogspot.in>';
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: '.$from.' <'.$from_email.'>' . "\r\n";

	$headers .= 'Reply-To: '.$from.' <'.$from_email.'>' . "\r\n";
	$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
	$headers .= "Organization: DogSpot\r\n";

	if(mail($toEmail, $mailSubject, $mailBody, $headers,"-f contact@dogspot.in -r mybounceemail@dogspot.in")){
	  echo"<h1>Success!</h1> <p>Thank you for applying for catalog builder account. <br>Team DogSpot will soon get in touch with you</p>";
	}else {
	  echo"<h1>Error!</h1> <p>Error in sending mail!</p>";
	}
}else{
?>

<form id="applycb" name="applycb" method="post" action="">

<table width="100%" border="0">
  <tr>
    <td width="40%">Your Name:<label class="error">*</label></td>
    <td><input name="name" type="text" id="name" style="width:300px;" /></td>
  </tr>
  <tr>
    <td>Name of the Club:<label class="error">*</label></td>
    <td><input type="text" name="clubname" id="clubname"style="width:300px;" /></td>
  </tr>
  <tr>
    <td>Your Email:
      <label class="error">*</label></td>
    <td><input type="text" name="email" id="email"style="width:300px;" /></td>
  </tr>
  <tr>
    <td>Mobile Number:<label class="error">*</label></td>
    <td><input type="text" name="mobile" id="mobile" /></td>
  </tr>
  <tr>
    <td>Your Designation with Club:<label class="error">*</label></td>
    <td><input type="text" name="designation" id="designation" style="width:300px;"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="apply" id="apply" value="Submit" class="AlinkButton"/></td>
  </tr>
</table>
</form>
<? }?>
<div id="clearall"></div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>