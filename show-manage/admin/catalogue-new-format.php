<?php
	include($DOCUMENT_ROOT . "/session-require.php");
	include($DOCUMENT_ROOT . "/database.php");
	include($DOCUMENT_ROOT . "/functions.php");
	include($DOCUMENT_ROOT . "/show-manage/show-functions.php");
	include($DOCUMENT_ROOT . "/arrays.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo "$show_name";?></title>
</head>

<body>
<?php
	$class_num = 1;
	$selGroup  = mysql_query("SELECT DISTINCT group_id, group_code FROM show_dog_show WHERE show_id='$show_id' ORDER BY group_id");
	if (!$selGroup) {
    	die(mysql_error());
	}
	$j = 0; 
	while ($rowGroup = mysql_fetch_array($selGroup)) {
		$group_code = $rowGroup["group_code"];
	    $group_id   = $rowGroup["group_id"];
        $rowGroupName = getSingleRow("group_name", "show_group", "group_id = '$group_id'");
    	$group_name   = $rowGroupName["group_name"];
		$selBreed = mysql_query("SELECT DISTINCT dog_breed FROM show_dog_show WHERE group_id = '$group_id' AND show_id = '$show_id' ORDER BY dog_breed_name");
	    if (!$selBreed) {
    	    die(mysql_error());
    	}
		while ($rowBreed = mysql_fetch_array($selBreed)) { 
			$dog_breed    = $rowBreed["dog_breed"];
        	$rowGroupName = getSingleRow("breed_name", "show_breed", "breed_id = '$dog_breed'");
       		$breed_name   = $rowGroupName["breed_name"];
        	// Get Dog and User info
        	$checkCode='';
        	$selClass = mysql_query("SELECT DISTINCT c.class_code,class_id FROM show_dog_show AS s, show_dog_class AS c WHERE s.dog_breed = '$dog_breed' AND s.dog_id=c.dog_id AND s.show_id=c.show_id AND c.show_id='$show_id' AND s.show_id='$show_id' ORDER BY c.class_code");
        	while ($showDog_id = mysql_fetch_array($selClass)) {
            	$class_id   = $showDog_id['class_id'];
             	$class_code = $showDog_id['class_code'];
				$breedName  = query_execute_row("SELECT class_name FROM show_class WHERE class_id='$class_id'");
				$class_name=$breedName['class_name'];
				$checkMale=array('A','B','C','D','E','F','G','H');
				$checkFemale=array('I','J','K','L','M','N','O','P');
				//$checkChampion=array('M');
				$checkCode[]=$class_code."@@@".$class_id;
				foreach($checkCode as $code12){ 
					$code121 = explode("@@@", $code12);
					$code = $code121['0'];
			 		if(in_array($code,$checkMale)){
						$temp1=$code; 
			 		}elseif(in_array($code,$checkFemale)){
						$temp2=$code; 
			 		}
			 	}
			}
			$count_dog=count($checkCode);
			foreach($checkCode as $classCode1){
				$j++;
				$classCode_arr = explode("@@@", $classCode1);
				$classcode = $classCode_arr[0];
				$classID = $classCode_arr[1];
				$SQL_cl_name = query_execute_row("SELECT class_name FROM show_class WHERE class_id='$classID'");
				$className = $SQL_cl_name['class_name'];
				
				$selExhabit= mysql_query("SELECT s.exhibit_num FROM show_dog_show AS s, show_dog_class AS c WHERE c.class_code='$classcode' AND s.show_id='$show_id' AND s.show_id=c.show_id AND s.dog_id=c.dog_id AND s.dog_breed = '$dog_breed' ORDER BY s.exhibit_num ASC");
				
				$grp_id= query_execute_row("SELECT s.group_id, s.group_code FROM show_dog_show AS s, show_dog_class AS c WHERE c.class_code='$classcode' AND s.show_id='$show_id' AND s.show_id=c.show_id AND s.dog_id=c.dog_id AND s.dog_breed = '$dog_breed'");
				$grp_info = $grp_id['group_code'];
		
				if (!$selExhabit){ die(mysql_error()); }
				$totrecord = mysql_num_rows($selExhabit);
				$exhibit_num='';
?>
<!-- main table start-->

<table style="font-size:16px; background:#fff; padding-top:5px; margin-top:5px; padding-bottom:5px; font-size:13px; font-family:sans-serif; border-collapse:collapse;  width:624px;"  border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody>
		<tr>
			<td style="" valign="top" align="center">
			<!-- table header-->
 			<!-- table header end-->
			<!-- table row start-->
<table width="100%"  border="0" cellpadding="0" cellspacing="0" style="   border-collapse:collapse; width:624px; border-bottom:1px solid #333;">
	<tr>
		<td colspan="3" style="width:300px; border-right:2px solid #333; padding-bottom:10px;">
			<div style="width:100%; float:left; margin-bottom:15px;">
                <div style="float:left; width:70px; padding-left: 5px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:200px; margin-left:5px; margin-top: -7px;">
				<?=$j." - ".$classcode." ($className)"?></div>
			</div>
            <div style="width:100%; float:left;  margin-bottom:15px;">
                <div style="float:left; width:50px; padding-left: 5px;">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:220px; margin-left:5px; margin-top: -7px;"><?=$breed_name?></div>
			</div>
            <div style="width:100%; float:left;">
				<div style="float:left; width:82px; padding-left: 5px;" >No. of Prizes</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: -7px;">&nbsp;</div>
			</div>
            <div style="width:100%; float:left; margin-top:10px;">
				<div style="float:left; width:82px; padding-left: 5px;" >Group ID</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: -2px;"><?=$grp_info?></div>
			</div>
		</td>
		<td colspan="2" style="width:162px; border-right:2px solid #333; padding-bottom:10px;">
			<div style="font-size:15px;  margin-top:10px;padding-left: 5px; margin-bottom:10px;"><strong>K.C.I. <br>Office Slip</strong></div>
			<div style=" float:left; width:100%; margin-top:5px; margin-bottom:10px;">
            	<div style="float:left; width:64px; padding-left: 5px; ">Class No.</div>
	            <div style="border-bottom:1px dotted #333; float:left; width: 73px; margin-left:5px; margin-top: -7px;">
				<?=$j." - ".$classcode." ($className)"?></div>
    		</div>
            <div style="width:100%; float:left; margin-bottom:5px;">
                <div style="float:left; width:52px; padding-left: 5px; ">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:88px; margin-left:5px; margin-top: -7px;"><?=$breed_name?></div>
			</div>
            <div style="width:100%; float:left; margin-bottom:5px; margin-top:10px;">
                <div style="float:left; width:52px; padding-left: 5px; ">Group ID </div>
                <div style="border-bottom:1px dotted #333; float:left; width:70px; margin-left:5px; margin-top: -2px;"><?=$grp_info?></div>
			</div>
    	</td>
		<td colspan="2" style="width:162px; padding-bottom:10px;">
			<div style="font-size:15px; margin-top:10px; padding-left: 5px;"><strong>Slip for <br>Award Board </strong></div>
			<div style="width:100%; float:left; margin-top:15px; margin-bottom:10px;">
                <div style="float:left; width:65px; padding-left: 5px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:86px; margin-left:5px; margin-top: -7px;">
				<?=$j." - ".$classcode." ($className)"?></div>
			</div>
              
            <div style="width:100%; float:left; margin-bottom:10px;"> 
            	<div style="float:left; width:50px; padding-left: 5px;">BREED </div>
              	<div style="border-bottom:1px dotted #333; float:left; width:88px; margin-left:5px; margin-top: -7px;"><?=$breed_name?></div>
			</div>
            <div style="width:100%; float:left; margin-bottom:10px; margin-top:10px;"> 
            	<div style="float:left; width:52px; padding-left: 5px;">Group ID </div>
              	<div style="border-bottom:1px dotted #333; float:left; width:86px; margin-left:5px; margin-top: -2px;"><?=$grp_info?></div>
			</div>
		</td>
	</tr>
 	<tr>
		<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:10px; padding-bottom:10px; border-top:2px solid #333; border-bottom:2px  solid #333; ">Number</td>
		<td  style="width:60px;  text-align:center; border-top:2px solid #333; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
		<td  style="width:180px;  text-align:center; border-top:2px solid #333; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; padding-top:10px; padding-bottom:10px;">NOTES</td>
		<td  style="width:92px;  text-align:center; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333;  border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Number</td>
		<td  style="width:70px;  text-align:center;  border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
		<td  style="width:92px;  text-align:center;   border:1px solid #333;border-right:2px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Number</td>
		<td  style="width:70px;  text-align:center; border:1px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
	</tr>
    
    <? $countRow = mysql_num_rows($selExhabit); 
	$storeBlank='';
	if($countRow>17){
		
		$storeBlank="SELECT s.exhibit_num FROM show_dog_show AS s, show_dog_class AS c WHERE c.class_code='$classcode' AND s.show_id='$show_id' AND s.show_id=c.show_id AND s.dog_id=c.dog_id AND s.dog_breed = '$dog_breed' ORDER BY s.exhibit_num ASC LIMIT 16,50";
	$selExhabit= mysql_query("SELECT s.exhibit_num FROM show_dog_show AS s, show_dog_class AS c WHERE c.class_code='$classcode' AND s.show_id='$show_id' AND s.show_id=c.show_id AND s.dog_id=c.dog_id AND s.dog_breed = '$dog_breed' ORDER BY s.exhibit_num ASC LIMIT 16");	
	//while($rowExhabit = mysql_fetch_array($selExhabit)){ $ex=$rowExhabit['exhibit_num']; }	
	$countRow = mysql_num_rows($selExhabit); 
	}
	while($rowExhabit = mysql_fetch_array($selExhabit)){ $ex=$rowExhabit['exhibit_num'];?>
	<tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px; "><?=$ex?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:92px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$ex?></td>
		<td  style="width:70px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:92px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$ex?></td>
		<td  style="width:70px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? } ?>
    <? if($countRow< '17'){ for($i=1; $i<=17-$countRow; $i++){?>
    <? if($i==15-$countRow){?>
    
    <!-------Testing 1-------->
    <?  $xcode=""; $xcode1="";$temp3='';$xcode2=""; $xcode3="";
		if($temp3!="" && $temp1!="" && $temp2!=""){
			if($classcode==$temp1 || $classcode==$temp2){ 
				$xcode = "CC"; $xcode1="RCC"; 
			}elseif($classcode==$temp3){ 
				$xcode ="BOB"; $xcode1 = "RBOB"; 
			}				
		}elseif($count_dog==1 && ($classcode==$temp1 || $classcode==$temp2) && $temp3==""){ 
			$xcode = "CC";
			$xcode1 = "BOB"; 
		}elseif($count_dog>1 && ($classcode==$temp1) && $temp3==""){ 
			$xcode = "CC";
			$xcode1 = "RCC";
		}elseif($count_dog > 1 && $classcode==$temp2){
			$xcode = "CC";
			$xcode1 = "RCC";
			$xcode2 = "BOB";
			$xcode3 = "RBOB";
		}else{
			$xcode=""; $xcode1="";$xcode2=""; $xcode3="";
		}
	?>
    <!------Testing 1 END-------->
    <? if($xcode!=""){?>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode?></td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode?></td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? } if($xcode1!=""){ ?>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode1?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode1?></td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode1?></td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? }if($xcode2!=""){?>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode2?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode2?></td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode2?></td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? }if($xcode3!=""){?>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode3?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode3?></td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode3?></td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? }}else{?>
    <tr>
        <td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;">&nbsp;</td>
        <td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; ">&nbsp;</td>
        <td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>	
	<? } } }else{?>
	<tr>
        <td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode?></td>
        <td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode?></td>
        <td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode?></td>
        <td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <tr>
        <td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode1?></td>
        <td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode1?></td>
        <td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode1?></td>
        <td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
	
	<? } ?>
</table>
<!-- table row end-->
</td>
</tr>
</tbody>
</table>
<div style="page-break-after: always;"></div>
<? if($storeBlank !=''){
	$selExhabit=mysql_query($storeBlank); 
	$countRow = mysql_num_rows($selExhabit);?>
	<!-- main table start-->

<table style="font-size:16px; background:#fff; padding-top:5px; margin-top:5px; padding-bottom:5px; font-size:13px; font-family:sans-serif; border-collapse:collapse;  width:624px;"  border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody>
		<tr>
			<td style="" valign="top" align="center">
			<!-- table header-->
 			<!-- table header end-->
			<!-- table row start-->
<table width="100%"  border="0" cellpadding="0" cellspacing="0" style="   border-collapse:collapse; width:624px; border-bottom:1px solid #333;">
	<tr>
		<td colspan="3" style="width:300px; border-right:2px solid #333; padding-bottom:10px;">
			<div style="width:100%; float:left; margin-bottom:15px;">
                <div style="float:left; width:70px; padding-left: 5px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:200px; margin-left:5px; margin-top: -7px;">
				<?=$j." - ".$classcode." ($className)"?></div>
			</div>
            <div style="width:100%; float:left;  margin-bottom:15px;">
                <div style="float:left; width:50px; padding-left: 5px;">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:220px; margin-left:5px; margin-top: -7px;"><?=$breed_name?></div>
			</div>
            <div style="width:100%; float:left;">
				<div style="float:left; width:82px; padding-left: 5px;" >No. of Prizes</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: -7px;">&nbsp;</div>
			</div>
            <div style="width:100%; float:left; margin-top:10px;">
				<div style="float:left; width:82px; padding-left: 5px;" >Group ID</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: -2px;"><?=$grp_info?></div>
			</div>
		</td>
		<td colspan="2" style="width:144px; border-right:2px solid #333; padding-bottom:10px;">
			<div style="font-size:15px;  margin-top:10px;padding-left: 5px; margin-bottom:10px;"><strong>K.C.I. <br>Office Slip</strong></div>
			<div style=" float:left; width:100%; margin-top:5px; margin-bottom:10px;">
            	<div style="float:left; width:64px; padding-left: 5px; ">Class No.</div>
	            <div style="border-bottom:1px dotted #333; float:left; width: 40px; margin-left:5px; margin-top: -7px;">
				<?=$j." - ".$classcode." ($className)"?></div>
    		</div>
            <div style="width:100%; float:left; margin-bottom:5px;">
                <div style="float:left; width:52px; padding-left: 5px; ">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:70px; margin-left:5px; margin-top: -7px;"><?=$breed_name?></div>
			</div>
            <div style="width:100%; float:left; margin-bottom:5px; margin-top:10px;">
                <div style="float:left; width:52px; padding-left: 5px; ">Group ID </div>
                <div style="border-bottom:1px dotted #333; float:left; width:70px; margin-left:5px; margin-top: -2px;"><?=$grp_info?></div>
			</div>
    	</td>
		<td colspan="2" style="width:180px; padding-bottom:10px;">
			<div style="font-size:15px; margin-top:10px; padding-left: 5px;"><strong>Slip for <br>Award Board </strong></div>
			<div style="width:100%; float:left; margin-top:15px; margin-bottom:10px;">
                <div style="float:left; width:65px; padding-left: 5px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:90px; margin-left:5px; margin-top: -7px;">
				<?=$j." - ".$classcode." ($className)"?></div>
			</div>
              
            <div style="width:100%; float:left; margin-bottom:10px;"> 
            	<div style="float:left; width:50px; padding-left: 5px;">BREED </div>
              	<div style="border-bottom:1px dotted #333; float:left; width:105px; margin-left:5px; margin-top: -7px;"><?=$breed_name?></div>
			</div>
            <div style="width:100%; float:left; margin-bottom:10px; margin-top:10px;"> 
            	<div style="float:left; width:52px; padding-left: 5px;">Group ID </div>
              	<div style="border-bottom:1px dotted #333; float:left; width:105px; margin-left:5px; margin-top: -2px;"><?=$grp_info?></div>
			</div>
		</td>
	</tr>
 	<tr>
		<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:10px; padding-bottom:10px; border-top:2px solid #333; border-bottom:2px  solid #333; ">Number</td>
		<td  style="width:60px;  text-align:center; border-top:2px solid #333; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
		<td  style="width:180px;  text-align:center; border-top:2px solid #333; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; padding-top:10px; padding-bottom:10px;">NOTES</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333;  border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Number</td>
		<td  style="width:80px;  text-align:center;  border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
		<td  style="width:100px;  text-align:center;   border:1px solid #333;border-right:2px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Number</td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
	</tr>
    <? $countRow = mysql_num_rows($selExhabit); 

	while($rowExhabit = mysql_fetch_array($selExhabit)){ $ex=$rowExhabit['exhibit_num'];?>
	<tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px; "><?=$ex?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$ex?></td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$ex?></td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? } ?>
    <? if($countRow< '17'){ for($i=1; $i<=17-$countRow; $i++){?>
    <? if($i==15-$countRow){?>
    
    <!-------Testing 1-------->
    <?  $xcode=""; $xcode1="";
		if($temp3!="" && $temp1!="" && $temp2!=""){
			if($classcode==$temp1 || $classcode==$temp2){ 
				$xcode = "CC"; $xcode1="RCC"; 
			}elseif($classcode==$temp3){ 
				$xcode ="BOB"; $xcode1 = "RBOB"; 
			}				
		}elseif($count_dog==1 && ($classcode==$temp1 || $classcode==$temp2) && $temp3==""){ 
			$xcode = "CC";
			$xcode1 = "BOB"; 
		}elseif($count_dog>1 && ($classcode==$temp1 || $classcode==$temp2) && $temp3==""){ 
			$xcode = "BOB";
			$xcode1 = "RBOB";
		}elseif($count_dog > 1 && $classcode==$temp3 && $temp1=="" && $temp2==""){
			$xcode = "BOB";
			$xcode1 = "RBOB";
		}elseif($count_dog > 1 && $temp3=="M" && $temp1!="" && $temp2!=""){
			$xcode = "BOB";
			$xcode1 = "RBOB";
		}else{
			$xcode=""; $xcode1="";
		}
	?>
    <!------Testing 1 END-------->
    <? if($xcode!=""){?>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode?></td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode?></td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? } if($xcode1!=""){ ?>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode1?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode1?></td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode1?></td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <? }}else{?>
    <tr>
        <td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;">&nbsp;</td>
        <td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; ">&nbsp;</td>
        <td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>	
	<? } } }else{?>
	<tr>
        <td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode?></td>
        <td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode?></td>
        <td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode?></td>
        <td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <tr>
        <td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><?=$xcode1?></td>
        <td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"><?=$xcode1?></td>
        <td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; "><?=$xcode1?></td>
        <td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
	
	<? } ?>
</table>
<!-- table row end-->
</td>
</tr>
</tbody>
</table>
<div style="page-break-after: always;"></div>
<?php } }  ?>
<!-- main table end-->
<? } ?>
<!-------------Umesh ----->
<table style="font-size:16px; background:#fff; padding-top:5px; padding-bottom:5px; font-size:13px; font-family:sans-serif; border-collapse:collapse;  width:624px;"  border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody>
		<tr>
			<td style="" valign="top" align="center">
			<!-- table header-->
 			<!-- table header end-->
			<!-- table row start-->
<table width="100%"  border="0" cellpadding="0" cellspacing="0" style="   border-collapse:collapse; width:624px; border-bottom:1px solid #333;">
	<tr>
		<td colspan="3" style="width:300px; border-right:2px solid #333; padding-bottom:10px;">
			<div style="width:100%; float:left;">
                <div style="float:left; width:70px; padding-left: 5px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:200px; margin-left:5px; margin-top: 10px;"></div>
			</div>
            <div style="width:100%; float:left;  margin-bottom:15px; margin-top:15px;">
                <div style="float:left; width:50px; padding-left: 5px;">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:220px; margin-left:5px; margin-top: 0px;">GROUP WINNER</div>
			</div>
            <div style="width:100%; float:left;">
				<div style="float:left; width:82px; padding-left: 5px;" >No. of Prizes</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: 10px;"></div>
			</div>
            <div style="width:100%; float:left; margin-top:10px;">
				<div style="float:left; width:82px; padding-left: 5px;" >Group ID</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: 0px;">GROUP <?=$group_id?></div>
			</div>
		</td>
		<td colspan="2" style="width:144px; border-right:2px solid #333; padding-bottom:10px;">
			<div style="font-size:15px;  margin-top:10px;padding-left: 5px; margin-bottom:10px;"><strong>K.C.I. <br>Office Slip</strong></div>
			<div style=" float:left; width:100%; margin-top:5px; ">
            	<div style="float:left; width:64px; padding-left: 5px; ">Class No.</div>
	            <div style="border-bottom:1px dotted #333; float:left; width: 40px; margin-left:5px; margin-top: 10px;"></div>
    		</div>
            <div style="width:100%; float:left; margin-top:15px; margin-bottom:15px;">
                <div style="float:left; width:50px; padding-left: 5px; ">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:70px; margin-left:5px; margin-top: 0px;">GROUP WINNER</div>
			</div>
            <div style="width:100%; float:left; margin-bottom:5px; margin-top:10px;">
                <div style="float:left; width:52px; padding-left: 5px; ">Group ID </div>
                <div style="border-bottom:1px dotted #333; float:left; width:70px; margin-left:5px; margin-top: 0px;">GROUP <?=$group_id?></div>
			</div>
    	</td>
		<td colspan="2" style="width:180px; padding-bottom:10px;">
			<div style="font-size:15px; margin-top:10px; padding-left: 5px;"><strong>Slip for <br>Award Board </strong></div>
			<div style="width:100%; float:left; margin-top:15px; ">
                <div style="float:left; width:65px; padding-left: 5px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:90px; margin-left:5px; margin-top: 10px;"></div>
			</div>
            <div style="width:100%; float:left; margin-top:15px; margin-bottom:15px;"> 
            	<div style="float:left; width:50px; padding-left: 5px;">BREED </div>
              	<div style="border-bottom:1px dotted #333; float:left; width:105px; margin-left:5px; margin-top: 0px;">GROUP WINNER</div>
			</div>
            <div style="width:100%; float:left; margin-bottom:10px; margin-top:10px;"> 
            	<div style="float:left; width:52px; padding-left: 5px;">Group ID </div>
              	<div style="border-bottom:1px dotted #333; float:left; width:105px; margin-left:5px; margin-top: 0px;">GROUP <?=$group_id?></div>
			</div>
		</td>
	</tr>
 	<tr>
		<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:10px; padding-bottom:10px; border-top:2px solid #333; border-bottom:2px  solid #333; ">Number</td>
		<td  style="width:60px;  text-align:center; border-top:2px solid #333; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
		<td  style="width:180px;  text-align:center; border-top:2px solid #333; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; padding-top:10px; padding-bottom:10px;">NOTES</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333;  border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Number</td>
		<td  style="width:80px;  text-align:center;  border:1px solid #333; border-right:2px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
		<td  style="width:100px;  text-align:center;   border:1px solid #333;border-right:2px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Number</td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-bottom:2px  solid #333; border-top:2px solid #333; padding-top:10px; padding-bottom:10px;">Prize</td>
	</tr>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px; ">Ist Winner</td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"></td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; ">&nbsp;</td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;"><? if($group_id=='3' || $group_id=='4'){ echo ""; }else{ echo "IInd Winner"; }?></td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;"></td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; ">&nbsp;</td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;">
        <? if($group_id=='2'){ echo "IIIrd Winner"; }?>
        </td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; ">&nbsp;</td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <tr>
    	<td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;">Puppy</td>
		<td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
		<td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; ">&nbsp;</td>
		<td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>
    <?php 
	for($r=1; $r<25;$r++){?>
    <tr>
        <td  style="width:80px; text-align:center; border:1px solid #333; border-right:2px solid #333; padding-top:5px; padding-bottom:5px;">&nbsp;</td>
        <td  style="width:60px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:180px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:64px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:80px;  text-align:center; border:1px solid #333; border-right:2px solid #333;">&nbsp;</td>
        <td  style="width:100px;  text-align:center; border:1px solid #333;border-right:2px solid #333; ">&nbsp;</td>
        <td  style="width:80px; text-align:center; border:1px solid #333; ">&nbsp;</td>
	</tr>	
    <? } ?>
</table>
<!-- table row end-->
</td>
</tr>
</tbody>
</table>
<div style="page-break-after: always;"></div>
<!-----------End ------------->
<? } ?>
</body>
</html>