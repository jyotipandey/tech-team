<?php
include($DOCUMENT_ROOT . "/session-require.php");
include($DOCUMENT_ROOT . "/database.php");
include($DOCUMENT_ROOT . "/functions.php");
include($DOCUMENT_ROOT . "/show-manage/show-functions.php");
include($DOCUMENT_ROOT . "/arrays.php");


//Get user level Details END
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php
echo "$show_name";
?>
</title>
<style type="text/css">
.redbg {
	background-color: #F60;
}
table {
	padding-top: 5px;
	padding-top: 5px;
}
.td1 {
	padding-top: 0px;
	padding-bottom: 10px;
	padding-left: 20px;
	padding-right: 10px;
}
</style>
</head>

<body>
<?php
$class_num = 1;
$selGroup  = mysql_query("SELECT DISTINCT group_id, group_code FROM show_dog_show WHERE show_id='$show_id' ORDER BY group_id");
if (!$selGroup) {
    die(mysql_error());
}
while ($rowGroup = mysql_fetch_array($selGroup)) {
    $group_code = $rowGroup["group_code"];
    $group_id   = $rowGroup["group_id"];
    
    $rowGroupName = getSingleRow("group_name", "show_group", "group_id = '$group_id'");
    $group_name   = $rowGroupName["group_name"];
?>
<?php
    // Get Group Breeds
    $selBreed = mysql_query("SELECT DISTINCT dog_breed FROM show_dog_show WHERE group_id = '$group_id' AND show_id = '$show_id' ORDER BY dog_breed_name");
    if (!$selBreed) {
        die(mysql_error());
    }
    while ($rowBreed = mysql_fetch_array($selBreed)) {
        $dog_breed    = $rowBreed["dog_breed"];
        $rowGroupName = getSingleRow("breed_name", "show_breed", "breed_id = '$dog_breed'");
       $breed_name   = $rowGroupName["breed_name"];
        // Get Dog and User info
        
         $checkCode='';
        $selClass = mysql_query("SELECT DISTINCT c.class_code,class_id FROM show_dog_show AS s, show_dog_class AS c WHERE s.dog_breed = '$dog_breed' AND s.dog_id=c.dog_id AND s.show_id=c.show_id AND c.show_id='$show_id' AND s.show_id='$show_id' ORDER BY c.class_code");
        while ($showDog_id = mysql_fetch_array($selClass)) {
             $class_id   = $showDog_id['class_id'];
             $class_code = $showDog_id['class_code'];
             $breedName  = query_execute_row("SELECT class_name FROM show_class WHERE class_id='$class_id'");
			 $class_name=$breedName['class_name'];
			 $checkMale=array('A','B','C','D','E','F');
			 $checkFemale=array('G','H','I','J','K','L');
			 $checkChampion=array('M');
			 $checkCode[]=$class_code;
			 foreach($checkCode as $code){ 
			 if(in_array($code,$checkMale))
			 {
				$temp1=$code; 
			 }elseif(in_array($code,$checkFemale))
			 {
				$temp2=$code; 
			 }elseif($code=='M')
			 {
				 $temp3='M';
			 }
			 }
		}
		?>
<?
		     $count_dog=count($checkCode);
			 foreach($checkCode as $classcode){
			 $selExhabit= mysql_query("SELECT s.exhibit_num FROM show_dog_show AS s, show_dog_class AS c WHERE c.class_code='$classcode' AND s.show_id='$show_id' AND s.show_id=c.show_id AND s.dog_id=c.dog_id AND s.dog_breed = '$dog_breed' ORDER BY s.exhibit_num ASC");
	if (!$selExhabit){ 	die(mysql_error()); 	}
	$totrecord = mysql_num_rows($selExhabit);
  	$exhibit_num='';
	?>
<table style="font-size:16px; background:#f8f8f8; padding-top:5px;" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tbody>
  
    <tr>
  
    <td style="" valign="top" align="center">
  
    <div width="624" align="center" style="width:624px;  ">
  
  <table width="624" align="center" border="0" cellpadding="0" cellspacing="0" style="font-size:16px; font-family:sans-serif; ">
      <tbody>
    
      <tr>
    
      <td>
    
    <table width="624" align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0"  style=" ">
        <tbody>
      
        <tr>
      
        <td style="font-size:14px;color:#000;font-weight:normal;  text-align:left;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top; ">
      
      <table width="100%"  border="0" cellpadding="0" cellspacing="1" style="padding-left:5px; padding-right:5px;    border-collapse:collapse;
" >
        <tbody>
          <tr>
            <td  align="left" style="padding-left:5px; padding-right:5px; border-bottom:2px solid #333; padding-bottom:10px; width:300px;">
            <div style="width:100%; float:left; margin-bottom:5px;">
                <div style="float:left; width:70px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:200px; margin-left:5px; margin-top: -7px;">
                  <?=$classcode?>
                </div>
              </div>
              <div style="width:100%; float:left; margin-bottom:5px;">
                <div style="float:left; width:50px;">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:220px; margin-left:5px; margin-top: -7px;">
                  <?=$breed_name?>
                </div>
              </div>
              <div style="width:100%; float:left;">
                <div style="float:left; width:82px;">No. of Prizes</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: -7px;">&nbsp;</div>
              </div>
              </td>
            
            <td align="left" 
                      style="border-right:2px solid #000; border-left:2px solid #000; padding-left:5px; padding-right:5px; border-bottom:2px solid #333; padding-bottom:10px; width:144px;"><div style="font-size:15px; margin-top:5px;"><strong>K.C.I <br />
                Office Slip</strong></div>
                
              <div style="width:100%; float:left;">
                <div style="float:left; width:64px;">Class No.</div>
                <div style="border-bottom:1px dotted #333; float:left; width: 70px; margin-left:5px; margin-top: -7px;">
                  <?=$classcode?>
                </div>
              </div>
              <div style="width:100%; float:left;">
                <div style="float:left; width:50px;">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:80px; margin-left:5px; margin-top: -7px;">
                  <?=$breed_name?>
                </div>
              </div>
              </td>
              
            <td   align="left"  style="padding-left:5px; padding-right:5px; border-bottom:2px solid #333; padding-bottom:10px; width:180px;"><div style="font-size:15px; margin-top: 5px;"> <strong>Slip for <br />
                Award Board </strong></div>
              <div  style="width:100%; float:left;">
                <div  style="float:left; width:65px;">Class No. </div>
                <div  style="border-bottom:1px dotted #333; float:left; width:110px; margin-left:5px; margin-top: -7px;">
                  <?=$classcode?>
                </div>
              </div>
              
              <div style="width:100%; float:left;"> 
                           <div  style="float:left; width:50px;">BREED </div>
              <div style="border-bottom:1px dotted #333; float:left; width:120px; margin-left:5px; margin-top: -7px;">
                <?=$breed_name?>
              </div>
          </div>
        
          </td>
        
          </tr>
        
          </tbody>
      </table>
      <table width="100%"  border="0" cellpadding="0" cellspacing="1" style="padding-left:5px; padding-right:5px;    border-collapse:collapse;
" >
        <tbody>
          <tr>
            <td  align="left" style="padding-left:5px; padding-right:5px; border-bottom:2px solid #333; padding-bottom:10px; width:300px;">
            <div style="width:100%; float:left; margin-bottom:5px;">
                <div style="float:left; width:70px;">Class No. </div>
                <div style="border-bottom:1px dotted #333; float:left; width:200px; margin-left:5px; margin-top: -7px;">
                  <?=$classcode?>
                </div>
              </div>
              <div style="width:100%; float:left; margin-bottom:5px;">
                <div style="float:left; width:50px;">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:220px; margin-left:5px; margin-top: -7px;">
                  <?=$breed_name?>
                </div>
              </div>
              <div style="width:100%; float:left;">
                <div style="float:left; width:82px;">No. of Prizes</div>
                <div style="border-bottom:1px dotted #333; float:left; width:188px; margin-left:5px; margin-top: -7px;">&nbsp;</div>
              </div>
              </td>
            
            <td align="left" 
                      style="border-right:2px solid #000; border-left:2px solid #000; padding-left:5px; padding-right:5px; border-bottom:2px solid #333; padding-bottom:10px; width:144px;"><div style="font-size:15px; margin-top:5px;"><strong>K.C.I <br />
                Office Slip</strong></div>
                
              <div style="width:100%; float:left;">
                <div style="float:left; width:64px;">Class No.</div>
                <div style="border-bottom:1px dotted #333; float:left; width: 70px; margin-left:5px; margin-top: -7px;">
                  <?=$classcode?>
                </div>
              </div>
              <div style="width:100%; float:left;">
                <div style="float:left; width:50px;">BREED </div>
                <div style="border-bottom:1px dotted #333; float:left; width:80px; margin-left:5px; margin-top: -7px;">
                  <?=$breed_name?>
                </div>
              </div>
              </td>
              
            <td   align="left"  style="padding-left:5px; padding-right:5px; border-bottom:2px solid #333; padding-bottom:10px; width:180px;"><div style="font-size:15px; margin-top: 5px;"> <strong>Slip for <br />
                Award Board </strong></div>
              <div  style="width:100%; float:left;">
                <div  style="float:left; width:65px;">Class No. </div>
                <div  style="border-bottom:1px dotted #333; float:left; width:110px; margin-left:5px; margin-top: -7px;">
                  <?=$classcode?>
                </div>
              </div>
              
              <div style="width:100%; float:left;"> 
                           <div  style="float:left; width:50px;">BREED </div>
              <div style="border-bottom:1px dotted #333; float:left; width:120px; margin-left:5px; margin-top: -7px;">
                <?=$breed_name?>
              </div>
          </div>
        
          </td>
        
          </tr>
        
          </tbody>
      </table>
      <!-- table row start-->
          <table  align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0"  style="border-collapse:collapse; width:624px;">
        <tbody>
      <tr>
        
                <td  align="left"  style="border-bottom:2px solid #333; width:300px;">
                <table style=" text-align:center;  border-collapse:collapse; width:300px;" >
                    <tr>
                      <td width="80" >Name</td>
                      <td width="60">Prize</td>
                      <td width="160">NOTES</td>
                    </tr>
                  </table></td>
                  
                  
                <td  align="left"  style="border-right:2px solid #000; border-left:2px solid #000;  border-bottom:2px solid #333; width:144px;"><table style=" text-align:center; width:144px;" >
                    <tr>
                      <td width="60">Number</td>
                      <td width="84"> Prize</td>
                    </tr>
                  </table></td>
                <td   align="left"  style=" width:180px; border-bottom:2px solid #333;"><table style=" text-align:center; width:180px;" >
                    <tr>
                      <td width="90">Number</td>
                      <td width="90">Prize</td>
                    </tr>
                  </table></td>
              </tr>
              </tbody></table> 
                <!-- blank comum start-->
                <?
	while($rowExhabit = mysql_fetch_array($selExhabit)){
		$ex=$rowExhabit['exhibit_num'];
		?>
        <table width="624" align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0"  style=" border-collapse: ">
        <tbody>
      <tr>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" ><?=$ex?></td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100"><?=$ex?></td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100"><?=$ex?></td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <?
		
	
	}
		?>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >&nbsp;</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >&nbsp;</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >&nbsp;</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <?
		if($count_dog>1 && ($classcode==$temp1 || $classcode==$temp2)){ ?>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >CC</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >RCC</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <? }
		if($count_dog>1 && ($classcode==$temp3)){ ?>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>

                      <td width="80" >BOB</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >RBOB</td>
                      <td width="60">&nbsp</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <? }
		 if($count_dog==1){ ?>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >BOB</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <? }
		 
		?>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >&nbsp;</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td  align="left" width="285" ><table style=" text-align:center; border-collapse:collapse;" width="285" >
                    <tr>
                      <td width="80" >&nbsp;</td>
                      <td width="60">&nbsp;</td>
                      <td width="160">&nbsp;</td>
                    </tr>
                  </table></td>
                <td  align="left" width="183" style="border-right:2px solid #000; border-left:2px solid #000; "><table style=" text-align:center; " width="185">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
                <td   align="left" width="214" ><table style=" text-align:center;" width="216">
                    <tr>
                      <td width="100">&nbsp;</td>
                      <td width="100">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <?		
			 
        }
    }
}
?>
            </tbody>
          </table></td>
      </tr>
      <!-- end-->
      </tbody>
          </table>
  
      <!-- table row end-->
 
      
        </td>
      
        </tr>
      
       
      
      
        </tbody>
      
    </table>
    <!--list -->
    
      </td>
    
      </tr>
    
      </tbody>
    
  </table>
    </div>
  
    </td>
  
    </tr>
  
    </tbody>
  
</table>
</body>
</html>