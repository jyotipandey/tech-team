<?
include($DOCUMENT_ROOT."/session-require.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
require_once($DOCUMENT_ROOT."/arrays.php");
include($DOCUMENT_ROOT."/common/countries.php");
?>
<?
if($submit){
$qDogOwner=query_execute("SELECT dogowner_id, dog_id FROM show_dog_show WHERE show_id = '$show_id' AND exhibit_num > $ex_no ORDER BY exhibit_num ASC");
$totrecord = mysql_num_rows($qDogOwner);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Late Entries</title>
<link href="/css/common.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/jquery/jquery-latest.js"></script> 
<script type="text/javascript" src="/jquery/jquery.tablesorter.js"></script> 

<script type="text/javascript">
$(document).ready(function() { 
        $("#myTable").tablesorter({
			sortList: [[2,0]] 
		}); 
    } 
); 
</script>
<style type="text/css">
table.tablesorter {
	font-family:arial;
	background-color: #CDCDCD;
	margin:10px 0pt 15px;
	font-size: 10pt;
	width: 100%;
	text-align: left;
}
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
	background-color: #e6EEEE;
	border: 1px solid #FFF;
	font-size: 8pt;
	padding: 4px;
}
table.tablesorter thead tr .header {
	background-image: url(bg.gif);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter tbody td {
	color: #3D3D3D;
	padding: 4px;
	background-color: #FFF;
	vertical-align: top;
}
table.tablesorter tbody tr.odd td {
	background-color:#F0F0F6;
}
table.tablesorter thead tr .headerSortUp {
	background-image: url(asc.gif);
}
table.tablesorter thead tr .headerSortDown {
	background-image: url(desc.gif);
}
table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
background-color: #8dbdd8;
}
</style>
<link href="/show-manage/css/cb.css" rel="stylesheet" type="text/css" />
<link href="/show-manage/css/cb-top-navi.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT.'/common/top.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/show-manage/common/top.php'); ?>
<? $sqlsho=query_execute_row("SELECT show_name FROM show_description WHERE show_id='$show_id'");
$show_name=$sqlsho['show_name'];?>
<h1><? echo $show_name;?></h1>
<h3>Late Entries.</h3>
<? if($ex_no!=''){?>
<h2><a href="/show-manage/admin/catalogue-late-entry.php?show_id=<?=$show_id?>&ex_no=<?=$ex_no?>">View Catalogue</a></h2>
<? }?>
<form method="POST" action="">
<table width="100%" cellspacing="0" cellpadding="2" style="text-align:center; border-color:#999; border-width:1px; border-style:solid; background-color:#edeaea">
  <tr>
  <td>
  <label>Select the exibit no from you want to get the entries</label>
  <select name="ex_no" id="ex_no">
    <option>-----SELECT-----</option>
    <? $sqlgetexno=query_execute("SELECT exhibit_num	FROM show_dog_show WHERE show_id='$show_id' ORDER BY exhibit_num ASC");
	while($getdoresult=mysql_fetch_array($sqlgetexno)){
		$ex_no=$getdoresult['exhibit_num']?>
    <option value="<?=$ex_no?>" <? if($ex_no=='$ex_no'){echo 'selected=selected';}?>><?=$ex_no;?></option>
    <? }?>  
  </select>
</td> 
 </tr> 
 </tr>
</table>

<div style="text-align:center; margin-top:5px;"><input type="submit" name="submit" id="submit" value="submit" /></div>
<div style=" margin-top:5px;">
  <table border="1" align="center" cellpadding="3" cellspacing="0" class="tablesorter" id="myTable" >
    <thead>
      <tr style="width:500px">
        <th>Sr.No</th>
        <th>Exhibit Num</th>
        <th>Owner Name</th> 
        <th>Dog Name</th>        
        <th>No. of Dogs</th>
        <th>Breed</th>
        <th>Status</th>        
      </tr>
    </thead>
  <tbody>
<? 
if($totrecord>0){
$s=1;
while($rowDogOwner = mysql_fetch_array($qDogOwner)){
	$dogowner_id=$rowDogOwner['dogowner_id'];
	$dogidg=$rowDogOwner['dog_id'];	
$sql="SELECT owner_name,address,street_address,city_name,state_name,zip,mobile_main FROM show_dogowner WHERE dogowner_id = '$dogowner_id'";
$qGetMyOrders=query_execute("$sql");
$max = mysql_num_rows($qGetMyOrders);
$rowMyOrders = mysql_fetch_array($qGetMyOrders);
	$exhibit=query_execute("SELECT * FROM show_dog_show WHERE dogowner_id = '$dogowner_id' AND show_id = $show_id");
	$breedname=query_execute("SELECT dog_breed_name FROM show_dog_show WHERE dogowner_id = '$dogowner_id' AND show_id=$show_id");
	?>    
    <tr style="width:400px">
      <td><?= $s ?></td>
      <td>
	  <? while($rowexhibit = mysql_fetch_array($exhibit)){	
	$exhibit_num=$rowexhibit['exhibit_num'];
	echo $exhibit_num.',';
	}?></td>
     
      <td><?=$rowMyOrders["owner_name"]."</br>".$rowMyOrders["address"]." ".$rowMyOrders["street_address"].",".$rowMyOrders["city_name"]."-".$rowMyOrders["zip"].",".$rowMyOrders["state_name"]."</br> Mob. No:".$rowMyOrders["mobile_main"]?></td>
      <?
	$rowcount=query_execute_row("SELECT count(dog_id) as dog FROM show_dog_show WHERE dogowner_id = '$dogowner_id' AND show_id=$show_id GROUP BY dog_id");
	$rowcodog=query_execute_row("SELECT dog_name FROM show_dog WHERE dog_id = '$dogidg'");
	?>
    <td><?=$rowcodog['dog_name']?></td>
      <td><?=$rowcount['dog']?></td>
     <td>
	  <? while($rowbreed = mysql_fetch_array($breedname)){	
	$breedname1=$rowbreed['dog_breed_name'];
	echo $breedname1.',';
	}?></td>
  
  <?
	$rowpaid=query_execute_row("SELECT paid FROM show_dog_show WHERE dogowner_id = '$dogowner_id' AND show_id=$show_id");
	$c=$rowpaid['paid'];
	?>
      <td><? if($c=='1'){ echo "Paid"; } else { echo "Unpaid"; }?></td>
      </tr>
  <?
$s++;

 }}?>
  </tbody>
  </table>
  </form>
</div> 
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>