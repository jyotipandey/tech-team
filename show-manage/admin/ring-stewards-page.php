<?php
include($DOCUMENT_ROOT."/session-require.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/show-manage/show-functions.php");
include($DOCUMENT_ROOT."/arrays.php");
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ring Steward Page</title>
</head>

<body>
<h2 align="center"><?php echo"$show_name"." AWARDS"; ?></h2>
<?
// Check valud user END	--------------------------------------------------------------------

// Get Show Details
$selectArt = mysql_query ("SELECT * FROM show_description WHERE show_id = '$show_id'");
  if(!$selectArt){   die(mysql_error());   }	
  $rowArt = mysql_fetch_array($selectArt);
	$show_name = $rowArt["show_name"];
	$entry = $rowArt["entry"];
// Get Show Details END

//Get user level Details
$selectUser = mysql_query ("SELECT * FROM show_user WHERE show_id = '$show_id' AND userid = '$userid'");
  if(!$selectUser){   die(mysql_error());   }	
  $rowUser = mysql_fetch_array($selectUser);
	$show_user_level = $rowUser["show_user_level"];
//Get user level Details END
?>

<div align="center">
<table width="95%" border="1" cellspacing="0" cellpadding="1" style="font-size:14px; text-align:center; text-transform: uppercase" >
  <tr>
    <td width="6%"><strong>S.No</strong></td>
    <td width="17%" style="text-align:left;"><strong>Breed</strong></td>
    
    <td width="9%"><strong>CC Dog</strong></td>
    <td width="9%"><strong>RCC Dog</strong></td>
    <td width="9%"><strong>Puppy Dog</strong></td>
    <td width="9%"><strong>CC Bitch</strong></td>
    <td width="9%"><strong>RCC Bitch</strong></td>
    <td width="9%"><strong>Puppy Bitch</strong></td>
    <td width="9%"><strong>BOB</strong></td>
    <td width="9%"><strong>RBOB</strong></td>
    </tr>
<?php
// all Groups
$s=1;
$selGroup= mysql_query("SELECT DISTINCT group_id, group_code FROM show_dog_show WHERE show_id = '$show_id' AND paid = '1' ORDER BY group_id");
 if (!$selGroup){ 	die(mysql_error()); 	} $d=0;
  while($rowGroup = mysql_fetch_array($selGroup)){
	$group_code = $rowGroup["group_code"];
	$group_id = $rowGroup["group_id"];
	$rowGroupName = getSingleRow("group_name", "show_group", "group_id = '$group_id'");
	$group_name = strtoupper($rowGroupName["group_name"]);
	$s+1;
?>  <tr>
    <td width="7%"><strong></strong></td>
    <td width="17%" style="text-align:left;"><strong><?=$group_name?></strong></td>
  
    <td width="9%"><strong></strong></td>
    <td width="9%"><strong></strong></td>
    <td width="9%"><strong></strong></td>
    <td width="9%"><strong></strong></td>
    <td width="9%"><strong></strong></td>
    <td width="9%"><strong></strong></td>
    <td width="9%"><strong></strong></td>
    <td width="9%"><strong></strong></td>	
    </tr>
</div>
<?php 
// Get Group Breeds
$selBreed= mysql_query("SELECT DISTINCT dog_breed, dog_breed_name FROM show_dog_show WHERE group_id = '$group_id' AND show_id = '$show_id' AND paid = '1' ORDER BY dog_breed_name");
 if(!$selBreed){ 	die(mysql_error()); 	}
while($rowBreed = mysql_fetch_array($selBreed)){
 $dog_breed = $rowBreed["dog_breed"];
 $dog_breed_name = strtoupper($rowBreed["dog_breed_name"]);

$selClass= mysql_query("SELECT DISTINCT c.dog_id FROM show_dog_class as c, show_dog_show as d WHERE d.dog_breed = '$dog_breed' AND d.dog_id=c.dog_id AND d.paid = '1' AND d.show_id = '$show_id' AND c.show_id = '$show_id' ORDER BY c.class_code, c.dog_id");
  if(!$selClass){ 	die(mysql_error()); 	}
  $num_rows = mysql_num_rows($selClass);
  $total=$total+$num_rows;
  //echo"<li><b>$dog_breed_name - $num_rows</b></li>"; 
?> 
  <tr>
    <td><?= $s ?></td>
    <td style="text-align:left;"><? echo "$dog_breed_name"?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td></tr>
<?php $s=$s+1;}// Get User Details?>
<? }?>
</table>
<br/>
<br/>
<h2 align="center"><?php echo"$show_name"." Best In Group (First Ring)"; ?></h2>
<table width="95%" border="1" cellspacing="0" cellpadding="1" style="font-size:14px; text-align:center; text-transform: uppercase" >
  <tr>
    <td width="5%"><strong>S.No</strong></td>
    <td width="18%" style="text-align:left;"><strong>Best In Group</strong></td>
    <td width="11%"><strong>1st</strong></td>
    <td width="11%"><strong>2nd</strong></td>
    <td width="11%"><strong>3rd</strong></td>
    <td width="11%"><strong>Best Puppy In Group</strong></td>
    </tr>
  <tr>
    <td>1</td>
    <td style="text-align:left;">Toy Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    <td>2</td>
    <td style="text-align:left;">Terrier Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    <td>3</td>
    <td style="text-align:left;">Hound Group</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>4</td>
    <td style="text-align:left;">Utility Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    <td>5</td>
    <td style="text-align:left;">Gundog Group</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>6</td>
    <td style="text-align:left;">working Group</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>7</td>
    <td style="text-align:left;">Patoral Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    </table>
 </br>
 </br>
 <h2 align="center"><?php echo"$show_name"." Best In Group (Second Ring)"; ?></h2>
<table width="95%" border="1" cellspacing="0" cellpadding="1" style="font-size:14px; text-align:center; text-transform: uppercase" >
  <tr>
    <td width="5%"><strong>S.No</strong></td>
    <td width="18%" style="text-align:left;"><strong>Best In Group</strong></td>
    <td width="11%"><strong>1st</strong></td>
    <td width="11%"><strong>2nd</strong></td>
    <td width="11%"><strong>3rd</strong></td>
    <td width="11%"><strong>Best Puppy In Group</strong></td>
    </tr>
  <tr>
    <td>1</td>
    <td style="text-align:left;">Toy Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    <td>2</td>
    <td style="text-align:left;">Terrier Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    <td>3</td>
    <td style="text-align:left;">Hound Group</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>4</td>
    <td style="text-align:left;">Utility Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    <td>5</td>
    <td style="text-align:left;">Gundog Group</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>6</td>
    <td style="text-align:left;">working Group</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>7</td>
    <td style="text-align:left;">Patoral Group</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td></tr>
      <tr>
    </table>
 </br>
 </br>  
 <h2 align="center"><?php echo"$show_name"." Line Up (First Ring)"; ?></h2> 
    <table width="95%" border="1" cellspacing="0" cellpadding="1" style="font-size:14px; text-align:center; text-transform: uppercase" >
  <tr>
    <td width="4%"><strong>S.No</strong></td>
    <td width="14%" style="text-align:left;"><strong>Line Up</strong></td>
    <td width="11%"><strong>Exbit No.</strong></td>
    <td width="11%"><strong>Breed</strong></td>
    <td width="15%"><strong>Owner</strong></td>
    </tr>
  <tr>
    <td>1</td>
    <td style="text-align:left;">BIS</td>
    <td></td>
    <td></td>
    <td></td>
</tr>
      <tr>
    <td>2</td>
    <td style="text-align:left;">2nd BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>3</td>
    <td style="text-align:left;">3rd BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>4</td>
    <td style="text-align:left;">4th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>5</td>
    <td style="text-align:left;">5th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>6</td>
    <td style="text-align:left;">6th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>7</td>
    <td style="text-align:left;">7th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>8</td>
    <td style="text-align:left;">8th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>9</td>
    <td style="text-align:left;">BBI</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>10</td>
    <td style="text-align:left;">RBBI</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>11</td>
    <td style="text-align:left;">BPIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>12</td>
    <td style="text-align:left;">RBPIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    </table> 
 <h2 align="center"><?php echo"$show_name"."  Line Up"." (Second Ring)"; ?></h2> 
    <table width="95%" border="1" cellspacing="0" cellpadding="1" style="font-size:14px; text-align:center; text-transform: uppercase" >
  <tr>
    <td width="4%"><strong>S.No</strong></td>
    <td width="14%" style="text-align:left;"><strong>Line Up</strong></td>
    <td width="11%"><strong>Exbit No.</strong></td>
    <td width="11%"><strong>Breed</strong></td>
    <td width="15%"><strong>Owner</strong></td>
    </tr>
  <tr>
    <td>1</td>
    <td style="text-align:left;">BIS</td>
    <td></td>
    <td></td>
    <td></td>
</tr>
      <tr>
    <td>2</td>
    <td style="text-align:left;">2nd BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>3</td>
    <td style="text-align:left;">3rd BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>4</td>
    <td style="text-align:left;">4th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>5</td>
    <td style="text-align:left;">5th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>6</td>
    <td style="text-align:left;">6th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    <td>7</td>
    <td style="text-align:left;">7th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>8</td>
    <td style="text-align:left;">8th BIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>9</td>
    <td style="text-align:left;">BBI</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>10</td>
    <td style="text-align:left;">RBBI</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>11</td>
    <td style="text-align:left;">BPIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
            <tr>
    <td>12</td>
    <td style="text-align:left;">RBPIS</td>
    <td></td>
    <td></td>
    <td></td></tr>
      <tr>
    </table>
</body>
</html>