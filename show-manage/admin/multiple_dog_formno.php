<?
include($DOCUMENT_ROOT."/session-require.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
require_once($DOCUMENT_ROOT."/arrays.php");
include($DOCUMENT_ROOT."/common/countries.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Single & Multiple Exhibit Log.</title>
<link href="/css/common.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/jquery/jquery-latest.js"></script> 
<script type="text/javascript" src="/jquery/jquery.tablesorter.js"></script> 

<script type="text/javascript">
$(document).ready(function() { 
        $("#myTable").tablesorter({
			sortList: [[2,0]] 
		}); 
    } 
); 
</script>
<style type="text/css">
table.tablesorter {
	font-family:arial;
	background-color: #CDCDCD;
	margin:10px 0pt 15px;
	font-size: 10pt;
	width: 100%;
	text-align: left;
}
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
	background-color: #e6EEEE;
	border: 1px solid #FFF;
	font-size: 8pt;
	padding: 4px;
}
table.tablesorter thead tr .header {
	background-image: url(bg.gif);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter tbody td {
	color: #3D3D3D;
	padding: 4px;
	background-color: #FFF;
	vertical-align: top;
}
table.tablesorter tbody tr.odd td {
	background-color:#F0F0F6;
}
table.tablesorter thead tr .headerSortUp {
	background-image: url(asc.gif);
}
table.tablesorter thead tr .headerSortDown {
	background-image: url(desc.gif);
}
table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
background-color: #8dbdd8;
}
</style>

<h1><? echo $show_name;?></h1>
<h2>Single & Multiple Exhibit Log.</h2>
<div style=" margin-top:5px;">
  <table border="1" align="center" cellpadding="3" cellspacing="0" class="tablesorter" id="myTable" >
    <thead>
      <tr style="width:500px">
        <th>Sr.No</th>
        <th>Form Num</th>
        <th>Owner Name</th>
        <th>Mobile</th>        
        <th>No. of Dogs</th>
        <th>Dog Name</th>
        <th>Breed</th>
        <th>Paid/UnPaid</th> 
        
      </tr>
    </thead>
  <tbody>
<? 
$s=1;
$sqlDogOwner="SELECT dogowner_id, form_no FROM show_dog_show WHERE show_id = '$show_id' GROUP BY form_no ORDER BY form_no ASC";
$qDogOwner=query_execute("$sqlDogOwner");
while($rowDogOwner = mysql_fetch_array($qDogOwner)){
	$dogowner_id=$rowDogOwner['dogowner_id'];
	$form_no=$rowDogOwner['form_no'];
	
$sql="SELECT owner_name,address,street_address,city_name,state_name,zip,mobile_main FROM show_dogowner WHERE dogowner_id = '$dogowner_id'";
$qGetMyOrders=query_execute("$sql");
$max = mysql_num_rows($qGetMyOrders);
  

$rowMyOrders = mysql_fetch_array($qGetMyOrders);

	$exhibit=query_execute("SELECT * FROM show_dog_show WHERE dogowner_id = '$dogowner_id' AND show_id = $show_id");
	$breedname=query_execute("SELECT dog_breed_name, dog_id, exhibit_num FROM show_dog_show WHERE form_no = '$form_no' AND show_id=$show_id");
	$breednam11e=query_execute("SELECT dog_breed_name, dog_id FROM show_dog_show WHERE form_no = '$form_no' AND show_id=$show_id");
	?>    
    <tr style="width:400px">
      <td><?= $s ?></td>
      <td>
	  <? while($rowexhibit = mysql_fetch_array($exhibit)){	
	$exhibit_num=$rowexhibit['form_no'];
	echo $exhibit_num.',';
	}?></td>
     
      <td><?=$rowMyOrders["owner_name"]."</br>".$rowMyOrders["address"]." ".$rowMyOrders["street_address"].",".$rowMyOrders["city_name"]."-".$rowMyOrders["zip"].",".$rowMyOrders["state_name"]."</br> Mob. No:".$rowMyOrders["mobile_main"]?></td>
      <?
	$rowcount=query_execute_row("SELECT count(dog_id) as dog FROM show_dog_show WHERE form_no = '$form_no' AND show_id='$show_id'");
	?>
    <td><?=$rowMyOrders["mobile_main"]?></td>
      <td><?=$rowcount['dog']?></td>
     <td>
	  <? while($rowbreed = mysql_fetch_array($breedname)){	
	$breedname2=$rowbreed['dog_id'];
	$sqlddname=query_execute_row("SELECT dog_name FROM show_dog WHERE dog_id='$breedname2'");
	echo $sqlddname['dog_name'].', '."Ex No:".$rowbreed['exhibit_num']."<br>";
	}?></td>
         <td>
	  <? while($rowbreed12 = mysql_fetch_array($breednam11e)){	
	$breedname1=$rowbreed12['dog_breed_name'];
	echo $breedname1.'<br>';
	}?></td>
  
  <?
	$rowpaid=query_execute_row("SELECT paid FROM show_dog_show WHERE dogowner_id = '$dogowner_id' AND show_id=$show_id");
	$c=$rowpaid['paid'];
	?>
      <td><? if($c=='1'){ echo "Paid"; } else { echo "Unpaid"; }?></td>
      </tr>
  <?
$s++;

 }?>
  </tbody>
  </table>
</div>
