<?
include($DOCUMENT_ROOT."/session.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/functions.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Show Management: Free to use Catalog Builder</title>
<?php require_once($DOCUMENT_ROOT.'/common/top.php'); ?>
<link href="css/cb.css" rel="stylesheet" type="text/css" />
<div id="pagebody">
<div id="greedBox">
<h1>Free to use Catalog Builder</h1>
<ul>
  <li><strong>Full catalog sorting as per KCI Rules</strong></li>
  <li><strong>No Loss of data back up online</strong></li>
  <li><strong>Different Catalog Formats for printing</strong></li>
</ul>
<h1>Your club do not have a account yet? </h1><br />
<p><strong><a href="cb-apply.php" class="AlinkButton" style=" margin-left:200px;">Apply Now</a></strong></p>
<div id="clearall"></div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>