<?
require_once($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");
require_once($DOCUMENT_ROOT.'/common/countries.php');

//Get Dog Show Details
//echo $kci_num ."-". $show_id;
$selectArt = mysql_query ("SELECT * FROM show_description WHERE show_id = '$show_id'");
  if (!$selectArt){	die(mysql_error());  }	
			  
	$rowArt = mysql_fetch_array($selectArt);
	$show_name = $rowArt["show_name"];
	$type_id = $rowArt["type_id"];
//Get Dog Show Details END 

if($kci_num){
	$selDog = mysql_query("SELECT * FROM show_dog WHERE kci_num LIKE '%$kci_num%'");
}elseif($dog_id_e){
	$selDog = mysql_query("SELECT * FROM show_dog WHERE dog_id = '$dog_id_e'");
}

if($kci_num || $dog_id_e){
  if (!$selDog){  die(mysql_error()); 	}
  $rowDog = mysql_fetch_array($selDog);
    $dog_id = $rowDog["dog_id"];
	$dog_breed = $rowDog["dog_breed"];
	$dog_owner = stripslashes($rowDog["dog_owner"]);
	
	$microchip = $rowDog['microchip'];
	$exhibit_num = $rowDog['exhibit_num'];
	$address = stripslashes($rowDog["address"]);
	
	$city = $rowDog["city"];
	$phone = $rowDog["phone"];
	$name_title = $rowDog["name_title"];
	$dog_name = stripslashes($rowDog["dog_name"]);
	
	$dog_sex = $rowDog["dog_sex"];
	$dog_color = stripslashes($rowDog["dog_color"]);
	
	$sire = $rowDog["sire"];
	$dam = $rowDog["dam"];
	$breeder = stripslashes($rowDog["breeder"]);
	
	$dob = $rowDog["dob"];
	
	list($year, $month, $day) = split("-", $dob);

$selCls = mysql_query ("SELECT * FROM show_dog_class WHERE dog_id = '$dog_id' AND show_id = '$show_id'");
  if (!$selCls){	die(mysql_error());  }	
			  
  while($rowCls = mysql_fetch_array($selCls)){
	$dogCls_id = $rowCls["class_id"];
	$dogCls = $rowCls["class_code"];
	
	$dogCls_idArr[] = $dogCls_id;
	//echo" tapeshwar". $dog_id."-". $dogCls_id."-".$dogCls."<br>";
  }

}else{
	$dog_breed = "";
	$dog_owner = "";
	$microchip = "";
	$exhibit_num = "";
	$address = "";
	$city = "";
	$phone = "";
	$name_title = "";
	$dog_name = "";
	$dog_sex = "";
	$dog_color = "";
	$sire = "";
	$dam = "";
	$breeder = "";
	$year = ""; 
	$month = ""; 
	$day = "";
	
	$dogCls_idArr = "";
}	

//print_r($dogCls_idArr);
?>

<table width="98%" border="0" cellpadding="2" cellspacing="0"> 
    <tr class="dtd">
      <td width="110" align="right"> Name of Dog:<span id="redsmall"> *</span></td>
      <td>&nbsp;</td>
      <td><select name="name_title" id="name_title" style="width:60px;">
            <option value="0"> Title </option>
            <option value="CH." <? echo ($name_title == "CH." ? "selected='selected'" : ""); ?>>Champion</option>
            <option value="" >Not Champion</option>
		  </select>
          <input name="dog_name" type="text" id="dog_name" value="<? echo $dog_name;?>" style="width:200px;"/>
     </td>
    </tr>
     <tr class="dtd">
      <td align="right">Sex:<span id="redsmall"> *</span></td>
      <td>&nbsp;</td>
      <td><select name="dog_sex" id="dog_sex">
        <option>--Select--</option>
	    <?
        foreach($DogSex2 as $sexCode => $sexName){
         echo"<option value='$sexCode'"; if($sexCode == $dog_sex) echo"selected='selected'"; echo">$sexName</option>";
        }
	   ?>
      </select>
      </td>
    </tr>
    <tr class="dtd">
          <td align="right">Microchip:</td>
          <td>&nbsp;</td>
  <td><input name="microchip" type="text" id="microchip" value="<? echo"$microchip";?>" size="20" /></td>
        </tr>
    <tr class="dtd">
      <td align="right">Breed:<span id="redsmall"> *</span></td>
      <td>&nbsp;</td>
      <td><select name="dog_breed" id="dog_breed" onchange="chkBreed()">
      <option value=""> ------ Select dog breed ------- </option>
        <?php
          $select_breed = mysql_query ("SELECT * FROM show_breed WHERE type_id = '$type_id' ORDER BY breed_name");
            if (!$select_breed){   die(mysql_error());	}
                
            while($row1 = mysql_fetch_array($select_breed)){
             $breed_id = $row1["breed_id"];
             $group_id = $row1["group_id"];
             $group_code = $row1["group_code"];
             $breed_name = $row1["breed_name"];
         ?>
         <option value="<? echo $breed_id.";".$group_id.";".$group_code.";".$breed_name;?>" <? echo($breed_id == $dog_breed ? "selected='selected'" : "");?>><? echo $breed_name;?></option>
        <? } ?>
      </select>
     </td>
    </tr>
    <tr class="dtd">
      <td align="right">Colors and Markings:</td>
      <td>&nbsp;</td>
      <td><input name="dog_color" type="text" id="dog_color" value="<? echo"$dog_color";?>" size="45" class=" textWrite"/></td>
    </tr>
    <tr class="dtd">
      <td align="right">Name of Sire: </td>
      <td>&nbsp;</td>
      <td><input name="sire" type="text" id="sire" value="<? echo"$sire";?>" size="30" class=" textWrite"/></td>
    </tr>
    <tr class="dtd">
      <td align="right">Name of Dam:</td>
      <td>&nbsp;</td>
      <td><input name="dam" type="text" id="dam" value="<? echo"$dam";?>" size="30" class=" textWrite"/></td>
    </tr>
    <tr class="dtd">
      <td align="right">BI/Imported: </td>
      <td>&nbsp;</td>
      <td><input type="radio" name="dognationality" id="radio" value="B" />
        BI 
          <input type="radio" name="dognationality" id="radio2" value="I" />
          Imported</td>
    </tr>
    <tr class="dtd">
      <td align="right" valign="top">Date of Birth:<span id="redsmall"> *</span></td>
      <td>&nbsp;</td>
      <td><select name="day" class="formselect">
      <option>--Day--</option>
     <?
      for($di=1; $di<=31; $di++){ 
      echo"<option value=\"$di\""; if($di==$day){ echo"selected"; } echo">$di</option>";
      }
     ?>
      </select>
      &nbsp;
    <select name="month" class="formselect">
    <option>--Month--</option>
      <?
        foreach($months as $mcode => $mname){
        echo"<option value=\"$mcode\""; if($mcode==$month){ echo"selected"; } echo">$mname</option>";
        } 
      ?>
   </select>
  &nbsp;
   <select name="year" class="formselect" >
    <option>--Year--</option>
    <?
      for($yi=1991;$yi<=2010;$yi++){ 
      echo"<option value=\"$yi\""; if($yi==$year){ echo"selected"; } echo">$yi</option>";
      }
    ?>
  </select>   </td>
 </tr>
    <tr class="dtd">
      <td align="right">Name of Breeder: </td>
      <td>&nbsp;</td>
      <td><input name="breeder" type="text" id="breeder" value="<? echo $breeder;?>" size="30" class=" textWrite"/></td>
    </tr>
    
    <tr class="dtd">
      <td align="right">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    
    

    
    <tr class="dtd">
      <td align="right">Dog Class: <span id="redsmall">*</span></td>
      <td>&nbsp;</td>
      <td>
	  <select name="dog_class[]" id="dog_class[]" multiple="multiple" size="5" class=" textWrite">
        <option value=""> -- Select Dog Class -- </option>
		<?
          $sql_dog_class = mysql_query ("SELECT * FROM show_class WHERE type_id = '$type_id' ORDER BY class_code");
            if (!$sql_dog_class){   die(mysql_error());	}
            while($rowClass = mysql_fetch_array($sql_dog_class)){ 
		?>
        <option value="<? echo $rowClass['class_id'].";".$rowClass['class_code'];?>" <? $numElmnt = count($dogCls_idArr); for($c = 0; $c < $numElmnt; $c++) echo ($rowClass['class_id'] == $dogCls_idArr[$c] ? "selected='selected'" : "");?>> <? echo $rowClass['class_code'];?></option>
       <? } ?>
      </select>
      </td>
    </tr>

</table>