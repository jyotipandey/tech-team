<?php
// get Dog Month
function dogMonth($gd,$gm,$gy){
/*
     * Usage: -- echo curage(m,d,y); -- 
     * Where 'm' is birth month, 'd' is birth day 
     * and 'y' is birth year 
    **/ 

    $r = ''; 

    /** 
     * days of each month 
    **/ 

    for($x=1 ; $x<=12 ; $x++){ 

        $dim[$x] = date('t',mktime(0,0,0,$x,1,date('Y'))); 

    } 


    /** 
     * vars for current date 
    **/ 

    $cm = date('n'); 
    $cd = date('j'); 
    $cy = date('Y'); 


    /** 
     * calculate differences 
    **/ 

    $m = $cm - $gm; 
    $d = $cd - $gd; 
    $y = $cy - $gy; 


    /** 
     * if the given day is ahead of the current day 
    **/ 

    if($d < 0) { 
      
        $td = $cd + $dim[$cm]; 
        $tm = $cm - 1; 
        $d = $td - $gd; 
        $m = $tm - $gm; 
        if(($tm - $gm) < 0) { 

            $tm += 12; 
            $ty = $cy - 1; 
            $m = $tm - $gm; 
            $y = $ty - $gy; 

        } 

    } 


    /** 
     * if the given month is ahead of the current month 
    **/ 

    if($m < 0) { 

        $tm = $cm + 12; 
        $ty = $cy - 1; 
        $m = $tm - $gm; 
        $y = $ty - $gy; 

} 


    /** 
     * the processing area 
    **/ 

    if($y < 0) { 

        die("That date is in the future."); 

    } else { 

        switch($y) { 

            case 0 : $yr .= ''; break; 
            case 1 : $yr .= $y.($m == 0 && $d == 0 ? '' : ''); break; 
            default : $yr .= $y.($m == 0 && $d == 0 ? '' : ''); 

        } 


        switch($m) { 

            case 0: $mr .= ''; break; 
            case 1: $mr .= ($y == 0 && $d == 0 ? $m.'' : ($y == 0 && $d != 0 ? $m.'' : ($y != 0 && $d == 0 ? ''.$m.'' : ''.$m.''))); break; 
            default: $mr .= ($y == 0 && $d == 0 ? $m.'' : ($y == 0 && $d != 0 ? $m.'' : ($y != 0 && $d == 0 ? ''.$m.'' : ''.$m.''))); break; 

        } 

    }
	$yy=$yr*12+$mr; 
	//$rr="$y;$mr";
    return $yy;
}// END 

function dogClassM($gd,$gm,$gy){
$dmonth= dogMonth($gd,$gm,$gy);
if($dmonth <=6){
	$Dclass="A";
}elseif($dmonth > 6 && $dmonth<=9){
	$Dclass="B";
}elseif($dmonth > 9 && $dmonth<=18){
	$Dclass="C";
}elseif($dmonth > 18 && $dmonth<=24){
	$Dclass="D";
}elseif($dmonth > 24 && $dmonth<96){
	$Dclass="F";
}elseif($dmonth >= 96){
	$Dclass="E";
}
return $Dclass;
}// End Function 

function dogClassF($gd,$gm,$gy){
$dmonth= dogMonth($gd,$gm,$gy);

if($dmonth <=6){
	$Dclass="I";
}elseif($dmonth > 6 && $dmonth<=9){
	$Dclass="J";
}elseif($dmonth > 9 && $dmonth<=18){
	$Dclass="K";
}elseif($dmonth > 18 && $dmonth<=24){
	$Dclass="L";
}elseif($dmonth > 24 && $dmonth<96){
	$Dclass="N";
}elseif($dmonth >= 96){
	$Dclass="M";
}
return $Dclass;
}// End Function 

// Get Dog Breed
function ShowDogBreed($breed_id){
$selectArt = mysql_query ("SELECT * FROM show_breed WHERE breed_id = '$breed_id'");
			  if (!$selectArt){
				die(sql_error());
			  }	
	$rowArt = mysql_fetch_array($selectArt);
	$breed_name = $rowArt["breed_name"];
	return $breed_name;
}
// Get Dog Breed END 
?>