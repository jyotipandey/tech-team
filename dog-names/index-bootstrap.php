<?php
include('../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$sitesection = "dog-names";
$mobile_browser=is_mobile2();
	 if ($mobile_browser > 0) {
		    $request=str_replace("/","",$_SERVER['REQUEST_URI']);
			//echo "SELECT id FROM mobile_section WHERE section_name='$request' AND status='active'";		
			$rowPagesec=query_execute_row("SELECT id FROM mobile_section WHERE section_name='$request' AND status='active'");
			if($rowPagesec['id'])
			{
			header('Location: https://m.dogspot.in/'.$request.'/');
			exit();	
			}
			
		  }
$maxshow = 40;
if (empty($show)) {
    $show = 0;
} else {
    $show = $show - 1;
}
 
$showRecord  = $show * $maxshow;
$nextShow    = $showRecord + $maxshow;

$dog_breed_name=query_execute_row("SELECT breed_name, nicename FROM dog_breeds WHERE nicename='$breed_nname'");
$dog_breed_name_sex=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
$sex_bred=$dog_breed_name_sex['breed_name'];
$breed=$dog_breed_name['breed_name'];
$br_nicename=$dog_breed_name['nicename'];
$exp_value=explode('-', $section[1],2);
if(($exp_value[0]=="male" || $exp_value[0]=="female") && $exp_value[1]!=""){
$select_sex_options=query_execute("SELECT dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND breed_nicename='$exp_value[1]' AND dog_sex!='' GROUP BY dog_sex");
}else{
$select_sex_options=query_execute("SELECT dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex!='' GROUP BY dog_sex");
}

if($exp_value[0]=="male"){
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex='M' AND (breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND breed_nicename!='bull-terrier-miniature' AND  breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') GROUP BY breed_nicename ORDER BY dog_breed ASC");
}elseif($exp_value[0]=="female"){
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1'  AND dog_sex='F' AND (breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND breed_nicename!='bull-terrier-miniature' AND  breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') GROUP BY breed_nicename ORDER BY dog_breed ASC");
}else{
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND (dog_breed!='' AND breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND  breed_nicename!='bull-terrier-miniature' AND breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY breed_nicename ORDER BY dog_breed ASC");
}

if($sex=="male"){$sex="M";}if($sex=="female"){$sex="F";}
if(strlen($section[1])=='1' && !is_nan($section[1])){
$sc_dog_query=" AND dog_name LIKE '$section[1]%'";	
}elseif($sex!='' && $breed==''){
$sc_dog_query=" AND dog_sex='$sex'";
$option_sex=$sex;	
}elseif($sex=='' && $breed!=''){
if($br_nicename=='bull-terrier'){
$sc_dog_query=" AND (breed_nicename='bull-terrier' OR breed_nicename='bull-terrier-miniature')";	
}elseif($br_nicename=='kombai'){
$sc_dog_query=" AND (breed_nicename='kombai' OR breed_nicename='combai')";		
}elseif($br_nicename=='dachshund-miniature-long-smooth-wire-haired'){
$sc_dog_query=" AND (breed_nicename='dachshund-miniature-long-haired' OR breed_nicename='dachshund-miniature-smooth-haired' OR breed_nicename='dachshund-miniature-long-smooth-wire-haired')";		
}elseif($br_nicename=='dachshund-standard-long-smooth-wire-haired'){
$sc_dog_query=" AND (breed_nicename='dachshund-standard-long-haired' OR breed_nicename='dachshund-standard-smooth-haired' OR breed_nicename='dachshund-standard-long-smooth-wire-haired' OR breed_nicename='dachshundstandard(smoothhaired)')";		
}elseif($br_nicename=='poodle-standard-miniature-toy'){
$sc_dog_query=" AND (breed_nicename='poodle-miniature' OR breed_nicename='poodle-standard' OR breed_nicename='poodle-toy'  OR breed_nicename='poodle-standard-miniature-toy')";		
}elseif($br_nicename=='chihuahua-long-smooth-coat'){
$sc_dog_query=" AND (breed_nicename='chihuahua' OR breed_nicename='chihuahua-long-smooth-coat')";		
}elseif($br_nicename=='spitz'){
$sc_dog_query=" AND (breed_nicename='spitz-indian' OR breed_nicename='spitz' OR breed_nicename='spitz-german')";		
}else{
$sc_dog_query=" AND dog_breed='$breed'";		
}	
$breed_n_name=query_execute_row("SELECT nicename FROM dog_breeds WHERE breed_name='$breed'");
$option_breed=$breed_n_name['nicename'];
}elseif($sex!='' && $breed!=''){
$sc_dog_query=" AND dog_breed='$breed' AND dog_sex='$sex'";
$option_sex=$sex;
$option_breed=$breed;
}elseif($exp_value[0]=="male"){
$sc_dog_query=" AND breed_nicename='$exp_value[1]' AND dog_sex='M'";
$option_sex='M';
$option_breed=$exp_value[1];
}elseif($exp_value[0]=="female"){
$sc_dog_query=" AND breed_nicename='$exp_value[1]' AND dog_sex='F'";
$option_sex='F';
$option_breed=$exp_value[1];
}elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[1]!=''){
$sc_dog_query=" AND breed_nicename='$breed_nname'";
$option_breed=$breed_nname;	
}else{
	$sc_dog_query=" AND dog_name LIKE 'a%'";
	$option_sex='';
$option_breed='';
}
$url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];


if (strpos($url,'shopQuery=&filter=filter') !== false) {
 $showRecord=0; 
}

$seach_dog_filter=query_execute("SELECT dog_name, breed_nicename, dog_breed, dog_nicename, dog_sex,dog_image FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY dog_name ORDER BY dog_name ASC LIMIT $showRecord, $maxshow");
$seach_dog_filter_all=query_execute("SELECT dog_name, breed_nicename, dog_breed, dog_nicename, dog_sex,dog_image FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY dog_name");
$totrecord = mysql_num_rows($seach_dog_filter_all);
$titlepage   = $show+1;
$paginationcount=($totrecord-(12*$titlepage));
/*function showPaginarion($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl){
$totpage = $totrecord / $maxshow;
$show=$show+1;
$int = (int)$totpage;
$decimal = $totpage - $int;

if($decimal){
$totpage = $int + 1;
}
$pageLimit = 12;
$pageMid = $pageLimit/ 2;
if(!$show){
$show=1;
} 

if($show >= $pageLimit){ 
$previous=$show-$pageLimit;
if($previous < 1){
$previous=1;
} ?>
<li class='prev'>
<?
echo"<a href='$pageUrl/$previous/'>&lt; Prev $pageLimit</a>";
?></li>
<?
}

if($show < $pageLimit){
$i=1;
}else{
$i=$show - $pageMid;
}
if($show >= $pageLimit){
$pageShowEnd = $show + $pageMid;
}else{
$pageShowEnd = $pageLimit;
}
if($pageShowEnd >= $totpage){
$i=$i - $pageMid;
}
if($i < 1){
$i=1;
}
for($i;$i <= $totpage and $i <= $pageShowEnd;$i++){
	if($show==$i){ ?>
    <li data-original-title='Current Page ' class='current active'><?
	echo"<a>$i</a>"; ?>
    </li><?
	}else{ ?>
    <li data-original-title='Current Page'><?
	echo"<a href='$pageUrl/$i/'>$i</a>"; ?>
    </li><?
	}
}
if($totpage > $i){ 
$next=$i;
echo"<li class='next'><a href='$pageUrl/$next/'>Next $pageLimit &gt;</a></li>";
} 
}*/
 if($section[0]=='' && $section[1]==''){
  $title="Dog Names | Complete list of popular dog names | Dogspot.in";
  $keyword="Dog name, puppy names, cute dog names, popular dog names, cool dog names, friendly dog name, funny puppy namess";
  $desc="Names are very important for our little furry friends. Here is the complete list of popular male and female dog names at dogspot.in";
  $h1_data='Dog Names'; ?>
<? }elseif($section[1]=='female'){
  $title="Best Dog Name For Female | Popular Puppy Names for Girl";
  $keyword="Dog name for females, female dog name, list of dog names for girl, best dog name for girl.";
  $desc="Search best dog names for your female puppy. Discover list of cute and funny dog names for girl.";
  $h1_data='Dog Names for Female Puppy'; 
   }elseif($section[1]=='male'){
  $title="Best Dog Name For Male | Popular Puppy Names for Boy";
  $keyword="Dog name for Males, male dog name, list of dog names for boy, best dog name for boy.";
  $desc="Search best dog names for your male puppy. Discover list of cute and funny dog names for boy.";
  $h1_data='Dog Names for Male Puppy'; 
  }elseif(strlen($section[1])=='1' && !is_nan($section[1])){
  $title="Dog Name Start with Letter ".ucwords($section[1])." | Popular Puppy Names Begin with Alphabet".ucwords($section[1]);
  $keyword="Dog name with letter ".ucwords($section[1]).",".ucwords($section[1])." letter Names for puppy, best dog name start with letter ".ucwords($section[1]).", popular puppy name with Alphabet ".ucwords($section[1]);
  $desc="Search Dog Names starts with Letter ".ucwords($section[1])." for your male and female puppy. Discover best and funny dog names begin with Alphabet ".ucwords($section[1]);
  $h1_data='Dog Names Start with Letter'.' '. ucwords($section[1]); 
  }elseif($exp_value[0] == "female"){
  $title="Best Dog Names for Female ".$sex_bred." | Popular Puppy Names for Girl".$sex_bred;
  $keyword="Best Dog Names for female".$sex_bred.",".$sex_bred." female dog names, best ".$sex_bred." nicknames for girl";
  $desc="Search best dog names for your female".$sex_bred." Discover list of cute and funny ".$sex_bred." dog names for girl";
  $h1_data='Dog Names for Female'.' '. $sex_bred; 
 }elseif($exp_value[0] == "male"){
  $title="Best Dog Names for Male ".$sex_bred." | Popular Puppy Names for Boy ".$sex_bred;
  $keyword="Best Dog Names for male ".$sex_bred.", ".$sex_bred."male dog names, best ".$sex_bred." nicknames for boy.";
  $desc="Search best dog names for your male ".$sex_bred.". Discover list of cute and funny ".$sex_bred." dog names for boy.";
  $h1_data='Dog Names for Male'.' '. $sex_bred; 
  }elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[1]!=''){
  $title="Best Dog Names for ".$breed." | Popular Puppy Names for ".$breed;
  $keyword="Best Dog Names for ".$breed.", ".$breed." dog names, best ".$breed." nicknames";
  $desc="Search best dog names for your ".$breed.". Discover list of cute and funny ".$breed." dog names.";
  $h1_data='Dog Names for'.' '. $breed; ?>
<? }?>
<? if($section[1]==''){
$alternate="https://m.dogspot.in/dog-names/"; 
 }else{
	 $alternate="https://m.dogspot.in/dog-names/".$section[1]."/"; 
	}?>
<?
if($section[1]==''){ ?>
<link rel="next" href="/dog-names/a/1/">	
<?	}
 if($titlepage>1){ ?>
<link rel="prev" href="/dog-names/<? if($section[1]!=''){ ?><?=$section['1'];?>/<? echo ($titlepage-1)?>/<? }?>">
<? }?>
<? if($paginationcount>1 && $section[1]!=''){ ?>
<link rel="next" href="/dog-names/<? if($section[1]!=''){ ?><?=$section['1'];?>/<? echo ($titlepage+1)?>/<? }?>">
<? }?>
<? if($section[1]) { 
$canonical="https://www.dogspot.in/dog-names/$section[1]/";
}else
{
$canonical="https://www.dogspot.in/dog-names/";	
}
   $imgURLAbs="https://www.dogspot.in/new/pix/dog-name.jpg";
   $page_type='dog-names';
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
if(strlen($section[1])=='1' && !is_nan($section[1])){
	$bread_crum='Starts with Letter'.' '.$section[1];
    }elseif($section[1]=="male"){
    $bread_crum='Male';
}elseif($section[1]=="female"){
	$bread_crum='Female';
	}elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[1]!=''){
	$bread_crum=$breed;
    $dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$section[1]'");	
	 $firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$section[1];
	}elseif($exp_value[0] == "male" && $exp_value[1]!=""){
		$dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
		$firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$exp_value[1];
		$bread_crum='Male'.' '.$dog_breed_name_bredcrum['breed_name'];
    }elseif($exp_value[0] == "female" && $exp_value[1]!=""){
		$dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
		$firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$exp_value[1];
		$bread_crum='Female'.' '.$dog_breed_name_bredcrum['breed_name'];
	}
?>
<div class="breadcrumbs" >
  <div class="container"> 
  <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
    <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <a content="https://www.dogspot.in/" itemprop="item"><span itemprop="name">Home</span></a>
      <meta itemprop="position" content="1" /> </span>
     <span> / </span>
    
     <? $wag_dog_brrrd = query_execute_row("SELECT breed_engine FROM dog_breeds WHERE nicename='$firstBreadcum' ");
 if($wag_dog_brrrd['breed_engine'] == '1'){?>
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<a content="https://www.dogspot.in/<?=$firstBreadcum?>/" itemprop="item"  data-ajax="false" ><span itemprop="name"><?=$firstBread?></span></a>
 <meta itemprop="position" content="2" /></span>
<?  }else{?>

     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <span itemprop="name"><a content="https://www.dogspot.in/dog-names/" itemprop="item">Dog Names</a></span>
     <meta itemprop="position" content="2" /> 
     </span>
     <? }?>
    <? if($section[0]!=''){?> 
    <span> / </span>
    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <span class="brd_font_bold" itemprop="name"><?=ucwords($bread_crum);?> Names</span>
       <meta itemprop="position" content="3" /> </span><? }?>
    </div>
  </div>
</div> 
</div>

<div class="container">
<div class="row">
<div class="col-md-12 text-center">

<? require_once($DOCUMENT_ROOT . '/applyBanner.php'); ?> 
</div>
</div>
</div>

<section class="dog-name-sec">
<div class="container">
<div class="row">
<div class="col-md-8">                     
<h1>Dog Names</h1>
		   
<div class="dogname-dec">Have you met new parents or parents - to - be? One of the most interesting discussions that often arise in the company of new parents is "what would be the new baby called?", followed by a myriad of suggestions by family and friends. Just like human babies, new pet parents also face a similar predicament "what to name their new furry baby". Your search for the perfect name ends right here. We bring to you a list of names that you can select and choose from! Happy searching!! </div>
<div class="dogname-header">
      <form method="post" name="formcomnt" id="formcomnt">
        <div class="row">
         <div class="col-md-5">
          <select class="form-control dogname-filter_drop" name="breed" id="breed" onchange="get_sex_data()">
            <option value="">Select Breed</option>
                        <? while($get_breed=mysql_fetch_array($select_dog_data)){
				$dg_breed=$get_breed['breed_nicename'];?>
            <option value="<?=$get_breed['breed_nicename'];?>" <? if($dg_breed==$option_breed){echo'selected="selected"';}?>><?=$get_breed['dog_breed'];?></option>
            <? }?>
           </select>
           </div>
          <div class="col-md-5">
          <select class="form-control dogname-filter_drop" name="sex" id="sex">
            <option value="">Select Gender</option>
                        <? while($get_sex_options=mysql_fetch_array($select_sex_options)){
				$sex_data_opt=$get_sex_options['dog_sex'];
				if($sex_data_opt=='M'){
					$v_data_opt='male';
					$dis_data_opt='Male';
					}
					if($sex_data_opt=='F'){
					$v_data_opt='female';
					$dis_data_opt='Female';
					}
				?>
            <option value="<?=$v_data_opt?>" <? if($option_sex==$sex_data_opt){echo'selected="selected"';}?> id="<?=$sex_data_opt; ?>"><?=$dis_data_opt;?></option>
            <? }?>  </select>
          </div>
         <div class="col-md-2"> 
         <input type="button" value="Go" class="dog_breed_btn" name="search_dog" id="search_dog" onclick="callbreedurl()">
        </div>
        </div>
        <ul>
          <li>Names A-Z</li>
          <li><a href="/dog-names/a/" class="current">A</a></li>
          <li><a href="/dog-names/b/">B</a></li>
          <li><a href="/dog-names/c/">C</a></li>
          <li><a href="/dog-names/d/">D</a></li>
          <li><a href="/dog-names/e/">E</a></li>
          <li><a href="/dog-names/f/">F</a></li>
          <li><a href="/dog-names/g/">G</a></li>
          <li><a href="/dog-names/h/">H</a></li>
          <li><a href="/dog-names/i/">I</a></li>
          <li><a href="/dog-names/j/">J</a></li>
          <li><a href="/dog-names/k/">K</a></li>
          <li><a href="/dog-names/l/">L</a></li>
          <li><a href="/dog-names/m/">M</a></li>
          <li><a href="/dog-names/n/">N</a></li>
          <li><a href="/dog-names/o/">O</a></li>
          <li><a href="/dog-names/p/">P</a></li>
          <li><a href="/dog-names/q/">Q</a></li>
          <li><a href="/dog-names/r/">R</a></li>
          <li><a href="/dog-names/s/">S</a></li>
          <li><a href="/dog-names/t/">T</a></li>
          <li><a href="/dog-names/u/">U</a></li>
          <li><a href="/dog-names/v/">V</a></li>
          <li><a href="/dog-names/w/">W</a></li>
          <li><a href="/dog-names/x/">X</a></li>
          <li><a href="/dog-names/y/">Y</a></li>
          <li><a href="/dog-names/z/">Z</a></li>
        </ul>
         <div class="dog-breeds-dec"><? if($section[0]==''){?><h2 class="dog-breeds-dec_h2">Dog Names A to Z List</h2>
		<? }else{?><h2><?=$h1_data;?><span style="display:none"><?=$section[2]?></span></h2><? }?>
        </div>
     </form>
      </div>
<div class="dognames-list">
        <table cellpadding="0" cellspacing="0" >
          <tr>
            <th>Dog Name</th>
            <th>Breed</th>
            <th>Gender</th>
            <th>Details</th>
          </tr>
          <? while($get_search_data=mysql_fetch_array($seach_dog_filter)){
			   $dog_image=$get_search_data['dog_image'];
			   $imm=$DOCUMENT_ROOT."/dogs/images/".$dog_image;
			  if(file_exists($imm)){
			 $dg_name=$get_search_data['dog_name'];
			  $string=preg_replace('/[^A-Za-z0-9\-]/', ' ', ucwords(strtolower($get_search_data['dog_name'])));
			  $wordlist = array("ch", "biss", "Ind Ch", "Bis", "Ch", "Grand Ind", "sale", "month", "imp", "Sale", "Month", "Imp", "Import", "Dogs", "-");
              foreach ($wordlist as &$word) {
              $word = '/\b' . preg_quote($word, '/') . '\b/';
              }
			  $string = preg_replace('/[0-9]+/', '', $string);
              $string = preg_replace($wordlist, '', $string);
			  $breed_engine_data=query_execute_row("SELECT nicename FROM dog_breeds WHERE breed_name='".$get_search_data['dog_breed']."' AND breed_engine='1'");
			  $breed_engine_nicename=$breed_engine_data['nicename'];?>
          <tr>
            <td class="td-highlight"><?=$string;?></td>
            <td><? if($breed_engine_nicename!=''){?><a href="/<?=$breed_engine_nicename?>/"><? }?><?=$get_search_data['dog_breed'];?><? if($breed_engine_nicename!=''){?></a><? }?></td>
            <td><? if($get_search_data['dog_sex']=="M"){echo "Male";}else{echo "Female";}?></td>
            <td><a href='/wag_club/allsearch/breed/location/<?=strtolower($dg_name);?>/'>Meet all <?=ucwords($string);?></a></td>
          </tr>
          <? } }?>
        </table>
      </div>
      <? if($totrecord>12){?>
<div>    <ul class="dogname-pagination">
<?
if($section[0]==''){
	$pageUrl = "/dog-names/a";
	}else{
$pageUrl = "/$section[0]/$section[1]";
	}
showPaginarion($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl);
?>
</ul>
</div>    
 <? }?>
  <? if($totrecord>12){?>
<h2>Most Searched Dog Names By Breed</h2>  
<div class="row">

<div class="col-md-6">
<div class="breed-name-box"><a href="https://www.dogspot.in/dog-names/golden-retriever/">
Dog Names for Golden Retriever</a> </div>


</div>
<div class="col-md-6">
<div class="breed-name-box">
<a href="https://www.dogspot.in/dog-names/german-shepherd-dog-alsatian/">Dog Names for German Shepherd</a>
</div>

</div>
     
     
<div class="col-md-6">
<div class="breed-name-box"><a href="https://www.dogspot.in/dog-names/tibetan-mastiff/">
Dog Names for Tibetan Mastiff</a> </div>


</div>
<div class="col-md-6">
<div class="breed-name-box">
<a href="https://www.dogspot.in/dog-names/rottweiler/">Dog Names for Rottweiler</a>
</div>

</div>
<div class="col-md-6">
<div class="breed-name-box">
<a href="https://www.dogspot.in/dog-names/great-dane/">Dog Names for Great Dane</a>
</div>

</div>
<div class="col-md-6">
<div class="breed-name-box">
<a href="https://www.dogspot.in/dog-names/labrador-retriever/">Dog Names for Labrador Retriever</a>
</div>

</div>
<div class="col-md-6">
<div class="breed-name-box">
<a href="https://www.dogspot.in/dog-names/pug/">Dog Names for Pug</a>
</div>

</div>
<div class="col-md-6">
<div class="breed-name-box">
<a href="https://www.dogspot.in/dog-names/beagle/">Dog Names for Beagle</a>
</div>

</div>

     </div>
<? }?>
      <!-- left--> 
    </div>
    
    <!-- right-->
     <div class="col-md-4 col-xs-12 ">
    <div class="row">
    
     <? if($totrecord>2){?>
     
        <section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;"> 
           <div class="dogname-releted-article">
        <div class="dogname-releted-art-text">You May Also Like To Read</div>
        <ul class="dogname-releted-artsec">
          <li>
            <div class="dogname-rel-art-img"> <a href="/male-dog-names/"> <img src="https://ik.imagekit.io/2345/dog-names/Images/art-1.jpg" alt="Unique &amp; Interesting Male Dog Names" title="Unique &amp; Interesting Male Dog Names"></a></div>
            <div class="dogname-rel-art-headline"> <a href="/male-dog-names/">Unique &amp; Interesting Male Dog Names</a> </div>
          </li>
          <li>
            <div class="dogname-rel-art-img"> <a href="/female-dog-names/"> <img src="https://ik.imagekit.io/2345/dog-names/Images/art-2.jpg" alt="Unique and Interesting Female Dog Names" title="Unique and Interesting Female Dog Names"></a></div>
            <div class="dogname-rel-art-headline"> <a href="/female-dog-names/">Unique and Interesting Female Dog Names</a> </div>
          </li>
          <li>
            <div class="dogname-rel-art-img"> <a href="/best-dog-names/"> <img src="https://ik.imagekit.io/2345/dog-names/Images/art-3.jpg" alt="Most Popular Dog Names" title="Most Popular Dog Names"></a></div>
            <div class="dogname-rel-art-headline"> <a href="/best-dog-names/">Most Popular Dog Names</a> </div>
          </li>
          <li>
            <div class="dogname-rel-art-img"> <a href="/cute-dog-names/"> <img src="https://ik.imagekit.io/2345/dog-names/Images/art-4.jpg" alt="Reliable Lab Breeder / Trusted Vet" title="Reliable Lab Breeder / Trusted Vet"></a></div>
            <div class="dogname-rel-art-headline"> <a href="/cute-dog-names/">Cute Dog Names</a> </div>
          </li>
          <li>
            <div class="dogname-rel-art-img"> <a href="/special-names-for-your-new-best-friend/"> <img src="https://ik.imagekit.io/2345/dog-names/Images/art-5.jpg" alt="Special Names For Your New Best Friend" title="Special Names For Your New Best Friend"></a></div>
            <div class="dogname-rel-art-headline"> <a href="/special-names-for-your-new-best-friend/">Special Names For Your New Best Friend</a> </div>
          </li>
        </ul>
      </div>
           </section> 
<section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;"> 
                                                                <!-- Widget Header --> 
                                                                <header class="clearfix"> 
                                                                        <h4>Sponsored: In the Stores</h4> 
                                                                                                        </header> 
<?php
$getItems = query_execute("SELECT si.name,si.nice_name,si.item_id,sia.price,sia.mrp_price,si.item_parent_id FROM shop_items as si,shop_item_affiliate as sia WHERE sia.item_id=si.item_id AND showbanner='1' ");
while ($fetchaffiliate = mysql_fetch_array($getItems)) {
    $name           = $fetchaffiliate['name'];
    $item_id        = $fetchaffiliate['item_id'];
    $item_parent_id = $fetchaffiliate['item_parent_id'];
    $nice_name      = $fetchaffiliate['nice_name'];
    $price          = $fetchaffiliate['price'];
    $mrp_price      = $fetchaffiliate['mrp_price'];
    $rowdatM        = query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
    if (!$rowdatM['media_file']) {
        $rowdatM = query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
    }
    
    $imglink = 'https://ik.imagekit.io/2345/tr:h-100,w-100,c-at_max/shop/item-images/orignal/' . $rowdatM['media_file'];
    
    
?> 
                       <div class="gurgaon_offers">  
             
                <div class="gurgaon_offersr"> <a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><img src="<?= $imglink ?>" width="100" height="93"></a> </div> 
                <div class="gurgaon_offersl"> 
                    <div class="gurgaon_offers_pn"><a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><?= $name ?></a> </div> 
                                        <div class="gurgaon_offers_pr"> 
                                        <?php
    if ($mrp_price > $price) {
?> 
                                       <span>Rs. <del><?= $mrp_price ?></del>  
                                        </span> &nbsp; &nbsp; <?php
    }
?> 
                                       <span class="price-color"> 
                                        Rs. <?= $price ?></span> 
                                       <div class="aff-m-logo"> <a href="/<?= $nice_name ?>/?UTM=bannerAmazon"><img src="/new/articles/img/amazon.jpg" /></a> 
                                       </div> 
                                        </div> 
                                    </div> 
               
        </div> 
     <?php
}
?> 
        </section>
</div>

      <? }?>
      </div>
     </div>
     </div>
  </div>
  </section>
  <!-- right end--> 
  <!-- dog breeds name section end--> 
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script type="text/javascript" src="/bootstrap/js/toggle-nav.js"></script>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/dog-name.css" />
<script type="text/javascript">
        $( document ).ready(function() {
			var fbId = "https://www.dogspot.in/dog-names/";
            $.getJSON('https://graph.facebook.com/?id='+fbId+'', OnCallBack);
        });
        function OnCallBack(r, s) {
				var valueurl="https://www.dogspot.in/dog-names/";
ShaAjaxJquary("/share-update-des.php?surl="+valueurl+"&fbShare="+r.share.share_count+"&fbS=1", "#mis", '', 'formcomnt', 'GET', '#loading', '','REP');
//alert("total share:- "+r.share.share_count);

        }
</script>

<script>
function get_sex_data(){
var sex_data=$('#breed').val();
if(sex_data){
ShaAjaxJquary('/dog-names/ajax-data.php?sex_data='+sex_data+'', '#att_2', '', '', 'GET', '#att_2', 'Loding...', 'REP');
}
}
</script>
<script>
function callbreedurl(){
	var brd=document.getElementById("breed").value;
	var sex=document.getElementById("sex").value;
	var sexvalue=document.getElementById("sex").value;
	var sexvalue=sexvalue+'-';
	if(brd!='' && sex==''){
	window.location.href = '/dog-names/'+brd;
	
}
	if(brd=='' && sex!=''){
	window.location.href = '/dog-names/'+sex;
}
	if(brd!='' && sex!=''){
	var breedImplode=sexvalue.concat(brd);
	window.location.href = '/dog-names/'+breedImplode;
}
}
</script>
<!-- for image zoom -->
<!-- read more and --read less-->
<script>
$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 400;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Read More";
    var lesstext = "Read Less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
<!-- read more end read less end-->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script>   
 $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });</script>
<script>
$('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
  interval: false
});

</script>
<script>
  $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

});
   </script> 
<script>
  $(function() {
  $(".expand").on( "click", function() {
    $(this).next().slideToggle(200);
    $expand = $(this).find(">:first-child");
    
    if($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
});
  </script>  

 <?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?> 