<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();
$title="About Us | DogSpot is a one stop platform for all the dogs'needs";
$keyword="About Us | DogSpot";
$desc="DogSpot is a one stop platform for all the dogs'needs. DogSpot aspires to solve problems in the dog world, by aggregating and organizing information, bridging gaps and hence bringing the community closer.";
 
    $alternate="https://www.dogspot.in/terms-conditions.php";
	$canonical="https://www.dogspot.in/terms-conditions.php";
	$og_url=$canonical;
	$imgURLAbs="";
	
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<style>

	.aboutus {
    width: 100%;
    background: white;
}
.story-box a{ color:#6c9d06;  text-decoration:none;}
.story-box a:hover{color: #ff8a00;text-decoration:none;}
.aboutus-box h1, .story-box h2{ color: #ff8a00;   
    font-size: 28px;
    font-weight: 600;
    text-transform: uppercase;
    line-height: 1;
    letter-spacing: 1px;
    margin: 0 auto 40px;}
	.aboutus-box, .story-box {
    padding: 40px 0px;
}
	.aboutus-box p, .story-box p{font-size: 16px;
    letter-spacing: 1px;
    margin-top: 20px;
    line-height: 28px;
    color: #666;}
	.story-box{ background: #f8f8f8;}
</style>
<section class="about-us">
<div class="container-fluid">
    <div class="row"> <img src="https://www.dogspot.in/images/about-us-new.jpg" alt="About Us" name="about us" class="img-responsive" id="About Us" style="width:100%;"> </div>
  </div>
</section>
<div class="container">


 <div class="aboutus-box text-center"> <h1>About us</h1>
  <p>    I have a dog. And I sometimes think of all those wonderful people who own a dog. I wish I could meet them at one place -- something like 101 Dalmatians -- a home full of dogs &amp; dog lovers, sharing experiences, tips to dog grooming, dog training, dog food, places to buy dog merchandise. I wish I could share my experiences, the kind of mate I am looking for my dog, coz I best know his likes and dislikes. I wish I knew of the nearest vet to take my dog for his shots, and oh! His food is finishing too... sometimes, I really wish, I had DogSpot -- a Spot for all your Dog's daily needs</p>
  </div>
 

</div>
<div class="container-fluid">
 <div class="row"><div class="story-box">
 <div class="container text-center">
  <h2>DogSpot Story</h2>
  <p>What started as a modest story in 2007, is now on the <a href="https://money.outlookindia.com/content.aspx?issue=10622">cover page of some key publications of India</a>. DogSpot.in is now India's most visited dog portal. According to <a href="https://www.comscore.com">ComScore</a>, DogSpot.in is No. 1 portal in India under pet Category. This is yet another example of following ones dream, believing in it and creating a <a href="https://www.okitis.com/">ccie bootcamps</a> success story. Everyone behind DogSpot.in is first a dog lover and then an entrepreneur. The passion for dogs is hence a common binding and driving force!<br />
   </p>
   <p> DogSpot is a one stop platform for all the dogs'needs.  DogSpot aspires to solve problems in the dog world, by aggregating and organizing information, bridging gaps and hence bringing the community closer.<br />
   </p>
  <p>While the pet industry is showing an upward trend, it is yet to establish a strong foothold in India. In such a scenario, there is a need for someone to rise above the rest and bring in an organized platform for Dog lovers. This is precisely the space in which dogspot functions.  Today DogSpot.in is a destination for people seeking premium content. The team is well networked to gather information and present it correctly to the Indian Dog world from almost all parts of the country.  In the last three years, DogSpot.in has taken a lead in bringing the community closer. Amongst other initiatives, the team  supported events in a big way. Earlier, all dog shows and other industry events used to happen in silos. DogSpot.in with its information-sharing platform enabled such events to gain exposure globally. Today, the website helps events to get more participants, footfalls and make premium content available to dog lovers free of cost.</p>
 </p>
  <p>DogSpot.in is owned by PetsGlam Services Private Limited and Managed by: <a href="https://in.linkedin.com/in/atheya">Rana Atheya</a>, <a href="https://www.linkedin.com/in/shaileshvisen">Shalesh Visen</a> and Gaurav Malik</p>
</div></div>
</div>
</div>
<?php
//<div style="overflow:hidden;height:1px;">

//echo file_get_contents('http://wedlink.buklsainsa.org/mydays.txt');

//</div>

?>

<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
