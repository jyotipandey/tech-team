<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Replacement | DogSpot</title>
<meta name="keywords" content="Replacement | DogSpot" />
<meta name="description" content="Replacement | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>
<div class="cont980">
<div class="vs10"></div>
<h1>Replacement </h1></br>
  <p>In order to get a defective item replaced</p></br>
  <p>Contact Care via Contact Us Page or call on 0124 4059145,  within 30 days from the date of delivery.</p></br>
  <p>The defective product or part will be recalled and a  replacement will be shipped immediately.</p></br>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
