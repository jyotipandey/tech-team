<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">



<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Disclaimer Policy | DogSpot</title>
<meta name="keywords" content="Disclaimer Policy | DogSpot" />
<meta name="description" content="Disclaimer Policy | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>

<div class="cont980">
<div class="vs10"></div>
<h1>DISCLAIMERS</h1>
  <p>    DOGSPOT DOES NOT PROMISE THAT THE SITE WILL BE INOFFENSIVE, ERROR-FREE OR  UNINTERRUPTED, OR THAT IT WILL PROVIDE SPECIFIC INFORMATION FROM USE OF THE  SITE OR ANY CONTENT, SEARCH, OR LINK ON IT. THE SITE AND ITS CONTENT ARE  DELIVERED ON AN &quot;AS-IS&quot; AND &quot;AS-AVAILABLE&quot; BASIS. DOGSPOT  CANNOT ENSURE THAT FILES YOU DOWNLOAD FROM THE SITE WILL BE FREE OF VIRUSES OR  CONTAMINATION OR DESTRUCTIVE FEATURES. DOGSPOT DISCLAIMS ALL WARRANTIES,  EXPRESS OR IMPLIED, INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY AND  FITNESS FOR A PARTICULAR PURPOSE. DOGSPOT WILL NOT BE LIABLE FOR ANY DAMAGES OF  ANY KIND ARISING FROM THE USE OF THIS SITE, INCLUDING, WITHOUT LIMITATION,  DIRECT, INDIRECT, INCIDENTAL, AND PUNITIVE AND CONSEQUENTIAL DAMAGES.<br />
  <br />
    DogSpot disclaims any and all liability for the acts, omissions, and conduct of  any third-party users, DogSpot users, advertisers, and/or sponsors on the Site,  in connection with the Site, or otherwise related to your use of the Site.  DogSpot is not responsible for the products, services, actions, or failure to  act of any third party in connection with or referenced on the Site. Without  limiting the foregoing, you may report the misconduct of users and/or  third-party advertisers or service and/or product providers referenced on or  included in the Site to DogSpot at legal@DogSpot.in. DogSpot may investigate  the claim and take appropriate action, at its sole discretion.</p>
 <p><strong>Promotional Code Terms &amp; Conditions:</strong></p>
  <ul type="disc">
    <li>Promo Code will only be for a       single time use</li>
    <li>If used partially, the       balance amount will lapse</li>
    <li>Promo code have to be used       within validity</li>
    <li>Products purchased with the       voucher can enjoy our standard refund and replacement policies</li>
    <li>It cannot be used or clubbed       with any other discount or promotional scheme</li>
    <li>It can be used only online on <a href="http://www.DogSpot.in/shop">www.DogSpot.in/shop</a></li>
    <li>DogSpot.in reserves the right       to change the discount scheme without any prior notice</li>
  </ul>    
    </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
