<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wag_club/css/wag_tag.css" />
<title>Wag Club</title>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script language="javascript" type="text/javascript" src="js/jquery.min.1.8.2.js"></script>  
<script type="text/javascript" src="/new/js/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?> 
<script>
$(document).ready(function(e) { 
    $("#open_detail_dg").click(function(e) {
	// $("#open_detailBox_dg").insertBefore("#open_detail_dg");
 	$("#open_detail_dg").hide();
	$("#open_detailBox_dg").show("slide",{direction:'top'});
    $("#open_detailBox_dg").show();
	});
	$("#open_detail_dg1").click(function(e) {
    $("#open_detailBox_dg").hide();
    $("#open_detail_dg").show();
	});
});
function callzipdial(){
	user_phn=$("#mobile1").val();
ShaAjaxJquary('zipdial1.php?user_phone='+user_phn+'', '#amountBox1', '', '', 'POST', '#amountBox1', '', 'REP');
}
</script>
<div class="dog_to_track_wt">
<div class="cont980">
<div class="wagTag_box">
<h1> Activate your wag id</h1>
<div class="wt_leftBox">
<div class="wt_logo">
  Wag Tag
</div>
<div class="wt_Imgs">

</div>
</div>

<div class="wt_rytBox">
<div class="wt_form">
<h3>Wag Tag Info</h3>
<ul>
<li>
<label>Wag ID</label>
<input name="" type="text" />
</li>
<li>
<label>Activation Code</label>
<input name="" type="text" />
</li>
<li>
<label>Mobile number</label>
<input name="mobile1" id="mobile1" type="text" />
<a onclick="callzipdial()">Verify</a></li>
<li><span><a href="#"> + Add More</a></span></li>
</ul>
<div class="wt_btnBox">
<div class="wt_actBtn">Activate</div>
<div class="wt_canBtn wt_actBtn">Cancel</div>
</div>
 <div id="amountBox1"></div>
<ul>
<li>
<label>Alternative number </label>
<input name="" type="text" />
</li>
<li><span><a href="#"> + Add More Details</a></span></li>
</ul>
<div class="wt_btnBox">
<div class="wt_actBtn">Activate</div>
<div class="wt_canBtn wt_actBtn">Cancel</div>
</div>
<h3>KCI Registered Dogs</h3>
<ul>
<li>
<label>KCI Registration Number </label>
<input name="" type="text" />
</li>
<li>
<label>Registered Name</label>
<input name="" type="text" />
</li>
<li>
<label>Microchip ID</label>
<input name="" type="text" />
</li>
<li>
<label>Breeder</label>
<input name="" type="text" />
</li>
<li>
<label>Dam</label>
<input name="" type="text" />
</li>
<li>
<label>Sire</label>
<input name="" type="text" />
</li>
<li><span><a href="#"> + Add Behavioural Info</a></span></li>
</ul>
<div class="wt_btnBox">
<div class="wt_actBtn">Activate</div>
<div class="wt_canBtn wt_actBtn">Cancel</div>
</div>
<h3>Behavioural Info</h3>
<ul>
<li>
<label>Spay/Neuter</label>
<select name="">
<option>YES</option>
<option>NO</option>
</select>
</li>
<li>
<label>Physical Description (color, markings etc.)</label>
<textarea name="" cols="" rows=""></textarea>
</li>
<li>
<label>Medical Information (allergies, diseases etc.)</label>
<textarea name="" cols="" rows=""></textarea>
</li>
<li>
<label>Behavioural Information</label>
<textarea name="" cols="" rows=""></textarea>
</li>
</ul>
<div class="wt_btnBox">
<div class="wt_actBtn">Activate</div>
<div class="wt_canBtn wt_actBtn">Cancel</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script>
var ajax_call = function() { 
	
  //your jQuery ajax code
  
  var oid=$('#order_id').val();
   var trans=$('#transaction').val();
  var zipitem=$('#zipitem').val();
   var fishtype=$('#fishtype').val();
  var payment1=document.getElementById('payment_type').value;
		
 ShaAjaxJquary("check_order_confirm.php?order_id="+oid+"", "#codconfirm12", '',  '', 'GET', "", '','REP');
   if(zipitem!='2'){
	  	 ShaAjaxJquary('/zipdial/phonevarify.php?transactionToken='+trans+'&order_id='+oid+'&paymenttype='+payment1+'&fishtype='+fishtype+'', '#discountboxconfirm', '', 'formBilling', 'POST', '','', 'REP');
  }
};

var interval = 3000; // where X is your every X minutes
setInterval(ajax_call, interval);
var mo=$('#mobile1').val();
//var od=$('#order_id').val();
ShaAjaxJquary('/new/shop/data_save.php?order_id='+od+'&mobile='+mo+'', '#amountBox1', '', '', 'POST', '#amountBox1', '', 'REP');
 </script> 
