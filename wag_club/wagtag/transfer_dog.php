<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/dogs/arraybreed.php');

if($dog_id){
$sel_details_dog=query_execute_row("SELECT userid FROM dogs_available WHERE dog_id='$dog_id'");
$old_user=$sel_details_dog['userid'];
$transfer_dog_name=$sel_details_dog['userid'];

if($type=='accept'){
$update_dog_owner=query_execute_row("UPDATE dogs_available SET userid='$userid' WHERE dog_id='$dog_id'");
$update_dog_owner=query_execute_row("UPDATE wag_tag SET userid='$userid' WHERE dog_id='$dog_id' AND active_status='1'");
$delete_wag_tag_transfer=query_execute_row("UPDATE wag_tag_transfer SET status='1' WHERE dog_id='$dog_id' AND new_owner_email='$new_email'");
//header("Location:https://www.dogspot.in/dogs/".$dog_nicename);

	$toEmail=	$new_email;
	$mailSubject = "Transfer Wag Tag";
	// mail Body
	$mailBody .= '<p>Dear <strong>'.dispUName($userid).' </strong><br /><br />'.
    'Congratulations. '.$transfer_dog_name.'\'s wag id has been successfully transferred. Kindly update your contact details on the profile page of '
	.$seldetails['dog_name'].' <br />
    To know more about wag tag </p>
    <a href="https://www.dogspot.in/wagtag/index-wt.php">Click Here</a>
<p>---------------------------------------------------------------------<br>
  DogSpot.in<br>
  Spot for all your Dog needs. The Store That Delivers at Your Door.<br>
  --------------------------------------------------------------------- </p>';		
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'From: DogSpot <contact@dogspot.in>' . "\r\n";
        $headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";

        
        $headers .= "Reply-To: DogSpot <contact@dogspot.in>\r\n";
        $headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
        $headers .= "Organization: DogSpot\r\n";        
		
        if(mail($toEmail, $mailSubject, $mailBody, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")) {
            //$registerFlag = 2;
			 $ArrayReturnEmail['errorFlag']= 2;
        
		    //  header("Location: /reg-log/more.php?userid=$cust_email");
        }else{
			$ArrayReturnEmail['errorFlag']= 1;
			$ArrayReturnEmail['errorText']= 'Internal Server Error 1 !';
           // $errorlogin = 1;
            //$errortext3 = "Internal Server Error 1 !";
            //header ("Location: /error.php?errortext=Internal Server Error!");
        }//Send Final Email xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//--------------------------------------------------------email send to old owner--------------------------------------------
	$old_owner_mail=useremail($old_user);
	$toEmail1=	$old_owner_mail;
	$mailSubject1 = "Transfer Wag Tag";
	// mail Body
	$mailBody1 .= '<p>Dear <strong>'.dispUName($old_user).' </strong><br /><br />'.
    dispUName($userid).' has accepted your request for the transfer of '.$transfer_dog_name.' tag , <br /></p>


<p>---------------------------------------------------------------------<br>
  DogSpot.in<br>
  Spot for all your Dog needs. The Store That Delivers at Your Door.<br>
  --------------------------------------------------------------------- </p>';		
	 // Send Final Email-------------------------------------------------------------------------------------------
	 // To send HTML mail, the Content-type header must be set
      //  $toEmail = '"' . $name . '" <' . $useremail . '>';
/*        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'From: DogSpot <contact@dogspot.in>' . "\r\n";
        $headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";

        
        $headers .= "Reply-To: DogSpot <contact@dogspot.in>\r\n";
        $headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
        $headers .= "Organization: DogSpot\r\n";*/
        
        if(mail($toEmail1, $mailSubject1, $mailBody1, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")) {
            //$registerFlag = 2;
			 $ArrayReturnEmail['errorFlag']= 2;
        
		    //  header("Location: /reg-log/more.php?userid=$cust_email");
        }else{
			$ArrayReturnEmail['errorFlag']= 1;
			$ArrayReturnEmail['errorText']= 'Internal Server Error 1 !';
           // $errorlogin = 1;
            //$errortext3 = "Internal Server Error 1 !";
            //header ("Location: /error.php?errortext=Internal Server Error!");
        }//Send Final Email xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

}
if($type=='decline'){
//$delete_wag_tag_transfer=query_execute_row("UPDATE wag_tag_transfer SET status='1' WHERE dog_id='$dog_id' AND new_owner_email='$new_email'");
$delete_wag_tag_transfer=query_execute_row("DELETE FROM wag_tag_transfer WHERE dog_id='$dog_id' AND new_owner_email='$new_email'");
//header("Location:https://www.dogspot.in/dogs/".$dog_nicename);

//--------------------------------------------------------email send to old owner--------------------------------------------
	$old_owner_mail=useremail($old_user);
	$toEmail1=	$old_owner_mail;
	$mailSubject1 = "Transfer Wag Tag";
	// mail Body
	$mailBody1 .= '<p>Dear <strong>'.dispUName($old_user).' </strong><br /><br />'.
    dispUName($userid).' has declined your request to transfer '.$transfer_dog_name.' tag , <br /></p>


<p>---------------------------------------------------------------------<br>
  DogSpot.in<br>
  Spot for all your Dog needs. The Store That Delivers at Your Door.<br>
  --------------------------------------------------------------------- </p>';		
	 // Send Final Email-------------------------------------------------------------------------------------------
	 // To send HTML mail, the Content-type header must be set
      //  $toEmail = '"' . $name . '" <' . $useremail . '>';
/*        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'From: DogSpot <contact@dogspot.in>' . "\r\n";
        $headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";

        
        $headers .= "Reply-To: DogSpot <contact@dogspot.in>\r\n";
        $headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
        $headers .= "Organization: DogSpot\r\n";*/
        
        if(mail($toEmail1, $mailSubject1, $mailBody1, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")) {
            //$registerFlag = 2;
			 $ArrayReturnEmail['errorFlag']= 2;
        
		    //  header("Location: /reg-log/more.php?userid=$cust_email");
        }else{
			$ArrayReturnEmail['errorFlag']= 1;
			$ArrayReturnEmail['errorText']= 'Internal Server Error 1 !';
           // $errorlogin = 1;
            //$errortext3 = "Internal Server Error 1 !";
            //header ("Location: /error.php?errortext=Internal Server Error!");
        }//Send Final Email xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

}}else
{
	?>
                     <html><head> 
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
 </head><body>

 <div class="cb"></div>
 
 </body></html>
	<?
}
?>

