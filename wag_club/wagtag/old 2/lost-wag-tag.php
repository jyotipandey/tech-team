<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wag_club/css/wag_tag.css" />
<title>Wag Club</title>
<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>
<script language="javascript" type="text/javascript" src="js/jquery.min.1.8.2.js"></script>  
<script>
$(document).ready(function(e) { 
    $("#open_detail_dg").click(function(e) {
	// $("#open_detailBox_dg").insertBefore("#open_detail_dg");
 	$("#open_detail_dg").hide();
	$("#open_detailBox_dg").show("slide",{direction:'top'});
    $("#open_detailBox_dg").show();
	});
	$("#open_detail_dg1").click(function(e) {
    $("#open_detailBox_dg").hide();
    $("#open_detail_dg").show();
	});
});
</script>
<div class="dog_to_track_wt">
<div class="cont980">
<div class="wagTag_box">
<h1>Get a Unique Wag Tag for Bruno</h1>
<div class="wt_leftBox">
<div class="wt_logo">
  Wag Tag
</div>
<div class="wt_Imgs">

</div>
</div>

<div class="wt_rytBox">
<div class="wt_form">
<h3>If you apply for a new Wag Tag 'Wag ID 12354' will be deactivated.</h3>
<ul>
<li>
<label>Name</label>
<input name="" type="text" />
</li>
<li>
<label>Pin Code</label>
<input name="" type="text" />
</li>
<li>
<label>Mobile number</label>
<input name="" type="text" />
</li>
<li>
<label>Address</label>
<textarea name="" cols="" rows=""></textarea>
</li>
<li>
<label>Landmark</label>
<input name="" type="text" />
</li>
<li>
<label>City</label>
<select name="">
<option>YES</option>
<option>NO</option>
</select>
</li>
<li>
<label>State</label>
<select name="">
<option>YES</option>
<option>NO</option>
</select>
</li>
</ul>
<div class="wt_btnBox">
<div class="wt_actBtn">Send Wag Tag</div>
<div class="wt_canBtn wt_actBtn">Cancel</div>
</div>

</div>
</div>
</div>
</div>
</div>