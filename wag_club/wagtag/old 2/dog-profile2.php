<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wag_club/css/wag_tag.css" />
<title>Wag Club</title>
<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>
<script language="javascript" type="text/javascript" src="js/jquery.min.1.8.2.js"></script>  
<script>
$(document).ready(function(e) { 
    $("#open_detail_dg").click(function(e) {
	// $("#open_detailBox_dg").insertBefore("#open_detail_dg");
 	$("#open_detail_dg").hide();
	$("#open_detailBox_dg").show("slide",{direction:'top'});
    $("#open_detailBox_dg").show();
	});
	$("#open_detail_dg1").click(function(e) {
    $("#open_detailBox_dg").hide();
    $("#open_detail_dg").show();
	});
});
</script>



<div class="dog_name_dg">
<div class="dog_profileImg_dg">
  <img src="images/pluto.jpg" width="1354" height="706" alt="" />
</div>
<div class="dog_detail_dg">
<div class="name_box_dg">Pluto</div>
<div class="tagline_box_dg">dont disturb i am going online</div>
<div class="wc_chagePic">
<div class="wc_changeDP"><img src="images/change.png" width="14" height="14" alt="" /> Change Display Picture</div>
<div class="wc_changeMenu">
<ul>
<li><img src="images/cam.png" width="14" height="" alt="" /> Choose From Photos</li>
<li><img src="images/upload1.png" width="14" height="" alt="" /> Upload photo</li>
<li><img src="images/reposition.png" width="14" height="14" alt="" /> Reposition</li>
<li><img src="images/cls.png" width="14" height="" alt="" /> Remove</li>
</ul>
</div>
</div>
</div>


<div class="details_box_dg">
<div id="open_detailBox_dg" class="open_detailBox_dg" style="display: none;position:absolute;bottom: -58px; text-align:left;">
                   <ul >
                   <li id="open_detail_dg1" class="firstLi_dg">Pluto<img src="images/arw.png" width="8" height="5" alt="" style=" float:right;padding: 7px 0;"/></li>
                   <li><a href="search.php">Golden Retriever</a></li>
                   <li>10th Oct</li>
                   <li>7, Male</li>
                   <li><a href="wall.php">Upload Activity</a></li>
                   <li>Prerna</li>
                   <li class="lastLi_dg"><a href="add_dog.php">+ Add Another Dog</a></li>
                   </ul>
                   </div>
                 <a id="open_detail_dg" class="open_detail_dg" >Pluto <img src="images/arw-up.png" width="8" height="5" alt="" style=" float:right;padding: 7px 0;"/></a>
                   
</div>

<div class="ryt_boxEdit_dg">
<ul>
<li><a href="#"><span style="height:50px;"><img src="images/wag.png" width="48" height="46" alt="" /></span><p> Wag it</p></a></li>
<li><a href="#"><span style="height:50px;"><img src="images/share.png" width="38" height="38" alt="" /></span><p> Share</p></a></li>
<li><a href="#"><span style="height:50px;"><img src="images/edit.png" width="38" height="38" alt="" /></span><p> Edit</p></a></li>
<li><a href="#"><span style="height:50px;">
<img src="images/upload.png" width="48" height="48" alt="" /></span><p> Upload</p></a></li>
</ul>     
</div>

<div class="dg_infoTag">
<h2><div class="dg_wtBox">About</div> </h2>
<div class="dg_wtBox">
<div class="wt_infoBox fl wt_form">
<h3>Wag Tag Info<span class="wt_edit fr">Edit</span></h3>
<ul>
<li>
<label>Wag ID</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Activation Code</label>
<div class="wt_textInfo"> 14523 </div>
</li>
</ul>
</div>


<div class="wt_infoBox fr wt_form">
<h3>KCI Registered Dogs <span class="wt_edit fr">Edit</span></h3>
<ul>
<li>
<label>KCI Registration Number </label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Registered Name</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Microchip ID</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Breeder</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Dam</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Sire</label>
<div class="wt_textInfo"> 14523 </div>
</li>
</ul>
</div>
<div class="wt_infoBox fl wt_form">
<h3>Contact Info<span class="wt_edit fr">Edit</span></h3>
<ul>
<li>
<label>Name</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Pin Code</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Mobile number</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Address</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Landmark</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>City</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>State</label>
<div class="wt_textInfo"> 14523 </div>
</li>
</ul>
</div>
<div class="wt_infoBox fl wt_form">
<h3>Behavioural Info <span class="wt_edit fr">Edit</span></h3>
<ul>
<li>
<label>Spay/Neuter</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Physical Description (color, markings etc.)</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Medical Information (allergies, diseases etc.)</label>
<div class="wt_textInfo"> 14523 </div>
</li>
<li>
<label>Behavioural Information</label>
<div class="wt_textInfo"> 14523 </div>
</li>
</ul>
</div>



</div>
</div>
</div>

<?php //require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>