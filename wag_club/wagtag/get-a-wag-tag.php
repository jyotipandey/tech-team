<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wag_club/css/wag_tag.css" />
<title>Wag Club</title>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script language="javascript" type="text/javascript" src="js/jquery.min.1.8.2.js"></script>  
<?php /*?><script>
$(document).ready(function(e) { 
    $("#open_detail_dg").click(function(e) {
	// $("#open_detailBox_dg").insertBefore("#open_detail_dg");
 	$("#open_detail_dg").hide();
	$("#open_detailBox_dg").show("slide",{direction:'top'});
    $("#open_detailBox_dg").show();
	});
	$("#open_detail_dg1").click(function(e) {
    $("#open_detailBox_dg").hide();
    $("#open_detail_dg").show();
	});
});
</script><?php */?>
<script type="text/javascript" src="/new/js/jquery/jquery-ui.js"></script>

<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type="text/javascript">
function saveorder(){
	var error='0';
	name=$("#name").val();
	pin=$("#pin").val();
	phn=$("#phn").val();
	add=$("#add").val();
	land=$("#land").val();
	city=$("#city").val();
	state=$("#state").val();
	citystate=$("#citystate").val();
	//---------------------------------name check---------------------
	if(name==''){
	$("#name1").text("Please enter name");
	error='1';
	}else{
	$("#name1").text("");
	}
	//--------------------------------pin check-----------------------
	if(pin==''){
	$("#pin1").text("Please enter pincode");
	error='1';
	}else{
	$("#pin1").text("");
	}
	//-------------------------------phn check------------------------
	if(phn==''){
	$("#phn1").text("Please enter mobile number");
	error='1';
	}else{
	$("#phn1").text("");
	}
	//------------------------------add check-------------------------
	if(add==''){
	$("#add1").text("Please enter address");
	error='1';
	}else{
	$("#add1").text("");
	}
	//------------------------------city check------------------------
	if(city==''){
	$("#city1").text("Please enter city name");
	error='1';
	}else{
	$("#city1").text("");
	}
	//------------------------------state check-----------------------
	if(state==''){
	$("#state1").text("Please enter state name");
	error='1';
	}else{
	$("#state1").text("");
	}
	if(error=='0'){
	ShaAjaxJquary('/wag_club/wagtag/save_order.php?name='+name+'&pin='+pin+'&phn='+phn+'&add='+add+'&land='+land+'&city='+city+'&state='+state+'&citystate='+citystate+'', '#show', '', '', 'POST', '#show', '<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />', 'REP');
}
}
function checkpin(){
	pin=$("#pin").val();
 	ShaAjaxJquary('checkpincode.php?pin='+pin+'', '#ajaxdiv', '', '', 'POST', '#ajaxdiv', '<img src="/images/indicator.gif" width="16" height="16" alt="Loading" />', 'REP');
}
</script>
<script type="text/javascript">
 $(document).ready(function() {
	  $("#info").validate({
	});
	
});
</script>
<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>

<div class="dog_to_track_wt">
<div class="cont980">
<div class="wagTag_box">
<h1>Get a Unique Wag Tag for Bruno</h1>
<div class="wt_leftBox">
<div class="wt_logo">
  Wag Tag
</div>
<div class="wt_Imgs">

</div>
</div>

<div class="wt_rytBox">
<div class="wt_form" id="show">
<form id="info" name="info" method="post">
<h3>Contact Info</h3>
<ul>
<li>
<label>Name</label>
<input name="name" id="name" type="text"/>
<label id="name1" name="name1" style="color:#F00"></label>
</li>
<li>
<label>Pin Code</label>
<input name="pin" id="pin" type="text" onblur="checkpin()" onchange="checkpin()" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="6" class="required checkOut formField signin_input" />
<label id="pin1" name="pin1" style="color:#F00"></label>
</li>
<li>
<label>Mobile number</label>
<input name="phn" id="phn" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" type="text" maxlength="10"/>
<label id="phn1" name="phn1" style="color:#F00"></label>
</li>
<li>
<label>Address</label>
<textarea name="add" id="add" cols="" rows="" ></textarea>
<label id="add1" name="add1" style="color:#F00"></label>

</li>
<li>
<label>Landmark</label>
<input name="land" id="land" type="text"/>
</li>
<div id="loc">
<li>
<label>City</label>
<input name="city" id="city" type="text"/>
<label id="city1" name="city1" style="color:#F00"></label>
</li>
<li>
<label>State</label>
<input name="state" id="state" type="text"/>
<label id="state1" name="state1" style="color:#F00"></label>
</li>
</div>
</ul>
<div class="wt_btnBox">
<input type="button" onclick="saveorder()" value="Send Wag Tag" class="wt_canBtn wt_actBtn" />
<input type="reset" value="Cancel" class="wt_canBtn wt_actBtn" />
</div>
</form>
<div id="ajaxdiv"></div>
</div>
</div>
</div>
</div>
</div>