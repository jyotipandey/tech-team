<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wag-tag/css/wt_style.css" />
<title>Wag Club</title>

<?php require_once($DOCUMENT_ROOT . '/new/common/header-new.php');?>

<div class="wc_headerBar">
<div class="cont980">
<div class="wt_topNav">
<ul>
<li><a href="#">HOME</a></li>
<li><a href="#">WHAT IS WAGTAG?</a></li>
<li><a href="#">HOW IT WORKS?</a></li>
<li><a href="#">SAMPLE PROFILE</a></li>
<li><a href="#" class="wt_last">FAQ</a></li>
</ul>
</div>

<div class="wt_search">
<div class="wc_search " style="width: 200px;margin: 6px 5px 0 10px;">
<input id="search_name" type="text" placeholder="search by wag id">
</div>
<div class="wc_srchbtn" onclick="keyworddog()"><img src="/wag_club/images/search.png" width="16" height="16"></div>
</div>
</div>
</div>

<div class="cont980">
<div class="wt_faqSec">
<h2>faq</h2>
<ul class="wt_faqBox">
  <li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>Why does my dog need a wag tag?</label>
</div>
<div class="wt_ansTxt">
Every dog needs a wag tag to ensure its safety. A wag tag will help your pet to reunite with you in case of any unfortunate and unforeseen circumstances. The unique id on the tag will help the finder to contact you while maintaining confidentiality of your information. The call will be connected to DogSpot and they will reroute it to the rightful owner.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>How much does it cost?</label>
</div>
<div class="wt_ansTxt">
 The wag tag is worth 300 INR but you can avail it by getting a profile for your dog on Wag Clubow much.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>Do you charge any monthly fees for wag tag?</label>
</div>
<div class="wt_ansTxt">
No, there is no fee charged for using wag tag. There are no hidden subscriptions involved with this product.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>I want more than one tag?</label>
</div>
<div class="wt_ansTxt">
If you want more than one tag, then you will have to create individual profiles for each of your pet. Then you can generate wag id for each by placing a request.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>I have damaged the activation code that came with the product. What shall I do next?</label>
</div>
<div class="wt_ansTxt">
Every wag tag is unique in its way. The tag will be activated only with the activation code provided in the pack. If you have accidentally damaged the code then just place a request for a new tag and it shall be sent to you immediately.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>How secure is your data?</label>
</div>
<div class="wt_ansTxt">
Your data is absolutely secure and no one will be able to accsess it. In fact to ensure privacy and confidentiality of the owner the number is also not displayed on the tag. The call is routed to the rightful owner through DogSpot.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label> How will the finder contact me?</label>
</div>
<div class="wt_ansTxt">
The finder will call the number given on the tag and give in the unique wag tag id given on the tag and the call will be routed to the rightful owner.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>Is it an alternative to micro chipping?</label>
</div>
<div class="wt_ansTxt">
No, micro chipping and the tag are two different things. A wag tag allows the finder to contact you instantly, whereas in a microchip the individual needs a hand scanner that can scan the information on the microchip. Your pet will require a surgery for a microchip, whereas the tag can be easily attached to any of the commercially available dog collars.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label> I have a tag but do not have a profile on Wag Club?</label>
</div>
<div class="wt_ansTxt">
To activate your wag tag it is absolutely integral to have a profile of your dog on the wag club. Otherwise you cannot activate the tag.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>Benefits over Traditional tag?</label>
</div>
<div class="wt_ansTxt">
A traditional tag will display your information on the tag itself, whereas the wag tag will maintain the privacy for the owner and route the call through DogSpot. wag tag allows you to store your pet’s vital information and if required allows you to update it as well.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>How do I activate my tag?</label>
</div>
<div class="wt_ansTxt">
The tag is activated with the help of an activation code provided at the back of the product. You just need to create a profile for your dog. There is an activate tab on the page that has to be clicked  and you just have to enter the activation code provided in the pack and the tag is activated with all the required information.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>What happens if my dog losses my tag?</label>
</div>
<div class="wt_ansTxt">
if accidentally the tag is lost, you can always put in a request for a new tag. The moment we receive the request for the new tag, the older tag will be de activated.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>How many numbers can I add in my wag tag?</label>
</div>
<div class="wt_ansTxt">
You can add up to three numbers on your wag tag profile.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label> Are there any sizes to chose from?</label>
</div>
<div class="wt_ansTxt">
No there is a standard size for all your dogs, which suits and fits all.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>My friends wants a tag, can I give them mine?</label>
</div>
<div class="wt_ansTxt">
 No you cannot share or give your tag to anyone else as the tag has been generated with your personal details and your pets. If your friend wants one then he or she has to register their dog in
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label> What is the tag made of and how durable is it?</label>
</div>
<div class="wt_ansTxt">
The tag is made of durable quality stainless steel.
</div>
</li>

</ul>
</div>
</div>

<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>