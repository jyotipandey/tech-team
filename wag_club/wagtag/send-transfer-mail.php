<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

if($email_id && $dog_id && $userid !='Guest'){
$insrt_info=query_execute_row("INSERT INTO wag_tag_transfer (owner,new_owner_email,dog_id,status) VALUES ('$userid','$email_id','$dog_id','0')");
$seldetails=query_execute_row("SELECT * FROM dogs_available WHERE dog_id='$dog_id'");
$seldetails1=query_execute_row("SELECT * FROM wag_tag WHERE dog_id='$dog_id' AND userid='$userid'");
$seluserdet=query_execute_row("SELECT * FROM users WHERE userid='$userid'");
$seluserphn=query_execute_row("SELECT phone_num FROM wagtag_phone_verify WHERE userid='$userid' AND wag_id='$seldetails1[wag_id]' AND type='main' AND dog_id='$dog_id'");$seluserphn1=query_execute_row("SELECT phone_num FROM wagtag_phone_verify WHERE userid='$userid' AND wag_id='$seldetails1[wag_id]' AND type='alt' AND dog_id='$dog_id'");

$old_owner_mail=useremail($userid);
//--------------------------------------------------------email send to new owner--------------------------------------------
	$toEmail=	$email_id;
	$mailSubject = "Transfer Wag Tag";
	// mail Body
	$mailBody .= '<p>Dear <strong>User </strong><br /><br />'.
    'If you want '.$seldetails['dog_name'].'\'s tag to be transferred on your name. Just follow the link and update the owner\'s information. <br />
    Link to accept the transfer .
    <a href="https://www.dogspot.in/dogs/'.$seldetails[dog_nicename].'/?transfer=1">https://www.dogspot.in/dogs/'.$seldetails['dog_nicename'].'/</a></p>
    <br>
	<p>If you are not registered on DogSpot, then follow the link to register and after registration click on the link above to complete the transfer process.<br />
	To register.
    <a href="https://www.dogspot.in/reg-log/register-1.php">Click Here</a></p>

<p>---------------------------------------------------------------------<br>
  DogSpot.in<br>
  Spot for all your Dog needs. The Store That Delivers at Your Door.<br>
  --------------------------------------------------------------------- </p>';		
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'From: DogSpot <contact@dogspot.in>' . "\r\n";
        $headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";

        
        $headers .= "Reply-To: DogSpot <contact@dogspot.in>\r\n";
        $headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
        $headers .= "Organization: DogSpot\r\n";        
		
        if(mail($toEmail, $mailSubject, $mailBody, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")) {
            //$registerFlag = 2;
			 $ArrayReturnEmail['errorFlag']= 2;
        
		    //  header("Location: /reg-log/more.php?userid=$cust_email");
        }else{
			$ArrayReturnEmail['errorFlag']= 1;
			$ArrayReturnEmail['errorText']= 'Internal Server Error 1 !';
           // $errorlogin = 1;
            //$errortext3 = "Internal Server Error 1 !";
            //header ("Location: /error.php?errortext=Internal Server Error!");
        }//Send Final Email xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//--------------------------------------------------------email send to old owner--------------------------------------------
	$toEmail1=	$old_owner_mail;
	$mailSubject1 = "Transfer Wag Tag";
	// mail Body
	$mailBody1 .= '<p>Dear <strong>'.dispUName($userid).' </strong><br /><br />'.
    'We have received a request to transfer the ownership of '.$seldetails['dog_name'].'\'s tag from '.dispUName($userid).'to '.$email_id.', <br />
    Once '.$email_id.' accepts the transfer. The tag will be transferred on his/her name   .</p>


<p>---------------------------------------------------------------------<br>
  DogSpot.in<br>
  Spot for all your Dog needs. The Store That Delivers at Your Door.<br>
  --------------------------------------------------------------------- </p>';		
	 // Send Final Email-------------------------------------------------------------------------------------------
	 // To send HTML mail, the Content-type header must be set
      //  $toEmail = '"' . $name . '" <' . $useremail . '>';
/*        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'From: DogSpot <contact@dogspot.in>' . "\r\n";
        $headers .= 'Bcc: DogSpot <contact@dogspot.in>' . "\r\n";

        
        $headers .= "Reply-To: DogSpot <contact@dogspot.in>\r\n";
        $headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
        $headers .= "Organization: DogSpot\r\n";*/
        
        if(mail($toEmail1, $mailSubject1, $mailBody1, $headers, "-f contact@dogspot.in mybounceemail@dogspot.in")) {
            //$registerFlag = 2;
			 $ArrayReturnEmail['errorFlag']= 2;
        
		    //  header("Location: /reg-log/more.php?userid=$cust_email");
        }else{
			$ArrayReturnEmail['errorFlag']= 1;
			$ArrayReturnEmail['errorText']= 'Internal Server Error 1 !';
           // $errorlogin = 1;
            //$errortext3 = "Internal Server Error 1 !";
            //header ("Location: /error.php?errortext=Internal Server Error!");
        }//Send Final Email xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

?>
<script type="text/javascript">
$("#second_transfer_div").css("display","block");
$("#first_transfer_div").css("display","none");
$("#transfer_done").css("display","block");
$("#transfer_pending").css("display","none");
</script>
<? } ?>