<?php
ini_set("post_max_size", "40M");
ini_set("upload_max_filesize", "15M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "99M");
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

?>
<style>
.wc-close-new-btn
{ background: none repeat scroll 0 0 #888;
    border-radius: 3px;
    color: #fff;
    margin-top: 5px;
    padding: 5px 15px;
	text-decoration:none;
	cursor:pointer;
}
.wc-close-new-btn:hover{text-decoration:none; color:#fff;}
.wc_margin-top{margin-top:0px!important;}
.share_box-border{ border:3px solid #ccc;}
</style>
<!-- share updates and images-->
<div class="share_img_updatesBlock share_box-border" id="popup_sharebox" >
<div class="share_img_updates">
<ul>
<li>
<i class="fa fa-edit" style="color:#000; padding-left:0px; padding-right:0px;" ></i> Share Updates
</li>
<!--<li><i class="fa fa-picture-o"></i> <a onclick="showimage()" style="cursor:pointer">Share Image</a></li>-->
</ul>
</div>
<!-- text-area-->
<div class="" id="sharetext">
<textarea placeholder="Enter your text" style="width: 632px; height: auto; margin: 0px; border-top: medium none; border-radius: 0px;font-size: 15px;" id="sharetextbox-popup"></textarea>

 <div class="share_btnbox" >
<input type="button" class="wagclub_sharebtn wc_margin-top" value="Share" onclick="sharepost('<?=$dog_owner ?>','<?=$dog_id?>','ror')";>
<a class="close wc-close-new-btn">Cancel</a>
</div>

</div>

<!-- text-area-->
</div>