<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');

$keywords = query_execute_row("SELECT * FROM `restrict_keywords` WHERE section = '$sectionname'");
$restrict_keywords = $keywords['keyword'];
$keywords_arr = explode(',', $restrict_keywords);

$count = 0;
foreach($keywords_arr as $restrict_words){
	$find_pos = stripos($tagtext, $restrict_words);	
	if ($find_pos === false){
		// Searched Keyword not found.		
	}else{
		$count = $count+1;	
	}
}

if($count > 0){ ?>
<script>
		document.getElementById("dogtagline").style.display = "block"; 
		document.getElementById("NextButton").style.display = "none"; 		
</script>
    <div class="loginScreen"> <span class="cancel" onclick="closepopup();">&times;</span>
     <div class="wag_popup_terms_cont">
     <h2>Please see below terms and conditions for Wag Club.</h2>
     <div class="wag_popup_terms_cont_text">Wag Club is a platform to discover and share things you love about dogs. It allows you to post content, including photos, videos, comments, and other materials. You agree to use Wag Club Products only in a manner consistent with its terms and conditions.</div>
     <div class="wag_popup_terms_cont_text">You agree not to post User Content that:</div>
    
     <ul>
     <li>Caters to buying, selling, breeding of dogs;</li>
     
    <li>Relates to subjects explicitly other than dogs or is clearly irrelevant to Wag Club;</li>
    <li>Contains any information or content we deem to be abusive, defamatory, harassing, harmful, hateful, humiliating to other people (publicly or otherwise), invasive of personal privacy or publicity rights, libelous, profane, racially or ethnically offensive, threatening, violent, or otherwise objectionable;</li>
 <li>Seeks to harm or exploit children by exposing them to inappropriate content, requesting personally identifiable details, or any other action;</li>
 <li>You shall not impersonate any other person on Wag Club. You will not malign or plagiarize or defame or otherwise sully any other person through posting on the Wag Club platform.</li>
 <li>Creates or may create a risk of harm, loss, physical or mental injury or illness, emotional distress, death, disability, or disfigurement to yourself, to any other person, or to any animal or which may create a risk of any other loss or damage to any person or property;</li>
     </ul>
     <div class="wag_popup_terms_cont_text_text">
     Wag Club reserves the right, but is not obligated, to remove any User Content for any reason or no reason, including User Content that Wag Club believes violates these terms and conditions.
     </div>
     

     
     <div class="wag_popup_terms_cont_text_textf">Wag Club may also permanently or temporarily terminate or suspend your account without notice and liability for any reason or no reason, including if Wag Club in its sole discretion determines that you have violated any provision of these terms and conditions.</div>
      </div>  
      </div>    
<? }else{ ?>
	<script>
		document.getElementById("dogtagline").style.display = "none"; 
		document.getElementById("NextButton").style.display = "block";
	</script>
<? }