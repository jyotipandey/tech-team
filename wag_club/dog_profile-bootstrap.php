<?php
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "10M");
ini_set("max_input_time", "300");
ini_set("max_execution_time", "300");
ini_set("memory_limit", "1512M");
include('../constants.php');
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT . '/arrays.php');
require_once($DOCUMENT_ROOT . '/dogs/arraybreed.php');
$user_ip = ipCheck();

$ajaxbody    = "dog";
$sitesection = "dogview";

// redirect it with 301 redirect to the page with /
makeProperURL($requestedUrl, $requested);
// redirect it with 301 redirect to the page with /

$dogDet            = dogDetailsNice($dog_nicename);
$dog_owner         = $dogDet['userid'];
$dogstatatus       = $dogDet["publish_status"];
$dog_id            = $dogDet['dog_id'];
$dog_name          = $dogDet['dog_name'];
$city              = $dogDet['city'];
$dog_sex           = $dogDet['dog_sex'];
$city_nicename     = $dogDet['city_nicename'];
$state             = $dogDet['state'];
$country_name      = $dogDet['country_name'];
$dog_image_profile = $dogDet['dog_image'];
$dog_image         = $dogDet['dog_image'];
$dog_name1         = str_replace('"', " ", $dogDet['dog_name']);

$dog_image1       = $dogDet['dog_image1'];
$dog_image2       = $dogDet['dog_image2'];
$dog_image3       = $dogDet['dog_image3'];
$dog_image4       = $dogDet['dog_image4'];
$dog_nicename     = $dogDet['dog_nicename'];
$dog_color        = $dogDet['dog_color'];
$day              = $dogDet['day'];
$month            = $dogDet['month'];
$year             = $dogDet['year'];
$dogs_desc        = $dogDet['dogs_desc'];
$tag              = $dogDet['tag'];
$wag              = $dogDet['wag'];
$publish_status   = $dogDet['publish_status'];
$image_margin_top = $dogDet['image_margin_top'];
$breed_nicename   = $dogDet['breed_nicename'];
$Dogbreed         = $ArrDogBreed[$breed_nicename];

$dogprofile          = $dog_owner;
$dog_name            = stripslashes($dogDet[dog_name]);
$dog_NAME            = breakLongWords($dog_name, 30, " ");
$user_email_transfer = useremail($userid);
//----------------------------------------wag tag code--------------------------------------------------
$sel_transfer = query_execute_row("SELECT * FROM wag_tag_transfer WHERE dog_id='$dog_id' AND status='0'");
if ($sel_transfer['new_owner_email'] != '') {
    $new_owner_email = $sel_transfer['new_owner_email'];
} else {
    $new_owner_email = "0";
}
//--------------------------------------------ends-------------------------------------------------------

if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
    //if($userid=='jyotimmmec'){echo $dog_owner.$sessionLevel;}
} else {
    if ($watch == 'yes' || $publish_status != 'publish') {
        header("Location: https://www.dogspot.in/login.php?refurl=/dogs/" . $dog_nicename);
    }
}
if (!$breed_nicename) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();
}

//--------------------------------------wag tag code-------------------------------------------------------
$active_tag = mysql_query("SELECT * FROM wag_tag WHERE dog_id='$dog_id' AND active_status='1'");
$hastag     = mysql_num_rows($active_tag);
if ($dog_owner == $userid) {
    $wagtagdet   = mysql_query("SELECT * FROM wag_tag WHERE dog_id='$dog_id' AND userid='$userid' AND active_status='1'");
    $countwagtag = mysql_num_rows($wagtagdet);
    $match       = '1';
}
if ($transfer && $userid == 'Guest') {
    header("Location: https://www.dogspot.in/login.php?refurl=/dogs/" . $dog_nicename . "&transfer=" . $transfer);
    exit();
}

//-----------------------------------------ends------------------------------------------------------------
if (!$dog_nicename) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();
}

if ($dog_sex == 'M') {
    $sex = 'his';
} else {
    $sex = 'her';
}
if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
    $summary12 = 'My ' . $dog_NAME . ' got ' . $sex . ' very own social profile on Wagclub. Does your dog have an exclusive social identity?';
} else {
    $summary12 = $dog_NAME . ' got ' . $sex . ' very own social profile on Wagclub. Does your dog have an exclusive social identity?';
}
$title12 = 'Wag Club';
$url12   = urlencode('https://www.dogspot.in/dogs/' . $dog_nicename);

$activities1 = mysql_query("SELECT DISTINCT da.image,da.activity1,da.id,da.user_activity FROM dogs_activity as da,dogs_available as das WHERE da.dog_id='$dog_id' AND da.dog_id=das.dog_id AND das.userid='$dog_owner' AND (da.publish_status='publish' OR das.userid='$userid' ) ORDER BY da.id DESC ");
$activities2 = mysql_query("SELECT DISTINCT da.image,da.activity1,da.id,da.publish_status,da.user_activity FROM dogs_activity as da,dogs_available as das WHERE da.dog_id='$dog_id' AND da.dog_id=das.dog_id AND das.userid='$dog_owner' AND (da.publish_status='publish' OR das.userid='$userid' ) ORDER BY da.id DESC LIMIT 0,3 ");

$selbreedid = query_execute_row("SELECT breed_id FROM dog_breeds WHERE nicename='$breed_nicename'");

$selsize2 = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='" . $selbreedid[breed_id] . "' AND att_id='41'");
$selsize  = $selsize2['value'];
if ($selsize == '193') {
    $selsize1 = '2';
} else if ($selsize == '194') {
    $selsize1 = '3';
} else if ($selsize == '195') {
    $selsize1 = '5';
} else {
    $selsize1 = '6';
}
$t_total = mysql_num_rows($activities1);
if ($t_total != 0) {
    $all_photos = 0;
}
if (!$sprice0) {
    $sprice0 = 1;
}
if (!$sprice) {
    $sprice = 1;
}
if (!$sprice1) {
    $sprice1 = 1;
}
if (!$sprice2) {
    $sprice2 = 1;
}
if (!$sprice3) {
    $sprice3 = 1;
}
if (!$sprice4) {
    $sprice4 = 1;
}
if (!$sprice5) {
    $sprice5 = 1;
}
$size        = getimagesize($DOCUMENT_ROOT . "/dogs/images/" . $dog_image);
$imageinfo   = $size[3];
$width       = explode(" ", $imageinfo);
$img_width   = explode('"', $width[0]);
$img_height  = explode('"', $width[1]);
$imagewidth  = $img_width[1];
$imageheight = $img_height[1];

if ($imagewidth < 200 || $imageheight < 200) {
    
    $dest = imagecreatefromjpeg('/home/dogspot/public_html/wag_club/images/white-bg.jpg');
    if ($ext == 'jpeg' || $ext == 'jpg') {
        $src = imagecreatefromjpeg('/home/dogspot/public_html/dogs/images/340x300-' . $dog_image);
    } elseif ($ext == 'png') {
        $src = imagecreatefrompng('/home/dogspot/public_html/dogs/images/340x300-' . $dog_image);
        
    } else {
        $src = imagecreatefromgif('/home/dogspot/public_html/dogs/images/340x300-' . $dog_image);
        
    }
    imagealphablending($dest, false);
    imagesavealpha($dest, true);
    
    imagecopymerge($dest, $src, 30, 30, 10, 10, 400, 400, 100);
    //imagecopymerge ( resource $dst_im , resource $src_im , int $dst_x , int $dst_y , int $src_x , int $src_y , int $src_w , int $src_h , int $pct )//have to play with these numbers for it to work for you, etc.
    
    //header('Content-Type: image/jpeg');
    imagepng($dest, "/home/dogspot/public_html/dogs/images/340x300-" . $dog_image);
    
    imagedestroy($dest);
    imagedestroy($src);
    $imgface = "/dogs/images/340x300-" . $dog_image;
?>
<?
    
    
} else {
 $imgface = "/dogs/images/340x300-" . $dog_image;
}

$dogDet[dog_color] = preg_replace('/\"/', ' ', $dogDet[dog_color]);
$dogDet[dog_name]  = preg_replace('/\"/', ' ', $dogDet[dog_name]);
$sitesection="dogview";
$ant_section = 'wag club';
$ant_page = 'waghome';
$title=dispUname($dog_owner)." 's ".$dogDet['dog_name'].' | '. $Dogbreed.'|'.' Dogs in'. $city.'|'.$dog_id.'|'.' DogSpot';
$keyword=dispUname($dog_owner)." 's ".$dogDet['dog_name'].' | '. $Dogbreed.'|'.' Dogs in'. $city.'|'.$dog_id.'|'.' DogSpot';
$desc=dispUname($dog_owner)." 's ".$dogDet['dog_name'].' | '.$summary12;
    $alternate="http://m.dogspot.in/$section[0]/";
	$canonical="https://www.dogspot.in/$section[0]/";
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in" . $imgface;
?>
<meta name="robots" content="noindex, nofollow">
<?php require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/wagclub.css?v936" />
<script>$(function() {
        $( "#slider-range" ).slider({
            range: "min",
            min:1,
            max: 10,
            value: <?= $sprice ?>,
            slide: function( event, ui ) {
                $( "#sprice" ).val(  ui.value );
				
            }
		});	
        $( "#sprice" ).val(  $( "#slider-range" ).slider( "value") );
           
		
		
		
		 $( "#slider-range1" ).slider({
            range: "min",
            min:1,
            max: 10,
            value: <?= $sprice1 ?>,
            slide: function( event, ui ) {
                $( "#sprice1" ).val(ui.value );
				
            }
		});
        $( "#sprice1" ).val( $( "#slider-range1" ).slider( "value") );
           
		
			
		
		  $( "#slider-range2" ).slider({
            range: "min",
            min:1,
            max: 10,
            value: <?= $sprice2 ?>,
            slide: function( event, ui ) {
                $( "#sprice2" ).val(  ui.value );
				
            }
		});
        $( "#sprice2" ).val( $( "#slider-range2" ).slider( "value") );
           
		
			
		
		  $( "#slider-range3" ).slider({
            range: "min",
            min:1,
            max: 10,
            value: <?= $sprice3 ?>,
            slide: function( event, ui ) {
                $( "#sprice3" ).val( ui.value );
				
            }
		});
        $( "#sprice3" ).val( $( "#slider-range3" ).slider( "value") );
           
		
			
		
		  $( "#slider-range4" ).slider({
            range: "min",
            min:1,
            max: 10,
            value: <?= $sprice4 ?>,
            slide: function( event, ui ) {
                $( "#sprice4" ).val( ui.value );
				
            }
		});
        $( "#sprice4" ).val($( "#slider-range4" ).slider( "value") );
           
		
			
		
		  $( "#slider-range5" ).slider({
            range: "min",
            min:1,
            max: 10,
            value: <?= $sprice5 ?>,
            slide: function( event, ui ) {
                $( "#sprice5" ).val( ui.value );
				
            }
		});
        $( "#sprice5" ).val( $( "#slider-range5" ).slider( "value") );
           
				
		$( "#slider-range0" ).slider({
            range: "min",
            min:1,
            max: 10,
            value: <?= $sprice0 ?>,
            slide: function( event, ui ) {
                $( "#sprice0" ).val(  ui.value );
				
            }
		});	
        $( "#sprice0" ).val(  $( "#slider-range0" ).slider( "value") );	
		 });
		 
  $(function() {
    $( "#datepicker" ).datepicker();
  });
   $(function() {
    $( "#datepicker1" ).datepicker();
  });
   $(function() {
    $( "#datepicker2" ).datepicker();
  });
   $(function() {
    $( "#datepicker3" ).datepicker();
  });
   $(function() {
    $( "#datepicker4" ).datepicker();
  });
   $(function() {
    $( "#datepicker5" ).datepicker();
  });
   $(function() {
    $( "#datepicker6" ).datepicker();
  });
   $(function() {
    $( "#datepicker7" ).datepicker();
  });
 
function submitcharacter()
{
var price0=$('#sprice0').val();
var price=$('#sprice').val();
var price1=$('#sprice1').val();
var price2=$('#sprice2').val();
var price3=$('#sprice3').val();
var price4=$('#sprice4').val();
var price5=$('#sprice5').val();
var dog_id=$('#dog_id').val();
//alert('fsaf');
ShaAjaxJquary('/wag_club/save_character.php?price0='+price0+'&price='+price+'&price1='+price1+'&price2='+price2+'&price3='+price3+'&price4='+price4+'&price5='+price5+'&dog_id='+dog_id+'&feature=character', '#aftersubmit', '', '', 'POST', '#aftersubmit', '<img src="/images/crazy_dog_running.gif">update...', 'REP');	
}	
function submitmedical()
{
var spay=$('#spay').val();
var Disease=$('#Disease').val();
var allergies=$('#allergies').val();
var surgeries=$('#surgeries').val();
var Vet=$('#Vet').val();
var Location=$('#Location').val();
var datepicker=$('#datepicker').val();
var datepicker1=$('#datepicker1').val();
var datepicker2=$('#datepicker2').val();
var datepicker3=$('#datepicker3').val();
var datepicker4=$('#datepicker4').val();
var datepicker5=$('#datepicker5').val();
var datepicker6=$('#datepicker6').val();
var datepicker7=$('#datepicker7').val();
var dog_id=$('#dog_id').val();
//alert('fsaf');
ShaAjaxJquary('/wag_club/save_character.php?spay='+spay+'&Disease='+Disease+'&allergies='+allergies+'&surgeries='+surgeries+'&Vet='+Vet+'&Location='+Location+'&datepicker='+datepicker+'&datepicker1='+datepicker1+'&dog_id='+dog_id+'&feature=medical&datepicker2='+datepicker2+'&datepicker3='+datepicker3+'&datepicker4='+datepicker4+'&datepicker5='+datepicker5+'&datepicker6='+datepicker6+'&datepicker7='+datepicker7+'', '#medicaloption', '', '', 'POST', '#medicaloption', '<img src="/images/crazy_dog_running.gif">update...', 'REP');	
}	 
function onedit()
{
var price0=$('#sprice0').val();
var price=$('#sprice').val();
var price1=$('#sprice1').val();
var price2=$('#sprice2').val();
var price3=$('#sprice3').val();
var price4=$('#sprice4').val();
var price5=$('#sprice5').val();
var dog_id=$('#dog_id').val();
//alert('fsaf');
ShaAjaxJquary('/wag_club/character.php?sprice0='+price0+'&sprice='+price+'&sprice1='+price1+'&sprice2='+price2+'&sprice3='+price3+'&sprice4='+price4+'&sprice5='+price5+'&dog_id='+dog_id+'', '#aftersubmit', '', '', 'POST', '#aftersubmit', '<img src="/images/crazy_dog_running.gif">update...', 'REP');
}

		</script>
<div id="fb-root"></div>
<script type="text/javascript" >
$(document).ready(function() {
	
      $('#vasPhoto_uploads1').on('change', function() {
		   $("#vasPLUS_Programming_Blog_Form2").vPB({
            beforeSubmit: function() {
                  $("#vasPhoto_uploads_Status1").show();
				     //$("#crop_img1").hide();
					 $("#crop_img21").show();
	
                  $("#vasPhoto_uploads_Status1").html('');
                  $("#vasPhoto_uploads_Status1").html('<div style="" align="center"><font style="font-family: Verdana, Geneva, sans-serif; font-size:12px; color:black;">Upload</font> <img src="https://www.dogspot.in/images/indicator.gif" alt="Upload...." align="absmiddle" title="Upload...."/></div><br clear="all">');
            },
			//alert('cs2');
		
            url: '/wag_club/activity_dog_image.php',
            success: function(response) {
				 $("#submitspan1").hide();
                  $("#vasPhoto_uploads_Status1").html($(response).fadeIn(2000));
            }
      }).submit();
      }); 
}); 
</script> 
<script type="text/javascript" >
$(document).ready(function() {
	//alert('cs');
	var dog_nicename=$("#dog_nicename").val();
      $('#vasPhoto_uploads').on('change', function() {
           $("#vasPLUS_Programming_Blog_Form").vPB({
            beforeSubmit: function() {
                  $("#vasPhoto_uploads_Status").show();
				     //$("#crop_img1").hide();
					 $("#crop_img2").show();
					 
	//alert('cs1');
	//alert('in')
	

                  $("#vasPhoto_uploads_Status").html('');
                  $("#vasPhoto_uploads_Status").html('<div style="" align="center"><font style="font-family: Verdana, Geneva, sans-serif; font-size:12px; color:black;">Upload</font> <img src="https://www.dogspot.in/images/indicator.gif" alt="Upload...." align="absmiddle" title="Upload...."/></div><br clear="all">');
            },
			//alert('cs2');
			
		
            url: '/wag_club/activity_dog_image.php',
            success: function(response) {
				 $("#submitspan").hide();
// ShaAjaxJquary("/wag_club/add-activity.php?profilechange=1&dog_nicename="+dog_nicename+"&image="+image+"&dog_insert_id="+dog_id+"&user="+user_id+"", '#show', '',  '', 'post', '', '','REP');
                  $("#vasPhoto_uploads_Status").html($(response).fadeIn(2000));
            }
      }).submit();
      }); 
}); 
function sleep(milliseconds,act) {
	var dog_nicename=$("#dog_nicename").val();
	var user_id=$("#user_id").val();	
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
	
      break;
    }

	//alert(act);
	window.location.href="https://www.dogspot.in/wag_club/activity/"+act+"/";

  }
	
		//var act_id=$("#act_id").val();
//window.location.href="https://www.dogspot.in/activity/"+act_id+"/"+dog_nicename+"/";
}
</script> 
<script type="text/javascript">
<!---Delete Post--->
function remove_post(postactivity,postno){
	if(postactivity != ''){
		ShaAjaxJquary('/wag_club/delete-post-ajax.php?postactivity='+postactivity+'&postno='+postno+'', '#'+postno, '', '', 'POST', '', '', 'REP');	
	}
}
<!---Delete Post--->

<!------------------------------------------------------------wag tag code------------------------------------------

function transfer_action(type){
		var dog_nicename=$("#dog_nicename").val();
		var dog_id=$("#dog_id").val();
		var new_email=$("#new_owner_email").val();
		if(dog_nicename){
		ShaAjaxJquary('/wag_club/wagtag/transfer_dog.php?dog_nicename='+dog_nicename+'&dog_id='+dog_id+'&new_email='+new_email+'&type='+type+'', '#transfer_div', '', '', 'POST', '#transfer_div', '', 'REP');
		setTimeout(transfer_redirect, 1000);
}
}
function transfer_redirect(){
location.reload();
}
function wagtag(){
var dogname=$("#dog_name").val();
var dogid=$("#dog_id").val();
ShaAjaxJquary('/wag_club/wagtag/slider/add_dog.php?dog_id='+dogid+'&dog_name='+dogname+'', '#wagtagdiv', '', '', 'POST', '#wagtagdiv', '', 'REP');
}
function managetag(){
	document.getElementById("wag_tag_div").style.display="block";
		moveToFooter('show_breed_dg');
}

<!--------------------------------------------------------------ends------------------------------------------------
var cat_nice='';
 $(document).ready(function() { 
 //alert('233');
 var dog_id=$("#dog_id").val();
	var dog_owner=$("#dog_owner").val();
 cat_nice=document.getElementById('txt1').innerHTML;
 var c=0;
 countr=document.getElementById('txt2').innerHTML;
//alert ( $(".imgtxtcontwag:last").attr('id'));
if(countr!='0' && dog_id !='' && dog_owner!=''){
$(window).data('ajaxready', true).scroll(function(e) {
    if ($(window).data('ajaxready') == false) return;
	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	
	if(c!=2){
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/wag_club/loadmore1_test1.php?dog_id="+dog_id+"&dog_owner="+dog_owner+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();c=c+1;
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajex close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<a href='javascript:void(0);' onclick=get("+e12+")><div id='divw"+e12+"' class='divw"+e12+ " ViewMore'  >See More </div></a>");
		
	c=0;}$('#loadMoreComments').hide();
	} // c condition close
	}

	});
}
	});
</script> 
<script>
function get(rt){
	var dog_id=$("#dog_id").val();
	var dog_owner=$("#dog_owner").val();
	//alert('fdwef');
	$('#divw'+rt).hide();
	c=0;

	
   if(($(document).height() - $(window).height()) - $(window).scrollTop() < $('#footerContent').height()) {	
	$('#loadMoreComments').show();
	$(window).data('ajaxready', false);
	<!--
	if(c!=3){
	
	$.ajax({
	cache: false,
	dataType : "html" ,
	contentType : "application/x-www-form-urlencoded" ,
	url: "/wag_club/loadmore1_test1.php?dog_id="+dog_id+"&dog_owner="+dog_owner+"&lastComment="+ $(".imgtxtcontwag:last").attr('id') ,
	data: {cat_nicename:cat_nice} ,
	success: function(html) {
	if(html){		
	$("#rytPost_list").append(html);
	$('#loadMoreComments').hide();
	c=c+1; 
	}else {
    $('#loadMoreComments').html();
	}
    $(window).data('ajaxready', true);
	
	}
	}); // ajax close
   	}
	else{e12=  $(".imgtxtcontwag:last").attr('id');
		if(e12!=countr){
		$("#rytPost_list").append("<div id='divw"+e12+"' class='divw"+e12+ " ViewMore' style='cursor:pointer' onclick=get("+e12+")>load more..</div>");
		c=0;}$('#loadMoreComments').hide();
	
	} // c condition close-->
	}
	
	}

</script> 
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1677990482530277";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<style>
.ViewMore {
    background: none repeat scroll 0 0 #eee; 
	border:1px solid #999;
	border-radius: 5px;
    color: #000;
	font-size:12px;
	font-weight:bold;
    float: left;
    margin: 20px 0 10px;
    padding: 10px 0;
    text-align: center;
    width: 74%;
	margin-right: 27px;
	margin-left: 92px;
}
.wag-club-delete-post
{
 font-size: 16px; 
 display: block; 
 overflow: hidden; 
 float: left; 
 width: 87%; 
 border-radius: 4px; 
 padding-bottom: 5px; 
 margin-top: 15px; 
 padding-top: 5px; 
 text-align: center; 
 margin-bottom: 5px; 
 background:#fce7e7; 
 color:#b00; 
 border: 1px solid #b00;
}
.wc_profile_activity{ width:100%; margin:auto; overflow:hidden;}
.wc_profile_btns{ width:49%; float:left;}
.dog-profile-bg{ overflow:hidden;}
.name_box_dg{border-bottom: 0px !important; text-shadow: 4px 1px 0px #000; text-transform: capitalize; margin-bottom: 0px !important;
    padding-bottom: 0px !important;}
	
#beforewag .dg_wag_no{margin-left: 60px; right:inherit !important;}
.open_detailBox_dg ul li{ text-align:left;}	

/*new css*/
.wc-dialog .modal-content{
    border: 7px solid rgba(170, 170, 170, 0.73);}
.wc-dialog  .modal-header{    background: #999;
    color: #fff;}
.wc-dialog  .modal-header .close{opacity: 1; color:#fff;}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
       width: 100%;
    max-width: 250px;
    height: auto;
    margin: auto;
    text-align: center;
	
    margin-top: 20px
}
#fileselector {
    margin: 10px; 
}
#upload-file-selector {
    display:none;   
}
.margin-correction {
    margin-right: 10px;   
}
.wc-dialog .btn-uplaod {width: 100%;
    background: #f4f4f4;
    padding: 10px;}
.wc-dialog .modal-content{ float:left; width:100%;}

/*new css end*/
</style>
<script>
function cancelpic(valpic)
{
	$('#'+valpic).hide();
	
	if(valpic=='crop_img21')
	{
		$('#submitspan1').show();
		$('#vasPhoto_uploads1').val('');
		$('#vasPhoto_uploads_Status1').text('');
	}else
	{ $('#submitspan').show();
	var inserted_id=$("#inserted_id").val();
		$('#vasPhoto_uploads').val('');
		$('#vasPhoto_uploads_Status').text('');
if(inserted_id){
ShaAjaxJquary("/wag_club/add-activity.php?deletetag=1&inserted_id="+inserted_id+"", '#show', '',  '', 'post', '', '','REP');
}
	}
	//alert('ji');
	$('#wc_changeMenu').hide();
	$('#mask , .login-popup').fadeOut(300 , function() {
	$('#mask').remove();
	});
}
</script> 
 
 
<script>
	$(document).ready(function() {
$('a.login-window1').click(function() {
var loginBox = $(this).attr('href');
$(loginBox).fadeIn(300);
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 20) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
});
$('a.close, #mask').on('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});
</script> 
 
 
 
 
<script>
function get_repo_state() {
        $('#shoppro').addClass('profile_cover');
		$('#imagepicchange').css("cursor","move");
		//$('#shoppro7').addClass('profile_cover');
		$('#wc_chagePic').hide();
		$("#imagepicchange").css('margin-top', "0px");
		//$('#shoppro7').addClass('profile_cover');
       // $('.repo').html('save').attr('onclick', "save_repo()");
	   $('#drgtorepo').show();
	   $('#imagesubmit').show();
	   $('#imagesubmit1').show();
        var y1 = $('.profile_cover').height();
        var y2 = $('.profile_cover img').height();
        $(".profile_cover img").draggable({
            scroll: false, 
            axis: "y",
            drag: function(event, ui) {
                if (ui.position.top >= 0) {
                    ui.position.top = 0;
                } else if(ui.position.top <= y1 - y2) {
                    ui.position.top = y1 - y2;
                }
            }            
       });
       
    }
	function get_repo_state1() {
        $('#shoppro7').addClass('profile_cover');
		
	   $('#shoppro5').css("cursor","move");
		//$('#shoppro7').addClass('profile_cover');
		$('#wc_chagePic').hide();
		$("#shoppro5").css('margin-top', "0px");
		//$('#shoppro7').addClass('profile_cover');
       // $('.repo').html('save').attr('onclick', "save_repo()");
	   $(".reposbutton1").css("display","block");
	   $('#drgtorepo').show();
	  // $('#imagesubmit').show();
	 
        var y1 = $('.profile_cover').height();
        var y2 = $('.profile_cover img').height();
		
		
		if(y2<700)
		{
			y2='700';
		}
        $(".profile_cover img").draggable({
            scroll: false, 
            axis: "y",
            drag: function(event, ui) {


                if (ui.position.top >= 0) {
                    ui.position.top = 0;
                } else if(ui.position.top <= y1 - y2) {
                    ui.position.top = y1 - y2;
                }
            }            
       });
       
    }
	</script> 
<script>
	function submitdata()
	{
		var dog_nicename=$("#dog_nicename").val();
		var dog_id=$("#dog_id").val();
		//var uploadimage=$("#uploadimage").val();
		var inserted_id=$("#inserted_id").val();
var tagline2=$("#tagline2").val();
var user_id=$("#user_id").val();

		if(dog_nicename!='' )
		{

$("#error").css("display","none");
$("#crop_img21").css("display","none");

		ShaAjaxJquary("/wag_club/add-activity.php?acttag=1&inserted_id="+inserted_id+"&tagline="+tagline2+"&dog_insert_id="+dog_id+"", '#show', '',  '', 'post', '', '','REP');
		//window.location.href="https://www.dogspot.in/dogs/"+dog_nicename;
	sleep(3000,inserted_id);
	
		}else
{
$("#error").css("display","block");
}
		
		
	}
	function submitpic()
	{
		var dog_nicename=$("#dog_nicename").val();
		var dog_id=$("#dog_id").val();
		var user_id=$("#user_id").val();
		var image=$("#image").val();
		//alert('fdsfs');
		if(dog_nicename!='' && image!='' && typeof image!='undefined')
		{
			//alert('welcoe');
			$("#crop_img21").css("display","none");
		ShaAjaxJquary("/wag_club/change-profile.php?profilechange=1&dog_nicename="+dog_nicename+"&image="+image+"&dog_insert_id="+dog_id+"&user="+user_id+"", '#show', '',  '', 'post', '', '','REP');
		
	
		}	
		
	}
	
function dg_show(did)
{
	//alert(did);
	if(did=='open_detail_dg'){
	//	alert('sfdsf');
	$("#open_detail_dg").hide();
	$("#open_detailBox_dg").show();
	//$("#open_detailBox_dg").show("slide",{direction:'top'});
    $("#open_detailBox_dg").show();
	}
	if(did=='open_detail_dg1')
	{
	$("#open_detailBox_dg").hide();
    $("#open_detail_dg").show();
	}
	
}

function showpix()
{
	
	$("#shoppro").css("display","none");
	$("#before").css("display","block");
	$("#imagesubmit").css("display","block");
	$("#imagesubmit1").css("display","block");
	$("#checktop").css("display","block");
	$("#mask2").css("height","800");
	$("#changediv").css("display","none");

}

function changepic()
{
	var wu_menu=$("#wc_changeMenu").css("display");
	if(wu_menu=='none'){
	$("#wc_changeMenu").css("display","block");
	}else{
		$("#wc_changeMenu").css("display","none");
	}
}




function selectoption(val)
{


	var dog_id=$("#dog_id").val();
	var wu_menu=$("#wc_changeMenu").css("display");
	if(wu_menu=='none'){
	$("#wc_changeMenu").css("display","block");
	}else{
		
		$("#wc_changeMenu").css("display","none");
	}
	//alert(val);
	if(val=='remove')
	{
		var con=confirm('Are you really want remove the profile pic');
		
		if(con==true)
		{
			//alert('fewf');
		ShaAjaxJquary("/wag_club/remove_profile.php?dog_id="+dog_id+"&update=1", "#show", '',  '', 'POST', "", '','REP');	
	}else
	{
		
		
	}
	}
	
}
function change_cover(url)
{
	var dog_id=$("#dog_id").val();
	//alert(url);
	var image=url.split('-');
	//alert(image[2]);
ShaAjaxJquary("/wag_club/change-profile.php?profilechange=1&image="+image[2]+"&dog_insert_id="+dog_id+"", '#show', '',  '', 'post', '', '','REP');	
}
</script> 
<script>
function oneditmedical()
{
//	alert('sdasd');
var spay=$('#spay').val();
var Disease=$('#Disease').val();
var allergies=$('#allergies').val();
var Vet=$('#Vet').val();
var Location=$('#Location').val();
var datepicker=$('#datepicker').val();
var datepicker1=$('#datepicker1').val();
var datepicker2=$('#datepicker2').val();
var datepicker3=$('#datepicker3').val();
var datepicker4=$('#datepicker4').val();
var datepicker5=$('#datepicker5').val();
var datepicker6=$('#datepicker6').val();
var datepicker7=$('#datepicker7').val();
var dog_id=$('#dog_id').val();
//alert('fsaf');
ShaAjaxJquary('/wag_club/character.php?spay='+spay+'&Disease='+Disease+'&allergies='+allergies+'&Vet='+Vet+'&Location='+Location+'&datepicker='+datepicker+'&datepicker1='+datepicker1+'&dog_id='+dog_id+'&feature=medical&datepicker2='+datepicker2+'&datepicker3='+datepicker3+'&datepicker4='+datepicker4+'&datepicker5='+datepicker5+'&datepicker6='+datepicker6+'&datepicker7='+datepicker7+'', '#medicaloption', '', '', 'POST', '#medicaloption', '<img src="/images/crazy_dog_running.gif">update...', 'REP');	
}
		</script> 
<script> function confirmDelete1(review,url)
{
	//alert('fdsfsd');
ShaAjaxJquary("/wag_club/review_delete.php?delete=1&review_id="+review+"&reUrl="+url+"", '#max', '',  '', 'post', '', '','REP');	
}</script> 
<script language="javascript">
function validateComment(activity_id){
      
	 // alert('fdsfs');
	 // alert(activity_id);
  var divapend = $("#sitemcount").val();      
               var review_body= $('#review_body'+activity_id).val();
              // alert(review_body);
			  $('#review_body'+activity_id).val('');
			  if(review_body!='' && typeof review_body!='Undefined'){
               ShaAjaxJquary("/wag_club/checkcomment.php?review_body="+review_body+"&activity_id="+activity_id+"", "#mis"+activity_id, "#"+fName, '', 'POST', '#loading', '<img src="/images/indicator.gif" />','APE');
			  }
       
               //document.rytPost_list.review_title.value = ""        }
               
}
</script> 
<script> function confirmDelete2(review,url,activity_id)
{
	//alert('23');
	
var user=$("#user_id").val();
//alert(user);
var viewmore=$("#viewmore"+activity_id).text();
//alert(viewmore);
if(viewmore!='')
{
	var view=1;
}else
{
	var view=2;
}
ShaAjaxJquary("/wag_club/review_delete.php?ram=1&review_id="+review+"&reUrl="+url+"&userid="+user+"&view="+view+"", "#aftercmntq"+activity_id, '',  '', 'post', '#loading', '<img src="/images/indicator.gif" />','APE');	
}</script> 
<script language="javascript">
function validateComment1(activity_id,maxreview){
      
	 // alert('fdsfs');
	 // alert(activity_id);
              $('#loadingdiv'+activity_id).show();                        
              var review_body1= $('#review_body'+activity_id).val();
              // alert(review_body);
			var review_body=$.trim(review_body1);
			  $('#review_body'+activity_id).val('');
			  if(review_body!='' && typeof review_body!='Undefined'){
     ShaAjaxJquary("/wag_club/checkcomment.php?review_body="+review_body+"&activity_id="+activity_id+"", "#aftercmntq"+activity_id, '', '', 'POST', '#loading', '<img src="/images/indicator.gif" />','APE');
			  }
      // document.getElementById('review_body').value() = ""
               //document.rytPost_list.review_title.value = ""        }
    // $('#loadingdiv'+activity_id).hide();            
}
</script> 
<script>
$(function () {
 
 var msie6 = $.browser == 'msie' && $.browser.version < 7;
 
 if (!msie6) {
   var top = $('#show_breed_dg').offset().top - parseFloat($('#show_breed_dg').css('margin-top').replace(/auto/, 0));
   $(window).scroll(function (event) {
     // what the y position of the scroll is
     var y = $(this).scrollTop();
     
     // whether that's below the form
     if (y >= top) {
       // if so, ad the fixed class
       $('#show_breed_dg').addClass('fixed');
     } else {
       // otherwise remove it
       $('#show_breed_dg').removeClass('fixed');
     }
   });
 }  
});
 
</script> 
<script>
function moveToFooter(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top},
        'slow');
}
function wagfuncount()
{
var user=$("#user_id").val();
	var dog_id=$("#dog_id").val();
 ShaAjaxJquary("/wag_club/wag_insert.php?user="+user+"&dog_id="+dog_id+"", "#wag", '',  '', 'POST', "", '','REP')
}
function wagfuncount2()
{
var user=$("#user_id").val();
	var dog_id=$("#dog_id").val();
 ShaAjaxJquary("/wag_club/wag_insert.php?image_wag=1&user="+user+"&dog_id="+dog_id+"", "#wag", '',  '', 'POST', "", '','REP')
}
function wagfuncount1(activity_id)
{
var user=$("#user_id").val();
	var dog_id=$("#dog_id").val();
	//alert(user);
	//alert(dog_id);
 ShaAjaxJquary("/wag_club/wag_insert.php?activity_wag=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
}
function wagcount()
{
var user=$("#user_id").val();
var dog_id=$("#dog_id").val();
ShaAjaxJquary("/wag_club/wag_insert.php?user="+user+"&dog_id="+dog_id+"&update=1", "#wag", '',  '', 'POST', "", '','REP')
}
function wagcount1(activity_id)
{
var user=$("#user_id").val();
var dog_id=$("#dog_id").val();

ShaAjaxJquary("/wag_club/wag_insert.php?activity_wag_delete=1&user="+user+"&dog_id="+dog_id+"&activity="+activity_id+"", "#wag"+activity_id, '',  '', 'POST', "", '','REP')
}
function wagcount2()
{

var user=$("#user_id").val();
var dog_id=$("#dog_id").val();

ShaAjaxJquary("/wag_club/wag_insert.php?image_wag_delete=1&user="+user+"&dog_id="+dog_id+"", "#wag", '',  '', 'POST', "", '','REP')
}

</script> 
<script>
$(document).ready(function(e) { 
 $("#open_detailBox_dg").show();
$("#open_detail_dg").hide();
});
</script> 
<script>
function dg_show(did)
{
	//alert(did);
	if(did=='open_detail_dg'){
	//	alert('sfdsf');
	$("#open_detail_dg").hide();
	$("#open_detailBox_dg").show();
	//$("#open_detailBox_dg").show("slide",{direction:'top'});
    $("#open_detailBox_dg").show();
	}
	if(did=='open_detail_dg1')
	{
	$("#open_detailBox_dg").hide();
    $("#open_detail_dg").show();
	}
	
}
function save_repo(val1) {
	
        var pos = $("#"+val1+" img").css('top');
		if(val1=='shoppro'){
				       $(".reposbutton").css("display","none");
				}else
				{
					$(".reposbutton1").css("display","none");
				}
		
	   var dog_id=$("#dog_id").val();
       // $('#loading').html('<img src="<?php //echo base_url(); 
?>images/indicator.gif" />');
     $.ajax({
           type : "POST",
            url: "/wag_club/change-top.php?updatechange=1&dog_insert_id="+dog_id+"&top="+pos,
            success: function() {
                $('#'+val1+' img').draggable("destroy");
				
$('#wc_chagePic').show();
		$('#wc_changeMenu').hide();
		$('#drgtorepo').hide();
		$('#imagepicchange').css("cursor","auto");
		$('#shoppro5').css("cursor","auto");
               // $('.repo').html('Reposition').attr('onclick', "get_repo_state()");
                $('#'+val1+'').removeClass('profile_cover');
               // $('#loading').hide();
            }
        });
    }
 </script> 
<script>

function nothingcrop1(val1)
{
 $('#'+val1+' img').draggable("destroy");
 //ShaAjaxJquary("/wag_club/wag_insert.php?checkupdate=1&dog_id="+dog_id+"", "#check", '',  '', 'POST', "", '','REP')
				      if(val1=='shoppro'){
				       $(".reposbutton").css("display","none");
				}else
				{
					$(".reposbutton1").css("display","none");
				}
				
$('#drgtorepo').hide();
$("#shoppro7").css("display","none");
$("#repo1").css("display","none");
$("#repo").css("display","block");
$("#shoppro").css("display","block");
$("#shoppro").css("top","0px");
$('#imagepicchange').css("cursor","auto");
$('#imagepicchange').css("top","0");
$('#shoppro5').css("top","0");
		$('#shoppro5').css("cursor","auto");

   var dog_id=$("#dog_id").val();
 
   $('#wc_chagePic').show();
		$('#wc_changeMenu').hide();

               // $('.repo').html('Reposition').attr('onclick', "get_repo_state()");
                $('#'+val1+'').removeClass('profile_cover');
				ShaAjaxJquary("/wag_club/ad_dog_activity.php?Noprofilechange=1&dog_insert_id="+dog_id+"", '#show23', '',  '', 'post', '', '','REP');	

}
function show_image_verified(){
$("#wag_tag_verified_icon").css("display","block");
}
function hide_image_verified(){
$("#wag_tag_verified_icon").css("display","none");
}
</script> 
<script>
function mypic(dogname,userid,dogid){
	if(dogname){
ShaAjaxJquary("/wag_club/mypic.php?nicename="+dogname+"&userid="+userid+"&dog_id="+dogid+"", '#friendsinfo', '',  '', 'post', '#friendsinfo', 'Loading.....','REP');
	}
}
function myfnd(dogname,userid){
ShaAjaxJquary("/wag_club/myfriends.php?nicename="+dogname+"&userid="+userid+"", '#friendsinfo', '',  '', 'post', '#friendsinfo', 'Loading.....','REP');
}

function showupdate(){
	$('#sharetext').show(100);
	$('#shareimage').hide(100);
	}

function showimage(){
	$('#sharetext').hide(100);
	$('#shareimage').show(100);	
	}
	
function sharepost(owner,dogid,cc){
	var txt=$('#sharetextbox').val();
	if(txt == '' || txt === undefined){
		var txt = $('#sharetextbox-popup').val();
	}	
	ShaAjaxJquary("/wag_club/add_posts.php?dog_id="+dogid+"&userid="+owner+"&sharetext="+txt+"", '#post', '',  '', 'post', '#loading', '','APE');
	$('#sharetextbox').val('');
	$('#sharetextbox-popup').val('');
	if(cc == 'ror'){			
		$('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();
		});
		$(document).ajaxStop(function(){
   			 window.location.reload();
		});
		//location.reload();		
	}
}
	
function myrecommend(selsize1,breed,name,thisdogid){
	ShaAjaxJquary("/wag_club/recommend-products.php?curr_dogid="+thisdogid+"&selsize1="+selsize1+"&breed_id="+breed+"&name="+name+"", '#friendsinfo', '',  '', 'post', '#friendsinfo', '','REP');
	
	}
	function myabout(nicename,dog_id){
	ShaAjaxJquary("/wag_club/pro.php?dog="+dog_id+"&feature=character", '#friendsinfo', '',  '', 'post', '#friendsinfo', '','REP');
	
	}
</script> 
<script>
function friend_request(userid,dogid,reqtype){
    ShaAjaxJquary("/wag_club/requestpage.php?reqtype="+reqtype+"&userid="+userid+"&dogid="+dogid+"", "#sendreqbox_"+dogid, '', '', 'GET',"#sendreqbox_"+dogid, '<img src="/images/indicator.gif" />','REP');
}
</script>
<div class="wagclub-section">
<?
if ($hastag == '1') {
?>
<?
} else {
?>
<?
}
if ($user_email_transfer == $new_owner_email && $userid != 'Guest' && $match != '1') {
?>
<div class="trans_wc">
  <div class="container">
    <label class="accTran_wt">Do you want to accept this transfer</label>
    <div class="transAcc_wc"><a onclick="transfer_action('accept')" id="accept" name="accept" style="cursor:pointer">Yes</a></div>
    <div class="transDec_wc"><a onclick="transfer_action('decline')" id="decline" name="decline" style="cursor:pointer">No</a></div>
    <div id="transfer_div"></div>
  </div>
</div>
<?
}
?>
<div id="fb-root"></div>
<div id="show23"></div>
<?
$dog_image = $dog_image_profile;
//echo "SELECT * FROM dogs_available WHERE userid='$userid' ORDER BY dog_id DESC LIMIT 1";
//$recent_dog=mysql_query("SELECT * FROM dogs_available WHERE userid='$userid' ORDER BY dog_id DESC LIMIT 1");
//$r_dog=mysql_fetch_array($recent_dog);
//$dog_id=$r_dog['dog_id'];
//$tag_line=mysql_query("SELECT * FROM dogs_activity WHERE dog_id='".$r_dog['dog_id']."' AND tag_line!='' ORDER BY id DESC LIMIT 1");
//$t_dog=mysql_fetch_array($tag_line);
?>
<?
if ($publish_status != 'publish') {
?>
<div class="profile-vs text-center" > Your profile will be visible soon after it is reviewed.</div>
<?
}
?>
<div class="dog_name_dg" id="dogtop_margin" style="position: inherit; background:none;">
<div class="dg_dragRep" id="drgtorepo" style="display:none">Drag to reposition</div>
<?
$size        = getimagesize($DOCUMENT_ROOT . "/dogs/images/" . $dog_image);
$imageinfo   = $size[3];
$width       = explode(" ", $imageinfo);
$img_width   = explode('"', $width[0]);
$img_height  = explode('"', $width[1]);
$imagewidth  = $img_width[1];
$imageheight = $img_height[1];
$imgNname1   = $dog_image;
// chmod($img_location.$imgNname, 0777);
define("LWIDTH", "1000");
define("LHEIGHT", "1200");
define("MWIDTH", "360");
define("MHEIGHT", "312");
define("TWIDTH", "200");
define("THEIGHT", "160");

$orignal_path = $DOCUMENT_ROOT . "/dogs/images/";
$save_path    = $DOCUMENT_ROOT . "/dogs/images/";
 
$filename1      = $orignal_path . $imgNname1;
//echo $filename1;
$image_nicename = pathinfo($filename1, PATHINFO_FILENAME);
if ($dog_image != '') {
    $imglink455 = '/dogs/images/340x300-' . $dog_image;
    $src1 = $DOCUMENT_ROOT . '/dogs/images/' . $dog_image;
}
if ($dog_image != '') {
    $imgmedium = '/dogs/images/medium1_' . $dog_image;
    $src1 = $DOCUMENT_ROOT . '/dogs/images/' . $dog_image;
}
$dest1imgmedium  = $DOCUMENT_ROOT . $imgmedium;
$dest1imglink455 = $DOCUMENT_ROOT . $imglink455;
createImgThumbIfnot($src1, $dest1imgmedium, '300', '300', 'ratiowh');
createImgThumbIfnot($src1, $dest1imglink455, 'MWIDTH', 'MHEIGHT', 'ratiowh');
/*$thumb = make_thumbinBox($filename1,$thumb_name,TWIDTH,THEIGHT);
$medium = make_thumbinBox($filename1,$medium_name,MWIDTH,MHEIGHT);
$large = make_thumb($filename1,$large_name,LWIDTH,LHEIGHT);*/
//echo $medium_name;

$imm = $DOCUMENT_ROOT . "/dogs/images/medium1_" . $imgNname1;
$imm;
if ($imagewidth < 990 || !file_exists($imm) || $imgNname1 == '') {
    if (!file_exists($imm) || $imgNname1 == '') {
?>
<div class="container-fluid dog-profile-bg" id="shoppro" style="background-repeat:repeat; background: url(https://ik.imagekit.io/2345/wag_club/images/no_imgDog.jpg); width:100%; height:706px;float:left;"> </div>
<?
    } else {
?>
<div class="container-fluid dog-profile-bg" id="shoppro" style="background-repeat:repeat; background: url(https://ik.imagekit.io/2345/dogs/images/medium1_<?= $dog_image ?>); width:100%; height:706px;float:left;"> </div>
<?
    }
?>
<?
} else {
?>
<div class="container-fluid dog-profile-bg" id="shoppro" style=" float:left;width:100%;">
  <div class="row"> <img src="https://ik.imagekit.io/2345/dogs/images/<?= $dog_image; ?>"  width="1354" style="width:100%; height:706px; margin-top:<?= $image_margin_top ?>" id="imagepicchange" alt="<?= $dog_name ?>" title="<?= $dog_name ?>" /> </div>
</div>
<?
}
?>
<div class="container-fluid dog-profile-bg" id="shoppro2" style="background-repeat:repeat;display:none; background: url(https://ik.imagekit.io/2345/wag_club/images/no_imgDog.jpg); width:100%; height:706px;float:left;"> </div>
<div class="container-fluid dog-profile-bg" id="shoppro4" style="display:none;background-repeat:repeat; background: url(https://ik.imagekit.io/2345/dogs/images/medium1_<?= $dog_image ?>); width:100%; height:706px;float:left;"></div>
<div class="container-fluid dog-profile-bg" id="shoppro7"  style="display:none; float:left;width:100%;"> <img src="https://ik.imagekit.io/2345/dogs/images/<?= $dog_image; ?>"  width="1354" alt="<?= $dog_name ?>" title="<?= $dog_name ?>"  id="shoppro5" style="width:100%; height:706px;"  /> </div>
<div class="container" style="position: absolute; top: 180px; right: 100px; text-align: right; ">
  <div class="row"> 
    
    <!-- tag details-->
    <div class="dog_detail_dg">
      <h1 class="name_box_dg">
        <?= ucwords($dog_NAME) ?>
        <span style="display:none">
        <?=$dog_id?>
        </span></h1>
      <div class="tagline_box_dg">
        <?= $tag ?>
      </div>
      <?
if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
?>
      <div class="dropdown" style="max-width: 200px;
    float: right;">
        <button class="btn dropdown-toggle wc-changedp-btn" type="button" data-toggle="dropdown"> <i class="fa fa-pencil"></i> Change Display Picture </button>
        <ul class="dropdown-menu dropdown-menu-right" style="min-width:180px; margin-top:0px">
          <?
    if ($all_photos == '1') {
?>
          <li onclick="selectoption('c');"> <a href="#login-box6" class="login-window6"> Choose From Photos</a></li>
          <?
    }
?>
          <li onclick="selectoption('u');"><a href="#login-box5" class="login-window5"><i class="fa fa-upload"></i> Upload photo</a></li>
          <?
    if ($imgNname1) {
        if ($imageheight > 700) {
?>
          <li onclick="get_repo_state()" id="repo"> <a><i class="fa fa-arrows"></i> Reposition</a></li>
          <?
        }
?>
          <li onclick="get_repo_state1()" style="display:none" id="repo1"><a><i class="fa fa-arrows"></i> Reposition</a></li>
          <li onclick="selectoption('remove');" id="remove2"><a><i class="fa fa-remove"></i>  Remove</a></li>
          <?
    } else {
?>
          <li onclick="get_repo_state1()" style="display:none" id="repo1"><a><i class="fa fa-arrows"></i>Reposition</a></li>
          <li onclick="selectoption('remove');" style="display:none" id="remove1"><a><i class="fa fa-remove"></i> Remove</a></li>
          <?
    }
?>
        </ul>
      </div>
      <?
}
?>
      <div class="dg_repBtnBox fl">
        <input type="button" value="Save" class="reposbutton dg_saveReP"  style="display:none" name="imagesubmit" id="imagesubmit" onclick="save_repo('shoppro')">
        <input type="button" value="Cancel" class="reposbutton dg_saveReP"  style="display:none" name="imagesubmit1" id="imagesubmit1" onClick="nothingcrop1('shoppro')">
      </div>
      <div class="dg_repBtnBox fl">
        <input type="button" value="Save" class="reposbutton1 dg_saveReP"  style="display:none" name="imagesubmit" id="imagesubmit" onclick="save_repo('shoppro7')">
        <input type="button" value="Cancel" class="reposbutton1 dg_saveReP"  style="display:none" name="imagesubmit1" id="imagesubmit1" onClick="nothingcrop1('shoppro7')">
      </div>
    </div>
  </div>
  <!-- tag details end--> 
</div>

<!-- details toggle--->
<div class="container-fluid" style="position:absolute;top: 345px;text-align:left;  width: 100%;" >
<div class="container" >
<div class="details_box_dg" >
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 visible-md visible-lg">

<div id="open_detailBox_dg" class="open_detailBox_dg" >
                   <ul >
                  
<?  
if ($hastag == '1') {
?>
<?
} else {
?>                    
<li class="firstLi_dg wt_firstLi_dg" id="get-tag" style="cursor:pointer" onclick="moveToFooter('show_breed_dg')">Get your Wag tag</li>
<?
}
?>
 <li id="open_detail_dg1" onclick="dg_show(this.id);" class="">
<?
if ($hastag == '1') {
?>
<label class="wt_verifiedTxt" style="display:none" id="wag_tag_verified_icon">Wag tag Verified
  <span class="wt_arwDwn"></span>
</label>
<span class="wt_stampPaw" id="ver-tag" style="cursor:pointer;" onmouseover="show_image_verified()" onmouseout="hide_image_verified()"><img src="/wag_club/images/Verified-Icons.png"></span>
<?
} else {
?>
<label class="wt_verifiedTxt" style="display:none"id="wag_tag_verified_icon">Wag tag Verified
  <span class="wt_arwDwn"></span>
</label>                    
<span class="wt_stampPaw" id="ver-tag" style="cursor:pointer;display:none" onmouseover="show_image_verified()" onmouseout="hide_image_verified()"><img src="/wag_club/images/tag-stamp.png"></span>
<?
}
?>
 <?= ucwords($dog_NAME) ?>
 
<i class="fa fa-chevron-up" style=" float:right;"></i>
<img src="https://ik.imagekit.io/2345/wag_club/images/dog_icon.png" width="20" height="16" alt="Dog Icon" title="Dog Icon" style=" float:right;"/></li>
                   <li><a href="/dogs/breed/<?= $breed_nicename ?>/" target="_blank"><?= ucwords($Dogbreed) ?></a></li>
                           <?
if ($day != '0' || $month != '0') {
?>
                   <li id="dog_age"><?
    print(pupDOB1($day, $month, $year));
?></li>
                   <?
}
?>
                   <li> <?
$dogs = $dogDet[dog_sex];
echo "$DogSex[$dogs]";
?></li>
                 <!--  <li><a href="wall.php">Upload Activity</a></li>-->

                

                   <li><? if($city_nicename){?><a href="/dogs/citysearch/<?= $city_nicename ?>/"><? }?><?= ucwords($city) ?>
                   <? if($city_nicename){?></a><? }?></li>
                   <?
$wag_dog_brrrd = query_execute_row("SELECT breed_engine FROM dog_breeds WHERE nicename='$breed_nicename'");
if ($wag_dog_brrrd['breed_engine'] == '1') {
?>
                    <li><a href="/<?= $breed_nicename ?>/">About <?= ucwords($Dogbreed) ?> Dog Breed</a></li>
                   <?
}
?>
				   <?
if ($hastag == '1') {
?>
                   <li class="" id="manage_tag" style="cursor:pointer" onclick="managetag()">Manage Wag Tag</li>
				   <?
} else {
    $salusre    = query_execute_row("SELECT userid FROM users WHERE userid='$dog_owner'");
    $userdog_id = $salusre['userid'];
?>
                   <li class="" id="manage_tag" style="display:none;cursor:pointer" onclick="managetag()">Manage Wag Tag</li>
                   <?
    if ($userdog_id != '') {
?>
                      <li><a href="/dogs/user/<?= $dog_owner ?>/"><?= dispUname($dog_owner); ?></a> <img src="https://ik.imagekit.io/2345/wag_club/images/men-icons.png" width="20" height="20" alt="men Icon" title="Dog Icon" style=" float:right;"/></li>
                      <?
    }
?>
				   <?
}
?>
				   <? 
if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
?>
                   <li class="lastLi_dg"><a href="/wag_club/add_dog.php">+ Add Another Dog</a></li>
                   <?
}


$dog_friend_query = "SELECT * from dog_friends WHERE userid = '$userid' AND friend_id = '$dog_id'";
$check_forreq     = mysql_query($dog_friend_query);
$count_result     = mysql_num_rows($check_forreq);
if ($count_result > 0) {
    $check_status = query_execute_row($dog_friend_query);
    $user_status  = $check_status['status'];
    if ($user_status == 'P') {
?>
                        <li class="lastLi_dg">Request Pending</li>
                       <?php
    } elseif ($user_status == 'A') {
?>
                        <li class="lastLi_dg">Already Friends</li>
                       <?php
    } elseif ($user_status == 'R') {
?>
                        <li class="lastLi_dg">Request Sent</li>
                       <?php
    } elseif ($user_status == 'C') {
        // Nothing
    } else {
        $if_already_friends = mysql_query("SELECT dog_id FROM dogs_available WHERE userid = '$userid'");
        while ($check_all_dogs = mysql_fetch_array($if_already_friends)) {
            $check_dog_id = $check_all_dogs['dog_id'];
            //echo "SELECT * FROM dog_friends WHERE dogid = '$dog_id' AND friend_id = '$check_dog_id'";
            $if_friends   = mysql_query("SELECT * FROM dog_friends WHERE dogid = '$dog_id' AND friend_id = '$check_dog_id'");
            if (mysql_num_rows($if_friends) > 0) {
                $count_for_friends[] = $check_dog_id;
            }
        }
        if (!empty($count_for_friends)) {
            //////
        } else {
            if ($dog_owner != $userid && $userid != "Guest") {
?>
							   <li class="lastLi_dg" id="sendreqbox_<?= $dog_id ?>"><a onclick="friend_request('<?= $userid ?>', '<?= $dog_id ?>', 'frndreq')" value="Send Request" style="cursor:pointer">Add Friend</a></li>
								<?php
            }
        }
    }
} else {
    $if_already_friends = mysql_query("SELECT dog_id FROM dogs_available WHERE userid = '$userid' AND publish_status='publish'");
    while ($check_all_dogs = mysql_fetch_array($if_already_friends)) {
        $check_dog_id = $check_all_dogs['dog_id'];
        //echo "SELECT * FROM dog_friends WHERE dogid = '$dog_id' AND friend_id = '$check_dog_id'";
        $if_friends   = mysql_query("SELECT * FROM dog_friends WHERE dogid = '$dog_id' AND friend_id = '$check_dog_id'");
        if (mysql_num_rows($if_friends) > 0) {
            $count_for_friends[] = $check_dog_id;
        }
    }
    if (!empty($count_for_friends)) {
        // Request Already Sent OR Pending
    } else {
        if ($dog_owner != $userid && $userid != "Guest" && $check_dog_id) {
?>
							   <li class="lastLi_dg" id="sendreqbox_<?= $dog_id ?>"><a onclick="friend_request('<?= $userid ?>', '<?= $dog_id ?>', 'frndreq')" value="Send Request" style="cursor:pointer">Add Friend</a></li>
							<?php
        }
    }
}

?>
                   </ul>
                   </div>
                 <a id="open_detail_dg" onclick="dg_show(this.id);" class="open_detail_dg" ><?= ucwords($dog_NAME); ?>
                 <i class="fa fa-chevron-down" style="float:right"></i>
                  
                  </a>
                   
</div>


<!-- end details toggle-->
<div class="col-xs-12 col-sm-6 col-md-6 text-right">
<!-- share buttons-->
<form method="post" name="frm" id="frm" action="">

<input type="hidden" name="user_id" id="user_id" value="<?= $userid ?>"/>
<input type="hidden" name="dog_id" id="dog_id" value="<?= $dog_id ?>"/>
<input type="hidden" name="dog_nicename" id="dog_nicename" value="<?= $dog_nicename ?>"/>

<div id="show" style="display:none"></div>

<div class="ryt_boxEdit_dg">
<ul>
<? 
//echo "SELECT * FROM dog_wag WHERE dog_id='$dog_id' AND userid='$userid'";
$wag_r = mysql_query("SELECT * FROM dog_wag WHERE dog_id='$dog_id' AND userid='$userid'");

$wag_dog     = mysql_query("SELECT * FROM dog_wag WHERE dog_id='$dog_id'");
$wag_like    = mysql_num_rows($wag_r);
$wag_count12 = mysql_num_rows($wag_dog);

/*if($userid=='Guest')
{?>
<li><a href="#login-box4" class="login-window4"><span style="height:50px;"><img src="images/wag_new.png" width="48" height="46" alt="" /></span><p> Like it</p></a></li>
<? }
else*/
//echo 'dfg'.$wag_like;
if ($wag_like == 0) {
    if ($_SERVER['REQUEST_URI']) {
        $refUrl = $_SERVER['REQUEST_URI'];
    } else {
        $refUrl = '/';
    }
    if (strpos($refUrl, 'loadmore1_test1.php') != false) {
        $refUrl = "/dogs/" . $dog_nicename;
    }
    if ($userid == 'Guest') {
?><a href="/login.php?refUrl=<?= $refUrl ?>" rel="nofollow">
   <li id="beforewag"><img src="/wag_club/images/wag_new.png" width="48" height="46" alt="Like" title="Like"/></span><p> Like it</p></li></a><?
        if ($wag_like != '0') {
?><div class="dg_wag_no" id="dg_wag1"><?= $wag_count12 ?>
</div><?
        } else {
?><div class="dg_wag_no" ><?= $wag_count12 ?>
</div><?
        }
?></li> </a>
<?
    } else {
?>
<li id="beforewag"><a onclick="wagfuncount();"><span style="height:50px;"><img src="/wag_club/images/wag_new.png" width="48" height="46" alt="Like" title="Like"/></span><p> Like it</p></a><?
        if ($wag_like != '0') {
?><div class="dg_wag_no" id="dg_w1"><?= $wag_count12 ?>
</div><?
        } else {
?><div class="dg_wag_no" id="dg_wag1"><?= $wag_count12 ?>
</div><?
        }
?></li>
<?
    }
} else {
?>
<li id="afterwag"><a onclick="wagcount();"><span style="height:50px;"><img src="/wag_club/images/wag_new.png" width="48" height="46" alt="Like" title="Like"/></span><p> Liked</p></a><?
    if ($wag_like != '0') {
?><div class="dg_wag_no" id="dg_wag"><?= $wag_count12 ?>
</div><?
    } else {
?><div class="dg_wag_no" id="dg_wag" ><?= $wag_count12 ?>
</div><?
    }
?></li>
<?
}
?>
<li id="afterwag" style="display:none"><a onclick="wagcount();"><span style="height:50px;"><img src="/wag_club/images/wag_new.png" width="48" height="46" alt="Like" title="Like"/></span><p> Liked</p></a><?
if ($wag_like != '0') {
?><div class="dg_wag_no" id="dg_wag"><?= $wag_count12 ?>
</div><?
} else {
?><div class="dg_wag_no" id="dg_wag" ><?= $wag_count12 ?>
</div><?
}
?></li>
<li id="beforewag" style="display:none"><a onclick="wagfuncount();"><span style="height:50px;"><img src="/wag_club/images/wag_new.png" width="48" height="46"
 alt="Like" title="Like"/></span><p> Like it</p></a><?
if ($wag_like != '0') {
?><div class="dg_wag_no" id="dg_wag1"><?= $wag_count12 ?>
</div><?
} else {
?><div class="dg_wag_no" id="dg_wag1" ><?= $wag_count12 ?>
</div><?
}
?></li>
<? 
if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
?>


<?php
    /*?><div   ></div><?php */
?>

<!-- <li><a href="#" onclick="fb_callout()"><span style="height:50px;"><img src="/wag_club/images/share.png" width="38" height="38" alt=""/></span><p> <p>Share</a></p></li> -->
<li> <a href="#login-box" class="login-window"><span style="height:50px;"><img src="/wag_club/images/edit.png" width="38" height="38" alt="" title="edit" /></span><p> Edit</p></a></li>
<li> <a href="#login-box1" class="login-window1"><span style="height:50px;">
<img src="/wag_club/images/upload.png" width="48" height="48" alt="" title="upload" /></span><p> Upload</p></a></li>
<?
    if ($sessionLevel == 1) {
?>
<li>Status : <?= $publish_status ?><a href="/wag_club/delete.php?delete=1&dog_id=<?= $dog_id ?>&nicename=<?= $dog_nicename ?>">
<span style="height:50px;"><img src="/wag_club/images/share.png" width="38" height="38" alt="Share" title="Share" /></span><p>Delete</p></a>
</li>
<? } ?>
<? } ?>
  
<?php
if ($dog_sex == 'M') {
    $sex = 'his';
} else {
    $sex = 'her';
}
if (($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
    $summary12 = 'My ' . $dog_NAME . ' got ' . $sex . ' very own social profile on Wagclub. Does your dog have an exclusive social identity?';
    
} else {
    $summary12 = $dog_NAME . ' got ' . $sex . ' very own social profile on Wagclub. Does your dog have an exclusive social identity?';
}
$title12 = 'Wag Club';
$url12   = urlencode('https://www.dogspot.in/dogs/' . $dog_nicename);
if ($userid == 'brajendra') {
    $image12 = $imgface;
} else {
    $image12 = urlencode('https://www.dogspot.in/dogs/images/' . $dog_image);
}
if ($dog_image!='') {
    $imglinkp    = '/dogs/images/69x69-' . $dog_image;
    $imglinkleft = '/dogs/images/102x102-' . $dog_image;
    $src = $DOCUMENT_ROOT . '/dogs/images/' . $dog_image;
} else {
    $imglinkp    = '/wag_club/images/69x69-no_imgDog.jpg';
    $imglinkleft = '/wag_club/images/102x102-no_imgDog.jpg';
    $src         = $DOCUMENT_ROOT . '/wag_club/images/no_imgDog.jpg';
}

$dest  = $DOCUMENT_ROOT . $imglinkp;
$dest1 = $DOCUMENT_ROOT . $imglinkleft;
//$destm = $DOCUMENT_ROOT.$imglinkm;
createImgThumbIfnot($src, $dest, '69', '69', 'ratiowh');
createImgThumbIfnot($src, $dest1, '102', '102', 'ratiowh');

if ($publish_status == 'publish') {
    if ($userid == 'Guest') {
?><a href="/login.php?refUrl=<?= $refUrl ?>" rel="nofollow">
   <li><span style="height:50px;"><img src="/wag_club/images/share.png" width="38" height="38" alt="Share" title="Share" /></span><p> Share</p></li></a>
<?
    } else {
?>
<li><a onClick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php
        echo $title12;
?>&amp;p[summary]=<?php
        echo $summary12;
?>&amp;p[url]=<?php
        echo $url12;
?>&amp;p[images][0]=<?php
        echo $image12;
?>','sharer','toolbar=0,status=0,width=548,height=325,top=100,left=250');" href="javascript: void(0)"><span style="height:50px;"><img src="/wag_club/images/share.png" width="38" height="38" alt="Share" title="Share" /></span><p> Share</p></a>
</li>
<?
    }
}
?>
</ul>     
</div>
</form>
<!-- share section end-->
</div>
</div>
</div>
</div>
</div>
<?php 
require_once($DOCUMENT_ROOT . '/wag_club/sub-header-bootstrap.php');
?>
<!----------------------------------------------------------wag tag code------------------------------------------------->
<? 
if ($hastag == '1') {
?>
<div id="wag_tag_div" style="display:none">
<?
    //if($userid=='abhi2290'){ 
    if (($countwagtag != '0' && $match == '1') || ($userid != $dog_owner && $match == '1')) {
        require_once($DOCUMENT_ROOT . '/wag_club/wagtag/lost-tag-bootstrap.php');
    }
    if ($countwagtag == '0' && $match == '1') {
        require_once($DOCUMENT_ROOT . '/wag_club/wagtag/slider/add_dog-bootstrap.php');
    }
?>
</div>
<?
} else {
?> 
<div id="wag_tag_div">
<? 
    if (($countwagtag != '0' && $match == '1') || ($userid != $dog_owner && $match == '1')) {
        require_once($DOCUMENT_ROOT . '/wag_club/wagtag/lost-tag-bootstrap.php');
    }
    if ($countwagtag == '0' && $match == '1') {
        require_once($DOCUMENT_ROOT . '/wag_club/wagtag/slider/add_dog-bootstrap.php');
    }
?>
</div>
<?
}
?>

<div id="wagtagdiv" name="wagtagdiv"></div>
<? //} 
?>
<!---------------------------------------------------------------ends--------------------------------------------------->
<!-- upload activity-->

  <div class="cont980">
<div id="friendsinfo" name="friendsinfo">
<?
if ($insertImg_id && $album_id) {
?>
<?php
    require_once($DOCUMENT_ROOT . '/wag_club/editphotos.php');
?>
<?
} else {
?>

<?php //require_once($DOCUMENT_ROOT.'/wag_club/login_wag.php'); 
?>

<div id="txt1" style='display:none; cursor:pointer' ><?
    echo "dogview";
?></div>
<div id="txt2" style='display:none'><?
    echo $t_total;
?></div>
<?
  
    //echo "fer".$t_total.$dog_owner;
    if ($t_total == '0' && ($dog_owner == $userid || $sessionLevel == 1) && $userid != "Guest") {
?>



<div class="upload-act-wc">
    <div class="container">
    <div class="row">
    <div class="col-sm-12 text-center">
    <h2>EVERY DAY IS AN ADVENTURE</h2>
    <p>Create your first club Post for <?= $dog_NAME ?> to start capturing the simple, magical, everyday dog moments in life.</p>
    </div>
   
   
   <div class="col-xs-12 col-sm-6 col-md-6 text-center">
<a data-toggle="modal" href="#uploadphoto" class="login-window1 wc_profile_btns"><div class="btn_act_wc">Upload Activity</div></a>
   </div>
     <div class="col-xs-12 col-sm-6 col-md-6 text-center"> <a data-toggle="modal" href="#share-box" class="login-window1 wc_profile_btns"><div class="btn_act_wc">Share Update</div></a></div>
    </div>


</div>


</div>

<div id="login-box2" class="login-popup">
<?php
        require_once($DOCUMENT_ROOT . '/wag_club/act_form.php');
?></div>
<?php //include($DOCUMENT_ROOT.'/wag_club/related-dogs.php'); 
?>
<?
    } else {
        
?>
<? 
        $i       = 1;
        $countul = 0;
        while ($t = mysql_fetch_array($activities1)) {
            // $imn7="https://www.dogspot.in/dogs/images/".$t['image'];
            if ($t['image'] != '' || $t['user_activity'] != '') {
                
                $i++;
            }
        }
        if ($i > 1) {
            
?>
    
<div class="cont980">
<div class="dg_profile_leftsec">
<?
            if ($dog_owner == $userid) {
                $getidd = query_execute_row("SELECT id from dogs_activity where dog_id='$dog_id' order by id desc");
                $c      = $getidd['id'];
?>
<!-- share updates and images-->
<div class="share_img_updatesBlock">
<div class="share_img_updates">

<ul>
<li>
<i class="fa fa-edit" style="color:#000; padding-left:0px; padding-right:0px;" ></i> <a onclick="showupdate()" style="cursor:pointer">Share Updates</a>
</li>
<!--<li><i class="fa fa-picture-o"></i> <a onclick="showimage()" style="cursor:pointer">Share Image</a></li>-->
</ul>
</div>
<!-- text-area-->
<div class="" id="sharetext">
<textarea placeholder="Enter your text" style="width: 632px; height: auto; margin: 0px; border-top: medium none; border-radius: 0px;font-size: 15px;" id="sharetextbox"></textarea>

 <div class="share_btnbox">
<input type="button" class="wagclub_sharebtn" value="Share" onclick="sharepost('<?= $dog_owner ?>','<?= $dog_id ?>','<?= $c ?>')";></div>

</div>
<!--
<div class="" id="shareimage" style="display:none">
<div class="share_btnbox">
<input type="button" class="wagclub_sharebtn" value="Share" onclick="share();"> 
<input type="button" class="wagclub_cancelbtn" value="Cancel" onclick="share();"></div>
</div>


<!-- text-area-->
</div>
<!-- share updates and images-->
<input name="postidd1" id="postidd1" type="hidden" value="0"/>
<div id="post"></div>
<?
            }
?>
<form name="wagty" id="wagty"  method="post">
<div class="rytPost_list" id="rytPost_list" >

<?
            $iid = 0;
			
            while ($t_to = mysql_fetch_array($activities2)) {
				
                $activity_id     = $t_to['id'];
                $activity_dog_id = $t_to['dog_id'];
                $activity_wag    = $t_to['wag'];
                $activity1       = $t_to['activity1'];
                $user_activitys  = $t_to['user_activity'];
                $status          = $t_to['publish_status'];
                $act_img         = $t_to['image'];
                $iid             = $iid + 1;
                $countul++;
				
                $imn6 = "https://www.dogspot.in/dogs/images/" . $act_img;
                if ($act_img) {
                    $imglink = '/dogs/images/400x400-' . $act_img;
                    $src = $DOCUMENT_ROOT . '/dogs/images/' . $act_img;
                }else{
                    $imglink = '/dogs/images/150-200-no-photo.jpg';
                    $src = $DOCUMENT_ROOT . '/dogs/images/no-photo.jpg';
                }
				$dest = $DOCUMENT_ROOT.$imglink;
                createImgThumbIfnot($src, $dest, '400', '400', 'ratiowh');
                $file_loc = $DOCUMENT_ROOT . '/dogs/images/400x400-' . $act_img;
                if (file_exists($file_loc) || $user_activitys != '') {
?>
		<div class="imgtxtcontwag" id='<?= $iid ?>'>
			<div class="wagclub_cmtsec">
				<div class="dogThumb_img"><img src="<?=$imglinkp ?>" alt="<?=$dog_NAME.' '.$iid ?>" title="<?=$dog_NAME.' '.$iid ?>"/></div>
				<div class="wagclub_Postsec">
				<? if ($userid == $dog_owner) { ?>
					<div style="float:right;cursor:pointer" onclick="remove_post(<?= $activity_id ?>,<?= $iid ?>)">[X]</div>
				<? } ?>
				<div class="dogName_act1">
				<? if ($_SERVER['REQUEST_URI']) {
                        $refUrl1 = $_SERVER['REQUEST_URI'];
                    } else {
                        $refUrl1 = '/';
                    }
                    if (strpos($refUrl1, '/wag_club/loadmore1_test1.php') != false) {
                        $refUrl1 = "/dogs/" . $dog_nicename;
                    }
				?>
                <?= $dog_NAME ?>, <a href="/dogs/breed/<?= $breed_nicename ?>/"><strong><?= $Dogbreed ?></strong></a> posted by <a href="/dogs/user/<?= $dog_owner ?>/"><?= DispUname($dog_owner) ?> </a>
                </div>
				<? if ($user_activitys && $user_activitys != 'contest') { ?>
				<div class="actTxt_post_new">
					<?= $user_activitys ?>
				</div>
				<? } else { ?>
                <a target="_blank" href="/wag_club/activity/<?= $activity_id ?>/">
                    <div class="actImg_post_new">
                        <img src="/dogs/images/400x400-<?=$act_img ?>"  height="" alt="<?= $dog_NAME . ' ' . $iid ?>" title="<?= $dog_NAME . ' ' . $iid ?>" />
                    </div>
                </a>
				<div class="postIcon_wc" style="width:528px;">
					<? 
						//echo "SELECT * FROM dog_wag WHERE dog_id='$dog_id' AND userid='$userid'";
                        //echo "SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND userid='$userid' AND id='$activity_id'";
                        $wag_activity       = mysql_query("SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND userid='$userid' AND activity_id = '$activity_id'");
                        //echo "SELECT * FROM dogs_activity WHERE dog_id='$dog_id' AND userid='$userid' AND id='$activity_id'";
                        //echo "SELECT * FROM dogs_activity WHERE dog_id='$dog_id' AND id='$activity_id'";
                        $count_activity_wag = mysql_query("SELECT * FROM dog_activity_wag WHERE dog_id='$dog_id' AND activity_id='$activity_id'");
                        $count_activity     = mysql_num_rows($count_activity_wag);
                        $wag_like1          = mysql_num_rows($wag_activity);
                        if ($wag_like1 == 0) {
                            if ($userid == 'Guest') {
					?>
					<a href="/login.php?refUrl=<?= $refUrl ?>" rel="nofollow">
					    <div class="wagPost" id="postwagbefore<?= $activity_id ?>">
							<img src="/wag_club/images/wag_new.png" width="28" height="26" alt="Like" title="Like">
						</div>
					</a>
					<? }else { ?>
					<a onclick="wagfuncount1('<?= $activity_id ?>');">
						<div class="wagPost" id="postwagbefore<?= $activity_id ?>">
							<img src="/wag_club/images/wag_new.png" width="28" height="26" alt="Like" title="Like" />
							<? if ($count_activity != '0') {?>
                                <span id="acti_wag<?= $activity_id ?>"><?= $count_activity ?></span>
							<? }else {?>
							<span id="acti_wag<?= $activity_id ?>"><?= $count_activity ?></span>
							<? } ?>
						</div>
					</a>
					<? } ?>
                    <? }else { ?>
						<a href="javascript:void(0)" onclick="wagcount1('<?= $activity_id ?>');">
							<div class="wagPost" id="postwagafter<?= $activity_id ?>">
								<img src="/wag_club/images/wag_new.png" width="28" height="26" alt="Unlike" title="Unlike">
							<? if ($count_activity != '0') {?>
                            	<span id="acti_wagq<?= $activity_id ?>"><?= $count_activity ?></span>
							<? } else {?>
								<span id="acti_wagq<?= $activity_id ?>" ><?= $count_activity ?></span>
							<? } ?>
							</div>
						</a>
						<? }?>
						<a onclick="wagcount1('<?= $activity_id ?>');">
							<div class="wagPost" id="postwagafter<?= $activity_id ?>" style="display:none">
								<img src="/wag_club/images/wag_new.png" width="28" height="26" alt="Unlike" title="Unlike"/>
							<? if ($count_activity != '0') {?>
                            	<span id="acti_wagq<?= $activity_id ?>"><?= $count_activity ?></span>
                            <? }else{?>
                            	<span id="acti_wagq<?= $activity_id ?>"><?= $count_activity ?></span>
							<? } ?>
							</div>
						</a>
                        <a onclick="wagfuncount1('<?= $activity_id ?>');">
	                        <div class="wagPost" id="postwagbefore<?= $activity_id ?>" style="display:none">
    	                        <img src="/wag_club/images/wag_new.png" width="28" height="26" alt="Like" title="Like">
        	                    <? if ($count_activity != '0') {?>
            	                <span id="acti_wag<?= $activity_id ?>"><?= $count_activity ?></span>
                	            <? }else {?>
                    	        <span id="acti_wag<?= $activity_id ?>" ><?= $count_activity ?></span>
                        	    <? } ?>
	                        </div>
                        </a>
						<span id="wag<?= $activity_id ?>"></span>
						<?
                        $titlewag   = $sqldog;
                        $summarywag = dispUname($userid) . " shared activity of " . $dog_NAME;
                        $urlwag     = "https://www.dogspot.in/wag_club/activity/" . $activity_id;
                        $imagewag   = "https://www.dogspot.in/dogs/images/600-600-" . $act_img;
                        if ($act_img != '') {
                            $imglink455 = '/dogs/images/340x300-' . $act_img;
                            //$imglinkm='/imgthumb/100-'.$imgFL[0];
                            $src1 = $DOCUMENT_ROOT . '/dogs/images/' . $act_img;
                        }
                        $dest1 = $DOCUMENT_ROOT . $imglink455;
                        //$destm = $DOCUMENT_ROOT.$imglinkm;
                        createImgThumbIfnot($src1, $dest1, '460', '280', 'ratiowh');
                        $orignal_path = $DOCUMENT_ROOT . "/dogs/images/";
                        $save_path    = $DOCUMENT_ROOT . "/dogs/images/";
                        $filename = $orignal_path . $act_img;
                        $image_nicename = pathinfo($filename, PATHINFO_FILENAME);
                        $ext            = pathinfo($filename, PATHINFO_EXTENSION);
                        $thumb_name     = $save_path . 'thumb_' . $imgNname;
                        $medium_name    = $save_path . 'medium_' . $imgNname;
                        $large_name     = $save_path . $imgNname;
                        $size        = getimagesize($DOCUMENT_ROOT . "/dogs/images/340x300-" . $act_img);
                        $imageinfo   = $size[3];
                        $width       = explode(" ", $imageinfo);
                        $img_width   = explode('"', $width[0]);
                        $img_height  = explode('"', $width[1]);
                        $imagewidth  = $img_width[1];
                        $imageheight = $img_height[1];
                        if ($imagewidth < 200 || $imageheight < 200) {
                            $dest = imagecreatefromjpeg('/home/dogspot/public_html/wag_club/images/white-bg.jpg');
                            if ($ext == 'jpeg' || $ext == 'jpg') {
                                $src = imagecreatefromjpeg('/home/dogspot/public_html/dogs/images/340x300-' . $act_img);
                            }elseif($ext == 'png') {
                                $src = imagecreatefrompng('/home/dogspot/public_html/dogs/images/340x300-' . $act_img);
                            }else{
                                $src = imagecreatefromgif('/home/dogspot/public_html/dogs/images/340x300-' . $act_img);
                            }
                            imagealphablending($dest, false);
                            imagesavealpha($dest, true);
                            imagecopymerge($dest, $src, 30, 30, 10, 10, 400, 400, 100);
                            //imagecopymerge ( resource $dst_im , resource $src_im , int $dst_x , int $dst_y , int $src_x , int $src_y , int $src_w , int $src_h , int $pct )//have to play with these numbers for it to work for you, etc.
                            //header('Content-Type: image/jpeg');
                            imagepng($dest, "/home/dogspot/public_html/dogs/images/medium1_" . $act_img);
                            imagedestroy($dest);
                            imagedestroy($src);
                            $imgface = "/wag_club/images/340x300-" . $act_img;
                        }else{
                            $imgface = "/dogs/images/medium1_" . $act_img;
                        }
                        if ($status == 'publish') {
                            if ($userid == 'Guest') {
						?>
                            <a href="/login.php?refUrl=<?= $refUrl ?>" rel="nofollow">
                                <div class="wagPost"><img src="/wag_club/images/share.png" width="28" height="28" alt="Share" title="Share"></div>
                            </a>
							<? }else{ ?>
							<a onClick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $titlewag;?>&amp;p[summary]=<?php echo $summarywag;?>&amp;p[url]=<?php echo $urlwag;?>&amp;p[images][0]=<?php echo $imgface;?>','sharer','toolbar=0,status=0,width=548,height=325,top=100,left=250');" href="javascript: void(0)">
                            	<div class="wagPost"><img src="/wag_club/images/share.png" width="28" height="28" alt="Share" title="Share"></div>
                            </a>
						<? } } ?>
						</div>
						<div class="actTxt_post_new"><?= $activity1 ?></div>
					<?php
						}
						if ($userid != 'Guest') {
							include($DOCUMENT_ROOT . '/wag_club/comment_wagclub_test1.php');
						} else {
							include($DOCUMENT_ROOT . '/wag_club/commentguest_test1.php');
						}
					?>
                    <input type="hidden" name="dog_id" id="dog_id" value="<?= $dog_id ?>"  />
                    <input type="hidden" name="dog_owner" id="dog_owner" value="<?= $dog_owner ?>"  />
                    <input type="hidden" name="new_owner_email" id="new_owner_email" value="<?= $new_owner_email ?>"  />
                    <div id="txt1" style='display:none; cursor:pointer'><? echo "dogview"; ?></div>
                    <div id="txt2" style='display:none'><? echo $t_total; ?></div>
				</div>
			</div>
		</div>
	<? } ?>
<? } ?>
		<div id='loadMoreComments' style='display:none'><img src="/new/pix/loading2.gif" /></div>
	</div>
</form>
</div>
</div><!-- side bar-->
<div id="dog_profile_sidebar"><?php  require_once($DOCUMENT_ROOT . '/wag_club/profilesidebar.php'); ?></div>
<? } ?>
<? } ?>
<? } ?>
</div>
</div>
<!-- end uplaod acitvity-->
<!-- pop up start-->

<!-- Modal -->
  <div class="modal" id="uploadphoto" role="dialog">
    <div class="modal-dialog wc-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Make it simple and fun to capture and share the everyday magic in your dog's life. Give it a whirl!</h4>
        </div>
        <div class="modal-body ">
          <div class="form-group text-center">
        <label>&nbsp;</label>
        <div class="input-group ">
            <span class="input-group-btn" id="fileselector">
                 <label class="btn btn-default btn-file btn-uplaod" for="upload-file-selector">
				
                    <input type="file" id="imgInp"><i class="fa fa-upload"></i> Upload File
                </label>
            </span>
           <input type="text" class="form-control" readonly style="display:none;">
        </div>
        <img id='img-upload'/>
		
    </div>
	<div class="form-group">
<label>Tagline</label>
<input name="tagline2" class="form-control" id="tagline2" type="text" value="">
</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" >Save</button>
		  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- edit-->
  <div class="modal" id="editprofile" role="dialog">
    <div class="modal-dialog wc-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Dog Info</h4>
        </div>
        <div class="modal-body ">
          <div class="form-group">
<label>Dog's Name</label>
<input class="form-control"  name="dogname" id="dogname" type="text" value="Seru" maxlength="25">
</div>
	<div class="form-group">
<label>Tagline</label>
<input class="form-control" name="tagline" id="tagline" type="text" value="" maxlength="40">
</div>
<div class="form-group">
<label>Breed</label>
<select name="breed" id="breed" class="form-control">
<option value="Mixed Breed@@mixed-breed" selected="selected">Mixed Breed</option>
<option value="Affen Pinscher@@affen-pinscher">Affen Pinscher</option>
<option value="Afghan Hound@@afghan-hound">Afghan Hound</option>
</select>
</div>
<div class="form-group">
<label>Birthday</label>
<div class="row">
<div class="col-xs-4">
 <select name="day" id="day" class="form-control">
                <option value="day">Day</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29" selected="selected">29</option><option value="30">30</option><option value="31">31</option>				</select>
</div>
	<div class="col-xs-4">			
				<select name="month" id="month" class="form-control">
                <option value="month">Month</option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7" selected="selected">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option>				</select>
</div>
				<div class="col-xs-4">
 <select id="year" name="year" class="form-control">
                <option value="year">Year</option>
				<option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014" selected="selected">2014</option><option value="2015">2015</option><option value="2016">2016</option>				</select>

</div>
</div>
</div>
<div class="form-group">
<label>Gender</label>
<select name="gender" id="gender" class="form-control">
<option value="M" selected="selected">Male</option>
<option value="F">Female</option>
</select>
</div>


<div class="form-group">
              
<label>Location</label>
<input class="form-control" name="location" id="location" type="text" value="Ghaziabad">
<input name="dog_id" id="dog_id" type="hidden" value="1013735">
<input class="form-control" name="dog_nicename" id="dog_nicename" type="hidden" value="seru">
</div>
        <div class="modal-footer">


<div class="btn_is_box1">
<input name="update" id="update" type="submit" value="Publish" class="btn btn-default">
</div>
        </div>
      </div>
      
    </div>
  
 
  
  </div>
  <!-- edit end-->
  
</div>

<div class="modal" id="share-box" role="dialog">
    <div class="modal-dialog wc-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body" style="padding:0px;">
         <div class="share_img_updatesBlock" style="margin:0px;">
              <div class="share_img_updates" >
                <ul>
                  <li> <i class="fa fa-edit"></i> <a onclick="showupdate()" style="cursor:pointer">Share Updates</a> </li>
                
                </ul>
              </div>
             
              <div class="" id="sharetext">
                <textarea placeholder="Enter your text" id="sharetextbox"></textarea>
                <div class="share_btnbox text-right">
                  <input type="button" class="btn btn-default wagclub_sharebtn" value="Share" onclick="sharepost('nareshsingh','1013735','18440')" ;="" style=" height:36px;"> 
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
              </div>
             
            </div>
	
        </div>
      
      </div>
      
    </div>
  </div>
<!-- popups-->
<!-- pop up end-->

</section>
<script type="text/javascript" src="/wag_club/file_uploads.js"></script> 
<script src="/new/js/jquery/jquery-ui.js" type="text/javascript"></script> 
<script type="text/javascript" src="/new/js/slide.js"></script>
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');
?>
