<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/dg_style.css" />
<title>Wag Club</title>
<style>
.actImg_wc{ margin:30px auto; width:1200px;}
.post_wc {
background: #fff; display:table-cell;
border-radius: 5px;
box-shadow:0px 0px 14px 0px #0F0F0F;
margin: 10px;width: 300px;
padding: 10px;
}
.postTxt_wc {
float: left; height:80px;font-family: 'throw_my_hands_up_in_the_aiBd'; overflow:hidden;font-size: 19px;color: #999;
margin-top: 5px;
}
.post_dg {
float: left;
margin: 40px; text-align:left;
}
.postImg_wc {
float: left;
height: 272px;
overflow: hidden;
background: #444;
}
.wagPost {
background: #000;
float: left;
height: 30px;
margin: 5px 5px 5px 0;
padding: 5px;
}
.wagPost img {
float: left;
}
.wagPost span {
color: #fff;
float: right;
margin: 7px;
}

.postPin {
float: left;
margin-right: -64px;
margin-top: -39px;
}
.leftBox_wc {
display: table-cell;
width: 235px;
vertical-align: top;
background: #f7f7f7;
border-right: 1px dashed #bfbfbf;
}
.dogImg_wc {
float: left;
padding: 5px;
background:#fff;
border: 1px solid #bfbfbf;
border-radius: 2px;
margin: 12px;
}
.dogDetail_wc {
float: left;
font-size: 14px;
width: 200px;
padding: 0 12px;
}
.dogDetail_wc ul li {
float: left;
width: 200px;
list-style: none;
padding: 5px;
}
.wt_grnStrip {float: left;background: #09ff16;width: 300px;height: 41px;-ms-transform: rotate(170deg);-webkit-transform: rotate(170deg);transform: rotate(170deg);position: relative;z-index: -1;}
.wt_pinkStrip {float: left;background: #ff005a;width: 300px;margin-top: -30px;color: #fff;font-size: 23px;padding: 7px;text-align: center;}
.wagTag_dg {
    float: left;
    width: 100%;
    text-align: left;
}
.wagImg_dg {
    float: left;margin-left: 50px;
}
.wagTxt_dg {
    float: right;
   width: 40%;
padding: 40px 50px; margin-right:50px;
}
.wagTxt_dg h3 {
    color: #fff;
    font-size: 42px;
    line-height: 45px;
}
.wagTxt_dg p {
    color: #fff;
    font-size: 22px;margin-top: 20px;
line-height: 30px;
}
.wagBtn_dg {
float: left;
position: relative;
z-index: 0;
margin-top: 50px;
}
.wagTagBox_dg{ background:#f89e1d; height:568px;}
.clubBox_dg{ color:#000;}
.clubBox_dg {
    float: left;
   width: 44%; text-align:left;
padding: 120px 20px;
}
.clubBox_dg h3 { color:#000;
    font-size: 42px;
    line-height: 52px;
}
.clubBox_dg p {
font-size: 26px;
margin-top: 20px;
line-height: 40px;
}
.clubImg_dg {
float: right;
width: 46%;
margin-left: 60px;
margin-top: 40px;margin-bottom: 40px;
}
.clubImg_dg>.search_listing>ul>li {
    width: 124px;margin: 5px
}
.clubImg_dg>.search_listing>ul>li>a {
    width: 80px;border: 4px solid #fff;
    height: 80px;margin-left: 8px;
}
.clubImg_dg>.search_listing>ul>li .dog_brief_detail_dg {
    width: 110px;
}
.clubImg_dg>.search_listing>ul>li .dog_brief_detail_dg>.nam_color{ height:20px;}
.breedBox_dg {
float: left;
width: 88%;
text-align: center;
font-size: 16px;
color: #666;
}
.dog_to_track .clubBox_dg h3{ color:#fff;}
.dog_to_track .clubBox_dg{ padding: 10px;color:#fff;
margin-top: 80px;text-shadow: -1px -1px 0 #000, 
 1px -1px 0 #000,
 -1px 1px 0 #000,
 1px 1px 0 #000;
/*background:rgba(80, 61, 21, 0.45);*/}

/*line num 487*/
.Imgtrack.fl {
border: 5px solid #fff;
margin: 0px;
width: 180px;
border-radius: 50%;
height: 180px;
overflow: hidden;
background: #a89263;
text-align: center;
}
/*line num 497*/
.Imgtrack1.fl {
margin: 0;
border: 5px solid #fff;
width: 180px;
border-radius: 50%;
height: 180px;
overflow: hidden;
background: #a89263;
text-align: center;
}
.dog_to_track .dg_popTxt {
float: left;
background: rgba(255, 255, 255, 0.82);
margin-top: -186px;
padding: 80px 11px 57px 14px;
width: 88%;
text-align: center; display:none;
position: relative;
}
.popBox_dg {
border: 0px solid #FFF;
margin: 10px;
width: 190px;
padding: 0;
border-radius: 50%;
height: 190px;
overflow: hidden;
background: #a89263;
text-align: center;
}
.popBox_dg:hover .dg_popTxt{ display:block;}
.popBox_dg.popBox_dg1{margin: -72px 0;
margin-left: 187px;}
.homepage-fancystripe {
float: left;
width: 100%;
}
/*line num 479*/
.ryt_text_track.fr {
float: right;
width:35%;text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
text-align: left;
font-size: 49px;
color: #fff;
line-height: 75px;
margin-top: 25px;
padding: 40px 0;
font-weight: bold;
}
/*line num 46
.bg_body {
height: 535px;
background: url(/wag_club/images/wag-bg.png) no-repeat center 1px;
float: left;
width: 100%;
}*/
.wf_prfBack {
float: left;
width: 100%;
position: relative;
height:520px; overflow:hidden;
}
.dog_to_track {
background: url(http://packdog.com/static/images/homepage2/grain.png) repeat;
height: 520px;
width: 100%;
position: relative;
float: left;
margin-top: -520px;
}
.wf_prfBack img{
position: absolute;
top: 0;
min-width: 100%;
left: 0;}
div.wf_slide {
    float: left;
    width: 100%;
}
div.wf_slideBox {
    float: left;
    width: 266px;
height:177px;cursor: pointer;
    position: relative;
}
div.wf_slideImg {
    float: left;
}
div.wf_slideImg img{-webkit-filter: grayscale(100%);filter: gray; }
div.wf_slideImg img:hover{-webkit-filter: grayscale(0%);filter: none; }
</style>
<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>
<div class="bg_body_op"></div>
<div class="bg_body"></div>
<div class="txt_dg">
<div class="text_box_dg">
<h1>Wag Club</h1>
<p> <span class="font174">An</span>
<span class="font50">Exclusive</span>
<span class="font174">club</span> 
<span class="font50">just</span> 
<span class="font174">for</span> 
<span class="font50">your</span>
<span class="font174">dog</span>

</p>
<div class="add_btn_dg">
  <div class="ad_dg_btn"><span style="font-size:33px; margin-left:30px;">+</span><span style="padding: 8px;">Add Your Dog</span></div>
</div>  
</div>
</div>
<div class="homepage-fancystripe"></div>
<div class="y_dog_dg">
<h1>Dogs are your Trusted Allies.</h1>
<p>Capture and share the everyday antics of your companion with others</p>
</div>
<div class="homepage-fancystripe"></div>
<div class="wf_prfBack">
<img src="http://packdog.com/static/images/homepage2/dogs/shorthair.jpg" width="" height="" alt=""> </div>
<div class="dog_to_track">
<div class="actImg_wc" style="width:1000px;">

<div class="postBox_wc" style="width:50%; float:left;">
<div class="post_dg">
<div class="post_wc">
<div class="postImg_wc">
  <img src="images/9.jpg" width="300" height="" alt="" />
</div>
<div class="postIcon_wc">
<div class="wagPost">
<img src="images/wag.png" width="28" height="26" alt="" />
<span>200</span>
</div>
<div class="wagPost"><img src="images/share.png" width="28" height="28" alt="" /></div>
<!--<div class="wagPost"><img src="images/upload.png" width="28" height="28" alt=""></div>-->
</div>
<div class="postTxt_wc">
<p>Just another day...</p>
</div>
</div>
</div>


</div>


<div class="clubBox_dg">
<h3>An Adventure Called Life</h3>
<p>Why keep those adorable pictures with you when you can share it with other fellow pet lovers
</p>
</div>



</div>
</div>
<div class="homepage-fancystripe"></div>


<div class="actImg_wc" style="width:1000px;">

<div class="clubBox_dg">
<h3>Come and Connect with your Club </h3>
<p>Every dog will love to meet members of his own clan.
Join the all exclusive breed club
</p>
</div>
<div class="clubImg_dg">
<div class="search_listing">
<ul>
<li>
<a href="/dogs/browney_3"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">browney</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>
<li>
<a href="/dogs/reo_4"><img src="images/4.jpg" width="80" height="80" alt="" /></a>

<div class="dog_brief_detail_dg">
<span class="nam_color">reo</span>
</div>
</li>

</ul>
</div>
<div class="breedBox_dg">(The Beagle Club)</div>
</div>
</div>
<!--<div class="homepage-fancystripe"></div>
<div class="wf_prfBack" style="height:589px;">
<img src="http://packdog.com/static/images/homepage2/dogs/shorthair.jpg" width="" height="" alt=""> </div>
<div class="dog_to_track" style="height: 589px;margin-top: -589px;">
<div class="cont980">
<div class="left_img_track fl">
<div class="popBox_dg">
<div class="Imgtrack fl">
  <img src="images/4.jpg" width="207" height="181" alt="" /> 
</div>
 <div class="dg_popTxt">
<h3>Barley</h3>
<h4>Border Collie</h4>
</div>
</div>
<div class="popBox_dg popBox_dg1">
<div class="Imgtrack1 fl">
  <img src="images/4.jpg" width="207" height="181" alt="" /> 
</div>
 <div class="dg_popTxt">
<h3>Barley</h3>
<h4>Border Collie</h4>
</div>
</div>
<div class="popBox_dg">
<div class="Imgtrack fl">
  <img src="images/4.jpg" width="207" height="181" alt="" /> 
</div>
 <div class="dg_popTxt">
<h3>Barley</h3>
<h4>Border Collie</h4>
</div>
</div>
</div>
<div class="ryt_text_track fr">
Come <br/>
& Share <br/>
the lives <br/>
of these <br/>
Cyber <br/>
Stars
</div>
</div>
</div>-->
<div class="homepage-fancystripe"></div>
<div class="y_dog_dg" style="margin: 40px 0;">
<h1 style="font-size: 56px;">Come & Share the lives of these Cyber Stars</h1>
</div>
<div class="homepage-fancystripe"></div>
<div class="wf_slide">
<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0013.JPG" width="266" height="" alt=""> </div>
<div class="wf_back"></div>
</div>
<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0019.JPG" width="266" height="" alt=""> </div>
</div>

<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0022.JPG" width="266" height="" alt=""> </div>
</div>

<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0013.JPG" width="266" height="" alt=""> </div>
</div>

<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0019.JPG" width="266" height="" alt=""></div>
</div>


<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0022.JPG" width="266" height="" alt=""></div>
</div>


<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0013.JPG" width="266" height="" alt=""></div>
</div>

<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0013.JPG" width="266" height="" alt=""> </div>
<div class="wf_back"></div>
</div>
<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0019.JPG" width="266" height="" alt=""> </div>
</div>

<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0022.JPG" width="266" height="" alt=""> </div>
</div>

<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0013.JPG" width="266" height="" alt=""> </div>
</div>

<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0022.JPG" width="266" height="" alt=""></div>
</div>


<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0022.JPG" width="266" height="" alt=""></div>
</div>


<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0013.JPG" width="266" height="" alt=""></div>
</div>
<div class="wf_slideBox">
<div class="wf_slideImg">
  <img src="images/IMG_0022.JPG" width="266" height="" alt=""></div>
</div>
</div>
<div class="homepage-fancystripe"></div>
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>