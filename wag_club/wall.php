<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/dg_style.css" />
<title>Wag Club</title>

<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>

<div class="dog_name_dg">
<div class="add_name_box_dg">
<div class="que_box_dg">What’s Pluto doing?</div>
<div class="select_box_dg"> 
                <input name="" type="file" />
                 <div class="upload_photo_dg">Upload Activity</div>
                 </div>
</div>
<div class="activity_box_dg mt20">
<div class="act_box_dg">
<ul>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt="" /></a>
</li>
<li class="act_txt_dg">
<h2>Flyball</h2>
<p>Flyball is a growing dog sport involving a team knockout competition. 
It’s fast, fun, exciting and has a dedicated following at Crufts, the world’s 
largest dog show.
</p>
</li>
</ul>
</div>
<div class="act_box_dg">
<ul>
<li class="act_txt_dg">
<h2>Flyball</h2>
<p>Flyball is a growing dog sport involving a team knockout competition. 
It’s fast, fun, exciting and has a dedicated following at Crufts, the world’s 
largest dog show.
</p>
</li>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt="" /></a>
</li>

</ul>
</div>
</div>

<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>