<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/dg_style.css" />
<title>Wag Club</title>
<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>
<script language="javascript" type="text/javascript" src="js/jquery.min.1.8.2.js"></script>  
<script>
$(document).ready(function(e) { 
    $("#open_detail_dg").click(function(e) {
	// $("#open_detailBox_dg").insertBefore("#open_detail_dg");
 	$("#open_detail_dg").hide();
	$("#open_detailBox_dg").show("slide",{direction:'top'});
    $("#open_detailBox_dg").show();
	});
	$("#open_detail_dg1").click(function(e) {
    $("#open_detailBox_dg").hide();
    $("#open_detail_dg").show();
	});
});
</script>



<div class="dog_name_dg">
<div class="dog_profileImg_dg">
  <img src="images/pluto.jpg" width="1354" height="706" alt="" />
</div>
<div class="dog_detail_dg">
<div class="name_box_dg">Pluto</div>
<div class="tagline_box_dg">dont disturb i am going online</div>
</div>


<div class="details_box_dg">
<div id="open_detailBox_dg" class="open_detailBox_dg" style="display: none;position:absolute;bottom: -58px; text-align:left;">
                   <ul >
                   <li id="open_detail_dg1" class="firstLi_dg">Pluto<img src="images/arw.png" width="8" height="5" alt="" style=" float:right;padding: 7px 0;"/></li>
                   <li><a href="search.php">Golden Retriever</a></li>
                   <li>10th Oct</li>
                   <li>7, Male</li>
                   <li><a href="wall.php">Upload Activity</a></li>
                   <li>Prerna</li>
                   <li class="lastLi_dg"><a href="add_dog.php">+ Add Another Dog</a></li>
                   </ul>
                   </div>
                 <a id="open_detail_dg" class="open_detail_dg" >Pluto <img src="images/arw-up.png" width="8" height="5" alt="" style=" float:right;padding: 7px 0;"/></a>
                   
</div>

<div class="ryt_boxEdit_dg">
<ul>
<li><a href="#"><span style="height:50px;"><img src="images/wag.png" width="48" height="46" alt="" /></span><p> Wag it</p></a></li>
<li><a href="#"><span style="height:50px;"><img src="images/share.png" width="38" height="38" alt="" /></span><p> Share</p></a></li>
<li><a href="#"><span style="height:50px;"><img src="images/edit.png" width="38" height="38" alt="" /></span><p> Edit</p></a></li>
<li><a href="#"><span style="height:50px;">
<img src="images/upload.png" width="48" height="48" alt="" /></span><p> Upload</p></a></li>
</ul>     
</div>


<div class="upload_act_wc">
<div class="act_wc">
<h2>EVERY DAY IS AN ADVENTURE</h2>
<p>Create your first club Post for kj to start capturing the simple, magical, everyday dog moments in life.</p>
</div>
<div class="btn_act_wc">Upload Activity</div>
</div>

<div class="activity_box_dg mt20">
<div class="act_box_dg">
<ul>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>
<li class="act_txt_dg">
<h2>Flyball</h2>
<p>Flyball is a growing dog sport involving a team knockout competition. 
It’s fast, fun, exciting and has a dedicated following at Crufts, the world’s 
largest dog show.
</p>
</li>
</ul>
</div>
<div class="act_box_dg">
<ul>
<li class="act_txt_dg">
<h2>Flyball</h2>
<p>Flyball is a growing dog sport involving a team knockout competition. 
It’s fast, fun, exciting and has a dedicated following at Crufts, the world’s 
largest dog show.
</p>
</li>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>

</ul>
</div>
<div class="act_box_dg">
<ul>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>
</ul>
</div>

<div class="act_box_dg">
<ul>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="200" alt=""></a>
</li>
</ul>
</div>


</div>
<!--<div class="slide_dog_dg">
<div class="slider_dg">
<ul>
<li><a href="#"><img src="images/slide1.jpg" width="200" height="181" alt="" /></a></li>
<li><a href="#"><img src="images/slide1.jpg" width="200" height="181" alt="" /></a></li>
<li><a href="#"><img src="images/slide1.jpg" width="200" height="181" alt="" /></a></li>
<li><a href="#"><img src="images/slide1.jpg" width="200" height="181" alt="" /></a></li>
</ul>
</div>
</div>

<div class="activity_box_dg">
<div class="act_box_dg">
<ul>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="181" alt="" /></a>
</li>
<li class="act_txt_dg">
<h2>Flyball</h2>
<p>Flyball is a growing dog sport involving a team knockout competition. 
It’s fast, fun, exciting and has a dedicated following at Crufts, the world’s 
largest dog show.
</p>
</li>
</ul>

<ul>
<li class="act_img_dg">
<a href="#"><img src="images/slide1.jpg" width="200" height="181" alt="" /></a>
</li>
<li class="act_txt_dg">
<h2>Flyball</h2>
<p>Flyball is a growing dog sport involving a team knockout competition. 
It’s fast, fun, exciting and has a dedicated following at Crufts, the world’s 
largest dog show.
</p>
</li>
</ul>
</div>-->
</div>

<?php //require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>