<?php
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');

if($userid=='Guest'){
	header("Location: /login.php?refUrl=/new/shop/myorders.php");
	ob_end_flush();
	exit(); 
}
$sitesection = "shop";
$session_id = session_id();?>
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link href="/css/linkbutton.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/new/js/jquery.min.js"></script>

<script type="text/javascript" src="/new/js/scrolltopcontrol.js"></script>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<script>
	$(document).ready(function() {
$('a.login-window').click(function() {
var loginBox = $(this).attr('href');
$(loginBox).fadeIn(300);
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 20) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
});
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});
</script>
<script>
	$(document).ready(function() {
$('a.login-window1').click(function() {
var loginBox = $(this).attr('href');
$(loginBox).fadeIn(300);
var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 20) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
});
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});
</script>
<div id="tabsetting" class="container dg_conbox">
<? 
$selectUserI = query_execute ("SELECT user_profile_status, updatenews, updatecomments FROM users WHERE userid = '$member_id'");
$rowI = mysql_fetch_array($selectUserI);
$user_profile_status=$rowI['user_profile_status'];
$updatenews=$rowI['updatenews'];
$updatecomments=$rowI['updatecomments'];


?>
<!--<ul class="tabs">
         
         <li><a href="#tab1"><span> Personal Setting</span></a></li>
        <li><a href="#tab2"><span>Privacy Setting</span></a></li>
    </ul>-->
  <div class="cb"></div>
   <div class="tab_container" style="border:none;"> 
    <div id="tab1" style="padding:20px 20px 0px 20px;" class="tab_content1">
<!--<a href="<? // echo"/dog-blog/author/$member_id/";?>" class="userbutton" style="text-align:mibble">Dog Blogs (<? // echo"$num_blog";?>)</a> -->
<h3>My Account</h3>
 <a href="/reg-log/more.php?edit=edit">Edit Profile
 </a><div class="vs10"></div>

<a href="#login-box" class="login-window">Edit Photo</a>

  

 <div class="vs10"></div>
 <div class="hrlineSolid"></div>
 <div class="vs10"></div>
<h3>Privacy settings</h3>
<a href="#login-box1" class="login-window1">Change Password</a><div class="vs10"></div>

<a href="/wag_club/del-profile-img.php?del=profileImg">Delete photo </a><div class="vs10"></div>

<a href="/home.php?del_member=<? echo"$member_id"; ?>&logoff=logoff" onclick="return confirm('Do you want delete?');">Delete my account </a> 


<div class="cb"></div>
 <div class="vs10"></div>
 <div class="hrlineSolid"></div>
 <div class="vs10"></div>
 

 
<div class="cb"></div>
</div>
<div id="tab2" style="padding:0px 20px 0px 20px;" class="tab_content1">
<div id="mis23"></div>
<form id="usersetting" name="usersetting" method="post">
<h3>Email Settings</h3>
<table border="0" width="100%">
<tr>
<td width="70%">Send me alerts if come one comments on my photo, article, QNA</td><td><div>
[<input  type="radio" name="alert" id="alert"  value="Y" <? if($updatecomments=='Y'){ ?>checked="checked"<? }?>>Yes</input>
<input  type="radio" name="alert" id="alert"  value="N" <? if($updatecomments=='N'){ ?>checked="checked"<? }?>>No</input>]
</div></td></tr>

<tr>
<td width="70%">Do not send me Shop related newsletters</td><td><div>
[<input  type="radio" name="sendnews" id="sendnews"  value="Y" <? if($updatenews=='Y'){ ?>checked="checked"<? }?>>Yes</input>
<input  type="radio" name="sendnews" id="sendnews" value="N" <? if($updatenews=='N'){ ?>checked="checked"<? }?>>No</input>]
</div></td></tr>

<tr><td colspan="2">
<div style="font-size:9px">Note:
You will get shopping order tracking emails when you order products online</div>
 </td></tr>
 <tr><td colspan="2"><hr />
 <h3>Privacy Engine</h3></td></tr>
 <tr><td width="70%">Only my DogSpot friends can see my profile</td><td>
 <div>
 
<input  type="radio" name="seeprofile" id="seeprofile" value="Friend" <? if($user_profile_status=='Friend'){ ?>checked="checked"<? }?>></input>

</div></td></tr>
 <tr><td width="70%">All DogSpot registered users can see your profile</td><td><div>
<input  type="radio" name="seeprofile" id="seeprofile"  value="Dogspot" <? if($user_profile_status=='Dogspot'){ ?>checked="checked"<? }?> /></input>
</div></td></tr>
<tr><td>no one can see your profile</td><td><div>
<input  type="radio" name="seeprofile" id="seeprofile" value="Private" <? if($user_profile_status=='Private'){ ?>checked="checked"<? }?> ></input>
</div></td>
</tr>
<tr><td>Any can see your profile</td><td><div>
<input  type="radio" name="seeprofile" id="seeprofile" value="Public" <? if($user_profile_status=='Public'){ ?>checked="checked"<? }?> ></input>
</div></td>
</tr>
<tr><td>
<? $userid12=$member_id; ?>
<input type="button" name="save23" id="save23" value="save" style="margin: 10px 0;padding: 3px 10px;width: 100px;font-size: 15px;text-transform: uppercase;"/>
<input type="hidden" name="member_id" id="member_id" value="<?= $member_id;?>"/>

</td><td>
</td></tr></table>
</form>
<div id="login-box1" class="login-popup">
					<?php require_once($DOCUMENT_ROOT.'/wag_club/resetpass.php'); ?></div>
                    <div id="login-box" class="login-popup">
					<?php require_once($DOCUMENT_ROOT.'/wag_club/user-profile-img.php'); ?></div>
</div>
</div>
</div>
</div>