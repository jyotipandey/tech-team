<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Adoption</title>
<style>
.AboutDogAdoption
{
 float:left;
 width:100%;
}
.AboutDogAdoption h1,h2
{
color: #668000;
width: 100%;
font-size: 20px;
margin: 0px;
font-weight: normal;
text-align: left;
margin-bottom:5px;

}
.AboutDogAdoption p{
margin:0px;
padding: 0px;
font-size: 16px;
text-align: left;
margin-bottom:5px;
}
	
</style>
</head>

<body>
<div class="AboutDogAdoption">
<h1>What is adoption at DogSpot?</h1>
<h2>What we do?</h2>

<p>In an effort to aid adoptions and fostering of dogs and cats across the country, DogSpot is proud to introduce an online portal presenting all animals that are available pan-India on one place. This aids various shelters and activists in expediting the process of screening and placing animals. </p>

<h2>How?</h2>

<p>DogSpot will have an extensive database of pan-India rescued animals looking for care and forever homes. 
Interested parties may apply to adopt the dog/cat they may be interested in by filling up a simple questionnaire.</p> 

<p>After internal screening, someone from our end will get back to you for further questioning. Once all the information is aligned and verified, the potential candidate will be then connected to the caretaker of the respective pet to take forward the adoption formalities. </p>

<h2>Why?</h2>

<p>Our ultimate aim is welfare of animals who we so dearly care for. Dogspot aims to provide a unanimous portal where one can find animals to foster and adopt. In addition, one can also upload animals on their own which they would like to put up for foster care or adoption. </p>

<h2>What is our mission? </h2>

<p>"To make the world a better place. To help expedite the adoption process for the activists and find loving families for poor abandoned animals".</p>
</div>
 <?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
