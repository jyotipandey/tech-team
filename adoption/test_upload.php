
<?php
ini_set("post_max_size", "400M");
ini_set("upload_max_filesize", "200M");
ini_set("max_input_time", "3000");
ini_set("max_execution_time", "3000");
ini_set("memory_limit", "990M");

require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/check_img_function.php');

$upload_location = $DOCUMENT_ROOT."/vet_img/";

if($_POST['upload']){
	$file_name = $_FILES['img']['name'];
	$file_tmp = $_FILES['img']['tmp_name'];
	
	$temp = explode(".", $_FILES['img']['name']);
	$newfilename = round(microtime(true)) . '.' . end($temp);
	
	$file_array = array('file_path'=>$upload_location,'new_name'=>$newfilename,'temp_name'=>$file_tmp);
	
	if(move_uploaded_file($_FILES['img']['tmp_name'], $upload_location.$newfilename)){
		$return = checkImgType($file_array);
		echo $return['status'];
		if($return['status']=="Safe"){
			if (file_exists($upload_location.$newfilename)) {
				chmod($upload_location.$newfilename, 0644);
				echo "File has been uploaded successfully";
			}
		}else{
			echo "This is a not valid image, Please upload a valid ";
		}
	}else{
		echo "File hassassssss not uploaded successfully";
	}
}
  

?>

<html>
<head>
<title>Upload test</title>
</head>
<body>
	<form action="" method="post" name="frmuplaod" enctype="multipart/form-data">
    	<input type="file" name="img" id="img" />
        <input type="submit" name="upload" id="upload" value="Upload" />
    </form>
    <br/>
    <br/>
    
</body>
</html>