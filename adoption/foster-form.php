<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
include($DOCUMENT_ROOT."/constants.php");
include($DOCUMENT_ROOT."/session-no.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/dogs/arraybreed.php");
include($DOCUMENT_ROOT."/common/countries.php");
include($DOCUMENT_ROOT."/Adoption1/fostermail.php");

require_once($DOCUMENT_ROOT . '/arrays.php');

$ajaxbody = "puppie";
$sitesection = "puppie";
?>

<?

// Set Order in query
if($order == "DESC"){
 $order = "ASC";
}else{
 $order = "DESC";
}

//------------Show
$maxshow = 20;
if (empty($show)) {
	$show=0;
}else{
$show = $show - 1;
}
$showRecord = $show * $maxshow;
$nextShow = $showRecord + $maxshow;?>

<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');

?>
<script src="/Adoption1/js/jquery.cycle2.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? $pageTitle="Foster Me";
$pageKey="";
$pageDesc="";?>
<title><?=$pageTitle?> <?php if($section[3]) {echo " | ".$titlepage;}?> <? if($section[1]=="breed"){ echo " | Breed"; }elseif($section[1]=="page")
{echo " | Page ". $section[2];} ?> </title>
<meta name="keywords" content="<?=$pageKey?> <?php if($section[3]) {echo " , ".$titlepage;} if($section[1]=="breed"){ echo " , Breed"; }elseif($section[1]=="page") {echo ", Page ". $section[2];}?>"/>
<meta name="description" content="<?=$pageDesc?> <?php if($section[3]) {echo " , ".$titlepage;} if($section[1]=="breed"){ echo " , Breed"; }
elseif($section[1]=="page") {echo ", Page ". $section[2];}?>" />

<? if($titlepage > ($totalpage+1)){ ?>
<meta name="robots" content="noindex, follow"> <? } ?>
<!--<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
--><?php /*?><link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />
<link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />
<link rel="stylesheet" type="text/css" href="/new/css/business_listing.css" media="all"  /><?php */?>
<link rel="canonical" href="https://www.dogspot.in/adoption/foster-form.php" />
<link rel="stylesheet" type="text/css" href="/shapopup/shapopup.css" media="all"  />
<link href="/css/common.css?v=020112" rel="stylesheet" type="text/css" />
<link href="/new/css/style1.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="/Adoption1/css/style.css">
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<script src="/js/ajax.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/highslide.js"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script src="/shapopup/shapopup.js" type="text/javascript"></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<script> function housing()
{
	if($('#housing').val()!=''){
	$('#housing1').css("display","block");
	}
}
function checkonpinedit()
{
	//alert('34');
	var pin = $('#pincode').val();
		var count=pin.length;
		if(count==6){
			//alert(count);
		ShaAjaxJquary('/new/shop/checkpincode.php?address_zip='+pin+'', '#citystate34', '','', 'post', '#citystate34', '', 'REP');
		//ShaAjaxJquary('/shop/checkcod2.php?pin='+pin+'', '#codbox2', '', '', 'post', '#codbox2', '', 'REP');
		setTimeout('check2()',2000);
		
	}
}
function check2()
{   
	var citystate=$('#citystate').val();
	//alert(citystate);
	if(citystate!='') {
	var a = citystate.split(",") // Delimiter is a string
    var city=a[0];
	var state=a[1];
	var ab = state.split(";") 
	$('#city').val(city);
	$('#state').val(ab[0]);
	
	//alert(city);
	//alert(state);
	if(city=='' || state==''){
		//alert('de'+city);
		//document.getElementById("address_city").readOnly = 'false';
	$("#city").attr("readonly", false);


	$("#state").attr("readonly", false);
	
	}else{
	$("#city").attr("readonly", false);
	$("#state").attr("readonly", true);
			}
	}
	
}
function checkCheckBoxes() {
	if ($("#Medical:checked").length == 0) 
	{
		$('#dis').css("display","block");
		return false;
	} else { 	
	$('#dis').css("display","none");
		return true;
	}
    }</script>
<style>
.form-headline{color: #668000;width: 100%;border-bottom: 3px solid #668000;padding: 5px; font-size: 20px;text-align: left; margin: 0px;font-weight: normal;}
    
   
    
	
</style>

</head>

 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
    <!--==============================content================================-->
  <div class="breadcrumb">
            	<div class="cont980" style="">
                 
                 <div style="float:left;width:930px"><a href="/">Home</a> » <a href="/adoption/">Adoption</a> » Form
</div>
                  <div>
                    
                                              
                        <a href="https://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a><a href="https://twitter.com/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a>
                     </div>
                  <div class="cb"></div>
                 </div>
            
            </div>  <section id="content">

      <div class="petAdoption_MainBlock">
        <div class="col-1">
         <!-- foster form start-->  
		 <?
		  if($_POST){
			  $text="<div style='color:#668800; text-align:left; margin-top:30px; font-size:20px; border:1px solid #ddd; padding:10px; '>Hi ".$aplication.'<br/>';
			  $text.="<p>Thank you for your time and input. Our representative will get in touch with you via email/phone if you are compatible for  any dog or puppy in our database.</p></div>";
			  echo  $text;
			  
			// echo "ad".$aplication.$_POST['aplication'];
			$dob=$year.'-'.$month.'-'.$date;	
			$time=$timehour;
			$aplication=mysql_real_escape_string($aplication);
			$housing=mysql_real_escape_string($housing);
			$gated=mysql_real_escape_string($gated);
			$caretaker=mysql_real_escape_string($caretaker);
			$money=mysql_real_escape_string($money);
			$m=$money.$Medical;
			$call=$start.' '.$startperiod.'To'.$end.' '.$endperiod;
			fostermail($aplication,$email,$city,$state,$pincode,$dob,$mobile,$call,$no_of_family,$no_age_child,$housing,$gated,$fenced,$residence,$Before,$otherpet,$caretaker,$time,$m,$scheduled);
			//echo "INSERT INTO `foster-details`(`aplication_name`, `full_name`, `city`, `state`, `state_id`, `state_nicename`, `pincode`, `dob`, `mobile`, `best-time-call`, `no-of-family-member`, ` no-age-of-child`, `type-of-housing`, `gated-community`, `fenced-yard`, `member-agree`, `another-dog`, `foster-dog`, `caretaker`, `timing`, `money`,scheduled) VALUES ('$aplication','','$city','$state','$state','','$pincode','$dob','$mobile','$call','$no_of_family','$no_age_child','$housing','$gated','$fenced','$residence','$Before','$otherpet','$caretaker','$time','$m','$scheduled')";
			$satate=query_execute_row("SELECT * FROM `state` WHERE `state_id`='$state'");
			$num=mysql_query("INSERT INTO `foster_details`(`aplication_name`, `full_name`, `city`, `state`, `state_id`, `state_nicename`, `pincode`, `dob`, `mobile`, `best_time_call`, `no_of_family_member`,no_of_child, `type_of_housing`, `gated_community`, `fenced_yard`, `member_agree`, `another_dog`, `foster_dog`, `caretaker`, `timing`, `money`,scheduled) VALUES ('$aplication','','$city','".$satate['state_name']."','$state','".$satate['state_nicename']."','$pincode','$dob','$mobile','$call','$no_of_family','$no_age_child','$housing','$gated','$fenced','$residence','$Before','$otherpet','$caretaker','$time','$m','$scheduled')");	 
		 }else{?>
		<div class="foster-form">
		<h1>Personal Details</h1>
		<form action="" method="post" onsubmit="return checkCheckBoxes()">
		<div class="personaldetailsDiv">
        <div id="citystate34" style="display:none"></div>
        <div style="font-size:16px; color:#333; margin-top:5px; margin-bottom:5px;"> All fields mandatory </div>
		<div>
			<label class="adoption_ques_label">Applicant Name:</label>
		<input  class="adoption_ques_input" type="text" name="aplication" required="required" autocomplete="off">
		</div>
		<?php /*?><div>
			<label class="adoption_ques_label">Full Name:</label>
		<input  class="adoption_ques_input" type="text" name="full" required="required" autocomplete="off">
		</div><?php */?>
        <div>
			<label class="adoption_ques_label">Pincode:</label>
		<input  class="adoption_ques_input" type="text"  name="pincode" id="pincode" onblur="checkonpinedit();" onchange="checkonpinedit();" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required="required" max="6" min="6" autocomplete="off">
		</div>
        
         <div>
         <?php $email=$userid?useremail($userid):'';?>
			<label class="adoption_ques_label">Email:</label>
		<input  class="adoption_ques_input" type="text"  name="email" id="email" value="<?=$email?>" required="required" max="6" min="6" autocomplete="off" style="padding:0 7px !important;">
		</div>
		
		
		
		<div>
		<label class="adoption_ques_label">Date of birth:</label>
	<select id="fosterform_drop" class="adoption_ques_input" name="date"><option>Date</option>
	<? for($i=1;$i<=31;$i++){?>
     <option value="<?=$i?>"><?=$i?></option>
	 <?
	}?></select>
	<select id="fosterform_drop" class="adoption_ques_input" name="month" style="width:69px;"><option>Month</option>
    <option value="1">January</option>
				<option value="2">February</option>
				<option value="3">March</option>
				<option value="4">April</option>
				<option value="5">May</option>
				<option value="6">June</option>
				<option value="7">July</option>
				<option value="8">August</option>
				<option value="9">September</option>
				<option value="10">October</option>
				<option value="11">November</option>
				<option value="12">December</option></select>
	<select id="fosterform_drop" class="adoption_ques_input" name="year"><option>Year</option>
    <? //$month=date('m');
				
					//$year=date('Y');
				
				
				for($i=1959;$i<=2005;$i++)
				{
					echo '<option value='.$i.'>'.$i.'</option>';
					}?></select>
		</div>
		<div>
		<label class="adoption_ques_label">Mobile No:</label>
	<input class="" value="+91" style="border: 1px solid #a5ce08; width: 35px; height: 30px; border-radius: 5px; margin-top: 5px; margin-bottom: 5px; text-align:center;" type="text" name="mobile" required="required" readonly> <input  class="" style="border: 1px solid #a5ce08; width: 210px; height: 30px; border-radius: 5px; margin-top: 5px; margin-bottom: 5px; text-align: left;" type="text" name="mobile" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required="required" maxlength="13" min="10"  autocomplete="off">
		</div>
		<div>
	<label class="adoption_ques_label">Best time to call:</label>
		<select name="start" style="width:60px; border: 1px solid #A5CE08; height:30px;" required><option value="" >Time</option><? for($j=1;$j<=12;$j++){?>
        <option value="<?=$j?>"><?=$j?></option><? }?></select>
        <select name="startperiod" required style="width:50px; border: 1px solid #A5CE08; height:30px;">
        <option value="am">AM</option><option value="pm">PM</option></select> To
        	<select  style="width:60px; border: 1px solid #A5CE08; height:30px;" name="end" required>
            <option value="">Time</option><? for($j=1;$j<=12;$j++){?>
        <option value="<?=$j?>"><?=$j?></option><? }?></select>
        <select style="width:50px; border: 1px solid #A5CE08; height:30px;" name="endperiod" required>
        <option value="am">AM</option><option value="pm">PM</option></select>
		</div>
        <div>
			<label class="adoption_ques_label">City:</label>
		<input  class="adoption_ques_input" type="text" name="city" id="city" required="required" autocomplete="off">
		</div>
		<div>
			<label class="adoption_ques_label">State:</label>
            <select name="state" id="state" class="adoption_ques_input" required>
            <option value="">Select State</option>
         <?   $state1=mysql_query("SELECT * FROM state");
	while($fetch_city1=mysql_fetch_array($state1)){;
	$state_name=$fetch_city1['state_name'];
	$state_id=$fetch_city1['state_id']; 
	?><option value="<?=$state_id?>"><?=$state_name?></option><? }?>
    </select>
		
		</div>
		<!-- household -->
		</div>
		
		<div class="form-headline">
		Household
		</div>
		<div class="householdDiv">
		<div>
		<label class="personaldetails_label">Number of Family Members Sharing Residence:</label>
        <select style="margin-top:-15px;" name="no_of_family" class="household_input" required>
        <option value="">Select</option>
        <? for($j=1;$j<=15;$j++){
			?><option value="<?=$j?>"><?=$j?></option><?
		}
        ?>
        </select>
		</div>
		
		<div>
		<label class="personaldetails_label">Number And Age of Children In the Residence:</label>
        <input type="text" class="household_input" name="no_age_child" style="margin-top:-15px;" required="required">
		</div>
		
		
		<div>
	<label class="personaldetails_label">Type Of Housing:</label>
    <select name="housing" id="housing" required class="household_input" >
     <option value="" onclick="housing()">Select House</option>
    <option value="owned" onclick="housing()">Owned</option>
    
     <option value="rented" onclick="housing()">Rented</option>
    </select>
     <select name="housing1" id="housing1" style="display:none">
    <option value="apartment">Apartment</option>
     <option value="independent">Independent</option>
      <option value="Farm House">Farm House</option>
    </select>
		</div>
		
		
		<div>
		<label class="personaldetails_label">Do You Live In A Gated Community?</label>
        <select name="gated" required class="household_input">
        <option value="">Select</option>
        <option value="yes">Yes</option>
         <option value="no">No</option>
         </select>
		</div>
		
		<div>
		<label class="personaldetails_label">Do You Have A fenced Yard (In-House or Nearby)?</label> 
        <select name="fenced" required class="household_input">
        <option value="">Select</option>
        <option value="yes">Yes</option>
         <option value="no">No</option>
         </select>
		</div>
     
		<div>
		<label class="personaldetails_label">Is everyone in your current residence ready to foster a dog?</label> 
        <select name="residence" required class="household_input">
        <option value="">Select</option>
        <option value="yes">Yes</option>
         <option value="no">No</option>
         </select>
		</div>
		</div>
		<!--household-->
		
		<!-- Foster info -->
		
		<div class="form-headline">
		Foster info
		</div>
		<div class="foster_infoDiv">
		<div>
		<label class="fosterinfo_label">Have You Owned A Dog Before? Please share details:</label>
		<input type="text" class="fosterinfo_input" name="Before" required="required">
		</div>
		
		<div>
		<label class="fosterinfo_label">
		Do you own any other pets besides willing to foster a new dog? (Please give details of species/breeds/age/gender)</label>
		<input type="text" class="fosterinfo_input" style="margin-top:-15px;" name="otherpet" required="required">
		</div>
		
		
		<div>
		<label class="fosterinfo_label">Who will be the primary caretaker of the dog?:</label>
		<input type="text" class="fosterinfo_input" name="caretaker" required="required">
		</div>
		
		
		<div>
		<label class="fosterinfo_label">How much time in a day will you be willing to provide to the

dog? (For exercise and training)</label>
         <select name="timehour" required class="householdDrop">
         <option value=" ">Select Hour</option>
       
       <? for($l=0;$l<=60;$l++){?> <option value="<?=$l?>"><?=$l?></option><? }?>
        
         </select>
		</div>
		
		<div>
		<label class="fosterinfo_label">How much will you be able to cover the finances for the upkeep 

of the dog? (Medical/Feeding/Residence) 
</label>
		
		<input type="checkbox" name="Medical" id="Medical" value="Medical"><span>Medical</span>
		<input type="checkbox" name="Medical" id="Medical" value="Feeding"><span>Feeding</span>
        <input type="checkbox" name="Medical" id="Medical" value="Residence"><span>Residence</span>
        <div id="dis" style="display: none; margin-left:415px; color:#f00">This is required field</div>
		

		</div>
     <div>
    <label class="fosterinfo_label"> Please specify a realistic figure.</label>
    <input type="text" class="fosterinfo_input" name="money" required="required">
     </div>
		<div>
		<label class="fosterinfo_label">Are You Comfortable with scheduled house-checks from our

representative as well as someone from the NGO?</label>
 <select name="scheduled" class="household_input" style="margin-top:-15px;" required>
 <option value="">Select</option>
        <option value="yes">Yes</option>
         <option value="no">No</option>
         </select>
		</div>
		</div>
		<!--Foster info-->
		<div class="foster_submitbtnDiv">
		<input type="Submit" value="Submit" class="fosterform_submitbtn" > 
		</div>
        </form>
		</div>
		<? }?>
		
		
		<!-- form end-->
</div>	
<!-- foster form End-->	 
		
		</div>
		<!-- right side end-->
		<!-- left side-->
        <div class="col-2 left-2 sidebar">
        <div  class="Dog_adoption_Banner">
	<? if($userid !='Guest'){?>
	<a href="/adoption/new.php"><img src="https://www.dogspot.in/Adoption1/img/adoption_upload_your_banner.jpg"></a>
    <? }else{?>
    <a href="/login.php"><img src="https://www.dogspot.in/Adoption1/img/adoption_upload_your_banner.jpg"></a>
  
    <? }?>	</div>
		<!-- foster pet-->
          <div class="Foster_pet" >
			   <h2>Foster a pet</h2>
			   <a href="#"><img src="/Adoption1/img/2.png"></a>
			   <p>Would you be interested in Fostering a dog/puppy? Click below to fill up the form.</p>
			   <div class="foster-me"> <a href="/adoption/foster-form.php"><input type="submit" name="Foster me" value="Foster me" class="fm_btn"></a></div>
               </div>
			   <!-- foster pet-->
			   <!-- you may like post-->
			     <?
			   mysql_select_db("test");
			    $article=query_execute("SELECT wpp.ID , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wptr.term_taxonomy_id = '10' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category'  AND wpp.post_date>'2014-01-01 00:00:00' ORDER BY wpp.ID DESC");
				?>
			    <div class="youmaylike_post" >
			   <h2>You may also like to read</h2>
			   <div style="z-index:0;" class="cycle-slideshow composite-example" 
    data-cycle-fx="scrollHorz" 
    data-cycle-slides="> div"
    data-cycle-timeout="2000"
    ><?
               while($rowArt=mysql_fetch_array($article)){
				$post_name   = $rowArt["post_title"];
				$cat_name=$rowArt['post_name'];
				$main_fea_art_body=$rowArt['post_content'];
				$main_fea_art_id=$rowArt['ID'];
			$feat_imgURL    = get_first_image($main_fea_art_body, $DOCUMENT_ROOT);
	       $imgURLAbs = make_absolute($feat_imgURL, 'https://www.dogspot.in');
		   $feat_check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$main_fea_art_id' AND post_type='attachment' AND post_mime_type like '%image%'");
			
		if (strpos($imgURLAbs,'wordpress') !== false) {
    	$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/280x205-'.$image_name;
		}
		else if($feat_check_img['guid'] !=''){
		$imgURLAbs=$feat_check_img['guid'];	
		$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/280x205-'.$image_name;
		}
   else if ($imgURLAbs!='https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
	   $URL=$imgURLAbs;
	   $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
	   $pos = strrpos($image_name,'/');
	   $image_name = substr($image_name,$pos+1);
		$extension = stristr($image_name,'.');
		if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
		$image_name;
		}
		$src = $DOCUMENT_ROOT.'/userfiles/images/'.$image_name;
		//$src = $imgURLAbs;
		$imageURL='/imgthumb/280x205-'.$image_name;
		} 
		else if($rowUser1["image"]) {
		$src = $DOCUMENT_ROOT.'/profile/images/'.$rowUser1["image"];
		$imageURL = '/imgthumb/280x205-'.$rowUser1["image"];
		} 
		else  {
        $src = $DOCUMENT_ROOT.'/images/black_dog_02_139885.jpg';
		$imageURL = '/imgthumb/280x205-black_dog_02_139885.jpg';
		}
		
		$dest = $DOCUMENT_ROOT.$imageURL;
		createImgThumbIfnot($src,$dest,'280','205','ratiowh');
				?>
                
    <div >
		  <a href="/<?=$cat_name?>/"> <img src="<?=$imageURL?>" alt="<?=$cat_name ?>" title="<?=$cat_name ?>" style="width:280px;height:205px;"></a>
			    <div class="cycle-overlay"><h3><?=snippetwop($post_name,$length=25,$tail="...");?></h3></div></div>
               <? }?>
               </div>	
			   <!-- you may like post-->
                 <!-- email box-->
               <div class="adoption_emailbox">
<div class="adoption_emailtext">Have any questions regarding Adoption/Foster?</div>

<div class="adoption_emailid">Write to us at <a href="mailto:name@rapidtables.com">adoption@dogspot.in</a> </div>
</div>
               <!-- email box->
        
		<!-- left side End-->
		</div>
		</div>
		</section>
		<!--end-->
        <div class="clear"></div>

  <?php
  //mysql_select_db("dogspot_dogspot");
  require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?> 
