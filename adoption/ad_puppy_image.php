<?php
	ini_set("post_max_size", "400M");
	ini_set("upload_max_filesize", "200M");
	ini_set("max_input_time", "3000");
	ini_set("max_execution_time", "3000");
	ini_set("memory_limit", "990M");
	require_once($DOCUMENT_ROOT.'/constants.php');
	require_once($DOCUMENT_ROOT.'/session.php');
	require_once($DOCUMENT_ROOT.'/database.php');
	require_once($DOCUMENT_ROOT.'/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
	require_once($DOCUMENT_ROOT.'/arrays.php');
	require_once($DOCUMENT_ROOT.'/check_img_function.php');
	
	$upload_location = $DOCUMENT_ROOT."/puppies/images/";
	$newfilename = '';
	if($_FILES['vasPhoto_uploads']['name']){
		$size = $_FILES['vasPhoto_uploads']['size'];
		
		$temp = explode(".", $_FILES['vasPhoto_uploads']['name']);
		$newfilename = round(microtime(true)) . '.' . end($temp);	
	
		$image_info = getimagesize($_FILES["vasPhoto_uploads"]["tmp_name"]);
		$image_width = $image_info[0];
		$image_height = $image_info[1];
		if($image_width>350 && $image_height>350){
			echo '<input id="pac_image" name="pac_image" type="hidden" value="'.$newfilename.'" />';
			if(move_uploaded_file($_FILES['vasPhoto_uploads']['tmp_name'], $upload_location.$newfilename)) {
				
				// Check uploaded file is valid or not
				$file_array = array('file_path'=>$upload_location,'new_name'=>$newfilename,'temp_name'=>$_FILES['vasPhoto_uploads']['tmp_name']);
				$return = checkImgType($file_array);
				if($return['status']=="Safe"){
					if (file_exists($upload_location.$newfilename)) {
						chmod($upload_location.$newfilename, 0644);
						echo "File has been uploaded successfully".$newfilename;
						//Run your SQL Query here to insert the new image file named $actual_image_name if you deem it necessary
						$img=$newfilename;
						$imglink455='/puppies/images/200-200-'.$img;
						//$imglinkm='/imgthumb/100-'.$imgFL[0];
						$src1=$DOCUMENT_ROOT.'/puppies/images/'.$img;
						$dest1 = $DOCUMENT_ROOT.$imglink455;
						//$destm = $DOCUMENT_ROOT.$imglinkm;
						createImgThumbIfnot($src1,$dest1,'200','200','ratiowh');
						echo '<span class="uploadeFileWrapper" style="margin-left:113px;">
							<img src="/puppies/images/200-200-'.$newfilename.'" id="imagepicchange"></div></span><br clear="all" /><br clear="all" />';
					}
				}else{
					echo "<div class='notice' style='width:500px; color:red;>Sorry, Your Image File could not be uploaded at the moment. <br>Please try again or contact the site admin if this problem persist. Thanks.</div><br clear='all' />";
				}
				// End Uploaded file check
			}else {
				echo "<div class='notice' style='width:500px;'>Sorry, Your Image File could not be uploaded at the moment. <br>Please try again or contact the site admin if this problem persist. Thanks.</div><br clear='all' />";
			}
		}else{
			 echo "<div class='notice' style='width:500px;'>Sorry, Your Image File could not be uploaded.Please upload an image above 350*350.</div><br clear='all' />";	  
		}
	}else{
	?>
        <script>$("#crop_img21").hide();</script>
    <?php } ?>
