<?php
include($DOCUMENT_ROOT."/constants.php");
include($DOCUMENT_ROOT."/session-no.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/dogs/arraybreed.php");
include($DOCUMENT_ROOT."/common/countries.php");

require_once($DOCUMENT_ROOT . '/arrays.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<link rel="canonical" href="https://www.dogspot.in/adoption/what-is-adoption.php" />

<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>

<script type='text/javascript' src='/ajax-autocomplete/jquery.autocomplete.js'></script>

<script type='text/javascript' src='/js/shaajax2.js'></script>
 
   <script src="/Adoption1/js/jquery.hoverpulse.js"></script>
<script type="text/javascript" src="/js/highslide.js"></script>
<script src="/adoption/js/jquery.cycle2.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery-ui.js"></script>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>What is adoption at DogSpot | Welfare of animals</title>
 <link rel="stylesheet" href="/adoption/css/stylef.css">
  <link rel="stylesheet" href="/adoption/css/font-awesome.css">
  <?php /*?><script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script><?php */?>

<meta name="keywords" content= "What is pet adoption ,Welfare of animals" />
<meta name="description" content="Adoption at DogSpot is  an effort to aid adoptions and fostering of dogs and cats across the country, DogSpot is proud to introduce an online portal for Adoption." />
<?php
/*?><link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />
<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />
<link rel="stylesheet" type="text/css" href="/new/css/business_listing.css" media="all"  /><?php */
?>
<!--<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />-->
<link rel="stylesheet" type="text/css" href="/shapopup/shapopup.css" media="all"  />
<link href="/css/common.css?v=020112" rel="stylesheet" type="text/css" />
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" media="screen" href="/adoption/css/style.css">
   
 
    
<?php
/*?>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>

<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="/shapopup/shapopup.js" type="text/javascript"></script>
<script src="/js/ajax.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/highslide.js"></script>
<?php */
?> 


<!--<link href="/css/highslide.css" rel="stylesheet" type="text/css" />-->

<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/adoption/css/style-slider.css?=s2" type="text/css" media="screen" charset="utf-8" />

    <script type="text/javascript" src="/new/js/jquery/jquery-ui.js"></script>
<script src="/adoption/js/slider.js" type="text/javascript" charset="utf-8"></script>
    
 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="breadcrumb">
            	<div class="cont980" >
                 
                 <div class="breadcrumb_cont"><a href="/">Home</a> <span>></span> 
                 <span class="brd_font_bold">
                 <a href="/adoption/">Adoption</a>
                 </span>
</div>
                  
                  <div class="cb"></div>
                 </div>
            
            </div>
<div class="cont980">
              <? 
		require_once($DOCUMENT_ROOT . '/banner1.php');
              addbannerallsite(); ?>

</div>
            <section id="content">

      <div class="petAdoption_MainBlock">
      <div class="col-1"> 
      <div class="petadoption_fileter">
         <form id="formSearch1" name="formSearch1" method="get" onsubmit=" return searchsubmit();" action="/adoption/search.php">
          <label style="display:none;" id="searcherror"><font color="#FF0000">You need to select atleast one of the filters</font></label>
		 <select name="dog_breedSearch" id="dog_breedSearch" style="width: 225px; height: 32px; border: 1px solid rgb(102, 128, 0); margin-bottom: 10px; margin-top: 10px; margin-right: 8px;">
         <option value="">Select Breed</option>
          <? $breed=mysql_query("SELECT b.breed_nicename,bn.breed_name FROM puppies_available as b,dog_breeds as bn WHERE b.adoption='yes' AND b.breed_nicename=bn.nicename");
		  while($breed_detail=mysql_fetch_array($breed))
		  {
			  $ArrDogBreed1[$breed_detail['breed_nicename']]=$breed_detail['breed_name'];
		  }
		  foreach($ArrDogBreed1 as $KeyBreedNice => $BreedName){
			 print "<option value='$KeyBreedNice'";
		     if($KeyBreedNice == "$dog_breedSearch"){ echo"selected"; } print ">$BreedName</option>";
		   }
		 ?><option <? if($dog_breedSearch=="cats"){ echo"selected"; } ?> value="cats">Cats</option></select> 
         <select name="citySearch" id="citySearch" style="width:199px;"> <option value="">Select Current Location</option>
          <?php
              $queryCitySearch = mysql_query("SELECT distinct(city_id),city_nicename,city FROM puppies_available WHERE adoption='yes' ");
               if (!$queryCitySearch){	die(sql_error());	}
               while($rowCitySearch = mysql_fetch_array($queryCitySearch)){
              $city_idSearch = $rowCitySearch["city_id"];
                $cityNameSearch = $rowCitySearch["city"];
                $city_niceSearch = $rowCitySearch["city_nicename"];
              
                print "<option value='$city_idSearch'";
                if($city_niceSearch == $citySearch){ echo "selected";  }  print ">$cityNameSearch</option>";
               }
            ?>
         </select> 
          <?php /*?><select name="dog_statusSearch" id="dog_statusSearch"> <option value="">Select Status</option>
           <option value="rescue">Rescue</option>
            <option value="foster">Foster</option>
             <option value="adopted">Adopted</option></select><?php */?>
              <input type="submit" name="searchBut" id="button" value="  Go  " class="go_btn" style="cursor:pointer"></form></div>
<div class="AboutDogAdoption">
<h1>What is adoption at DogSpot?</h1>
<h2>What we do?</h2>

<p>In an effort to aid adoptions and fostering of dogs and cats across the country, DogSpot is proud to introduce an online portal presenting all animals that are available pan-India on one place. This aids various shelters and activists in expediting the process of screening and placing animals. </p>

<h2>How?</h2>

<p>DogSpot will have an extensive database of pan-India rescued animals looking for care and forever homes. 
Interested parties may apply to adopt the dog/cat they may be interested in by filling up a simple questionnaire.</p> 

<p>After internal screening, someone from our end will get back to you for further questioning. Once all the information is aligned and verified, the potential candidate will be then connected to the caretaker of the respective pet to take forward the adoption formalities. </p>

<h2>Why?</h2>

<p>Our ultimate aim is welfare of animals who we so dearly care for. Dogspot aims to provide a unanimous portal where one can find animals to foster and adopt. In addition, one can also upload animals on their own which they would like to put up for foster care or adoption. </p>

<h2>What is our mission? </h2>

<p>"To make the world a better place. To help expedite the adoption process for the activists and find loving families for poor abandoned animals".</p>
</div>
<?
$ip = $_SERVER['REMOTE_ADDR'];
$details = json_decode(file_get_contents("https://ipinfo.io/{$ip}"));
$locity = $details->city;
if($locity!=''){
?> 
<div id="slider">   

<div class="related_pets">More Pets From <?=$locity;?></div>
<div class="scrollButtons left sb_icon_left"></div>
           

<div style="overflow: hidden;" class="scroll">
   <?
$qdataM1=mysql_query("SELECT `puppi_img`, city, puppi_name, puppy_nicename FROM `puppies_available` WHERE city='$locity' AND `puppi_img`!='' AND puppi_name!='' AND publish_status='publish' ORDER BY c_date DESC");
while($sqlgetqdata=mysql_fetch_array($qdataM1)){
$imdet=$sqlgetqdata['puppi_img'];
$puppy_nicename=$sqlgetqdata['puppy_nicename'];
if($imdet){
$src = $DOCUMENT_ROOT.'/puppies/images/'.$imdet;
$imageURL121='/puppies/images/145x143_'.$imdet;
//$imageURLface='/puppies/images/350x210-'.$sqlimage1;

}else{
$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
$imageURL121='/dogs/images/no-photo-t.jpg';
}

$dest = $DOCUMENT_ROOT.$imageURL121;
//echo "jyoti". $src.$dest;
//$dest1 = $DOCUMENT_ROOT.$imageURLface;
createImgThumbIfnot($src,$dest,'145','143','ratiohh');
$image_info = getimagesize($dest);
$image_width = $image_info[0];
$image_height = $image_info[1]; 
?>
<div class="scrollContainer">

               <div class="panel" id="panel">
<div class="inside">
<div class="insideimgBox"><a href="/adoption/<?=$puppy_nicename?>/">
                            <img src="<?=$imageURL121?>" alt="picture"  width="<?=$image_width?>" height="<?=$image_height?>"/></a></div>
<h2><a href="/adoption/<?=$puppy_nicename?>/"><?=$sqlgetqdata['puppi_name'].",".' '.$sqlgetqdata['city']?></a></h2>

</div>
</div>
               </div>
<? }?>
<div id="left-shadow"></div>
<div id="right-shadow"></div>

           </div>
<div class="scrollButtons right sb_icon_right"></div>


       </div>
       <? }?>
</div>
       
<div class="col-2 left-2 sidebar">
       <div  class="Dog_adoption_Banner">
	 <? if($userid !='Guest'){?>
	<a href="/adoption/new.php"><img src="https://www.dogspot.in/Adoption1/img/adoption_upload_your_banner.jpg"></a>
    <? }else{?>
    <a href="/login.php"><img src="https://www.dogspot.in/Adoption1/img/adoption_upload_your_banner.jpg"></a>
  
    <? }?></div>
		<!-- foster pet-->
          <div class="Foster_pet" >
			   <h2>Foster a pet</h2>
			   <a href="#"><img src="/Adoption1/img/2.png"></a>
			   <p>Would you be interested in Fostering a dog/puppy? Click below to fill up the form. </p>
			  <a href="/adoption/foster-form.php"> <div class="foster-me"><input type="submit" name="Foster me" value="Foster me" class="fm_btn"></div></a>
               </div>
			   <!-- foster pet-->
			   <!-- you may like post-->
			 
			 <?  mysql_select_db("test");
			    $article=query_execute("SELECT wpp.ID , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wptr.term_taxonomy_id = '10' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_date>'2014-01-01 00:00:00' ORDER BY wpp.ID DESC");
				?>
			    <div class="youmaylike_post" >
			   <h2>You may also like to read</h2>
			   <div style="z-index:0;" class="cycle-slideshow composite-example" 
    data-cycle-fx="scrollHorz" 
    data-cycle-slides="> div"
    data-cycle-timeout="2000"
    ><?
               while($rowArt=mysql_fetch_array($article)){
				$post_name   = $rowArt["post_title"];
				$cat_name=$rowArt['post_name'];
				$main_fea_art_body=$rowArt['post_content'];
				$main_fea_art_id=$rowArt['ID'];
			$feat_imgURL    = get_first_image($main_fea_art_body, $DOCUMENT_ROOT);
	       $imgURLAbs = make_absolute($feat_imgURL, 'https://www.dogspot.in');
		   $feat_check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$main_fea_art_id' AND post_type='attachment' AND post_mime_type like '%image%'");
			
		if (strpos($imgURLAbs,'wordpress') !== false) {
    	$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/280x205-'.$image_name;
		}
		else if($feat_check_img['guid'] !=''){
		$imgURLAbs=$feat_check_img['guid'];	
		$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/280x205-'.$image_name;
		}
   else if ($imgURLAbs!='https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
	   $URL=$imgURLAbs;
	   $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
	   $pos = strrpos($image_name,'/');
	   $image_name = substr($image_name,$pos+1);
		$extension = stristr($image_name,'.');
		if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
		$image_name;
		}
		$src = $DOCUMENT_ROOT.'/userfiles/images/'.$image_name;
		//$src = $imgURLAbs;
		$imageURL='/imgthumb/280x205-'.$image_name;
		} 
		else if($rowUser1["image"]) {
		$src = $DOCUMENT_ROOT.'/profile/images/'.$rowUser1["image"];
		$imageURL = '/imgthumb/280x205-'.$rowUser1["image"];
		} 
		else  {
        $src = $DOCUMENT_ROOT.'/images/black_dog_02_139885.jpg';
		$imageURL = '/imgthumb/280x205-black_dog_02_139885.jpg';
		}
		
		$dest = $DOCUMENT_ROOT.$imageURL;
		createImgThumbIfnot($src,$dest,'280','205','ratiowh');
				?>
                
    <div >
		  <a href="/<?=$cat_name?>/"> <img src="<?=$imageURL?>" alt="<?=$cat_name ?>" title="<?=$cat_name ?>" style="width:280px;height:205px;"></a>
			    <div class="cycle-overlay"><h3><?=snippetwop($post_name,$length=25,$tail="...");?></h3></div></div>
               <? }?>
               </div>
                 <!-- email box-->
               <div class="adoption_emailbox">
<div class="adoption_emailtext">Have any questions regarding Adoption/Foster?</div>

<div class="adoption_emailid">Write to us at <a href="mailto:name@rapidtables.com">adoption@dogspot.in</a> </div>
</div>
               <!-- email box->
               		
			   <!-- you may like post-->
                       <div class="about_dog_adoption">
<div>Want to know more about Adoption at DogSpot? 
<a href="/adoption/what-is-adoption.php">Click here</a></div></div>

<div class="about_dog_adoption">
<div>Want to know about Adoption Procedures? <a href="/adoption/how-to-adopt.php">Click here</a></div>
</div>
        </div>
        </div>
        </div>

         </section>
  <?php
  mysql_select_db("dogspot_dogspot");
   require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
