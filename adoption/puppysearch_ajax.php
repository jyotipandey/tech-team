

<?
if($q || $aq && $filter=='filter'){
	require_once($DOCUMENT_ROOT.'/constants.php');
	require_once($DOCUMENT_ROOT.'/session.php');
	require_once($DOCUMENT_ROOT.'/database.php');
	require_once($DOCUMENT_ROOT.'/functions.php');
	require_once($DOCUMENT_ROOT.'/shop/functions.php');
	include($DOCUMENT_ROOT."/dogs/arraybreed.php");
	include($DOCUMENT_ROOT."/common/countries.php");
	//include($DOCUMENT_ROOT . "/new/articles/catarray.php");
	
if($breed){
	$puppy = str_replace("undefined", "", $breed);
				$puppy=trim($puppy,'-');
				$spuppy=explode('-',$puppy);
				if($spuppy[0]){
				foreach($spuppy as $sb){
					$bsql.=" OR breed_nicename:$sb ";
				}
				$bsql = trim($bsql);
				$bsql = trim($bsql,' OR ');
				$bsql = "&fq=($bsql)";
				}
			}
if($location){
	$loc = str_replace("undefined", "", $location);
				$loc=trim($loc,'-');
				$sloc=explode('-',$loc);
				if($sloc[0]){
				foreach($sloc as $sb){
					$bsqlloc.=" OR city:$sb ";
				}
				$bsqlloc = trim($bsqlloc);
				$bsqlloc = trim($bsqlloc,' OR ');
				$bsqlloc = "&fq=($bsqlloc)";
				}
			}

			$query=$q;
			$query=escapeSolrValue($query, 'OR');
			$maxshow='200';
			$st='&sort=c_date desc';
			//$query=urlencode($query);
	 		$url = "https://localhost:8080/dogspotpuppy/select/?q=$query AND publish_status:publish$bsql$bsqlloc$st&version=2.2&rows=$maxshow&fl=* score&qf=title_tag^2&indent=on";
			$url = str_replace(" ","%20",$url);
			//echo $url;
			$result = get_solr_result($url);
			$totrecord = $result['TOTALHITS'];

}
		$s = 0; 
		
if($q || $aq){	//echo "Total Record:".$totrecord;		?>
        <?php /*?><div class="articleLoad" style="margin-left:80px; margin-top:10px; float:left"></div><?php */?>
    
    
     <div id="productFilter">
        <div class="results fr" style="display:inline-block;text-align:right"><?=$totrecord?> Results</div>
      <div class="cb"></div>
                <div style="display:inline-block" class="vs5"></div>
                
				<div id="productListing542Load"></div>
     <div id="puppiesContainer">
   
    <?php  if($totrecord>0){?>
  <div class="puppiesContainer_header">
    <div class="image headerText_container">Image</div>
    <div class="details headerText_container">Puppies Details</div>
    <div class="breed headerText_container">Breed</div>
    <div class="age headerText_container">Age</div>
    <div class="location headerText_container">Location</div>
  </div>
           <div class="puppiesContainer_body">
        <?
		foreach($result['HITS'] as $rowgroup){
			 $s             = $s + 1;
			$puppi_id  = $rowgroup["puppi_id"];
	
		$puppy_owner  = $rowgroup["userid"];

		$puppi_breed  = $rowgroup["puppi_breed"];

		$puppi_img = $rowgroup["puppi_img"];
		$day = $rowgroup["day"];
		$month = $rowgroup["month"];
		$year = $rowgroup["year"];
		$dob=pupDOB($day, $month, $year);
		
		$puppy_nicename = $rowgroup["puppy_nicename"];
		$breed_nicename = $rowgroup["breed_nicename"];
		
		$country = $rowgroup["country"];
		$state = $rowgroup["state"];

		$city =$rowgroup["city"];

		$img_src = $DOCUMENT_ROOT."/puppies/images/thumb_$puppi_img";
		
		if(file_exists($img_src)){
		  $imgWH = @WidthHeightImg($img_src,'100','87');
		}
		
 ?> 
 
<div class="bodyText_row">

      <div class="image bodyText_container">
<? if($puppi_img){ ?>
   <a href="<? echo"/puppies/$puppy_nicename/";?>" ><img src="<? echo"/puppies/images/thumb_$puppi_img";?>" border="0" alt="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),20); ?>" title="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),20); ?>" width="<? echo"$imgWH[0]";?>" height="<? echo"$imgWH[1]";?>" /></a>
<? }else{ ?>
  <a href="<? echo"/puppies/$puppy_nicename/";?>"><img src="https://www.dogspot.in/images/no-img.jpg" alt="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),20); ?>" width="100" height="87" border="0"/></a>
<?  } ?></div>
      <div class="details bodyText_container">Owner: <? guestredirect1($userid, dispUname($puppy_owner),$puppy_owner) ; ?><br /><br />
<? if($userid == "Guest"){ ?>
<b><img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> <a href="<? echo"/login.php";?>">Contact <? print getTeaser(dispUname($puppy_owner),20); ?></a></b>
<? }else{ ?>
<img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> Contact: <a href="<? echo"/scrapbook/msgview-post.php?msg_from=$puppy_owner";?>" onclick="return hs.htmlExpand(this, { contentId: 'highslide-tc', objectType: 'iframe',	preserveContent: false, objectWidth: 600, objectHeight: 300} )"><b><? print getTeaser(dispUname($puppy_owner),20); ?></b></a>
<? } ?>
<? if(($puppy_owner == $userid || $sessionLevel == 1) && $userid != "Guest"){ ?>
    <br />
    <div id="editBut"><div class="buttonwrapper">
    <a href="<? echo"javascript:confirmDelete('/puppies/index.php?puppi_id=$puppi_id&del=del')";?>" class="ovalbutton"><span>Delete</span></a> &nbsp;<a href="<? echo"/puppies/new.php?puppi_id=$puppi_id";?>" class="ovalbutton"><span>Edit</span></a>
    </div></div>
   <? } ?></div>
      <div class="breed bodyText_container"><a href= "<? echo"/puppies/$puppy_nicename/";?>" class="imgBoder"><? echo "$ArrDogBreed[$breed_nicename]"; ?></a></div>
      <div class="age bodyText_container"><? print (pupDOB($day, $month, $year)); echo"<br>"; print (numPupies($puppi_id)); echo" Puppies"; ?>   </div>
      <div class="location bodyText_container"><?
if($city){
 echo"$city <br>";
}
if($state){
 echo"$state <br>";
}
if($country){
 echo $countries[$country];
}
?></div>

    </div>
     <? }?>
    </div>
    
   </div>
    </div>
    <div class="line"></div>
 
 <div align="center"><?php // require_once($DOCUMENT_ROOT.'/banner1.php'); addbanner600('74','600','1'); ?></div> 
<div class="vs5"></div>
  <? }else
  { echo 'Sorry !!! No result';
  }}?>                                            
                



