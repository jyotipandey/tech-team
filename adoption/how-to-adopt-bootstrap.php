<?php
include($DOCUMENT_ROOT."/constants.php");
include($DOCUMENT_ROOT."/session-no.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/dogs/arraybreed.php");
include($DOCUMENT_ROOT."/common/countries.php");

require_once($DOCUMENT_ROOT . '/arrays.php');
$title="How to adopt a pet | Pet Adoption Guide | DogSpot.in";
	$keyword="How to adopt a pet, Pet Adoption Guide, Pet Adoption, Adopt A Pet";
    $desc="Here you can find process to adopt a pet. Place for pet adoption. DogSpot is the Best website for adoptable pets.";
	$alternate="https://m.dogspot.in".$_SERVER['REQUEST_URI'];
	$canonical="https://www.dogspot.in".$_SERVER['REQUEST_URI'];
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in/new/breed_engine/images/logo-300x300.jpg";
	$page_type='Adoption';
 require_once($DOCUMENT_ROOT.'/new/common/header-bootstrap.php'); ?>
<link type="text/css"  rel="stylesheet" href="/bootstrap/css/adoption.css?v=2" />
<div class="breadcrumbs">
 <div class="container">
   <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
   <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> 
    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <a href="/" itemprop="item"><span itemprop="name">Home</span></a>
      <meta itemprop="position" content="1" /> </span>
     <span> / </span>
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <span itemprop="name"><a  href="/adoption/" itemprop="item" >Adoption</a></span>
     <meta itemprop="position" content="2" /> 
     </span>
    </div>
    </div>
  </div>
</div>


            <section id="content">

  <div class="container">   
  <div class="row"> 
<div class="col-md-9">
<h1>How to adopt:</h1> 

<p>To adopt a pet, please follow below steps:</p>
<p>Go onto 'Community > Adoption' section.</p>
<p>Scroll through the extensive pst of animals looking for a good home.</p> 
<p>Click on 'Interested' tab against the pet you are willing to adopt.</p>
<p>You will receive an email on the registered ID with the 'Link' to the form.</p>
<p>Click on 'Link' and you will be directed to a page with the form.</p>
<p>Fill in the form with all the details and click on 'Submit'.</p>
</div>

<aside class="col-md-3">
 <div class="row">
  <section class="col-sm-6 col-md-12 widget no-mobile">
  <div>
 <? if($userid !='Guest'){?>
	<a href="/adoption/new.php"><img src="https://ik.imagekit.io/2345/Adoption1/img/adoption_upload_your_banner.jpg"></a>
    <? }else{?>
    <a href="/login.php"><img src="https://ik.imagekit.io/2345/Adoption1/img/adoption_upload_your_banner.jpg"></a>
  
    <? }?></div>
 </section>
 
   <section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
                                                                <!-- Widget Header -->
                                                                <header class="clearfix">
                                                                        <h4>Foster a pet</h4>
                                                                                                        </header>
                                                                                                        
                                                                                                         <div><a href="/adoption/foster-form.php"><img src="https://ik.imagekit.io/2345/Adoption1/img/2.png" width="280" height="151"></a></div>
  
  <div class="text-center">Would you be interested in Fostering a dog/puppy? Click below to fill up the form.</div>
      <div class="text-center foster-btn">

      <a href="/adoption/foster-form.php" class="foster-me-btn " data-toggle="tooltip"  data-placement="bottom" title="Many animals require urgent medical care or immediate attention in different ways. ‘Foster’ parents exist for those users who are willing to give temporary homes and care to the animal in need till a forever home is found. Time in this case cannot be specified." >
    Foster Me</a>
           
					
              
      </div>                                                                                                  </section>
 
 <section class="col-sm-6 col-md-12 widget no-mobile" style=" z-index: 0;">
                                                                <!-- Widget Header -->
                                                                <header class="clearfix">
                                                                        <h4>Sponsored: In the Stores</h4>
                                                                                                        </header>
<? $getItems=query_execute("SELECT si.name,si.nice_name,si.item_id,sia.price,sia.mrp_price,si.item_parent_id FROM shop_items as si,shop_item_affiliate as sia WHERE sia.item_id=si.item_id AND showbanner='1' ");
				 while($fetchaffiliate=mysql_fetch_array($getItems)){
			 $name=$fetchaffiliate['name'];
			 $item_id=$fetchaffiliate['item_id'];
			  $item_parent_id=$fetchaffiliate['item_parent_id'];
			   $nice_name=$fetchaffiliate['nice_name'];
			    $price=$fetchaffiliate['price'];
			   $mrp_price=$fetchaffiliate['mrp_price'];
		 $rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
		 if(!$rowdatM['media_file'])
		 {
		$rowdatM=query_execute_row("SELECT media_file FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
	     }
					 $imglink = 'https://ik.imagekit.io/2345/tr:h-100,w-100,c-at_max/shop/item-images/orignal/' . $rowdatM['media_file'];
		 ?>
                        <div class="gurgaon_offers"> 
        	
                <div class="gurgaon_offersr"> <a href="/<?=$nice_name?>/?UTM=bannerAmazon"><img class="img-responsive" src="<?=$imglink?>" width="100" height="93"></a> </div>
                <div class="gurgaon_offersl">
                    <div class="gurgaon_offers_pn"><a style="color: #333;" href="/<?=$nice_name?>/?UTM=bannerAmazon"><?=$name?></a> </div>
                                        <div class="gurgaon_offers_pr">
                                        <? if($mrp_price>$price){?>
                                        <span>Rs. <del><?=$mrp_price?></del> 
                                        </span> &nbsp; &nbsp; <? }?>
                                        <span class="price-color">
                                        Rs. <?=$price?></span>
                                       <div class="aff-m-logo"> <a href="/<?=$nice_name?>/?UTM=bannerAmazon"><img src="https://ik.imagekit.io/2345/new/articles/img/amazon.jpg" /></a>
                                       </div>
                                        </div>
                                    </div>
          	
		</div>
     <? }?>
     </section>

<section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
  <div class="adoption_emailbox">
<div class="adoption_emailtext">Have any questions regarding Adoption/Foster?</div>

<div class="adoption_emailid">Write to us at <a href="mailto:name@rapidtables.com">adoption@dogspot.in</a> </div>
</div>
  
  </section>
 
 <section class="col-sm-6 col-md-12 widget no-mobile" style="display: block;">
  
  <div class="adoption_emailbox">
<div>Want to know more about Adoption at DogSpot? 
<a href="/adoption/what-is-adoption.php">Click here</a></div></div>
  </section>
 <section class="col-sm-6 col-md-12 widget no-mobile" style="display:block">

<div class="adoption_emailbox">
<div>Want to know about Adoption Procedures? <a href="/adoption/how-to-adopt.php">Click here</a></div>
</div>
</section>

<section class="col-sm-6 col-md-12 widget no-mobile" style="margin-bottom: 5px;">
  <header class="clearfix">
                                                                        <h4>Dogs For Adoption</h4>
                                                                                                        </header>
  <div class="adoption_tag">
<table class="adoption_tags_table">

<tbody>
<tr>
<td class="text-left"><a href="/german-shepherd-dog-alsatian-dogs-for-adoption/" rel="tag">German Shepherd for Adoption</a></td>
</tr><tr>
<td class="text-left"><a href="/rottweiler-puppy-for-adoption/" rel="tag">Rottweiler for Adoption</a></td>
</tr><tr>
<td class="text-left"><a href="/golden-retriever-puppy-for-adoption/" rel="tag">Golden Retriever for Adoption</a></td>
</tr><tr>
<td class="text-left"><a href="/labrador-retriever-puppy-for-adoption/" rel="tag">Labrador Retriever for Adoption</a></td>
</tr><tr>
<td class="text-left"><a href="/great-dane-puppy-for-adoption/" rel="tag">Great Dane for Adoption</a></td>
</tr><tr>
<td class="text-left"><a href="/dobermann-puppy-for-adoption/" rel="tag">Dobermann for Adoption</a></td>
</tr><tr>
<td class="text-left"><a href="/pug-puppy-for-adoption/" rel="tag">Pug for Adoption</a></td>
</tr><tr>
<td class="text-left"><a href="/beagle-puppy-for-adoption/" rel="tag">Beagle for Adoption</a></td>
</tr>
<tr>
<td class="text-left"><a href="/dogs-for-adoption/" rel="tag">Dogs for Adoption</a></td>
</tr>
<tr>
<td class="text-left"><a href="/adp-for-adoption/" rel="tag">Puppies for Adoption</a></td>
</tr>
</tbody>
</table>
</div>
  
  </section>
 
 <section class="col-sm-6 col-md-12 widget no-mobile" style="display:block">
  <header class="clearfix">
                                                                        <h4>Cats For Adoption</h4>
                                                                                                        </header>
  <div class="adoption_tag">
<table class="adoption_tags_table">

<tbody>
<tr>
<td class="text-left"><a href="/cats-for-adoption-in-new-delhi/" rel="tag">Cats for Adoption in New Delhi</a></td>
</tr><tr>
<td class="text-left"><a href="/cats-for-adoption-in-mumbai/" rel="tag">Cats for Adoption in Mumbai</a></td>
</tr>

<tr>
<td class="text-left"><a href="/cats-for-adoption/" rel="tag">Cats for Adoption</a></td>
</tr>

</tbody>
</table>
</div>
  </section>
 </div>
 </aside>
 </div>
 </div>
</section>







  <? require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
