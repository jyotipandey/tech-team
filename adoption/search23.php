<?php
include($DOCUMENT_ROOT."/session-no.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/dogs/arraybreed.php");
include($DOCUMENT_ROOT."/common/countries.php");

$sitesection = "puppie";

$pageTitle = "Puppies India DogSpot.in";
$pageh1="Puppies";
$pageKey="Puppies India, Dog India, dogs for sale, dog breeds, breeding dogs";
$pageDesc="Puppies India, Dog India DogSpot.in";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><? echo"$pageTitle";?> | DogSpot</title>
<meta name="keywords" content="<? echo"$pageTitle";?>, Puppies Wanted, Breed, dogs, dogs india, dogs world, puppies, dog | DogSpot" />
<meta name="description" content="<? echo"$pageTitle";?> | Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog: DogSpot" />
<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
<?php /*?><link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />
<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />
<link rel="stylesheet" type="text/css" href="/new/css/business_listing.css" media="all"  /><?php */?>
<link rel="stylesheet" type="text/css" href="/shapopup/shapopup.css" media="all"  />
<?php /*?><link href="/css/common.css?v=020112" rel="stylesheet" type="text/css" /><?php */?>
<link href="/new/css/style1.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<script src="/js/ajax.js" type="text/javascript"></script>
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/highslide.js"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<?php /*?><script src="/jquery/jquery-latest.js" type="text/javascript"></script>
<script src="/jquery/jquery-latest.js" type="text/javascript"></script>
<script src="/jquery/jquery.validate-1.9.js" type="text/javascript"></script><?php */?>

<script type="text/javascript">
 jQuery(document).ready(function() {
	  jQuery("#formSearch").validate({
		  
	});
});
</script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
</script>
<style>
#mytable{ 
 
  border-collapse: collapse; 
}
/* Zebra striping */
#mytable tr:nth-of-type(odd) { 
  background: #eee; 
}
#mytable th { 
  background: #999; 
  color: #fff; 
  font-weight:normal;
  font-size:16px;
}
#mytable td, th { 

  padding: 5px; 
  border: 1px solid #ccc; 
  text-align: left; 
  font-weight:normal;
  font-size:14px;
}
</style>
 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
 

 <div class="breadcrumb" style="">
            	<div class="cont980" style="">
                 
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="61%"><a href="/">Home</a> &gt;&gt; Puppies 
</td>                   
                  
                     <td width="20%" align="right"><div class="rea">
                                              
                        <a href="https://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a><a href="https://twitter.com/#!/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a>
                     </div></td><td width="0%"></td>
                    </tr>
                  </table>
                  <div class="cb"></div>
                 </div>
            
            </div>
            <div class="cont980">
            <div class="cont660" style="width:644px; margin-right:28px; ">
             
             <div id="pagebody">
             
              
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="61%"><h1><? echo"$pageh1";?></h1>  
</td>
                  <? if($userid=='admin'){?>  <td width="19%" align="center" >
                    <div class="rea">
                             <a href="/adoption/new.php"><img src="/new/pix/add-new-puppy.png" /></a>

                      </div></td>
                      <? }?>
                     
                  
                  
                    </tr>
                  </table>
                 <div class="cb"></div>
                 </div>
 
             <div id="clearall"></div> 
  
         <div class="vs5"></div>
<?php /*?><div align="center"><?php require_once($DOCUMENT_ROOT.'/banner1.php'); addbanner600('74','600','1'); ?></div> <?php */?>
<div class="vs5"></div>     
    <form id="formSearch1" name="formSearch1" method="get" action="/adoption/search.php">
    <table width="100%" border="1" cellspacing="0" cellpadding="5" style=" border-width:thin;">
    <?php /*?><tr>
    <td colspan="3" style=" border-bottom:none; border-left:none; border-right:none; border-top:none;">
    <center>
<a href="https://www.dogspot.in/puppies/jaisimhakb-cocker-spaniel-english/" target="_blank"><img src="/new/pix/bannerRabrika.jpg"  alt="Rabrika Cockers" title="Rabrika Cockers"></a></center>
    </td>
    </tr><?php */?>
      <tr style="background:#eee">
        <td >
        <select name="dog_breedSearch" id="dog_breedSearch" style=" width:248px;" class="required">
          <option value="0">Select Breed</option>
          <? foreach($ArrDogBreed as $KeyBreedNice => $BreedName){
			 print "<option value='$KeyBreedNice'";
		     if($KeyBreedNice == "$dog_breedSearch"){ echo"selected"; } print ">$BreedName</option>";
		   }
		 ?>
        </select></td>
        <td><select id="citySearch" name="citySearch" style=" width:170px;" class="required">
          <option value="0">Select Current Location</option>
          <?php
              $queryCitySearch = mysql_query("SELECT * FROM city ORDER BY city_name ASC");
               if (!$queryCitySearch){	die(sql_error());	}
               while($rowCitySearch = mysql_fetch_array($queryCitySearch)){
              $city_idSearch = $rowCitySearch["city_id"];
                $cityNameSearch = $rowCitySearch["city_name"];
                $city_niceSearch = $rowCitySearch["city_nicename"];
              
                print "<option value='$city_idSearch'";
                if($city_niceSearch == $citySearch){ echo "selected";  }  print ">$cityNameSearch</option>";
               }
            ?>
          <option value="-1" <? if($citySearch == -1) echo"selected"; ?>> Other City </option>
        </select></td>
        <td><select name="dog_statusSearch" id="dog_statusSearch" style="width: 127px;" class="required">
          <option value=" ">Select Status</option>
           <option value="rescue">Rescue</option>
            <option value="foster">Foster</option>
             <option value="adopted">Adopted</option>
             
          
        </select></td>
        <td><input type="submit" name="searchBut" id="button" value="  Search  " class="searchBut"/></td>
      </tr>
    </table>
    </form>

<?php
//------------Show
$maxshow = 20;
if(empty($show)) {
  $show=0;
}else{
  $show = $show - 1;
}

$showRecord = $show * $maxshow;
$nextShow = $showRecord + $maxshow;
//-------------Show									

if($dog_breedSearch || $citySearch || $dog_statusSearch){
if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND adoption='yes' ORDER BY puppi_id DESC";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' ORDER BY puppi_id DESC";
}
elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' ORDER BY puppi_id DESC";
}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC";
}
elseif(!$citySearch && $dog_breedSearch && !$dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch'  AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch'  AND adoption='yes' ORDER BY puppi_id DESC";
}
elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC";
}else{
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC LIMIT $showRecord, $nextShow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' ORDER BY puppi_id DESC";
}
$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
}
else {
	$totrecord=0;
  
}
//echo $sql;
//----------------------------------
	if($totrecord == 0){
	  echo"<p align='center' class='redbtext'>No Puppies have been found.</p>"; 
	}else{
	?>
<? echo"Viewing <b>$show - $next</b> out of <b>$totrecord Puppies</b>";?>
<br /><br />
<div id="puppiesContainer">
  <div class="puppiesContainer_header">
    <div class="image headerText_container">Image</div>
    <div class="details headerText_container">Puppies Details</div>
    <div class="breed headerText_container">Breed</div>
    <div class="age headerText_container">Age</div>
    <div class="location headerText_container">Location</div>
  </div>
 
           <div class="puppiesContainer_body">
           
     <?
	 
	  while($rowgroup = mysql_fetch_array($selecg)){
		  $puppi_name  = $rowgroup["puppi_name"];
		$puppi_id  = $rowgroup["puppi_id"];
		$puppy_owner  = $rowgroup["userid"];
		$puppi_breed  = $rowgroup["puppi_breed"];
		$puppi_img = $rowgroup["puppi_img"];
		
		$source  = $rowgroup["source"];

		$status = $rowgroup["status"];
		$condition1  = $rowgroup["condition1"];

		$temperament = $rowgroup["temperament"];
		$day = $rowgroup["day"];
		$month = $rowgroup["month"];
		$year = $rowgroup["year"];
		$puppy_nicename = $rowgroup["puppy_nicename"];
		$breed_nicename = $rowgroup["breed_nicename"];
		$country = $rowgroup["country"];
		$state = $rowgroup["state"];
		$city =$rowgroup["city"];

		$img_src = $DOCUMENT_ROOT."/puppies/images/thumb_$puppi_img";
		
		if(file_exists($img_src)){
		  $imgWH = @WidthHeightImg($img_src,'100','87');
		}
 ?> 
 
<div class="bodyText_row">
 <div class="image bodyText_container">
  <? if($puppi_img){ ?>
   <a href="<? echo"/adoption/$puppy_nicename/";?>" ><img src="<? echo"/puppies/images/thumb_$puppi_img";?>" border="0" alt="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),15); ?>" title="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),15); ?>" width="<? echo"$imgWH[0]";?>" height="<? echo"$imgWH[1]";?>" /></a>
<? }else{ ?>
  <a href="<? echo"/adoption/$puppy_nicename/";?>"><img src="https://www.dogspot.in/images/no-img.jpg" alt="<? echo "$ArrDogBreed[$breed_nicename] | "; print getTeaser(dispUname($puppy_owner),10); ?>" width="100" height="87" border="0"/></a>
<?  } ?> 
</div>
      <div class="details bodyText_container">Puppy Name: <?=$puppi_name ?><br /><? if($status!=''){?>Status:<?=$status?><br/><? }?>
      <? if($source!=''){?>Source:<?=$source;?><? }?><? if($condition1!=''){?><br/>Condition:<?=$condition1;?><? }?>
     <? if($temperament!=''){?> <br />Temperament:<?=$temperament;?><? } ?><br />
<? if(($puppy_owner == $userid || $sessionLevel == 1) && $userid != "Guest"){ ?>
    <br />
    <div id="editBut"><div class="buttonwrapper">
    <a href="<? echo"javascript:confirmDelete('/adoption/index.php?puppi_id=$puppi_id&del=del')";?>" class="ovalbutton"><span>Delete</span></a> &nbsp;<a href="<? echo"/adoption/new.php?puppi_id=$puppi_id";?>" class="ovalbutton"><span>Edit</span></a>
    </div></div>
   <? } ?></div>
      <div class="breed bodyText_container"><a href= "<? echo"/adoption/$puppy_nicename/";?>" class="imgBoder"><? echo "$ArrDogBreed[$breed_nicename]"; ?></a></div>
      <div class="age bodyText_container"><? print (pupDOB($day, $month, $year)); echo"<br>"; print (numPupies($puppi_id)); echo" Puppies"; ?>fd   </div>
      <div class="location bodyText_container"><?
if($city){
 echo"$city <br>";
}
if($state){
 echo"$state <br>";
}
if($country){
 echo $countries[$country];
}
?></div>
       
 
</div>
 <div class="line"></div>
<? } ?>

<div class="vs5"></div>
<div id="shownext"> 
<div class="showPages">
<?
// ReSet Order
if($order == "DESC"){
 $order = "ASC";
}else{
 $order = "DESC";
}
// ReSet Order END 
if($section[1] == "category"){
	$pageUrl="/adoption/category/$cattype";
}elseif($section[1] == "page"){
	$pageUrl="/adoption/page";
}elseif($section[1] == "breed"){
	$pageUrl="/adoption/breed/$section[2]";
}elseif($section[1] == "owner"){
	$pageUrl="/adoption/owner/$section[2]";
}else{
	$pageUrl="/adoption/page";
}
showPages($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl);
?> <? } ?><?php if(!$puppi_id){?>
<?php require_once($DOCUMENT_ROOT.'/banner1.php'); addbanner600('74','600','1'); ?>
<?php }?>

</div>
</div>



</div>
 <?php if($puppi_id){?>
<div align="center" style="margin-top:10px">
<?php require_once($DOCUMENT_ROOT.'/banner1.php'); addbanner600('74','600','1'); ?>
<div align="center" class="cont660" style="margin-top:10px;" >
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Dog More Sale in these  Cities</th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/dog-for-sale-in-delhi/"  title="Dogs For Sale In Delhi">Dogs For Sale In Delhi</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-chennai/"  title="Dogs For Sale In Chennai">Dogs For Sale In Chennai</a></td>
                    <td width="29%" ><a href="/dog-for-sale-in-hyderabad/"  title="Dogs For Sale In Hyderabad">Dogs For Sale In Hyderabad </a></td>
                    </tr>
                     <tr>
                       <td ><a href="/dog-for-sale-in-bangalore/"  title="Dogs For Sale In Bangalore">Dogs For Sale In Bangalore</a></td>
                       <td ><a href="/dog-for-sale-in-lucknow/"  title="Dogs For Sale In Lucknow">Dogs For Sale In Lucknow</a></td>
                       <td ><a href="/dog-for-sale-in-gurgaon/"  title="Dogs For Sale In Gurgaon">Dogs For Sale In Gurgaon</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/dogs/"  title="Dogs For Sale In India">Dogs For Sale In India</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-mumbai/"  title="Dogs For Sale In Mumbai">Dogs For Sale In Mumbai</a></td>
					
					
              <td width="29%" ><a href="/dog-for-sale-in-kolkata/"  title="Dogs For Sale In Kolkata">Dogs For Sale In Kolkata</a></td>                         
                    </tr>
                     <tr>
                       <td ><a href="/dog-for-sale-in-coimbatore/"  title="Dogs For Sale In Coimbatore">Dogs For Sale In Coimbatore</a></td>
                       <td ><a href="/dog-for-sale-in-agra/"  title="Dogs For Sale In Agra">Dogs For Sale In Agra</a></td>
                       <td ><a href="/dog-for-sale-in-punjab/" title="Dogs For Sale In Punjab">Dogs For Sale In Punjab</a></td>
                     </tr>
                     <tr>
                    <td ><a href="/dog-for-sale-in-pune/"  title="Dogs For Sale In Pune">Dogs For Sale In Pune</a></td>
                    
                    <td ><a href="/dog-for-sale-in-chandigarh/"  title="Dogs For Sale In Chandigarh">Dogs For Sale In Chandigarh</a></td>     
					<td ><a href="/dog-for-sale-in-trivandrum/"  title="Dogs For Sale In Trivandrum">Dogs For Sale In Trivandrum</a></td>
                    </tr>
                     </thead></table>
               
                </div>
               
              <div class="vs20"></div>  
              
              
              <div align="center" class="cont660">
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Puppies for sale in These City</th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/puppies/"  title="Puppies For Sale">Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies-for-sale-in-delhi/"  title="Puppies For Sale In Delhi">Puppies For Sale In Delhi</a></td>
                    <td width="29%" ><a href="/puppies-for-sale-in-chennai/"  title="Puppies For Sale In Chennai">Puppies For Sale In Chennai</a></td>
                    </tr>
                     <tr>
                       <td ><a href="/puppies-for-sale-in-bangalore/"  title="Puppies For Sale In Bangalore">Puppies For Sale In Bangalore</a></td>
                       <td ><a href="/puppies-for-sale-in-kolkata/"  title="Puppies For Sale In Kolkata">Puppies For Sale In Kolkata</a></td>
                       <td ><a href="/puppies-for-sale-in-pune/"  title="Puppies For Sale In Pune">Puppies For Sale In Pune</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/puppies-for-sale-in-mumbai/"  title="Puppies For Sale In Mumbai">Puppies For Sale In Mumbai</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-hyderabad/"  title="Puppies For Sale In Hyderabad">Puppies For Sale In Hyderabad</a></td>
					
					
              <td width="29%" ><a href="/puppies-for-sale-in-lucknow/"  title="Puppies For Sale In Lucknow">Puppies For Sale In Lucknow</a></td>                         
                    </tr>
                    
                     </thead></table>
               
                </div>
               
                
              <div class="vs20"></div>
              
              <div align="center" class="cont660">
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Breed puppies For Sale </th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/puppies/breed/german-shepherd-dog-alsatian/"  title="German Shepherd Puppies For Sale">German Shepherd Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies/breed/labrador-retriever/"  title="Labrador Puppies For Sale">Labrador Puppies For Sale</a></td>
                    <td width="29%" ><a href="/puppies/breed/dobermann/"  title="Doberman Puppies For Sale">Doberman Puppies For Sale</a></td>
                    </tr>
                     <tr>
                       <td ><a href="/puppies/breed/pug/"  title="Pug Puppies For Sale">Pug Puppies For Sale</a></td>
                       <td ><a href="/puppies/breed/golden-retriever/"  title="Golden Retriever Puppies For Sale">Golden Retriever Puppies For Sale</a></td>
                       <td ><a href="/puppies/breed/rottweiler/"  title="Rottweiler Puppies For Sale">Rottweiler Puppies For Sale</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/puppies/breed/great-dane/"  title="Great Dane Puppies For Sale">Great Dane Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies/breed/beagle/"  title="Beagle Puppies For Sale">Beagle Puppies For Sale</a></td>
					
					
              <td width="29%" ><a href="/puppies/breed/boxer/"  title="Boxer Puppies For Sale">Boxer Puppies For Sale</a></td>                         
                    </tr>
                     <tr>
                       <td ><a href="/puppies/breed/dalmatian/"  title="Dalmatian Puppies For Sale">Dalmatian Puppies For Sale</a></td>
                      
                    </tr>
                     </thead></table>
                            </div>

<?php }?>
</div>
</div></div>

 <div class="vs5"></div>


 <div class="cont300" id="cont300" align='left'>
                 
   
  <?php include($DOCUMENT_ROOT."/include-files/inc-adopt.php"); ?>
   <?php
   $photoskey=$ArrDogBreed[$dog_breedSearch]="$breed_name";
    require_once($DOCUMENT_ROOT.'/new/includes/navi-right.php'); ?>
  </div>
  
<div id="clearall"></div> 
<div id="clearall"></div>
          


  <?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>  
