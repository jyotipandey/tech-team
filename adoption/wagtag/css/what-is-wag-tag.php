<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wagtag/css/wt_style.css" />
<title>Wag Club</title>

<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>

<div class="wc_headerBar">
<div class="cont980">
<div class="wt_topNav">
<ul>
<li><a href="/wagtag/index-wt.php/">HOME</a></li>
<li><a href="/wagtag/what-is-wag-tag.php/">WHAT IS WAGTAG?</a></li>
<li><a href="/wagtag/how-it-works.php">HOW IT WORKS?</a></li>
<li><a href="#">SAMPLE PROFILE</a></li>
<li><a href="/wagtag/faq.php/" class="wt_last">FAQ</a></li>
</ul>
</div>

<?php /*?><div class="wt_search">
<div class="wc_search " style="width: 200px;margin: 6px 5px 0 10px;">
<input id="search_name" type="text" placeholder="search by wag id">
</div>
<div class="wc_srchbtn" onclick="keyworddog()"><img src="/wagtag/images/search.png" width="16" height="16"></div>
</div><?php */?>
</div>
</div>

<div class="cont980">
<div class="wt_whatBox">
<div class="wt_infoBox1">
<div class="wt_logo">
  <img src="/wagtag/images/wt-logo.png" width="279" height="250" alt="">
</div>
<div class="wt_tagAll">
<div class="wt_tagLine">stainless steel tag that can be easily attached to your pet’s collar</div>
<div class="wt_whatImg"><img src="/wagtag/images/product.png" width="235" height="245" alt="" /></div>
<div class="wt_rytArw"><img src="/wagtag/images/ryt-arw.png" width="34" height="38" alt="" /></div>
<div class="wt_idArw">Wag ID</div>
<div class="wt_lftArw"><img src="/wagtag/images/lft-arw.png" width="46" height="53" alt="" /></div>
<div class="wt_calArw">Call Us</div>
</div>
</div>
<div class="wt_infoBox">
<h2>What is Wag Tag?</h2>
<div class="wt_whatTxt">
<p>wag tag is a high grade stainless steel tag that can be easily attached to your pet’s collar and will ensure safety of your pets. Giving you respite from sleepless nights.</p>
<p>The wag tag helps you to not only get your pet back but maintains confidentiality for the pet owner. Each of the tag comes with a unique code, if your pet gets lost the finder will call the number given on the tag and it will be connected to DogSpot. Our customer care will in turn connect the call to the rightful owner. </p>
</div>
<!--<div class="wt_tagAll">
<div class="wt_tagLine">stainless steel tag that can be easily attached to your pet’s collar</div>
<div class="wt_whatImg"><img src="images/product.png" width="235" height="245" alt="" /></div>
<div class="wt_rytArw"><img src="images/ryt-arw.png" width="34" height="38" alt="" /></div>
<div class="wt_idArw">Wag ID</div>
<div class="wt_lftArw"><img src="images/lft-arw.png" width="46" height="53" alt="" /></div>
<div class="wt_calArw">Call Us</div>
</div>-->
</div>

<!--<div class="wt_howBox">
<h2>How can you  get wag tag</h2>
<div class="wt_getInfo">
 You can claim your wag tag by just logging onto your pet’s profile that is created on Wag Club. If you have not added your dog on Wag Club then this will be the time to do that.  You just have to place a request online providing your shipping details and the wag tag will be delivered to you. It comes with an activation code that will help to activate the tag and ensure that pet is safe always.
 </div>
 </div>-->
 <div class="wt_howBox">
<h2> Information on your tag</h2>
<p>The tag will contain the following details that will help the pet to be reunited with its owners</p>
<div class="wt_fillInfo">
<ul>
<li>The tag will contain information of the owner, such as his number, name and address though this is not visible to the owner.</li>
<li>In addition it contains details of the pet such as medical information such as allergies or any specific medications.</li>
<li>Vaccination details of the lost pet</li>
<li> Dietary requirements of the pet in case any special requirements are there</li>
<li>It will also give information if the pet is spayed or natured and a description of his behavior</li>
</ul>
</div>
</div>

<!--<div class="wt_howBox">
<h2>How can you  get wag tag</h2>
<div class="wt_leftHow">
<div class="wt_leftHowBox">
<p><strong>Online</strong>: You can claim your Wag Tag by just logging onto his or her pet’s profile that is created on Wag Club. You just have to place a request online provide your shipping details and the Wag Tag will be delivered to you. It comes with an activation code that will help to activate the tag and ensure that pet is safe always.</p>
</div>
<div class="wt_leftHowBox">
<p><strong>Offline</strong>: The tag will be distributed in various Dog Shows and associated events that 
The tag will contain the following details that will help the pet to be reunited with its owners</p>
<ul>
<li>The tag will contain information of the owner, such as his number, name and address though this is not visible to the owner.</li>
<li>In addition it contains details of the pet such as medical information such as allergies or any specific medications.</li>
<li>Vaccination details of the lost pet</li>
<li>Dietary requirements of the pet in case any special requirements are there</li>
<li>It will also give information if the pet is spayed or natured and a description of his behavior</li>
</ul>
</div>
</div>
<div class="wt_rytHow">
<p>Wag Tag is one of its kind tag that you cannot get from the market. Yes, these tags are absolutely free the only pre requisite is that you have to register your dog on Wag Club. You can get you Wag Tag free by two ways i.e. online and offline</p>
</div>
<p>The finder if he wants can access on the WagClub with the unique id of the tag and obtain the required information.</p>
</div>--> 
<div class="wt_howBox">
<h2>How will it help to get your pet back?</h2>
<div class="wt_whatTxt">
<ul>
<li>Every dog is allocated a unique id eliminating chances of any errors</li>
<li>The owners number is kept confidential, the call is routed to the owner through DogSpot customer care</li>
<li>The tag is transferred when the owner is changed</li>
<li>wagtag can be easily activated by logging on to DogSpot or through the customer service IVR</li>
<li> Pet owners can update the information as and when required</li>
</ul>
</div>
<div class="wt_howBox">
<h2>What to do when you lose a tag?</h2>
<p>You will have to place an order for another tag which will have a unique identification number that will be different from the previous one. The moment the new tag is generated the older tag will get deactivated. Once you receive the new tag update the information on your pet's profile</p>
</div>
<div class="wt_howBox">
<h2>What happens when the owner is changed?</h2>
<p>The wag tag comes with a unique transfer feature. If you give your dog to someone; then you can also transfer the wag tag to him or her. You just have to log on the profile page of your pet and select the Transfer your tag option.</p>
</div>
<div class="wt_howBox">
<h2>Comparison</h2>
<div class="wt_paraBox">
<ul>
<li><div class="wt_paraHead"><strong>Features</strong></div></li>
<li><div class="wt_wagPara"><strong>Wag tag</strong></div></li>
<li><div class="wt_tradPara"><strong>Traditional tag</strong></div></li>
</ul>
<ul>
<li><div class="wt_paraHead">Privacy of information</div></li>
<li><div class="wt_wagPara">yes</div></li>
<li><div class="wt_tradPara">no</div></li>
</ul>
<ul>
<li><div class="wt_paraHead">Visibility</div></li>
<li><div class="wt_wagPara">yes</div></li>
<li><div class="wt_tradPara">yes</div></li>
</ul>
<ul>
<li><div class="wt_paraHead">Scanning without smartphone</div></li>
<li><div class="wt_wagPara">yes</div></li>
<li><div class="wt_tradPara">yes</div></li>
</ul>
<ul>
<li><div class="wt_paraHead">Value for money</div></li>
<li><div class="wt_wagPara">yes</div></li>
<li><div class="wt_tradPara">yes</div></li>
</ul>
<ul>
<li><div class="wt_paraHead">Dietary information</div></li>
<li><div class="wt_wagPara">yes</div></li>
<li><div class="wt_tradPara">no</div></li>
</ul>
<ul>
<li><div class="wt_paraHead">Medical information</div></li>
<li><div class="wt_wagPara">yes</div></li>
<li><div class="wt_tradPara">no</div></li>
</ul>
<ul>
<li><div class="wt_paraHead">Updation of information </div></li>
<li><div class="wt_wagPara">yes</div></li>
<li><div class="wt_tradPara">no</div></li>
</ul>
</div>
</div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>