
<?php
require_once('../session-no.php');
require_once('../arrays.php');
require_once('../database.php');
require_once('../functions.php');

$puppyDet = puppyDetails($puppi_id);
$puppy_owner = $puppyDet[userid];
$puppi_breed = $puppyDet[puppi_breed];
$dam = $puppyDet[dam];
$dam_kci = $puppyDet[dam_kci];
$sire = $puppyDet[sire];
$sire_kci = $puppyDet[sire_kci];
$day = $puppyDet[day];
$month = $puppyDet[month];
$year = $puppyDet[year];
$puppi_img = $puppyDet[puppi_img];
$puppi_desc = $puppyDet[puppi_desc];
$c_date = $puppyDet[c_date];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Dog lovers Community Health Information</title>
<meta name="keywords" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<script src="/js/ajax.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<?php $sitesection = "puppie"; ?>
<?php require_once('../common/top.php'); ?>


  <div id="pagebody">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top"><?php require_once('../common/navi.php'); ?></td>
        <td valign="top"><div id="inbody">
              <p class="sitenavi"><a href="/">Home</a> &gt; <a href="/dogs/">My puppies</a> >> puppy</p>
            <p><a href="new">Add puppies</a></p>
            <div id="imgbox"><? 
				  if($puppi_img){ ?>
				  <a href="<? echo"images/$puppi_img";?>" class="highslide" onclick="return hs.expand(this, {captionId: 'caption1'})">
                  <img src="<? echo"images/thumbs/thumb_$puppi_img";?>" alt="<? echo"$puppi_breed";?>" title="Click to enlarge" width="100" height="87" class="highslide" /></a> 
<? }else{ ?>
<img src="/images/no-img.jpg" alt="No image Photo available" width="100" height="87" border="0"/> 
<? }?></div>
            <div class="dtd" id="fieldTit">Puppy Owner:</div>
            <div class="dtd" id="fieldValue"><a href="<? echo"/profile/$puppy_owner/"; ?>"><? print (dispUname($puppy_owner));?></a></div>
            <div class="ltd" id="fieldTit">Breed:</div><div class="ltd" id="fieldValue"><? echo"$puppi_breed";?></div>
            <div class="dtd" id="fieldTit">Name of DAM:</div><div class="dtd" id="fieldValue"><? echo"$dam";?></div>
            <div class="ltd" id="fieldTit">Name of SIRE:</div>
            <div class="ltd" id="fieldValue"><? echo"$sire";?>&nbsp;</div>
            <div class="dtd" id="fieldTit">DOB:</div>
            <div class="dtd" id="fieldValue"><? echo"$day - $month - $year";?></div>
            <div id="boxhead">Puppiees Details</div>
         	<div id="boxbody">
            <div id="pupnum">Number</div>
            <div id="pupsex">Sex</div>
            <div id="pupcol">Color</div>
            <div id="puppri">Price</div>
            <div id="puprem"></div>
			<?
			$result = mysql_query ("SELECT * FROM puppies WHERE puppi_id = '$puppi_id'"); 
				if (!$result){
				die(sql_error());
			}
			while($row = mysql_fetch_array($result)){
	 		$puppinum = $row["puppinum"];
			$puppi_sex = $row["puppi_sex"];
			$puppi_color = $row["puppi_color"];
			$puppi_cost = $row["puppi_cost"];
			?>
            <div id="pupnum" <? if($tr==0){ echo"class='ltd'";  }else{ echo"class='dtd'"; } ?>><? echo"$puppinum";?></div>
            <div id="pupsex" <? if($tr==0){ echo"class='ltd'";  }else{ echo"class='dtd'"; } ?>><? echo"$DogSex[$puppi_sex]";?></div>
            <div id="pupcol" <? if($tr==0){ echo"class='ltd'";  }else{ echo"class='dtd'"; } ?>><? echo"$puppi_color";?></div>
            <div id="puppri" <? if($tr==0){ echo"class='ltd'";  }else{ echo"class='dtd'"; } ?>><? echo"$puppi_cost";?></div>
            <div id="puprem" <? if($tr==0){ echo"class='ltd'"; $tr = 1; }else{ echo"class='dtd'"; $tr = 0;} ?>></div>
            <? }?>
            <div id="clearall"></div> 
            </div>
            <div id="boxhead">Puppiees Description</div>
         	<div id="boxbody"><? echo nl2br($puppi_desc);?></div>
           
            <div id="clearall"></div>       
        </div></td>
      </tr>
    </table>
  </div>
<?php require_once('../common/bottom.php'); ?>
