<?
include($DOCUMENT_ROOT."/facebookconnect/facebooksettings.php");
?>
        <div id="fb-root"></div>
        <script>
          //initializing API
          window.fbAsyncInit = function() {
            FB.init({appId: '<?=FACEBOOK_APP_ID?>', status: true, cookie: true,
                     xfbml: true});
          };
          (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
              '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
          }());
        </script>

        <!-- custom login button -->
        <a href="#" onClick="fblogin();return false;"><img src="facebook-logo-large.png"></a>


        <script>
          //your fb login function
          function fblogin() {
            FB.login(function(response) {
				 window.location = "https://www.dogspot.in/facebookconnect/fblogin.php";
              //...
            }, {perms:'read_stream,publish_stream,offline_access'});
          }
        </script>
