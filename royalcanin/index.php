 <?php

require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT . '/session.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Royal Canin</title>
<link href="style.css" rel="stylesheet" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<script type="text/javascript">
function notify(){
	vat=$('#sd').val();
	ShaAjaxJquary('/royalcanin/index2.php?vat='+vat+'','#ucheck','', '','POST', '#ucheckLoading','Checking....','REP');
}
</script>
</head>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div id="ucheck"></div>
<div class="product_container">
<div class="coming_soon_text">Coming Soon <span class="nopart">3</span> <span class="new_class">New</span>Products in Royal Canin Pure Breed Range</div>
<div class="field_lunch">

<strong>Get Notified when they launch </strong>
<input type="text" value="Email" id="sd" name="email1" onfocus="if(this.value == 'Email') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Email';}" />
<input type="button" id="notify" onclick="notify();" value="Notify Me" />


</div>
<div class="lunch_list">
<ul>
<li><img src="images/lunch_product_1.jpg" /></li>
<li><img src="images/lunch_product_2.jpg" /></li>
<li><img src="images/lunch_product_3.jpg" /></li>
</ul>
</div>
</div>

<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom.php');
?>

