<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="EAcsZ1OXjGYexR+hCVtgUTDT+zeClPHe7kvPgUUqUwo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta property="twitter:account_id" content="4503599627971180" />
<title>Pet Supplies Store | Online Pet Products Shopping India | DogSpot.in</title>

<meta name="keywords" content="Dogs India, Dog Pictures, Dog Breeds, Dog Names, Dog Training, Types Of Dogs, Dog Images, Puppies In India, Dogs, Dog, Dog Games, Dog Kennel, Dog Grooming, Dog India, Dog Show" />

<meta name="description" content="DogSpot.in Pet Supplies Store - India's best online shopping site for pets supplies, accessories & products for pets. Shop for your pets and read information on pet training, breeders, dog shows, dog photos, pet articles and more." />
<meta property="og:image" content="http://www.dogspot.in/new/breed_engine/images/logo-300x300.jpg" /> 
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.dogspot.in/" /> 
<link rel="canonical" href="http://www.dogspot.in/" />

<link rel="stylesheet" type="text/css" href="http://d3srewv59jy33q.cloudfront.net/new/css/css.php" />
<script type="text/javascript" src="http://d3srewv59jy33q.cloudfront.net/new/css/js.php"></script>
<script type="text/javascript" defer="defer" src="http://d3srewv59jy33q.cloudfront.net/new/slide/jquery.jcarousel.pack.js"></script>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<!--Adding Defer attribute in script tag Date: 18-Nov-2014-->

<!--End-->


<script defer="defer" type="text/javascript">
//showhidediv_slider('featured','0','5');
function showhidediv_slider(itemtype,start,end,aid){
	//alert(itemtype);
	var cookieval=$('#cookieval').val();
	var user=$('#userfor').val();
	if(cookieval>0 && itemtype=='featured'){
	$(".prod_tabs").removeClass("active_hp");
	$("#"+aid).addClass("active_hp");
	var end=4;
	ShaAjaxJquary('/new/includes/recent-home.php?type='+itemtype+'&start='+start+'&end='+end+'', '#product_div', '', '', 'POST', '#product_div', '<img class="load_hp" src="/new/pix/loadingGif.gif" width="40" height="40" alt="Loading" title="Loading" />', 'REP');
	
	}else{
	$(".prod_tabs").removeClass("active_hp");
	$("#"+aid).addClass("active_hp");
	ShaAjaxJquary('/new/home_include.php?type='+itemtype+'&start='+start+'&end='+end+'', '#product_div', '', '', 'POST', '#product_div', '<img class="load_hp" src="/new/pix/loadingGif.gif" width="40" height="40" alt="Loading" title="Loading" />', 'REP');
	//}
}
}
function showhidediv_slider_inc(itemtype,start,end,type){
	if(type=='prev'){
		var start1=parseInt(start)-5;
		//var end1=parseInt(end)-5;
	}
	if(type=='next'){
		var start1=parseInt(start)+5;
		//var end1=parseInt(end)+5;
	}
	var cookieval=$('#cookieval').val();
	var user=$('#userfor').val();
	if(cookieval>0 && itemtype=='featured'){
	if(type=='prev'){
		
		var start2=parseInt(start1)-1;
		
		var end1=parseInt(start2)+4;
		
	}else{
	var start2=parseInt(start1)+1;
	var end1=parseInt(start2)+4;
	}
	ShaAjaxJquary('/new/includes/recent-home.php?start='+start2+'&end='+end1+'', '#product_div', '', '', 'POST', '#product_div', '<img class="load_hp" src="/new/pix/loadingGif.gif" width="40" height="40" alt="Loading" title="Loading" />', 'REP');
	}else{
	ShaAjaxJquary('/new/home_include.php?type='+itemtype+'&start='+start1+'&end='+end+'', '#product_div', '', '', 'POST', '#product_div', '<img class="load_hp" src="/new/pix/loadingGif.gif" width="40" height="40" alt="Loading" title="Loading"  />', 'REP');
}
}

</script>
<!--TOP HTML END-->
<script defer="defer" type="text/javascript">
var abc_home='';
function showhidediv_home(parent,main_div1,child1,child2){

if(abc_home != ''){
	document.getElementById(abc_home).style.display="none";
	$(".category_a").removeClass("active_Menu_hp");
}
	document.getElementById(main_div1).style.display="block";
	abc_home=document.getElementById(main_div1).id;
	$(".category_a").removeClass("active_Menu_hp");
	$("#"+parent).addClass("active_Menu_hp");
	$	
}
/*$('#drop-menu_width_home').mouseleave(function(){
$(".category_a").removeClass("active_Menu_hp");
//$('.'+main_div1).hide();
})*/
</script>
<script type="text/javascript"> 
 function drop_home() {
	var ty=document.getElementById('drop-menu_width').style.display;
	if(ty=='none'){
		document.getElementById('drop-menu_width').style.display='inline';
		document.getElementById('arw_wht').style.display='none';
		document.getElementById('arw_grn').style.display='block';
		$('#shoplink').addClass("selected");
	}/*else{
		document.getElementById('drop-menu_width').style.display="none";
	}*/
 }
</script>

<link href="/new/css/home-style.css" rel="stylesheet" type="text/css" />
<link href="/new/slide/skins1.css" rel="stylesheet" type="text/css" />
<link href="/new/css/shop-new.css" rel="stylesheet" type="text/css" />
<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="/new/css/all-ie-only.css" />
<![endif]-->
<link href="/shop-slider/css/nivo-slider2.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" defer="defer" src="/shop-slider/js/jquery.nivo.slider.js"></script>
<script type="text/javascript" defer="defer" src="/shop-slider/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" defer="defer" src="/shop-slider/js/jquery.nivo.slider-index4.js"></script>
    <script type="text/javascript">
       $(function() {
		$('#slider').nivoSlider();
		//$('#slider1').nivoSlider();
    });
    </script>

<!-- function for photo slider ends here -->

<script> 
$(function(){
	start();
});

function start(){
	$('.bannerSlide ul:gt(0)').hide();
	slideShop = setInterval(function(){$('.bannerSlide ul:first-child').hide(1000).next('ul').show(1000).end().appendTo('.bannerSlide');}, 5000);	
}

</script>
<style type="text/css"> 
<!--
.bannerSlide {position:relative; height:248px; width:660px; z-index:0;}
.bannerSlide ul { position:absolute; left:0; top:0; }
-->
.christmas_bell_left
{
position: absolute;
float: left;
display: block;
left: 75px;
top: 184px;
}

.christmas_bell_right
{
position: absolute;
float: left;
display: block;
top: 181px;
right: 75px;
}
@media screen and (max-width: 1300px) {
.christmas_bell_left
{
display:none;
}
.christmas_bell_right
{
display:none;
}
}
</style> 

<script type="text/javascript">
function abcd() {
	//$('a.login-window').click(function() {
ga('send', 'event', 'Need Help', 'Yes', {'page':'//'});		
var loginBox = $('a.login-window').attr('href');
$(loginBox).fadeIn(300);

var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
//});
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});

};

</script>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script> 
$(function(){
	$('#homeSlide a:gt(0)').hide();
	slideShop = setInterval(function(){$('#homeSlide a:first-child').hide(1000).next('a').show(1000).end().appendTo('#homeSlide');}, 5000);
});
$(function(){
	$('#homeSlide1 a:gt(0)').hide();
	slideShop = setInterval(function(){$('#homeSlide1 a:first-child').hide(1000).next('a').show(1000).end().appendTo('#homeSlide1');}, 5000);
});
	
</script>
<style type="text/css"> 
<!--
#homeSlide {position:relative; height:260px; width:486px;}
#homeSlide a{ position:absolute; left:0; top:0; }
#homeSlide1 {position:relative; height:260px; width:486px;}
#homeSlide1 a{ position:absolute; left:0; top:0; }
.rytSide-banner.fr {
width: 315px;
margin-top: 13px;
}
.sideBannerBox {
border: 1px solid #ccc;
}
.rytBorBottom.fl {
width: 302px;
border-bottom: 0px solid #ccc;
margin:7px 0;
}
</style> 
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />


<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="msvalidate.01" content="0665BC887BAD14D71FDE0CFE204AE648"/>
<meta name="alexaVerifyID" content="Jsm1u5BIbORxpUFCf7kGODiQNB8"/>

<!-- MICROFORMATS FOR GPLUS -->
<link href="https://plus.google.com/+indogspot/" rel="publisher"/>
<!----- Feed Burner Starts Here---->
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Blogs" href="http://feeds.feedburner.com/dogspotblog" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Q&A" href="http://feeds.feedburner.com/DogspotinQna" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Dog Gallery " href="http://feeds.feedburner.com/DogspotDogs" />
<link rel="alternate" type="application/rss+xml" title="Dog Spot Puppies Available" href="http://feeds.feedburner.com/DogspotPuppies" />
<!----- Feed Burner Ends Here---->
<!----- aLL jS fILES---->
<!----- aLL jS fILES ENDS HERE---->
<script type='text/javascript' src='/ajax-autocomplete/jquery.autocomplete.js'></script>

<!-----------------------------------------location finder starts-------------------------------------------------------->
<head>

<link rel="stylesheet" href="/new/common/css/header-style-new5.css" />
<link href="/new/common/css/mega_menu6.css" type="text/css" rel="stylesheet" />
<link href="/new/common/css/tabs-ui.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
function callsharengain(){
	centerPopup('#popupContactSnG');
	loadPopup('#popupContactSnG');	
}
</script>
<script type="text/javascript">
 $(document).ready(function() {
/*	 $("#searchbox").focus(function(e) {
		$("#display").text(""); 
        $("#display").css("display","block");
    });
	$("#searchbox_div").blur(function(e) {
        $("#display").css("display","none");
    });*/
	 $("#qa-my-acc-link-new").click(function(e) {
		$("#my_account_new").toggle();
    });
	 
      $("#loginFormnew").validate({
        
    });
	$("#resigterFormnew").validate({
        
    });
	 $("#form-validate-create-new").validate({
        
    });



	//$('#drop-menu_width_home').mouseleave(function(){
	//$('.cat-content').hide();
	//$('.category_a').removeClass('active_Menu_hp');
	//})
	$("#forgt_pass_cancel_btn").click(function(e) {
        $(".main_div").css("display","none");
		$("#login_div").css("display","block");
    });
});
</script>
<style>media (max-width:1269px){
	.diwali-theme{
		display:none
	}

.diwali-theme
{
    position: absolute;
    float:left; 
    text-align:left;
    margin-top:-50px;
}</style>
<script> 
$(document).ready(function(){
   $("#topbanner").slideDown("fast");
	$("#closetopbanner").click(function(e) {

        $("#topbanner").slideUp("slow");
		$("#downarrowdiv").slideDown("slow");
		$("#dogtop_margin").css("top","123px");
    });
		$("#showtopbanner").click(function(e) {

        $("#topbanner").slideDown("slow");
		$("#downarrowdiv").slideUp("slow");
		$("#dogtop_margin").css("top","159px");
    });
});
</script>
<script>
$(function() {
    var button = $('#searchbox');
    var box = $('#display');
    var form = $('#search_result_div');
    button.removeAttr('href');
    button.mouseup(function(resigter) {
        box.toggle();
        button.toggleClass('active');
    });
    form.mouseup(function() { 
        return false;
    });
    $(this).mouseup(function(login) {
        if(!($(login.target).parent('#searchbox').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });
});
</script>
<script>
$(function() {
    var button = $('#qa-my-acc-link-new');
    var box = $('#my_account_new');
    var form = $('#reguser');
    button.removeAttr('href');
    button.mouseup(function(login) {
        box.toggle();
        button.toggleClass('active');
    });
    form.mouseup(function() { 
       return false;
    });
    $(this).mouseup(function(login) {
        if(!($(login.target).parent('#qa-my-acc-link-new').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });
});
</script>

<script type="text/javascript">
function show(){
    if(document.getElementById('show_pass_new').checked){
    var w=document.getElementById('cust_password_new').value;
    document.getElementById('cust_password_new').type='text';
    document.getElementById('cust_password_new').value=w;
}else{
    document.getElementById('cust_password_new').type='password';
    }
}
	function show_main_divs(divid)
	{
		if(divid=='forgot_div'){
		var forgot_val=$("#login_email_new").val();
		$("#RegistrationFormnew").val(forgot_val);
		}
		$(".main_div").css("display","none");
		$("#"+divid).css("display","block");
	}
	
	function cancel_header_form(){
	$(".main_div").css("display","none");	
	$('#mask , .login-popup').fadeOut(300 , function() {
	$('#mask').remove();
	});
	}
    </script>


<!----validation for login panel start here	--->
<script type="text/javascript">
function checkpassword()
{
var pin=document.getElementById('login_email_new').value;
var password=document.getElementById('passwordnew').value;
var login='login';
ShaAjaxJquary('/new/common/checkuser1.php?email='+pin+'&password='+password+'&emailcheck='+login+'', '#login_error1', '', '', 'POST', 
'#login_error1', '', 'REP');
}
</script>
<!----validation for login panel ends here	--->
<script>
function checkthis(){
var pin=document.getElementById('cust_email_new').value;
if(pin!='' && pin!='Email*'){
var emailcheck='emailcheck';
ShaAjaxJquary('/new/common/checkuser1.php?email='+pin+'&emailcheck='+emailcheck+'', '#customer_error1_new', '', '', 'POST', '#customer_error1_new', '', 'REP');
		 }
}

function validpassword(){
		var email =document.getElementById('RegistrationFormnew').value;
		if(email !=''){
		$("#RegistrationFormnew").css("border-color","#999");
		document.getElementById('password_error1new').style.display='none';
		 ShaAjaxJquary('/new/common/forgetpassword.php?emailid='+email+'', '#DivStateLoading', '', '', 'GET', '#DivStateLoading', 'Loading...', 'REP');
		 document.getElementById('reset_passwordnew').style.display='none';
		}else{
		$("#RegistrationFormnew").css("border-color","#F00");
		}
}
function closed() {
	document.getElementById('DivStateLoading').innerHTML = '';
	document.getElementById('reset_passwordnew').style.display='block';
	document.getElementById('RegistrationFormnew').value='';
	document.getElementById('forgot_password_new').style.display='none';
	
}
function regsrclos() {	
	document.getElementById('new_customer_form').style.display='none';	
}
function resetpasd() {	
	document.getElementById('forgot_password_new').style.display='none';	
}
function showNewfeedbackForm() {	
	document.getElementById('loginBoxnew').style.display='none';
	document.getElementById('forgot_password_new').style.display='block';	
}
function clickclear() {
	document.getElementById('search-query-new').style.backgroundColor = "white";
}

function filevalidate(){
	if(document.getElementById('search-query-new').value=='' || document.getElementById('search-query-new').value=='Search e.g. Dog Food'){
		document.searchshopform.shopQuery.focus();
		
		return false;
			}
		else{
			
		//document.getElementById('search-query-new').style.background = "white";
		searchshopform.submit();
	 }
}
<!----- Validation for login and register ends here here---->
var countpasswd='0';
function changetype(){

	if(countpasswd=='0'){
document.getElementById("cust_password_new").type="text";
//document.getElementById("pwdtemp1").type="password";
countpasswd='1';

	}
		else{
document.getElementById("cust_password_new").type="password";
//document.getElementById("pwdtemp1").type="text";
countpasswd='0';
	}
	
}
</script>
<script>

function loginclose()
{
	//alert('few');
	$("#loginBoxnew").css("display","none");
}
function registerclose()
{
	//alert('few');
	$("#resigterBoxnew").css("display","none");
}
function customerclose()
{
	//alert('few');
	$("#customernew").css("display","none");
}
function forgotclose()
{
	//alert('few');
	
	document.getElementById("form-validate-create-new").reset();
	$("#forgot_password_new").css("display","none");
	document.getElementById('password_error1new').style.display='none';
	document.getElementById('reset_password').style.display='none';
	document.getElementById('reset_passwordnew').style.display='block';
	document.getElementById('DivStateLoading').style.display='none';
}
/*function accdivclose()
{
	//alert('few');
	$("#my_account_new").css("display","none");
}

$(document).not("#qa-my-acc-link-new").click(function(e) {
    $("#my_account_new").css("display","none");
});*/
function filevalidate(){
	if(document.getElementById('search-query-new').value=='' || document.getElementById('search-query-new').value=='Search e.g. Dog Food'){
		document.searchshopform.shopQuery.focus();
		
		return false;
			}
		else{
			
		//document.getElementById('search-query-new').style.background = "white";
		searchshopform.submit();
	 }
}
</script>
<script type="text/javascript">

$(document).ready(function(){
$(".search").keyup(function() 
{
var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;
if(searchbox=='')
{}
else
{
$.ajax({
type: "POST",
url: "/new/common/getall1.php",
data: dataString,
cache: false,
success: function(html)
{
$("#display").html(html).show();
}
});
}return false; 
});
});

function showw(vall){
	window.location.href="http://dogspot.in/"+vall+"/";
	}
</script>
<!----------------------------------------------------old search code--------------------------------------------------------->

<script type="text/javascript">
 var aliceImageHost = '#';
        var aliceStaticHost = '#';
        var globalConfig = {};   
$().ready(function() {
		$("#search-query-new").autocomplete("/new/shop/getall.php", {
		width: 390,
		matchContains: true,
		selectFirst: false
	});
	 
	 $(".slidingDiv").hide();
        $(".show_hide").show();
 
    $('.show_hide').click(function(){
    $(".slidingDiv").slideToggle();
    });
});
</script>

<!-------------------------------------------------------ends here------------------------------------------------------------->

<style>
.robotic { display: none; }

label.error{
color:#F00!important;
line-height: 17px!important;
font: bolder 14px arial!important;
margin-right: 1px!important;
display:none !important;
text-align: right!important;
font-weight:100!important;
float:left;
font-family:Arial!important;
font-size:12px!important;
}
input.error{
border:1px medium!important;}

input::-webkit-input-placeholder {
    color: #CCC;
}
input:focus::-webkit-input-placeholder {
    color: white;
}
#loginFormnew input.error {
border: 1px solid #f00;
}
#resigterFormnew input.error{
	/*display:block !important;  */
	border: 1px solid #f00;
}
#resigterFormnew select.error{
	/*display:block !important;  */
	border: 1px solid #f00;
}

/* Firefox < 19 */
input:-moz-placeholder {
    color: #CCC;

}
input:focus:-moz-placeholder {
    color: white;
}
/* Firefox > 19 */
input::-moz-placeholder {
    color: #CCC;
}
input:focus::-moz-placeholder {
    color: white;
}

/* Internet Explorer 10 */
input:-ms-input-placeholder {
    color: #CCC;
}
input:focus:-ms-input-placeholder {
    color: white;
}

</style>
<style>
#search_bar_div {
  position: relative;
/*  top: 0;
   padding-top: 19px;
 margin-top: 20px;
  border-top: 1px solid purple;

margin-top:5px;*/
 padding-top: 0px;
margin-left: 0px;
z-index: 9999;
display:none
}

#search_bar_div.fixed {
  position: fixed;
  top: -5px;
  float:left;
}
</style>

<!-----------------------------------------------------------new analytics code------------------------------------------->
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
Date();a=s.createElement(o),
 
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1552614-5', 'auto');
  ga('require', 'ec');
  
<!-- Analytics code for every section starts here -->
ga('require', 'displayfeatures');
ga('send', 'pageview');
<!-- Analytics code for every section ends here -->
</script>
<!-- ViralMint CSS -->
<style type="text/css">
	#vm-container, #vm-close{ top: 35px !important;}
</style>
<!-- ViralMint CSS -->

<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1423135197964769']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>

<!-----------------------------------------------code for pop-up------------------------------------------------------------->
<script type="text/javascript">
$(document).ready(function() {
$('a.login-window-header').click(function() {
$(".main_div").css("display","none");
$("#login_div").css("display","block");
var loginBox = $(this).attr('href');
$(loginBox).fadeIn(300);

var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;

$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});
});

$(document).ready(function() {
$('a.reg-window-header').click(function() {
$(".main_div").css("display","none");
$("#reg_div").css("display","block");	
var loginBox = $(this).attr('href');
$(loginBox).fadeIn(300);

var popMargTop = ($(loginBox).height() + 24) / 2;
var popMargLeft = ($(loginBox).width() + 24) / 2;
$(loginBox).css({
'margin-top' : -popMargTop,
'margin-left' : -popMargLeft
});
$('body').append('<div id="mask"></div>');
$('#mask').fadeIn(300);
return false;
//});
$('a.close, #mask').live('click', function() {
$('#mask , .login-popup').fadeOut(300 , function() {
$('#mask').remove();
});
return false;
});
});
});
</script>

<!-------------------------------------ends here------------------------------------------------------------------------------>

<noscript><img height="1" width="1" alt="" rel="nofollow" style="display:none" src="http://www.facebook.com/tr?id=1423135197964769&amp;ev=NoScript" /></noscript>
</head>
<body>
<link rel="stylesheet" href="/new/common/css/head_new.css?date=1252014" />
<link rel="stylesheet" href="/new/common/css/header-style-home.css?date=2562014" />


<div class="siteBanner fl" style="display:none" id="topbanner">
<div class="cont980">
<div class="spot_li_hp fl">
<ul>

<li  style="border-right:none;"><a href="/jobs.php" target="_blank"><img src="/new/common/images/headImg/we-are-hiring.gif" width="110" height="20"></a></li></ul>

</div>
<div class="rytTop_hp fr">
        <div class="fb_dogSpot" id="fb_like_head">  
          <div class="fb-like" data-href="http://www.facebook.com/indogspot" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
        
<div class="spot_li_hp fr">
<ul><li>Call our Pet Helpline  <span itemprop="telephone">+91-9212196633</span></li>
<li><a href="/contactus.php">Customer Support</a></li>
<li class="no_border_hp"><a id="qa-my-acc-link-new" class="arrow" style="cursor:pointer;">Shalesh</a></li>
 <!--registered user starts--> 
  <form id="reguser" name="reguser" action="" method="POST">
<div class="logIN_box fl" style="width:150px;display:none" id="my_account_new">
<span class="index_arrow-bottom" title="down-arrow"></span>
<div class="logDetail_box fl" style="width: 153px;">
<ul>
<li><a href="/new/shop/myorders.php" title="My Orders" rel="nofollow">My Orders</a></li>
<li><a href="/new/shop/reorder_details.php?utm_source=DogSpot-Reorder&utm_medium=Reorder&utm_campaign=DogSpot-Reorder" title="Reorder" rel="nofollow">Reorder</a></li>
<li><a href="/profile/" title="My Wishlist" rel="nofollow">My Wishlist</a></li>
<li><a href="/profile/" title="My Profile" rel="nofollow">My Profile</a></li>
<li><a href="/dogs/owner/shailesh/" title="My Dogs" rel="nofollow">My Dogs</a></li>
<li><a href="/wag_club/add_dog.php" title="Add Your Dog">Add Your Dog</a></li>
<!--<li><a >Change Password</a></li>-->
</ul>
<div class="log_newHead"><a href="/logout.php?logoff=logoff" title="Logout">Logout</a></div>
</div>
</div>
</form>
 <!--registered user ends--> 

</ul>
</div>
</div>
</div>
</div>
<div class="" id="search_bar_div" style="display:block">
<div class="siteBox_new fl">
<div class="cont980">
<div class="logo_new fl"><a href="http://www.dogspot.in/" >
  <img src="http://d3srewv59jy33q.cloudfront.net/new/common/images/headImg/logo.jpg" width="184" height="45" alt="Dogspot" title="Dogspot"></a>
</div>
<div class="search_new fl srch_hp" id="searchbox_div">
 
<form action="/new/common/all-search.php" id="cse-search-box" onSubmit="return filevalidate();" name="searchshopform">
  <input name="shopQuery" onKeyUp="clickclear()" id="search-query-new" value="" type="text" placeholder="Search Category, Brand, Products" />
  <img src="/new/common/images/headImg/srch.png" width="41" onClick=" return filevalidate();" height="33" alt="Search" style="cursor:pointer" />
  </form>
</div>
<div class="cart_hp fr">
<div class="imgCart_hp fl"><img src="/new/common/images/cart_hp.png" width="26" height="21" alt="Cart" title="Cart"></div>
<a href="https://www.dogspot.in/new/shop/new-cart.php" style="cursor:pointer"><div class="labelCart_hp fl">Cart </div></a>
<div class="item_hp" id="cartItemCountheader">0</div>
</div>

</div>
</div>

</div>

<div id="login-box-header" class="login-popup" style="padding:0; box-shadow:none; border:0px solid #fff;background: rgba(255, 255, 255, 0);">

<!-- Login Popup-->
<div class="LogINBox_hp main_div" id="login_div" style="display:none">
<div class="logbanner_hp"><img src="/new/pix/login-banner.png" alt="login-banner" width="306" height="198" title="login-banner"></div>
<a id="popupContactClose1" style="cursor: pointer !important;position: absolute;right: 14px;color: #333;font-weight: bold;font-size: 13px;text-decoration: none;top: 6px;" onClick="cancel_header_form()"> <span style=""> [X]</span></a>
<form id="loginFormnew" name="loginFormnew" action="" method="POST" class="">
<div class="check_log">
<div class="newUser_hp">New User? <a id="login_reg" style="cursor:pointer" onClick="show_main_divs('reg_div')">Signup</a></div>
<h3>Login to your account</h3>
<div class="check_logbox">
<label>Enter Email Address</label><br>
<input name="login_email_new" value=""  tabindex="1" class="required checkOut formField signin_input" id="login_email_new" type="text">
<div id="pass" style="display: inline-block;">
<label>Enter Password</label><br>
<input name="passwordnew" tabindex="2"  class="required checkOut formField signin_input" id="passwordnew" onBlur="restoreBox();" type="password"><br>
<input name="refUrl" id="refUrl" type="hidden" value="/"/> 
<div class="fl" style="margin: 7px 0 0px 0;">
<span class="fl"><input name="remembermenew" type="checkbox" class="checkbox" id="remembermenew" value="Y" /></span>
<span class="fl">Remember me</span>
</div>

</div>

<p class="btn_conti">
<input id="login_btn_new" name="login_btn_new" value="Login" type="submit" onClick="return validateLogin();">
</p>
<div class="for_pass"><a id="login_forgot" style="cursor:pointer" onClick="show_main_divs('forgot_div')">Forgot your password?</a></div>

<input type="hidden" id="donate_add" name="donate_add" value="">
<input type="hidden" id="donatebag" name="donatebag" value="">
</div>

<div class="or_img"><img src="/new/shop/images/img/or_img.png" width="35" height="175" alt="" /></div>
<div class="alt_log">
<label>Login with</label>
<div class="fb_log">
<div id="fb-root"></div>
        <script>
          //initializing API
          window.fbAsyncInit = function() {
            FB.init({appId: '119973928016834', status: true, cookie: true,
                     xfbml: true});
          };
          (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
              '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
          }());
        </script>
        <!-- custom login button -->
                <a href="javascript:voide();" onClick="fblogin();return false;">
        
        <span class="button-fb1" style="float:left;"><img src="/new/shop/images/img/fb_log.png" width="150" height="50" alt=""></span></a><br /></a>
                <script>
          //your fb login function
          function fblogin() {
            FB.login(function(response) {
				if (response.authResponse){
								 window.location = "http://www.dogspot.in/facebookconnect/fblogin.php?refUrl=/";
				               //...
			}else{
			 window.location = "http://www.dogspot.in/";
			}
            }, {perms:'email,user_birthday,user_location,publish_stream'});
          }
        </script>
       <input type="hidden" name="uro" id="uro" value="/" /></div>
<div class="g_log">
  
<a href="https://www.google.com/accounts/o8/ud?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=http%3A%2F%2Fwww.dogspot.in%2Fgoogleconnect%2Fglogin.php&openid.realm=http%3A%2F%2Fwww.dogspot.in&openid.ns.ax=http%3A%2F%2Fopenid.net%2Fsrv%2Fax%2F1.0&openid.ax.mode=fetch_request&openid.ax.type.namePerson_first=http%3A%2F%2Faxschema.org%2FnamePerson%2Ffirst&openid.ax.type.namePerson_last=http%3A%2F%2Faxschema.org%2FnamePerson%2Flast&openid.ax.type.contact_email=http%3A%2F%2Faxschema.org%2Fcontact%2Femail&openid.ax.type.birthDate=http%3A%2F%2Faxschema.org%2FbirthDate&openid.ax.type.person_gender=http%3A%2F%2Faxschema.org%2Fperson%2Fgender&openid.ax.type.contact_postalCode_home=http%3A%2F%2Faxschema.org%2Fcontact%2FpostalCode%2Fhome&openid.ax.type.contact_country_home=http%3A%2F%2Faxschema.org%2Fcontact%2Fcountry%2Fhome&openid.ax.type.pref_language=http%3A%2F%2Faxschema.org%2Fpref%2Flanguage&openid.ax.type.pref_timezone=http%3A%2F%2Faxschema.org%2Fpref%2Ftimezone&openid.ax.required=namePerson_first%2CnamePerson_last%2Ccontact_email%2CbirthDate%2Cperson_gender%2Ccontact_postalCode_home%2Ccontact_country_home%2Cpref_language%2Cpref_timezone&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select">
<span class="button-google1" style="float:left"><img src="/new/shop/images/img/g_log.png" width="151" height="50" alt=""></span>
</a>
</div>
</div>

</div>

<div class="LoginBen_hp">
<h3>Login Benefits</h3>
<ul>
<li>Save Your Cart</li>
<li>Get Exclusive Benefits</li>
<li>Quick Re-Order</li>
<li>Save Wish-list</li>
</ul>
</div>
</form>
</div>

<!-- Login Popup-->

<!-- Register Popup-->

<div class="LogINBox_hp main_div" id="reg_div" style="display:none">
<div class="logbanner_hp">
<img src="/new/pix/login-banner.png" alt="login-banner" title="login-banner" width="306" height="198" /></div>
<a id="popupContactClose1" style="cursor: pointer !important;position: absolute;right: 14px;color: #333;font-weight: bold;font-size: 13px;text-decoration: none;top: 6px;" onClick="cancel_header_form()"> <span style=""> [X]</span></a>
<form id="resigterFormnew"  action="" method="POST" name="f1" class="" onSubmit="javascript:checkthis();" novalidate>

<div class="check_log">
<div class="newUser_hp">Already have an account. <a style="cursor:pointer" id="reg_login" onClick="show_main_divs('login_div')">Login</a></div>
<div class="check_logbox">

<label>Enter Email Address</label><br>
<input tabindex="1" name="cust_email_new" type="text" id="cust_email_new" class="searchadd required checkOut formField signin_input email" onBlur="javascript: checkthis();" autocomplete="off" placeholder="" />
<label>Full Name</label><br>
<input tabindex="2" name="fullnamenew" type="text" id="fullnamenew" class="searchadd required checkOut formField signin_input placeholder" autocomplete="off" maxlength="50" placeholder="" />

<label>Enter Password</label><br>
<input tabindex="4" name="cust_password_new" class="required checkOut formField placeholder" placeholder="" id="cust_password_new" onBlur="restoreCreatePasswordBox()" type="password" /> 
<div class="eDetail_box fl">
<span class="fl"><input value="1" tabindex="8" name="show_pass_new" id="show_pass_new" type="checkbox" onChange="return show();" onClick="changetype()" /></span>
<span class="fl">Show Password</span>
</div>
<div class="eDetail_box fl">
<label>You are</label>
<select name="youarenew" id="youarenew" class="required checkOut">
     <option value="">Select</option>
     <option value="21;Pet lover">Pet lover</option>
     <option value="20;Pet owner">Pet owner</option>
     <option value="19;Just curious">Just curious</option>
</select>
</div>
<input id="refurl2" name="refurl" value="/" type="hidden" />
                  <div style="display: none; color: red;" class="row mts" id="customer_error_new"></div>
                   <div style="display:none;color:red;" class="row mts" id="customer_error1_new"></div>
                   <div class="eDetail_box fl" style="margin: 7px 0 3px;">
<span class="fl"><input checked="checked" value="1" style="width:15px" tabindex="8" name="is_newsletter_subscribednew" class="checkOut formField" id="is_newsletter_subscribednew" type="checkbox" /></span>
<span class="fl" style="font-size:11px;"> I would like to subscribe for<br />
DogSpot.in updates & newsletter</span>
</div>
<p class="btn_conti">
<input class="sign_btn" id="account_btn_new" name="account_btn_new" value="Sign Up Now" type="submit" onClick="javascript:checkthis();" tabindex="7" style=""/></p>
<div><div id="error23" class="errorPop" style="display:none">All fields are required</div></div>
<div><div id="error234" class="errorPop" style="display:none">Please Enter Email Id</div></div>
<div><div id="error2345" class="errorPop" style="display:none">Email ID already exist.</div></div>
<input type="hidden" id="donate_add" name="donate_add" value="">
<input type="hidden" id="donatebag" name="donatebag" value="">
</div>

<div class="or_img"><img src="/new/shop/images/img/or_img-reg.jpg" width="35" height="293" alt="" /></div>
<div class="alt_log">
<label>Login with</label>
<div class="fb_log"><div id="fb-root"></div>
        <script>
          //initializing API
          window.fbAsyncInit = function() {
            FB.init({appId: '119973928016834', status: true, cookie: true,
                     xfbml: true});
          };
          (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
              '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
          }());
        </script>
        <!-- custom login button -->
                <a href="javascript:voide();" onClick="fblogin();return false;">
        
        <span class="button-fb1" style="float:left;"><img src="/new/shop/images/img/fb_log.png" width="150" height="50" alt=""></span></a><br /></a>
                <script>
          //your fb login function
          function fblogin() {
            FB.login(function(response) {
				if (response.authResponse){
								 window.location = "http://www.dogspot.in/facebookconnect/fblogin.php?refUrl=/";
				               //...
			}else{
			 window.location = "http://www.dogspot.in/";
			}
            }, {perms:'email,user_birthday,user_location,publish_stream'});
          }
        </script>
       <input type="hidden" name="uro" id="uro" value="/" /></div>
<div class="g_log">
  
<a href="https://www.google.com/accounts/o8/ud?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=http%3A%2F%2Fwww.dogspot.in%2Fgoogleconnect%2Fglogin.php&openid.realm=http%3A%2F%2Fwww.dogspot.in&openid.ns.ax=http%3A%2F%2Fopenid.net%2Fsrv%2Fax%2F1.0&openid.ax.mode=fetch_request&openid.ax.type.namePerson_first=http%3A%2F%2Faxschema.org%2FnamePerson%2Ffirst&openid.ax.type.namePerson_last=http%3A%2F%2Faxschema.org%2FnamePerson%2Flast&openid.ax.type.contact_email=http%3A%2F%2Faxschema.org%2Fcontact%2Femail&openid.ax.type.birthDate=http%3A%2F%2Faxschema.org%2FbirthDate&openid.ax.type.person_gender=http%3A%2F%2Faxschema.org%2Fperson%2Fgender&openid.ax.type.contact_postalCode_home=http%3A%2F%2Faxschema.org%2Fcontact%2FpostalCode%2Fhome&openid.ax.type.contact_country_home=http%3A%2F%2Faxschema.org%2Fcontact%2Fcountry%2Fhome&openid.ax.type.pref_language=http%3A%2F%2Faxschema.org%2Fpref%2Flanguage&openid.ax.type.pref_timezone=http%3A%2F%2Faxschema.org%2Fpref%2Ftimezone&openid.ax.required=namePerson_first%2CnamePerson_last%2Ccontact_email%2CbirthDate%2Cperson_gender%2Ccontact_postalCode_home%2Ccontact_country_home%2Cpref_language%2Cpref_timezone&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select">
<span class="button-google1" style="float:left"><img src="/new/shop/images/img/g_log.png" width="151" height="50" alt=""></span>
</a>
</div>
</div>


</div>
<div class="LoginBen_hp">
<h3>Login Benefits</h3>
<ul>
<li>Save Your Cart</li>
<li>Get Exclusive Benefits</li>
<li>Quick Re-Order</li>
<li>Save Wish-list</li>
</ul>
</div>
</form>
</div>

<!-- Register Popup-->

<!-- Forgot Password Popup-->
<div class="LogINBox_hp main_div" id="forgot_div" style="display:none">
<div class="logbanner_hp">
<img src="/new/pix/login-banner.png" alt="login-banner" title="login-banner" width="306" height="198"/></div>
<a id="popupContactClose1" style="cursor: pointer !important;position: absolute;right: 14px;color: #333;font-weight: bold;font-size: 13px;text-decoration: none;top: 6px;" onClick="cancel_header_form()"> <span style=""> [X]</span></a>
<form id="form-validate-create-new" action=""  method="post">
<div class="check_log">
<div class="newUser_hp">New User? <a id="forgot_reg" onClick="show_main_divs('reg_div')">Signup</a></div>
<h3>Login to your account</h3>
<div class="check_logbox">
 <p>Enter your email id here to receive your password</p>
<!--<label>Enter Email Address</label><br>-->
<input tabindex="1" name="RegistrationFormnew" value="" class="required email checkOut formField signin_input" autocomplete="off" placeholder="Enter Your Email ID"  id="RegistrationFormnew" type="text"/>
<p class="btn_conti" style="margin:7px 0 0 0;">
<input type="button" id="reset_passwordnew" name="reset_passwordnew"  onClick="validpassword();" tabindex="7" value="Send Mail" />
</p>

<div class="for_pass"><a id="forgt_pass_cancel_btn" style="cursor:pointer">Cancel</a></div>


<div style="display:none;color:red;" class="row mts" id="password_error1new"></div>
<div id="DivStateLoading"></div>
</div>
<div class="or_img"><img src="/new/shop/images/img/or_img.png" width="35" height="175" alt="" /></div>
<div class="alt_log">
<label>Login with</label>
<div class="fb_log"><div id="fb-root"></div>
        <script>
          //initializing API
          window.fbAsyncInit = function() {
            FB.init({appId: '119973928016834', status: true, cookie: true,
                     xfbml: true});
          };
          (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
              '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
          }());
        </script>
        <!-- custom login button -->
                <a href="javascript:voide();" onClick="fblogin();return false;">
        
        <span class="button-fb1" style="float:left;"><img src="/new/shop/images/img/fb_log.png" width="150" height="50" alt=""></span></a><br /></a>
                <script>
          //your fb login function
          function fblogin() {
            FB.login(function(response) {
				if (response.authResponse){
								 window.location = "http://www.dogspot.in/facebookconnect/fblogin.php?refUrl=/";
				               //...
			}else{
			 window.location = "http://www.dogspot.in/";
			}
            }, {perms:'email,user_birthday,user_location,publish_stream'});
          }
        </script>
       <input type="hidden" name="uro" id="uro" value="/" /></div>
<div class="g_log">
  
<a href="https://www.google.com/accounts/o8/ud?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=http%3A%2F%2Fwww.dogspot.in%2Fgoogleconnect%2Fglogin.php&openid.realm=http%3A%2F%2Fwww.dogspot.in&openid.ns.ax=http%3A%2F%2Fopenid.net%2Fsrv%2Fax%2F1.0&openid.ax.mode=fetch_request&openid.ax.type.namePerson_first=http%3A%2F%2Faxschema.org%2FnamePerson%2Ffirst&openid.ax.type.namePerson_last=http%3A%2F%2Faxschema.org%2FnamePerson%2Flast&openid.ax.type.contact_email=http%3A%2F%2Faxschema.org%2Fcontact%2Femail&openid.ax.type.birthDate=http%3A%2F%2Faxschema.org%2FbirthDate&openid.ax.type.person_gender=http%3A%2F%2Faxschema.org%2Fperson%2Fgender&openid.ax.type.contact_postalCode_home=http%3A%2F%2Faxschema.org%2Fcontact%2FpostalCode%2Fhome&openid.ax.type.contact_country_home=http%3A%2F%2Faxschema.org%2Fcontact%2Fcountry%2Fhome&openid.ax.type.pref_language=http%3A%2F%2Faxschema.org%2Fpref%2Flanguage&openid.ax.type.pref_timezone=http%3A%2F%2Faxschema.org%2Fpref%2Ftimezone&openid.ax.required=namePerson_first%2CnamePerson_last%2Ccontact_email%2CbirthDate%2Cperson_gender%2Ccontact_postalCode_home%2Ccontact_country_home%2Cpref_language%2Cpref_timezone&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select">
<span class="button-google1" style="float:left"><img src="/new/shop/images/img/g_log.png" width="151" height="50" alt=""></span>
</a>
</div>
</div>

</div>
<div class="LoginBen_hp">
<h3>Login Benefits</h3>
<ul>
<li>Save Your Cart</li>
<li>Get Exclusive Benefits</li>
<li>Quick Re-Order</li>
<li>Save Wish-list</li>
</ul>
</div>
</form>
</div>

<!-- Forgot Password Popup-->

</div>

<link href="/new/common/css/header-style-new5.css" rel="stylesheet" />
<link rel="stylesheet" href="http://www.dogspot.in/new/articles/css/font-awesome.css" />

<script type="text/javascript"> 
 function drop(linkbtn,divid,aid) {

	//	document.getElementById('dropturtle').style.display='block';
	//	document.getElementById('arw_wht').style.display='none';
	//	document.getElementById('arw_grn').style.display='block'; 
		$("#"+divid).css("display","block");
		$("#"+aid).addClass("selected");
				
 }
 


function addcss(aid){
    if(aid=='ds-id-2' || aid=='ds-id-1' || aid=='ds-id-3' || aid=='ds-id-4'  || aid=='ds-id-6'  || aid=='ds-id-7'  || aid=='ds-id-5' || aid=='ds-id-8'){
      $("#"+aid).addClass("ds-state-hover");  
    }else{
	$("#"+aid).addClass("selected");}
}
function removecss(aid){
    if(aid=='ds-id-2' || aid=='ds-id-1' || aid=='ds-id-3' || aid=='ds-id-4'  || aid=='ds-id-6'  || aid=='ds-id-7'  || aid=='ds-id-5' || aid=='ds-id-8'){
         $("#"+aid).removeClass("ds-state-hover");
        }else{
    $("#"+aid).removeClass("selected");}
}

$(document).ready(function(e) { 
if (window.location.href.indexOf("cat") > -1) {
	catsss();
}
 	
$('#foodcat').mouseleave(function(){$('#foodcat').hide();
	})
$('#food').mouseleave(function(){
	$('#foodcat').hide();
	$('#food1').removeClass("selected");
	})
$('#food').mouseenter(function(){$('#food').addClass("selected").addClass("drop");
	})
//-----------------

$('#accessocat').mouseleave(function(){$('#accessocat').hide();
	})
$('#accesso').mouseleave(function(){
	$('#accessocat').hide();
	$('#accesso1').removeClass("selected");
	})
$('#accesso').mouseenter(function(){$('#accesso').addClass("selected").addClass("drop");
	})

$('#dogtoycat').mouseleave(function(){$('#dogtoycat').hide();
	})
$('#dogtoy').mouseleave(function(){
	$('#dogtoycat').hide();
	$('#dogtoy1').removeClass("selected");
	})
$('#dogtoy').mouseenter(function(){$('#dogtoy').addClass("selected").addClass("drop");
	})
	
$('#doggrommingcat').mouseleave(function(){$('#doggrommingcat').hide();
	})
$('#doggromming').mouseleave(function(){
	$('#doggrommingcat').hide();
	$('#doggromming1').removeClass("selected");
	})
$('#doggromming').mouseenter(function(){$('#doggromming').addClass("selected").addClass("drop");
	})
	
$('#collarcat').mouseleave(function(){$('#collarcat').hide();
	})
$('#collar').mouseleave(function(){
	$('#collarcat').hide();
	$('#collar1').removeClass("selected");
	})
$('#collar').mouseenter(function(){$('#collar').addClass("selected").addClass("drop");
	})

$('#cc_homecat').mouseleave(function(){$('#cc_homecat').hide();
	})
$('#cc_home').mouseleave(function(){
	$('#cc_homecat').hide();
	$('#cc_home1').removeClass("selected");
	})
$('#cc_home').mouseenter(function(){$('#cc_home').addClass("selected").addClass("drop");
	})

$('#dt_homecat').mouseleave(function(){$('#dt_homecat').hide();
	})
$('#dt_home').mouseleave(function(){
	$('#dt_homecat').hide();
	$('#dt_home1').removeClass("selected");
	})
$('#dt_home').mouseenter(function(){$('#dt_home').addClass("selected").addClass("drop");
	})
	
$('#hc_homecat').mouseleave(function(){$('#hc_homecat').hide();
	})
$('#hc_home').mouseleave(function(){
	$('#hc_homecat').hide();
	$('#hc_home1').removeClass("selected");
	})
$('#hc_home').mouseenter(function(){$('#hc_home').addClass("selected").addClass("drop");
	})

$('#cat_homecat').mouseleave(function(){$('#cat_homecat').hide();
	})
$('#cat_home').mouseleave(function(){
	$('#cat_homecat').hide();
	$('#cat_home1').removeClass("selected");
	})
$('#cat_home').mouseenter(function(){$('#cat_home').addClass("selected").addClass("drop");
	})

$('#fish_homecat').mouseleave(function(){$('#fish_homecat').hide();
	})
$('#fish_home').mouseleave(function(){
	$('#fish_homecat').hide();
	$('#fish_home1').removeClass("selected");
	})
$('#fish_home').mouseenter(function(){$('#fish_home').addClass("selected").addClass("drop");
	})
//------------------	
$('#c_homecat').mouseleave(function(){$('#c_homecat').hide();
	})
$('#cat_foods').mouseleave(function(){
	$('#c_homecat').hide();
	$('#cat_home_food').removeClass("selected");
	})
$('#cat_foods').mouseenter(function(){$('#cat_foods').addClass("selected").addClass("drop");
	})
		
$('#c_homeGrooming').mouseleave(function(){$('#c_homeGrooming').hide();
	})
$('#cat_Grooming').mouseleave(function(){
	$('#c_homeGrooming').hide();
	$('#cat_home_Grooming').removeClass("selected");
	})
$('#cat_Grooming').mouseenter(function(){$('#cat_Grooming').addClass("selected").addClass("drop");
	})
	
$('#c_homeCleaning').mouseleave(function(){$('#c_homeCleaning').hide();
	})
$('#cat_Cleaning').mouseleave(function(){
	$('#c_homeCleaning').hide();
	$('#cat_home_Cleaning').removeClass("selected");
	})
$('#cat_Cleaning').mouseenter(function(){$('#cat_Cleaning').addClass("selected").addClass("drop");
	})
$('#c_homeCrates').mouseleave(function(){$('#c_homeCrates').hide();
	})
$('#cat_Crates').mouseleave(function(){
	$('#c_homeCrates').hide();
	$('#cat_home_Crates').removeClass("selected");
	})
$('#cat_Health').mouseenter(function(){$('#cat_Health').addClass("selected").addClass("drop");
	})
$('#cat_Health').mouseleave(function(){
	$('#c_homeHealth').hide();
	$('#cat_home_Health').removeClass("selected");
	})
$('#cat_Health').mouseenter(function(){$('#cat_Health').addClass("selected").addClass("drop");
	})	
$('#cat_Collars').mouseenter(function(){$('#cat_Collars').addClass("selected").addClass("drop");
	})
$('#cat_Collars').mouseleave(function(){
	$('#c_homeCollars').hide();
	$('#cat_home_Collars').removeClass("selected");
	})
$('#cat_Collars').mouseenter(function(){$('#cat_Collars').addClass("selected").addClass("drop");
	})			
$('#cat_Toys').mouseenter(function(){$('#cat_Toys').addClass("selected").addClass("drop");
	})
$('#cat_Toys').mouseleave(function(){
	$('#c_homeToys').hide();
	$('#cat_home_Toys').removeClass("selected");
	})
$('#cat_Toys').mouseenter(function(){$('#cat_Toys').addClass("selected").addClass("drop");
	})
$('#cat_Bowls').mouseenter(function(){$('#cat_Bowls').addClass("selected").addClass("drop");
	})
$('#cat_Bowls').mouseleave(function(){
	$('#c_homeBowls').hide();
	$('#cat_home_Bowls').removeClass("selected");
	})
$('#cat_Bowls').mouseenter(function(){$('#cat_Bowls').addClass("selected").addClass("drop");
	})	
$('#cat_Training').mouseenter(function(){$('#cat_Training').addClass("selected").addClass("drop");
	})
$('#cat_Training').mouseleave(function(){
	$('#c_homeTraining').hide();
	$('#cat_home_Training').removeClass("selected");
	})
$('#cat_Training').mouseenter(function(){$('#cat_Training').addClass("selected").addClass("drop");
	})
$('#cat_CA').mouseenter(function(){$('#cat_CA').addClass("selected").addClass("drop");
	})
$('#cat_CA').mouseleave(function(){
	$('#c_homeCA').hide();
	$('#cat_home_CA').removeClass("selected");
	})
$('#cat_CA').mouseenter(function(){$('#cat_CA').addClass("selected").addClass("drop");
	})
	
$('#cat_More').mouseenter(function(){$('#cat_More').addClass("selected").addClass("drop");
	})
$('#cat_More').mouseleave(function(){
	$('#c_homeMore').hide();
	$('#cat_home_More').removeClass("selected");
	})
$('#cat_More').mouseenter(function(){$('#cat_More').addClass("selected").addClass("drop");
	})	
});
function dogsss(){
	$('#li3,#li2,#li7,#li4,#li5,#li6').removeClass("ds-state-active");
	$('#li1').addClass("ds-state-active");
	$('#tabs-1').css('display','block');
	$('#tabs-2,#tabs-3,#tabs-4,#tabs-5').css('display','none');
	}

function catsss(){
	$('#li1,#li3,#li4,#li5,#li6,#li7').removeClass("ds-state-active");
	$('#li2').addClass("ds-state-active");
	$('#tabs-2').css('display','block');
	$('#tabs-1,#tabs-3,#tabs-4,#tabs-5').css('display','none');
	}
function comsss(){
	$('#li1,#li2,#li4,#li5,#li6,#li7').removeClass("ds-state-active");
	$('#li3').addClass("ds-state-active");
	$('#tabs-3').css('display','block');
	$('#tabs-1,#tabs-2,#tabs-4,#tabs-5').css('display','none');
	}
function comspecial(){
	//speacial deal
	$('#li1,#li2,#li3,#li5,#li6,#li7').removeClass("ds-state-active");
	$('#li4').addClass("ds-state-active");
	$('#tabs-4').css('display','block');
	$('#tabs-1,#tabs-2,#tabs-3,#tabs-5').css('display','none');
	}
function comwinter(){
	//winter
	$('#li1,#li2,#li4,#li3,#li6,#li7').removeClass("ds-state-active");
	$('#li5').addClass("ds-state-active");
	$('#tabs-5').css('display','block');
	$('#tabs-1,#tabs-2,#tabs-4,#tabs-3').css('display','none');
	}

function golive(dda){
	window.location.href = "http://dogspot.in/"+dda+"/";
	}
</script>


<div id="tabsss" class="ds-tabs ds-widget ds-widget-content ">
<div class="cont980"><ul role="tablist" class="ds-tabs-nav ds-helper-reset ds-helper-clearfix ds-widget-header">
<!-- dogs-->
<li style="margin-left:1px;" class="ds-state-default ds-corner-top ds-tabs-active ds-state-active" id="li1" onmouseover="addcss('ds-id-1')" onmouseout="removecss('ds-id-1')">
<a id="ds-id-1" tabindex="-1" role="presentation" style="cursor:pointer" class="ds-tabs-anchor" onclick="dogsss();">Dog</a></li>

<!-- dogs-->
<!-- cat-->
<li class="ds-state-default ds-corner-top" id="li2" onmouseover="addcss('ds-id-2')" onmouseout="removecss('ds-id-2')"><a id="ds-id-2" tabindex="-1" role="presentation" class="ds-tabs-anchor" onclick="catsss();">Cat</a></li>
<!-- cat-->
<!-- Special Deal-->
<li class="ds-state-default ds-corner-top" id="li4" onmouseover="addcss('ds-id-4')" onmouseout="removecss('ds-id-4')"><a id="ds-id-4" tabindex="-1" role="presentation" class="ds-tabs-anchor" onclick="comspecial();">Special Deal</a></li>
<!--Special Deal-->
<!--Winter-->
<li class="ds-state-default ds-corner-top" id="li5" onmouseover="addcss('ds-id-5')" onmouseout="removecss('ds-id-5')"><a id="ds-id-5" tabindex="-1" role="presentation" class="ds-tabs-anchor" onclick="comwinter();">Winter</a></li>
<!--Winter-->
<!--Birds-->
<li class="ds-state-default ds-corner-top" id="li6" onmouseover="addcss('ds-id-6')" onmouseout="removecss('ds-id-6')"><a id="ds-id-6" tabindex="-1" role="presentation" class="ds-tabs-anchor" onclick="golive('birds');">Birds</a></li>
<!--Birds-->
<!--Small Pet-->
<li class="ds-state-default ds-corner-top" id="li7" onmouseover="addcss('ds-id-7')" onmouseout="removecss('ds-id-7')"><a id="ds-id-7" tabindex="-1" role="presentation" class="ds-tabs-anchor"  onclick="golive('small-pets');">Small Pet</a></li>
<!--Small Pet-->
<!-- community-->
<li class="ds-state-default ds-corner-top" id="li3" onmouseover="addcss('ds-id-3')" onmouseout="removecss('ds-id-3')"><a id="ds-id-3" tabindex="-1" role="presentation" class="ds-tabs-anchor" onclick="comsss();">Community</a></li>
<!-- community-->

<!--DogShow-->
<li class="ds-state-default ds-corner-top" id="li8" onmouseover="addcss('ds-id-8')" onmouseout="removecss('ds-id-8')"><a id="ds-id-8" tabindex="-1" role="presentation" class="ds-tabs-anchor" onclick="golive('dog-events');">Dog Show</a></li>
<!--Dog Show-->
</ul>
</div>
<!-- dog menu-->
<div aria-hidden="false" role="tabpanel" class="ds-tabs-panel ds-widget-content ds-corner-bottom" aria-labelledby="ds-id-1" id="tabs-1">
<!-- menu goes here-->
 <!-- navigation starts here -->
<div id="navContainer" align="left">
  <div class="navContainer_inside">
    <div class="navContainer_left">
      <div id="nav-wrapper">
        <div class="new_navigation">
          <ul id="navigation" class="fleft">
                 <li  id="food"><a href="javascript:void()" id="food1" onmouseover="drop('food','foodcat','food1')"><span>
				  Food</span></a>
                   <div class="drop-menu" style="display: none;" id="foodcat">
                   <div class="ds_topSubNav">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                    <li><a class="active" href="/dry-dog-food/">Dry Dog Food</a></li>
                    <li><a class="active" href="/dog-biscuits/">Dog Biscuits</a></li>
                    <li><a class="active" href="/canned-dog-food/">Canned Dog Food</a></li>
                    <li><a class="active" href="/dog-dental-treats/">Dog Dental Treats</a></li>
                    <li><a class="active" href="/dog-meaty-treats/">Dog Meaty Treats</a></li>
                    <li><a class="active" href="/veg-treats/">Dog Veg. Treats</a></li>
                    <li><a class="active" href="/training-treats/">Training Treats</a></li>
                    <li><a class="active" href="/bakery-products/">Bakery Products</a></li>
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   <div class="subNav_col">
                   <h4>Shop by Brands</h4>
                <ul class="fleft dscat-main" >
                    <li><a class="active" href="/focus/">Focus</a></li>
                    <li><a class="active" href="/Drools/">Drools</a></li>
                    <li><a class="active" href="/Pedigree/">Pedigree</a></li>
                    <li><a class="active" href="/royal-canin/">Royal Canin</a></li>
                    <li><a class="active" href="/cibau/">Cibau</a></li>
		   <!-- <li class="view_morebrands"><a class="active" href="/treats-food/">View More</a></li>-->	
               	</ul>
                </div>
                <!-- shop by Brands-->
                <!-- View All Products--><div class="view_all_products"><a href="/treats-food/">View All Food Products</a></div><!-- View All Products-->
                </div>
                </div></li>
                <li  id="doggromming"><a href="javascript:void()" id="doggromming1" onmouseover="drop('doggromming','doggrommingcat','doggromming1')"><span>
				  Grooming & Healthcare</span></a>
                   <div class="drop-menu" style="display: none;" id="doggrommingcat">
                   <div class="ds_topSubNav">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                 <li><a class="active" href="/shampoos/">Shampoos</a></li>
                                                        <li><a class="active" href="/conditioners/">Conditioners</a></li>
                                                        <li><a class="active" href="/dental-care/">Dental care</a></li>
                                                        <li><a class="active" href="/ear-care/">Ear Care</a></li>
                                                        <li><a class="active" href="/grooming-tools/">Grooming Tools</a></li>
                                                        <li><a class="active" href="/brushes-combs/">Brushes & Combs</a></li>
                                                        <li><a class="active" href="/bath-accessories/">Bath Accessories</a></li>
                                                        <li><a class="active" href="/shedding-control/">Shedding Control</a></li>
                                                        <li><a class="active" href="/advanced-grooming/">Advanced Grooming</a></li>
                                                        <li><a class="active" href="/dry-bathing/">Dry Bathing</a></li>
                                                        <li><a class="active" href="/deodorizers/">Deodorizers</a></li>
                                                        <li><a class="active" href="/dewormer/">Dewormer</a></li>
                                                         <li><a class="active" href="/supplements/">Supplements</a></li>
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   <div class="subNav_col">
                   <h4>Shop by Brands</h4>
                <ul class="fleft dscat-main" >
                 <li><a class="active" href="/dogspot/">DogSpot</a></li>
                   <li><a class="active" href="/wahl/">Wahl</a></li>
                                <li><a class="active" href="/andis/">Andis</a></li>
                                <li><a class="active" href="/forbis/">Forbis</a></li>

                                        <li><a class="active" href="/simple-solutions/"> Simple Solution</a></li>
                                        
                                         <li><a class="active" href="/himalaya/">Himalaya</a></li>
                                          <li><a class="active" href="/bio-groom/">Bio-groom</a></li>
                                            <li><a class="active" href="/beaphar/">Beaphar</a></li>
                                               
                                                 <li><a class="active" href="/furminator/">Furminator</a></li>
                                              <!-- <li class="view_morebrands"><a class="active" href="/dog-grooming/">View More</a></li>-->
                       
               	</ul>
                </div>
                <!-- shop by Brands-->
                <!-- view all products--><div class="view_all_products"><a href="/dog-grooming/">View All Grooming & Healthcare Products</a></div><!-- view all products-->
                </div>
                </div></li>
                <li  id="collar"><a href="javascript:void()" id="collar1" onmouseover="drop('collar','collarcat','collar1')"><span>
				  Collars & Leashes</span></a>
                   <div class="drop-menu" style="display: none;" id="collarcat">
                   <div class="ds_topSubNav">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                
                <li><a class="active" href="/dog-collars/">Dog Collars</a></li>
                <li><a class="active" href="/dog-leashes/">Dog Leashes</a></li>
                <li><a class="active" href="/dog-harnesses/">Dog Harness</a></li>
                <li><a class="active" href="/retractable-leashes/">Retractable Leashes</a></li>
                <li><a class="active" href="/show-leashes/">Show Leashes</a></li>
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   <div class="subNav_col">
                   <h4>Shop by Brands</h4>
                <ul class="fleft dscat-main" >
                 <li><a class="active" href="/dogspot/">DogSpot</a></li> 
                 <li><a class="active" href="/flexi/">Petmate</a></li>
                 <li><a class="active" href="/flexi/">Flexi</a></li>
                 <li><a class="active" href="/karlie/">Karlie</a></li>
                        
                 <li><a class="active" href="/trixie/">Trixie</a></li>
                  <li><a class="active" href="/sprenger/">Sprenger</a></li>
                 <li><a class="active" href="/ferplast/">Ferplast</a></li>
                <!--<li class="view_morebrands"><a class="active" href="/collars-leashes/">View More</a></li>-->
               	</ul>
                </div>
                <!-- shop by Brands-->
                <!-- view all products--><div class="view_all_products"><a href="/collars-leashes/">View All Collars & Leashes Products</a></div><!-- view all products-->
                </div>
                </div></li>
                <li  id="hc_home"><a href="javascript:void()" id="hc_home1" onmouseover="drop('hc_home','hc_homecat','hc_home1')"><span>
				  Cleaning & Flea, Ticks</span></a>
                   <div class="drop-menu" style="display: none;" id="hc_homecat">
                   <div class="ds_topSubNav ">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/flea-ticks/">Flea & Ticks</a></li>
                <li><a class="active" href="/odour-remover/">Odour Remover</a></li>
                <li><a class="active" href="/pet-hair-remover/">Hair Remover</a></li>
                <li><a class="active" href="/diapers/">Diapers</a></li>
                <li><a class="active" href="/waste-management/">Waste Management</a></li>
                
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   <div class="subNav_col">
                   <h4>Shop by Brands</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/dogspot/">DogSpot</a></li> 
                  <li><a class="active" href="/Bayer/">Bayer</a></li>
                   <li><a class="active" href="/himalaya/">Himalaya</a></li>
                   <!--<li class="view_morebrands"><a class="active" href="/dog-clean-up/">View More</a></li>-->
                   
               	</ul>
                </div>
                <!-- shop by Brands-->
                 <!-- view all products--><div class="view_all_products"><a href="/dog-clean-up/">View All Cleaning & Flea, Ticks Products</a></div><!-- view all products-->
                </div>
                </div></li>
                
                <li  id="cc_home"><a href="javascript:void()" id="cc_home1" onmouseover="drop('cc_home','cc_homecat','cc_home1')"><span>
				  Crates & Cages</span></a>
                   <div class="drop-menu" style="display: none;" id="cc_homecat">
                   <div class="ds_topSubNav">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/crates/">Crates</a></li>
                <li><a class="active" href="/cages/">Cages</a></li>
                <li><a class="active" href="/beds/">Beds</a></li>
                <li><a class="active" href="/dog-house/">Dog House</a></li>
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   <div class="subNav_col">
                   <h4>Shop by Brands</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/dogspot/">DogSpot</a></li>
                 <li><a class="active" href="/super-dog/">Super Dog</a></li>
              <li><a class="active" href="/smart-way/">Smart Way</a></li>
                     <li><a class="active" href="/petmate/">Petmate</a></li>
                     <li><a class="active" href="/all4pets/">All4pets</a></li>
                  <li><a class="active" href="/ferplast/">Ferplast</a></li>
                  <li><a class="active" href="/savic/">Savic</a></li>
                  <!--<li class="view_morebrands"><a class="active" href="/crates-beds/">View More</a></li> -->
               	</ul>
                </div>
                <!-- shop by Brands-->
                <!-- view all products--><div class="view_all_products"><a href="/crates-beds/">View All Crates & Cages Products</a></div><!-- view all products-->
                </div>
                </div></li>
                <li  id="dogtoy"><a href="javascript:void()" id="dogtoy1" onmouseover="drop('dogtoy','dogtoycat','dogtoy1')"><span>
				  Toys</span></a>
                   <div class="drop-menu" style="display: none;" id="dogtoycat">
                   <div class="ds_topSubNav">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                   <li><a class="active" href="/kong-toys/">Kong Toys</a></li>
                  <li><a class="active" href="/soft-toys/">Dog Soft Toys</a></li>
                  <li><a class="active" href="/interactive-dog-toys/">Interactive Dog Toys</a></li>
                  <li><a class="active" href="/ball-toys/">Ball Toys</a></li>
                  <li><a class="active" href="/squeaker-toy/">Squeaker Toy</a></li>
                  <li><a class="active" href="/bone-toys/">Bone Toys</a></li>
                  <li><a class="active" href="/jw-pet-toys/">JW Pet Toys</a></li>
                  
                  <li><a class="active" href="/latex-dog-toys/">Latex Dog Toys</a></li>
                  <li><a class="active" href="/rope-dog-toys/">Rope Dog Toys</a></li>
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   <div class="subNav_col">
                   <h4>Shop by Brands</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/dogspot/">DogSpot</a></li>
                   <li><a class="active" href="/jwpet/">JWPet</a></li>
                   <li><a class="active" href="/knot-rope/">Knot A Rope</a></li>
                  
                   <li><a class="active" href="/all4pets/">All4pets</a></li>
                   <li><a class="active" href="/ourpet_1/">Ourpets</a></li>
                   <li><a class="active" href="/petsport-usa/">Petsport</a></li>
                    <li><a class="active" href="/pet-stage/">Petstages</a></li>
                      <!--<li class="view_morebrands"><a class="active" href="/dog-toy/">View More</a></li> -->
               	</ul>
                </div>
                <!-- shop by Brands-->
                   <!-- view all products--><div class="view_all_products"><a href="/dog-toy/">View All Toys Products</a></div><!-- view all products-->
                </div>
                </div></li>
                 <li  id="accesso"><a href="javascript:void()" id="accesso1" onmouseover="drop('accesso','accessocat','accesso1')"><span>
				  Bowls & Feeders</span></a>
                   <div class="drop-menu" style="display: none;" id="accessocat">
                   <div class="ds_topSubNav">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                    <li><a class="active" href="/feeders/">Feeders</a></li>
                    
                    <li><a class="active" href="/tip-bowls/">Tip Bowls</a></li>
                    <li><a class="active" href="/non-tip-bowls/">Non Tip Bowls</a></li>
                    <li><a class="active" href="/adjustable-bowl/">Adjustable Bowl Stand</a></li>
                    <li><a class="active" href="/clamp-bowls/">Clamp Bowl</a></li>
                    <li><a class="active" href="/slow-feeding-bowl/">Slow Feeding Bowl</a></li>
					<li><a class="active" href="/puppy-feeders/">Puppy Feeder</a></li>
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   <div class="subNav_col">
                   <h4>Shop by Brands</h4>
                <ul class="fleft dscat-main" >
                   <li><a class="active" href="/savic/">Savic</a></li>      
               <li><a class="active" href="/all4pets/">All4Pets</a></li>      
                   <li><a class="active" href="/durapet/">Durapet</a></li>
                   <li><a class="active" href="/dogspot/">DogSpot</a></li>
 <!--<li class="view_morebrands"><a class="active" href="/dog-bowls/">View More</a></li> -->
               	</ul>
                </div>
                <!-- shop by Brands-->
                   <!-- view all products--><div class="view_all_products"><a href="/dog-bowls/">View All Bowls & Feeders Products</a></div><!-- view all products-->
                </div>
                </div></li>
                 
           <li  id="cat_home"><a href="javascript:void()" id="cat_home1" onmouseover="drop('cat_home','cat_homecat','cat_home1')"><span>
				  Training</span></a>
                   <div class="drop-menu" style="display: none;" id="cat_homecat">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
               
                <li><a class="active" href="/training-leashes/">Leashes</a></li>
<li><a class="active" href="/training-pads/">Pads</a></li>		
<li><a class="active" href="/training-aids/">Aids</a></li>
                <li><a class="active" href="/barking-control/">Control</a></li>
              
                 <li><a class="active" href="/education/">Education</a></li>
                 <li><a class="active" href="/training-products/">Products</a></li>
                 <li><a class="active" href="/starter-kit/">Starter Kit</a></li>
<!--<li class="view_morebrands"><a class="active" href="/dog-training-behavior/">View More</a></li> -->
               	</ul>
                </div>
                  <!-- view all products--><div class="view_all_products"><a href="/dog-training-behavior/">View All Training Products</a></div><!-- view all products-->
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   
                <!-- shop by Brands-->
                
                </div>
                </div></li>
                 <li onmouseover="addcss('aqx')" onmouseout="removecss('aqx')"><a href="/dog-store/"  id="aqx">
				 <span>Dog Store</span></a></li>
           <li  id="fish_home"><a href="javascript:void()" id="fish_home1" onmouseover="drop('fish_home','fish_homecat','fish_home1')"><span>
				  More</span></a>
                   <div class="drop-menu" style="display: none;" id="fish_homecat">
                   <div class="ds_subMenuSmall ds_mlMore ">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/clothes/">Clothes</a></li>
              	<li><a class="active" href="/utility/">Utility</a></li>
                <li><a class="active" href="/cooling-dog-coat/">Cooling Dog Coats</a></li>
                <li><a class="active" href="/milk-replacers/">Milk Replacer</a></li> 
                <li><a class="active" href="/new-born/">New Born</a></li>
                <li><a class="active" href="/pet-tag/">Pet Tag</a></li>
                <li><a class="active" href="/muzzles/">Muzzles</a></li>
                <li><a class="active" href="/pet-food/">Pet Food</a></li>
                <li><a class="active" href="/bandana/">Bandana</a></li> 
                 <li><a class="active" href="/gift-pack/">Gift Pack</a></li>
                  <li><a class="active" href="/birds/">Birds </a></li>
                   <li><a class="active" href="/small-pets/">Small Pets</a></li>
                 <li><a class="active" href="/pet-lovers-gallery/">Pet Lover's Gallery</a></li>
                 <li><a class="active" href="http://www.fishspot.in/">FishSpot</a></li>
               	</ul>
                </div>
                <!-- shop by cat-->
                 <!-- shop by Brands-->
                   
                <!-- shop by Brands-->
                
                </div>
                </div></li>
         
          
          <!--  <li onmouseover="addcss('aq')" onmouseout="removecss('aq')"><a href="/sales/"  id="aq">
				 <span>Sale</span></a></li>-->
            
          </ul>
        </div>
      </div>
    </div>
    
  </div>
</div>
<!-- navigation ends here-->

<!-- menu goes here-->
</div>
<!-- dog menu section end-->
<!-- cat menu section start-->
<div aria-hidden="true" style="display: none;" role="tabpanel" class="ds-tabs-panel ds-widget-content ds-corner-bottom" aria-labelledby="ds-id-2" id="tabs-2">
 <!-- navigation starts here -->
<div id="navContainer" align="left">
  <div class="navContainer_inside">
    <div class="navContainer_left">
      <div id="nav-wrapper">
        <div class="new_navigation">
          <ul id="navigation" class="fleft">
           <li  id="cat_foods"><a href="javascript:void()" id="cat_home_food" onmouseover="drop('cat_foods','c_homecat','cat_home_food')"><span>
				  Foods & Treats</span></a>
                   <div class="drop-menu" style="display: none;" id="c_homecat">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/dry-cat-food/">Dry Cat Food</a></li>
				<li><a class="active" href="/wet-cat-food/">Wet Cat Food</a></li>

<!--<li class="view_morebrands"><a class="active" href="/cat-food-and-treats/">View More</a></li> -->
              
               	</ul>
                </div>
                <!-- view all products--><div class="view_all_products"><a href="/cat-food-and-treats/">View All Products</a></div><!-- view all products-->
               </div>
                </div></li>
            
            <li  id="cat_Grooming"><a href="javascript:void()" id="cat_home_Grooming" onmouseover="drop('cat_Grooming','c_homeGrooming','cat_home_Grooming')"><span>
				  Grooming</span></a>
                   <div class="drop-menu" style="display: none;" id="c_homeGrooming">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/cat-shampoos-and-conditioners/">Shampoos & Conditioners</a></li>
				<li><a class="active" href="/cat-shedding-control/">Shedding Control</a></li>
                
                <li><a class="active" href="/cat-eye-care/">Eye Care</a></li>
                <li><a class="active" href="/cat-bath-accessories/">Bath Accessories</a></li>
                <li><a class="active" href="/cat-grooming-tools/">Grooming Tools</a></li>
                <li><a class="active" href="/cat-dry-bathing/">Dry Bathing</a></li>
                <li><a class="active" href="/cat-brushes-and-combs/">Brushes & Combs</a></li>
              <!--<li class="view_morebrands"><a class="active" href="/cat-grooming/">View More</a></li> -->
               	</ul>
                </div>
                 <!-- view all products--><div class="view_all_products"><a href="/cat-grooming/">View All Products</a></div><!-- view all products-->
                <!-- shop by cat-->
               </div>
                </div></li>     
            <li  id="cat_Cleaning"><a href="javascript:void()" id="cat_home_Cleaning" onmouseover="drop('cat_Cleaning','c_homeCleaning','cat_home_Cleaning')"><span>
				  Cleaning</span></a>
                   <div class="drop-menu" style="display: none;" id="c_homeCleaning">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                
				<li><a class="active" href="/cat-stain-remover/">Stain Remover</a></li>
                
                <li><a class="active" href="/cat-odor-remover/">Odor Remover</a></li>
                <li><a class="active" href="/cat-fleas-and-ticks/">Fleas And Ticks</a></li>
               <!--  <li class="view_morebrands"><a class="active" href="/cat-cleaning/">View More</a></li> -->
              
               	</ul>
                </div>
                <!-- view all products--><div class="view_all_products"><a href="/cat-cleaning/">View All Cleaning Products</a></div><!-- view all products-->
                <!-- shop by cat-->
               </div>
                </div></li> 
             <li  id="cat_Crates"><a href="javascript:void()" id="cat_home_Crates" onmouseover="drop('cat_Crates','c_homeCrates','cat_home_Crates')"><span>
				  Crates & Cages</span></a>
                   <div class="drop-menu" style="display: none;" id="c_homeCrates">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/cat-crates/">Cat Crates</a></li>
				<li><a class="active" href="/cat-cages/">Cat Cages</a></li>
<!--<li class="view_morebrands"><a class="active" href="/cat-crates-and-cages/">View More</a></li>       -->         
</ul>
                </div>
                
                <!-- view all products--><div class="view_all_products"><a href="/cat-crates-and-cages/">View All Products</a></div><!-- view all products-->
                <!-- shop by cat-->
               </div>
                </div></li> 
           <li  id="cat_Health"><a href="javascript:void()" id="cat_home_Health" onmouseover="drop('cat_Health','c_homeHealth','cat_home_Health')"><span>
				  Healthcare</span></a>
                   <div class="drop-menu" style="display: none;" id="c_homeHealth">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/cat-dewormer/">Cat Dewormer</a></li>
				<li><a class="active" href="/cat-supplements/">Supplements</a></li>
<!--<li class="view_morebrands"><a class="active" href="/cat-health-and-Care/">View More</a></li> -->                   
</ul>
                </div>

                <!-- view all products--><div class="view_all_products"><a href="/cat-health-and-Care/">View All Products</a></div><!-- view all products-->
                <!-- shop by cat-->
               </div>
                </div></li> 
              <li  id="cat_Collars"><a href="javascript:void()" id="cat_home_Collars" onmouseover="drop('cat_Collars','c_homeCollars','cat_home_Collars')"><span>
				  Litter</span></a>
                   <div class="drop-menu" style="display: none;" id="c_homeCollars">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
<li><a class="active" href="/cat-litter/">Cat Litter</a></li>
                            <li><a class="active" href="/cat-litter-boxes/">Cat Litter Box</a></li>
                             <li><a class="active" href="/cat-litter-accessories/">Litter Accessories</a></li>
                
<!--<li class="view_morebrands"><a class="active" href="/cat-collars/">View More</a></li> -->               
</ul>
                </div>
                <!-- shop by cat-->
                 <!-- view all products--><div class="view_all_products"><a href="/cat-collars/">View All Collars Products</a></div><!-- view all products-->
               </div>
                </div></li>
             <li  id="cat_Toys"><a href="javascript:void()" id="cat_home_Toys" onmouseover="drop('cat_Toys','c_homeToys','cat_home_Toys')"><span>
				  Toys</span></a>
                   <div class="drop-menu" style="display: none;" id="c_homeToys">
                   <div class="ds_subMenuSmall">
                   <!-- shop by cat-->
                   <div class="subNav_col">
                   <h4>Shop by Category</h4>
                <ul class="fleft dscat-main" >
                <li><a class="active" href="/cat-ball-toys/">Cat Ball Toys</a></li>
				<li><a class="active" href="/cat-interactive-toys/">Interactive Cat Toys</a></li>
                <li><a class="active" href="/cat-plush-toys/">Cat Plush Toys</a></li>
                
                <li><a class="active" href="/cat-teasers-and-wands/">Teasers & Wands</a></li>
                <li><a class="active" href="/cat-scratcher-toys/">Scratcher Toys</a></li>

<!--<li class="view_morebrands"><a class="active" href="/cat-toys/">View More</a></li>-->
                </ul>
                </div>
                <!-- shop by cat-->
                <!-- view all products--><div class="view_all_products"><a href="/cat-toys/">View All Toys Products</a></div><!-- view all products-->
               </div>
                </div></li>        
            <li  id="cat_Bowls"><a href="javascript:void()" id="cat_home_Bowls" onmouseover="drop('cat_Bowls','c_homeBowls','cat_home_Bowls')"><span>
                              Bowls & Feeders</span></a>
                               <div class="drop-menu" style="display: none;" id="c_homeBowls">
                               <div class="ds_subMenuSmall">
                               <!-- shop by cat-->
                               <div class="subNav_col">
                               <h4>Shop by Category</h4>
                            <ul class="fleft dscat-main" >
                            <li><a class="active" href="/cat-bowls/">Bowls</a></li>
                            <li><a class="active" href="/cat-feeders/">Feeders</a></li>
<!--<li class="view_morebrands"><a class="active" href="/cat-bowls-and-feeders/">View More</a></li>-->
                            </ul>
                            </div>
                                <!-- view all products--><div class="view_all_products"><a href="/cat-bowls-and-feeders/">View All Products</a></div><!-- view all products-->
                            <!-- shop by cat-->
                           </div>
                            </div></li>
           <li  id="cat_Training"><a href="javascript:void()" id="cat_home_Training" onmouseover="drop('cat_Training','c_homeTraining','cat_home_Training')"><span>
                              Training</span></a>
                               <div class="drop-menu" style="display: none;" id="c_homeTraining">
                               <div class="ds_subMenuSmall">
                               <!-- shop by cat-->
                               <div class="subNav_col">
                               <h4>Shop by Category</h4>
                            <ul class="fleft dscat-main" >
                            <li><a class="active" href="/cat-training-aids/">Training Aids</a></li>
                            <li><a class="active" href="/cat-repellents/">Repellents</a></li>

<!--<li class="view_morebrands"><a class="active" href="/cat-training/">View More</a></li> -->                           
</ul>
                            </div>
                            <!-- shop by cat-->
                              <!-- view all products--><div class="view_all_products"><a href="/cat-training/">View All Products</a></div><!-- view all products-->
                           </div>
                            </div></li> 
			<li  id="cat_CA"><a href="javascript:void()" id="cat_home_CA" onmouseover="drop('cat_CA','c_homeCA','cat_home_CA')"><span>
                              Accessories</span></a>
                               <div class="drop-menu" style="display: none;" id="c_homeCA">
                               <div class="ds_subMenuSmall">
                               <!-- shop by cat-->
                               <div class="subNav_col">
                               <h4>Shop by Category</h4>
                            <ul class="fleft dscat-main" >
                            <li><a class="active" href="/cat-beds/">Beds</a></li>
                            <li><a class="active" href="/cat-furniture/">Cat Furniture</a></li>
                            <li><a class="active" href="/cat-scratchers/">Cat Scratchers</a></li>
                            <li><a class="active" href="/cat-doors/">Cat Doors</a></li>
                            
<!--<li class="view_morebrands"><a class="active" href="/cat-bath-accessories/">View More</a></li> -->
                            </ul>
                            </div>
                            <!-- shop by cat-->
                              <!-- view all products--><div class="view_all_products"><a href="/cat-bath-accessories/">View All Products</a></div><!-- view all products-->
                           </div>
                            </div></li> 
            <li  id="cat_More"><a href="javascript:void()" id="cat_home_More" onmouseover="drop('cat_More','c_homeMore','cat_home_More')"><span>
                             More</span></a>
                               <div class="drop-menu" style="display: none;" id="c_homeMore">
                               <div class="ds_subMenuSmall ds_mlMore">
                               <!-- shop by cat-->
                               <div class="subNav_col">
                               <h4>Shop by Category</h4>
                            <ul class="fleft dscat-main" >
                            <li><a class="active" href="/cat-collars/">Collars</a></li>
				<li><a class="active" href="/cat-harness/">Cat Harness</a></li>
                <li><a class="active" href="/cat-pet-tags/">Cat Pet Tags</a></li>
                              <li><a class="active" href="/cat-magazines/">Magazines</a></li>
                              
                               
                            
                            </ul>
                            </div>
                            <!-- shop by cat-->
                            
                           </div>
                            </div></li>
          </ul>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
<!-- cat menu section end-->
<!-- Special Deal menu section start-->
<div aria-hidden="true" style="display: none;" role="tabpanel" class="ds-tabs-panel ds-widget-content ds-corner-bottom" aria-labelledby="ds-id-4" id="tabs-4">
<!-- navigation starts here -->
<div id="navContainer" align="left">
  <div class="navContainer_inside">
    <div class="navContainer_left">
      <div id="nav-wrapper">
        <div class="new_navigation">
          <ul id="navigation" class="fleft">
                 
            <li onmouseover="addcss('aq114')" onmouseout="removecss('aq114')"><a href="/dry-dog-food/"  id="aq114">
				 <span>Dry Food</span></a></li>
            <li onmouseover="addcss('aq414')" onmouseout="removecss('aq414')"><a href="/canned-dog-food/"  id="aq414">
				 <span>Canned Food</span></a></li>
            <li onmouseover="addcss('aq814')" onmouseout="removecss('aq814')"><a href="/dog-dental-treats/"  id="aq814">
				 <span>Dental Treat</span></a></li>
                <li onmouseover="addcss('aq614')" onmouseout="removecss('aq614')"><a href="/dog-meaty-treats/"  id="aq614">
				 <span>Meaty Treat</span></a></li>
            <li onmouseover="addcss('aq914')" onmouseout="removecss('aq914')"><a href="/veg-treats/"  id="aq914">
				 <span>Veg Treat</span></a></li>
            <li onmouseover="addcss('aq514')" onmouseout="removecss('aq514')"><a href="/training-treats/"  id="aq514">
				 <span>Training Treat</span></a></li>   
          </ul>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
<!-- Special Deal menu section End-->

<!--  Winter  Deal menu section start-->
<div aria-hidden="true" style="display: none;" role="tabpanel" class="ds-tabs-panel ds-widget-content ds-corner-bottom" aria-labelledby="ds-id-5" id="tabs-5">
<!-- navigation starts here -->
<div id="navContainer" align="left">
  <div class="navContainer_inside">
    <div class="navContainer_left">
      <div id="nav-wrapper">
        <div class="new_navigation">
          <ul id="navigation" class="fleft">
                 
            <li onmouseover="addcss('aq115')" onmouseout="removecss('aq115')"><a href="http://www.dogspot.in/clothes/?filter=filter&category_id=27&item_brand=undefined&s_price=Rs330-Rs1975&show1=1&record=&type_breed11=9-605|"  id="aq115">
				 <span>Winter T-Shirt</span></a></li>
            <li onmouseover="addcss('aq415')" onmouseout="removecss('aq415')"><a href="http://www.dogspot.in/clothes/?filter=filter&category_id=27&item_brand=undefined&s_price=Rs330-Rs1975&show1=1&record=&type_breed11=9-607|"  id="aq415">
				 <span>Winter Coat</span></a></li>
            <li onmouseover="addcss('aq815')" onmouseout="removecss('aq815')"><a href="http://www.dogspot.in/clothes/?filter=filter&category_id=27&item_brand=undefined&s_price=Rs330-Rs1975&show1=1&record=&type_breed11=9-608|"  id="aq815">
				 <span>Sweater</span></a></li>
            <li onmouseover="addcss('aq615')" onmouseout="removecss('aq615')"><a href="http://www.dogspot.in/clothes/?filter=filter&category_id=27&item_brand=undefined&s_price=Rs330-Rs1975&show1=1&record=&type_breed11=9-609|"  id="aq615">
				 <span>T-Shirt</span></a></li>
            <li onmouseover="addcss('aq915')" onmouseout="removecss('aq915')"><a href="/beds/"  id="aq915">
				 <span>Bed</span></a></li>   
          </ul>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
<!-- Winter menu section End-->

<!-- Community menu section start-->
<div aria-hidden="true" style="display: none;" role="tabpanel" class="ds-tabs-panel ds-widget-content ds-corner-bottom" aria-labelledby="ds-id-3" id="tabs-3">
<!-- navigation starts here -->
<div id="navContainer" align="left">
  <div class="navContainer_inside">
    <div class="navContainer_left">
      <div id="nav-wrapper">
        <div class="new_navigation">
          <ul id="navigation" class="fleft">
                 
            <li onmouseover="addcss('aq11')" onmouseout="removecss('aq11')"><a href="/dog-breeds/"  id="aq11">
				 <span>Dog Breeds</span></a></li>
            <li onmouseover="addcss('aq41')" onmouseout="removecss('aq41')"><a href="/dog-blog/"  id="aq41">
				 <span>Articles</span></a></li>
            <li onmouseover="addcss('aq81')" onmouseout="removecss('aq81')"><a href="/qna/"  id="aq81">
				 <span>Q&A </span></a></li>
            <li onmouseover="addcss('aq61')" onmouseout="removecss('aq61')"><a href="/dog-listing/"  id="aq61">
				 <span>Classifieds</span></a></li>
            <li onmouseover="addcss('aq91')" onmouseout="removecss('aq91')"><a href="/dog-events/"  id="aq91">
				 <span>Dog Show </span></a></li>
            <li onmouseover="addcss('aq51')" onmouseout="removecss('aq51')"><a href="/wag_club/"  id="aq51">
				 <span>Wag Club </span></a></li>                     
            <li onmouseover="addcss('aq31')" onmouseout="removecss('aq31')"><a href="/adoption/"  id="aq31">
				 <span>Adoption</span></a></li>
            <li onmouseover="addcss('aq21')" onmouseout="removecss('aq21')"><a href="/wagfund/"  id="aq21">
				 <span>Donation</span></a></li>
            <li onmouseover="addcss('aq71')" onmouseout="removecss('aq71')"><a href="/wagtag-info/"  id="aq71">
				 <span>Wag Tag</span></a></li>
            
          </ul>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
<!-- Community menu section end-->

</div>



	<!-- header -->
    <!-- ViralMint Javascript -->
<!-- ViralMint Javascript -->

<input type="hidden" name="userfor" id="userfor" value="shailesh" />
<div class="cont980">
<div class="christmas_bell_left"><img src="/new/pix/crismats-bells.png" width="88" height="84"/></div>

<div style="margin:auto; width:972px; "> 
    
    <div id="photoslider" style=" margin-top:13px; float:left">
    <div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
    
    <!--<a href="http://www.dogspot.in/sales/"><img src="/new/pix/sale_13_12_2014.jpg" title="Sale" width="628" height="432" alt="Sale"  border="0" /></a>-->
<a href="http://www.dogspot.in/dry-dog-food/?utm_source=dogspot&utm_medium=banner&utm_campaign=sale26.12.14">
<img src="/new/pix/festive-sale-home-page-bannre.jpg" alt="Sale" width="628" height="432"   border="0" /></a>
<a href=""><img src="/new/pix/iiptf-home-page-banner.jpg" alt="iiptf" width="628" height="432" border="0" /></a>    
<!--<a href="http://www.dogspot.in/wagtag-info/"><img src="/new/pix/wag-tag-home.jpg" title="WAG TAG" width="628" height="432" alt="WAG TAG"  border="0" /></a>--> 
<!--<a href="http://www.dogspot.in/adoption/"><img src="/new/pix/adoption-banner-home.jpg" width="628" height="432" title="Adoption" alt="Adoption"  border="0" /></a>--> 
<a href="http://www.dogspot.in/dogspot/">
<img src="/new/pix/pet-products.jpg" width="628" height="432" alt="Dogspot Products"   border="0" /></a>

<!--<a href="http://www.dogspot.in/royal-canin/"><img src="/new/pix/royal-canin-home.jpg" width="628" height="432" title="Royal Canin" alt="Royal Canin"  border="0" /></a>-->
 <a href="http://www.dogspot.in/canned-dog-food/?filter=filter&category_id=14&item_brand=undefined79-&s_price=Rs110-Rs275&show1=1">
 <img src="/new/pix/drools.jpg" width="628" height="432" alt="Drools Free Banner"   border="0" /> </a> 
<a href="http://www.dogspot.in/focus/"><img src="/new/pix/focus-banner.jpg" width="628" height="432" alt="Focus"  border="0" /> </a> 
<a href="http://www.dogspot.in/subscribe.php"><img src="/new/pix/schedule-delivery-home.jpg" width="628" height="432" alt="Schedule Delivery"  border="0" /></a>
 
 

 
 
    </div>
  </div></div>
   
   <div class="rytSide-banner fr">
   <div class="sideBannerBox fl"> <a href="http://www.dogspot.in/photos/doon-valley-kennel-club1419414984/" style="margin:0px; padding:0px;"><img src="/new/pix/dehradun-dogshow.jpg" width="302" height="208" title="DogShow"  alt="DogShow" /></a>
				
   </div>
   <div class="rytBorBottom fl"></div>
   <div class="sideBannerBox fl"><a onclick="abcd();" style="cursor:pointer"><img src="/new/pix/product-recommedation-shop.jpg" width="302" height="208" alt="Sale" title="Sale" /></a>
<!--<a href="http://www.dogspot.in/sales/" style="margin:0px; padding:0px;"><img src="/new/pix/christmas-side-banner.jpg" width="302" height="208" title="DogShow"  alt="DogShow" /></a>--></div>   
   </div>

        <!-- shop tabs -->
               <div class="cb"></div>
     <!-- shop tabs -->
    </div>
<div class="christmas_bell_right"><img src="/new/pix/crismats-bells.png"/></div>
    <div id="login-box" class="login-popup" style="padding:0; box-shadow:none; border:0px solid #fff;background: rgba(255, 255, 255, 0);">
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/new/css/recommend.css" type="text/css" rel="stylesheet" />
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script type="text/javascript">
function callmainpage(){
	var dogvalue=$("#radvalue").val();
	if(dogvalue==''){
		document.getElementById("errormsg").style.display="inline";
	}else{
	var res = dogvalue.split("@@");
		if(res[0]=='11' || res[0]=='636' || res[0]=='37' || res[0]=='60' || res[0]=='65' || res[0]=='67' || res[0]=='86' || res[0]=='108' || 
	res[0]=='106' || res[0]=='112'){
			if(showlifediv=="0"){
			window.location="/recommend/?item_breed="+res[0]+"&life_stage="+res[1]+"&dog_name="+res[3];
			}
			if(showlifediv=="1"){
			var doglifestage=$("#dogage2").val();
			window.location="/recommend/?item_breed="+res[0]+"&life_stage="+doglifestage+"&dog_name="+res[3];
			}
		}else{
			if(showlifediv=="0"){
			window.location="/recommend/?item_breeds=g&breed_type="+res[4]+"&item_breed="+res[0]+"&life_stage="+res[1]+"&dog_name="+res[3];
			}
			if(showlifediv=="1"){
			var doglifestage=$("#dogage2").val();
			window.location="/recommend/?item_breeds=g&breed_type="+res[4]+"&item_breed="+res[0]+"&life_stage="+doglifestage+"&dog_name="+res[3];
			}
		}
	}
}
function callajax3(){
	ShaAjaxJquary("/Recommend-Product/not_logged_in.php", "#ajaxdiv1", '', '', 'GET', '#ajaximg1', '<img src="/images/indicator.gif" />','REP');
//window.location="/Recommend-Product/not_logged_in.php?abcd='abcd'";
}

function getvalue(value1){
document.getElementById("radvalue").value=value1;
	//var dogvalue=$("#radvalue").val();
	//alert("abcd");
	var res = value1.split("@@");
	//alert(res[1]);
	if(res[1]=='0'){
		//alert(res[1]);
	document.getElementById("labelage").style.display="block";
	document.getElementById("dogage2").style.display="block";
	showlifediv="1";
	}else{
	document.getElementById("labelage").style.display="none";
	document.getElementById("dogage2").style.display="none";
	showlifediv="0";
	}
}
</script>
<style>
.enter_agePR {
float: left;
margin-left: 22px;
width:100%;
}
.enter_agePR label {
float: left;
line-height: 32px;
margin-right: 10px;
}
.enter_agePR select{width: 162px;
border-radius: 5px;
float: left;
border: 1px solid #bfbfbf;
margin-right: 10px;
padding: 5px;}
</style>
</head>

<body>
<div id="ajaximg1" style="position:absolute; top:50%; left:50%;"></div>
<div class="pop_pr_box" id="ajaxdiv1">
<div class="hlp_box1 pop_box1">
<div class="hlp_head">
<h2>For whom are we shopping today ?</h2>
<span class="cls_hlp">
<a href="" class="close"  >
<img src="/Recommend-Product/images/btn5.png" width="18" height="18" alt="" /></a>
</span>
</div>
<div class="hlp_detail">
<ul class="box_dog_breed_pr">
<li class="breed_li">
<div class="breed_select">
<h3>Pug</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-pug.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('108@@0@@108-2@@Test dog  @@2')" />
<label>Test dog  </label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Afghan Hound</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-Afghan.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('3@@0@@3-5@@Test@@5')" />
<label>Test</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Afghan Hound</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-Afghan.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('3@@0@@3-5@@hello@@5')" />
<label>hello</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Beagle</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-beagle.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('11@@0@@11-3@@Shailesh Singh@@3')" />
<label>Shailesh Singh</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Shih Tzu</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-shih-tzu.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('121@@0@@121-2@@fdsfsd@@2')" />
<label>fdsfsd</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Pug</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-pug.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('108@@0@@108-2@@greger@@2')" />
<label>greger</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Beagle</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-beagle.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('11@@0@@11-3@@Test 111@@3')" />
<label>Test 111</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Bull Mastiff</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-Bull-Mastiff.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('27@@0@@27-6@@testing@@6')" />
<label>testing</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Bull Mastiff</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-Bull-Mastiff.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('27@@0@@27-6@@Test dog  @@6')" />
<label>Test dog  </label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Bull Mastiff</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-Bull-Mastiff.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('27@@0@@27-6@@ffgfg@@6')" />
<label>ffgfg</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Beagle</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-beagle.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('11@@0@@11-3@@temp   2@@3')" />
<label>temp   2</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Bull Mastiff</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-Bull-Mastiff.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('27@@0@@27-6@@gu@@6')" />
<label>gu</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Lhasa Apso</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-lhasa.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('89@@5@@89-2@@Sweety@@2')" />
<label>Sweety</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>German Shepherd Dog (Alsatian)</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-german.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('60@@6@@60-5@@Xylo@@5')" />
<label>Xylo</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Cocker Spaniel (English)</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-eng-cock.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('37@@2@@37-3@@ghjv@@3')" />
<label>ghjv</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Cocker Spaniel (English)</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-eng-cock.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('37@@2@@37-3@@ghjv@@3')" />
<label>ghjv</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Golden Retriever</h3>
    <img src="/new/breed_engine/images/dog_images/165-145-golden.jpg" width="165" height="145" alt="" /> 
    </div>
    <div class="name_of_dog">
<input name="chkbox" type="radio" value="" id="chkbox" 
onclick="getvalue('65@@2@@65-5@@ddoo@@5')" />
<label>ddoo</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Airedale Terrier</h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label>Test   dog 1</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Affen Pinscher</h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label>ddddf</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3></h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label></label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Affen Pinscher</h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label>vcb</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Airedale Terrier</h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label>hgj</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Affen Pinscher</h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label>test dog</label>
</div>
<li class="breed_li">
<div class="breed_select">
<h3>Affen Pinscher</h3>
<img src="/Recommend-Product/images/com_soon.jpg" width="165" height="145" alt="" /> 
</div>
<div class="name_of_dog">
<label>My New Dog</label>
</div>
<input type="hidden" name="radvalue" id="radvalue" />

</ul>
<div class="enter_agePR" id="agediv">
<label id="labelage" style="display:none">Enter your dog age</label>
<select name="" id="dogage2" style="display:none">
<option value="1">Upto 2 months</option>
<option value="2">2 to 24 months</option>
<option value="5">2 to 7 years</option>
<option value="6">More than 7 years</option>
</select>
</div>


<div>

<div class="shop_more">

<a onclick="callajax3()" style="cursor:pointer">Shopping for someone else ?</a>
</div>
<div class="btn_box1">
<label id="errormsg" style="display:none; color:#F00; font-weight:200;text-decoration: none;float: left;margin-bottom: 5px;width: 100%;">Please select a dog</label>
<img src="/Recommend-Product/images/btn3.png" width="192" height="37" alt="" onclick="callmainpage()" style="cursor:pointer" />
</div>
<div class="btn_box2">
<p><strong>Hi!</strong> We are still adding more breeds to our product recommendation engine & will soon have a pleothra of 
products handpicked just for your dog.</p>
<div class="txt_pro_rec">
<p>We have a wide variety of products available in our shopping section 
but if you are confused just give a call to our pet expert.</p>
<div class="ryt_txt_pr">
<img src="/Recommend-Product/images/icon1.png" width="16" height="16" alt="" />
 <span>+91-9212196633</span>
</div>
</div>
</div>
</div>
</div>
 </div>
 </div>
</body>
</html>
</div>
    
	<a href="#login-box" class="login-window"></a>        
              


   <div class="cb"></div>

        <span class="cl"></span>
  
   <div class="cb"></div>
   <!-- tabs -->
      <!-- first half two column -->
   <div class="vs20"></div>
   <!-- dog gallery -->
   
   <!--Products Tab-->
<div class="cont980">
<div class="product_tab_hp fl">
<div class="product_head_hp fl">
<ul>
<li class="fl" id="usale" onclick="showhidediv_slider('recent','0','5','sale_a')" style="cursor:pointer"><a id="sale_a" class="active_hp prod_tabs">Featured </a></li>
<li class="fl" id="ulike" onclick="showhidediv_slider('featured','0','5','feat_a')" style="cursor:pointer"><a id="feat_a" class="prod_tabs">You May Like</a></li>
<li class="fl" id="newarr" onclick="showhidediv_slider('new','0','5','newarr_a')" style="cursor:pointer"><a id="newarr_a" class="prod_tabs" >New Arrivals</a></li>
<li class="fl" id="odrbuy" onclick="showhidediv_slider('recent','0','5','odrbuy_a')" style="cursor:pointer"><a id="odrbuy_a" class="prod_tabs" >What Others Are Buying</a></li>
</ul>
</div>

<div class="product_slider_hp fl" id="product_div" >
    
<!------------------------------------------------------------new arrivals------------------------------------------------------------->

<!------------------------------------------------------new arrivals ends here--------------------------------------------------------->

<!-------------------------------------------------------featured products ------------------------------------------------------------>

<!--------------------------------------------------featured products ends here------------------------------------------------------->

<!--------------------------------------------------------recently bought------------------------------------------------------------->
<ul>
<li class="fl prev_hp"><img src="/new/pix/dull-prev.jpg" alt="Previous" title="Previous" width="32" height="32"/></li>
<li style="width:170px;height:230px;" ><a href="/dogspot-puppy-toilet-tray-orange-training-pad/" class="db vs160">
<img src="http://www.dogspot.in/imgthumb/200x161-1419682926673.png" alt="DogSpot Puppy Toilet Tray Orange with Training Pad" border="0"  align="middle" title="DogSpot Puppy Toilet Tray Orange with Training Pad" height="161" width="172"/>
</a><h2 class="vs40"><a href="/dogspot-puppy-toilet-tray-orange-training-pad/" class="vs40" alt="DogSpot Puppy Toilet Tray Orange with Training Pad" title="DogSpot Puppy Toilet Tray Orange with Training Pad" >DogSpot Puppy Toilet Tray Orange with...</a></h2>

<a  class="link vs40"><strong>Rs. 1,243</strong><br />
                        <del>Rs. 1,480</del>
                        </a>

</li>

<li style="width:170px;height:230px;" ><a href="/chicopee-fish-rice-adult-dog-food-15-kg/" class="db vs160">
<img src="http://www.dogspot.in/imgthumb/200x161-1406264373154.jpg" alt="Chicopee Fish And Rice Adult Dog Food - 15 Kg" border="0"  align="middle" title="Chicopee Fish And Rice Adult Dog Food - 15 Kg" height="161" width="172"/>
</a><h2 class="vs40"><a href="/chicopee-fish-rice-adult-dog-food-15-kg/" class="vs40" alt="Chicopee Fish And Rice Adult Dog Food - 15 Kg" title="Chicopee Fish And Rice Adult Dog Food - 15 Kg" >Chicopee Fish And Rice Adult Dog Food - 15...</a></h2>

<a  class="link vs40"><strong>Rs. 5,300</strong><br />
                        <del></del>
                        </a>

</li>

<li style="width:170px;height:230px;" ><a href="/dogspot-winter-dog-tshirt-talkto-paw-blue-size-12/" class="db vs160">
<img src="http://www.dogspot.in/imgthumb/200x161-1419412342831.jpg" alt="DogSpot Winter Dog T-shirt TalkTo The Paw (Blue) Size - 12" border="0"  align="middle" title="DogSpot Winter Dog T-shirt TalkTo The Paw (Blue) Size - 12" height="161" width="172"/>
</a><h2 class="vs40"><a href="/dogspot-winter-dog-tshirt-talkto-paw-blue-size-12/" class="vs40" alt="DogSpot Winter Dog T-shirt TalkTo The Paw (Blue) Size - 12" title="DogSpot Winter Dog T-shirt TalkTo The Paw (Blue) Size - 12" >DogSpot Winter Dog T-shirt TalkTo The Paw...</a></h2>

<a  class="link vs40"><strong>Rs. 460</strong><br />
                        <del></del>
                        </a>

</li>

<li style="width:170px;height:230px;" ><a href="/vitapol-economic-food-hamster-12-kg/" class="db vs160">
<img src="http://www.dogspot.in/imgthumb/200x161-1417031063682.gif" alt="Vitapol Economic Food For Hamster - 1.2 Kg" border="0"  align="middle" title="Vitapol Economic Food For Hamster - 1.2 Kg" height="161" width="172"/>
</a><h2 class="vs40"><a href="/vitapol-economic-food-hamster-12-kg/" class="vs40" alt="Vitapol Economic Food For Hamster - 1.2 Kg" title="Vitapol Economic Food For Hamster - 1.2 Kg" >Vitapol Economic Food For Hamster - 1.2 Kg</a></h2>

<a  class="link vs40"><strong>Rs. 360</strong><br />
                        <del></del>
                        </a>

</li>

<li style="width:170px;height:230px;" ><a href="/dogspot-winter-dog-tshirt-talkto-paw-red-size-12/" class="db vs160">
<img src="http://www.dogspot.in/imgthumb/200x161-1419411779121.jpg" alt="DogSpot Winter Dog T-shirt TalkTo The Paw (Red) Size - 12" border="0"  align="middle" title="DogSpot Winter Dog T-shirt TalkTo The Paw (Red) Size - 12" height="161" width="172"/>
</a><h2 class="vs40"><a href="/dogspot-winter-dog-tshirt-talkto-paw-red-size-12/" class="vs40" alt="DogSpot Winter Dog T-shirt TalkTo The Paw (Red) Size - 12" title="DogSpot Winter Dog T-shirt TalkTo The Paw (Red) Size - 12" >DogSpot Winter Dog T-shirt TalkTo The Paw...</a></h2>

<a  class="link vs40"><strong>Rs. 460</strong><br />
                        <del></del>
                        </a>

</li>

<li class="fr next_hp"><img src="/new/pix/next.jpg" alt="Next" title="Next" onclick="showhidediv_slider_inc('recent','0','5','next')" style="cursor:pointer" width="32" height="32"/></li>
</ul>
<!--------------------------------------------------------recently bought ends here---------------------------------------------------->
<!--------------------------------------------------------recently bought------------------------------------------------------------->
            </div>
          
            </div>
</div>
<input type="hidden" name="cookieval" id="cookieval" value="" />
   <!--Products Tab-->
   
   <!--Category Tab-->
  <div class="cont980">
  <div class="category_box_hp fl">
  <a href="http://www.dogspot.in/dry-dog-food/" style="cursor:pointer">
  <div class="catBox_hp fl">
  <div class="catBox_head_hp fl">Dry Dog Food</div>
  <div class="catBox_img_hp fl"><img src="http://www.dogspot.in/new/pix/Dog-food-1.jpg" alt="Dry dog food" width="315" height="243" title="Dry dog food" border="0" align="middle"></div>
  </div>
  </a>
  <a href="http://www.dogspot.in/dog-grooming/" style="cursor:pointer">
  <div class="catBox_hp fl">
  <div class="catBox_head_hp fl">Grooming Essentials</div>
  <div class="catBox_img_hp fl"><img src="http://www.dogspot.in/new/pix/dog-cat-bath.jpg" alt="Summer Essentials" width="315" height="243" title="Summer Essentials" border="0" align="middle"></div>
  </div>
  </a>
  <a href="http://www.dogspot.in/cat-food-and-treats/" style="cursor:pointer">
  <div class="catBox_hp fl no_margin_hp">
  <div class="catBox_head_hp fl">Cat Food</div>
  <div class="catBox_img_hp fl"><img src="http://www.dogspot.in/new/pix/cat-food-1.jpg" alt="Cat Food" width="315" height="243" title="Cat Food" border="0" align="middle"></div>
  </div>
  </a>
  </div>
  </div> 
   <!--Category Tab-->
   
   <!--Product Banner-->
   <div class="cont980">
   <div class="wagBanner_hp fl">
   <div class="homepage-fancystripe"></div>
<div class="wag_rec_dg1">

<div class="wc_sec">
<div class="wag_indexSec">

<div class="dg_popMost">
<div class="dg_popBox dg_pop2Box"><a href="/dogs/casper-cockerspanielenglishcocker-spaniel-english/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-1228723116.jpg" alt="casper-cockerspanielenglishcocker-spaniel-english" title="casper-cockerspanielenglishcocker-spaniel-english" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>Casper</h3>
<h4>Cocker Spaniel (English)</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/lucy/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-DSC00046.JPG" alt="lucy" title="lucy" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>Lucy</h3>
<h4>Spitz German</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/buddy-lhasa-apso/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-8789_20131002_005156.jpg" alt="buddy-lhasa-apso" title="buddy-lhasa-apso" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>Buddy</h3>
<h4>Lhasa Apso</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/jeadan-labrador-retriever/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-1377864692236.jpg" alt="jeadan-labrador-retriever" title="jeadan-labrador-retriever" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>jeadan</h3>
<h4>Labrador Retriever</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/bis-biss-grand-ind-ch-crekside-stryker-of-vidanes--great-dane/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-1209229388.jpg" alt="bis-biss-grand-ind-ch-crekside-stryker-of-vidanes--great-dane" title="bis-biss-grand-ind-ch-crekside-stryker-of-vidanes--great-dane" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>BIS .BISS. Grand Ind. Ch. CREKSIDE STRYKER OF VIDANE’S</h3>
<h4>Great Dane</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/buddy-labrador-retriever_14/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-1368112144.jpg" alt="buddy-labrador-retriever_14" title="buddy-labrador-retriever_14" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>BUDDY</h3>
<h4>Labrador Retriever</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/flora-stbernard/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-1362843935.jpg" alt="flora-stbernard" title="flora-stbernard" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>Flora</h3>
<h4>St.Bernard</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/ralph/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-20274_photo.JPG" alt="ralph" title="ralph" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>Ralph</h3>
<h4>Golden Retriever</h4>
</div></a>
</div>
<div class="dg_popBox dg_pop2Box"><a href="/dogs/wolfsbecks-hulk-dobermann/">


<div class="dg_popImg">
  <img src="/dogs/images/100-100-1359700105.jpg" alt="wolfsbecks-hulk-dobermann" title="wolfsbecks-hulk-dobermann" height="100" width="100">
  </div>
  
 
<div class="dg_popTxt">
<h3>WOLFSBECKS HULK</h3>
<h4>Dobermann</h4>
</div></a>
</div>
</div>
</div>
<div class="wag_indexText">
<h4>Wag Club</h4>
<h3>Connecting over 6000 dogs and helping them to share their story</h3>
<div class="be_click"><a href="http://www.dogspot.in/wag_club/">Visit Wag club</a></div> 
</div>
</div></div>
<div class="homepage-fancystripe"></div>

   </div>
   <a href="http://www.dogspot.in/dog-breeds/" style="cursor:pointer">
   <div class="wagBanner_hp no_margin_hp fl wagPedia_hp">
   <img src="http://www.dogspot.in/new/pix/wagpedia.jpg" alt="WAGpedia" border="0" align="middle" title="WAGpedia" width="475" height="331">
   </div>
   </a>
   </div>
   <!--Product Banner-->
   <!--Article and Q&A-->
   <div class="cont980">
   <div class="artBox_hp fl">
     <!-- title -->
<h3><a href="/dog-blog/">Articles</a></h3>
    <!-- 	<h3 style="margin-bottom:10px;"><a href="/dog-blog/">articles</a></h3>title -->
        <!-- content -->

<div class='imgtxtcont' id='1'>
            
              
     <div class="imghold" align="center" style="width:90px; height:73px;"><a href="/profile//"><img src="/imgthumb/90x73-Cause-for-Paws.png" alt="Neha"  width="90" height="59" title="Neha" /></a></div>                
					
	<div class="txthold" style="width:372px; "><h2><a href="/cause-for-paws-attracts-more-than-4-million-viewers/">"Cause For Paws" attracts more than 4 million viewers!</a></h2>
					
                     <p>The recent episode of &quot;Cause For Paws - An All Star Dog Spectacular&quot; attracted more than 4 million viewers, a happy result for the producers of the sh...  <a href='/cause-for-paws-attracts-more-than-4-million-viewers/' class='read'>read more</a> </p>
			<div class="fl"><p class="caption1" align="left">
							 Posted in: <b><a href='/dog-blog/category/wag-news/'>Wag News</a></b> | Posted by:  
     <a href="/profile/neha.manchanda/">Neha Manchanda</a> | Date:29 Dec 2015</p>
</div>

			</div>     <!--class="txthold" style="width:622px;"-->
                        <div class="cb"></div>
					</div> <!-- class="imgtxtcont"  above div         -->
                      <div class="cb"></div>

        
<div class='imgtxtcont' id='2'>
            
              
     <div class="imghold" align="center" style="width:90px; height:73px;"><a href="/profile//"><img src="/imgthumb/90x73-skydiving-1.jpg" alt="Neha"  width="90" height="63" title="Neha" /></a></div>                
					
	<div class="txthold" style="width:372px; "><h2><a href="/skydiving-dog-sets-new-record-at-13000-feet/">Skydiving dog sets new record at 13,000 feet! </a></h2>
					
                     <p>One of the most exhilarating sports in the world is also one of the most challenging. Yes that&rsquo;s right, we are talking about sky diving! Jumping off a pla...  <a href='/skydiving-dog-sets-new-record-at-13000-feet/' class='read'>read more</a> </p>
			<div class="fl"><p class="caption1" align="left">
							 Posted in: <b><a href='/dog-blog/category/wag-news/'>Wag News</a></b> | Posted by:  
     <a href="/profile/neha.manchanda/">Neha Manchanda</a> | Date:26 Dec 2014</p>
</div>

			</div>     <!--class="txthold" style="width:622px;"-->
                        <div class="cb"></div>
					</div> <!-- class="imgtxtcont"  above div         -->
                      <div class="cb"></div>

        
<div class='imgtxtcont' id='3'>
            
              
     <div class="imghold" align="center" style="width:90px; height:73px;"><a href="/profile//"><img src="/imgthumb/90x73-Christmas-care-1.jpg" alt="Neha"  width="87" height="73" title="Neha" /></a></div>                
					
	<div class="txthold" style="width:372px; "><h2><a href="/merry-christmas-christmas-care-for-pets/">Merry Christmas! Christmas & care for pets!!</a></h2>
					
                     <p>December is one of the most festive month&rsquo;s of the year. With Christmas and New Year holidays to mark the end of an old year and the beginning of a new ye...  <a href='/merry-christmas-christmas-care-for-pets/' class='read'>read more</a> </p>
			<div class="fl"><p class="caption1" align="left">
							 Posted in: <b><a href='/dog-blog/category/wag-brag/'>Wag Brag</a></b> | Posted by:  
     <a href="/profile/neha.manchanda/">Neha Manchanda</a> | Date:25 Dec 2014</p>
</div>

			</div>     <!--class="txthold" style="width:622px;"-->
                        <div class="cb"></div>
					</div> <!-- class="imgtxtcont"  above div         -->
                      <div class="cb"></div>

                     
<a href="/dog-blog/" class="viewall">View More » </a><!-- content -->   </div>
   <div class="artBox_hp no_margin_hp fl">
   <h3><a href="/qna/">Q&A </a></h3>
   <ul>
      <li class="fl"><a href="/qna/size-dog-clthes/">how do i size dog clthes...</a></li>
   <li class="fl"><a href="/qna/have-60-day-old-cocker-spaniel-he-tends-bite-lot-he-very-ac/">I have a 60 day old cocker spaniel he tends to bite a lot he is very active and playfull and is fed regularly can anyone suggest what to do...</a></li>
   <li class="fl"><a href="/qna/moving-out-india-dont-have-anyone-look-after-ma-6-months-la/">I am moving out of India and don't have anyone to look after ma 6 months labrador-mongrel mix puppy. Any true dog lover around dwarka new delhi ...</a></li>
   <li class="fl"><a href="/qna/dalmatian-been-having-lot-ear-wax-lately-since-winters-have/">My dalmatian has been having a lot of ear wax lately since the winters have started, I need suggestion for a good ear drop....</a></li>
   <li class="fl"><a href="/qna/looking-flexible-dog-chew-sticks-not-stiff-ones-had-been-fe/">I am looking for Flexible dog chew sticks, not the stiff ones. I had been feeding my adult male German Spitz All4Pets meat/chicken flavored dog sticks. On biting the sticks used to...</a></li>
   </ul>
   <a href="http://www.dogspot.in/qna/" class="viewall">View More » </a>
   </div>
   </div>
   <!--Article and Q&A-->
<!--USer Stories-->


<div class="cont980">
<!--brands logos-->
<div>
<link type="text/css" rel="stylesheet" href="/new/shop/css/brand-slider.css" />
<!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>-->
<script type="text/javascript" src="/new/shop/js/jquery.flexisel.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $("#flexiselDemo1").flexisel();
    
});
</script>

<ul id="flexiselDemo1"> 
        <li><a href="/karlie/" style="text-decoration:none"><img src="/imgthumb/130x54-1309423312640.jpg" alt="KARLIE" title="KARLIE" width="54" height="54" /></a></li>
        <li><a href="/forbis/" style="text-decoration:none"><img src="/imgthumb/130x54-1308854870499.jpg" alt="Forbis" title="Forbis" width="100" height="54" /></a></li>
        <li><a href="/isle-dogs/" style="text-decoration:none"><img src="/imgthumb/130x54-1314451839602.jpg" alt="Isle of Dogs" title="Isle of Dogs" width="65" height="54" /></a></li>
        <li><a href="/flexi/" style="text-decoration:none"><img src="/imgthumb/130x54-1309427010000.jpg" alt="Flexi" title="Flexi" width="99" height="54" /></a></li>
        <li><a href="/dogs-and-pups-11/" style="text-decoration:none"><img src="/imgthumb/130x54-1308566460375.jpg" alt="Dogs and Pups" title="Dogs and Pups" width="104" height="54" /></a></li>
        <li><a href="/kong/" style="text-decoration:none"><img src="/imgthumb/130x54-1306754111299.jpg" alt="Kong" title="Kong" width="101" height="54" /></a></li>
        <li><a href="/prozyme/" style="text-decoration:none"><img src="/imgthumb/130x54-1407329469061.jpg" alt="Prozyme" title="Prozyme" width="54" height="54" /></a></li>
        <li><a href="/knot-rope/" style="text-decoration:none"><img src="/imgthumb/130x54-1310122315312.jpg" alt="Knot A Rope" title="Knot A Rope" width="61" height="54" /></a></li>
        <li><a href="/ourpet_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1310118180546.jpg" alt="Ourpets" title="Ourpets" width="127" height="54" /></a></li>
        <li><a href="/jerhigh/" style="text-decoration:none"><img src="/imgthumb/130x54-1310123887437.jpg" alt="JerHigh" title="JerHigh" width="130" height="48" /></a></li>
        <li><a href="/durapet/" style="text-decoration:none"><img src="/imgthumb/130x54-1306754282172.jpg" alt="Durapet" title="Durapet" width="89" height="54" /></a></li>
        <li><a href="/doggie-dabbas/" style="text-decoration:none"><img src="/imgthumb/130x54-1409659081723.jpg" alt="Doggie Dabbas" title="Doggie Dabbas" width="89" height="54" /></a></li>
        <li><a href="/creature-companions/" style="text-decoration:none"><img src="/imgthumb/130x54-1307624529377.jpg" alt="Creature Companions" title="Creature Companions" width="130" height="39" /></a></li>
        <li><a href="/dogs-more/" style="text-decoration:none"><img src="/imgthumb/130x54-1313669261843.jpg" alt="Dogs & More" title="Dogs & More" width="130" height="47" /></a></li>
        <li><a href="/huft/" style="text-decoration:none"><img src="/imgthumb/130x54-1307722675281.jpg" alt="Heads Up For Tail s" title="Heads Up For Tail s" width="88" height="54" /></a></li>
        <li><a href="/phoenix-pet-foods/" style="text-decoration:none"><img src="/imgthumb/130x54-1314097315060.jpg" alt="Phoenix" title="Phoenix" width="48" height="54" /></a></li>
        <li><a href="/beaphar/" style="text-decoration:none"><img src="/imgthumb/130x54-1310126344812.jpg" alt="Beaphar" title="Beaphar" width="130" height="35" /></a></li>
        <li><a href="/chomp/" style="text-decoration:none"><img src="/imgthumb/130x54-1311072766093.jpg" alt="Chomp" title="Chomp" width="114" height="54" /></a></li>
        <li><a href="/dogspot/" style="text-decoration:none"><img src="/imgthumb/130x54-1373960539094.jpg" alt="DogSpot" title="DogSpot" width="130" height="52" /></a></li>
        <li><a href="/royal-canin/" style="text-decoration:none"><img src="/imgthumb/130x54-1311761791031.jpg" alt="Royal Canin" title="Royal Canin" width="130" height="45" /></a></li>
        <li><a href="/bayer/" style="text-decoration:none"><img src="/imgthumb/130x54-1314097433736.jpg" alt="Bayer" title="Bayer" width="54" height="54" /></a></li>
        <li><a href="/palamountains/" style="text-decoration:none"><img src="/imgthumb/130x54-1311761565312.jpg" alt="palaMountains" title="palaMountains" width="130" height="21" /></a></li>
        <li><a href="/frontline/" style="text-decoration:none"><img src="/imgthumb/130x54-1329992425247.jpg" alt="FRONTLINE" title="FRONTLINE" width="130" height="38" /></a></li>
        <li><a href="/petdig-30/" style="text-decoration:none"><img src="/imgthumb/130x54-1329930754048.jpg" alt="PETDIG" title="PETDIG" width="96" height="54" /></a></li>
        <li><a href="/scoobee/" style="text-decoration:none"><img src="/imgthumb/130x54-1314115929956.png" alt="Scoobee" title="Scoobee" width="123" height="54" /></a></li>
        <li><a href="/dr-patels/" style="text-decoration:none"><img src="/imgthumb/130x54-1419933146496.jpg" alt="Dr. Patels" title="Dr. Patels" width="62" height="54" /></a></li>
        <li><a href="/company-animals/" style="text-decoration:none"><img src="/imgthumb/130x54-1312364336015.jpg" alt="THE COMPANY OF ANIMALS" title="THE COMPANY OF ANIMALS" width="130" height="34" /></a></li>
        <li><a href="/petsport-usa/" style="text-decoration:none"><img src="/imgthumb/130x54-1329992535656.jpg" alt="PETSPORT, USA" title="PETSPORT, USA" width="85" height="54" /></a></li>
        <li><a href="/bio-groom/" style="text-decoration:none"><img src="/imgthumb/130x54-1312367860625.jpg" alt="BIO-GROOM" title="BIO-GROOM" width="130" height="52" /></a></li>
        <li><a href="/sprenger/" style="text-decoration:none"><img src="/imgthumb/130x54-1312368049281.jpg" alt="Sprenger" title="Sprenger" width="130" height="12" /></a></li>
        <li><a href="/pet-brands/" style="text-decoration:none"><img src="/imgthumb/130x54-1312370014453.jpg" alt="PET BRANDS" title="PET BRANDS" width="94" height="54" /></a></li>
        <li><a href="/bramton-company/" style="text-decoration:none"><img src="/imgthumb/130x54-1312371132062.jpg" alt="Bramton Company" title="Bramton Company" width="71" height="54" /></a></li>
        <li><a href="/npic/" style="text-decoration:none"><img src="/imgthumb/130x54-1329931934207.jpg" alt="NPIC" title="NPIC" width="94" height="54" /></a></li>
        <li><a href="/dogtec/" style="text-decoration:none"><img src="/imgthumb/130x54-1329403586121.jpg" alt="Dog*tec" title="Dog*tec" width="130" height="47" /></a></li>
        <li><a href="/venkys/" style="text-decoration:none"><img src="/imgthumb/130x54-1313839339937.jpg" alt="Venky s" title="Venky s" width="81" height="54" /></a></li>
        <li><a href="/simple-solutions/" style="text-decoration:none"><img src="/imgthumb/130x54-1314100176253.jpg" alt="Simple Solution" title="Simple Solution" width="70" height="54" /></a></li>
        <li><a href="/petmate/" style="text-decoration:none"><img src="/imgthumb/130x54-1407217782546.jpg" alt="Petmate" title="Petmate" width="54" height="54" /></a></li>
        <li><a href="/cipla/" style="text-decoration:none"><img src="/imgthumb/130x54-1314449689904.jpg" alt="Cipla" title="Cipla" width="130" height="44" /></a></li>
        <li><a href="/birdspot/" style="text-decoration:none"><img src="/imgthumb/130x54-1403010545364.png" alt="BirdSpot" title="BirdSpot" width="128" height="54" /></a></li>
        <li><a href="/tea-tree/" style="text-decoration:none"><img src="/imgthumb/130x54-1329987990992.jpg" alt="Tea Tree" title="Tea Tree" width="74" height="54" /></a></li>
        <li><a href="/intas/" style="text-decoration:none"><img src="/imgthumb/130x54-1314450484204.jpg" alt="Intas" title="Intas" width="130" height="53" /></a></li>
        <li><a href="/pet-stage/" style="text-decoration:none"><img src="/imgthumb/130x54-1314449979468.jpg" alt="Petstages" title="Petstages" width="130" height="40" /></a></li>
        <li><a href="/out/" style="text-decoration:none"><img src="/imgthumb/130x54-1315221237468.png" alt="Out" title="Out" width="54" height="54" /></a></li>
        <li><a href="/imac/" style="text-decoration:none"><img src="/imgthumb/130x54-1368684984361.JPG" alt="IMAC" title="IMAC" width="130" height="49" /></a></li>
        <li><a href="/skinneeez/" style="text-decoration:none"><img src="/imgthumb/130x54-1326356990722.jpg" alt="Skinneeez" title="Skinneeez" width="129" height="54" /></a></li>
        <li><a href="/virbac/" style="text-decoration:none"><img src="/imgthumb/130x54-1326357168855.jpg" alt="Virbac" title="Virbac" width="109" height="54" /></a></li>
        <li><a href="/petcare/" style="text-decoration:none"><img src="/imgthumb/130x54-1329992720243.jpg" alt="Petcare" title="Petcare" width="130" height="50" /></a></li>
        <li><a href="/drools/" style="text-decoration:none"><img src="/imgthumb/130x54-1329402600290.jpg" alt="Drools" title="Drools" width="101" height="54" /></a></li>
        <li><a href="/pedigree/" style="text-decoration:none"><img src="/imgthumb/130x54-1336208685297.jpg" alt="Pedigree" title="Pedigree" width="75" height="54" /></a></li>
        <li><a href="/vetnex/" style="text-decoration:none"><img src="/imgthumb/130x54-1326356715106.jpg" alt="Vetnex" title="Vetnex" width="130" height="38" /></a></li>
        <li><a href="/hellopet_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1329995991251.jpg" alt="Hello-Pet" title="Hello-Pet" width="130" height="45" /></a></li>
        <li><a href="/wahl/" style="text-decoration:none"><img src="/imgthumb/130x54-1364904216710.JPG" alt="WAHL" title="WAHL" width="130" height="48" /></a></li>
        <li><a href="/cats-best/" style="text-decoration:none"><img src="/imgthumb/130x54-1327005044204.jpg" alt="Cats Best" title="Cats Best" width="75" height="54" /></a></li>
        <li><a href="/scientific-remedies/" style="text-decoration:none"><img src="/imgthumb/130x54-1327685715729.jpg" alt="Scientific Remedies" title="Scientific Remedies" width="46" height="54" /></a></li>
        <li><a href="/whiskas/" style="text-decoration:none"><img src="/imgthumb/130x54-1337256658696.jpeg" alt="Whiskas" title="Whiskas" width="110" height="54" /></a></li>
        <li><a href="/all4pets/" style="text-decoration:none"><img src="/imgthumb/130x54-1337254301530.jpg" alt="all4pets" title="all4pets" width="130" height="45" /></a></li>
        <li><a href="/jwpet/" style="text-decoration:none"><img src="/imgthumb/130x54-1419408327855.JPG" alt="JWPet" title="JWPet" width="54" height="54" /></a></li>
        <li><a href="/pethead/" style="text-decoration:none"><img src="/imgthumb/130x54-1339514224634.jpg" alt="PetHead" title="PetHead" width="55" height="54" /></a></li>
        <li><a href="/glenand/" style="text-decoration:none"><img src="/imgthumb/130x54-1340617417210.jpg" alt="Glenand" title="Glenand" width="91" height="54" /></a></li>
        <li><a href="/me-o/" style="text-decoration:none"><img src="/imgthumb/130x54-1341234132066.jpg" alt="Me-O" title="Me-O" width="76" height="54" /></a></li>
        <li><a href="/andis/" style="text-decoration:none"><img src="/imgthumb/130x54-1344483499568.jpg" alt="ANDIS" title="ANDIS" width="55" height="54" /></a></li>
        <li><a href="/farmina/" style="text-decoration:none"><img src="/imgthumb/130x54-1344501011969.JPG" alt="Farmina" title="Farmina" width="61" height="54" /></a></li>
        <li><a href="/alc-inovators/" style="text-decoration:none"><img src="/imgthumb/130x54-1346744491072.jpg" alt="ALC INovators" title="ALC INovators" width="130" height="33" /></a></li>
        <li><a href="/innovation-india/" style="text-decoration:none"><img src="/imgthumb/130x54-1347618173239.jpg" alt="Innovation India" title="Innovation India" width="54" height="54" /></a></li>
        <li><a href="/super-dog/" style="text-decoration:none"><img src="/imgthumb/130x54-1419410485980.jpg" alt="Super Dog" title="Super Dog" width="56" height="54" /></a></li>
        <li><a href="/pet-treats-ltd/" style="text-decoration:none"><img src="/imgthumb/130x54-1370325341568.JPG" alt="Rena Treats" title="Rena Treats" width="98" height="41" /></a></li>
        <li><a href="/rolibookscom/" style="text-decoration:none"><img src="/imgthumb/130x54-1373532076930.JPG" alt="Roli Books" title="Roli Books" width="130" height="23" /></a></li>
        <li><a href="/choostix-113/" style="text-decoration:none"><img src="/imgthumb/130x54-1350631455832.png" alt="Choostix" title="Choostix" width="130" height="47" /></a></li>
        <li><a href="/vetoquinolinenhomehtml/" style="text-decoration:none"><img src="/imgthumb/130x54-1355383912047.gif" alt="Vetoquinol" title="Vetoquinol" width="128" height="54" /></a></li>
        <li><a href="/precious-paws-foundation/" style="text-decoration:none"><img src="/imgthumb/130x54-1357913225250.JPG" alt="Precious Paws Foundation" title="Precious Paws Foundation" width="67" height="54" /></a></li>
        <li><a href="/get-off/" style="text-decoration:none"><img src="/imgthumb/130x54-1364971550383.JPG" alt="Get Off" title="Get Off" width="55" height="54" /></a></li>
        <li><a href="/hyperkewl/" style="text-decoration:none"><img src="/imgthumb/130x54-1358843679539.JPG" alt="HyperKewl" title="HyperKewl" width="89" height="54" /></a></li>
        <li><a href="/techkewl/" style="text-decoration:none"><img src="/imgthumb/130x54-1358845745178.JPG" alt="TechKewl " title="TechKewl " width="95" height="54" /></a></li>
        <li><a href="/habitrail/" style="text-decoration:none"><img src="/imgthumb/130x54-1358947120011.jpg" alt="Habitrail" title="Habitrail" width="130" height="29" /></a></li>
        <li><a href="/tropimix/" style="text-decoration:none"><img src="/imgthumb/130x54-1358947696792.png" alt="Tropimix" title="Tropimix" width="128" height="54" /></a></li>
        <li><a href="/prime/" style="text-decoration:none"><img src="/imgthumb/130x54-1358948259911.png" alt="Prime" title="Prime" width="101" height="54" /></a></li>
        <li><a href="/techniche/" style="text-decoration:none"><img src="/imgthumb/130x54-1359024640333.JPG" alt="Techniche" title="Techniche" width="130" height="47" /></a></li>
        <li><a href="/natural-delicious/" style="text-decoration:none"><img src="/imgthumb/130x54-1361170272451.JPG" alt="Natural & Delicious" title="Natural & Delicious" width="61" height="54" /></a></li>
        <li><a href="/trixie/" style="text-decoration:none"><img src="/imgthumb/130x54-1361257431664.JPG" alt="TRIXIE" title="TRIXIE" width="86" height="54" /></a></li>
        <li><a href="/savavet/" style="text-decoration:none"><img src="/imgthumb/130x54-1363160540737.JPG" alt="SAVAVET" title="SAVAVET" width="130" height="51" /></a></li>
        <li><a href="/savic/" style="text-decoration:none"><img src="/imgthumb/130x54-1363173416516.JPG" alt="Savic" title="Savic" width="98" height="54" /></a></li>
        <li><a href="/rex/" style="text-decoration:none"><img src="/imgthumb/130x54-1363942335191.JPG" alt="REX " title="REX " width="77" height="54" /></a></li>
        <li><a href="/nutrivet/" style="text-decoration:none"><img src="/imgthumb/130x54-1364190062506.JPG" alt="Nutri-Vet" title="Nutri-Vet" width="74" height="54" /></a></li>
        <li><a href="/bi/" style="text-decoration:none"><img src="/imgthumb/130x54-1364189905204.JPG" alt="BI" title="BI" width="69" height="54" /></a></li>
        <li><a href="/tails-love-gifts-too/" style="text-decoration:none"><img src="/imgthumb/130x54-1413439603881.jpg" alt="Tails Love Gifts Too" title="Tails Love Gifts Too" width="70" height="54" /></a></li>
        <li><a href="/purepet/" style="text-decoration:none"><img src="/imgthumb/130x54-1413864467621.jpg" alt="Purepet" title="Purepet" width="64" height="54" /></a></li>
        <li><a href="/earthborn-holistic/" style="text-decoration:none"><img src="/imgthumb/130x54-1415704768378.jpg" alt="Earthborn Holistic" title="Earthborn Holistic" width="67" height="54" /></a></li>
        <li><a href="/proden-plaqueoff/" style="text-decoration:none"><img src="/imgthumb/130x54-1364189919028.JPG" alt=" ProDen PlaqueOff" title=" ProDen PlaqueOff" width="130" height="26" /></a></li>
        <li><a href="/ferplast/" style="text-decoration:none"><img src="/imgthumb/130x54-1364991398100.JPG" alt="ferplast" title="ferplast" width="57" height="54" /></a></li>
        <li><a href="/pfizer_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1366719236472.jpg" alt="Pfizer" title="Pfizer" width="54" height="54" /></a></li>
        <li><a href="/hills/" style="text-decoration:none"><img src="/imgthumb/130x54-1365749157000.jpg" alt="Hill s" title="Hill s" width="54" height="54" /></a></li>
        <li><a href="/boyu/" style="text-decoration:none"><img src="/imgthumb/130x54-1366105904019.JPG" alt="Boyu" title="Boyu" width="130" height="34" /></a></li>
        <li><a href="/aromatree/" style="text-decoration:none"><img src="/imgthumb/130x54-1398242195094.jpg" alt="Aromatree" title="Aromatree" width="70" height="54" /></a></li>
        <li><a href="/himalaya/" style="text-decoration:none"><img src="/imgthumb/130x54-1366805588409.JPG" alt="Himalaya " title="Himalaya " width="130" height="43" /></a></li>
        <li><a href="/cibau/" style="text-decoration:none"><img src="/imgthumb/130x54-1369204048837.JPG" alt="CIBAU" title="CIBAU" width="130" height="53" /></a></li>
        <li><a href="/cimiao/" style="text-decoration:none"><img src="/imgthumb/130x54-1369204226084.JPG" alt="CIMIAO" title="CIMIAO" width="130" height="51" /></a></li>
        <li><a href="/fun-cat/" style="text-decoration:none"><img src="/imgthumb/130x54-1369204965901.JPG" alt="Fun Cat" title="Fun Cat" width="66" height="54" /></a></li>
        <li><a href="/fun-dog/" style="text-decoration:none"><img src="/imgthumb/130x54-1369205082285.JPG" alt="Fun Dog" title="Fun Dog" width="69" height="54" /></a></li>
        <li><a href="/ecopet/" style="text-decoration:none"><img src="/imgthumb/130x54-1369205248803.JPG" alt="ECOPET" title="ECOPET" width="130" height="51" /></a></li>
        <li><a href="/kiki/" style="text-decoration:none"><img src="/imgthumb/130x54-1369299272272.JPG" alt="KIKI" title="KIKI" width="105" height="54" /></a></li>
        <li><a href="/ayurvet/" style="text-decoration:none"><img src="/imgthumb/130x54-1370001360777.JPG" alt="AYURVET" title="AYURVET" width="72" height="54" /></a></li>
        <li><a href="/sandy-perch/" style="text-decoration:none"><img src="/imgthumb/130x54-1370435261277.JPG" alt="Sandy Perch" title="Sandy Perch" width="130" height="50" /></a></li>
        <li><a href="/gold-medal-pets/" style="text-decoration:none"><img src="/imgthumb/130x54-1370586865152.JPG" alt="GOLD MEDAL Pets" title="GOLD MEDAL Pets" width="130" height="50" /></a></li>
        <li><a href="/petag/" style="text-decoration:none"><img src="/imgthumb/130x54-1372146771772.JPG" alt="PetAg" title="PetAg" width="89" height="54" /></a></li>
        <li><a href="/sheba/" style="text-decoration:none"><img src="/imgthumb/130x54-1372742558442.JPG" alt="Sheba" title="Sheba" width="62" height="54" /></a></li>
        <li><a href="/petkin/" style="text-decoration:none"><img src="/imgthumb/130x54-1373353928021.jpg" alt="Petkin" title="Petkin" width="54" height="54" /></a></li>
        <li><a href="/petkin_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1373355598852.jpg" alt="Petkin" title="Petkin" width="54" height="54" /></a></li>
        <li><a href="/miglior-gatto/" style="text-decoration:none"><img src="/imgthumb/130x54-1373370177847.jpg" alt="Miglior Gatto" title="Miglior Gatto" width="54" height="54" /></a></li>
        <li><a href="/miglior-cane/" style="text-decoration:none"><img src="/imgthumb/130x54-1373360087749.jpg" alt="Miglior cane" title="Miglior cane" width="54" height="54" /></a></li>
        <li><a href="/dog-gone-smart/" style="text-decoration:none"><img src="/imgthumb/130x54-1373974046234.jpg" alt="Dog Gone Smart" title="Dog Gone Smart" width="67" height="54" /></a></li>
        <li><a href="/oxygen-life/" style="text-decoration:none"><img src="/imgthumb/130x54-1374224189616.gif" alt="Oxygen For Life " title="Oxygen For Life " width="41" height="54" /></a></li>
        <li><a href="/vitapol/" style="text-decoration:none"><img src="/imgthumb/130x54-1375422168786.jpg" alt="Vitapol" title="Vitapol" width="67" height="54" /></a></li>
        <li><a href="/pet_santa/" style="text-decoration:none"><img src="/imgthumb/130x54-1374650075331.jpg" alt="Pet Santa" title="Pet Santa" width="54" height="54" /></a></li>
        <li><a href="/solid-gold/" style="text-decoration:none"><img src="/imgthumb/130x54-1375783372602.jpg" alt="Solid Gold" title="Solid Gold" width="61" height="54" /></a></li>
        <li><a href="/kennel-club-books/" style="text-decoration:none"><img src="/imgthumb/130x54-1375944751568.jpg" alt="Kennel Club Books" title="Kennel Club Books" width="104" height="54" /></a></li>
        <li><a href="/bright-side/" style="text-decoration:none"><img src="/imgthumb/130x54-1380797214575.jpg" alt="The Bright Side" title="The Bright Side" width="117" height="54" /></a></li>
        <li><a href="/vision_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1381486778880.jpg" alt="Vision" title="Vision" width="73" height="54" /></a></li>
        <li><a href="/woof-magazine/" style="text-decoration:none"><img src="/imgthumb/130x54-1384167617438.jpg" alt="Woof Magazine" title="Woof Magazine" width="130" height="53" /></a></li>
        <li><a href="/orione/" style="text-decoration:none"><img src="/imgthumb/130x54-1386673852739.png" alt="Orione" title="Orione" width="129" height="54" /></a></li>
        <li><a href="/special-dog/" style="text-decoration:none"><img src="/imgthumb/130x54-1387543229983.png" alt="Special Dog" title="Special Dog" width="83" height="54" /></a></li>
        <li><a href="/union-animal-lifestyle/" style="text-decoration:none"><img src="/imgthumb/130x54-1388385935636.jpg" alt="Union Animal Lifestyle" title="Union Animal Lifestyle" width="57" height="54" /></a></li>
        <li><a href="/intersand_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1388750933940.jpg" alt="Intersand" title="Intersand" width="120" height="54" /></a></li>
        <li><a href="/pettagu_2/" style="text-decoration:none"><img src="/imgthumb/130x54-1390275458111.jpg" alt="Pettagu" title="Pettagu" width="104" height="54" /></a></li>
        <li><a href="/sporn/" style="text-decoration:none"><img src="/imgthumb/130x54-1390948854913.jpg" alt="Sporn" title="Sporn" width="95" height="54" /></a></li>
        <li><a href="/yuppie-puppy_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1390944998998.jpg" alt="Yuppie Puppy" title="Yuppie Puppy" width="95" height="54" /></a></li>
        <li><a href="/living-world_1/" style="text-decoration:none"><img src="/imgthumb/130x54-1390947657512.jpg" alt="Living World" title="Living World" width="95" height="54" /></a></li>
        <li><a href="/finikee/" style="text-decoration:none"><img src="/imgthumb/130x54-1395814548043.jpg" alt="Finikee" title="Finikee" width="99" height="54" /></a></li>
        <li><a href="/pet-demand/" style="text-decoration:none"><img src="/imgthumb/130x54-1395661086829.jpg" alt="Pet Demand" title="Pet Demand" width="130" height="47" /></a></li>
        <li><a href="/daikin/" style="text-decoration:none"><img src="/imgthumb/130x54-1398426543889.jpg" alt="Daikin" title="Daikin" width="70" height="54" /></a></li>
        <li><a href="/satva/" style="text-decoration:none"><img src="/imgthumb/130x54-1401872609803.jpg" alt="Satva" title="Satva" width="58" height="54" /></a></li>
        <li><a href="/zupreem/" style="text-decoration:none"><img src="/imgthumb/130x54-1405941636945.jpg" alt="Zupreem" title="Zupreem" width="58" height="54" /></a></li>
        <li><a href="/chicopee/" style="text-decoration:none"><img src="/imgthumb/130x54-1406194350525.jpg" alt="Chicopee" title="Chicopee" width="94" height="54" /></a></li>
        <li><a href="/genesis/" style="text-decoration:none"><img src="/imgthumb/130x54-1406194518023.jpg" alt="Genesis" title="Genesis" width="94" height="54" /></a></li>
        <li><a href="/iamfree/" style="text-decoration:none"><img src="/imgthumb/130x54-1408625213593.jpg" alt="I Am Free" title="I Am Free" width="96" height="54" /></a></li>
        <li><a href="/bella-delivers/" style="text-decoration:none"><img src="/imgthumb/130x54-1409733051519.jpg" alt="Bella Delivers  " title="Bella Delivers  " width="130" height="46" /></a></li>
        <li><a href="/focus/" style="text-decoration:none"><img src="/imgthumb/130x54-1412667884712.png" alt="Focus" title="Focus" width="88" height="54" /></a></li>
        <li><a href="/bark-barkery/" style="text-decoration:none"><img src="/imgthumb/130x54-1411453346659.jpg" alt="Bark On Barkery" title="Bark On Barkery" width="70" height="54" /></a></li>
        <li><a href="/petacom/" style="text-decoration:none"><img src="/imgthumb/130x54-1412062670311.jpg" alt="Petacom" title="Petacom" width="70" height="54" /></a></li>
        <li><a href="/fidele/" style="text-decoration:none"><img src="/imgthumb/130x54-1412070673555.jpg" alt="Fidele" title="Fidele" width="70" height="54" /></a></li>
        <li><a href="/more/" style="text-decoration:none"><img src="/imgthumb/130x54-1412073129156.jpg" alt="More" title="More" width="70" height="54" /></a></li>
        <li><a href="/catspot/" style="text-decoration:none"><img src="/imgthumb/130x54-1412665849678.jpg" alt="CatSpot" title="CatSpot" width="70" height="54" /></a></li>
        <li><a href="/chip-chops/" style="text-decoration:none"><img src="/imgthumb/130x54-no-photo.jpg" alt="Chip Chops" title="Chip Chops" width="62" height="54" /></a></li>
                                                               
</ul> 
<div class="clearout"></div> 
 



</div>
<!--brands logos-->
<!-- Products Shop-->
<div style="float: left;">
 <h1 style="text-align: left; font-family:'Segoe UI';">Online Pet Products Shop</h1>
 <p style="color:#565656; line-height:16px; font-size:13px; font-family:'Segoe UI';">DogSpot is a leading online portal for all your supplies offering and provides a hassle free experience for customers and lists almost every product that is required by cat,dog, birds and small animal owners as well as their pets. DogSpot offers multiple paying options to its user such as COD, ROD, Credit Card, Debit Card and Net Banking through a secure and trusted payment gateway. DogSpot allows you to shop for dry and wet food for both your cats and dogs. Cages and crates for dogs, cats, guinea pigs,hamsters, rabbits and gerbils along with birds. Just browse through the prolific list of products and choose the product that you like. Some of the top selling brands on DogSpot include Royal Canin, Pedigree, Whiskas, Drools, Hills, IMAC and Ferplast.</p>
 </div>
<!-- Products Shop-->
</div>



   </div>
<div class="cb"></div>
<div class="vs20"></div>
   	<div class="hrline"></div>
    <style>
div.offerBanner_left {
position: fixed;
bottom: 0px;
left: px;
z-index: 99999;
border: 3px solid #585858;
border-bottom: 0px;
border-radius: 15px 15px 0 0;
overflow: hidden;
}
div.offerCls_left {
position: absolute;
top: 6PX;
right: 8PX;
font-weight: bold;
}
</style>
 <style>       
  .icon-amex {
float: left;
margin: 5px 2px;
} 
div.offerBanner {
position: fixed;
bottom: 0px;
right: 4px;
z-index: 99999;
border: 3px solid #585858;
border-bottom: 0px;
border-radius: 15px 15px 0 0;
overflow: hidden;
}  
div.offerCls {
position: absolute;
top: 6PX;
right: 8PX;
font-weight: bold;
}
</style>
   <!-- footer -->
   <link href="/new/css/footer-home.css" rel="stylesheet" type="text/css" />
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
function disablePopup1(){
document.getElementById('offerBanner').style.display="none";
var currentDate = new Date(new Date().getTime() + 20 * 60 * 60 * 1000);	
document.cookie="banner=yes; expires="+currentDate+";path=/";
	}
function disablePopup2(){
document.getElementById('offerBanner_left').style.display="none";
var currentDate = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);	
document.cookie="banner1=yes; expires="+currentDate+";path=/";
	}
/*$(document).ready(function(e) {
	$("#diwalibanner_main").hide();
    $("#diwalibanner").click(function(e) {
        $("#diwalibanner_main").show();
    });
});*/
/*
$(document).ready(function(){
	$("diwalibanner").click(
	function(){
alert();
	$("#diwalibanner_main").fadeIn("slow");
	$("#diwalibanner_main").fadeIn("4000");

	});
});*/
function showdiv(hidebanner,showbanner){
document.getElementById(hidebanner).style.display="none";
document.getElementById(showbanner).style.display="block";
}
</script>

<div id="newFooter">
  <div id="footerContent">
    <div id="footerContent_left" class="cont980">
    
        <div class="footerContainer" style="margin-right: 30px; width:250px; ">
          <h3>DogSpot</h3>
         <ul  style="width:120px; *width:120px; *float:left;">
            <li><a href="/"> Home</a></li>
            <li><a href="/shop/"> Shop</a></li>
            <li><a href="/dog-blog/"> Articles</a></li>
            <li><a href="/dog-events/"> Dog Shows</a></li>
            <li><a href="/dogs/"> Wag Club</a></li>
            <li><a href="/dog-listing/"> Classifieds</a></li>
            <li><a href="/qna/"> Q&amp;A</a></li>
            <li><a href="/dog-breeds/">Dog Breeds</a></li>
          </ul>
          <ul style="width:120px;">
            <li><a href="/about-us.php"> About Us</a></li>
            
            <li><a href="/press-release/"> Press Release</a></li>
            <li><a href="/jobs.php"> Careers</a></li>
            
            <li><a href="/sitemap.php"> Site Map</a></li>
            <li><a href="/sitemap.xml"> Sitemap XML</a></li>
            			<li><a href="/adoption/"> Adoption</a></li>
          <!--   <li><a href="#"> Sitemap</a></li>-->
          </ul>
        </div>
        <div class="footerContainer" style=" margin-right:25px; width:140px; *width:140px;*float:left; margin-left:15px; ">
          <h3>Our Policies</h3>
          <ul>
           <li><a href="/shop-policy.php#shipping"> Shipping &amp; Delivery</a></li>
            <li><a href="/shop-policy.php#cancelation"> Order Cancellation</a></li>
            <li><a href="/shop-policy.php#refund"> Refund &amp; Return</a></li>
            <li><a href="/shop-policy.php#replacement"> Replacement</a></li>
            <li><a href="/shop-policy.php#pricing"> Pricing Information</a></li>
            <li><a href="/terms-conditions.php"> Terms &amp; Conditions</a></li>
            <li><a href="/shop-policy.php#privacy"> Privacy Policy</a></li>
            <li><a href="/terms-conditions.php#disclaimer"> Disclaimer</a></li>
          </ul>
        </div>

          
        
        <div class="footerContainer" style="width:290px; margin-right:30px;">
          <h3>Top Breeds</h3>
          <ul style="width:150px;">
            <li><a href="http://www.dogspot.in/german-shepherd-dog-alsatian/"> German Shepherd</a></li>
            <li><a href="http://www.dogspot.in/rottweiler/">Rottweiler</a></li>
            <li><a href="http://www.dogspot.in/tibetan-mastiff/">Tibetan Mastiff</a></li>
            <li><a href="http://www.dogspot.in/golden-retriever/">Golden Retriever</a></li>
            <li><a href="http://www.dogspot.in/labrador-retriever/">Labrador Retriever</a></li>
            <li><a href="http://www.dogspot.in/great-dane/">Great Dane</a></li>
            <li><a href="http://www.dogspot.in/dobermann/">Doberman</a></li>
			<li><a href="http://www.dogspot.in/pug/"> Pug</a></li>
            <li><a href="http://www.dogspot.in/beagle/">Beagle</a></li>
          </ul>
          <ul style="width:135px;">
            
            <li><a href="http://www.dogspot.in/siberian-husky/">Siberian Husky</a></li>
            <li><a href="http://www.dogspot.in/pomeranian/">Pomeranian</a></li>
            <li><a href="http://www.dogspot.in/shih-tsu/">Shih tzu</a></li>
            <li><a href="http://www.dogspot.in/dalmatian/">Dalmatian</a></li>
            <li><a href="http://www.dogspot.in/dachshund-standard-smooth-haired/">Dachshund</a></li>
            <li><a href="http://www.dogspot.in/stbernard/">St.Bernard</a></li>
            <li><a href="http://www.dogspot.in/chihuahua-long-smooth-coat/">Chihuahua</a></li>
            <li><a href="http://www.dogspot.in/afghan-hound/">Afghan Hound</a></li>
            <li><a href="http://www.dogspot.in/akita/">Akita</a></li>
        
          </ul>
 
        </div>
        
      
         <div class="footerContainer" style="  width:136px;  ">
          <h3>Top Brands</h3>
         <ul style="width:125px;">           
            <li><a href="http://www.dogspot.in/dogspot/"> DogSpot</a></li>
            <li><a href="http://www.dogspot.in/focus/"> Focus</a></li>
            <li><a href="http://www.dogspot.in/drools/"> Drools</a></li>
            <li><a href="http://www.dogspot.in/Pedigree/"> Pedigree</a></li>
            <li><a href="http://www.dogspot.in/royal-canin/"> Royal Canin</a></li>
            <li><a href="http://www.dogspot.in/hills/">Hill’s Science Plan</a></li>
            <li><a href="http://www.dogspot.in/jerhigh/"> Jerhigh</a></li>
            <li><a href="http://www.dogspot.in/kong/"> Kong</a></li>
            <li><a href="http://www.dogspot.in/Pethead/"> PetHead</a></li>
            <li><a href="http://www.dogspot.in/imac/">IMAC</a></li>
                      
          </ul>
  
        </div>
      
      
      
      
    </div>
    
    <div class="footDetail">
    <div class="footHelp">
    <label>Need Help?<a href="/contactus.php"><span>Contact Us</span></a></label>
    <div class="footNum">(or) Call us: +91-9212196633</div>
    </div>
    <div class="foot_join">
    <label>Join us on</label>
     <div align="left">
          <a href="https://twitter.com/indogspot" target="_blank"  rel="nofollow"><span class="twitter"></span></a>
          <a href="http://www.facebook.com/indogspot" target="_blank" rel="nofollow"><span class="facebook"></span></a> 
          <a href="https://plus.google.com/101662873906223019669/" target="_blank" rel="nofollow"><span class="googleplus"></span></a>
          <a href="http://www.linkedin.com/company/dogspot/" target="_blank" rel="nofollow"><span class="linkedin"></span></a>
          <a href="http://www.youtube.com/user/indogspot" target="_blank" rel="nofollow"><span class="youtube"></span></a>
          <a href="http://pinterest.com/indogspot/" target="_blank" rel="nofollow"><span class="pinit"></span></a></div>
        </div>
    </div>
    </div>
    
    <div class="bottomFoot_tb">
    <div class="cont980">
    <div class="tb_listing">
    <ul>
    <li><label>3000+ Products<br />
170+Brands</label></li>
    <li>
    <div class="foot_icon_tb"><img src="http://www.dogspot.in/new/common/images/icon_delivery.png" alt="delivery" border="0" align="middle" title="delivery" width="38" height="34"></div>
    <label>Delivery<br />
Across India</label></li>
    <li><div class="foot_icon_tb"><img src="http://www.dogspot.in/new/common/images/icon_cal.png" alt="delivery" border="0" align="middle" title="delivery" width="32" height="32"></div><label>Free 30-day<br />
Return</label></li>
    <li><label>Free Pet Expert<br />
Service</label></li>
    </ul>
    </div>
    
    <div class="cateFoot_list">
            <a href="http://www.dogspot.in/treats-food/"> Dog Food</a> ,
            <a href="http://www.dogspot.in/dog-collars/"> Dog Collars</a> ,
            <a href="http://www.dogspot.in/dog-leashes/"> Dog Leash</a> , 
            <a href="http://www.dogspot.in/dog-harnesses/"> Dog Harness</a> ,
            <a href="http://www.dogspot.in/accessories/"> Dog Accessories</a> ,
            <a href="http://www.dogspot.in/clothes/"> Dog Clothes</a> ,
			<a href="http://www.dogspot.in/beds/"> Dog Beds</a> ,
            <a href="http://www.dogspot.in/dog-grooming/"> Dog Grooming</a> ,
            <a href="http://www.dogspot.in/feeders/"> Dog Feeder</a> ,
            <a href="http://www.dogspot.in/shampoos/"> Dog Shampoo</a> ,
            <a href="http://www.dogspot.in/cages/"> Dog Cage</a> ,
            <a href="http://www.dogspot.in/dog-biscuits/"> Dog Biscuits</a> ,
            <a href="http://www.dogspot.in/supplements/"> Dog Supplements</a> ,
            <a href="http://www.dogspot.in/crates/"> Dog Crates</a> ,
            <a href="http://www.dogspot.in/flea-ticks/"> Dog Ticks</a> ,
            <a href="http://www.dogspot.in/dog-toy/"> Dog Toys</a> ,
            <a href="http://www.dogspot.in/dog-meaty-treats/"> Dog Meaty Treats</a> ,
            <a href="http://www.dogspot.in/dewormer/"> Dog Dewormer </a> ,
            <a href="http://www.dogspot.in/feeders/"> Dog Feeder</a> ,
            <a href="http://www.dogspot.in/diapers/"> Dog Diapers</a> ,
            <a href="http://www.dogspot.in/muzzles/"> Dog Muzzles</a> ,
            <a href="http://www.dogspot.in/dog-bowls/"> Dog Bowls</a> ,
            <a href="http://www.dogspot.in/pet-hair-remover/"> Pet Hair Remover</a> ,
            <a href="http://www.dogspot.in/birds/"> Bird Products</a> ,
            <a href="http://www.dogspot.in/cat-food-and-treats/"> Cat Foods</a>
          
    </div>
    
    <div class="payFoot_icon">
     <h3>Payment Method</h3>
            <div class="icon-mastercard" title="mastercard"></div>
            <div class="icon-visa" title="visa"></div>
            <div class="icon-netbanking" title="netbanking"></div>
            <div class="icon-cod" title="cod"></div>
            <div class="icon-cheque" title="cheque"></div>
            <div class="icon-amex1"><img src="http://www.dogspot.in/new/common/images/amex1.png" alt="amex" width="49" height="29" title="amex"/></div>
            <div class="icon-amex1"><img src="http://www.dogspot.in/new/common/images/diner.png" alt="diner" title="diner" width="49" height="29"/></div>
                      
    </div>
    
    </div>
    </div>
  </div>
  
  <div id="footerGreen" class="footgrey">
  <div class="cont980">
    <div class="fr" style="width:150px;float: right;"><a href="http://www.dmca.com/" title="DMCA" target="_blank">
     <span class="dmca_copyright_protected150c"></span> </a></div>
    <p align="left">

    

    Views and Articles are not endorsed by DogSpot.in. DogSpot does not assume responsibility or liability for any Comment or for any claims, damages, or losses resulting from any use of the Site or the materials contained therein. All contributions and Articles are owned by DogSpot.in. </p>
  </div>
  </div>
</div><!-- main container -->
<!-- popup Box Start--> 
<!-- popup Box END-->
<!-- Google Code for Remarketing Tag -->
 

 


<!--CEMANTIKA server of events-->
<script>

</script>
<!--CEMANTIKA server of events close here-->
<!-- ClickTale end of Bottom part -->
<!-------------------------- sharengain Box Start---------------------> 
<!-------------------------- sharengain Box Start--------------------->
<!-------------------------- popup Box Start---------------------> 
<div class="popupContactSnG" id="popupContactSnG" style=" z-index: 99999;">
<span class="pop_close"><a onclick="Javascript:disablePopup('#popupContactSnG')">
<span class="close_icon"></span>
</a></span>

</div> 
<div id="backgroundPopup"></div>
<!-------------------------- popup Box END--------------------->
<iframe  width="0" height="0" frameborder="0" src="http://www.fishspot.in/chq-session.php?master_session_id=j0s4ln1psejcr5cqqhguofkfd3&userid=shailesh&sessionName=Shalesh&sessionDPid=5&sessionLevel=2&sessionProfileImg=shailesh-shailesh.jpg&sessionFacebook=100008216491662-##-CAABtHZAtcm8IBAKS2fEgyFm7AJSFXm4UwJsmsCHGOHegv9o24DTbgKN4jsp2Fpa9DtUStLEVxybAQtDapg6WFrbSZC9MDcgPZBErFScaz0VZCI2sWI6B1AK5zliZA5E1GJr9XZA41RAt5zMjoZA9WY89WZBePrVdZAoZAlbnl6GRc1cvBREeJcINZA3N1ZCIVHSLPkoZD&sessionTwitter=&sessionSocialaction="></iframe>
<div style="display:none" id="tsuser_id">5</div>
<!-- end olark code -->
<!-- Facebook Conversion Code for air purifier daikin -->


<!--date : 23/09/2014 develop by BS (Sociomantic)-->
<script type="text/javascript">
		
</script>
<script type="text/javascript">
(function(){
var s = document.createElement('script');
var x = document.getElementsByTagName('script')
[0];
s.type = 'text/javascript';
s.async = true;
s.src =('https:'==document.location.protocol?'https://':'http://') + 'ap-sonar.sociomantic.com/js/2010-07-01/adpan/dogspot-in';
x.parentNode.insertBefore( s, x );
})();
</script>
<!--date : 23/09/2014 develop by BS (Sociomantic)-->
<script type="text/javascript">
    var _ss_track={"id":"a8fb14e8-f7a0-4419-aec3-197f943e609b","events":[],"handlers":[],"alarms":[],"options":{}};
    (function() {
        var ss = document.createElement('script'); ss.type = 'text/javascript'; ss.async = true; ss.id = "__ss";
        ss.src = '//cdn-jp.gsecondscreen.com/static/ssclient.min.js';
        var fs = document.getElementsByTagName('script')[0]; fs.parentNode.insertBefore(ss, fs);
    })();
</script>
<script type="text/javascript">


    (function() {                 

        var jc = document.createElement('script');

        jc.type = 'text/javascript';

        jc.async = 'true';

        jc.src = 'https://www.junglee.com/ec/js?mId=A2WQ2B9BVSXPGW';

		document.getElementsByTagName('script')[0];

        s.parentNode.insertBefore(jc, s);

    })();


</script>
</body>
</html>
