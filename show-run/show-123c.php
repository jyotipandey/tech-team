<?
//include($DOCUMENT_ROOT."/session.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/arrays.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Run Dog Show</title>
</head>
<SCRIPT LANGUAGE="JavaScript">
function submitForms() {
if (isCcBogBreed() && isCcDogClass())
return true;
else
return false;
}

function isCcBogBreed() {
 if (document.formr.ccDogBreed.value == "0")
  {
   alert ("\n Please Select Dog Breed.")
   document.formr.ccDogBreed.focus();
   return false;
  }
 return true;
}

function isCcDogClass() {
 if (document.formr.ccDogClass.value == "0")
  {
   alert ("\n Please Select Dog Class.")
   document.formr.ccDogClass.focus();
   return false;
  }
 return true;
}

</script>

<?php require_once($DOCUMENT_ROOT.'/show-run/common/top.php'); ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
   <td align="center">
  <h2>Show for 1/2/3 of class</h2>
    <form name="formr" method="post" action="show-123c-dogs.php" onsubmit="return submitForms()">     
    <h2>Choose Dog Breed</h2>
    <p>
      <select name="ccDogBreed" id="ccDogBreed">
       <option value="0">------Select dog breed -------</option>
		  <?php
            $select_breed = mysql_query("SELECT DISTINCT breed_id FROM show_temp_report WHERE show_id = '$show_id' ORDER BY breed_id");
              if(!$select_breed){	die(mysql_error());   }
               while($row1 = mysql_fetch_array($select_breed)){
                 $breed_id = $row1["breed_id"];
                    
                  $Row_group = getSingleRow("breed_name", "show_breed", "breed_id = '$breed_id'"); //For Breed name
                  $breed_name = $Row_group["breed_name"];
                        
                echo"<option value='$breed_id'>$breed_name</option>";
              }
         ?>              
      </select>
    </p>
    <h2>Choose Breed Class</h2>
    <p>
      <select name="ccDogClass" id="ccDogClass">
       <option value="0">------Select dog Class -------</option>
      <?php
      $select_breed = mysql_query ("SELECT DISTINCT class_code FROM show_temp_report WHERE show_id = '$show_id' AND (class_code != 'G' AND class_code != 'O')ORDER BY class_code");
	   if(!$select_breed){    die(mysql_error());    }
		while($row1 = mysql_fetch_array($select_breed)){
		  $class_code = $row1["class_code"];
			
		  $Row_CLass = getSingleRow("class_name, class_code, sex", "show_class", "class_code = '$class_code' AND type_id = '2'"); //For Breed name
			$class_code = $Row_CLass["class_code"];
			$class_name = $Row_CLass["class_name"];
			$sex = $Row_CLass["sex"];
				
		echo"<option value='$class_code'>$class_code - $class_name ($DogSex[$sex])</option>";
	  }
	 ?>		 
   </select>
  </p>
  <p>
     <input name="show_id" type="hidden" id="show_id" value="<? echo"$show_id";?>" />
     <input name="ring_id" type="hidden" id="ring_id" value="<? echo"$ring_id";?>" />
      <input type="submit" name="ListCC" id="ListCC" value=" List all Dogs " />
 </p>
 </form>
    </td>
  </tr>
</table>

<?php require_once($DOCUMENT_ROOT.'/show-run/common/bottom.php'); ?>