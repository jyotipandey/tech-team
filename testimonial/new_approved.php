<?php
require_once($DOCUMENT_ROOT."/session-require.php"); 
require_once($DOCUMENT_ROOT."/arrays.php");
require_once($DOCUMENT_ROOT."/database.php");
require_once($DOCUMENT_ROOT."/functions.php");

$sitesection = "testimonial_new";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dog testimonial : DogSpot: Spot for all your Dog's Needs</title>
<meta name="keywords" content="Dog testimonial :DogSpot: Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog testimonial :DogSpot: Spot for all your Dog's Needs, dogs, dogs india, dogs world, puppies, dog" />
<link rel="stylesheet" type="text/css" href="/new/css/dropdown.css" media="all"  />
<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />
<link type="text/css" rel="stylesheet" href="/new/css/business_listing.css" media="all" />




<?php require_once($DOCUMENT_ROOT.'/new/common/top.php'); ?>
<?php

 $selectArt = query_execute_row("SELECT comments FROM users_testimonial WHERE testimonial_id = '$testimonial_id'");
 
  
 
	
	$art_subject = $selectArt["comments"];
	
	$art_subject = stripslashes($art_subject);
	$art_subject = breakLongWords($art_subject, 30, " ");


?>
      
   	<!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="cont980">
                
            <div class="fl">
                        <p align="left"><a href="/">Home</a> &gt; Approve testimonial</p>
                     </div>
                  <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->
      <div class="vs15"></div>      
   <!-- main container -->
   		<div class="cont980">
        <form id="form1" name="form1" method="post" action="">
        <p align="left"> <strong>Your Testimonial:</strong> </p>
         
          <textarea name="textarea" id="textarea" disabled="disabled"  cols="35" rows="3"><?= $art_subject;?></textarea>
        </form>
        <p style="font-size:18px;" >The above testimonial will be published after a review by a panel of experts at DogSpot. The following rules will be followed </p>

 <ul type="disc">
 <li>The testimonial should be well researched.</li><br />

 <li> The testimonial should be backed by data / information, surveys as the case may be.</li><br />

 <li>The testimonial should be of general public interest. Specific queries on any issues / subject would be diverted to the forum.</li><br />

 <li>The users need to dispel a sense of responsibility by not writing/publishing any information which is not in general public interest.</li><br />

 <li>The testimonial can represent personal views / opinion but should not be directed at any specific individual(s) representing any personal interest.</li><br />

 <li>All testimonial will be moderated and no message that has inflammatory, abusive, derogatory language or any language deemed unfit for publication will displayed.
</li><br />

 <li>Though it will be endeavored that as manytestimonial as possible are displayed, however there will be time lag between the submission and publication of the testimonial.
 </li><br />

 <li>The views represented in the testimonial are solely of the testimonialer, DogSpot does not either endorse or reject the views.</li><br />

  <li>DogSpot reserves the right to cancel, modify, edit any testimonial without taking any prior permission from the testimonialer.</li><br />

  <li>DogSpot will consider presenting the name of the testimonialer in the up-coming Expert Zone.</li>
 </ul>
 <br />


   </div>
   <!-- main container -->
   
   <?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>  

