<?php 
include($DOCUMENT_ROOT."/session-require.php"); 
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invite your Friends to DogSpot.in</title>
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript" src="/js/highslide-html.js"></script>
<script type="text/javascript" language="JavaScript"><!--
function HideContent(d) {
document.getElementById(d).style.display = "none";
}
function ShowContent(d) {
document.getElementById(d).style.display = "block";
}
function ReverseDisplay(d) {
if(document.getElementById(d).style.display == "none") { document.getElementById(d).style.display = "block"; }
else { document.getElementById(d).style.display = "none"; }
}
//--></script>
<script type="text/javascript">    
    hs.graphicsDir = '/site-img/graphics/';
    hs.outlineType = 'rounded-white';
    hs.outlineWhileAnimating = true;
    hs.objectLoadTime = 'after';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<link href="/css/global.css" rel="stylesheet" type="text/css" />
<script src="/js/ajax.js" type="text/javascript"></script>
<?php include($DOCUMENT_ROOT."/common/top.php"); ?>
<div id="pagebody">
 	<div style="margin:10px;">
    <strong>You can skip this page and go straight to the profile<a href="../profile/"> home page >> </a></strong>  </div>
    <?php require_once('invite-box.php'); ?>
    </div>
  <?php include($DOCUMENT_ROOT."/common/bottom.php"); ?>
