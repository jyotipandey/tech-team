<?php require_once('session.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Dog lovers Community Health Information</title>
<meta name="keywords" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<?php $sitesection = "profile"; ?>
<?php require_once('../common/top.php'); ?>
<?php
require_once('arrays.php');
require_once('database.php');
require_once('functions.php');
require_once('common/countries.php');

if(!$member_id){
	$member_id=$userid;
}

?>
<div id="pagebody">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
       <td width="160" valign="top"><?php require_once('../common/navi.php'); ?></td>
        <td valign="top">
        <div id="inbody">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>
        </td>
      </tr>
    </table>
</div>  
<?php require_once('../common/bottom.php'); ?>