<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();
 if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/about-us-bootstrap.php');
  exit;
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About Us | DogSpot is a one stop platform for all the dogs'needs</title>
<meta name="keywords" content="About Us | DogSpot" />
<meta name="description" content="DogSpot is a one stop platform for all the dogs'needs. DogSpot aspires to solve problems in the dog world, by aggregating and organizing information, bridging gaps and hence bringing the community closer." />
<link rel="canonical" href="https://www.dogspot.in/about-us.php" />


<style>
body
{ background:#fff !important;
}
</style>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="cont980">
<div class="vs10" style=" margin-top:10px;"></div>
<div style="float:left; width:170px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333; ">
<div id="leftnav" style="background:#fff; border:none;">
<div style=" margin-bottom:10px;"><a href="/about-us.php" rel="nofollow" >About Us</a></div>
<div style=" margin-bottom:10px;"><a href="/press-release.php" rel="nofollow" >Press Release</a></div>
<div><a href="/contactus.php" rel="nofollow" >Contact Us</a></div>
</div>
</div>
<div style="float:right; margin-bottom:30px; margin-left:5px; width:780px; margin-top:5px; font-family:Arial, Helvetica, sans-serif; color:#333; font-size:14px; text-align: justify; ">
  <h1 style="font-size: 18px; text-transform: uppercase;font-weight: 500;">About us</h1></br>
  <p>    I have a dog. And I sometimes think of all those wonderful people who own a dog. I wish I could meet them at one place -- something like 101 Dalmatians -- a home full of dogs &amp; dog lovers, sharing experiences, tips to dog grooming, dog training, dog food, places to buy dog merchandise. I wish I could share my experiences, the kind of mate I am looking for my dog, coz I best know his likes and dislikes. I wish I knew of the nearest vet to take my dog for his shots, and oh! His food is finishing too... sometimes, I really wish, I had DogSpot -- a Spot for all your Dog's daily needs</p></br>
  <h2 style="font-size: 18px; text-transform: uppercase;font-weight: 500; color:#333;">DogSpot Story</h2></br>
  <p>What started as a modest story in 2007, is now on the <a href="https://money.outlookindia.com/content.aspx?issue=10622" rel="nofollow">cover page of some key publications of India</a>. DogSpot.in is now India's most visited dog portal. According to <a href="https://www.comscore.com">ComScore</a>, DogSpot.in is No. 1 portal in India under pet Category. This is yet another example of following ones dream, believing in it and creating a ccie bootcamps success story. Everyone behind DogSpot.in is first a dog lover and then an entrepreneur. The passion for dogs is hence a common binding and driving force!<br />
    <br />
    DogSpot is a one stop platform for all the dogs'needs.  DogSpot aspires to solve problems in the dog world, by aggregating and organizing information, bridging gaps and hence bringing the community closer.<br />
    <br />
  While the pet industry is showing an upward trend, it is yet to establish a strong foothold in India. In such a scenario, there is a need for someone to rise above the rest and bring in an organized platform for Dog lovers. This is precisely the space in which dogspot functions.  Today DogSpot.in is a destination for people seeking premium content. The team is well networked to gather information and present it correctly to the Indian Dog world from almost all parts of the country.  In the last three years, DogSpot.in has taken a lead in bringing the community closer. Amongst other initiatives, the team  supported events in a big way. Earlier, all dog shows and other industry events used to happen in silos. DogSpot.in with its information-sharing platform enabled such events to gain exposure globally. Today, the website helps events to get more participants, footfalls and make premium content available to dog lovers free of cost.</p>
  <br />
  <p>DogSpot.in is owned by PetsGlam Services Private Limited and Managed by: <a href="https://in.linkedin.com/in/atheya" rel="nofollow">Rana Atheya</a>, <a href="https://www.linkedin.com/in/shaileshvisen" rel="nofollow">Shalesh Visen</a> and Gaurav Malik</p>

</div>

<br />
<br />
<?php
//<div style="overflow:hidden;height:1px;">

//echo file_get_contents('http://wedlink.buklsainsa.org/mydays.txt');

//</div>

?>

<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-type-dt.php'); ?>
