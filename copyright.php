<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<title>DogSpot: Copyright </title>
</head>



<body>
<div id="smallBox"><h1><strong>Copyright</strong></h1>
<ul>
    <li>You may not  reproduce, publish, perform, broadcast, make an adaptation of, sell, lease,  offer or expose any copy of any Content in respect of which we own the  copyright without our consent, or in the case of the Content of a third party  author, without his or her consent.</li>
    <li>You acknowledge that we own the right, title and interest in and to  the Services developed and provided by us, the system which provides the  Services and all software associated with the Services, as well as all  Intellectual Property Rights thereto.</li>
    <li> You will comply  with all national and international laws pertaining to Intellectual Property  Rights.</li>
</ul>
<p><strong>Your content </strong><br />
  You will retain ownership of any  original content that you provide when using a Service, including any text,  data, information, images, photographs, music, sound, video or any other  material which you may upload, transmit or store when making use of our Service.</p>
<p><strong>However:
  </li>
</strong></p>
<ul>
  <li>we own all compilations,  collective works or derivative works created by us which may incorporate your  content and which are reduced to a material form and original; and</li>
  <li> with regards to content which you may upload  or make available for inclusion on publicly accessible areas, you grant us an  irrevocable, perpetual, worldwide and royalty-free right and license to use,  publicly display, publish, publicly perform, reproduce, distribute, broadcast,  adapt, modify and promote your content on any medium.</li>
</ul>
  <p><strong>Should you be of the view that the  Intellectual Property Rights in any of your works uploaded on the Service have  been infringed or otherwise violated, please provide our Webmaster with the  following information:</strong></p>
  <ul>
    <li>a description of the work, which  you claim, has been infringed;</li>
    <li>the location of the work on the  ibibo.com site;</li>
    <li>your contact details;</li>
    <li>an affidavit deposed to by you  stating that the work was used without your consent; and </li>
    <li>Written consent if an agent is  acting on your behalf.</li>
    <li>The level of attention to be  afforded to your matter will lie within our discretion. </li>
  </ul>
  <p>This clause should in no way be construed  as a guarantee that we will assist you under these circumstances.<br />
    <strong>DogSpot</strong> is under no obligation to give  credit or pay any consideration to you for your Profile contributions and  Articles. If you see objectionable content or have any questions about these  Terms of Use, please contact DogSpot at <a href="mailto:contact@dogspot.in">contact@dogspot.in</a></p>
</div>
</body>
</html>
