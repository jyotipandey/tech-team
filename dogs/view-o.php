<?php require_once('../session-no.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Dog lovers Community Health Information</title>
<meta name="keywords" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<script src="/js/ajax.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<?php $sitesection = "dog"; ?>
<?php require_once('../common/top.php'); ?>
<?php
require_once('../arrays.php');
require_once('../database.php');
require_once('../functions.php');
$dogDet = dogDetails($dog_id);
$dog_owner = $dogDet[userid];
?>
  <div id="pagebody">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top"><?php require_once('../common/navi.php'); ?></td>
        <td valign="top"><div id="inbody">
              <p class="sitenavi"><a href="/">Home</a> &gt; <a href="/dogs/">My dogs</a> >> dog</p>
            <p><a href="/dogs/new">Add dogs</a> | <a href="/dogs/">My dogs</a></p>
            <p><a href="<? echo"/msg/compose?msg_to=$dog_owner";?>">Send message</a> | <a href="#">Add to friends</a></p>
                   
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <td>&nbsp;</td>
                  <td align="right"><a href="<? echo"new?dog_id=$dog_id";?>"></a></td>
                  <td width="100" align="right"><?
				  if($dog_owner == $userid){
				  echo"<a href='new?dog_id=$dog_id'><strong>Edit dog details</strong></a>";
				  }
                  ?></td>
                </tr>
                <tr class="ltd">
                  <td width="150"><strong>Name: </strong></td>
                  <td><? echo"$dogDet[dog_name]";?></td>
                  <td rowspan="4" valign="top"><? 
				  $dog_image = $dogDet[dog_image];
				  if($dog_image){ ?>
				  <a href="#"><img src="<? echo"images/thumbs/thumb_$dog_image";?>" width="100" height="87" border="0" onClick="MM_openBrWindow('<? echo"/large.php?image=$dog_image&imgurl=/dogs/images/"; ?>','','scrollbars=yes,resizable=yes,width=600,height=500')" /></a> 
<? }else{ ?>
<a href="<? echo"img?dog_id=$dog_id";?>"><img src="/images/no-img.jpg" alt="No image Photo available" width="100" height="87" border="0"/></a> 
<? }?></td>
                </tr>
                <tr class="dtd">
                  <td><strong>Dog Owner:</strong></td>
                  <td><? $dog_owner = $dogDet[userid];
				  print (dispUname($dog_owner));?></td>
                </tr>
                <tr class="ltd">
                  <td><strong> Breed: </strong></td>
                  <td><? echo"$dogDet[dog_breed]";?></td>
                </tr>
                <tr class="dtd">
                  <td><strong>Color:</strong></td>
                  <td><? echo"$dogDet[dog_color]";?></td>
                </tr>
                
                <tr class="ltd">
                  <td><strong>Sex: </strong></td>
                  <td><? $dogs = $dogDet[dog_sex];
				  echo"$DogSex[$dogs]";?></td>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr class="dtd">
                  <td><strong>Availabe for dating: </strong></td>
                  <td><? $Ad = $dogDet[dog_dating];
				  echo"$ADating[$Ad]";?></td>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr class="ltd">
                  <td><strong>CC No: </strong></td>
                  <td><? echo"$dogDet[dog_cc]";?></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="dtd">
                  <td><strong>Rcc No: </strong></td>
                  <td><? echo"$dogDet[dog_rcc]";?></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="ltd">
                  <td><strong>BIS No: </strong></td>
                  <td><? echo"$dogDet[dog_bis]";?></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="dtd">
                  <td><strong>KCI Registration: </strong></td>
                  <td><? echo"$dogDet[kci_reg]";?></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="ltd">
                  <td valign="top"><strong>Description: </strong></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="dtd">
                  <td colspan="3" valign="top"><? echo nl2br($dogDet[dogs_desc]);?></td>
                </tr>
              </table>
           
            
        </div></td>
      </tr>
    </table>
  </div>