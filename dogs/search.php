<?php 
include($DOCUMENT_ROOT."/session-no.php"); 
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");
include($DOCUMENT_ROOT."/dogs/arraybreed.php");
include($DOCUMENT_ROOT."/common/countries.php");

$sitesection = "dog";

//------------Show
$maxshow = 20;
if (empty($show)) {
	$show=0;
}else{
$show = $show - 1;
}
$showRecord = $show * $maxshow;
$nextShow = $showRecord + $maxshow;
//-------------Show	

$BannerCount="1";

if($dog_breedSearch || $citySearch){
if($dog_breedSearch && !$citySearch){
  $sql="SELECT * FROM dogs_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status='publish' ORDER BY dog_id DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM dogs_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status='publish' ORDER BY dog_id DESC";
}elseif($citySearch && !$dog_breedSearch){
  $sql="SELECT * FROM dogs_available WHERE city_id = '$citySearch' AND publish_status='publish' ORDER BY dog_id DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM dogs_available WHERE city_id = '$citySearch' AND publish_status='publish' ORDER BY dog_id DESC";
}else{
  $sql="SELECT * FROM dogs_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND publish_status='publish' ORDER BY dog_id DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM dogs_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND publish_status='publish' ORDER BY dog_id DESC";
}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?
$pageTitle = "Dog India DogSpot.in";
$pageh1="Dogs";
$pageKey="Dogs India, Dog India, dogs for sale, dog breeds, breeding dogs";
$pageDesc="Dogs India, Dog India DogSpot.in";
?>
<title><? echo"$pageTitle";?></title>
<meta name="keywords" content="<? echo"$pageKey";?>" />
<meta name="description" content="<? echo"$pageDesc";?>" />
<script src="/js/SpryMenuBar.js" type="text/javascript"></script>
<link href="/css/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<? if($section[0]!='dogspot' && $section[0]!='training-products' && (strpos($urldogspot,'/training-products/?shopQuery=') !== false || strpos($urldogspot,'/dogspot/?shopQuery=') !== false)){?>
 <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<? }?>
<link type="text/css" rel="stylesheet" href="/new/css/main.css" media="all" />
<!--<link type="text/css" rel="stylesheet" href="/new/css/headfoot.css" media="all" />-->
<link rel="stylesheet" type="text/css" href="/new/css/business_listing.css" media="all"  />
<link rel="stylesheet" type="text/css" href="/shapopup/shapopup.css" media="all"  />
<link href="/css/common.css?v=020112" rel="stylesheet" type="text/css" />
<link href="/new/css/style1.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->
<script type="text/javascript" src="/js/highslide.js"></script>

<script language="javascript" type="text/javascript">
function SubmitFormSearch(){
if(document.formSearch.dog_breedSearch.value == "" && document.formSearch.citySearch.value == ""){
	document.getElementById("DivSearch").innerHTML = " * Select Dog Breed or Location ";
	document.formSearch.dog_breedSearch.focus();
	return false;
}else{
 	document.getElementById("DivSearch").innerHTML = "";
	return true;
}

}//End function 
</script>  
<style>
#mytable{ 
 
  border-collapse: collapse; 
}
/* Zebra striping */
#mytable tr:nth-of-type(odd) { 
  background: #eee; 
}
#mytable th { 
  background: #999; 
  color: #fff; 
  font-weight:normal;
  font-size:16px;
}
#mytable td, th { 

  padding: 5px; 
  border: 1px solid #ccc; 
  text-align: left; 
  font-weight:normal;
  font-size:14px;
}
</style>

<!--<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
-->
 <?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="breadcrumb" style="margin-top:0px;">
            	<div class="cont980" >
                 
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="61%">
                    <a href="/">Home</a> » Dogs 
</td>                   
                  
                     <td width="20%" align="right">
                     <div class="rea">
                                              
                        <a href="http://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a><a href="http://twitter.com/#!/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a>
                     </div></td>
                    </tr>
                  </table>
                  <div class="cb"></div>
                 </div>
            
            </div>
            <div class="cont980">
            <div class="cont660" style="width:644px; margin-right:28px; ">
             
             <div>
             
              
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="61%"><h1><? echo"$pageh1";?></h1>
</td>
                    <td width="19%" align="right" ><div class="rea">
                              <a href="/dogs/new.php"><img src="/new/pix/add-new-dog.png" /></a>

                      </div></td>
                     
                  
                  
                    </tr>
                  </table>
                 <div class="cb"></div>
                 </div>
 
             <div id="clearall"></div> 
  
         <div class="vs5"></div>
        
   <div id="searchBoxBody">
    <form id="formSearch" name="formSearch" method="get" action="/dogs/search.php" onsubmit="return SubmitFormSearch()">
    <table>
     <tr><td colspan="2">
        <div align="left"><div class="redbtext" id="DivSearch"></div> 
         <select name="dog_breedSearch" id="dog_breedSearch"style=" width:300px;" >
      	  <option value="">Select Dog Breed</option>
          <? foreach($ArrDogBreed as $KeyBreedNice => $BreedName){
			 print "<option value='$KeyBreedNice'";
		     if($KeyBreedNice == "$dog_breedSearch"){ echo"selected"; } print ">$BreedName</option>";
		   }
		 ?>
        </select>
        </div>
     </td>
     <td>
         <select id="citySearch" name="citySearch">
          <option value="">Select Current Location</option>
            <?php
              $queryCitySearch = mysql_query("SELECT * FROM city ORDER BY city_name ASC");
               if (!$queryCitySearch){	die(sql_error());	}
               while($rowCitySearch = mysql_fetch_array($queryCitySearch)){
                $city_idSearch = $rowCitySearch["city_id"];
                $cityNameSearch = $rowCitySearch["city_name"];
                //$city_niceSearch = $rowCitySearch["city_nicename"];
              
                print "<option value='$city_idSearch'";
                if($city_idSearch == $citySearch){ echo"selected"; }  print ">$cityNameSearch</option>";
               }
            ?>
         <option value="-1" <? if($citySearch == -1) echo"selected"; ?>> Other City </option>
        </select>
     </td>
     <td><input type="submit" name="searchBut" id="button" value="  Search  " class="searchBut"/></td>
    </tr>
    
   </table>
   </form>
  </div>
        

<?
$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());	}
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
echo "</br>";	
if($totrecord == 0){
	echo"<p align='center' class='redbtext'>No Dogs have been found.</p>"; 
}else{

echo"Viewing <b>$showRecord - $nextShow</b> out of <b>$totrecord Dogs</b>";
?></br></br>

<div id="puppiesContainer">
  <div class="puppiesContainer_header">
    <div class="image headerText_container">Image</div>
    <div class="details headerText_container">Dogs Details</div>
    <div class="breed headerText_container">Breed</div>
    <div class="age headerText_container">Sex</div>
    <div class="location headerText_container">Location</div>
  </div>
           <div class="puppiesContainer_body">
  <? 
  while($rowgroup = mysql_fetch_array($selecg)){
	$dog_id  = $rowgroup["dog_id"];
	$dog_name  = $rowgroup["dog_name"];
	$dog_color  = $rowgroup["dog_color"];
	$dog_sex = $rowgroup["dog_sex"];
	$dog_image = $rowgroup["dog_image"];
	$dog_owner = $rowgroup["userid"];
	$dog_nicename = $rowgroup["dog_nicename"];
	$breed_nicename = $rowgroup["breed_nicename"];
	$country = $rowgroup["country"];
	$state = $rowgroup["state"];
	$city = $rowgroup["city"];
	
	$dog_name = stripslashes($dog_name);

$img_src = $DOCUMENT_ROOT."/dogs/images/thumb_$dog_image";

if(file_exists($img_src)){
  $new_w = 100;
  $new_h = 87;
  $imgWH = @WidthHeightImg($img_src,$new_w,$new_h);
}
if($imgWH[0]==''){
	$imgWH[0]='100';
}if($imgWH[1]==''){
	$imgWH[1]='87';
}

?>
<div class="bodyText_row">
      <div class="image bodyText_container"><? if($dog_image){ ?>
    <a href="<? echo "/dogs/$dog_nicename/"; ?>"><img src="<? echo"/dogs/images/thumb_$dog_image";?>" border="0" alt="<? echo "$ArrDogBreed[$breed_nicename]"; print (dispUname($dog_owner));?>" title="<? echo"$ArrDogBreed[$breed_nicename] | "; print (dispUname($dog_owner));?>" width="<? echo"$imgWH[0]";?>" height="<? echo"$imgWH[1]";?>" /></a>
    <? }else{ ?>
    <a href="<? echo "/dogs/$dog_nicename/"; ?>"><img src="https://www.dogspot.in/images/no-img.jpg" alt="<? echo "$ArrDogBreed[$breed_nicename]  | "; print (dispUname($dog_owner));?>" width="100" height="87" border="0"/></a>
<? } ?>    </div>
    <div class="details bodyText_container">Dog name: <a href="<? echo "/dogs/$dog_nicename/"; ?>"><? echo"<b>"; print breakLongWords($dog_name, 20, " "); echo"</b>"; ?><br><br> </a>
	
	<?
	if($dog_owner){
	echo "Dog Owner: <a href='/profile/$dog_owner/'>"; print (dispUname($dog_owner));?><br /><br />
    <? }?>
    <? if($userid=="Guest"){?>
    <img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> <a href="<? echo"../reg-log/index.php";?>"> Contact  <? print (dispUname($dog_owner));?></a>
    <? }else{?>
    <img src="/images/email.gif" alt="contact" width="15" height="11" border="0" /> Contact <a href="<? echo "../scrapbook/msgview-post.php";?>" onclick="return hs.htmlExpand(this, { contentId: 'highslide-tc', objectType: 'iframe',	preserveContent: false, objectWidth: 600, objectHeight: 300} )"><? print (dispUname($dog_owner));?></a>
    <? }?>
</div>
    <div class="breed bodyText_container"><? echo "$ArrDogBreed[$breed_nicename]"; ?></div>
    <div class="age bodyText_container"><? echo "$DogSex[$dog_sex]"; ?></div>
    
    <div class="location bodyText_container">
	<?
	 if($city){   echo"$city<br>";   }
	 if($state){  echo"$state<br>";  }
	 if($country){  echo $countries[$country];  }
    ?>
   </div>

    </div>
    <div class="line"></div>
    
 


  <? 
	$BannerCount++;
   } 
  }
 ?>
 </table>
<div id="shownext">              
<div class="showPages">
<?
if($dog_breedSearch || $citySearch){
 if($dog_breedSearch && !$citySearch){
  $pageUrl="/dogs/search/breed/$dog_breedSearch";
 }elseif($citySearch && !$dog_breedSearch){
  $pageUrl="/dogs/search/location/$citySearch";
 }else{
  $pageUrl="/dogs/search/$dog_breedSearch/$citySearch";
 }
}

showPages($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl);
?>
</div></div>
<?php if(!$dog_id){?>
<?php require_once($DOCUMENT_ROOT.'/banner1.php'); addbanner600('74','600','1'); ?>
<div align="center" class="cont660" style="margin-top:10px;" >
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Dog More Sale in these  Cities</th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/dog-for-sale-in-delhi/"  title="Dogs For Sale In Delhi">Dogs For Sale In Delhi</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-chennai/"  title="Dogs For Sale In Chennai">Dogs For Sale In Chennai</a></td>
                    <td width="29%" ><a href="/dog-for-sale-in-hyderabad/"  title="Dogs For Sale In Hyderabad">Dogs For Sale In Hyderabad </a></td>
                    </tr>
                     <tr>
                       <td ><a href="/dog-for-sale-in-bangalore/"  title="Dogs For Sale In Bangalore">Dogs For Sale In Bangalore</a></td>
                       <td ><a href="/dog-for-sale-in-lucknow/"  title="Dogs For Sale In Lucknow">Dogs For Sale In Lucknow</a></td>
                       <td ><a href="/dog-for-sale-in-gurgaon/"  title="Dogs For Sale In Gurgaon">Dogs For Sale In Gurgaon</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/dogs/"  title="Dogs For Sale In India">Dogs For Sale In India</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-mumbai/"  title="Dogs For Sale In Mumbai">Dogs For Sale In Mumbai</a></td>
					
					
              <td width="29%" ><a href="/dog-for-sale-in-kolkata/"  title="Dogs For Sale In Kolkata">Dogs For Sale In Kolkata</a></td>                         
                    </tr>
                     <tr>
                       <td ><a href="/dog-for-sale-in-coimbatore/"  title="Dogs For Sale In Coimbatore">Dogs For Sale In Coimbatore</a></td>
                       <td ><a href="/dog-for-sale-in-agra/"  title="Dogs For Sale In Agra">Dogs For Sale In Agra</a></td>
                       <td ><a href="/dog-for-sale-in-punjab/" title="Dogs For Sale In Punjab">Dogs For Sale In Punjab</a></td>
                     </tr>
                     <tr>
                    <td ><a href="/dog-for-sale-in-pune/"  title="Dogs For Sale In Pune">Dogs For Sale In Pune</a></td>
                    
                    <td ><a href="/dog-for-sale-in-chandigarh/"  title="Dogs For Sale In Chandigarh">Dogs For Sale In Chandigarh</a></td>     
					<td ><a href="/dog-for-sale-in-trivandrum/"  title="Dogs For Sale In Trivandrum">Dogs For Sale In Trivandrum</a></td>
                    </tr>
                     </thead></table>
               
                </div>
               
            <div class="vs20"></div>    
              
              
              <div align="center" class="cont660">
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Puppies for sale in These City</th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/puppies/"  title="Puppies For Sale">Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies-for-sale-in-delhi/"  title="Puppies For Sale In Delhi">Puppies For Sale In Delhi</a></td>
                    <td width="29%" ><a href="/puppies-for-sale-in-chennai/"  title="Puppies For Sale In Chennai">Puppies For Sale In Chennai</a></td>
                    </tr>
                     <tr>
                       <td ><a href="/puppies-for-sale-in-bangalore/"  title="Puppies For Sale In Bangalore">Puppies For Sale In Bangalore</a></td>
                       <td ><a href="/puppies-for-sale-in-kolkata/"  title="Puppies For Sale In Kolkata">Puppies For Sale In Kolkata</a></td>
                       <td ><a href="/puppies-for-sale-in-pune/"  title="Puppies For Sale In Pune">Puppies For Sale In Pune</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/puppies-for-sale-in-mumbai/"  title="Puppies For Sale In Mumbai">Puppies For Sale In Mumbai</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-hyderabad/"  title="Puppies For Sale In Hyderabad">Puppies For Sale In Hyderabad</a></td>
					
					
              <td width="29%" ><a href="/puppies-for-sale-in-lucknow/"  title="Puppies For Sale In Lucknow">Puppies For Sale In Lucknow</a></td>                         
                    </tr>
                    
                     </thead></table>
               
                </div>
               
                
              <div class="vs20"></div>
              
              <div align="center" class="cont660">
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Breed puppies For Sale </th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/puppies/breed/german-shepherd-dog-alsatian/"  title="German Shepherd Puppies For Sale">German Shepherd Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies/breed/labrador-retriever/"  title="Labrador Puppies For Sale">Labrador Puppies For Sale</a></td>
                    <td width="29%" ><a href="/puppies/breed/dobermann/"  title="Doberman Puppies For Sale">Doberman Puppies For Sale</a></td>
                    </tr>
                     <tr>
                       <td ><a href="/puppies/breed/pug/"  title="Pug Puppies For Sale">Pug Puppies For Sale</a></td>
                       <td ><a href="/puppies/breed/golden-retriever/"  title="Golden Retriever Puppies For Sale">Golden Retriever Puppies For Sale</a></td>
                       <td ><a href="/puppies/breed/rottweiler/"  title="Rottweiler Puppies For Sale">Rottweiler Puppies For Sale</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/puppies/breed/great-dane/"  title="Great Dane Puppies For Sale">Great Dane Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies/breed/beagle/"  title="Beagle Puppies For Sale">Beagle Puppies For Sale</a></td>
					
					
              <td width="29%" ><a href="/puppies/breed/boxer/"  title="Boxer Puppies For Sale">Boxer Puppies For Sale</a></td>                         
                    </tr>
                     <tr>
                       <td ><a href="/puppies/breed/dalmatian/"  title="Dalmatian Puppies For Sale">Dalmatian Puppies For Sale</a></td>
                      
                    </tr>
                     </thead></table>
                            </div>
<?php }?>
</div>  

<?php if($dog_id){?>
<div align="center" style="margin-top:10px">
<?php require_once($DOCUMENT_ROOT.'/banner1.php'); addbanner600('74','600','1'); ?>
</div>
<div align="center" class="cont660" style="margin-top:10px;" >
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Dog More Sale in these  Cities</th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/dog-for-sale-in-delhi/"  title="Dogs For Sale In Delhi">Dogs For Sale In Delhi</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-chennai/"  title="Dogs For Sale In Chennai">Dogs For Sale In Chennai</a></td>
                    <td width="29%" ><a href="/dog-for-sale-in-hyderabad/"  title="Dogs For Sale In Hyderabad">Dogs For Sale In Hyderabad </a></td>
                    </tr>
                     <tr>
                       <td ><a href="/dog-for-sale-in-bangalore/"  title="Dogs For Sale In Bangalore">Dogs For Sale In Bangalore</a></td>
                       <td ><a href="/dog-for-sale-in-lucknow/"  title="Dogs For Sale In Lucknow">Dogs For Sale In Lucknow</a></td>
                       <td ><a href="/dog-for-sale-in-gurgaon/"  title="Dogs For Sale In Gurgaon">Dogs For Sale In Gurgaon</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/dogs/"  title="Dogs For Sale In India">Dogs For Sale In India</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-mumbai/"  title="Dogs For Sale In Mumbai">Dogs For Sale In Mumbai</a></td>
					
					
              <td width="29%" ><a href="/dog-for-sale-in-kolkata/"  title="Dogs For Sale In Kolkata">Dogs For Sale In Kolkata</a></td>                         
                    </tr>
                     <tr>
                       <td ><a href="/dog-for-sale-in-coimbatore/"  title="Dogs For Sale In Coimbatore">Dogs For Sale In Coimbatore</a></td>
                       <td ><a href="/dog-for-sale-in-agra/"  title="Dogs For Sale In Agra">Dogs For Sale In Agra</a></td>
                       <td ><a href="/dog-for-sale-in-punjab/" title="Dogs For Sale In Punjab">Dogs For Sale In Punjab</a></td>
                     </tr>
                     <tr>
                    <td ><a href="/dog-for-sale-in-pune/"  title="Dogs For Sale In Pune">Dogs For Sale In Pune</a></td>
                    
                    <td ><a href="/dog-for-sale-in-chandigarh/"  title="Dogs For Sale In Chandigarh">Dogs For Sale In Chandigarh</a></td>     
					<td ><a href="/dog-for-sale-in-trivandrum/"  title="Dogs For Sale In Trivandrum">Dogs For Sale In Trivandrum</a></td>
                    </tr>
                     </thead></table>
               
                </div>
               
                <div class="vs20"></div>
              
              
              <div align="center" class="cont660">
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Puppies for sale in These City</th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/puppies/"  title="Puppies For Sale">Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies-for-sale-in-delhi/"  title="Puppies For Sale In Delhi">Puppies For Sale In Delhi</a></td>
                    <td width="29%" ><a href="/puppies-for-sale-in-chennai/"  title="Puppies For Sale In Chennai">Puppies For Sale In Chennai</a></td>
                    </tr>
                     <tr>
                       <td ><a href="/puppies-for-sale-in-bangalore/"  title="Puppies For Sale In Bangalore">Puppies For Sale In Bangalore</a></td>
                       <td ><a href="/puppies-for-sale-in-kolkata/"  title="Puppies For Sale In Kolkata">Puppies For Sale In Kolkata</a></td>
                       <td ><a href="/puppies-for-sale-in-pune/"  title="Puppies For Sale In Pune">Puppies For Sale In Pune</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/puppies-for-sale-in-mumbai/"  title="Puppies For Sale In Mumbai">Puppies For Sale In Mumbai</a></td>
                    <td width="26%" ><a href="/dog-for-sale-in-hyderabad/"  title="Puppies For Sale In Hyderabad">Puppies For Sale In Hyderabad</a></td>
					
					
              <td width="29%" ><a href="/puppies-for-sale-in-lucknow/"  title="Puppies For Sale In Lucknow">Puppies For Sale In Lucknow</a></td>                         
                    </tr>
                    
                     </thead></table>
               
                </div>
               
                
              <div class="vs20"></div>
              
              <div align="center" class="cont660">
                                   <table width="100%" id="mytable">
                    <thead><tr>
                    <th colspan="4" >Find more Breed puppies For Sale </th>
                    </tr>
                    <tr>
                    <td width="23%" ><a href="/puppies/breed/german-shepherd-dog-alsatian/"  title="German Shepherd Puppies For Sale">German Shepherd Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies/breed/labrador-retriever/"  title="Labrador Puppies For Sale">Labrador Puppies For Sale</a></td>
                    <td width="29%" ><a href="/puppies/breed/dobermann/"  title="Doberman Puppies For Sale">Doberman Puppies For Sale</a></td>
                    </tr>
                     <tr>
                       <td ><a href="/puppies/breed/pug/"  title="Pug Puppies For Sale">Pug Puppies For Sale</a></td>
                       <td ><a href="/puppies/breed/golden-retriever/"  title="Golden Retriever Puppies For Sale">Golden Retriever Puppies For Sale</a></td>
                       <td ><a href="/puppies/breed/rottweiler/"  title="Rottweiler Puppies For Sale">Rottweiler Puppies For Sale</a></td>
                      </tr>
                     <tr>
                    <td width="23%" ><a href="/puppies/breed/great-dane/"  title="Great Dane Puppies For Sale">Great Dane Puppies For Sale</a></td>
                    <td width="26%" ><a href="/puppies/breed/beagle/"  title="Beagle Puppies For Sale">Beagle Puppies For Sale</a></td>
					
					
              <td width="29%" ><a href="/puppies/breed/boxer/"  title="Boxer Puppies For Sale">Boxer Puppies For Sale</a></td>                         
                    </tr>
                     <tr>
                       <td ><a href="/puppies/breed/dalmatian/"  title="Dalmatian Puppies For Sale">Dalmatian Puppies For Sale</a></td>
                      
                    </tr>
                     </thead></table>
                            </div>
<?php }?>

  </div>  
  </div>
  <div class="cont300" id="cont300" align='left'>
               
   <?php include($DOCUMENT_ROOT."/include-files/inc-dogs-categories.php"); ?>
     <?php
	 $photoskey=$ArrDogBreed[$dog_breedSearch]="$breed_name";
	 
	  require_once($DOCUMENT_ROOT.'/new/includes/navi-right.php'); ?>
      </div>
 <div class="clearall"></div>
 
 <div class="cb"></div>
  
 
 
  <?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?> 