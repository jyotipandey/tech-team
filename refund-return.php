<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address
header( "HTTP/1.1 301 Moved Permanently" );
              header( "Location: /shop-policy.php#refund" );
              exit();

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Refund Return | DogSpot</title>
<meta name="keywords" content="Refund and Return | DogSpot" />
<meta name="description" content="Refund and Return | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/top-shop.php'); ?>
<div class="cont980">
<div class="vs10"></div>
<h1>Refund and Return </h1>
  <p>In case you are not happy with the quality of the product or  the product doesn't fit properly, you can return the product to us. We will  replace the product or refund entire amount.</p></br>
  <p>Please note that the product should be unused and should be  sent back along with original box and invoice to: Sonam, DogSpot.in, U-1/10-11, DLF Phase- III
Gurgaon 122002
Haryana, India </p>
  <p>The refund process will be initiated once we have received  the product(s). Typically refunds are processed in less than 10 working days  but in case of payments by Cheque or DD, it may take a few extra days for the  cheque to be delivered to your billing address, and for the funds to be  credited to your account, once you deposit the cheque.</p>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
