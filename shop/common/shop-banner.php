<script> 
jQuery(function(){
	jQuery('#featuredItemBox span:gt(0)').hide();
	setInterval(function(){jQuery('#featuredItemBox span:first-child').fadeOut(500).next('span').fadeIn(500).end().appendTo('#featuredItemBox');}, 5000);
});
</script>
<style type="text/css"> 
<!--
#featuredItemBox {position:relative; height:224px; width:710px;}
#featuredItemBox span { position:absolute; left:0; top:0; }
#quickText{
	height: 224px;
	width: 255px;
	float: right;
	margin-left: 5px;
}
.tBig{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #FFF;
	display: block;
}
.tSmall{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #8d233f;
	display: block;
}
#qtext{
	background-image: url(/shop/image/qImgbg.gif);
	background-repeat: repeat-x;
	height: 54px;
	background-color: #e3507a;
	text-align: center;
	vertical-align: middle;
	padding-top: 20px;
}
#featuredItemBox{
	height: 224px;
	width: 710px;
	margin-bottom: 5px;
	float: left;
}
-->
</style>
<div style="border-top-style:solid; border-top-width:1px; border-top-color:#2869ab; border-bottom-color:#2869ab; border-bottom-style:solid; border-bottom-width:1px;">
<div id="featuredItemBox">
<span><a href="/shop/brand/HUFT"><img src="/shop/image/featured-slide/huft.jpg" alt="Heads Up For Tails" width="710" height="224" border="0" /></a></span>
<span><a href="/shop/brand/royal-canin"><img src="/shop/image/featured-slide/rc.jpg" alt="Royal Canin Dog Food" width="710" height="224" border="0" /></a></span>
<span><a href="/shop/brand/kong"><img src="/shop/image/featured-slide/kong.jpg" alt="Kong Products" width="710" height="224" border="0" /></a></span>
<span><a href="/shop/brand/karlie"><img src="/shop/image/featured-slide/karlie.jpg" alt="Karlie Products" width="710" height="224" border="0" /></a></span>
<span><a href="/shop/brand/flexi"><img src="/shop/image/featured-slide/flexi.jpg" alt="Flexi Products" width="710" height="224" border="0" /></a></span>
<span><a href="/shop/brand/durapet"><img src="/shop/image/featured-slide/durapet.jpg" alt="Durapet Products" width="710" height="224" border="0" /></a></span>
</div>
<div id="quickText">
<div id="qtext" ><span class="tBig">Free Shipping</span> <span class="tSmall">All Products</span></div>
<div id="qtext" style="margin-top:2px;"><span class="tBig">24x7 Care</span> <span class="tSmall">+91-9818671863</span></div>
<div id="qtext" style="margin-top:2px;"><span class="tBig">Original</span> <span class="tSmall">Branded Products</span></div>
</div>
<div id="clearall"></div>
</div>