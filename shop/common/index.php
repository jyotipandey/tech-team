<?php
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
$sitesection = "shop";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop | DogSpot</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php require_once($DOCUMENT_ROOT.'/common/top.php'); ?>
<link href="css/shop.css" rel="stylesheet" type="text/css" />
<div id="pagebody">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
      <td width="180" valign="top">
      <div id="leftnav">
		<div id="userinfo">Categories</div>
        <?php require_once($DOCUMENT_ROOT.'/shop/common/categories.php'); ?>
        </div>
      
      </td>
        <td valign="top"><div id="inbody">
        <table width="100%" border="0" cellpadding="3" cellspacing="0">
  			<tr><td><p class="sitenavi"><a href="/">Home</a> &gt; <a href="/shop/">Shop</a> </p></td>
    			<td align="right"><div id="rssFeed"><a href="/syndication/" rel="alternate" type="application/rss+xml">Subscribe Now <img src="http://www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt="" style="vertical-align:middle;border:0"/></a></div></td>
    			<td align="right">
                <script type="text/javascript" src="http://w.sharethis.com/widget/?tabs=web%2Cpost%2Cemail&amp;charset=utf-8&amp;style=default&amp;publisher=203f1564-d8e9-4d1a-a18a-e0a64020e484"></script>
   			    <a href="https://www.dogspot.in/profile/invite-friends.php">Share this with Friends</a></td>
  		</tr>
	</table> 

<?
$qdata=query_execute("SELECT entity_id, url_key, name, price, thumbnail FROM mag_catalog_product_flat_1");
while($rowdat = mysql_fetch_array($qdata)){
		$entity_id = $rowdat["entity_id"];
		$name = $rowdat["name"];
		$price = $rowdat["price"];
		$thumbnail = $rowdat["thumbnail"];
		$url_key = $rowdat["url_key"];

?>
<div id="ShopItemBox">
<div id="ShopItemImage"><a href="/shop/<?=$url_key?>"><img src="/shop/magento/media/catalog/product<?=$thumbnail?>" alt="<?=$name?>" width="100" height="140"  border="0"  align="middle" title="<?=$name?>"/></a></div>
<div id="ShopItemDet"><a href="/shop/<?=$url_key?>"><?=$name;?></a></div>
<div id="ShopItemPriceList">Rs. <?=number_format($price);?></div>
</div>
<? }?>
<div style="clear:both"></div>

</div>


 
    </td>
        
      </tr>
    </table>
  </div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>