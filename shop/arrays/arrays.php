<?php
$AorderStatusUser = Array(
"inc" => "Incomplete",
"0" => "Paid",
"1" => "Unpaid",
"2" => "Transaction error 2"
);
$AorderStatusDogSpot = Array(
"0" => "Payment receved by DogSpot",
"1" => "Unpaid"
);

$AdeleveryStatusDogSpot = Array(
"dispatched-ready" => "Ready for Dispatch",
"undelivered" => "Undelivered",
"new" => "New",
"delivered" => "Delivered",
"pending-dispatch" => "Pending for Dispatch",
"dispatched" => "Dispatched",
"canceled" => "Cancelled",
"rto" => "RTO",
"refunded" => "Refunded",
"hcd" => "Happy calls Done"
);

$AOrderMethod = Array(
"cod" => "Cash-on-Delivery",
"UPI" => "UPI",
"dc" => "Debit Card",
"nb" => "Net Banking",
"cc" => "Credit Card",
"dd" => "Demand Draft",
"COD" => "Cash-on-Delivery",
"DC" => "Debit Card",
"NB" => "Net Banking",
"CC" => "Credit Card",
"DD" => "Demand Draft",
"PAISA"=>"ROD",
"rod"=>"ROD",
"rod"=>"ROD",
"WALLET"=>"PM",
"pm"=>"PM",
"gv"=>"Gift Voucher",
"ccb"=>"CC/DC",
"ccnb"=>"Net Banking",
"ccpaypal"=>"cc",
"AMEXZ"=>"CC",
"credit"=>"Credit",
"mobikwik"=>"MobiKwik",
"CASH"=>"CC",
"sbi"=>"SBI",
"PPI"=>"PayTm",
"ccpaytm"=>"PayTm"
);

$ACourier = Array(
"1" => "BlueDart AWB",
"2" => "Aramax",
"3" => "Self",
"4" => "Blazeflash",
"5" => "DTDC",
"6" => "Delhivery",
"7" => "BlueDart Surface",
"8" => "BlueDart Third party",
"9" => "Professional Courier",
"10" => "AFL viz",
"11" => "Aramax Third party",
"12" => "Speed Post",
"13" => "Fedex",
"14" => "Red Value(AWB)",
"15" => "Red Deal(Surface)",
"16" => "Gati Air",
"17" => "Gati Surface",
"18" => "Fedex Flat",
"19" => "Fedex Heavy ",
"23" => "BMP Services",
"24" => "BMP Surface",
"25" => "BMP Eco",
"26" => "Grofers",
"30" => "SCM",
"27" => "Amazon",
"28" => "Grofers",
"29" => "Delhivery",
"31" => "Local",
"33" => "Pickrr",
"34" => "Delhivery Surface",
"32" => "DMC"
);

$Adiscount_amount_type = Array(
"percentage" => "%",
"amount" => "Rs");

$Adelevery_status_limit = Array(
	"new" => "pending-dispatch",
	"pending-dispatch" => "dispatched-ready",
	"dispatched-ready" => "dispatched,pending-dispatch",
	
	"canceled"=>"",
	""=>"canceled,new"
);
$Adelevery_status_dispatch = Array(
	"new" => "pending-dispatch",
	"pending-dispatch" => "dispatched-ready",
	"dispatched-ready" => "dispatched,pending-dispatch",
	"dispatched" => "",
);

$Adelevery_status_limitIndore = Array(
	"new" => "pending-dispatch",
	"pending-dispatch" => "dispatched-ready",
	"dispatched-ready" => "dispatched",
	"dispatched" => "",
);
$Adelevery_status = Array(
	"new" => "New",
	"dispatched-ready" => "Dispatch Ready",
	"delivered" => "Delivered",
	"undelivered" => "Undelivered",
	"pending-dispatch" => "Pending Dispatch",
	"dispatched" => "Dispatched",
	"canceled" => "Canceled",
	"rto" => "RTO",
);

$Warehouse = Array(
	"1" => "Gurgaon",
	"2" => "Indore",
       "3" => "Banglore",

);

$aOrderTrackingURL = Array(
	"1" => "http://www.bluedart.com/servlet/RoutingServlet?handler=tnt&action=custawbquery&loginid=GGN37284&awb=awb&format=html&lickey=1cfb45518d2f09c54c5c7a67ed05ca9e&verno=1.3&scan=1&numbers="
);

?>