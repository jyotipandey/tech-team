<?
require_once($DOCUMENT_ROOT . '/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');

$maxshow = 50;
if(empty($show)) {
	$show = 0;
}else{
 $show = $show - 1;
}
 $showRecord = $show * $maxshow;
 $nextShow = $showRecord + $maxshow;
?>
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/jquery.tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery.tablesorter/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script>
$(document).ready(function() {
	  $("#addrrorder").validate({
	});
});


$(function() {
	$( "#datepicker" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});

$(function() {
	$( "#datepicker1" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
<?
if ($submit) {
	$sqlorderrr=query_execute("INSERT INTO shop_vendor_pickup (vendor_id, booking_code, courier_id, date_of_booking, date_of_pickup, status, comment, c_date) VALUES ('$vendorrid', '$booking_code', '$rpu_courier_id', '$datepicker', '$datepicker1', '$status', '$commentbox', NULL)");
}
if($update){
	$rpuupadte=query_execute("UPDATE shop_vendor_pickup SET booking_code='$booking_code', courier_id='$rpu_courier_id', date_of_booking='$datepicker', date_of_pickup='$datepicker1', status='$status', comment='$commentbox' WHERE vendor_id='$vendorid'");
	}

if($action=='edit' && $vendorid){
	$rpuedittt=query_execute("SELECT * FROM shop_vendor_pickup WHERE vendor_id='$vendorid'");
	$rpuedit = mysql_fetch_array($rpuedittt);
    $vendorid=$rpuedit['vendor_id'];
	$booking_code=$rpuedit['booking_code'];
	$rpu_courier_id=$rpuedit['courier_id'];
	$datepicker=$rpuedit['date_of_booking'];
	$datepicker1=$rpuedit['date_of_pickup'];
	$status=$rpuedit['status']; 
	$commentbox=$rpuedit['comment']; 
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vendor Pickup</title>
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<?php require_once($DOCUMENT_ROOT . '/shop/adminshop/common/top.php');?>
<h1>Book Vendor Pickup</h1> 
<form id="addrrorder" name="addrrorder" method="post">
<table border="0" width="980px" class="myClass tablesorter">   
  <thead>
  <tr>
    <th width="9%"><strong>Vendor Name</strong></th>
     <th width="9%"><strong>Booking Code</strong></th>
    <th width="9%"><strong>Courier</strong></th>
    <th width="13%"><strong>Date Of Booking</strong></th>    
    <th width="16%"><strong>Date Of Closure</strong></th>
    <th width="14%"><strong>Status</strong></th>
    <th width="16%"><strong>Comment</strong></th>
    <th width="8%"><strong>Action</strong></th>
  </tr>  </thead> <tbody>
   <tr>
  <tr>
  <td>
<input type="text" id="vendorrid" required  name="vendorrid" size="20" value="<?=$vendorid?>"/></td>
<td><input type="text" id="booking_code" required  name="booking_code" size="20" value="<?=$booking_code?>"/></td>
<td><select id="rpu_courier_id" name="rpu_courier_id" required="true" > 
<option value="">Courier</option>
    <? $couruer=query_execute("select * from shop_courier ");
	$s=1;
  		while($rowcourier = mysql_fetch_array($couruer)){
	  	$coureriid=$rowcourier['courier_id'];
	  	$courername=$rowcourier['courier_name']; 	?>    
<option value="<?= $coureriid ?>"<? if($rpu_courier_id==$coureriid) { echo "selected='selected'"; } ?>><?=$courername ?></option>
         <? } ?>  </select></td>
<td><input type="datepicker" required    id="datepicker" size="15"  name ="datepicker" value="<?=$datepicker?>"></td>
<td><input type="datepicker1"   id="datepicker1" size="15"  name ="datepicker1" value="<?=$datepicker1?>" ></td>

<td><select id="status" name="status" required="true" required="true">
<option value="">  Select  </option>
<option value="open"<? if($status=='open') { echo "selected='selected'"; } ?>>Open</option>
     <option value="close"<? if($status=='close') { echo "selected='selected'"; } ?>>Close</option>
</select></td>
<td><textarea name="commentbox" id="commentbox" cols="10" style="width:150px" rows="1"><?=$commentbox?></textarea> </td>
<td> <? if($action=='edit'){ ?>
<input name="update" type="submit" id="update" value="    Update    " /> <? }else{?>
<input name="submit" type="submit" id="submit" value="    SUBMIT    " /> <? } ?></td>
 </tr> </tbody></table>
</form>
<h2 align="center">List All Vendor Pickup</h2>
<table width="100%" border="1" cellpadding="5" cellspacing="0" id="ReportTable"  class="myClass tablesorter">
<thead><tr>
	<th width="9%"><strong>S. No</strong></th>
    <th width="10%"><strong>Vendor id</strong></th>
    <th width="10%"><strong>Booking Code</strong></th>
    <th width="10%"><strong>Courier</strong></th>
	<th width="12%"><strong>Date Of Booking</strong></th>
    <th width="12%"><strong>Date Of Closure</strong></th>
    <th width="12%"><strong>Days</strong></th>
    <th width="12%"><strong>C Date</strong></th>
    <th width="19%"><strong>Comment</strong></th>
    <th width="10%"><strong>Status</strong></th>
    <th width="10%"><strong>Action</strong></th>
     </tr></thead>
<? $sqlrpu=query_execute("SELECT * FROM shop_vendor_pickup order by c_date desc  LIMIT $showRecord, $maxshow");
while($sqltrou=mysql_fetch_array($sqlrpu)){
	$vendorid=$sqltrou['vendor_id'];
$from=$sqltrou['date_of_booking'];
$toDis=$sqltrou['date_of_pickup'];
if($toDis=='0000-00-00 00:00:00'){
	$toDis=$from; 
	$numberDays='0';}else{
 $startTimeStamp = strtotime($from, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);

$numberDays = $timeDiff/86400;  // 86400 seconds in one day

// and you might want to convert to integer
$numberDays = intval($numberDays);
	}
?>
<tr>
<td><?=$s;?></td>
<td><?=$sqltrou['vendor_id'];?></td>
<td><?=$sqltrou['booking_code'];?></td>
<td><? $rpucorier=query_execute_row("SELECT * FROM shop_courier WHERE courier_id = '".$sqltrou['courier_id']."'");
 echo $rpucorier['courier_name'];?></td>
<td><?=$sqltrou['date_of_booking'];?></td>
<td><?=$sqltrou['date_of_pickup'];?></td>
<td><?=$numberDays;?></td>
<td><?=$sqltrou['c_date'];?></td>
<td><?=$sqltrou['comment'];?></td>
<td><? if($sqltrou['date_of_pickup']=='0000-00-00 00:00:00'){
	echo "Close";}
	if($sqltrou['date_of_pickup']!='0000-00-00 00:00:00'){ echo "Open";}?></td>
    <td><? if($sqltrou['date_of_pickup']!='0000-00-00 00:00:00'){ ?>
		<a href='/shop/adminshop/vendor-pickup.php?action=edit&&vendorid=<?=$vendorid?>'>Edit</a><? }?></td>
<? $s++; } ?>
</tr>
</table> 
<div style="clear:both"></div>
<hr />
<div class="showPages">
<? 
$pageUrl="/shop/adminshop/vendor-pickup.php?filter=$filter&page=p";
showPagesLink($totrecord, $maxshow, $pageUrl, $show, $nextShow, $previous, $spUrl);

?>
</div>  
<div style="clear:both"></div>
