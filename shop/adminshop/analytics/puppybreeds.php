<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/supplier.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Puppy Breeds</title>

<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/jquery.tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery.tablesorter/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>

<script>
$(document).ready(function(){
	$("#ReportTable").tablesorter();
}); 
</script>
</head>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<body>
 <form action="/shop/adminshop/analytics/exporttoexcel.php" method="post"   
    onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'> 
    <input type="hidden" id="datatodisplay" name="datatodisplay">  
            <input type="submit" value="Export to Excel">
         
<table width="100%" border="1" cellpadding="5" cellspacing="0" id="ReportTable" class="myClass tablesorter">
<thead>
  <tr>
  <th width="28"><strong>S.NO.</strong></th>
     <th width="28"><strong>Dog Breed</strong></th>

  </tr>
  </thead>
<tbody>
<?
 $s=1;
	
$selectorder=query_execute("SELECT DISTINCT(puppi_breed) FROM puppies_available");
while($rowMyOrders = mysql_fetch_array($selectorder)){	
$puppibreed=$rowMyOrders['puppi_breed'];	
?>  
  <tr>
    <td><?= $s ?></td>
    <td><?=$puppibreed;?></td>

</tr>
<? 
$s=$s+1;
} ?>

</tbody>
</table>
</form>

</body>
</html>
