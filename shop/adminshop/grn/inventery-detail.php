<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/supplier.php');

if($datepicker ){
			
		$from = "$datepicker";
		$to = date('Y-m-d', strtotime(date("Y-m-d", strtotime(trim($datepicker1))) . '+1 day'));
		$toDis = "$datepicker1";
			
		$searchsql="WHERE create_date BETWEEN '$from' AND '$to'";
		$qGetMyCart=query_execute("SELECT grn_id FROM shop_supplier_detail $searchsql ORDER BY grn_id DESC");
}
else {
	
		$from = date('Y-m-d', strtotime('-30 days'));
		$to = date('Y-m-d',strtotime('+1 day'));
		$toDis = date('Y-m-d');
		$searchsql="WHERE create_date BETWEEN '$from' AND '$to'";
		$qGetMyCart=query_execute("SELECT grn_id FROM shop_supplier_detail $searchsql ORDER BY grn_id DESC");
}
while($rowItemName = mysql_fetch_array($qGetMyCart)){
		$grnid12[]=$rowItemName["grn_id"].', ';		
	}
	//print_r($grnid12);
	
	$grnid=array_unique($grnid12);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventery Detail</title>


<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/jquery.tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery.tablesorter/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>
<script>
$(document).ready(function(){
	$("#ReportTable").tablesorter();
}); 

$(function() {
	$( "#datepicker" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});

$(function() {
	$( "#datepicker1" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
</head>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>

<body>
<br />
<form id="formOrderSearch" name="formOrderSearch" method="post" action="<?php echo $PHP_SELF; ?>">
<table width="100%" cellspacing="0" cellpadding="2" style="text-align:center; border-color:#999; border-width:1px; border-style:solid; background-color:#edeaea">
  <tr>
  <td width="35%">
   <p> <strong>From:</strong>
     <input type="datepicker" id="datepicker" name ="datepicker"  value="<?=$from?>"  >   
        
     </p>
    
</td> 
<td width="35%">
   <p> <strong>To:</strong>
    
     <input type="datepicker" id="datepicker1" name ="datepicker1"  value="<?=$to?>"  >   
     </p>
    
</td> 
  </tr>
</table>
 
<div style="text-align:center; margin-top:5px;"><input type="submit" name="searchOrder" id="searchOrder" value="                    Search                 "  /></div>
<div style=" margin-top:5px;"> </form>


<p>
 Showing results from <strong><?=showdate($from, "d M o");?></strong> to  <strong><?=showdate($toDis, "d M o");?></strong>
 <form action="/shop/adminshop/analytics/exporttoexcel.php" method="post"   
    onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'> 
    <input type="hidden" id="datatodisplay" name="datatodisplay">  
            <input type="submit" value="Export to Excel">
         
<table width="100%" border="1" cellpadding="5" cellspacing="0" id="ReportTable" class="myClass tablesorter">
<thead>
  <tr>
  <th width="6%"><strong>GRN_ID</strong></th>
  <th width="6%"><strong>Item_ID</strong></th>
    <th width="6%"><strong>Supplier_Item_Code</strong></th>
    <th width="9%"><strong>Item_Location_ID</strong></th>
	<th width="10%"><strong>Name</strong></th>
    <th width="5%"><strong>Price</strong></th>
    <th width="6%"><strong>Buying Price</strong></th>
    <th width="6%"><strong>Visibility</strong></th>
    <th width="6%"><strong>Qty</strong></th>
    <th width="6%"><strong>Actual Qty</strong></th>
    <th width="6%"><strong>Stock Status</strong></th>
    <th width="6%"><strong>Item Brand</strong></th>
    <th width="6%"><strong>Item Supplier</strong></th>
    <th width="7%"><strong>Item Status</strong></th>
    </tr>
  </thead>
<tbody>
<?
$toalqty=0;$totalbuying=0;$totalselling=0;
foreach($grnid as $grn) {
 $get_det  = query_execute("SELECT * FROM shop_supplier_detail WHERE grn_id='$grn'  ");	
 $fgd=explode(",",$grn);
while($get_details=mysql_fetch_array($get_det)){
		$supplier_detail_id = $get_details["supplier_detail_id"];
        $item=$get_details["item_id"];?>
  
  <tr>
   <td>
   <a href="/shop/adminshop/grn/shop-supplier_search.php?grn_id=<?= $fgd[0] ?>" target="_blank"><font color="#0000FF">
   <?=$fgd[0];?>
   </font></td>
    <td><?=$item;?></td>
    <td><?=$supplier_detail_id?></td>
    <? $qItemName = query_execute_row("SELECT * FROM shop_items WHERE item_id='$item'");?>
     <td><? if ($qItemName["item_location_id"]=="") { echo "-"; } else { echo $qItemName["item_location_id"]; }?></td>
     <td><?= $qItemName["name"];?></td>
    <td><?=$qItemName['price']; ?></td>
    <td><?=$qItemName['buying_price']; ?></td>
    <td><?=$qItemName['visibility']; ?></td>
    <td><?= $qItemName['qty'];?></td>
      <td></td>
      <td><?= $qItemName['stock_status'];?></td>
        <td><?= $ArrayShopBrands[$qItemName['item_brand']];?></td>
        <td><?= $ArrayShopSupplier[$qItemName['item_supplier']];?></td>
        <td><?= $qItemName['item_status'];?></td>
     </tr> 
  <? } } ?>
</tbody>
</table>

</form>
</body>
</html>