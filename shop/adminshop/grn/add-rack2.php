<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT . '/shop/adminshop/grn/functions.php');
require_once($DOCUMENT_ROOT.'/shop/adminshop/arrays/shop-categorys.php');
$ip = ipCheck();
$rack=query_execute_row("SELECT rack_name FROM shop_item_rack ORDER BY rack_id DESC");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Rack | Shop DogSpot.in</title>

<script src="/js/formchanges.js"></script>

<script type='text/javascript' src='/js/swfobject.js'></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type='text/javascript' src='/js/shaajax2.js'></script>

<script language="javascript">

// show changed messages
function DetectChanges() {
	
	var f = FormChanges(formItem), msg = "";
	var itemid=document.getElementById('item_id').value;
	for (var e = 0, el = f.length; e < el; e++) msg += "@#" + f[e].id +",#" + f[e].value;
	var changed= (msg ? "Elements changed:" : "no") + msg;
		if(changed!='no'){			
		msg = msg.replace(/#/g, "");
		document.getElementById('item_change').value=msg;
			
//ShaAjaxJquary('listupdateitem.php?changeitem='+msg+'&itemid2='+itemid+'', '#mis12', '', 'formItem', 'post', '', '', 'REP'); 
			}
}
</script>
<link href="/css/login-style.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
function zone_type2()
{
	var type=document.getElementById('type3').value;
	var zone_id4=type.split('@@');
	if(type!='0')
	{
	document.getElementById('zone_type1').value=zone_id4[0];
	document.getElementById('min').value=zone_id4[2];
	document.getElementById('max').value=zone_id4[3];
	document.getElementById('zoneid').value=zone_id4[4];

	}
	else
	{
	document.getElementById('tot_loaction').style.display='none';
	document.getElementById('rack').style.display='none';
	document.getElementById('zone').style.display='none';
	}
	}
</script>
<script type="text/javascript">
 $(document).ready(function() {
	  $("#formItem").validate({
		  
	});
	
});
</script>

<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body> <div id="mis12"></div>
<div id="shopAdminBox">
<h1>Add / Edit Rack Details</h1> 
<form id="formrack" name="formrack" method="post">

<table width="100%" border="0" cellpadding="4" cellspacing="0" class="tableBorder">
  <tr>
    <td align="right"><span class="error">*</span>Select Zone Name:</td>
    <td style="width:100px"><select id="type3" name="type3"  class="required" onchange="return zone_type2();">
      <option value='0'>Select Zone</option>
        <?php
			$query_domain2 = query_execute("SELECT distinct(zone_type) as type,zone_name,min_weight,max_weight,zone_id FROM shop_item_zone");
		
			while($query_domain3 = mysql_fetch_array($query_domain2)){
			$type4 = $query_domain3["type"];
			$zone_name4=$query_domain3["zone_name"];
			$min_weight4=$query_domain3["min_weight"];
			$max_weight4=$query_domain3["max_weight"];
			$zone_id4=$query_domain3["zone_id"];
			  print "<option value='$type4@@$zone_nam4e@@$min_weight4@@$max_weight4@@$zone_id4'";
			  print ">$zone_name4</option>";
			 }
		  ?>
    </select>&nbsp;&nbsp;<div style="display:none"  id="domain_error"></div></td>
    </tr>
  <tr>
    <td align="right"><span class="error">*</span>Zone Type:</td>
    <td><input id="zone_type1" name="zone_type1" type="text"  value="" class="required textWrite"/></td>
  </tr>
  <tr>
    <td width="150" align="right"><span class="error">*</span>Min Volumetric weight:</td>
    <td><input id="min" name="min" type="text"  value="" class="required textWrite"/></td>
  </tr>
  <tr></tr>
  <tr>
    <td width="150" align="right"><span class="error">*</span>Max Volumetric weight:</td>
    <td><input id="max" name="max" type="text"  value="" class="required textWrite"/></td>
  </tr>
  <tr><td>
    <input type="submit" name="save1" id="save1" value="Save" />
        </td>
  </tr>
    <tr><td>
    <input type="hidden" name="zoneid" id="zoneid" value="" />
        </td>
  </tr>
  
</table>

<div id="location12" align="center" style="overflow:auto;width:600px;height:400px;"></div>
</form>
</div>

</body>
</html>