<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');

if($update){
	
	$qupdate=query_execute("UPDATE shop_items SET type_id = '$type_id', item_parent_id = '$item_parent_id', name = '$name', item_about = '$item_about', title = '$title', keyword = '$keyword', description = '$description', price = '$price', selling_price = '$selling_price', buying_price = '$buying_price', short_description = '$short_description', sku = '$sku', visibility = '$visibility', weight = '$weight', updated_at = NULL, qty = '$qty', stock_status = '$stock_status', item_brand = '$item_brand', item_supplier = '$item_supplier' WHERE item_id = '$item_id'");
	
	// Category----------------------------------------------------------
	$delCat=query_execute("DELETE FROM shop_item_category WHERE item_id = '$item_id'");
	$qCat=query_execute("INSERT INTO shop_item_category (category_id, item_id) 
										VALUES ('$category_id', '$item_id')");
	// Category-------------------------------------------------------------
	// Item attribute------------------------------------------------------
	$delAtt=query_execute("DELETE FROM shop_item_attribute WHERE item_id = '$item_id'");
	if($shop_item_attribute){
		$numAtt=count($shop_item_attribute);
		for($i=0;$i<$numAtt;$i++){
			$ite_att=explode('-',$shop_item_attribute[$i]);
			$qAtt=query_execute("INSERT INTO shop_item_attribute (item_id, attribute_set_id, attribute_value_id, attribute_value) 
										VALUES ('$item_id', '$ite_att[0]','$ite_att[1]','$attribute_value[$i]')");
		}
	}
	// Item attribute------------------------------------------------------
	// Item Media------------------------------------------------------
	$delMed=query_execute("DELETE FROM shop_item_media WHERE item_id = '$item_id'");
	if($pac_image){
		$numImg=count($pac_image);
		for($p=0;$p<$numImg;$p++){
			$qImg=query_execute("INSERT INTO shop_item_media (item_id, media_type, media_file, label, position) 
										VALUES ('$item_id', 'image', '$pac_image[$p]', '$image_title[$p]', '0')");
		}
	}
	// Item Media------------------------------------------------------	
	header("Location: /shop/admin-shop/list-items.php");
	exit();										
}

if($save){
	if(!$nice_name){
		$nice_name = createSlug($name);
		$nice_name = checkSlugAll('shop_items', 'nice_name', $nice_name);
	}else{
		$nice_name = checkSlugAll('shop_items', 'nice_name', $nice_name);
	}
	$qItem=query_execute("INSERT INTO shop_items (type_id, item_parent_id, name, nice_name, item_about, title, keyword, description, price, selling_price, buying_price, short_description, sku, visibility, weight, created_at, updated_at, qty, stock_status, item_brand, item_supplier) 
										VALUES ('$type_id', '$item_parent_id', '$name', '$nice_name', '$item_about', '$title', '$keyword', '$description', '$price', '$selling_price', '$buying_price', '$short_description', '$sku', '$visibility', '$weight', NULL, NULL, '$qty', '$stock_status', '$item_brand', '$item_supplier')");
	$newitem_id = mysql_insert_id();
	
	$qCat=query_execute("INSERT INTO shop_item_category (category_id, item_id) 
										VALUES ('$category_id', '$newitem_id')");
	
	if($shop_item_attribute){
		$numAtt=count($shop_item_attribute);
		for($i=0;$i<$numAtt;$i++){
			$ite_att=explode('-',$shop_item_attribute[$i]);
			if($ite_att[0]){
			$qAtt=query_execute("INSERT INTO shop_item_attribute (item_id, attribute_set_id, attribute_value_id, attribute_value) 
										VALUES ('$newitem_id', '$ite_att[0]','$ite_att[1]','$attribute_value[$i]')");
			}
		}
	}
	
	if($pac_image){
		$numImg=count($pac_image);
		for($p=0;$p<$numImg;$p++){
			$qImg=query_execute("INSERT INTO shop_item_media (item_id, media_type, media_file, label, position) 
										VALUES ('$newitem_id', 'image', '$pac_image[$p]', '$image_title[$p]', '0')");
		}
	}
	header("Location: /shop/$nice_name/");
	exit();
}

if($item_id && $action=='edit'){
	$qItems=query_execute("SELECT * FROM shop_items WHERE item_id='$item_id'");
	$rowItem = mysql_fetch_array($qItems);
	$type_id = $rowItem["type_id"];
	$item_parent_id = $rowItem["item_parent_id"];
	$name = $rowItem["name"];
	$nice_name = $rowItem["nice_name"];
	$item_about = $rowItem["item_about"];
	$title = $rowItem["title"];
	$keyword = $rowItem["keyword"];
	$description = $rowItem["description"];
	$price = $rowItem["price"];
	$selling_price = $rowItem["selling_price"];
	$buying_price = $rowItem["buying_price"];
	$sku = $rowItem["sku"];
	$visibility = $rowItem["visibility"];
	$weight = $rowItem["weight"];
	$qty = $rowItem["qty"];
	$stock_status = $rowItem["stock_status"];
	$short_description = $rowItem["short_description"];
	$item_brand = $rowItem["item_brand"];
	$item_supplier = $rowItem["item_supplier"];
	
	//Get category
	$qCat=query_execute("SELECT category_id FROM shop_item_category WHERE item_id='$item_id'");
	$rowCat = mysql_fetch_array($qCat);
	$category_id = $rowCat["category_id"];
	// END
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add / Edit Item | Shop DogSpot.in</title>

<script type='text/javascript' src='/js/swfobject.js'></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script type="text/javascript">
 $(document).ready(function() {
	  $("#formItem").validate({
	});
});

<!--new Photo Upload-->
function loadProgress(pro){
	document.getElementById('DivProgress').innerHTML = pro;
}
function photoloading(msg){
	document.getElementById('loading').innerHTML = msg;
}
function selectPhotos(){					
	document.getElementById('player_mc').uploadpics();	
}
function upload(){					
	document.getElementById('player_mc').uploadpics();	
}
function loading(){
  document.getElementById('loading').innerHTML = "<img src='/images/loading.gif' />";
}
function alldone(){
   document.getElementById('loading').innerHTML = "";
}
function showpics(imageName){
	var ext = imageName.substring(imageName.lastIndexOf(".")+1);
	var fName=imageName.substring(0, imageName.indexOf("."+ext));
	ShaAjaxJquary("upload_resize.php?imgName="+imageName+"", "#imgpic", "#"+fName, '', 'GET', 'loading', '<img src="/images/loading-round.gif" />','APE');
	return false;
}
<!--new Photo Upload END-->
function removeElement(childDiv, imagename){
	ShaAjaxJquary("removephoto.php?rmImage="+imagename+"&childDiv="+childDiv+"", "#imgpic", '', '', 'GET', 'loading', '<img src="/images/loading-round.gif" />','REP');
}
</script>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="shopAdminBox">
<h1>Add / Edit Item Details</h1>
<form id="formItem" name="formItem" method="post" action="add-item.php">

<table width="100%" border="0" cellpadding="4" cellspacing="0" class="tableBorder">
  <tr>
    <td align="right"><span class="error">*</span>Item Name:</td>
    <td><input name="name" type="text" id="name" value="<?=$name?>" class="required textWrite"/></td>
  </tr>
  <tr>
    <td width="150" align="right"><span class="error">*</span>Item Type:</td>
    <td><select name="type_id" id="type_id" class="required">
    <option value="">Please Select</option>
      <option value="simple" <? if($type_id=='simple'){echo'selected="selected"';}?>>Simple</option>
      <option value="configurable" <? if($type_id=='configurable'){echo'selected="selected"';}?>>configurable</option>
    </select></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Item Parent:</td>
    <td><select name="item_parent_id" id="item_parent_id" class="required">
      <option value="0">Root</option>
    <?
	$qItemParent=query_execute("SELECT item_id, name FROM shop_items WHERE item_parent_id = '0'");
	while($rowItemParent = mysql_fetch_array($qItemParent)){
		?>
		<option value="<?=$rowItemParent["item_id"]?>" <? if($item_parent_id==$rowItemParent["item_id"]){echo'selected="selected"';}?>><?=$rowItemParent["name"]?></option>
		<?
	}
	?>
    
    </select></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Item Category: </td>
    <td><select name="category_id" id="category_id" class="required">
      <option value="">Please Select</option>
      <?
	  foreach($ArrayShopCategorys as $cat_id => $catValue){
	  ?>
      <option value="<?=$cat_id?>" <? if($category_id==$cat_id){echo'selected="selected"';}?>><?=$catValue?></option>
      <? }?>
    </select></td>
  </tr>
  <? if($action!='edit'){?>
  <tr>
    <td align="right">Item URL Name:</td>
    <td><input name="nice_name" type="text" class="textWrite" id="nice_name" value="<?=$nice_name?>" /></td>
  </tr>
  <? }?>
  <tr>
    <td align="right"><span class="error">*</span>Short Description:</td>
    <td><textarea name="short_description" class="required textWrite" id="short_description"><?=$short_description?></textarea></td>
  </tr>
  <tr>
    <td align="right">About: </td>
    <td><textarea name="item_about" id="item_about" cols="45" rows="5" class="required textWrite ckeditor"><?=$item_about?></textarea></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Meta Title:</td>
    <td><input name="title" type="text" class="textWrite required" id="title" value="<?=$title?>"/></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Meta Keyword:</td>
    <td><textarea name="keyword" class="textWrite required" id="keyword"><?=$keyword?></textarea></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Meta Description:</td>
    <td><textarea name="description" class="textWrite required" id="description" ><?=$description?></textarea></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>MRP:</td>
    <td><input name="price" type="text" id="price" value="<?=$price?>" class="required"/></td>
  </tr>
  <tr>
    <td align="right">DogSpot Selling Price: </td>
    <td><input name="selling_price" type="text" id="selling_price" value="<?=$selling_price?>" class="required"/></td>
  </tr>
  <tr>
    <td align="right">DogSpot Buying Price: </td>
    <td><input name="buying_price" type="text" id="buying_price" value="<?=$buying_price?>" class="required"/></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Sku:</td>
    <td><input name="sku" type="text" class="textWrite required" id="sku" value="<?=$sku?>" /></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Visibility:</td>
    <td><select name="visibility" id="visibility" class="required">
      <option value="">Please Select</option>
      <option value="visible" <? if($visibility=='visible'){echo'selected="selected"';}?>>Visible</option>
      <option value="invisible" <? if($visibility=='invisible'){echo'selected="selected"';}?>>In Visible</option>
    </select></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Weight:</td>
    <td><input name="weight" type="text" id="weight" value="<?=$weight?>" class="required"/></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Quentity:</td>
    <td><input name="qty" type="text" id="qty" value="<?=$qty?>" class="required"/></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>in Stock:</td>
    <td><select name="stock_status" id="stock_status" class="required">
      <option value="">Please Select</option>
      <option value="instock" <? if($stock_status=='instock'){echo'selected="selected"';}?>>In Stock</option>
      <option value="outofstock" <? if($stock_status=='outofstock'){echo'selected="selected"';}?>>Out of Stock</option>
    </select></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Brand Name:</td>
    <td><select name="item_brand" id="item_brand" class="required">
      <option value="">Please Select</option>
      <?
	$qItemBrand=query_execute("SELECT brand_id, brand_name FROM shop_brand ORDER BY brand_name ASC");
	while($rowItemBrand = mysql_fetch_array($qItemBrand)){
		?>
		<option value="<?=$rowItemBrand["brand_id"]?>" <? if($item_brand==$rowItemBrand["brand_id"]){echo'selected="selected"';}?>><?=$rowItemBrand["brand_name"]?></option>
		<?
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td align="right"><span class="error">*</span>Supplier: </td>
    <td><select name="item_supplier" id="item_supplier" class="required">
      <option value="">Please Select</option>
      <?
	$qItemSuply=query_execute("SELECT supplier_id, supplier_name FROM shop_supplier ORDER BY supplier_name ASC");
	while($rowItemSuply = mysql_fetch_array($qItemSuply)){
		?>
		<option value="<?=$rowItemSuply["supplier_id"]?>" <? if($item_supplier==$rowItemSuply["supplier_id"]){echo'selected="selected"';}?>><?=$rowItemSuply["supplier_name"]?></option>
		<?
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td align="right">Attribute: </td>
    <td><input name="ifatt" type="checkbox" id="ifatt" value="1" />
      <br />
      <table width="500" border="0" cellpadding="3" cellspacing="0">
      <? 
	  $qAtSet=query_execute("SELECT * FROM shop_attribute_set");
		while($rowAtSet = mysql_fetch_array($qAtSet)){
			$attribute_set_id=$rowAtSet["attribute_set_id"];
	  ?>
      <tr>
        <td><?=$rowAtSet["attribute_set_name"]?>: </td>
        <td><select name="shop_item_attribute[]" id="shop_item_attribute">
        <option value="">Please Select</option>
        <?
		$qAtSeValt=query_execute("SELECT * FROM shop_attribute_value WHERE attribute_set_id='$attribute_set_id'");
		while($rowAtSetVal = mysql_fetch_array($qAtSeValt)){
			$qAtt=query_execute("SELECT attribute_value_id, attribute_value FROM shop_item_attribute WHERE item_id='$item_id' AND attribute_set_id='$attribute_set_id'");
			$rowAtt = mysql_fetch_array($qAtt);
			$attribute_value=$rowAtt["attribute_value"];
		?>
        <option value="<?=$attribute_set_id.'-'.$rowAtSetVal["attribute_value_id"]?>" <? if($rowAtt["attribute_value_id"]==$rowAtSetVal["attribute_value_id"]){echo'selected="selected"';}?>><?=$rowAtSetVal["attribute_value_name"]?></option>
        <? }?>
        </select></td>
        <td><input name="attribute_value[]" type="text" id="attribute_value" value="<?=$attribute_value?>" /></td>
      </tr>
      <? }?>
    </table></td>
  </tr>
  <tr>
    <td align="right">Photo:</td>
    <td>
    <div class="DivButGrayPhoto" id="flashupbut"></div>
    <div id='loading' style="height:20px; float: left;"></div>
    <div id='DivProgress' style="height:20px; float: left;"></div>(optional)
    <div id="clearall" style="margin-top:5px;"></div>
	<div id='imgpic' style="padding:3px;">
    <? 
$SqlPhotos = query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id = '$item_id'");
$All_photos_rows = mysql_num_rows($SqlPhotos);
 
if($All_photos_rows != 0){
 while($RowPhotos = mysql_fetch_array($SqlPhotos)){
	 
   $image_title = $RowPhotos['label'];
   $imgName = $RowPhotos['media_file']; //use in showimg-edit.php
	include('upload_resize.php');
 }
}

?> 
    </div>
    </td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>
    <? if($action=='edit'){?>
    <input type="submit" name="update" id="update" value="Update Item Details" />
    <input name="item_id" type="hidden" id="item_id" value="<?=$item_id?>" />
<? }else{?>
    <input type="submit" name="save" id="save" value="Upload" />
    <? }?>
    </td>
  </tr>
</table>
</form>
</div>
<script type="text/javascript">
var so = new SWFObject("mupload2.swf", "player_mc", "60", "14", "8", "#336699");
   so.addParam("quality", "high");
   so.addParam("wmode", "transparent");
   so.addParam("pluginurl", "http://www.macromedia.com/go/getflashplayer");
   so.addParam("pluginspage", "http://www.macromedia.com/go/getflashplayer");
   so.addParam("salign", "t");
   so.write("flashupbut");
swfobject.registerObject("FlashID");
</script>
</body>
</html>