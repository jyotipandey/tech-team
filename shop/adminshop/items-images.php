<?php
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/supplier.php');

$shop_items = mysql_query("SELECT item_id,name,item_parent_id from shop_items");
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Items Images</title>
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>

<script>
$(function() {
	$( "#datepicker" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});

});

$(function() {
	$( "#datepicker1" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
<style>
h1{
	padding-top: 0px;
	padding-left: 0px;
	padding-bottom: 0px;
	padding-right: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 0px;
}
body{
	font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
	font-style: normal;
	font-size: 10px;
}
</style>
</head>

<body>
<h1>Items Images</h1>

<hr>
<form action="/shop/adminshop/analytics/exporttoexcel.php" method="post"   
    onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable0").eq(0).clone() ).html() )'> 
    <input type="hidden" id="datatodisplay" name="datatodisplay">     
    <input type="submit" value="Export to Excel">
<table width="100%" border="1" cellpadding="0" cellspacing="0" id="ReportTable0" class="showhide" >
  <tr style="font-weight:bold">
  	<td>S no</td>
    <td>Item ID</td>
    <td>Item Name</td>
    <td>Image</td>   
  </tr>
  <?
  $sno = 1;
  	while($images = mysql_fetch_array($shop_items)){
		$new_item_id = '';
				
		$fetch_images = query_execute_row("SELECT position,media_file FROM shop_item_media WHERE item_id='".$images['item_id']."' ORDER BY position ASC");
		
		if($fetch_images['media_file'] == ''){			
			$new_item_id = $images['item_parent_id'];
			$fetch_new_images = query_execute_row("SELECT position,media_file FROM shop_item_media WHERE  item_id='$new_item_id' ORDER BY position ASC");
			$new_image = $fetch_new_images['media_file'];
		}
		else{
			$new_image = $fetch_images['media_file'];
		}		
		?>
        <tr>
        	<td><?=$sno?></td>
        	<td><?=$images['item_id']?></td>
            <td><?=$images['name']?></td>
            <td><a href="http://www.dogspot.in/shop/item-images/orignal/<?=$new_image?>" target="_blank">http://www.dogspot.in/shop/item-images/orignal/<?=$new_image?></a></td>            
        </tr>        
        <?
		$sno++;	
	}
  ?>  
</table>
</form>

</form>
</body>
</html>
