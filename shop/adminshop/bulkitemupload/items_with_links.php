<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/supplier.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
if($datepicker && $datepicker1){
$from =$datepicker;
$to = $datepicker1;
$searchSql="AND order_c_date BETWEEN '$from' AND '$to'";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items With Links</title>

<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/jquery.tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery.tablesorter/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>

<script>
$(document).ready(function(){
	$("#ReportTable").tablesorter();
});

$(function() {
	$( "#datepicker" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});

$(function() {
	$( "#datepicker1" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
</head>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<body>

<h1>Items With Links</h1>
<form name="formfile" id="formfile" enctype="multipart/form-data" method="post" action="/shop/adminshop/bulkitemupload/items_with_links.php">
<input type="file" name="xls_file" id="xls_file" />
<div style="text-align:center; margin-top:5px;"><input type="submit" name="searchOrder" id="searchOrder" value="                    Search                 " /></div>
</form>
<form action="/shop/adminshop/analytics/exporttoexcel.php" method="post"   
    onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'> 
    <input type="hidden" id="datatodisplay" name="datatodisplay">  
            <input type="submit" value="Export to Excel">
<table width="100%" border="1" cellpadding="5" cellspacing="0" id="ReportTable" class="myClass tablesorter">
<thead>
  <tr>
    <th width="28"><strong>S.No</strong></th>
    <th width="69"><strong>Item ID</strong></th>
    <th width="69"><strong>Link</strong></th>

  </tr>
  </thead>
<?
$s=1;
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$item_id=$data->sheets[0]['cells'][$j+1][1];
/*$att_id=$data->sheets[0]['cells'][$j+1][2];
$value=$data->sheets[0]['cells'][$j+1][3];
$selling=$data->sheets[0]['cells'][$j+1][3];
$buying=$data->sheets[0]['cells'][$j+1][4];*/
//$lifestage=$data->sheets[0]['cells'][$j+1][5];

if($item_id){

	$selectlink=query_execute_row("SELECT nice_name FROM shop_items WHERE item_id = '$item_id'");
	$link=$selectlink["nice_name"];
?>
  <tr>
   <td><?=$s; ?></td>
    <td><?= $item_id; ?></td>
    <td>https://www.dogspot.in/<?= $link; ?>/</td>

 </tr>

	<? }
	$s++;
	
//echo $orderid."<br>";
?>

<?
}
/*$item_id='';
$item_cat='';
$breed_id='';
$breed_type='';
$emailid='';
$att_id='';*/
$emailid='';

}
?>
</table>
</form>

</body>
</html>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php'); ?>