<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Insert AWB For Courier</title>
</head>

<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>

<h1>Bulk Upload for Stock Status</h1>

<h3> 1st Column should be Item ID </h3>
<h3> 2nd Column should be the Display Order </h3>
<h3> Save your file in ".txt" format (or if you are using MS Excel then save in 'Text(Tab delimited)' format)</h3>
<br />
<h4> Table should be like this </h4>
<table width="200" border="1">
  <tr>
    <td>item_id</td>
    <td>display_order</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br />

<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font><input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>

</form>
<hr />
<table width="728" border="1" cellpadding="3" cellspacing="0">  
  <tr>
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
				$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
				//$data = new Spreadsheet_Excel_Reader();
	
				//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
				$arr_location_id = "";
				for($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
					$item_id = $data->sheets[0]['cells'][$j+1][1];
		
					$display_order = $data->sheets[0]['cells'][$j+1][2];
				if($item_id && $display_order){
		echo "UPDATE shop_items SET item_display_order='$display_order' WHERE item_id='$item_id'";
		$update_order = query_execute("UPDATE shop_items SET item_display_order='$display_order' WHERE item_id='$item_id'");
		$display_order='';
		$item_id='';
		}

?>

<?
    
}
	
}?>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php'); ?>

</table>