<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Insert AWB For Courier</title>
</head>

<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>

<h1>Bulk Upload for Breed Finder product</h1>

<h3> 1st Column should be Item ID </h3>
<h3> 2nd Column should be Breed Name </h3>
<h3> 3rd Column should be Age Min </h3>
<h3> 4th Column should be Age Max </h3>
<h3> 5th Column should be Weight Min </h3>
<h3> 6th Column should be Weight Max </h3>
<h3> Save your file in ".txt" format (or if you are using MS Excel then save in 'Text(Tab delimited)' format)</h3>
<br />
<h4> Table should be like this </h4>
<table width="200" border="1">
  <tr>
    <td>Item id</td>
    <td>Breed</td>
     <td>Age min</td>
    <td>Age max</td>
     <td>Weight min</td>
    <td>Weight max</td>
  </tr>
 
</table>
<br />

<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font><input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>

</form>
<hr />
<table width="728" border="1" cellpadding="3" cellspacing="0">  
  <tr>
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
				$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
				//$data = new Spreadsheet_Excel_Reader();
	
				//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
				$arr_location_id = "";
				for($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
					$item_id = $data->sheets[0]['cells'][$j+1][1];
		            $breed = $data->sheets[0]['cells'][$j+1][2];
					$agemin = $data->sheets[0]['cells'][$j+1][3];
		            $agemax = $data->sheets[0]['cells'][$j+1][4];
					$weightmin = $data->sheets[0]['cells'][$j+1][5];
		            $weightmax = $data->sheets[0]['cells'][$j+1][6];
		if($item_id){
		if($breed){	
		$selectBreed=query_execute_row("SELECT breed_id FROM dog_breeds WHERE breed_name LIKE '%$breed%'");
		$breed_id=$selectBreed['breed_id'];
		if($breed_id){
		$update_order = query_execute("INSERT INTO `mapping_subscription`(`breed_id`, `age_min`, `weight_min`, `item_id`, `age_max`, `weight_max`) VALUES ('$breed_id','$agemin','$agemax','$weightmin','$weightmax')");
		$msg='Insert Successfully';
		}else{
			$msg='Breed Not found';
		}
		}else
		{
		$msg='Insert Successfully';
		$update_order = query_execute("INSERT INTO `mapping_subscription`(`breed_id`, `age_min`, `weight_min`, `item_id`, `age_max`, `weight_max`) VALUES ('$breed_id','$agemin','$agemax','$weightmin','$weightmax')");	
		}
		}

?>
<td><?=$item_id?></td>
<td><?=$breed?></td>
<td><?=$agemin?></td>
<td><?=$agemax?></td>
<td><?=$weightmin?></td>
<td><?=$weightmax?></td>
<td><?=$msg?></td>

<?
    
}
$item_id='';
$breed='';
$agemin='';
$agemax='';
$weightmin='';
$weightmax='';
	
}?>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php'); ?>

</table>