<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PinCode Delete</title>
</head>

<body>
<h1>PinCode Delete</h1>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" />

</form>
<hr />
<table border="1" cellpadding="3" cellspacing="0" style="width:100%">  
<tr valign="top">
    <td>supplier_item_code</td>
    <td>domain_id</td>
    <td>type_id</td>
    <td>item_parent_id</td>
    <td>item_location_id</td>
    <td>name</td>
    <td>item_about</td>
    <td>nice_name</td>
    <td>title</td>
    <td>keyword</td>
    <td>description</td>
    <td>price</td>
    <td>selling_price</td>
    <td>buying_price</td>
    <td>item_shipping_amount</td>
    <td>price_type</td>
    <td>tax_vat</td>
    <td>tax_added_type</td>
    <td>tax_added</td>
    <td>tax_inharyana</td>
    
    <td>short_description</td>
    <td>sku</td>
    <td>thumbnail</td>
    <td>thumbnail_label</td>
    <td>visibility</td>
    <td>weight</td>
    <td>weight_price</td>
    <td>weight_type</td>
    <td>created_at</td>
    <td>updated_at</td>
    <td>qty</td>
    <td>virtual_qty</td>
    <td>stock_status</td>
    <td>master_cat</td>
    <td>child_cat</td>
    <td>item_brand</td>
    <td>item_supplier</td>
    <td>num_views</td>
    <td>discount</td>
    
    <td>item_display_status</td>
    <td>item_status</td>
    <td>item_display_order</td>
    <td>payment_mode</td>
    
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
	?>
<?	
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$itemid=$data->sheets[0]['cells'][$j+1][1];
//$domain_id=$data->sheets[0]['cells'][$j+1][2];

//$ship_charge="100";
if($itemid){
	//-------------------------Update Item------------------------------------------------------------------------------------	

$updt_query=query_execute_row("SELECT  * FROM shop_items  WHERE item_id='$itemid'");
	//-------------------------Update Item END------------------------------------------------------------------------------------	
?>
 <tr valign="top">
    <td><?=$updt_query['supplier_item_code'];?></td>
    <td><?=$updt_query['domain_id'];?></td>
    <td><?=$updt_query['type_id'];?></td>
    <td><?=$updt_query['item_parent_id'];?></td>
    <td><?=$updt_query['item_location_id'];?></td>
    <td><?=$updt_query['name'];?></td>
    <td><?=$updt_query['item_about'];?></td>
    <td><?=$updt_query['nice_name'];?></td>
    <td><?=$updt_query['title'];?></td>
    <td><?=$updt_query['keyword'];?></td>
    <td><?=$updt_query['description'];?></td>
    <td><?=$updt_query['price'];?></td>
    <td><?=$updt_query['selling_price'];?></td>
    <td><?=$updt_query['buying_price'];?></td>
    <td><?=$updt_query['item_shipping_amount'];?></td>
    <td><?=$updt_query['price_type'];?></td>
    <td><?=$updt_query['tax_vat'];?></td>
    <td><?=$updt_query['tax_added_type'];?></td>
    <td><?=$updt_query['tax_added'];?></td>
    <td><?=$updt_query['tax_inharyana'];?></td>
    
    <td><?=$updt_query['short_description'];?></td>
    <td><?=$updt_query['sku'];?></td>
    <td><?=$updt_query['thumbnail'];?></td>
    <td><?=$updt_query['thumbnail_label'];?></td>
    <td><?=$updt_query['visibility'];?></td>
    <td><?=$updt_query['weight'];?></td>
    <td><?=$updt_query['weight_price'];?></td>
    <td><?=$updt_query['weight_type'];?></td>
    <td><?=$updt_query['created_at'];?></td>
    <td><?=$updt_query['updated_at'];?></td>
    <td><?=$updt_query['qty'];?></td>
    <td><?=$updt_query['virtual_qty'];?></td>
    <td><?=$updt_query['stock_status'];?></td>
    <td><?=$updt_query['master_cat'];?></td>
    <td><?=$updt_query['child_cat'];?></td>
    <td><?=$updt_query['item_brand'];?></td>
    <td><?=$updt_query['item_supplier'];?></td>
    <td><?=$updt_query['num_views'];?></td>
    <td><?=$updt_query['discount'];?></td>
    
    <td><?=$updt_query['item_display_status'];?></td>
    <td><?=$updt_query['item_status'];?></td>
    <td><?=$updt_query['item_display_order'];?></td>
    <td><?=$updt_query['payment_mode'];?></td>
    
  </tr>
<?
}
$itemid='';
//$domain_id='';

}
}
?>
</table>
</body>
</html>