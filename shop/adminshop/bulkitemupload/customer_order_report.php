<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/supplier.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
if($datepicker && $datepicker1){
$from =$datepicker;
$to = $datepicker1;
$searchSql="AND order_c_date BETWEEN '$from' AND '$to' AND delevery_status != 'canceled' AND mode != 'TEST' AND order_status='0' AND  order_status!='inc' AND order_type !='partial'";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Order Report</title>

<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/jquery.tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery.tablesorter/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>

<script>
$(document).ready(function(){
	$("#ReportTable").tablesorter();
});

$(function() {
	$( "#datepicker" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});

$(function() {
	$( "#datepicker1" ).datepicker({
		showWeek: true,
		firstDay: 1,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
</head>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<body>

<table width="728" border="1" cellpadding="3" cellspacing="0">  
</table>



<h1>Customer Order Report</h1>
<form name="formfile" id="formfile" enctype="multipart/form-data" method="post" action="/shop/adminshop/bulkitemupload/customer_order_report.php">
<table width="100%" cellspacing="0" cellpadding="2" style="text-align:center; border-color:#999; border-width:1px; border-style:solid; background-color:#edeaea">
  <tr>
  <td>
   <p> From:  <input type="datepicker" id="datepicker" name ="datepicker"  value="<?=$from?>"  >   </p>
    
</td>  
<td>
   <p>  To: <input type="datepicker1"   id="datepicker1"  name ="datepicker1"  value="<?=$to?>" >  </p>

</td>  
  </tr>
</table>
<input type="file" name="xls_file" id="xls_file" />

<div style="text-align:center; margin-top:5px;"><input type="submit" name="searchOrder" id="searchOrder" value="                    Search                 " /></div>

<p>
Showing results from <strong><?=showdate($from, "d M o");?></strong> to  <strong><?=showdate($to, "d M o");?></strong>
</p>
</form>
<form action="/shop/adminshop/analytics/exporttoexcel.php" method="post"   
    onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'> 
    <input type="hidden" id="datatodisplay" name="datatodisplay">  
            <input type="submit" value="Export to Excel">
<table width="100%" border="1" cellpadding="5" cellspacing="0" id="ReportTable" class="myClass tablesorter">
<thead>
  <tr>
    <th width="28"><strong>S.No</strong></th>
<th width="69"><strong>Shopping Done</strong></th>

    <th width="69"><strong>Order ID</strong></th>
    <th width="215"><strong>Order Amount Amount</strong></th>
    <th width="97"><strong>Discount Code</strong></th>
  </tr>
  </thead>
<?
$s=1;
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],$DOCUMENT_ROOT."/shop/adminshop/bulkitemupload/xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$emailid=$data->sheets[0]['cells'][$j+1][1];
/*$att_id=$data->sheets[0]['cells'][$j+1][2];
$value=$data->sheets[0]['cells'][$j+1][3];
$selling=$data->sheets[0]['cells'][$j+1][3];
$buying=$data->sheets[0]['cells'][$j+1][4];*/
//$lifestage=$data->sheets[0]['cells'][$j+1][5];

if($emailid){
   	$getvalues=query_execute("SELECT order_id FROM shop_order_address WHERE address_email = '$emailid' AND address_type_id='1'");
	while($getdetails=mysql_fetch_array($getvalues)){
	$orderid=$getdetails["order_id"];
	$selectorder=query_execute_row("SELECT order_amount,order_discount_id FROM shop_order WHERE order_id='$orderid' $searchSql");
$disid=$selectorder["order_discount_id"];
	//$countresult=mysql_num_rows($selectorder);
$selectdiscount=query_execute_row("SELECT discount_code FROM shop_discount_main WHERE discount_id='$disid'");
	if($selectorder["order_amount"]=='' && $selectorder["order_discount_id"]==''){ ?>
  <tr>
   <td><?=$s; ?></td>
    <td>No</td>
<td>-</td>
    <td>-</td>
    <td>-</td>
 </tr>
<? }else{ ?>
  <tr>
  <td><?=$s; ?></td>
    <td>Yes</td>
<td><?=$orderid; ?></td>
    <td><?=$selectorder["order_amount"]; ?></td>
    <td><?=$selectdiscount["discount_code"]; ?></td>
 </tr>
	<? }
	$s++;
	}
//echo $orderid."<br>";
?>

<?
}
/*$item_id='';
$item_cat='';
$breed_id='';
$breed_type='';
$emailid='';
$att_id='';*/
$emailid='';
}
}
?>
</table>
<?php /*?><tbody>
<?
 $s=1;
while($rowMyOrders = mysql_fetch_array($qdsr)){	
     
	 $cou=$rowMyOrders["courier"];
	 
	 $uid=$rowMyOrders["uid"];	
	  $itm= $rowMyOrders["itmqty"];
	  $id=$rowMyOrders["discountid"];
	  
	
   	$qGetMyCart=query_execute("SELECT courier_name FROM shop_courier WHERE courier_id = '$cou'");
	while($rowcou = mysql_fetch_array($qGetMyCart)){	
	$couriername=$rowcou['courier_name'];}
	//pending date
	if($rowMyOrders["deliverystatus"]=='delivered' || $rowMyOrders["deliverystatus"]=='dispatched'){
	$pending=query_execute("select c_date from section_reviews where review_body like '%pending-dispatch%' AND review_section_name = 'shop-order' AND review_section_id='".$rowMyOrders["orderid"]."'");
	$pen = mysql_fetch_array($pending);
	$autherizedate=(showdate($pen["c_date"], "d M Y g:i a"));
	}
	   
		 //dispatched date
		 
		 if($rowMyOrders["deliverystatus"]=='delivered' || $rowMyOrders["deliverystatus"]=='dispatched'){
	$dispatched=query_execute("select c_date from section_reviews where review_body like '%dispatched%' AND review_section_name = 'shop-order' AND review_section_id='".$rowMyOrders["orderid"]."'");
	$dis = mysql_fetch_array($dispatched);
	$dispatcheddate=(showdate($dis["c_date"], "d M Y g:i a"));
	}
		 //delivered date
		 if($rowMyOrders["deliverystatus"]=='delivered' || $rowMyOrders["deliverystatus"]=='dispatched'){
	$delivered=query_execute("select c_date from section_reviews where review_body like '%delivered%' AND review_section_name = 'shop-order' AND review_section_id='".$rowMyOrders["orderid"]."'");
	$delive = mysql_fetch_array($delivered);
	$delivered=(showdate($delive["c_date"], "d M Y g:i a"));
	}
		$qGetMyuid=query_execute("SELECT * FROM shop_order_address WHERE address_type_id='1' AND order_id= '".$rowMyOrders["orderid"]."'");
	while($rowc = mysql_fetch_array($qGetMyuid)){	
	$email=$rowc['address_email'];
	$city=$rowc['address_city'];
	$state=$rowc['address_state'];
	$phone=$rowc['address_phone1'];
	$mobile=$rowc['address_phone2'];} 
	
	
	
?>  
  <tr>
    <td><?= $s ?></td>
   <td><a href="/shop/adminshop/order-detail.php?order_id=<?=$rowMyOrders["orderid"];?>" style="text-decoration:underline"><?=$rowMyOrders["orderid"]?></a></td>
    <td><div style="width:200px; height:40px; overflow:auto"><?=$rowMyOrders["item_id"]?></div></td>
	<td><?=$rowMyOrders["orderamount"]?></td>
   
	
	<td><?=$rowMyOrders["method"]?></td>
    <td><?=(showdate($rowMyOrders["date1"], "d M Y g:i a"));?></td>
    <td><?=$autherizedate?></td>
     <td><?=$dispatcheddate?></td>
     <td><?=$delivered ?>  </td>   
    
	<td><?=$rowMyOrders["deliverystatus"]?></td>
	<td><?=$couriername?></td>
	<td><?=$rowMyOrders["tracking"]?></td>
	<td><? if($rowMyOrders["deliverystatus"]=='delivered' || $rowMyOrders["deliverystatus"]=='dispatched'){
	echo dateDiff($rowMyOrders["date1"], $dispatcheddate); 	}?></td>
    
	<td><?  if($rowMyOrders["deliverystatus"]=='delivered' || $rowMyOrders["deliverystatus"]=='dispatched'){
	echo dateDiff($rowMyOrders["date1"], $delivered); 
		} ?></td>
	<td><?=$city?></td>
    <td><?=$state?></td>	
    <td><?=$email ?></td>
    <td><?=$mobile ?> <?= $phone ?></td>
	  
</tr>
<? $s=$s+1;
 } 
?>  


</tbody>
</table>
<?php */?></form>

</body>
</html>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php'); ?>