<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Upload for Suppliers</title>
</head>

<body>

<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>

<h1>Bulk Upload for Suppliers</h1>

<h3> 1st Column should be Item ID </h3>
<h3> 2nd Column should be ID of first supplier (Primary Supplier)</h3>
<h3> 3rd Column should be ID of second supplier</h3>
<h3> 4th Column should be ID of third supplier</h3>
<h3> Excel file should be of 97-2003 Workbook</h3>


<br />
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font><input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>

</form>
<hr />
<table width="728" border="1" cellpadding="3" cellspacing="0">  
  <tr>
    <td width="40">Item ID</td> 
    <td width="40">Supplier ID1</td>   
    <td width="40">Supplier ID2</td> 
    <td width="40">Supplier ID3</td> 
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$item_id=$data->sheets[0]['cells'][$j+1][1];
$supp1=$data->sheets[0]['cells'][$j+1][2];
$supp2=$data->sheets[0]['cells'][$j+1][3];
$supp3=$data->sheets[0]['cells'][$j+1][4];

$count='0';
if($item_id){
if($supp1!=''){
//$query1=mysql_query("UPDATE shop_items SET item_supplier='$supp1' WHERE item_id='$item_id'");
$checkvalue1=mysql_query("SELECT * FROM shop_item_supplier WHERE item_id='$item_id' AND supplier_id='$supp1'");
$checkvalue1count=mysql_num_rows($checkvalue1);
if($checkvalue1count == 0){
$query1=mysql_query("INSERT INTO shop_item_supplier (item_id,supplier_id,warehouse_id) VALUES ('$item_id','$supp1','1') ");
$selsuppitemcode=query_execute_row("SELECT supplier_item_code FROM shop_items WHERE item_id='$item_id'");
$updtsuppitemcode=mysql_query("UPDATE shop_item_supplier SET supplier_item_code='$selsuppitemcode[supplier_item_code]' WHERE item_id='$item_id'");
}
}
if($supp2!=''){
$checkvalue2=mysql_query("SELECT * FROM shop_item_supplier WHERE item_id='$item_id' AND supplier_id='$supp2'");
$checkvalue2count=mysql_num_rows($checkvalue2);
if($checkvalue2count == 0){
$query1=mysql_query("INSERT INTO shop_item_supplier (item_id,supplier_id,warehouse_id) VALUES ('$item_id','$supp2','1') ");
$selsuppitemcode=query_execute_row("SELECT supplier_item_code FROM shop_items WHERE item_id='$item_id'");
$updtsuppitemcode=mysql_query("UPDATE shop_item_supplier SET supplier_item_code='$selsuppitemcode[supplier_item_code]' WHERE item_id='$item_id'");
}
}
if($supp3!=''){
$checkvalue3=mysql_query("SELECT * FROM shop_item_supplier WHERE item_id='$item_id' AND supplier_id='$supp3'");
$checkvalue3count=mysql_num_rows($checkvalue3);
if($checkvalue3count == 0){
$query1=mysql_query("INSERT INTO shop_item_supplier (item_id,supplier_id,warehouse_id) VALUES ('$item_id','$supp3','1') ");
$selsuppitemcode=query_execute_row("SELECT supplier_item_code FROM shop_items WHERE item_id='$item_id'");
$updtsuppitemcode=mysql_query("UPDATE shop_item_supplier SET supplier_item_code='$selsuppitemcode[supplier_item_code]' WHERE item_id='$item_id'");
}
}
?>
 <tr valign="top">
    <td><?=$item_id;?></td> 
    <td><?=$supp1;?></td>   
     <td><?=$supp2;?></td>
      <td><?=$supp3;?></td>
  </tr>
<?
}
$item_id='';
$supp1='';
$supp2='';
$supp3='';
}
}
?>
</table>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php'); ?>

</body>
</html>