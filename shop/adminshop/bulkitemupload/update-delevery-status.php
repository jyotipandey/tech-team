<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Upload Order Delevery Status</title>
<style>
table {
	border: 1px solid #ccc;
}
tr {
	border: 1px solid #ccc;
}
td {
	border: 1px solid #ccc;
}
th {
	border: 1px solid #ccc;
}
</style>
</head>
<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<h3>1st Coloum should be Order Id</h3>
<h3>2nd Coloum should be Airwaybill No</h3>
<h3>Order Should Be Dispatch Before Doing It delivered Or Lost</h3></br>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
  <center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select Status:</font>
<select id="delevery_status" name="delevery_status" required>
<option value="">SELECT</option>
<option value="delivered">Delivered</option>
<option value="lost">Lost</option>
<option value="rto">RTO</option>
<option value="dispatched">Dispatched</option>
</select>
    <font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font>
    <input type="file" name="xls_file" id="xls_file" />
    <input type="submit" name="button" id="button" value="Upload" />
  </center>
</form>
<div>
</div>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);?>
<table>
  <tr>
    <th>Order Id</th>
    <th>AWB NO</th>
    <th>Status</th>
  </tr>
  <?
		for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
		 $order_id=$data->sheets[0]['cells'][$j+1][1];
		 $waybill_no=$data->sheets[0]['cells'][$j+1][2];
		 $order_id=trim($order_id);
		 $waybill_no=trim($waybill_no);
		 $check_order=query_execute_row("SELECT delevery_status, order_tracking,warehouse_id FROM shop_order WHERE order_id='$order_id'");
         $order_status_ck=$check_order['delevery_status'];
		 $order_waybill_ck=$check_order['order_tracking'];
		 $order_ware_id=$check_order['warehouse_id'];
		 echo $order_status_ck.'@@'.$order_waybill_ck.'@@'.$waybill_no;
		 if($order_id !='' && $delevery_status !='' && ($order_status_ck=='dispatched' || $order_status_ck=='dispatched-ready' )  && $order_waybill_ck==$waybill_no){	
		 if($delevery_status=='dispatched')
		 {
			 if($order_ware_id=='3'){
		$update_order_table=query_execute("UPDATE shop_order SET delevery_status='$delevery_status' WHERE order_id='$order_id'");
		$get_insert_itemid=query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body, review_name, c_date) VALUES ('$userid', 'shop-order', '$order_id', '$delevery_status', '$delevery_status', NULL)");
		$error="$delevery_status";	
			 }else
			 {
				 $error="<font color='#FF0000'>This order is not belong to Banglore";
			 }
		 }elseif($delevery_status=='delivered' && $order_status_ck=='dispatched')
		 {
	$delevery_msg=$delevery_status;	 
	$update_order_table=query_execute("UPDATE shop_order SET delevery_status='$delevery_status' WHERE order_id='$order_id'");
	$get_insert_itemid=query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body, review_name, c_date) VALUES ('$userid', 'shop-order', '$order_id', '$delevery_status', '$delevery_msg', NULL)");
	$error="$delevery_status";
	}else{
 
     if($delevery_status=='rto')
	 {
	 $delevery_msg='rto-transit';	 
	 }else
	 {
	$delevery_msg=$delevery_status;	 
	 }
	$update_order_table=query_execute("UPDATE shop_order SET delevery_status='$delevery_status' WHERE order_id='$order_id'");
	$get_insert_itemid=query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body, review_name, c_date) VALUES ('$userid', 'shop-order', '$order_id', '$delevery_status', '$delevery_msg', NULL)");
	$error="$delevery_status";
		 }
		?>
  
  <?
   }else
   {
	   $error="there is some error in your data";
   }
   ?>
   <tr>
    <td><?=$order_id?></td>
    <td><?=$waybill_no?></td>
    <td><?=$delevery_status?></td>
     <td><?=$error?></td>
  </tr><?
$order_id='';
$waybill_no='';					
		}						
		?>
</table>
<?
    }
?>
</form>
</body>
</html>