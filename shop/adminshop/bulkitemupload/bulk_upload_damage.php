<?php
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
error_reporting(E_ALL ^ E_NOTICE);

$ip = ipCheck();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Update For Damage Item </title>
</head>

<body>

<h1>Bulk Update For Damage Item</h1>
<font color="#FF0000">
	<p>Excel Sheet have only 2 column</p>
	<h3>First Column:  Item ID</h3>
	<h3>Second Column: Damage Qty</h3>
</font>


<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
  	<table border="0" width="100%" class="myClass tablesorter">   
  		<tr>
        	<td width="50%">
    			<input type="file" name="xls_file" id="xls_file" /></td><td>
    			<input type="submit" name="button" id="button" value="Upload" /> 
			</td>
     	</tr> 
  	</table>
    <hr/>
    
</form>
<br/>
<table width="100%" border="1" cellpadding="3" cellspacing="0">  
	<tr>
    	<td width="10%">S. No</td>
    	<td width="35%">ITEM ID</td>  
     	<td width="35%">Qty</td>
        <td width="20%">Status</td>   
  	</tr>
<?php

if($xls_file){
	
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. <br/>";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//	echo $data->sheets[0]['numRows'];
	//	echo "+++++";
	//	echo $data->rowcount($sheet_index=0);
	//	echo "+++++";
	//	echo $data->colcount($sheet_index=0);
	//	die;
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
	$arr_item_id = "";
	for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
		
		$item_id=$data->sheets[0]['cells'][$j+1][1];
        
		$item_qty=$data->sheets[0]['cells'][$j+1][2];
		
		$SQ_shop_items = query_execute_row("SELECT qty FROM shop_items WHERE item_id='$item_id'");
       	
       	$shop_qty = $SQ_shop_items['qty'];
       	
       	$temp = $item_qty;
		
		$arr_item_id .= $item_id.", ";
		
		// Getting Item Details 
       	$selitemdetails=query_execute_row("SELECT * FROM shop_items WHERE item_id='$item_id'");
		
		$itemname = $selitemdetails['name'];
	
		$itemprice = $selitemdetails['price'];
	 
	 	$itemselling = $selitemdetails['selling_price'];
		
		$itembuying = $selitemdetails['buying_price'];
       	
       	// Check Item Id exists in table Or not
       	$selectstatus = query_execute("SELECT * FROM shop_rto_damage_items WHERE order_id='0' AND damage_items='$item_id'");
       	
		$count = mysql_num_rows($selectstatus);
       
		if($item_id && $item_qty > 0 && $shop_qty >=$item_qty){
			
			$check = 0;
			$section_check = 0;
			
			$SQ_shop_item_stock_status = query_execute("SELECT item_stock_qty,item_stock_status_id FROM shop_item_stock_status WHERE item_id='$item_id' AND item_stock_qty > 0 AND  status='active' AND warehouse_id='1' ORDER BY item_stock_status_id DESC");
				
			if(mysql_num_rows($SQ_shop_item_stock_status) > 0){
				while($row = mysql_fetch_array($SQ_shop_item_stock_status)){
					$item_stock_qty = $row['item_stock_qty'];
		
					$item_stock_status_id = $row['item_stock_status_id'];
			
					if($item_qty >= $item_stock_qty){
								
		  				query_execute("UPDATE shop_item_stock_status SET item_stock_qty = item_stock_qty - item_stock_qty WHERE item_id='$item_id' AND item_stock_status_id='$item_stock_status_id'  AND status='active'");
					
						//echo "UPDATE shop_item_stock_status SET item_stock_qty = item_stock_qty - item_stock_qty WHERE item_id='$item_id' AND item_stock_status_id='$item_stock_status_id' AND supplier_detail_id!='0' AND status='active'";
						
						$item_qty= $item_qty - $item_stock_qty;
						
						$check = 1;
						
						//echo "<br/>";
						
						$msg = "Updated Successfully";
							
					}
					if($item_qty < $item_stock_qty && $item_qty!='0'){
		 				
						query_execute("UPDATE shop_item_stock_status SET item_stock_qty = item_stock_qty - '$item_qty' WHERE item_id='$item_id' AND item_stock_status_id='$item_stock_status_id'  AND status='active'");
					
						//echo "UPDATE shop_item_stock_status SET item_stock_qty = item_stock_qty - '$item_qty' WHERE item_id='$item_id' AND item_stock_status_id='$item_stock_status_id' AND supplier_detail_id!='0' AND status='active'";
						
						//echo "<br/>";

						$msg = "Updated Successfully";
	
						$check = 1;
	
						break;
					}	
				}
			}
			if($check == 1){
				
				$UQ_shop_item = query_execute("UPDATE shop_items SET qty=qty - '$temp', virtual_qty=virtual_qty - '$temp' WHERE item_id='$item_id'");
				
				//echo "UPDATE shop_items SET qty=qty - '$temp', virtual_qty=virtual_qty - '$temp' WHERE item_id='$item_id'";
				
				
	
					$status='insert';
					
					//echo "INSERT INTO shop_rto_damage_items (order_id, damage_items, name, price, selling, buying, item_qty, entry_date, item_status, user) VALUES ('0', '$item_id', '$itemname', '$itemprice', '$itemselling', '$itembuying', '$temp', NULL, '$status', '$userid')";
					$itemname=addslashes($itemname);
					
					$insertdata = query_execute("INSERT INTO shop_rto_damage_items (order_id, damage_items, name, price, selling, buying, item_qty, entry_date, item_status, user,warehouse_id) VALUES ('0', '$item_id', '$itemname', '$itemprice', '$itemselling', '$itembuying', '$temp', NULL, '$status', '$userid','1')");
	
				
				//else{
				//	$status='update';
					
					//echo "UPDATE shop_rto_damage_items SET item_qty=item_qty + '$temp', entry_date=NULL, item_status='$status', user='$userid' WHERE order_id='0' AND damage_items='$item_id'";
					
				///	$insertdata = query_execute("UPDATE shop_rto_damage_items SET item_qty=item_qty + '$temp', entry_date=NULL, item_status='$status', user='$userid' WHERE order_id='0' AND damage_items='$item_id'");
				//
				
				$check = 0;	
				$section_check = 1;			
			}
				
		}	
		else{
			$msg = "<font color='#ff0000'>Damage Qty is greater than Shop Qty</font>";	
		}
?>
	<tr valign="top">
    	<td><?=$j?></td>
    	<td><?=$item_id?></td>
    	<td><?=$temp?></td>
        <td><?=$msg?></td>
   	</tr>
<?php
		$item_id="";
		$item_qty="";
		$temp = "";
		$msg = "";
	}
	if($section_check == '1'){
		
		$idArray = explode(", ,",$arr_item_id);         // remove one extra comma

		$idArray1 = explode(",",$idArray[0]);           // fetch one id from string of ids to insert into item_id col
		
		$IQ_section_review = query_execute("INSERT INTO section_reviews (section_review_id, userid, review_section_name, review_section_id, review_body, review_name, c_date, user_ip) VALUES ('', '$userid', 'Bulk-Upload', '$idArray1[0]', '$idArray[0]', 'Damage Items', NULL, '$ip')");
	}
}
?>
</table>
</body>
</html>