<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk mail</title>
<style>
table{border:1px solid #ccc;}
tr{border:1px solid #ccc;}
td{border:1px solid #ccc;}
th{border:1px solid #ccc;}
</style>
</head>
<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<h1>Bulk Upload for Ean</h1>
<h3>1st Column should be Item ID</h3>
<h3>2nd Column should be supplier code</h3>
<h3>3rd Column should be Ean</h3>
<h3>File should be saved in "xls format"</h3> 
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font>
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>
</form>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	?>
	 <table>
	 	<tr>
            <th>item_id</th>
            <th>supplier_code</th>
			<th>ean</th>
        </tr><? 
		for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
			$item_id=$data->sheets[0]['cells'][$j+1][1];
			$supplier_code=$data->sheets[0]['cells'][$j+1][2];
			$ean=$data->sheets[0]['cells'][$j+1][3];
			mysql_query("insert into shop_items_ean(item_id,supplier_code,ean)values('$item_id','$supplier_code','$ean')");
			?>
			<tr>
				<td><?=$item_id?></td>
				<td><?=$supplier_code?></td>
				<td><?=$ean?></td>
			</tr>
		<? 
			$item_id='';$supplier_code='';$ean='';
		} 		
		?>
        </table><?
    }
?>
</body>
</html>