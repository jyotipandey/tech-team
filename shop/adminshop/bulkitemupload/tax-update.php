<?
require_once($DOCUMENT_ROOT.'/session.php');

require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Awb Data</title>
</head>

<body>
<script type="text/javascript" src="/jquery.tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="/jquery.tablesorter/jquery.tablesorter.js"></script>

<link rel="stylesheet" href="/development-bundle/themes/base/jquery.ui.all.css">
<script src="/development-bundle/ui/jquery.ui.core.js"></script>
<script src="/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="/development-bundle/ui/jquery.ui.datepicker.js"></script>
<h1>AWB No Upload File</h1>
<font style="size:10px;color:#F00">Please check your execl don't have sheet2 or sheet3</font></br>
<h4> 1st Column should be Item Id</br>
File should be saved in "Excel 97-2003 Workbook" </h4>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" />

</form>
<hr />
<form action="/shop/adminshop/analytics/exporttoexcel.php" method="post"   
    onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'> 
    <input type="hidden" id="datatodisplay" name="datatodisplay">  
            <input type="submit" value="Export to Excel">
<table width="605" border="1" cellpadding="3" cellspacing="0" name="ReportTable" id="ReportTable">  
  <tr>
  <td width="36">Item ID</td>
  <td width="32">Tax Vat</td>
    <td width="36">Tax Added Type</td>
    <td width="36">Tax Added</td>
    <td width="36">Tax In Haryana</td>
    <td width="36">Tax Out Haryana</td>
  </tr>

<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$itemid=$data->sheets[0]['cells'][$j+1][1];
$taxvat=$data->sheets[0]['cells'][$j+1][2];
$taxaddedtype=$data->sheets[0]['cells'][$j+1][3];
$taxadded=$data->sheets[0]['cells'][$j+1][4];
$taxinharyana=$data->sheets[0]['cells'][$j+1][5];
$taxoutharyana=$data->sheets[0]['cells'][$j+1][6];

if($itemid){
	//echo "UPDATE shop_items SET tax_vat='$taxvat', tax_added_type='$taxaddedtype', tax_added='$taxadded', tax_inharyana='$taxinharyana', tax_outharyana='$taxoutharyana' WHERE item_id='$itemid';";
$taxupdateq=query_execute("UPDATE shop_items SET tax_vat='$taxvat', tax_added_type='$taxaddedtype', tax_added='$taxadded', tax_inharyana='$taxinharyana', tax_outharyana='$taxoutharyana' WHERE item_id='$itemid'");


	//-------------------------Update Item END------------------------------------------------------------------------------------	
?>
 <tr valign="top">
    <td><?=$itemid;?></td>
    <td><?=$taxvat;?></td>
    <td><?=$taxaddedtype;?></td>
    <td><?=$taxadded;?></td>
    <td><?=$taxinharyana;?></td>
    <td><?=$taxoutharyana;?></td>
  </tr>
<?
}
$itemid='';
$taxvat='';
$taxaddedtype='';
$taxadded='';
$taxinharyana='';
$taxoutharyana='';
}}
?>
  </table>
  </form>
</body>
</html>