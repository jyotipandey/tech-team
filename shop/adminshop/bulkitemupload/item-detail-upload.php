<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert/Update Item's Detail</title>
</head>

<body>
<h1>Insert/Update Item's Detail</h1>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" />

</form>
<hr />
<table width="605" border="1" cellpadding="3" cellspacing="0">  
  <tr>
    <td width="40">Item Id</td>
    <td width="73">Description</td>
    <td width="73">Specification</td>
    <td width="73">Features</td>
    <td width="73">Precautions</td>
    <td width="73">Others</td>
    
    
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);

for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$item_id=$data->sheets[0]['cells'][$j+1][1];
$description=$data->sheets[0]['cells'][$j+1][2];
$specification=$data->sheets[0]['cells'][$j+1][3];
$features=$data->sheets[0]['cells'][$j+1][4];
$precautions=$data->sheets[0]['cells'][$j+1][5];
$others=$data->sheets[0]['cells'][$j+1][6];

if($item_id){
$description = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $description);
$description=addslashes($description);

$specification = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $specification);
$specification=addslashes($specification);

$features = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $features);
$features=addslashes($features);

$precautions = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $precautions);
$precautions=addslashes($precautions);

$others = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $others);
$others=addslashes($others);

$itemupdet=mysql_query("UPDATE shop_items SET description='$description', specification='$specification', features='$features', precautions='$precautions', others='$others' WHERE item_id='$item_id'");
	//-------------------------Update Item END------------------------------------------------------------------------------------	
?>
 <tr valign="top">
    <td><?=$item_id;?></td>
    <td><?=$description;?></td>
    <td><?=$specification;?></td>
    <td><?=$features;?></td>
    <td><?=$precautions;?></td>
    <td><?=$others;?></td>
   
  </tr>
<?
}
$item_id='';
$description='';
$specification='';
$features='';
$features='';
$others='';
}
}
?>
</table>
</body>
</html>