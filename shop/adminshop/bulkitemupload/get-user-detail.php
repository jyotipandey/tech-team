<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk User Detail</title>
<style>
table{border:1px solid #ccc;}
tr{border:1px solid #ccc;}
td{border:1px solid #ccc;}
th{border:1px solid #ccc;}
</style>
</head>
<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font>
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>
</form>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);?>
	 <table>
	 	<tr>
            <th>UserID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Mobile</th>          
        </tr>       
		<?
		for ($j = 0; $j <= $data->sheets[0]['numRows']; $j++){
		 $userid_st=$data->sheets[0]['cells'][$j+1][1];
		 $getdata_user=query_execute_row("Select address_name, address_email, address_address1, address_city, address_state, address_phone1 from shop_order_address where userid='$userid_st'");
			?>
<?
$address_name=$getdata_user['address_name'];
$address_email=$getdata_user['address_email'];
$address_address1=$getdata_user['address_address1'];
$address_city=$getdata_user['address_city'];
$address_state=$getdata_user['address_state'];
$address_phone1=$getdata_user['address_phone1'];?>            
            <tr>
                <td><?=$userid_st?></td>
                <td><?=$address_name?></td>
                <td><?=$address_email?></td>
                <td><?=$address_address1?></td>
                <td><?=$address_city?></td>
                <td><?=$address_state?></td>
                <td><?=$address_phone1?></td>
            </tr>
            <? }}?>
        </table>
</body>
</html>