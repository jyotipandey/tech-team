<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Item stock Update</title>
</head>
<body>
<h1>Update Item's status</h1><font color='#FF0000'>
Note: Please Create Xls with 
first col : itemid <br />
second col : visibility <br />
third col : item status <br />
fourth col : stock status <br />
</font>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" />

</form>
<hr />
<table width="728" border="1" cellpadding="3" cellspacing="0">  
<tr>
    <td width="40">Item Id</td>
    <td width="40">Visibility(visible/invisible)</td>
    <td width="73">Item Status(soft/hard)</td>
    <td width="73">Stock Status(instock/outofstock)</td>
</tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$item_id=$data->sheets[0]['cells'][$j+1][1];
$Visibility=$data->sheets[0]['cells'][$j+1][2];
$ItemStatus=$data->sheets[0]['cells'][$j+1][3];
$StockStatus=$data->sheets[0]['cells'][$j+1][4];

$item_id = trim($item_id);
$Visibility = trim($Visibility);
$ItemStatus = trim($ItemStatus);
$StockStatus = trim($StockStatus);

if($item_id && $Visibility && $ItemStatus && $StockStatus ){
	//-------------------------Update Item------------------------------------------------------------------------------------
//echo "SELECT qty FROM shop_items WHERE item_id='$item_id'".";<br>";
$ItemStatus=strtolower($ItemStatus);
$Visibility=strtolower($Visibility);
$StockStatus=strtolower($StockStatus);
	if(($Visibility=='visible' || $Visibility=='invisible') && ($ItemStatus=='soft' || $ItemStatus=='hard') && ($StockStatus=='instock' || $StockStatus=='outofstock')){
	$query1= "UPDATE shop_items SET visibility='$Visibility',stock_status='$StockStatus', item_status='$ItemStatus' WHERE item_id='$item_id'";
	query_execute($query1);
	$selectData=query_execute_row("SELECT qty,virtual_qty FROM shop_items WHERE item_id='$item_id'");
	$selectDataAff=query_execute_row("SELECT item_id FROM shop_item_affiliate WHERE item_id='$item_id'");
	if(($selectData['virtual_qty']==0 || $selectData['qty']==0) && $ItemStatus=='hard' && !$selectDataAff['item_id']){
	$query12= "UPDATE shop_items SET visibility='invisible',stock_status='outofstock',virtual_qty='0' WHERE item_id='$item_id'";
	query_execute($query12);	
	}
		
		}else{
echo "<font color='#FF0000'>$item_id is not update due to some mismatch"."</font><br>";
}
	

	//-------------------------Update Item END------------------------------------------------------------------------------------	
?>
 <tr valign="top">
 <? if(($Visibility=='visible' || $Visibility=='invisible' || $Visibility!='') && ($ItemStatus=='soft' || $ItemStatus=='hard' || $ItemStatus!='') && ($StockStatus=='instock' || $StockStatus=='outofstock' || $StockStatus!='')){?>
    <td><?=$item_id;?></td>
    <td><?=$Visibility;?></td>
    <td><?=$ItemStatus;?></td>
    <td><?=$StockStatus;?></td>
   <? }else{ ?>
    <td style="color:#F30"><?=$item_id;?></td>
    <td style="color:#F30"><?=$Visibility;?></td>
    <td style="color:#F30"><?=$ItemStatus;?></td>
    <td style="color:#F30"><?=$StockStatus;?></td>

<? }?>
  </tr>
<?
}
$item_id='';
$Visibility='';
$ItemStatus='';
$StockStatus='';

}
}
?>
</table>
</body>
</html>