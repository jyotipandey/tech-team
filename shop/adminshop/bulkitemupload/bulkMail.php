<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk mail</title>
</head>
<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font>
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>
</form>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 0; $j <= $data->sheets[0]['numRows']; $j++){
	echo $email=$data->sheets[0]['cells'][$j+1][1];
			$owner="adoption@dogspot.in";
			$unameO="Adoption at DogSpot";
			$mailSubject="Adoption";
		    $mailBody='Hello '.$uname.'<br><br>';
			$mailBody.='Thank you for showing interest in adopting a pet from DogSpot.<br><br>';
			$mailBody.='I am sorry to inform you that we are unable to process your adoption request at this time due to certain constraints at our end.<br><br>';
			$mailBody.='One of our adoption experts will get back to you as soon as we are ready to process your request.<br><br>';
			$mailBody.='Once again, please accept our sincere apology for any inconvenience this may have caused you.<br><br>';
			$mailBody.='Regards,<br>';
			$mailBody.='Rana Atheya,<br>';
			$mailBody.='CEO, <a href="https://www.dogspot.in">DogSpot.in</a>';
			$toEmail = $email;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: '.ucfirst($unameO).' <'.$owner.'>' . "\r\n";
			$headers .= 'cc: DogSpot <adoption@dogspot.in>' . "\r\n";
			$headers .= 'Reply-To: '.$unameO.' <'.$owner.'>' . "\r\n";
			$headers .= "Return-Path: DogSpot <mybounceemail@dogspot.in>\r\n";
			$headers .= "Organization: DogSpot\r\n";
			mail($toEmail, $mailSubject, $mailBody, $headers,"-f contact@dogspot.in  mybounceemail@dogspot.in");
		}
    }
require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php');
?>
</body>
</html>