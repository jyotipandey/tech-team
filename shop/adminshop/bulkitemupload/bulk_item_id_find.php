<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Upload for Stock Status</title>
</head>

<body>

<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>

<h1>Bulk Upload Item Id for Stock Status</h1>

<h3> 1st Column should be Item ID </h3>

<br />
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font><input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>

</form>
<hr />
<table width="728" border="1" cellpadding="3" cellspacing="0">  
  <tr>
    <td width="40">S. No</td>  
    <td width="40">Item ID</td>  
    <td width="40">Supllier_Details_Id</td>  
    <td width="40">Item QTY</td>  
    <td width="40">Status</td>    
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
	for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
		$item_id=$data->sheets[0]['cells'][$j+1][1];
		
		//$stock_status=$data->sheets[0]['cells'][$j+1][2];
		
		if($item_id){
			
			$SQ_supplier_detail = query_execute("SELECT a.supplier_detail_id, a.item_id, b.item_stock_qty, b.status FROM shop_supplier_detail as a, shop_item_stock_status as b WHERE a.item_id = '$item_id' and a.supplier_detail_id=b.supplier_detail_id ORDER BY a.supplier_detail_id DESC LIMIT 10");
			
			if(mysql_num_rows($SQ_supplier_detail) > 0){
				$count = 1;
				while($getSupplier_id = mysql_fetch_array($SQ_supplier_detail)){
					$supplier_detail_id = $getSupplier_id['supplier_detail_id'];
					$item_stock_qty = $getSupplier_id['item_stock_qty'];
					$status = $getSupplier_id['status'];
					
		?>
                    <tr valign="top">
                        <td><?=$count;?></td>
                        <td><?=$item_id;?></td>
                        <td><?=$supplier_detail_id;?></td>
                        <td><?=$item_stock_qty;?></td>
                        <td><?=$status;?></td>
                    </tr>
									
		<?		$count++ ; }
			}
		}
	}
}
?>
</table>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php'); ?>
</body>
</html>