<?php
require_once($DOCUMENT_ROOT . '/constants.php');
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
//require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/arrays.php');
include($DOCUMENT_ROOT."/dogs/arraybreed.php");

$siteURL=''; 
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = '';
?>
<?php
$breedid         = $brd;
//echo "SELECT * FROM breed_engine_values WHERE breed_id='$breedid' ORDER BY att_id ASC";
$breednameselect = query_execute_row("SELECT breed_name,nicename,breed_life,monthly_keeping_cost_premium,monthly_keeping_cost_standard,icon,image_name,height,weight,img_link,img_dog_name,breed_engine,be_name FROM dog_breeds WHERE breed_id='$breed_id'");
$breedname       = $breednameselect['breed_name'];
$breednice       = $breednameselect['nicename'];
$icon            = $breednameselect['icon'];
$img             = $breednameselect['image_name'];
$ht              = $breednameselect['height'];
$wt              = $breednameselect['weight'];
$imglink         = $breednameselect['img_link'];
$img_dog_name    = $breednameselect['img_dog_name'];
$breed_engine    = $breednameselect['breed_engine'];


$ant_category = $be_name         = $breednameselect['be_name'];
$lifestage         = $breednameselect['breed_life'];
$costpr          = $breednameselect['monthly_keeping_cost_premium'];
$costst          = $breednameselect['monthly_keeping_cost_standard'];
$selorigin       = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='39'");
$selorigin1      = $selorigin['value'];
$selsize2       = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='41'");
$selsize    	 = $selsize2['value'];
if($selsize=='193'){
	$selsize1='2';
}else if($selsize=='194'){
	$selsize1='3';
}else if($selsize=='195'){
	$selsize1='5';
}else{
	$selsize1='6';
}
$selorigin1name  = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_id='39' AND att_att_id='$selorigin1'");
$selgroup        = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='35'");
$selgroup1       = $selgroup['value'];
$selgroup1name   = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_id='35' AND att_att_id='$selgroup1' ");
$seltag          = query_execute_row("SELECT data FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id='1'");
$selogn          = query_execute_row("SELECT data FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id='4'");
$selrelated      = mysql_query("SELECT bev.breed_id FROM breed_engine_values as bev , dog_breeds as db WHERE bev.breed_id=db.breed_id AND bev.att_id='35' AND bev.value='$selgroup1' AND bev.breed_id !='$breed_id' AND db.breed_engine='1' LIMIT 3");
$countrelated=mysql_num_rows($selrelated);
$sel_odr_grp_name=mysql_query("SELECT * FROM breed_engine_att_att WHERE att_id='35'");
while($selrelated1=mysql_fetch_array($selrelated)){
$relatedarray[]=$selrelated1['breed_id'];
}
$selecttitle=query_execute_row("SELECT * FROM breed_engine_breed_titles WHERE breed_id='$breed_id'");

$new_ht= round($new_ht=(266/72)*$ht);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$selecttitle['title']?></title>
<meta name="keywords" content="<?=$selecttitle['keyword']?>" />
<meta name="description" content="<?=$selecttitle['description']?>" />
<meta property="fb:app_id" content="1677990482530277" /> 
<meta property="og:site_name" content="DogSpot"/>
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.dogspot.in/<?= $breednice ?>/" /> 
<meta property="og:title" content="<?=$selecttitle['title']?>" /> 
<meta property="og:description" content="<?=$selecttitle['description']?>" />
<meta property="og:image" content="https://www.dogspot.in/new/breed_engine/images/dog_images/<?= $img ?>" /> 

<meta name="twitter:card" content="photo">
<meta name="twitter:site" content="@indogspot">
<meta name="twitter:url" content="https://www.dogspot.in/<?= $breednice ?>/">
<meta name="twitter:title" content="<?=$selecttitle['title']?>">
<meta name="twitter:description" content="<?=$selecttitle['description']?>">
<meta name="twitter:image" content="<?=$siteURL?>/new/breed_engine/images/dog_images/<?= $img ?>">
<meta name="twitter:image:width" content="610">
<meta name="twitter:image:height" content="610">
<link rel="canonical" href="https://www.dogspot.in/<?=$section[0]?>/" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/<?=$section[0]?>/" />
<? /*For Twitter Title and Description*/
$post_title = $selecttitle['title'];
$page_nice = $breednice;
$pageDesc = $selecttitle['description'];
/*For Twitter Title and Description ENDS*/
?>

<?php
//require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
//require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
if($breed_engine!='1' && $breed_engine!='2'){
header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();	
?>

<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<? }
?>
<script type="text/javascript" src="/new/js/jquery.min.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery-ui.js" ></script>
<?php

require_once($DOCUMENT_ROOT . '/new/common/header.php');
?>
<link rel="stylesheet" href="/new/breed_engine/css/be_style.css?v=8" />


<script>
function moveToFooter(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top},
        'slow');
}

function showpages(cmnt_limit){
var c_limit=cmnt_limit-1;
if(c_limit >= 0){	
var c_limit=c_limit*5;	
ShaAjaxJquary('/new/breed_engine/cmnt_paging.php?breed=<?=$breed_id ?>&cmnt_limit='+c_limit+'', '#show_cmnt', '', '', 'GET', '#show_cmnt', 'Loading...', 'REP');	
}
}
</script>


 <!-- breadcrumb -->
        	<div class="breadcrumb">
            	<div class="header cont980">
                
                    <div class="Dog Breeds"><span> <a href="/dog-breeds/">Dog Breeds</a></span> <span> ></span> <span class="brd_font_bold"> <?=$be_name; ?></span>
                     </div>
                      </div>              
                     
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->   
<div class="cont980" itemscope itemtype="https://www.schema.org/Product">

<div class="be_detailBox1">
<div class="be_imgDogInfo">
<div class="be_imgDetail1">
  <img itemprop="image" src="<?=$siteURL?>/new/breed_engine/images/dog_images/<?= $img ?>" alt="<?=$be_name?>" title="<?=$be_name; ?>" style="margin-top:40px;" /> 
</div>
<? if($img_dog_name!=''){ ?>
<div class="be_regidogname">
<label><strong>Picture Courtesy:</strong>
  <span>
 <? if($section[0]!='indian-pariah-dog'){?> <a href="<?=$imglink?>" target="_blank" ><? }?>
<?=$img_dog_name; ?> <? if($section[0]!='indian-pariah-dog'){?></a><? }?>
<? if($section[0]=='indian-pariah-dog'){?>(http://indog.co.in/)<? }?></span>
  </label>
  </div>
<? } ?>
</div>

<div class="be_txtDetail1" itemprop="description">
<h1 itemprop="name" ><?= $be_name ?></h1>
<div class="be_tag" ><?= stripcslashes(htmlspecialchars_decode($seltag['data'])); ?></div>

<div class="be_info">

<div class="be_height">
 <div class="be_dogicon">
 <? if($breed_id=='636'){ ?><p class="das_wc_height">Standard : <? }else{ ?><p><? } ?><?=$ht ?> Inches<a style="cursor:pointer" id="This is an average height till head" class="tooltip" title="This is an average height till head">*</a></p>
<?
	if($icon){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/icons/'.$icon;
		$imageURL='/new/breed_engine/images/icons/new-ht-'.$icon;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,$new_ht,$new_ht,'ratiohh');
		$compressed = compress_image($dest,$dest, 40);
?>
<img src="<?=$siteURL?>/new/breed_engine/images/icons/new-ht-<?=$icon?>" alt="<?=$breedname?>" title="<?=$breedname?>">
</div>
<? if($breed_id=='636'){ ?>
<div class="be_dogicon">
 <p class="be_das_height">Miniature : 6 Inches<a style="cursor:pointer" id="This an average height for the male dog" title="This an average height for the male dog" class="tooltip">*</a></p>
</div>
<? } ?>
  </div>
<? if($breed_id!='636'){ ?>
<div class="be_weight">
<p>In Kg<a style="cursor:pointer" id="Average for the breed" title="Average for the breed" class="tooltip">*</a><br>
<?=$wt ?></p></div>
<? } if($breed_id=='636'){ ?>
<div class="be_weight be_std_weight">
Standard
<p>In Kg<a style="cursor:pointer" id="Average for the breed" title="Average for the breed" class="tooltip">*</a><br>
<?=$wt ?></p></div>
<div class="be_weight be_das_weight">
Miniature
<p>In Kg<a style="cursor:pointer" id="Average for the breed" title="Average for the breed" class="tooltip">*</a><br>
upto 4 Kg</p></div>
<? } ?>
</div>
<p><!--<img src="images/detail_icon2.jpg" width="32" height="30" alt="" />-->
<label><strong>Origin:</strong> &nbsp;<?= stripcslashes(htmlspecialchars_decode($selorigin1name['value'])) ?></label></p>
<p><!--<img src="images/detail_icon3.jpg" width="32" height="30" alt="" />-->
<label><strong>Group:</strong> &nbsp;<a href="/<?=strtolower($selgroup1name['value'])?>-group-dog-breeds/" target="_blank" ><?= stripcslashes(htmlspecialchars_decode($selgroup1name['value'])) ?></a></label></p>
<p><!--<img src="images/detail_icon6.jpg" width="32" height="30" alt="" />-->
<label><strong>Origin of Name:</strong><div class="be_pBox" ><?= stripcslashes(htmlspecialchars_decode($selogn['data'])); ?> </div> </label>
</p>

</div>


</div>

<!--Breed Rating-->
 
     <!--Breed Rating-->
<div class="be_basicInfo1">
<div class="be_basicHead">
<div class="be_infobasic">
<ul>
<li class="be_infoHead"><strong>Size</strong></li>
<li class="be_infoHead" style=" width: 300px;"><strong>Efforts</strong></li>
<li class="be_infoHead" style=" width: 275px;"><strong>Shedding</strong></li>
</ul>

<ul>
<? 
$tablearry=array("41","30");
foreach($tablearry as $table_att){
$tabledata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$table_att'");
while($tabledata1=mysql_fetch_array($tabledata)){
	$tablevalue=$tabledata1['value'];
	//echo "SELECT value FROM breed_engine_att_att WHERE att_att_id='$tablevalue'"."<br>";
	$tablevaluename=query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$tablevalue'");
if($table_att!='30'){
?>
<li><?=stripcslashes(htmlspecialchars_decode($tablevaluename['value'])) ?></li>
<? }else{?>
<li style="width:290px; padding:0 5px"><?=stripcslashes(htmlspecialchars_decode($tablevaluename['value'])) ?></li>
<? }}} 
$minmax=query_execute_row("SELECT min_value,max_value FROM breed_engine_traits WHERE id='15'");
$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='15'");
$nRating=(($qRATING['value1']/1000)*100);
            if ($nRating <= 0 || $nRating > 5) {
                $finalratng = '0';
                $ratvalue   = "rate1";
            }
            if ($nRating > 0 && $nRating <= 1) {
                $finalratng = '1';
                $ratvalue   = "rate1";
            }
            if ($nRating > 1 && $nRating <= 2) {
                $finalratng = '2';
                $ratvalue   = "rate2";
            }
            if ($nRating > 2 && $nRating <= 3) {
                $finalratng = '3';
                $ratvalue   = "rate3";
            }
            if ($nRating > 3 && $nRating <= 4) {
                $finalratng = '4';
                $ratvalue   = "rate4";
            }
            if ($nRating > 4 && $nRating <= 5) {
                $finalratng = '5';
                $ratvalue   = "rate5";
            }
?>
<li style=" width: 275px;">    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>

<span class="fl"><?=$minmax['min_value']; ?></span>
<span class="fr"><?=$minmax['max_value']; ?></span>

</li>

</ul>
</div>

<div class="be_infobasic">
<ul>
<li class="be_infoHead" style="">
<strong>Monthly keeping cost</strong>
<ul>
<li class="be_subhead"><strong>Premium<a style='cursor:pointer' id='This is an average monthly expense for keeping this breed. This includes an approximation cost of premium dry dog food, grooming expenses and the vet bills incurred in a month for an adult dog of the breed' title='Average for the breed' class="tooltip">*</a></strong> </li>
<li class="be_subhead" style="border-right:0px;"><strong>Standard<a style='cursor:pointer' id='This is an average monthly expense for keeping this breed. This includes an approximation cost of regular dry dog food, grooming expenses and the vet bills incurred in a month for an adult dog of the breed' title='Average for the breed' class="tooltip">*</a></strong> </li>
</ul>
</li>
</ul>

<ul>

<li style="padding: 1px 0;"><img src="<?=$siteURL?>/new/breed_engine/images/be_rupee.png" width="12" style="margin-top:5px; margin-right:3px;" alt="Rupee" title="Rupee"/><?= number_format($costpr); ?></li>
<li><img src="<?=$siteURL?>/new/breed_engine/images/be_rupee.png" width="12" style="margin-top:5px; margin-right:3px;" alt="Rupee" title="Rupee" /><?= number_format($costst); ?></li>
</ul>
</div>
</div>
</div>
<div class="cont980">

<div class="container" id="tabs">
<ul class="tabs">
    <li class="active" onclick="moveToFooter('tabs-1')" style="cursor:pointer"><a>Breed Info</a></li>
      <li onclick="moveToFooter('tabs-8')" style="cursor:pointer"><a>Maintenance & Effort</a></li>
       <li onclick="moveToFooter('tabs-9')" style="cursor:pointer" ><a>Hair & Coat</a></li>
       <li onclick="moveToFooter('tabs-10')" style="cursor:pointer"><a>Health</a></li>
       <li onclick="moveToFooter('tabs-11')" style="cursor:pointer"><a>Behavior </a></li>
        <li class="be_tab" style="margin-right:0px;"><a style="cursor:pointer; padding:0 14px 0 15px;"> More</a>
  <ul>
  <li onclick="moveToFooter('tabs-6')" style="cursor:pointer"><a>Breeding </a></li>
  <li onclick="moveToFooter('tabs-7')" style="cursor:pointer"><a>Appearance </a></li>
  </ul>
  </li>
      </ul>
  <div class="tab_container">
      <?
$selectdata = mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' ORDER BY att_id DESC");
while ($selectdata1 = mysql_fetch_array($selectdata)) {
    $att_id1[] = $selectdata1['att_id'];
    $att_value = $selectdata1['value'];
}
?>
<div id="tabs-33" class="tab_content" name="tabs-33" style="margin-top:25px;">
      <p align="center">
<?
$photoskey = "11";
$type="breed";
$height="";
$width="680px";
$item_show="5";
$has_data="yes";
$page_name="product-page";
//$txt_type="featured";
/*$page_cmp="footer-product";*/
$cattype='1';
include($DOCUMENT_ROOT . "/new/includes/featured-products1.php");
?>
  </p>
  </div>
 <div id="tabs-1" class="tab_content" name="tabs-1">
  <div class="be_detailTxt">
  <div class="" style="">
   
    <h2 class="headingBreed_be"><?=$be_name;?> Dog Breed Information</h2>
    <div class="be_attSec">
    <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '37' || $att_id == '3' || $att_id == '14' || $att_id == '27') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
                    <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1']/1000)*100);
         ?> 

<ul>
  <li><!--<span><img src="images/detail_icon8.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border">
  <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?= $attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
  </li>
  </ul>

<?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
 <ul>
  <li><!--<span><img src="images/detail_icon8.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?><? if($att_id=='27'){ echo "<a style='cursor:pointer' class='tooltip' id='This is an average cost of getting this breed of puppy home, it may vary from breeder to breeder. though, if you want then you can always adopt a dog' title='Average for the breed'>*</a>";} ?><? if($att_id=='37'){ echo "<a style='cursor:pointer' id='Average for the breed' class='tooltip' title='Average for the breed'>*</a>";} ?></label></li>
  <li class="no_border">
  <?= $valuename['value']; 
  if($att_id=='27'){ ?>
  <? if($valuename['value']=='Economical'){ $tit_id='(upto Rs 5,000)';} else if($valuename['value']=='Pocket Friendly'){ $tit_id='(Rs 10,000 - Rs 20,000)';} else if
($valuename['value']=='Expensive'){$tit_id='(Rs 25,000 - Rs 30,000)';} else{$tit_id='(Rs 35,000 - Rs 50,000)';}?><a style="cursor:pointer" class='tooltip' id="<?=$tit_id?> approximate cost,which can very according to the puppy and the location." title="<?=$tit_id?> approximate cost,which can very according to the puppy and the location.">*</a> <? }
 ?> </li>
  </ul>
  <?
        }
    }
}
?>
</div>
</div>

<?

$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (2,3,5,7,15,16)  ORDER BY  title_id ASC");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
    
    if ($articletitleid != '15' && $articletitleid != '16' && $articletitleid != '7' ) {
?>
<strong><?= stripcslashes(htmlspecialchars_decode($selecttitlename['title'])); ?></strong>
<?=htmlspecialchars_decode(stripallslashes($articlecontent)); ?>

<?
    }
?>

<?
    if ($articletitleid == '15' || $articletitleid == '16'){
?>
<?
        if ($articletitleid == '15') {
?>
<div class="be_pros" style=" border-right:0px">
<strong>Pros</strong>
<ul>
<li><?= $articlecontent = preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', stripallslashes(htmlspecialchars_decode($articlecontent))); ?></li>
</ul>
</div>
<?
        } else {
?>
<div class="be_pros" >
<strong>Cons</strong>
<ul>
<li><?= $articlecontent = preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', stripallslashes(htmlspecialchars_decode($articlecontent))); ?></li>
</ul>
</div>
<?
        }
?>
<?
    }
	if($articletitleid=='7'){ ?>
    <div class="be_detailTxt be_funBox">
 <div class="be_funImg"><img src="<?=$siteURL?>/new/breed_engine/images/mask1.png" width="66" height="66" alt="" /></div>
<?php /*?><h2>  Fun Trivia</h2>
<?php */?><h3><?=$selecttitlename['title']; ?></h3>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
</div>
	
	<? }
}
?>
 </div>   
  </div>
<div style="float:left; margin-top:15px;">
     <? 
						   
                            $surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        	include($DOCUMENT_ROOT."/Adoption1/social1_article.php"); 
                        ?>
                        </div>
  <?
  if($breednice=='indian-pariah-dog'  || $breednice=='jack-russell-terrier'){
	  //------------------------------------ 
	$selalb2=mysql_query("SELECT * FROM dogs_available WHERE breed_nicename='$breednice' AND publish_status='publish' AND dog_image !='' ORDER BY cdate DESC LIMIT 8");  
	  ?>
<div class="be_slideBox">
<div class="be_funImg" style="margin: -5px 10px 0 16px"><img src="<?=$siteURL?>/new/breed_engine/images/gall.png" width="68" height="57" alt="" /></div>
<h3><?=$be_name ?> Photos from our collection</h3>
  <ul>
 <? 
	while ($row = mysql_fetch_array($selalb2)) {
	$dog_id  = $row["dog_id"];
	$dog_name  = ucfirst(strtolower($row["dog_name"]));
	$dog_image = $row["dog_image"];
	$dog_nicename = $row["dog_nicename"];
	$breed_nicename = $row["breed_nicename"];
	$dog_name = stripslashes($dog_name);
 
	$img_src = $DOCUMENT_ROOT."/dogs/images/thumb_$dog_image";

	
	if($dog_image){
		$src = $DOCUMENT_ROOT.'/dogs/images/'.$dog_image;
		$imageURL='/dogs/images/150-150-'.$dog_image;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	if(file_exists($src)){
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'150','150','ratiowh');
$compressed = compress_image($dest,$dest, 50);
 ?>
  <li><a href="/dogs/<?=$dog_nicename?>/" target="_blank" ><img src="/dogs/images/150-150-<?=$dog_image?>" alt="<?=$dog_image?>" title="<?=$dog_name?>" border="0"  width="150px;"/></a>
</li>
<? } }?>
  </ul>
  </div>
	<?  //------------------------
  }else{
  $v=0;$cc=0;
 if($breed_id=='636'){
	 $query="dachshund";
 }
 else if($breed_id=='494'){
	 $query="chihuahua";
 }
 else if($breed_id=='58'){
	 $query="fox terrier weired";
 }
  else if($breed_id=='627'){
	 $query="cane";
 }  else if($breed_id=='60'){
	 $query="German Shepherd";
 }  else if($breed_id=='96'){
	 $query="Neapolitan Mastiff";
  } else if($breed_id=='104'){
	 $query="Pit Bull";
 }else if($breed_id=='93'){
	 $query="English Mastiff";
 }
 else if($breed_id=='26'){
	 $query='-"French%20Bull%20Dog"'.'"bulldog"';
 }else if($breed_id=='68'){
	 $query="spitz";
 }
 else{
  	$query=trim($breedname);
 }
	$query = stripslashes(str_replace(" ","%20AND%20",$query));//&sort=no_views desc
	
	//$url = "http://101.53.137.39/solr/dogspotphotos/select/?q=$query&sort=cdate+desc&version=2.2&start=1&rows=10000&indent=on&df=text&wt=xml&indent=true";
$url = "http://localhost/solr/dogspotphotos/select/?q=$query&wt=xml&mm=0&indent=true&defType=dismax&qf=title%5E2+text%5E1&sort=cdate+desc&start=1&rows=300";
if($userid=='sneha-upreti')
	{
		echo $url;
	}
  //	$url = "http://101.53.137.39/solr/dogspotphotos/select/?q=$query&sort=cdate+desc&version=2.2&start=1&rows=10000&indent=on&df=text&wt=xml&indent=true";
	//echo"<hr>$url<hr>";
	$result = get_solr_result($url);
	$totrecord = $result['TOTALHITS'];
   if($totrecord > 0){?>
  <div class="be_slideBox">
  <div class="be_funImg" style="margin: -5px 10px 0 16px"><img src="<?=$siteURL?>/new/breed_engine/images/gall.png" width="68" height="57" alt="" /></div>
<h3><?=$be_name ?> Photos from our collection</h3>
  <ul>
  
  <? 
	$check_ex=0;
	$count_images='0';
	$check_random_img='0';
	$img_dis='0';
	// check for exhibition id n show id images----------------------------------------------------------------
	foreach($result['HITS'] as $row){
	$selalb=mysql_query("SELECT * FROM photos_album WHERE album_link_name='event' AND album_id='$row[album_id]'");
	$count_alb=mysql_num_rows($selalb);
	if($count_alb > 0){
	$album_id[] = $row["album_id"];
	$selimg=query_execute_row("SELECT image_desc FROM photos_image WHERE image_id='$row[image_id]'");
		if((strpos($selimg['image_desc'], "ex-")!== false) && (strpos($selimg['image_desc'], "Lineup")== '0') && (strpos($selimg['image_desc'], "BIS")== '0') && (strpos($selimg['image_desc'], "BOB")== '0')){
		$Aimage[]=$row["image_id"];
		$arr[]=$row["album_id"]."@@".$row["image_id"]."@@".$row["image_nicename"]."@@".$row["image"];	
		$check_ex=1;$count_images++;
		}

	}
	}
	// if exhibition id n show id images are not found----------------------------------------------------------------
	if($check_ex=='0' || $count_images < 20){	
	foreach($result['HITS'] as $row){
	$selalb=mysql_query("SELECT * FROM photos_album WHERE album_link_name='event' AND album_id='$row[album_id]'");
	$count_alb=mysql_num_rows($selalb);
	if($count_alb > 0){
	$count_images++;
	$album_id[] = $row["album_id"];
	$Aimage[]=$row["image_id"];
	$arr[]=$row["album_id"]."@@".$row["image_id"]."@@".$row["image_nicename"]."@@".$row["image"];	
	}	
	}
	}


	$Aalbum_id=array_unique($album_id);
	asort($Aalbum_id);
	$check_album_count=count($Aalbum_id);
	if($check_album_count<4){
		if($breednice=='rampur-hound'){
		$num_images='4';
		}elseif($breednice=='dogo-argentino')
		{
			$num_images='4';
		}else{
		$num_images='8';
		}
	}elseif($check_album_count==5 && $breednice=='rajapalayam'){
		if($breednice=='rajapalayam'){
		$num_images='4';
		}
	}else{
		$num_images='2';
	}
	//print_r($arr);
	if($count_images < 20){
	$check_random_img_var='1';
	}else{
	$check_random_img_var='2';
	}
	$arr=array_unique($arr);
	foreach($Aalbum_id as $rows){
	foreach($arr as $row1){
			$row11=explode("@@",$row1);
			if($rows==$row11[0]){	
		$check_random_img++;
	//$albumname = query_execute_row("SELECT image_id,image, image_nicename FROM photos_image WHERE album_id = '$rows' AND image_desc like '%sw%'");		
	 
	 $image_id = $row11[1];
	 $image = $row11[3];
	 $image_nicename = $row11[2];
	 $img_name = $DOCUMENT_ROOT."/photos/images/thumb_$image";
     $cc++;
	 if($check_random_img==$check_random_img_var){
	 $check_random_img='0';
	 $v++;$cc++;	
	 $img_dis++; 
	if(file_exists($img_name)){
		
	$imgWH = @WidthHeightImg($img_name,'150','');
	//$src = $DOCUMENT_ROOT."/photos/images/$image";
	//$destm = $DOCUMENT_ROOT.'/event_images/thumb_'.$image; 
	//createImgThumbIfnot($src,$destm,'150','','ratiowh');
	}else{
		$imgWH[0] = 150;
		$imgWH[1] = 86;
	}
	if(($breednice=='rampur-hound' || $breednice=='mudhol-hound' || $breednice=='weimaraner' || $breednice=='chippiparai' || 
	$breednice=='border-collie' || $breednice=='alaskan-malamute') && $img_dis<='4'){
		if($image!='kanpur-dog-show-2011_156.jpg' && $image !='kanpur-dog-show-2011_155.jpg'){
	 ?>
  <li><a href="/photos/<?=$image_nicename; ?>/" target="_blank" ><img src="<? echo"/photos/images/thumb_$image";?>" title="<?=$image?>" alt="<?=$image?>" border="0"  width="<? echo"$imgWH[0]";?>"/></a>
</li>
  <? 
		}
	}
if($img_dis<='8' && ($breednice!='rampur-hound' && $breednice!='mudhol-hound' && $breednice!='weimaraner' &&  $breednice!='chippiparai' && 
$breednice!='border-collie' && $breednice!='alaskan-malamute')){
 ?>
  <li><a href="/photos/<?=$image_nicename; ?>/" target="_blank" ><img src="<? echo"/photos/images/thumb_$image";?>" title="<?=$image?>" alt="<?=$image?>" border="0"  width="<? echo"$imgWH[0]";?>"/></a>
</li>
  <?  }
			}}if($v==$num_images){
				$v='0';
				break;
			}
}
if($cc=='32'){break;}
}
if($breednice=='border-collie'){
?>	
<li><a href="/photos/bangalore-dog-show_686/" target="_blank" ><img src="/photos/images/thumb_bangalore-dog-show_686.jpg" title="bangalore-dog-show_686" alt="bangalore-dog-show_686" border="0" height="101"  width="150"/></a>
</li>
<li><a href="/photos/bangalore-dog-show_683/" target="_blank" ><img src="/photos/images/thumb_bangalore-dog-show_683.jpg" title="bangalore-dog-show_683" 
alt="bangalore-dog-show_683" border="0" height="101"  width="150"/></a>
</li>
<? }
?>
  </ul>
  </div>
    <? }}?>
    <!---------------------------------------------------solr banner--------------------------------------------------------->
    
 
    <!------------------------------------------------------end-------------------------------------------------------------->
    
    
    <div id="tabs-8" class="tab_content" name="tabs-2">
 
  <div class="be_detailTxt">
  <!--<h2>Temperament</h2>-->
  
  <h3><?=$breedname ?> Maintenance & Effort</h3>
  <form name="formcomnt" id="formcomnt">
  <div class="be_attSec">
   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '22' || $att_id == '28' || $att_id == '7'  || $att_id == '34' || $att_id == '16' || $att_id == '17') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
          
?> 

 <ul>
  <li><!--<span><img src="images/detail_icon8.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?= $attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
 <ul>
   <li><!--<span><img src="images/detail_icon7.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?><? if($att_id=='11'){echo "<a style='cursor:pointer' id='Average for the breed' title='Average for the breed'>*</a>";} ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
   </ul>
<?
        }
    }
}
?>
</div>
  </form>

<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id ='9'");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>
<strong><?= $selecttitlename['title']; ?></strong>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>  
</div>
    <div id="tabs-9" class="tab_content" name="tabs-3">
     <div class="be_detailTxt">
 <!-- <h2>Grooming</h2>-->
  <h3><?=$breedname ?> Hair & Coat</h3>
  <div class="be_attSec">
     <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '24' || $att_id == '25' || $att_id == '9' || $att_id == '10' || $att_id == '46') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
    <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
            
?> 

 <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?= $attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
  </li>
  </ul><?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
  <li class="no_border"><?= $valuename['value'] ?></li>
  </ul> <?
        }
    }
}
?>
</div>
 
  </div>
  </div>
 
  <div id="tabs-10" class="tab_content" name="tabs-4">
  <div class="be_detailTxt">
<!--  <h2>Vet Bills</h2>-->
  <h3><?=$breedname ?> Health & Care</h3>
  <div class="be_attSec">
   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '40' || $att_id == '47') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");     
?>
                <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);

?> 

   <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?= $attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?></label></li>
  <li class="no_border"><?= $valuename['value'] ?></li>
  </ul>
<?
        }
    }
}
?>
</div>
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id ='8'");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>
<strong><?= $selecttitlename['title']; ?></strong>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>

  </div>  
  </div>
  
     <!---------------------------------------------------solr banner--------------------------------------------------------->
    
      <p align="center">
  </p>
<!------------------------------------------------------end-------------------------------------------------------------->
<div id="tabs-11" class="tab_content" name="tabs-5">
 <div class="be_detailTxt">
 <!-- <h2>Body Features</h2>-->

  <h3><?=$breedname ?> Behavior</h3>
<div class="be_attSec">  
   <?
   foreach ($att_id1 as $att_id){
	   if ($att_id == '21' ||  $att_id == '36') {
		   $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
		    $qRATING = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
		   $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>

  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
   }
foreach ($att_id1 as $att_id) {
 if ($att_id == '1' || $att_id == '2'  || $att_id == '4' || $att_id == '5' || $att_id == '18' || $att_id == '33' || $att_id == '19' 
 || $att_id == '12' || $att_id == '13' || $att_id == '43' || $att_id == '44') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
           <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20' || $att_id == '43' || $att_id == '44' || $att_id == '33') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
 
?> 
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?> </label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?= $attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
    }
}
?>
</div>
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (6,11,10)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>

<strong><?= $selecttitlename['title']; ?></strong>
<?= stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>
 </div>
  </div>
  <div id="tabs-6" class="tab_content" name="tabs-6">
 <div class="be_detailTxt">
 <!-- <h2>Body Features</h2>-->
  <h3><?=$breedname ?> Breeding</h3>
  <div class="be_attSec">
   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '26' || $att_id == '38') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
           <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating =(($qRATING['value1'] / 1000)*100);;
           
?> 

  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?> </label></li>
    <li class="no_border">
   <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?= $attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
    }
}
?>
</div>
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (12,13)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>

<strong><?= $selecttitlename['title']; ?></strong>
<?=stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>
 </div>
  </div>
 <div id="tabs-7" class="tab_content" name="tabs-7">
 <div class="be_detailTxt">
 <!-- <h2>Body Features</h2>-->
  <h3><?=$breedname ?> Appearance</h3>
  <div class="be_attSec">
   <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '8' || $att_id == '6' || $att_id == '29' || $att_id == '31' || $att_id == '42' || $att_id == '48' || $att_id == '23') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>
           <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') {
            //if($qRATING['value']!='0' && $qRATING['value']<='9') {
            $nRating = (($qRATING['value1'] / 1000)*100);
           
?> 

  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits'] ?> </label></li>
    <li class="no_border">
    <div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="fl"><?= $attname['min_value'] ?></span>
<span class="fr"><?=$attname['max_value'] ?></span>
  </li>
  </ul> <?
        } else {
            $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?>
  <ul>
   <li><!--<span><img src="images/detail_icon3.jpg" width="52" height="50" alt="" /></span>-->
  <label><?= $attname['traits']; ?></label></li>
  <li class="no_border"><?= $valuename['value']; ?></li>
  </ul>
  <?
        }
    }
}
?>
</div>
   
    
<?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (14)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?>
<strong><?= $selecttitlename['title']; ?></strong>
<?=stripallslashes(htmlspecialchars_decode($articlecontent)); ?>
<?
}
?>
</div>
</div>     
</div>
</div>
</div>
<div class="be_linkBox">
<? if($countrelated > 0){ ?>
<div class="be_ad_banner">
<h4>Related Breeds</h4>
<ul>
<? foreach($relatedarray as $relbrd1){ 
$rltdbrdname = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$relbrd1'");
$rltdbreedname1      = $rltdbrdname['be_name'];
$image_name          = $rltdbrdname['image_name'];
$rltdbreednice       = $rltdbrdname['nicename'];
	if($image_name){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$image_name;
		$imageURL='/new/breed_engine/images/dog_images/35-33-'.$image_name;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,'35','33','ratiowh');
$compressed = compress_image($dest,$dest, 50);
?>
<li class="be_rldBreed"><a href="/<?=$rltdbreednice;?>/" target="_blank" ><label><?=$rltdbreedname1;?></label>
<span class="be_relimg"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-<?= $image_name ?>" alt="<?=$rltdbreedname1; ?>" title="<?=$rltdbreedname1; ?>" ></span>
</a></li>
<? } ?> 
</ul>
</div>

<div class="be_ad_banner">
<div class="recom"><a href="/dog-names/<?=$breednice;?>/">Recommended Name for <?=$be_name;?></a></div>

</div>
<div class="be_ad_banner">
<h4>Compare Related Breeds</h4>
<ul>
<? foreach($relatedarray as $relbrd){ 
$rltdbrdname = query_execute_row("SELECT * FROM dog_breeds WHERE breed_id='$relbrd'");
$rltdbreedname       = $rltdbrdname['be_name'];
$rltdbreednice       = $rltdbrdname['nicename'];
$rltdimg             = $rltdbrdname['image_name'];
	if($rltdimg){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$rltdimg;
		$imageURL='/new/breed_engine/images/dog_images/80-77-'.$rltdimg;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;
	
		createImgThumbIfnot($src,$dest,'80','77','ratiowh');
		$compressed2 = compress_image($dest,$dest, 40);
		if($img){
		$src = $DOCUMENT_ROOT.'/new/breed_engine/images/dog_images/'.$img;
		$imageURL='/new/breed_engine/images/dog_images/80-77-'.$img;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	$dest = $DOCUMENT_ROOT.$imageURL;

		createImgThumbIfnot($src,$dest,'80','77','ratiowh');
			$compressed3 = compress_image($dest,$dest, 50);
$checkstring=$breednice>$rltdbreednice;
?><a <? if($checkstring>0){?>href="/<?=$rltdbreednice;?>-vs-<?=$breednice;?>-compare/"<? }else{?>href="/<?=$breednice;?>-vs-<?=$rltdbreednice;?>-compare/"<? }?> target="_blank" title="<?=$be_name ?> vs <?=$rltdbreedname ?>" >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-<?= $img ?>" alt="<?=$be_name ?>" title="<?=$be_name ?>"  /> 
<?php /*?>  <label><?=$be_name ?></label><?php */?>
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-<?= $rltdimg ?>"  alt="<?=$rltdbreedname ?>" title="<?=$rltdbreedname ?>" /> 
<?php /*?>  <label> <?=$rltdbreedname ?></label><?php */?>
</div>

<div class="be_vsName"><?=$be_name ?> vs <?=$rltdbreedname ?></div>
</li></a>
<? } ?>
</ul>
 
<div class="be_more">
<a href="/compare-<?=$breednice;?>-with-related-breeds/" >More Comparisons</a>
</div>
</div>
<?
}
			//$dt='&sort=cdate desc';
/*			if($breed_id=="494"){
				//$url4 = "http://101.53.137.39/solr/dogspotdog/select/?q=Chihuahua AND publish_status:publish$dt&version=2.2&rows=6&fl=* score&qf=dog_name^2&df=text&wt=xml&indent=true";
				$url4=mysql_query("SELECT * FROM dogs_available WHERE breed_id='494' AND publish_status='publish' AND dog_image !='' ORDER BY cdate DESC LIMIT 8");
			}else{
	 		//$url4 = "http://101.53.137.39/solr/dogspotdog/select/?q=$breedname AND publish_status:publish$dt&version=2.2&rows=6&fl=* score&qf=dog_name^2&df=text&wt=xml&indent=true";
			$url4=mysql_query("SELECT * FROM dogs_available WHERE breed_nicename='$breednice' AND publish_status='publish' AND dog_image !='' ORDER BY cdate DESC LIMIT 8");
			}
			$countwag = mysql_num_rows($url4); */
			
?>
<?php /*?><div class="be_ad_banner be_wcBack">
<h4>Meet our other <?=$be_name;?> Members and Mutts</h4>
<? if($countwag > 0){ ?>
<ul>
<?
$countitem11='1';
$countitem22='0';
$counttotitems12='0';
	while($rowgroup=mysql_fetch_array($url4)){
	$dog_id  = $rowgroup["dog_id"];
	$dog_name  = ucfirst(strtolower($rowgroup["dog_name"]));
	$dog_color  = $rowgroup["dog_color"];
	$dog_sex = $rowgroup["dog_sex"];
	$dog_image = $rowgroup["dog_image"];
	$dog_owner = $rowgroup["userid"];
	$dog_nicename = $rowgroup["dog_nicename"];
	$breed_nicename = $rowgroup["breed_nicename"];
	$country = $rowgroup["country"];
	$state = $rowgroup["state"];
	$city = $rowgroup["city"];
	$pub_status=$rowgroup['publish_status'];
	$dog_name = stripslashes($dog_name);

$img_src = $DOCUMENT_ROOT."/dogs/images/thumb_$dog_image";
$imm =$DOCUMENT_ROOT. "/dogs/images/".$dog_image;
//echo $img_path;
	
	if(file_exists($imm)){
		$src = $DOCUMENT_ROOT.'/dogs/images/'.$dog_image;
		$imageURL='/dogs/images/80-77-'.$dog_image;
	}else{
		$src = $DOCUMENT_ROOT.'/wag_club/images/no_imgDog.jpg';
		$imageURL='/wag_club/images/80-77-no_imgDog.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'80','77','ratiowh');
if($imgWH[0]==''){
	$imgWH[0]='100';
}if($imgWH[1]==''){
	$imgWH[1]='87';
}
$counttotitems12++;
?>
<? if($countitem11=='1' && $countitem22=='0'){ ?>
<li>
<? } ?>
<div class="be_relatedCom">
 <a href="/dogs/<?=$dog_nicename?>/" class="be_waggie" target="_blank" > <img src="<?=$imageURL?>"  alt="<?=$dog_name?>" title="<?=$dog_name?>"></a>
 <a href="/dogs/<?=$dog_nicename?>/" target="_blank" >  <label><?=snippetwop(ucwords(strtolower($dog_name)),$length=15,$tail="...");?></label></a>
</div>
<?
$countitem11='3';
$countitem22++;
if($countitem22=='2'){ 
$countitem22='0';
?>
</li>
<? if($counttotitems12!='8'){?>

<li>
<? }}} ?>
</ul>
<? if($counttotitems12 == '8'){ ?>
<div class="be_browse"><a href="/wag_club/allsearch/<?=$breednice;?>/location/name/" >Browse for more »</a></div><? } ?>
<div class="be_more" style="border-top:1px solid #668000;">
<a href="/wag_club/add_dog.php" target="_blank" >
<div class="be_addWag">
<div class="be_Wagimg">
<img src="<?=$siteURL?>/new/breed_engine/images/plus.jpg" width="53" height="50" alt="Plus" title="Plus">
</div>
<div class="be_Wagtxt">Add your dog to Wag Club</div>
</div>
</a>
</div>


<? 
}else{?><a href="/wag_club/add_dog.php" target="_blank" >
<div class="be_addWag">
<div class="be_Wagimg">
<img src="<?=$siteURL?>/new/breed_engine/images/plus.jpg" width="53" height="50" alt="Plus" title="Plus">
</div>
<div class="be_Wagtxt">Add your dog to wag club</div>
</div>

</a>
<? }
?>
</div><?php */?>
<div class="be_ad_banner">
<h4>Browse by Groups</h4>
<ul>
<? while($sel_odr_grp_name1=mysql_fetch_array($sel_odr_grp_name)){ ?>
<a href="/<?=strtolower($sel_odr_grp_name1['value'])?>-group-dog-breeds/" style="cursor:pointer" target="_blank" ><li class="be_rldBreed"><label><?=$sel_odr_grp_name1['value']; ?></label>
<span class="be_relimg" style="display:none"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-beagle.jpg" alt="Beagle" title="Beagle"></span>
</li></a>
<? } ?>
</ul>
</div>
<div class="be_ad_banner">
<h4>Top Breeds</h4>
<ul>
<? if($section[0]!='german-shepherd-dog-alsatian'){ ?><a href="/german-shepherd-dog-alsatian/" target="_blank" >
<li>German Shepherd
<span class="be_relimg"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-german.jpg" alt="German Shepherd" title="German Shepherd" ></span>
</li></a>
<? } ?>
<? if($section[0]!='rottweiler'){ ?><a href="/rottweiler/" target="_blank" >
<li>Rottweiler
<span class="be_relimg"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-rott.jpg" alt="Rottweiler" title="Rottweiler" ></span>
</li></a>
<? } ?>
<? if($section[0]!='tibetan-mastiff'){ ?>
<a href="/tibetan-mastiff/" target="_blank" ><li>Tibetan Mastiff
<span class="be_relimg"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-tibetan-mastiff.jpg" alt="Tibetan Mastiff" title="Tibetan Mastiff" ></span>
</li></a>
<? } ?>
<? if($section[0]!='golden-retriever'){ ?>
<a href="/golden-retriever/" target="_blank" ><li>Golden Retriever
<span class="be_relimg"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-golden.jpg" alt="Golden Retriever" title="Golden Retriever" ></span>
</li></a>
<? } ?>
<? if($section[0]!='great-dane'){ ?>
<a href="/great-dane/" target="_blank" ><li>Great Dane
<span class="be_relimg"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/35-33-great-dan.jpg" alt="Great Dane" title="Great Dane" ></span>
</li></a>
<? } ?>

</ul>
</div>
<!-------------------------------------------------Top Breed Comparison--------------------------------------------------------->
<div class="be_ad_banner">
<h4>Top Breed Comparison</h4>
<ul>
<a href="/german-shepherd-dog-alsatian-vs-labrador-retriever-compare/" target="_blank" title="German Shepherd Dog (Alsatian) vs Labrador Retriever" >
<li class="be_comRel">
  <div class="be_relatedCom"><img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-german.jpg" alt="German Shepherd Dog (Alsatian)" title="German Shepherd Dog (Alsatian)"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-lab.jpg"  alt="Labrador Retriever" title="Labrador Retriever" /> 
</div>
<div class="be_vsName">German Shepherd Dog (Alsatian) vs Labrador Retriever</div>
</li>
</a>
<a href="/pit-bull-terrier-american-vs-rottweiler-compare/" target="_blank" title="Rottweiler vs Pit Bull Terrier (American)" >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-rott.jpg" alt="Rottweiler" title="Rottweiler"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pit-bull.jpg"  alt="Pit Bull Terrier (American)" title="Pit Bull Terrier (American)" /> 
</div>
<div class="be_vsName">Rottweiler vs Pit Bull Terrier (American)</div>
</li>
</a>
<a href="/rottweiler-vs-tibetan-mastiff-compare/" target="_blank" title="Tibetan Mastiff vs Rottweiler">
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-tibetan-mastiff.jpg" alt="Tibetan Mastiff" title="Tibetan Mastiff"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-rott.jpg" alt="Rottweiler" title="Rottweiler"  /> 
</div>
<div class="be_vsName">Tibetan Mastiff vs Rottweiler</div>
</li>
</a>
<a href="/golden-retriever-vs-labrador-retriever-compare/" target="_blank" title="Golden Retriever vs Labrador Retriever"  >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-golden.jpg" alt="Golden Retriever" title="Golden Retriever"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-lab.jpg"  alt="Labrador Retriever" title="Labrador Retriever" /> 
</div>
<div class="be_vsName">Golden Retriever vs Labrador Retriever</div>
</li>
</a>
<a href="/german-shepherd-dog-alsatian-vs-great-dane-compare/" target="_blank" title="Great Dane vs German Shepherd Dog (Alsatian)">
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-great-dan.jpg" alt="Great Dane" title="Great Dane"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-german.jpg" alt="German Shepherd Dog (Alsatian)" title="German Shepherd Dog (Alsatian)"  /> 
</div>
<div class="be_vsName">Great Dane vs German Shepherd Dog (Alsatian)</div>
</li>
</a>
<a href="/boxer-vs-dobermann-compare/" target="_blank" title="Dobermann vs Boxer" >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-doberman.jpg" alt="Dobermann" title="Dobermann"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-boxer.jpg"  alt="Boxer" title="Boxer" /> 
</div>
<div class="be_vsName">Dobermann vs Boxer</div>
</li>
</a>
<a href="/beagle-vs-pug-compare/" target="_blank" title="Pug vs Beagle" >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pug.jpg" alt="Pug" title="Pug"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-beagle.jpg"  alt="Beagle" title="Beagle" /> 
</div>
<div class="be_vsName">Pug vs Beagle</div>
</li>
</a>
<a href="/beagle-vs-labrador-retriever-compare/" target="_blank" title="Beagle vs Labrador Retriever" >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-beagle.jpg"  alt="Beagle" title="Beagle" /> 
</div>

<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-lab.jpg"  alt="Labrador Retriever" title="Labrador Retriever" /> 
</div>

<div class="be_vsName">Beagle vs Labrador Retriever</div>
</li>
</a>
<a href="/pomeranian-vs-pug-compare/" target="_blank" title="Pomeranian vs Pug" >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pom.jpg" alt="Pomeranian" title="Pomeranian"  /> 
</div>
<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pug.jpg" alt="Pug" title="Pug"  /> 
</div>
<div class="be_vsName">Pomeranian vs Pug</div>
</li>
</a>
<a href="/pug-vs-shih-tsu-compare/" target="_blank" title="Shih Tzu vs Pug" >
<li class="be_comRel">
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-shih-tzu.jpg"  alt="Shih Tzu" title="Shih Tzu" /> 
</div>

<div class="be_relatedCom" style="width:20px;padding-top: 30px;">
  <label> vs </label>
</div>
<div class="be_relatedCom">
  <img src="<?=$siteURL?>/new/breed_engine/images/dog_images/80-77-pug.jpg" alt="Pug" title="Pug"  /> 
</div>
<div class="be_vsName">Shih Tzu vs Pug</div>
</li>
</a>
</ul>
</div>
<!---------------------------------------------------------------Ends Here----------------------------------------------->

<?
/*$st='&sort=c_date desc';
if($breed_id=="494"){
$url1 = "http://localhost/solr/dogspotarticles/select/?q=Chihuahua&fq=publish_status:publish$st&version=2.2&rows=100&fl=* score&qf=title_tag^2&df=text&wt=xml&indent=true";
}else{
$url1 = "http://localhost/solr/dogspotarticles/select/?q=$breedname&fq=publish_status:publish$st&version=2.2&rows=100&fl=* score&qf=title_tag^2&df=text&wt=xml&indent=true";
}
$url1 = str_replace(" ","%20",$url1);
$artresult = get_solr_result($url1);
$totrecordart = $artresult['TOTALHITS'];*/
//if($totrecordart > 0){
?>
<? /*if($userid !='95133'){?>
<div class="be_ad_banner">
<h4>Articles</h4>
<ul>
<?	$s=0;
	foreach($artresult['HITS'] as $rowArt){
		if($s==5){break;}
		 $s             = $s + 1;
			$articlecat_id = $rowArt["articlecat_id"];
			$article_id    = $rowArt["article_id"];
			$art_subject   = $rowArt["art_subject"];
			$art_body1      = $rowArt["art_body"];
			$c_date        = $rowArt["c_date"];
			$artuser       = $rowArt["userid"];
			$art_name      = $rowArt["art_name"];
			$rowUser34 = query_execute_row("SELECT art_body FROM articles WHERE article_id='" . $rowArt['article_id'] . "'");
			$art_body      = $rowUser34["art_body"];
			$art_subject = stripslashes($art_subject);
			$art_subject = breakLongWords($art_subject, 30, " ");
			
			// Get Post Teaser
			$art_body = stripslashes($art_body);
			$art_body = strip_tags($art_body);
			$art_body = trim($art_body);
			$art_body = substr($art_body, 0, 60);
			
			$art_body = stripslashes($art_body);
			$art_body = breakLongWords($art_body, 30, " ");

		?>
<li class="be_comRel">
<strong><a href="/<?=$art_name?>/" target="_blank" ><?=$art_subject; ?></a></strong>
                       <p><?
    echo "$art_body...  <a href='/$art_name/' class='read'>read more</a> ";
?></p>
</li>
<? } ?>
</ul>

<div class="be_more">
<a href="/dog-blog/search/<?=$breedname?>" rel="nofollow" >Browse for more articles</a>
</div>
</div>

<?
}else{*/
	?>
    <div class="be_ad_banner">
<h4>Articles</h4>
 <?     $article=query_execute("SELECT DISTINCT(wpp.ID), wpp.post_title,wpp.post_name,wpp.post_content FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_content LIKE '%$breedname%' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_date>'2014-01-01 00:00:00' AND wpp.ID NOT IN (16616,15348,17664,4773,2125) ORDER BY wpp.ID DESC LIMIT 7");
		$count=mysql_num_rows($article);
		if($count<3){
		$breedhalfDog=explode(' ',$breedname);
		$breedhalf=	$breedhalfDog[0];
		if(!$breedhalfDog[1])
		{
		$breedhalfDog=explode('.',$breedname);
		$breedhalf=	$breedhalfDog[1];	
		}
		if($breedhalf){
	 $article=query_execute("SELECT DISTINCT(wpp.ID), wpp.post_title,wpp.post_name,wpp.post_content FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_content LIKE '%$breedhalf%' AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_date>'2014-01-01 00:00:00' AND wpp.ID NOT IN (16616,15348,17664,4773,2125) ORDER BY wpp.ID DESC LIMIT 7");
		}else
		{
		$article=query_execute("SELECT DISTINCT(wpp.ID), wpp.post_title,wpp.post_name,wpp.post_content FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE (wpp.post_content LIKE '%$breedname%' OR wpp.post_content LIKE '%cost%') AND wptr.object_id=wpp.ID AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.post_date>'2014-01-01 00:00:00' AND wpp.ID NOT IN (16616,15348,17664,4773,2125) ORDER BY wpp.ID DESC LIMIT 7");
		
		}
		}?>
			    <div class="youmaylike_post" >
			    <div style="z-index:0;" class="cycle-slideshow composite-example" 
    data-cycle-fx="scrollHorz" 
    data-cycle-slides="> div"
    data-cycle-timeout="2000"
    ><?
               while($rowArt=mysql_fetch_array($article)){
				$post_name   = $rowArt["post_title"];
				$cat_name=$rowArt['post_name'];
				$main_fea_art_body=$rowArt['post_content'];
				$main_fea_art_id=$rowArt['ID'];
			$feat_imgURL    = get_first_image($main_fea_art_body, $DOCUMENT_ROOT);
	       $imgURLAbs = make_absolute($feat_imgURL, 'https://www.dogspot.in');
		   $feat_check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$main_fea_art_id' AND post_type='attachment' AND post_mime_type like '%image%' LIMIT 5");
			
		if (strpos($imgURLAbs,'wordpress') !== false) {
    	$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/200x190-'.$image_name;
		}
		else if($feat_check_img['guid'] !=''){
		$imgURLAbs=$feat_check_img['guid'];	
		$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $DOCUMENT_ROOT.$exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/200x190-'.$image_name;
		}
   else if ($imgURLAbs!='https://www.dogspot.in/new/pix/dogspot-logo-beta.gif') {
	   $URL=$imgURLAbs;
	   $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
	   $pos = strrpos($image_name,'/');
	   $image_name = substr($image_name,$pos+1);
		$extension = stristr($image_name,'.');
		if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
		$image_name;
		}
		$src = $DOCUMENT_ROOT.'/userfiles/images/'.$image_name;
		//$src = $imgURLAbs;
		$imageURL='/imgthumb/200x190-'.$image_name;
		} 
		else if($rowUser1["image"]) {
		$src = $DOCUMENT_ROOT.'/profile/images/'.$rowUser1["image"];
		$imageURL = '/imgthumb/200x190-'.$rowUser1["image"];
		} 
		else  {
        $src = $DOCUMENT_ROOT.'/images/black_dog_02_139885.jpg';
		$imageURL = '/imgthumb/200x190-black_dog_02_139885.jpg';
		}
		// Get Image Width and Height, Updated date: 18-Nov-2014
		$image_info02 = getimagesize($DOCUMENT_ROOT.$imageURL);		
		$image_width02 = $image_info02[0];
		$image_height02 = $image_info02[1];
		// End
		$dest = $DOCUMENT_ROOT.$imageURL;
		createImgThumbIfnot($src,$dest,'200','190','ratiowh');
				?>
                
    <div >
		  <a href="/<?=$cat_name?>/"> 
		  	<img src="<?=$imageURL?>" alt="<?=$cat_name; ?>" title="<?=$cat_name ?>" style="width:268px;height:205px;" width="<?= $image_width02 ?>" height="<?= $image_height02 ?>">
		  </a>
			    <div class="cycle-overlay"><h4><?=snippetwop($post_name,$length=25,$tail="...");?></h4></div></div>
               <? }?>

</div>
</div>
	<?
//} //}
?>

	<?
			$qt='&sort=qna_cdate desc';
			if($breed_id=="494"){
				$url2 = "http://localhost/solr/dogspotqna/select/?q=Chihuahua&fq=publish_status:publish$bsql$qt&version=2.2&rows=5&fl=* score&qf=title_tag^2&df=text&wt=xml&indent=true";
			}else{
	 		$url2 = "http://localhost/solr/dogspotqna/select/?q=$breedname&fq=publish_status:publish$bsql$qt&version=2.2&rows=5&fl=* score&qf=title_tag^2&df=text&wt=xml&indent=true";
			}
			$url2 = str_replace(" ","%20",$url2);
			
			$resultqna = get_solr_result($url2);
			$totrecordqna = $resultqna['TOTALHITS'];
			if($totrecordqna > 0){
?>
</div>
<div class="be_ad_banner">
<h4>Q&A</h4>
<ul>
<? 			foreach($resultqna['HITS'] as $rowArtqna){
			  $i=$i+1;
            $qna_id = $rowArtqna["qna_id"];
            $qnauser = $rowArtqna["userid"];
            $qna_question1 = $rowArtqna["qna_question"];
            $qna_name = $rowArtqna["qna_name"];
            $qna_tag = stripslashes($rowArtqna["qna_tag"]);
            $qna_catid = $rowArtqna["qna_catid"];
            $qna_cdate = $rowArtqna["qna_cdate"];
			$rowUser341 = query_execute_row("SELECT qna_question FROM qna_questions WHERE qna_id='" . $rowArtqna['qna_id'] . "'");
			$qna_question      = $rowUser341["qna_question"];
                
           // $qna_question = makeClickableLinks($qna_question);
		   	$qna_question = stripslashes($qna_question);
			$qna_question = strip_tags($qna_question);
			$qna_question = trim($qna_question);
			$qna_question = substr($qna_question, 0, 60);
            $qna_question = nl2br(stripslashes($qna_question));
            $qna_question = breakLongWords($qna_question, 60, " ");
?><a href="<? echo"/qna/$qna_name/";?>" target="_blank" >
<li class="be_comRel">
<p><? echo"$qna_question"."..."; ?></p>
</li></a>
<? } ?>
</ul>
<div class="be_more">
<a href="/qna/search/<?=$breedname?>" rel="nofollow">Browse for more</a>
</div>
</div>
<? } ?>

</div>


<div class="cont980">
</div>
</div>
<?
 $titlewag=$selecttitle['title'];
 $summarywag=$selecttitle['description'];
 $urlwag="https://www.dogspot.in/$breednice/";
 $imagewag="https://www.dogspot.in/new/breed_engine/images/logo-300x300.jpg";
  ?>
</div>

<input type="hidden" name="user" id="user" value="<?=$userid;?>" />
 <script type="text/javascript">
jQuery(document).ready(function($){
	var valueurl="<?=$surl?>";
	ShaAjaxJquary("/share-update-des.php?surl="+valueurl+"", "#mis", '', 'formcomnt', 'GET', '#loading', '','REP');
});
</script>
<script defer="defer" type="text/javascript" src="/jquery/jquery.validate.min-1.9.js"></script>
<script defer="defer" type="text/javascript" src="/new/js/scrolltopcontrol.js?lp=1" ></script>

<script defer="defer" type="text/javascript"  src="/shop-slider/js/jquery.tabSlideOut.js"></script>

<link rel="stylesheet" href="../../adoption/css/stylef.css" />
<link rel="stylesheet" href="../../adoption/css/font-awesome.css" />
<link rel="stylesheet" href="/new/breed_engine/css/starmatrix1.css" />
<link href="/new/css/toll-tip.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/new/css/jquery-ui.css" />
<link rel="stylesheet" href="/new/css/breed-info-min.css" />
<script  defer="defer" type="text/javascript" src="/js/shaajax.min.2.1.js"></script>
<script defer="defer" src="/adoption/js/jquery.cycle2.js"></script>
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom.php');
?>