<?
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
$curentdate = date('Y-m-d H:i:s');
$ip = ipCheck();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Bulk Update Location ID</title>
	</head>

	<body>
		<h1>Bulk Update Location ID</h1>
        <font color="#FF0000">
            <p>Excel Sheet have only 2 columns</p>
            <h3>First Column: LOCATION ID</h3>
            <h3>Second Column: ITEM ID</h3>
        </font>
		<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
			<table border="0" width="100%" class="myClass tablesorter">   
                <tr>
                    <td width="50%">
                       <input type="file" name="xls_file" id="xls_file" />
                    </td>
                    <td>
                        <input type="submit" name="button" id="button" value="Upload" />
                    </td>
                </tr> 
        	</table>
            <hr />
        </form>
		<br/>
		<table width="100%" border="1" cellpadding="3" cellspacing="0">  
			<tr>
            	<td width="5%">S.No</td>
				<td width="30%">LOCATION ID</td>
				<td width="30%">ITEM ID</td>
                <td width="25%">STATUS</td>
			</tr>
			<?
			if($xls_file){
				if($_FILES["xls_file"]["error"] > 0){
					echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
				}
				if(file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
					echo $_FILES["xls_file"]["name"] . " already exists. ";
				}else{
					move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/".$_FILES["xls_file"]["name"]);
					chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
					$updone=1;
				}
			}
			if($updone==1){
	
				require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
				$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
				//$data = new Spreadsheet_Excel_Reader();
	
				//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
				$arr_location_id = "";
				for($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
					
					$location_id = $data->sheets[0]['cells'][$j+1][1];
		
					$item_id = $data->sheets[0]['cells'][$j+1][2];
			
					if($item_id && $location_id){
			
						$section_check = 0;
						
						$arr_location_id .= $location_id.", ";
						
						$SQ_rack_position = mysql_query("SELECT * FROM shop_rack_position WHERE location_id='$location_id' ORDER BY location_id ASC");
						$SQ_shop_items = mysql_query("SELECT * FROM shop_items WHERE item_id='$item_id' ORDER BY item_id ASC");
						
						if((mysql_num_rows($SQ_rack_position) > 0) && (mysql_num_rows($SQ_shop_items) > 0)){
							
							$UQ_stock = mysql_query("UPDATE shop_rack_position SET item_id='$item_id' WHERE location_id='$location_id'");
									
							//echo "UPDATE shop_rack_position SET item_id='$item_id' WHERE location_id='$location_id'";
							
							//echo "<br/>++++++++++++++++++++++++<br/>";
							
							$UQ_stock = mysql_query("UPDATE shop_items SET location_id='$location_id' WHERE item_id='$item_id'");
							
							//echo "UPDATE shop_items SET location_id='$location_id' WHERE item_id='$item_id'";
							
							//echo "<br/>++++++++++++++++++++++++<br/>";
							
							$section_check = 1;
							
							$msg = "Update Successfully";
										
						}
						
						else{
							$msg = "<font color='#FF0000'>Item ID Or Location ID not found in Rack</font>";
						}
					?>
					<tr valign="top">
						<td><?=$j?></td>
						<td><?=$location_id?></td>
						<td><?=$item_id?></td>
						<td><?=$msg?></td>
					</tr>
					<?
					$item_id='';
					$location_id='';
					$temp='';
				}
			}
			if($section_check == '1'){
				
				$idArray = rtrim($arr_location_id,", ");         // remove last extra comma
				
				$idArray1 = explode(",",$idArray);           // fetch one id from string of ids to insert into item_id col
		
				$IQ_section_review = query_execute("INSERT INTO section_reviews (section_review_id, userid, review_section_name, review_section_id, review_body, review_name, c_date, user_ip) VALUES ('', '$userid', 'Bulk-Upload', '$idArray1[0]', '$idArray[0]', 'Credit Notes', NULL, '$ip')");
					
				//echo "INSERT INTO section_reviews (section_review_id, userid, review_section_name, review_section_id, review_body, review_name, c_date, user_ip) VALUES ('', '$userid', 'Bulk-Upload', '$idArray1[0]', '$idArray', 'Location ID Update', NULL, '$ip')";
			}
		}
		?>
		</table>
	</body>
</html>