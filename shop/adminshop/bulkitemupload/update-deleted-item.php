<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Upload for Price Update</title>
</head>

<body>

<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>

<h1>Bulk Upload for Price Update</h1>

<h3> 1st Column should be Item ID </h3>
<h3> 2nd Column should be Supplier Code </h3>
<h3> 3rd Column should be Price</h3>
<h3> 4th Column should be Selling Price</h3>
<h3> 1st Column should be Buying Price </h3>
<h3> 1st Column should be Stock Status Choose From (active, delete, home) </h3>
<h3> 1st Column should be Visibility Choose From (visible, invisible) </h3>
<h3> File should be saved in "Excel 97-2003 Workbook" </h3>
<br />
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font><input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>

</form>
<hr />
<table width="728" border="1" cellpadding="3" cellspacing="0">  
  <tr>
    <td width="40">Item ID</td>    
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$item_id=$data->sheets[0]['cells'][$j+1][1];
$supplier_code=$data->sheets[0]['cells'][$j+1][2];
$price=$data->sheets[0]['cells'][$j+1][3];
$selling=$data->sheets[0]['cells'][$j+1][4];
$buying=$data->sheets[0]['cells'][$j+1][5];
$status=$data->sheets[0]['cells'][$j+1][6];
$visibility=$data->sheets[0]['cells'][$j+1][7];

if($item_id){
	//-------------------------Update Item------------------------------------------------------------------------------------	
//echo "UPDATE shop_items SET supplier_item_code='$supplier_code' WHERE item_id='$item_id' AND item_display_status='delete'";
$update_price = query_execute("UPDATE shop_items SET supplier_item_code='$supplier_code' WHERE item_id='$item_id' AND item_display_status='delete'");
if($price!=''){
	//echo "UPDATE shop_items SET price='$price', selling_price='$selling', buying_price='$buying' WHERE item_id='$item_id' AND item_display_status='delete'";
$update_price1 = query_execute("UPDATE shop_items SET price='$price', selling_price='$selling', buying_price='$buying' WHERE item_id='$item_id' AND item_display_status='delete'");
}else{
	echo "<font color='#FF0000'><strong>No price for item id .'$item_id'.</strong></font>";}
if($status!=''){
	if($status=='active' || $status=='delete' || $status=='home'){
	//echo "UPDATE shop_items SET stock_status='$status' WHERE item_id='$item_id' AND item_display_status='delete'";
$update_price1 = query_execute("UPDATE shop_items SET item_display_status='$status' WHERE item_id='$item_id' AND item_display_status='delete'");
	}}
if($visibility!=''){
	if($visibility=='visible' || $visibility=='invisible'){
	//echo "UPDATE shop_items SET visibility='$visibility' WHERE item_id='$item_id' AND item_display_status='delete'";
$update_price1 = query_execute("UPDATE shop_items SET visibility='$visibility' WHERE item_id='$item_id' AND item_display_status='delete'");
	}}
//echo "UPDATE shop_supplier_detail SET mrp_price='$price', selling_price='$selling', buying_price='$buying' WHERE item_id='$item_id'".";<br>";

	//-------------------------Update Item END------------------------------------------------------------------------------------	
?>
 <tr valign="top">
    <td><?=$item_id;?></td>
    <td><?=$supplier_code;?></td>
    <td><?=$price;?></td>
    <td><?=$selling;?></td>
    <td><?=$buying;?></td>
    <td><?=$status;?></td>
    <td><?=$visibility;?></td>
   
    
  </tr>
<?
}
$item_id='';
$supplier_code='';
$price='';
$selling='';
$buying='';
$status='';
$visibility='';
}
}
?>
</table>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/bottom.php'); ?>

</body>
</html>