<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Item Upload/Update</title>
</head>

<body>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" />

</form>
<a href="/shop/adminshop/sample-excel.php"> Sample Excel For Upload</a>
<hr />
<table border="1" cellpadding="3" cellspacing="0">  
  <tr>
  
    <td width="36">Item Id</td>
    <td width="56">Domain Id</td>
    <td width="79">Supplier Item Code</td>    
    <?php /*?><td>type_id</td>
    <td>item_parent_id</td><?php */?>
    <td width="32">name</td>
    <td width="41">Weight</td>
    <td width="45">item about</td>
    <td width="21">title</td>
    <td width="54">keyword</td>
    <td width="66">description</td>
    <td width="30">price</td>
    <td width="51">selling price</td>
    <td width="54">buying price</td>
    <td width="79">short description</td>
    <td width="66">specification</td>
    <td width="66">features</td>
    <td width="66">precautions</td>
    <td width="66">others</td>
    <td width="21">sku</td>
    <td width="50"> stock status</td>
    <td width="46">item brand</td>
    <td width="58">item supplier</td>
    <td width="48">item Status</td>
    <td width="63">Display Order</td>
   
    <td width="57">Breed Id</td>
    <td width="41">Breed Type</td>
    <td width="44">Life Stage</td>
    <td width="63">Category 1</td>
    <td width="63">Category 2</td>
    <td width="63">Category 3</td>
     <td width="73">Category 4</td>
     
     
     <td width="63">tax vat</td>
    <td width="63">tax added type(up/haryana)</td>
    <td width="63">tax added</td>
     <td width="73">tax inharyana</td>
     <td width="73">tax outharyana</td>
     <td width="73">payment mode(COD/NONCOD)</td>
     <td width="73">Master Category</td>
     <td width="73">Child Category</td>
      <td width="73">TYPE </td>
      <td width="73">Shipping Charges </td>
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
	
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$domain_id=$data->sheets[0]['cells'][$j+1][1];
$supplier_item_code=$data->sheets[0]['cells'][$j+1][2];
$name=$data->sheets[0]['cells'][$j+1][3];
$name = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $name);
$name=addslashes($name);
$skuname=$data->sheets[0]['cells'][$j+1][4];
$weight=$data->sheets[0]['cells'][$j+1][5];
$item_about=$data->sheets[0]['cells'][$j+1][6];
$item_about = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $item_about);
$item_about=addslashes($item_about);

$title=$data->sheets[0]['cells'][$j+1][7];
$title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $title);
$title=addslashes($title);

$keyword=$data->sheets[0]['cells'][$j+1][8];
$keyword = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $keyword);
$keyword=addslashes($keyword);

$description=$data->sheets[0]['cells'][$j+1][9];
$description = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $description);
$description=addslashes($description);

$specification=$data->sheets[0]['cells'][$j+1][10];
$specification = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $specification);
$specification=addslashes($specification);

$features=$data->sheets[0]['cells'][$j+1][11];
$features = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $features);
$features=addslashes($features);

$precautions=$data->sheets[0]['cells'][$j+1][12];
$precautions = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $precautions);
$precautions=addslashes($precautions);

$others=$data->sheets[0]['cells'][$j+1][13];
$others = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $others);
$others=addslashes($others);

$price=$data->sheets[0]['cells'][$j+1][14];
$selling_price=$data->sheets[0]['cells'][$j+1][15];
$buying_price=$data->sheets[0]['cells'][$j+1][16];
$short_description=$data->sheets[0]['cells'][$j+1][17];
$short_description = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $short_description);
$short_description=addslashes($short_description);

$stock_status=$data->sheets[0]['cells'][$j+1][18];
$sku_brand=$data->sheets[0]['cells'][$j+1][20];
$item_brand=$data->sheets[0]['cells'][$j+1][21];
$sku_supplier=$data->sheets[0]['cells'][$j+1][23];
$item_supplier=$data->sheets[0]['cells'][$j+1][24];
$sku_id=$data->sheets[0]['cells'][$j+1][25];
$item_status=$data->sheets[0]['cells'][$j+1][26];

$sku_category=$data->sheets[0]['cells'][$j+1][28];

$category_id[]=$data->sheets[0]['cells'][$j+1][29];
$category_id[]=$data->sheets[0]['cells'][$j+1][30];
$category_id[]=$data->sheets[0]['cells'][$j+1][32];
$category_id[]=$data->sheets[0]['cells'][$j+1][33];
$display_order=$data->sheets[0]['cells'][$j+1][34];
$breed_id=$data->sheets[0]['cells'][$j+1][35];
$breed_type=$data->sheets[0]['cells'][$j+1][36];
$life_stage=$data->sheets[0]['cells'][$j+1][37];

$tax_vat=$data->sheets[0]['cells'][$j+1][38];
$tax_added_type=$data->sheets[0]['cells'][$j+1][39];
$tax_added=$data->sheets[0]['cells'][$j+1][40];
$tax_inharyana=$data->sheets[0]['cells'][$j+1][41];
$tax_outharyana=$data->sheets[0]['cells'][$j+1][42];
$payment_mode1=$data->sheets[0]['cells'][$j+1][43];
$master_cat=$data->sheets[0]['cells'][$j+1][44];
$child_cat=$data->sheets[0]['cells'][$j+1][45];
$type=$data->sheets[0]['cells'][$j+1][46];
$shipping_amount=$data->sheets[0]['cells'][$j+1][47];
if($payment_mode1=='cod' || $payment_mode1=='COD'){$payment_mode=$payment_mode1;}else{$payment_mode='non-cod';}
$sku=$sku_supplier."_".$sku_category."_".$skuname."_".$sku_brand."_".$sku_id;
$sku=trim($sku);
$sku=str_replace(' ','_',$sku);

	
	if($price) {
	//-------------------------Insert Item------------------------------------------------------------------------------------	
	$nice_name = createSlug($name);
	$nice_name = checkSlugAll('shop_items', 'nice_name', $nice_name);
	
	if($item_parent_id != '0'){
		$item_parent_id = $itepid;
	}
	//echo "INSERT INTO shop_items (domain_id,supplier_item_code, type_id, item_parent_id, name, nice_name, item_about,weight, title, keyword, description, specification, features, precautions, others, price, selling_price, buying_price,short_description, sku,visibility,created_at, updated_at, qty,virtual_qty, stock_status, item_brand, item_supplier,discount, item_display_status, payment_mode,item_status,item_display_order,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana,master_cat,child_cat) 
										//VALUES ('$domain_id','$supplier_item_code', 'simple', '0', '$name', '$nice_name', '$item_about','$weight', '$title', '$keyword', '$description', '$specification', '$features', '$precautions', '$others', '$price', '$selling_price', '$buying_price','$short_description', '$sku','invisible', NULL, NULL, '0','0', '$stock_status', '$item_brand', '$item_supplier','1', 'active', '$payment_mode','$item_status','$display_order','$tax_vat','$tax_added_type','$tax_added','$tax_inharyana','$tax_outharyana','$master_cat','$child_cat')";
	 $str1="INSERT INTO shop_items (domain_id,supplier_item_code, type_id, item_parent_id, name, nice_name, item_about,weight, title, keyword, description, price, selling_price, buying_price,short_description, specification, features, precautions, others, sku,visibility,created_at, updated_at, qty,virtual_qty, stock_status, item_brand, item_supplier,discount, item_display_status, payment_mode,item_status,item_display_order,tax_vat,tax_added_type,tax_added,tax_inharyana,tax_outharyana,master_cat,child_cat,item_shipping_amount) 
										VALUES ('$domain_id','$supplier_item_code', '$type', '0', '$name', '$nice_name', '$item_about','$weight', '$title', '$keyword', '$description', '$specification', '$features', '$precautions', '$others', '$price', '$selling_price', '$buying_price','$short_description', '$sku','invisible', NULL, NULL, '0','0', '$stock_status', '$item_brand', '$item_supplier','1', 'active', '$payment_mode','$item_status','$display_order','$tax_vat','$tax_added_type','$tax_added','$tax_inharyana','$tax_outharyana','$master_cat','$child_cat','$shipping_amount')";
	//echo $str1."<br>";
	$qItem=query_execute($str1);
	$newitem_id = mysql_insert_id();
	
	$insrt_supplier_table=query_execute_row("INSERT INTO shop_item_supplier (item_id , supplier_id , supplier_item_code) VALUES ('$newitem_id' , '$item_supplier' , '$supplier_item_code')");
	
	if($type_id == 'configurable'){
		$itepid = $newitem_id;
	}
	$item_id=$newitem_id;
	//-------------------------Insert Item END------------------------------------------------------------------------------------	
	
}

	
	// Insert Item Category
	array_unique($category_id);	
	if($category_id){
		$delCat=query_execute("DELETE FROM shop_item_category WHERE item_id = '$item_id'");
		foreach($category_id as $cid){
			if($cid){
				$seletcat=query_execute_row("SELECT category_parent_id from shop_category WHERE  category_id = '$cid'");
				if($seletcat['category_parent_id']=='0') {
					$qCat=query_execute("INSERT INTO shop_item_category (category_type,category_id, item_id) 
									VALUES ('parent','$cid', '$item_id')");
				}
				else {
				$qCat=query_execute("INSERT INTO shop_item_category (category_type,category_id, item_id) 
									VALUES ('child','$cid', '$item_id')");
				}
			}
		if($cid=='15' || $cid=='65' || $cid=='67'){
			$a_breed_id = explode(",", $breed_id);
			  for ($c = 0; $c < sizeof($a_breed_id); $c++) {
				  $bred=$a_breed_id[$c]; 
				   
			 
			  $a_breed_type = explode(",", $breed_type);
			  for ($s = 0; $s < sizeof($a_breed_type); $s++) {
				  $type=$a_breed_type[$s];
				   
			  
			  
			  $a_stage = explode(",", $life_stage);
			  for ($f = 0; $f < sizeof($a_stage); $f++) {
				  $stage=$a_stage[$f];
					  $sagar=query_execute("INSERT INTO breed_shop_items(item_id,item_category_id,breed_id,breed_type,life_stage) VALUES('$newitem_id','$cid','$bred','$type','$stage')");
			  }
			  }
			  }
				
			}
			
		}
	}// End insert item Category
	
	?>
    
    
    
    
    <tr valign="top">
    <td><?=$newitem_id;?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][1];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][2];?></td>
    <td><?=$name;?></td>
     <td><?=$data->sheets[0]['cells'][$j+1][5];?></td>
    <td><?=$item_about?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][7];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][8];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][9];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][10];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][11];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][12];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][13];?></td>
    
    <td><?=$data->sheets[0]['cells'][$j+1][14];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][15];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][16];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][17];?></td>
    <td><?=$sku?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][18];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][19];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][22];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][26];?></td>
    
    <td><?=$data->sheets[0]['cells'][$j+1][34];?></td>
    
   
    <td><?=$data->sheets[0]['cells'][$j+1][35];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][36];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][37];?></td>
    
    <td><?=$data->sheets[0]['cells'][$j+1][29];?></td>  
    <td><?=$data->sheets[0]['cells'][$j+1][30];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][32];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][33];?></td>
     <td><?=$data->sheets[0]['cells'][$j+1][38];?></td>   
    <td><?=$data->sheets[0]['cells'][$j+1][39];?></td>  
    <td><?=$data->sheets[0]['cells'][$j+1][40];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][41];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][42];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][43];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][44];?></td>
    <td><?=$data->sheets[0]['cells'][$j+1][45];?></td>
     <td><?=$data->sheets[0]['cells'][$j+1][46];?></td>
       <td><?=$data->sheets[0]['cells'][$j+1][47];?></td>

    
  </tr>
    
    <?
	
	$skuname='';
	$sku_supplier='';
	$sku_category='';
	$sku_brand='';
	$sku_id='';
	$newitem_id='';
	$breed_id='';
	$breed_type='';
	$life_stage='';
	$supplier_item_code='';
	$type_id='';
	$item_parent_id='';
	$name='';
	$nice_name='';
	$display_order='';
	$weight='';
	$title='';
	$keyword='';
	$description='';
	$specification='';
	$features='';
	$precautions='';
	$others='';
	$price='';
	$selling_price='';
	$buying_price='';
	$price_type='';
	$short_description='';
	$sku='';
	$thumbnail='';
	$thumbnail_label='';
	$visibility='';
	$weight='';
	$weight_type='';
	$created_at='';
	$updated_at='';
	$qty='';
	$stock_status='';
	$item_brand='';
	$item_supplier='';
	$num_views='';
	$discount='';
	$category_id='';
	$attribute_value_id='';
	$attribute_set_id='';
	$item_display_status='';
	$payment_mode='';
	$item_status='';
	$item_image='';
	$qadd='';
	$master_cat='';
	$child_cat='';
	$type='';
	$shipping_amount='';
?>
 
<?
$item_id='';
$item_about='';	
}}
//}
?>
</table>
</body>
</html>