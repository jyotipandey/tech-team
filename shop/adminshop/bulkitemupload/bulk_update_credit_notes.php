<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
$curentdate = date('Y-m-d H:i:s');
$ip = ipCheck();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Bulk Update For Credit Notes</title>
	</head>

	<body>
		<h1>Bulk Update For Credit Notes</h1>
        <font color="#FF0000">
            <p>Excel Sheet have only 3 columns</p>
            <h3>First Column:  GRN ID</h3>
            <h3>Second Column: ITEM ID</h3>
            <h3>Third Column: ITEM QTY</h3>
        </font>
		<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
			<table border="0" width="100%" class="myClass tablesorter">   
                <tr>
                    <td width="50%">
                       <input type="file" name="xls_file" id="xls_file" />
                    </td>
                    <td>
                        <input type="submit" name="button" id="button" value="Upload" />
                    </td>
                </tr> 
        	</table>
            <hr />
        </form>
		<br/>
		<table width="82%"  border="1" cellpadding="3" cellspacing="0">  
			<tr>
            	
				<td width="8%">GRN ID</td>
				<td width="8%">ITEM ID</td>
				<td width="10%">ITEM QTY</td>
                <td width="50%">STATUS</td>
			</tr>
			<?
			if($xls_file){
				if($_FILES["xls_file"]["error"] > 0){
					echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
				}
				if(file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
					echo $_FILES["xls_file"]["name"] . " already exists. ";
				}else{
					move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
					chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
					$updone=1;
				}
			}
			if($updone==1){
	
				require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
				$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
				//$data = new Spreadsheet_Excel_Reader();
	
				//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
				$arr_item_id = array();
				for($j = 1; $j < $data->sheets[0]['numRows']; $j++){
					$grn_id = $data->sheets[0]['cells'][$j+1][1];
		
					$item_id = $data->sheets[0]['cells'][$j+1][2];
		
					$qty = $data->sheets[0]['cells'][$j+1][3];
		
					$SQ_shop_item = query_execute_row("SELECT *,SUM(item_stock_qty) as item_stock_qty  FROM shop_supplier_detail as ssd,shop_item_stock_status as siss WHERE ssd.grn_id='$grn_id' AND siss.item_id='$item_id' AND ssd.supplier_detail_id=siss.supplier_detail_id  AND item_stock_qty !=0 AND siss.status='active'  order by ssd.supplier_detail_id  Desc");
       	
					$shop_qty = $SQ_shop_item['item_stock_qty'];
					$ware = $SQ_shop_item['warehouse_id'];
		
					
		
					//$lifestage=$data->sheets[0]['cells'][$j+1][5];

					if($grn_id && $item_id && $qty && $shop_qty >= $qty){
			
						$section_check = 0;
						$checkStatus = 0;
			
						$SQ_shop_supplier_detail = query_execute_row("SELECT * FROM shop_supplier_detail WHERE grn_id='$grn_id' AND item_id='$item_id'");
			
						$shop_supplier_id = $SQ_shop_supplier_detail['supplier_id'];
	
						$supplier_detail_id = $SQ_shop_supplier_detail['supplier_detail_id'];

						$item_batch_id = $SQ_shop_supplier_detail['item_batch_id'];

						$buying_price = $SQ_shop_supplier_detail['buying_price'];

						$mrp_price = $SQ_shop_supplier_detail['mrp_price'];

						$selling_price = $SQ_shop_supplier_detail['selling_price'];

						$tax_vat = $SQ_shop_supplier_detail['tax_vat'];

						$cst_tax = $SQ_shop_supplier_detail['cst_tax'];
						
						$SQ_shop_item_stock_status = query_execute_row("SELECT * FROM shop_item_stock_status WHERE supplier_detail_id='$supplier_detail_id' AND item_id='$item_id' AND status='active'");
						
						$SQ_count = query_execute("SELECT * FROM shop_item_stock_status WHERE supplier_detail_id='$supplier_detail_id' AND item_id='$item_id' AND status='active'");
						
						
						if($SQ_shop_item_stock_status['item_stock_qty'] >= $qty){
							
							$UQ_shop_item_stock_status = query_execute("UPDATE shop_item_stock_status SET item_stock_qty=item_stock_qty - '$qty' WHERE supplier_detail_id='$supplier_detail_id' AND item_id='$item_id' AND status='active'");
							
							//echo "UPDATE shop_item_stock_status SET item_stock_qty=item_stock_qty - '$qty' WHERE supplier_detail_id='$supplier_detail_id' AND item_id='$item_id' AND status='active'";
							
							//echo "<br/>+++++++++++++++++++++++++<br/>";
							
							$checkStatus = 1;
						}
						else{
			
							$checkStatus = 0;
							$msg = "<font color='#ff0000'>Enter QTY is greater than available QTY</font>";
						}
						
						if($checkStatus=='1'){
							
							$qItemscheck = query_execute_row("SELECT qty, virtual_qty, item_status FROM shop_items WHERE item_id='$item_id'");
							if($qItemscheck['qty'] > $qty && $qItemscheck['qty']!='0'){
		
								$shopupdate = query_execute("UPDATE shop_items SET qty = qty - '$qty' WHERE item_id='$item_id'");
								
								//echo "UPDATE shop_items SET qty = qty - '$qty' WHERE item_id='$item_id'";
							
								if($qItemscheck['virtual_qty'] > $qty && $qItemscheck['virtual_qty']!='0'){
								
									$shopvirtualupda = query_execute("UPDATE shop_items SET virtual_qty = virtual_qty - '$qty' WHERE item_id='$item_id'");	
									
									//echo "UPDATE shop_items SET virtual_qty = virtual_qty - '$qty' WHERE item_id='$item_id'";
								}
								else{
									if($qItemscheck['item_status']=='hard'){
							
										$shopvirtualupda1 = query_execute("UPDATE shop_items SET virtual_qty='0', stock_status='outofstock' WHERE item_id='$item_id'");
										
										//echo "UPDATE shop_items SET virtual_qty='0', stock_status='outofstock' WHERE item_id='$item_id'";
									}	
									else{
							
										$shopvirtualupda1 = query_execute("UPDATE shop_items SET virtual_qty='0' WHERE item_id='$item_id'");
										
										//echo "UPDATE shop_items SET virtual_qty='0' WHERE item_id='$item_id'";
									}			
								}
							}
							else{
								if($qItemscheck['item_status']=='hard'){
									
									$shopupdate1 = query_execute("UPDATE shop_items SET qty='0', virtual_qty='0', stock_status='outofstock' WHERE item_id='$item_id'");
									
									//echo "UPDATE shop_items SET qty='0', virtual_qty='0', stock_status='outofstock' WHERE item_id='$item_id'";	
								}
								else{
									
									$shopupdate1 = query_execute("UPDATE shop_items SET qty='0', virtual_qty='0' WHERE item_id='$item_id' ");		
									//echo "UPDATE shop_items SET qty='0', virtual_qty='0' WHERE item_id='$item_id'";
								}
							}

							// Insert Query For shop_supplier_invoice
							$ordercomnt = "Credit Note has been Created for grn id-  $grn_id";
						$arr_item_id[]= $grn_id.'@@'.$item_id.'@@'.$ordercomnt;
							 $sqlInsert = query_execute("INSERT into  shop_credit_detail(grn_id,supplier_id,item_id,item_batch_id,qty,billed_qty,buying_price,mrp_price,selling_price,tax_vat,cst_tax,create_date,expiry_date,status,grn_comments,warehouse_id) values('$grn_id','$shop_supplier_id','$item_id','$item_batch_id','$qty','$qty','$buying_price','$mrp_price','$selling_price','$tax_vat','$cst_tax',NULL,'','active','Credit Note For $grn_id ','$ware')");  // else if supp id
						
							$section_check = 1;
							$checkStatus = 0;
							$msg = "Updated Successfully";
			
						}
					}
					else{
						$msg = "<font color='#ff0000'>Qty or ITEM_ID or GRN_ID not found for Update. Please check</font>";	
					}	
					?>
					<tr valign="top">
						
						<td><?=$grn_id?></td>
                        <td><?=$item_id?></td>
						<td><?=$qty?></td>
                        <td><?=$msg?></td>
					</tr>
					<?
					$grn_id='';
					$item_id='';
					$qty='';
					$msg='';
				}
				foreach($arr_item_id as $idArray){
		
				//	$idArray = explode(",",$arr_item_id);         // remove one extra comma

					$idArray1 = explode("@@",$idArray);           // fetch one id from string of ids to insert into item_id col
		 //  print_r($idArray);
					
$resultinsert = query_execute("INSERT INTO section_reviews (userid, review_section_name, review_section_id, review_body,review_name, c_date, user_ip) VALUES ('$userid', 'GRN', '".$idArray1[0]."', '".$idArray1[2]."','".$idArray1[0]."', NULL, '$ip')");
 
					
					
				}
			}
			?>
		</table>
	</body>
</html>