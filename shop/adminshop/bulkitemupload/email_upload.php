<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk mail</title>
<style>
table{border:1px solid #ccc;}
tr{border:1px solid #ccc;}
td{border:1px solid #ccc;}
th{border:1px solid #ccc;}
</style>
</head>
<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<h1>Bulk Upload for email</h1>
<h3>1st Column should be email ID</h3>
<h3>File should be saved in "xls format"</h3> 
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font>
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" /></center>
</form>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	?>
	 <table>
	 	<tr>
            <th>Email</th>
            <th>Order amount</th>
			<th>Name</th>
			<th>Address1</th>
			<th>address_zip</th>
			<th>address_city</th>
			<th>address_state</th>
			<th>mobile</th>
        </tr><? 
		for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
			$email=$data->sheets[0]['cells'][$j+1][1];
			$order_amount=$data->sheets[0]['cells'][$j+1][2];
			$name=$data->sheets[0]['cells'][$j+1][3];
			$mobile=$data->sheets[0]['cells'][$j+1][4];
			$sql=mysql_query("select address_address1,address_zip,address_city,address_state from shop_order_address where address_email='$email' group by order_id order by order_id desc");
			$sdata=mysql_fetch_assoc($sql);
			?>
			<tr>
				<td><?=$email?></td>
				<td><?=$supplier_code?></td>
				<td><?=$order_amount?></td>
				<td><?=$sdata['address_address1']?></td>
				<td><?=$sdata['address_zip']?></td>
				<td><?=$sdata['address_city']?></td>
				<td><?=$sdata['address_state']?></td>
				<td><?=$mobile?></td>
			</tr>
		<? 
			$email='';$supplier_code='';$order_amount='';$mobile='';
		} 		
		?>
        </table><?
    }
?>
</body>
</html>