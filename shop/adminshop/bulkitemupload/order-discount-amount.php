<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order Discount Amount</title>
</head>

<body>
<h1>Order Discount Amount</h1>
<h2>1st column should be Order Id</h2>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" />

</form>
<hr />
<table border="1" cellpadding="3" cellspacing="0">  
  <tr>
    <td>Order ID</td>
    <td>Order Discount Amount</td>
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
	
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$item_id=$data->sheets[0]['cells'][$j+1][1];
$item_id=trim($item_id);

if($item_id){
	//-------------------------Update Item------------------------------------------------------------------------------------	
$updt_query=query_execute_row("SELECT order_discount_percentage FROM shop_order WHERE order_id='$item_id'");

	//-------------------------Update Item END------------------------------------------------------------------------------------	
?>
 <tr valign="top">
    <td><?=$item_id;?></td>
    <td><?=$updt_query['order_discount_percentage'];?></td>
  </tr>
<?
}
$item_id='';

}
}
?>
</table>
</body>
</html>