<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Update Combo Item Status</title>
<style>
table { border: 1px solid #ccc;}
tr { border: 1px solid #ccc;}
td { border: 1px solid #ccc;}
th { border: 1px solid #ccc;}
</style>
</head>
<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<h3>1st Coloum should be Item Id(Only Combo)</h3></br>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
  <center>
<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select Status:</font>
<select id="item_status" name="item_status" required>
<option value="">SELECT</option>
<option value="delete">Delete</option>
<option value="active">Active</option>
</select>
    <font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font>
    <input type="file" name="xls_file" id="xls_file" />
    <input type="submit" name="button" id="button" value="Upload" />
  </center>
</form>
<div>
</div>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);?>
<table>
  <tr>
    <th>Item Id</th>
    <th>Status</th>
  </tr>
  <?
		for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
		 $item_id=$data->sheets[0]['cells'][$j+1][1];
		 $item_id=trim($item_id);
		 $check_item=query_execute_row("SELECT type_id FROM shop_items WHERE item_id='$item_id'");
		 $type_item=$check_item['type_id'];
		 if($type_item=='combo' && $item_id!='' && $item_status!=''){		 
	     $update_order_table=query_execute("UPDATE shop_items SET item_display_status='$item_status' WHERE item_id='$item_id'");
	?>
  <tr>
    <td><?=$item_id?></td>
    <td><?=$item_status?></td>
  </tr>
  <?
   }
$item_id='';					
		}						
		?>
</table>
<?
    }
?>
</form>
</body>
</html>