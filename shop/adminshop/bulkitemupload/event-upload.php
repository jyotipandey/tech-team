<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Upload Event Info</title>
</head>

<body>
<b>All Data Should Be Upload In The Same formate as Shown In Table</b></br>
<b>All Date should be In Formate (YYYY-MM-DD)</b></br>
<b>Publish Ststus Should Be(publish OR markdel)</b></br>
<b>Map(google map iframe)</b></br>
<b>Excel Should Be Saved In 97-2003 Workbook</b></br>

<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<input type="file" name="xls_file" id="xls_file" />
<input type="submit" name="button" id="button" value="Upload" />

</form>
<hr />
<table width="728" border="1" cellpadding="3" cellspacing="0">  
  <tr>
    <td width="40">event name</td>
    <td width="40">start date</td>
    <td width="40">end date</td>
    <td width="40">start hr</td>
    <td width="40">start ampm</td>
    <td width="40">end hr</td>
    <td width="40">end ampm</td>
    <td width="40">venue</td>
    <td width="40">address</td>
    <td width="40">phone</td>
    <td width="40">mobile</td>
    <td width="40">email</td>
    <td width="40">expected entry</td>
    <td width="40">organized by</td>
    <td width="40">late entry date</td>
    <td width="40">about org</td>
    <td width="40">member club</td>
    <td width="40">member sec</td>
    <td width="40">publish status</td>
    <td width="40">map</td>
  </tr>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	//$data = new Spreadsheet_Excel_Reader();
	
	//$data->read("xls-files/".$_FILES["xls_file"]["name"]);
for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
$event_name=$data->sheets[0]['cells'][$j+1][1];
$start_date=$data->sheets[0]['cells'][$j+1][2];
$end_date=$data->sheets[0]['cells'][$j+1][3];
$start_hr=$data->sheets[0]['cells'][$j+1][4];
$start_ampm=$data->sheets[0]['cells'][$j+1][5];
$end_hr=$data->sheets[0]['cells'][$j+1][6];
$end_ampm=$data->sheets[0]['cells'][$j+1][7];
$venue=$data->sheets[0]['cells'][$j+1][8];
$address=$data->sheets[0]['cells'][$j+1][9];
$phone=$data->sheets[0]['cells'][$j+1][10];
$mobile=$data->sheets[0]['cells'][$j+1][11];
$email=$data->sheets[0]['cells'][$j+1][12];
$expected_entry=$data->sheets[0]['cells'][$j+1][13];
$organized_by=$data->sheets[0]['cells'][$j+1][14];
$late_entry_date=$data->sheets[0]['cells'][$j+1][15];
$about_org=$data->sheets[0]['cells'][$j+1][16];
$member_club=$data->sheets[0]['cells'][$j+1][17];
$member_sec=$data->sheets[0]['cells'][$j+1][18];
$publish_status=$data->sheets[0]['cells'][$j+1][19];
$publish_status=$data->sheets[0]['cells'][$j+1][20];
if($event_name){
	$event_nice_name = createSlug($event_name);
	$sqlinserte=query_execute("INSERT INTO events (event_name, event_nice_name, start_date, end_date, start_hr, start_ampm, end_hr, end_ampm, venue, address, phone, mobile, email, expected_entry, organized_by, late_entry_date, about_org, member_club, member_sec, publish_status, c_date, map) VALUES ('$event_name','$event_nice_name','$start_date','$end_date','$start_hr','$start_ampm','$end_hr','$end_ampm','$venue','$address','$phone','$mobile','$email','$expected_entry','$organized_by', '$late_entry_date', '$about_org', '$member_club', '$member_sec', '$publish_status', NULL, '$map' )");

	//-------------------------Update Item END------------------------------------------------------------------------------------	
?>
 <tr valign="top">
    <td><?=$event_name;?></td>
   <td><?=$start_date;?></td>
   <td><?=$end_date;?></td>
    <td><?=$start_hr;?></td>
    <td><?=$start_ampm;?></td>
    <td><?=$end_hr;?></td>
    <td><?=$end_ampm;?></td>
    <td><?=$venue;?></td>
    <td><?=$address;?></td>
    <td><?=$phone;?></td>
    <td><?=$mobile;?></td>
    <td><?=$email;?></td>
    <td><?=$expected_entry;?></td>
    <td><?=$organized_by;?></td>
    <td><?=$late_entry_date;?></td>
    <td><?=$about_org;?></td>
    <td><?=$member_club;?></td>
    <td><?=$member_sec;?></td>
    <td><?=$publish_status;?></td>
    <td><?=$map;?></td>
  </tr>
<?
}
$event_name='';
$start_date='';
$end_date='';
$start_hr='';
$start_ampm='';
$end_hr='';
$end_ampm='';
$venue='';
$address='';
$phone='';
$mobile='';
$expected_entry='';
$organized_by='';
$late_entry_date='';
$about_org='';
$member_club='';
$member_sec='';
$publish_status='';
$map='';
}
}
?>
</table>
</body>
</html>