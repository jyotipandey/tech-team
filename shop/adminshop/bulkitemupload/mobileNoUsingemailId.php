<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Update Combo Item Status</title>
<style>
table { border: 1px solid #ccc;}
tr { border: 1px solid #ccc;}
td { border: 1px solid #ccc;}
th { border: 1px solid #ccc;}
</style>
</head>
<body>
<?php require_once($DOCUMENT_ROOT.'/shop/adminshop/common/top.php'); ?>
<h3>1st Coloum should be Item Id(Only Combo)</h3></br>
<form action="" method="post" enctype="multipart/form-data" name="formfile" id="formfile">
<center>
	<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select Status:</font>
   	<font size="+2" style="font-family:Georgia, 'Times New Roman', Times, serif"> Select File to Upload :- </font>
    <input type="file" name="xls_file" id="xls_file" />
    <input type="submit" name="button" id="button" value="Upload" />
</center>
</form>
<?
if($xls_file){
	if ($_FILES["xls_file"]["error"] > 0){
 	 	echo "Error: " . $_FILES["xls_file"]["error"] . "<br />";
  	}
	if (file_exists("xls-files/" . $_FILES["xls_file"]["name"])){
		echo $_FILES["xls_file"]["name"] . " already exists. ";
    }else{
      	move_uploaded_file($_FILES["xls_file"]["tmp_name"],"xls-files/" . $_FILES["xls_file"]["name"]);
      	chmod("xls-files/".$_FILES["xls_file"]["name"], 0777);
		$updone=1;
    }
}
if($updone==1){
	require_once($DOCUMENT_ROOT.'/php-excel-reader-2.21/excel_reader2.php');
	$data = new Spreadsheet_Excel_Reader("xls-files/".$_FILES["xls_file"]["name"]);
	$userid =array();
	for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++){
		$email_id=$data->sheets[0]['cells'][$j+1][1];
		$email_id=trim($email_id);
		$check_item=query_execute_row("SELECT userid, u_email FROM users WHERE u_email='$email_id'");
		if($check_item['userid']!=""){
			$userid[] = $check_item['userid'];
		}
	}
	?>
    <table>
      <tr>
        <th>User ID</th>
        <th>Email ID</th>
        <th>Phone Number1</th>
        <th>Phone Number2</th>
        <th>City</th>
        <th>State</th>
      </tr>
    <?php 
	foreach($userid as $user_id){
		$address = query_execute_row("SELECT * FROM shop_order_more_address WHERE userid='$user_id' ORDER BY more_address_id DESC LIMIT 1");
		if($address['more_address_id']!=""){ ?>
		<tr>
            <td><?=$user_id?></td>
            <td><?=$address['address_email']?></td>
            <td><?=$address['address_phone1']?></td>
            <td><?=$address['address_phone2']?></td>
            <td><?=$address['address_city']?></td>
            <td><?=$address['address_state']?></td>
        </tr>	
		<?php }else{
			$address_order = query_execute_row("SELECT * FROM shop_order_address WHERE userid='$user_id' ORDER BY order_address_id DESC LIMIT 1");
			if($address_order['order_address_id']!=""){
		?>
        <tr>
            <td><?=$user_id?></td>
            <td><?=$address['address_email']?></td>
            <td><?=$address['address_phone1']?></td>
            <td><?=$address['address_phone2']?></td>
            <td><?=$address['address_city']?></td>
            <td><?=$address['address_state']?></td>
        </tr>
        <?php }
		}
	}
}
 ?>
 	</table>
</form>
</body>
</html>