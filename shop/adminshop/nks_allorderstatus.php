<?
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//Get user level Details
if($sessionLevel != 1){
Header("Location: /");
ob_end_flush();
exit(); 
}
//Get user level Details END
//------------Show
$maxshow = 200;
if(empty($show)) {
	$show = 0;
}else{
 $show = $show - 1;
}
 $showRecord = $show * $maxshow;
 $nextShow = $showRecord + $maxshow;
//-------------Show

if($filter=='pending'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND order_status = '0' AND (delevery_status = 'undelivered' OR delevery_status = 'pending-dispatch') ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND order_status = '0' AND (delevery_status = 'undelivered' OR delevery_status = 'pending-dispatch')";
	$hone='Pending Orders';
}elseif($filter=='delivered'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'delivered' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'delivered'";
	$hone='Delivered Orders';
}elseif($filter=='undelivered'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'dispatched' AND order_status = '0' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'dispatched' AND order_status = '0'";
	$hone='Undelivered Orders';
}elseif($filter == 'paid'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND order_status = '0' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND order_status = '0'";
	$hone='Paid Orders';
}elseif($filter == 'unpaid'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND order_status = '1' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND order_status = '1'";	
	$hone='UnPaid Orders';
}elseif($filter == 'canceled'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'canceled' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'canceled'";
	$hone='Canceled Orders';	
}elseif($filter == 'rto'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'rto' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'rto'";
	$hone='Return To Origin Orders';	
}elseif($filter == 'hcd'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'hcd' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'hcd'";
	$hone='Happy calls Done';	
}elseif($filter == 'dispatched-ready'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'dispatched-ready' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'dispatched-ready'";
	$hone='Dispatched Ready';	
}elseif($filter == 'new'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'new' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'new'";
	$hone='New Orders';	
}elseif($filter == 'success'){
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' AND delevery_status != 'canceled' AND order_status='0' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status != 'canceled' AND order_status='0'";
	$hone='Success Orders';	
}elseif($searchOrder){
	if($Sorder_id){
		$Sorder_id=trim($Sorder_id);
		$searchSql.="AND o.order_id = '$Sorder_id'";
	}
	if($Suserid){
		$Suserid=trim($Suserid);
		$searchSql.="AND o.userid LIKE '%$Suserid%'";
	}
	if($Sorder_transaction_id){
		$Sorder_transaction_id=trim($Sorder_transaction_id);
		$searchSql.="AND o.order_transaction_id = '$Sorder_transaction_id'";
	}
	if($Saddress_phone){
		$Saddress_phone=trim($Saddress_phone);
		$searchSql.="AND (a.address_phone1 LIKE '%$Saddress_phone%' OR a.address_phone2 LIKE '%$Saddress_phone%')";
	}
	if($Saddress_email){
		$Saddress_email=trim($Saddress_email);
		$searchSql.="AND a.address_email LIKE '%$Saddress_email%'";
	}
	if($StxtYear && $StxtYear){
		$from = "$StxtYear-$SddlMonth-$StxtDay 00:00:00";
		$to = "$ftxtYear-$fddlMonth-$ftxtDay 00:00:00";
		$searchSql.="AND order_c_date BETWEEN '$from' AND '$to'";
	}
	if($Scart_order_id){
		$Scart_order_id=trim($Scart_order_id);
		$searchSql.="AND c.item_id = '$Scart_order_id'";
	}
	
	
	$sql="SELECT DISTINCT o.order_id, o.u_id, o.userid, o.order_transaction_id, o.order_amount, o.order_transaction_amount, o.order_status, o.order_method, o.paymentid, o.mode, o.delevery_status, o.order_tracking, o.shop_courier_id, o.order_c_date FROM shop_order as o, shop_order_address as a, shop_cart as c WHERE o.mode != 'TEST' AND o.order_id = a.order_id AND a.order_id = c.cart_order_id $searchSql ORDER BY o.order_c_date DESC LIMIT $showRecord, $maxshow";
	
	$sqlall="SELECT count( * ) as total_record FROM shop_order as o, shop_order_address as a, shop_cart as c WHERE o.mode != 'TEST' AND o.order_id = a.order_id AND a.order_id = c.cart_order_id $searchSql";
	
	$hone='Search Orders';
}else{
	$sql="SELECT * FROM shop_order WHERE mode != 'TEST' ORDER BY order_c_date DESC LIMIT $showRecord, $maxshow";
	$sqlall="SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST'";
	$hone='All Orders';
}

//Exec SQL
$qGetMyCart=query_execute("$sql");
$qGetMyCartall=query_execute("$sqlall");
$rowTotal = mysql_fetch_row($qGetMyCartall);
$totrecord = $rowTotal[0];

//echo $sql;

//count All
$qGetMyCartPending=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST'");
$prowTotal = mysql_fetch_row($qGetMyCartPending);
$ttotrecord = $prowTotal[0];
//count Pending
$qGetMyCartPending=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND order_status = '0' AND (delevery_status = 'undelivered' OR delevery_status = 'pending-dispatch')");
$prowTotal = mysql_fetch_row($qGetMyCartPending);
$ptotrecord = $prowTotal[0];
//count Delivered
$qGetMyCartdelivered=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'delivered' AND order_status = '0'");
$drowTotal = mysql_fetch_row($qGetMyCartdelivered);
$dtotrecord = $drowTotal[0];
//count Undelivered
$qGetMyCartundelivered=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'dispatched' AND order_status = '0'");
$udrowTotal = mysql_fetch_row($qGetMyCartundelivered);
$udtotrecord = $udrowTotal[0];
//count Paid
$qGetMyCartPaid=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND order_status = '0'");
$pdrowTotal = mysql_fetch_row($qGetMyCartPaid);
$pdtotrecord = $pdrowTotal[0];
//count Unpaid
$qGetMyCartunPaid=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND order_status = '1'");
$updrowTotal = mysql_fetch_row($qGetMyCartunPaid);
$updtotrecord = $updrowTotal[0];
//count Cancled
$qGetMyCartunPaid=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'canceled'");
$updrowTotal = mysql_fetch_row($qGetMyCartunPaid);
$ctotrecord = $updrowTotal[0];
//count RTO
$qGetMyCartunRto=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'rto'");
$updrowTotal = mysql_fetch_row($qGetMyCartunRto);
$rtototrecord = $updrowTotal[0];
//count HCD
$qGetMyCartunRto=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'hcd'");
$updrowTotal = mysql_fetch_row($qGetMyCartunRto);
$hcdtotrecord = $updrowTotal[0];
//count Dispatched Ready
$qGetMyCartunRto=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'dispatched-ready'");
$updrowTotal = mysql_fetch_row($qGetMyCartunRto);
$drtotrecord = $updrowTotal[0];
//count New Orders
$qGetMyCartunRto=query_execute("SELECT count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status = 'new'");
$updrowTotal = mysql_fetch_row($qGetMyCartunRto);
$newtotrecord = $updrowTotal[0];
//count Success
$qGetMyCartSuccess=query_execute("SELECT sum(order_amount) as totalsale, avg(order_amount) as totalamt, count( * ) as total_record FROM shop_order WHERE mode != 'TEST' AND delevery_status != 'canceled' AND order_status='0'");
$srowTotal = mysql_fetch_array($qGetMyCartSuccess);
$stotrecord = $srowTotal["total_record"];
$stotalamt = $srowTotal["totalamt"];
$totalsale = $srowTotal["totalsale"];
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$hone?></title>
<script src="/jquery/jquery.min.js" type="text/javascript"></script>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<style type="text/css">
ul:hover {
	background: #DCE9F4;
}
ul.color {
	background: #F9F9F9;
}
ul.color:hover {
	background: #BAD4EB;
}
</style>
<?php require_once('common/top.php'); ?>
<p style="text-align:center; ">
<a href="allorderstatus.php?filter=new">New (<?=$newtotrecord?>)</a> |
<a href="allorderstatus.php?filter=pending">Pending orders (<?=$ptotrecord?>)</a> | <a href="allorderstatus.php?filter=dispatched-ready">Dispatched Ready (<?=$drtotrecord?>)</a> | <a href="pending-products.php" target="_blank">Pending products</a> | <a href="allorderstatus.php?filter=undelivered">Dispatched	 (<?=$udtotrecord?>)</a> | 
<a href="allorderstatus.php?filter=delivered">Delivered (<?=$dtotrecord?>)</a> | 
<a href="allorderstatus.php?filter=hcd">Happy calls Done (<?=$hcdtotrecord?>)</a> | <a href="allorderstatus.php?filter=canceled">Cancelled (<?=$ctotrecord?>)</a> |  
<a href="allorderstatus.php?filter=rto">Refunded (<?=$rtototrecord?>)</a> | <a href="allorderstatus.php?filter=paid">Paid (<?=$pdtotrecord?>)</a> | 
<a href="allorderstatus.php?filter=unpaid">UnPaid (<?=$updtotrecord?>)</a>| <a href="allorderstatus.php">All (<?=$ttotrecord?>)</a> | <a href="sold-products.php">Sold Items</a> | <a href="allorderstatus.php?filter=success">Success Orders (<?=$stotrecord?> total: <?=$totalsale?> Avg: <?=$stotalamt?>)</a></p>
<form id="formOrderSearch" name="formOrderSearch" method="get" action="/shop/adminshop/allorderstatus.php">
<table width="100%" cellspacing="0" cellpadding="2" style="text-align:center; border-color:#999; border-width:1px; border-style:solid; background-color:#edeaea">
  <tr>
  	
    <td width="80"><span style="text-align:center; font-family:Verdana, Geneva, sans-serif; font-size:10px">Product ID<br />
      <input name="Scart_order_id" type="text" id="Scart_order_id" style="width:100px;" value="<?=$Scart_order_id?>"/>
    </span></td>
    
    <td width="120"><span style="text-align:center; font-family:Verdana, Geneva, sans-serif; font-size:10px">Order ID<br />
      <input name="Sorder_id" type="text" id="Sorder_id" style="width:100px;" value="<?=$Sorder_id?>"/>
    </span></td>
    <td width="120">Profile Id
      <br />      
      <input name="Suserid" type="text" id="Suserid" style="width:100px;" value="<?=$Suserid?>"/></td>
    <td width="120">Transaction ID<br />
      <input name="Sorder_transaction_id" type="text" id="Sorder_transaction_id" style="width:100px;" value="<? $Sorder_transaction_id?>"/></td>
    <td>Mobile<br />
      <input name="Saddress_phone" type="text" id="Saddress_phone"  style="width:100px;" value="<?=$Saddress_phone?>" /></td>
    <td>Email ID<br />
      <input name="Saddress_email" type="text" id="Saddress_email"  style="width:100px;" value="<?=$Saddress_email?>" /></td>
    <td>Date<br />
    <select name="StxtDay" id="StxtDay">
      <option value="">Day</option>
               <? $ADate = range(01, 31);
                foreach($ADate as $Sdat){
					$Sdat = sprintf("%02d", $Sdat);
               ?>
                <option value="<? echo"$Sdat"; ?>" <? if($Sdat == $StxtDay) echo"selected='selected'"; ?>><? echo" $Sdat "; ?></option>
              <?  } ?>
            </select> 
    - 
    <select name="SddlMonth" id="SddlMonth" style="width:60px;">
    <option value="">Month</option>
            <? foreach($AMonths as $SMonth => $SValueMonth){ ?>
             <option value="<? echo"$SMonth"; ?>" <? if($SMonth == "$SddlMonth") echo"selected='selected'"; ?>><? echo"$SValueMonth"; ?></option>
          <? } ?>
           </select>
            -
           <select name="StxtYear" id="StxtYear">
           <option value="">Year</option>
            <? $AYear = range(2011, 2020);
             foreach($AYear as $SYea){
            ?>
             <option value="<? echo"$SYea"; ?>" <? if($SYea == "$StxtYear") echo"selected='selected'"; ?>><? echo" $SYea "; ?></option>
            <?  } ?>
          </select>
           
           to 
           <select name="ftxtDay" id="ftxtDay">
            <option value="">Day</option>
               <? $ADate = range(01, 31);
                foreach($ADate as $dat){
					$dat = sprintf("%02d", $dat);
               ?>
                 <option value="<? echo"$dat"; ?>" <? if($dat == $ftxtDay) echo"selected='selected'"; ?>><? echo" $dat "; ?></option>
                <?  } ?>
           </select> - 
          <select name="fddlMonth" id="fddlMonth" style="width:60px;">
          <option value="">Month</option>
            <? foreach($AMonths as $SMonth => $SValueMonth){ ?>
             <option value="<? echo"$SMonth"; ?>" <? if($SMonth == "$fddlMonth") echo"selected='selected'"; ?>><? echo"$SValueMonth"; ?></option>
          <? } ?>
           </select>
            -
           <select name="ftxtYear" id="ftxtYear">
           <option value="">Year</option>
            <? $AYear = range(2011, 2020);
             foreach($AYear as $SYea){
            ?>
             <option value="<? echo"$SYea"; ?>" <? if($SYea == "$ftxtYear") echo"selected='selected'"; ?>><? echo" $SYea "; ?></option>
            <?  } ?>
        </select>
    </td>
  </tr>
</table>


<div style="text-align:center; margin-top:5px;"><input type="submit" name="searchOrder" id="searchOrder" value="                    Search                 " /></div>
</form>
<div style="width:1200px; margin-left:auto; margin-right:auto"><h1><?=$hone?></h1></div>
<?
if($totrecord == 0){
	echo 'Sorry no Order...';
}else{
?>
<div id="table" style="width:1200px; height:auto; font-family:Arial, Helvetica, sans-serif; font-size:12px">

<ul class="header" style="height:40px;">
      <li style="width:30px; height:40px">Order</li>
      <li style="width:150px; height:40px; text-align:left">User</li>
      <li style="width:60px; height:40px;text-align:left; text-align:center">Trans ID</li>
      <li style="width:80px; height:40px">Order Amount</li>
      <li style="width:80px; height:40px">Transaction Amount</li>
      <li style="width:50px; height:40px">Status</li>
      <li style="width:100px; height:40px">Order Method</li>
      <li style="width:80px; height:40px">Date</li>
      <li style="width:100px; height:40px">Delivery Status</li>
      <li style="width:100px; height:40px">Courier Status</li>
  <li style="width:230px; height:40px">Latest Comment</li>
      <li style="width:50px; height:40px">Action</li>
    </ul>
<?
	
	while($rowMyCart = mysql_fetch_array($qGetMyCart)){
		
	?>
    <form id="formAdmin<?=$rowMyCart["order_id"];?>" name="formAdmin<?=$rowMyCart["order_id"];?>" method="post" action="">
    <ul style="height:127px;">
      <li style="width:30px; height:120px"><a href="nks_order-detail.php?order_id=<?=$rowMyCart["order_id"];?>"><?=$rowMyCart["order_id"];?>
      </a></li>
      <li style="width:150px; text-align:left; height:120px"><?
	  
	  if(!$rowMyCart["userid"]){
		  $uid=$rowMyCart["u_id"];
		  $qGuestUser=query_execute("SELECT u_email FROM users WHERE ID='$uid'");
		  $rowGuestUser = mysql_fetch_array($qGuestUser);
		  echo $rowGuestUser["u_email"];		  
	  }else{
		  echo $rowMyCart["userid"];
	  }
	  
	  
	  ?></li>
      <li style="width:60px; height:120px; text-align:left; " class="cartsmall"><?=$rowMyCart["order_transaction_id"];?></li>
      <li style="width:80px; height:120px" class="cartsmall">Rs. <?=$rowMyCart["order_amount"];?>
      <br />Discount Rs. <?=$rowMyCart["order_discount_amount"];?>
      <br />Item Rs. <?=$rowMyCart["order_items_amount"];?></li>
      <li style="width:80px; height:120px" class="cartsmall">Rs. <?=$rowMyCart["order_transaction_amount"];?></li>
      <li style="width:50px; height:120px; text-align:center" class="cartsmall"><div id="OrderStatus<?=$rowMyCart["order_id"];?>"><?=$AorderStatusUser[$rowMyCart["order_status"]];?></div>
      <div id="DivUnpay<?=$rowMyCart["order_id"];?>">
	  <?
	  //if($rowMyCart["order_method"] == 'cod' && $rowMyCart["order_status"] == 0){
		  ?>
          <br /><a href="Javascript:ShaAjaxJquary('makerepay.php?order_id=<?=$rowMyCart["order_id"]?>', '#DivUnpay<?=$rowMyCart["order_id"];?>', '', 'formAdmin<?=$rowMyCart["order_id"];?>', 'POST', '#DivUnpayLoad<?=$rowMyCart["order_id"];?>', 'Loading...', 'REP');">make unpaid</a>
	<?
	  //}
	  ?>
      </div>
      <select name="order_status" id="order_status" style="width:40px;">
          <option value="">Status</option>
          <option value="0">Paid</option>
          <option value="1">Unpaid</option>
        </select>
      <div id="DivUnpayLoad<?=$rowMyCart["order_id"];?>"></div>
      </li>
      <li style="width:100px; height:120px; text-align:center" class="cartsmall">
      <div id="orderMethod<?=$rowMyCart["order_id"]?>"><?=$AOrderMethod[$rowMyCart["order_method"]];?></div>
      <select name="order_method_up" id="order_method_up" style="width:80px;">
          <option value="0">Order Method</option>
          <? foreach($AOrderMethod as $omkey => $omval){?>
          <option value="<?=$omkey?>"><?=$omval?></option>
          <? }?>
        </select>
      </li>
      <li style="width:80px; height:120px"><?=showdate($rowMyCart["order_c_date"], "d M o h:m:s a");?></li>
      <li style="width:100px; height:120px">
	  
	  <div id="ShowDelevery<?=$rowMyCart["order_id"];?>"><?=$rowMyCart["delevery_status"];?></div>
      
        
        <select name="delevery_status" id="delevery_status" style="width:90px;">
          <option value="0">Update Status</option>
          <option value="new">New</option>
          <option value="dispatched-ready">Dispatched Ready</option>
          <option value="delivered">Delivered</option>
          <option value="undelivered">Undelivered</option>
          <option value="pending-dispatch">Pending for Dispatch</option>
          <option value="dispatched">Dispatched</option>
          <option value="canceled">Canceled</option>
          <option value="rto">RTO</option>
        </select>
        
        <input name="order_id" type="hidden" id="order_id" value="<?=$rowMyCart["order_id"];?>" />
        <div id="orderMethodHid<?=$rowMyCart["order_id"];?>"><input name="order_method" type="hidden" id="order_method" value="<?=$rowMyCart["order_method"];?>" /></div>
	    <select name="sendEmail" id="sendEmail" style="width:90px;">
	      <option value="0">Send Email</option>
	      <option value="1">Yes</option>
        </select>
      </li>
       <li style="width:100px; height:120px">
       <div id="OrderTracking<?=$rowMyCart["order_id"];?>"><?=$rowMyCart["order_tracking"];?></div>
       <div id="OrderCourier<?=$rowMyCart["order_id"];?>"><?=$ACourier[$rowMyCart["shop_courier_id"]];?></div>
       <div id="OrderCourier<?=$order_id?>"></div>
       <input name="order_tracking" type="text" id="order_tracking" style="width:90px;"value=""/>
        <select name="shop_courier_id" id="shop_courier_id" style="width:90px;">
          <option value="0">Courier</option>
          <option value="1">Blue Dart Express Limited</option>
          <option value="2">Aramax</option>
          <option value="3">Self</option>
          <option value="4">Blazeflash</option>
          <option value="5">DTDC</option>
         </select>
         <div id="OrderCourierAmt<?=$rowMyCart["order_id"]?>"><?=$rowMyCart["shop_courier_amount"]?></div>
         <input name="shop_courier_amount" type="text" id="shop_courier_amount" style="width:90px;"value=""/>
       </li>
       <li style="width:230px; height:120px">
       <div id="OrderCmnt<?=$rowMyCart["order_id"]?>">
       <?
	   $rowCmnt=query_execute_row("SELECT review_body, c_date FROM section_reviews WHERE review_section_id='".$rowMyCart["order_id"]."' AND review_section_name='shop-order' ORDER BY c_date DESC");
		if($rowCmnt["review_body"]){
			echo $rowCmnt["review_body"].' '.showdate($rowCmnt["c_date"], "d M o h:m:s a");
		}		
	   ?>
       </div>
         <textarea name="ordercomnt" id="ordercomnt" cols="2" rows="2" style="width:190px;"></textarea>
       </li>
    <li style="width:30px;height:120px"><a href="Javascript:ShaAjaxJquary('update-delevery.php?order_id=<?=$rowMyCart["order_id"];?>', '#CmntAll<?=$rowMyCart["order_id"];?>', '', 'formAdmin<?=$rowMyCart["order_id"];?>', 'POST', '#LoadDelevery<?=$rowMyCart["order_id"];?>', 'Loading...', 'REP');">Save</a> <a href="Javascript:ShaAjaxJquary('updateorder.php?order_id=<?=$rowMyCart["order_id"];?>', '#ShowDelevery<?=$rowMyCart["order_id"];?>', '', 'formAdmin<?=$rowMyCart["order_id"];?>', 'POST', '#LoadDelevery<?=$rowMyCart["order_id"];?>', 'Loading...', 'REP');">remove</a>
      <a href="Javascript:ShaAjaxJquary('allComments.php?order_id=<?=$rowMyCart["order_id"];?>', '#CmntAll<?=$rowMyCart["order_id"];?>', '', '', 'GET', '#LoadDelevery<?=$rowMyCart["order_id"];?>', 'Loading...', 'REP');">all comments</a>
      <div id="LoadDelevery<?=$rowMyCart["order_id"];?>"></div>
    <div id="LoadDeleveryShow<?=$rowMyCart["order_id"];?>"></div>
    </li>
    </ul>
    <div id="CmntAll<?=$rowMyCart["order_id"];?>">
    
    </div>
  </form>
<? }}?>   
</div>  
<div class="showPages">
<? 
$pageUrl="/shop/adminshop/allorderstatus.php?filter=$filter&page=p";
showPagesLink($totrecord, $maxshow, $pageUrl, $show, $nextShow, $previous, $spUrl);
?>
</div>  

<?php require_once('common/bottom.php'); ?>