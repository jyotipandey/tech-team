<?php
require_once($DOCUMENT_ROOT.'/shop/adminshop/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

$maxshow = 25;
if(empty($show)) {
	$show = 0;
}else{
 $show = $show - 1;
}
 $showRecord = $show * $maxshow;
 $nextShow = $showRecord + $maxshow;
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Bannners</title>
<script src="/jquery/jquery.min.js" type="text/javascript"></script>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<style>
.mobile-add-banner-box {
    float: left;
    width: 96%;
    border: 1px solid #ddd;
    margin: 1% 2% 14px;
    padding: 0 0 0 10px;
    display: table
}

.mobile-add-box-image {
    width: 24%;
    display: table-cell;
    vertical-align: bottom;
    position: relative;
    padding-bottom: 5px
}

.mobile-add-box-image img {
    max-width: 100px;
    height: auto;
    margin-left: -10px
}

.mobile-add-box-text {
    width: 76%;
    text-align: right;
    display: table-cell;
    padding: 0 15px 10px 20px
}

.mobile-add-buy-now-btn {
    display: block;
    margin-top: 10px
}

.mobile-add-buy-now-btn a {
    background: #ff7a22;
    color: #fff;
    padding: 5px 10px;
    border-radius: 3px
}

.mobile-add-box-text h2 {
    margin-bottom: 5px;
    text-align: left;
    padding-top: 10px;
	font-size:13px;
}

.banner-description {
    text-align: left;
    color: #888;
    font-style: italic;
    font-size: 13px
}

@-webkit-keyframes blinker {
    from {
        color: #ff7a22
    }
    to {
        color: #000
    }
}

.banner-text-blink {
    text-decoration: blink;
    -webkit-animation-name: blinker;
    -webkit-animation-duration: .6s;
    -webkit-animation-iteration-count: infinite;
    -webkit-animation-timing-function: ease-in-out;
    -webkit-animation-direction: alternate
}

.banner-discount {
    position: absolute;
    top: 0px;
    font-size: 14px;
    line-height: 40px;
    text-align: center;
    color: #ff7a22;
    border-radius: 25px;
    border: 1px solid #ff7a22;
    background: #fff;
    right: 0;
    height: 40px;
    width: 40px
}

.mobile-add-bg {
    background: #f2f2f2
}

#banner-show-case img {
    position: absolute;
    top: 0px;
    max-width: 100px;
    height: auto;
}
</style>
<script language="javascript" type="text/javascript">
function statuschange(sid){
var status=$("#status"+sid).val();	
ShaAjaxJquary('/shop/adminshop/banner-status.php?status='+status+'&id='+sid+'', '#userpro'+sid, '', '', 'POST', '#userpro'+sid, 'checking...', 'REP');
}
function statuschangeM(sid)
{

var status=$("#status"+sid).val();	
ShaAjaxJquary('/shop/adminshop/banner-status.php?M=1&status='+status+'&id='+sid+'', '#userpro'+sid, '', '', 'POST', '#userpro'+sid, 'checking...', 'REP');
	
}
</script>
<script>
$(document).ready(function()
{
    $('.banner-img:gt(0)').hide();
    setInterval(function() {
        $(".banner-img:first-child").fadeOut(3000).next(".banner-img").fadeIn(3000).end().appendTo("#banner-show-case")
}, 4000);
       
});
</script>

<?php require_once('common/top.php');?>
<span style="float:right;margin-right:10px;color: #f00;padding:8px;"><a href="/shop/adminshop/manage-banners.php?filter=active"><strong>Active
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=inactive"><strong>Inactive
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=home"><strong>Home Page
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=other"><strong>Other Page
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=mobile_img"><strong>Mobile Image
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=mobile_text"><strong>Mobile Text
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=offer_page"><strong>Offer Page
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=deal_page"><strong>Deal Page
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=deal_home"><strong>Deal Home
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=footer_popup"><strong>Footer Popup<br />
            </strong></a> | <a href="/shop/adminshop/add-banners.php"><strong>Upload New
            </strong></a> | <a href="/shop/adminshop/manage-banners.php?filter=mobile_product"><strong>Mobile Product Banner            </strong></a></span>
            <?
			if($filter=='active'){
$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active'");
}elseif($filter=='inactive'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='inactive' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='inactive'  ");
	}elseif($filter=='all'){
	$sqlgallerys=query_execute("SELECT * FROM banners ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners  ");
	}elseif($filter=='home'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='home' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='home'  ");
	}elseif($filter=='other'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='other' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='other'  ");
	}elseif($filter=='mobile_img'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='mobile-img' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
    $sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='mobile-img'  ");
	}elseif($filter=='offer_page'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='offer-page' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active' AND featured_on='offer-page'  ");
	}elseif($filter=='mobile_text'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' AND  featured_on='mobile-text' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active' AND  featured_on='mobile-text'  ");
	}elseif($filter=='deal_page'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' AND  featured_on='deal-page' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active' AND  featured_on='deal-page'  ");
	}elseif($filter=='deal_home'){
	$sqlgallerys=query_execute("SELECT * FROM deals WHERE status='active' ORDER BY c_date DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM deals WHERE status='active' ");
	}elseif($filter=='footer_popup'){
	$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' AND  featured_on='footer-popup' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active' AND  featured_on='footer-popup'  ");
	}elseif($filter=='mobile_product'){
	$sqlgallerys=query_execute("SELECT * FROM ads_banner WHERE   item_id !='' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
	$sqlgallerysc=query_execute("SELECT * FROM ads_banner WHERE   item_id !='' ");
	}else{
		$sqlgallerys=query_execute("SELECT * FROM banners WHERE status='active' ORDER BY cdate DESC LIMIT $showRecord, $maxshow");
		$sqlgallerysc=query_execute("SELECT * FROM banners WHERE status='active'  ");
		}
		$totrecord=mysql_num_rows($sqlgallerysc);
			?>
            <? if($filter !='mobile_product'){ ?>
 <div id="table" style="width:1200px; height:auto; font-family:Arial, Helvetica, sans-serif; font-size:12px">
 <span style="float:left;margin-top: 22px;"><? echo "<b>Total Records</b>: ". $totrecord;?></span>
 <ul class="header">
 
      <li style="width:50px; height:5px">No</li>
      <li style="width:240px; height:5px">Image</li>
      <li style="width:140px; height:5px">Change Status</li>
      <li style="width:140px; height:5px">Start Date</li>
      <li style="width:140px; height:5px">End Date</li>
      <li style="width:80px; height:5px">Featured On</li>
      <li style="width:70px; height:5px">Width</li> 
      <li style="width:70px; height:5px">Height</li> 
      <li style="width:40px; height:5px">Order</li>   
      <li style="width:70px; height:5px">Action</li>
      </ul>
<?  
$s=1;
while($ongallerys=mysql_fetch_array($sqlgallerys)){
	if($filter!='deal_home'){
	$galleryimgs=$ongallerys['img'];
	$ids=$ongallerys['id'];
	
if($galleryimgs){
		$src = $DOCUMENT_ROOT.'/allbanners/'.$galleryimgs;
		$imageURL='/allbanners/120x120-'.$galleryimgs;
	}else{
		$src = $DOCUMENT_ROOT.'/allbanners/no-photo-t.jpg';
		$imageURL='/allbanners/no-photo-t.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'120','120','ratiowh');
	}else
	{
	//$galleryimgs=$ongallerys['img'];
	$ids=$ongallerys['deal_id'];	
	}
?>
      <ul style="height:105px">
      <li style="width:50px; height:85px"><? echo $s;?></li>
      <? if($filter=='deal_home'){?>
      <li style="width:240px; height:85px"><?=$ongallerys['show_txt'].' '.$ongallerys['extra_txt'];?></li>
      <? }else{?>
      <img src="<?=$imageURL?>" height="80" width="80" />
      <li style="width:240px; height:85px"><?=$ongallerys['banner_name'];?></li>
      <? }?>
      <li style="width:140px; height:85px">Current Status : <div id="userpro<?=$ids?>" class="userpro"><? echo $ongallerys['status'];?></div><br />      
      <select name="status<?=$ids?>" id="status<?=$ids?>" class="required"   style="width:123px;">
      <option value="">Change Status</option>
      <option value="active" <? if($ongallerys["status"]=='active'){echo'selected="selected"';}?>>Active</option>
      <option value="inactive"  <? if($ongallerys["status"]=='inactive'){echo'selected="selected"';}?>>Inactive</option>
    </select></li>
       <? if($filter=='deal_home'){?>
       <li style="width:140px; height:85px"><? echo $ongallerys['c_date'];?></li>
       <? }else{?>
      <li style="width:140px; height:85px"><? echo $ongallerys['start_date'];?></li>
      <? }?>
      <li style="width:140px; height:85px"><? echo $ongallerys['end_date'];?></li>
       <li style="width:90px; height:85px"><? echo $ongallerys['featured_on'];?></li>
         <? if($filter!='deal_home'){?>
      <li style="width:70px; height:85px"><?=$ongallerys['width'];?></li>
       <li style="width:70px; height:85px"><?=$ongallerys['height'];?></li>
       <li style="width:40px; height:85px"><?=$ongallerys['banner_order'];?></li>
       <? }?>
       <? if($filter=='deal_home'){?>
        <li style="width:70px; height:85px"><input type="button" onclick="statuschange('<?=$ids?>')" value="Save"/></br>
      <a href='/shop/adminshop/add-banners.php?type=edit&deal=deal&id=<?=$ids?>'>Edit</a></li>
       <? }else{?>
      <li style="width:70px; height:85px"><input type="button" onclick="statuschange('<?=$ids?>')" value="Save"/></br>
      <a href='/shop/adminshop/add-banners.php?type=edit&id=<?=$ids?>'>Edit</a></li>
      <? }?>
      </ul>
      <? $s++; }
	  ?>
    </div>
    <? }else{?>
    <div id="table" style="width:1500px; height:auto; font-family:Arial, Helvetica, sans-serif; font-size:12px">
 <span style="float:left;margin-top: 22px;"><? echo "<b>Total Records</b>: ". $totrecord;?></span>
 <ul class="header">
  <li style="width:440px; height:5px">Image(item_id)</li>
      <li style="width:100px; height:5px">Change Status</li>
      <li style="width:100px; height:5px">Start Date</li>
      <li style="width:100px; height:5px">End Date</li>
      <li style="width:120px; height:5px">Display url</li>
      <li style="width:70px; height:5px">GA text</li> 
      <li style="width:120px; height:5px">DESC</li> 
      <li style="width:100px; height:5px">Dis/Img/Stock</li>   
      <li style="width:40px; height:5px">Color</li>
      <li style="width:60px; height:5px">FloatText</li>
      <li style="width:10px; height:5px">Action</li>
      </ul>
<?  
$s=1;
while($ongallerys=mysql_fetch_array($sqlgallerys)){
		$ids=$ongallerys['banner_id'];
		$item_id=$ongallerys['item_id'];
		$sqlbanner160=query_execute_row("SELECT si.item_id,si.name,si.price,si.selling_price,si.nice_name,si.stock_status FROM shop_items as si WHERE si.item_id='$item_id'");
	 if($sqlbanner160['price']<$sqlbanner160['selling_price']){
		 $desc=number_format(($sqlbanner160['selling_price']-$sqlbanner160['price'])/$sqlbanner160['selling_price']*100,0);
	 }
 $sqlmediaco12 = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
							$trecount12=mysql_fetch_array($sqlmediaco12);
							 if ($trecount12['trecd'] != '0') {
								$sqlmedia2 = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 2";
								}
							else{
								$sqlmedia2 = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 2";
								}
							$qdataM2=query_execute("$sqlmedia2");
							
							$countImg=mysql_num_rows($qdataM2);
?>
      <ul style="height:105px">
       <li style="width:440px;"><div class="mobile-add-banner-box">
   <div class="mobile-add-box-image">
   <a href="/<?=$sqlbanner160['nice_name']?>/" data-ajax="false" class="ui-link">
   <? if($desc>1 && $ongallerys['offer']=='yes'){?> <div class="banner-discount banner-text-blink" ><?=$desc?>% </div><? }?>
 <?  if($ongallerys['floatingimg']=='yes' && $countImg>2){?>  <div id="banner-show-case">
 <?  $sqlmediaco1 = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
							$trecount1=mysql_fetch_array($sqlmediaco1);
							 if ($trecount1['trecd'] != '0') {
								$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 2";
								}
							else{
								$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 2";
								}
							$qdataM=query_execute("$sqlmedia");
							while($rowdatM = mysql_fetch_array($qdataM)){
								if($rowdatM["media_file"]){
									$src = '/home/dogspot/public_html/shop/item-images/orignal/'.$rowdatM["media_file"];
									$imageURL='/home/dogspot/public_html/imgthumb/300x280-'.$rowdatM["media_file"];
									$imageURLS='https://www.dogspot.in/imgthumb/300x280-'.$rowdatM["media_file"];?>
									
   
    <img class="banner-img" src="<?=$imageURLS?>">
    <? } }?>
   </div> <? }else{?>
    <?  $sqlmediaco1 = mysql_query("SELECT COUNT(media_file) as trecd FROM shop_item_media WHERE item_id='$item_id'"); 
							$trecount1=mysql_fetch_array($sqlmediaco1);
								 if ($trecount1['trecd'] != '0') {
								$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1";
								}
							else{
								$sqlmedia = "SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1";
								}
							
							$qdataM=query_execute("$sqlmedia");
							while($rowdatM = mysql_fetch_array($qdataM)){
								if($rowdatM["media_file"]){
									$src = '/home/dogspot/public_html/shop/item-images/orignal/'.$rowdatM["media_file"];
									$imageURL='/home/dogspot/public_html/imgthumb/300x280-'.$rowdatM["media_file"];
									$imageURLS='https://www.dogspot.in/imgthumb/300x280-'.$rowdatM["media_file"];?>
 <img src="<?=$imageURLS?>" style="cursor:pointer; padding-right:0px;" alt="" title="" width="100" height="100"><? } }}?></a>
</div>
 <div class="mobile-add-box-text" style="color:<?=$ongallerys['color'];?> ">
   <h2><a href="/<?=$sqlbanner160['nice_name']?>/" data-ajax="false" class="ui-link"> <?=substr($sqlbanner160['name'],0,25)?>..</a></h2>
 <? if($ongallerys['descP'] || $ongallerys['floatingtext']){?>  
<p class="banner-description">
<? if($ongallerys['descP']){?>
    <?=$ongallerys['descP']?><? }?>
    <? if($ongallerys['floatingtext']){?>
   <marquee behavior="scroll" scrollamount="3" direction="left"  width="100%"><?=$ongallerys['floatingtext']?> </marquee> <? }?>
</p>
<? }?><? if($ongallerys['text']=='yes'){?>
<p style="color: #6c9d06;text-align: left;">
<?=$sqlbanner160['stock_status']?>
</p>
<? }?>
  <div>
   <? if($sqlbanner160['price']<$sqlbanner160['selling_price']){?>
   <del>Rs.<?=number_format($sqlbanner160['selling_price'],0)?></del> &nbsp;  <? }?> <span style="
    color: #ff7a22;font-weight: bold;">Rs.<?=number_format($sqlbanner160['price'],0);?></span></div>
 
   
   
   <div class="mobile-add-buy-now-btn">
   <a href="/<?=$sqlbanner160['nice_name']?>/" data-ajax="false"  class="ui-link"> Buy Now</a>
   </div>
    </div></div></li>
      
      
      <li style="width:100px; height:85px">Current Status : <div id="userpro<?=$ids?>" class="userpro"><? echo $ongallerys['status'];?></div><br />      
      <select name="status<?=$ids?>" id="status<?=$ids?>" class="required"   style="width:77px;">
      <option value="">Change Status</option>
      <option value="active" <? if($ongallerys["status"]=='active'){echo'selected="selected"';}?>>Active</option>
      <option value="inactive"  <? if($ongallerys["status"]=='inactive'){echo'selected="selected"';}?>>Inactive</option>
    </select></li>
       <li style="width:100px; height:85px"><? echo $ongallerys['start_date'];?></li>
      <li style="width:100px; height:85px"><? echo $ongallerys['end_date'];?></li>
       <li style="width:120px; height:85px"><a href="https://m.dogspot.in<? echo $ongallerys['banner_display_url'];?>">https://m.dogspot.in<? echo $ongallerys['banner_display_url'];?></a></li>
      
      <li style="width:70px; height:85px"><?=$ongallerys['banner_text'];?></li>
       <li style="width:120px; height:85px"><?=$ongallerys['descP'];?></li>
       <li style="width:100px; height:85px"><?=$ongallerys['offer'].'/'.$ongallerys['floatingimg'].'/'.$ongallerys['text'];?></li>
       <li style="width:40px; height:85px"><?=$ongallerys['color'];?></li>
       <li style="width:60px; height:85px"><?=$ongallerys['floatingtext'];?></li>
    
      <li style="width:10px; height:85px"><input type="button" onclick="statuschangeM('<?=$ids?>')" value="Save"/></br>
      <a href='/shop/adminshop/add-banners.php?type=edit&id=<?=$ids?>&postion=mobile-product'>Edit</a></li>
      </ul>
      <? $s++; }
	  ?>
    </div>
    <? }?>
    <div class="showPages">
<? 
$pageUrl="/shop/adminshop/manage-banners.php?filter=$filter&page=p";
showPagesLink($totrecord, $maxshow, $pageUrl, $show, $nextShow, $previous, $spUrl);
?>
</div>  
<div style="clear:both"></div>
    <?php require_once('common/bottom.php');?>