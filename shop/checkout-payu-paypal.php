<?php
require_once('../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/paypalfunctions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
$sitesection = "shop";
$session_id = session_id();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="hidden/html; charset=utf-8" />
<title>Checkout</title>
<script src="/jquery/jquery.min.js"></script>
</head>

<body>
<?
		
		$updateCart=query_execute("SELECT u_id, order_amount,order_method FROM shop_order WHERE order_id='$order_id'");
		
		$rowOrderamt = mysql_fetch_array($updateCart);
		$order_amount = $rowOrderamt["order_amount"];
		$u_id = $rowOrderamt["u_id"];
		$order_method= $rowOrderamt["order_method"];
		
		// Update Order amount
		/*$getUsersBill = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '2' AND order_id = '$order_id'");
		$rowUsersBill = mysql_fetch_array($getUsersBill);
		$name = removeSpecialCharFromString($rowUsersBill["address_name"]);
		$email = $rowUsersBill["address_email"];
		$address1 = removeSpecialCharFromString($rowUsersBill["address_address1"]);
		$address2 = removeSpecialCharFromString($rowUsersBill["address_address2"]);
		$address_city = removeSpecialCharFromString($rowUsersBill["address_city"]);
		$address_state = $rowUsersBill["address_state"];
		$address_country = $rowUsersBill["address_country_code"];
		$pin = $rowUsersBill["address_zip"];
		$mobile1 = $rowUsersBill["address_phone1"];
		$phone1 = $rowUsersBill["address_phone2"];
		$fax = $rowUsersBill["address_fax"];*/
		
// get details from shop_order_address table Shipping------------------------------------------
	$getUsersShip = query_execute("SELECT * FROM shop_order_address WHERE address_type_id = '1' AND order_id = '$order_id'");
	$rowUsersShip = mysql_fetch_array($getUsersShip);
	$ship_name = removeSpecialCharFromString($rowUsersShip["address_name"]);
	$ship_address1 = removeSpecialCharFromString($rowUsersShip["address_address1"]);
	$ship_address2 = removeSpecialCharFromString($rowUsersShip["address_address2"]);
	$email = $rowUsersShip["address_email"];
	$ship_city = $rowUsersShip["address_city"];
	$ship_state = $rowUsersShip["address_state"];
	$ship_country = $rowUsersShip["address_country_code"];
	$ship_pin = $rowUsersShip["address_zip"];
	$ship_phone1 = $rowUsersShip["address_phone1"];
	
	$name=addslashes($name);
	$address1=addslashes($ship_address1);
	$address2=addslashes($ship_address2);
	$ship_name=addslashes($ship_name);
	$ship_address1=addslashes($ship_address1);
	$ship_address2=addslashes($ship_address2);
$resultinsert = query_execute("INSERT INTO shop_checkout_logs (checkout_logs_amount, checkout_logs_account_id, checkout_logs_return_url, checkout_logs_description, checkout_logs_reference_no, checkout_logs_name, checkout_logs_address, checkout_logs_city, checkout_logs_state, checkout_logs_postal_code, checkout_logs_country, checkout_logs_email, checkout_logs_phone, checkout_logs_ship_name, checkout_logs_ship_address, checkout_logs_ship_city, checkout_logs_ship_state, checkout_logs_ship_postal_code, checkout_logs_ship_country, checkout_logs_ship_phone, checkout_logs_mode) 
						VALUES('$order_amount', '7860', 'https://www.dogspot.in/shop/response.php?DR={DR}', 'PetShop', '$order_id', '$name', '$address1 $address2', '$ship_city', '$ship_state', '$ship_pin', '$address_country', '$email', '$ship_phone1', '$ship_name', '$ship_address1 $ship_address2', '$ship_city', '$ship_state', '$ship_pin', '$ship_country', '$ship_phone1', 'LIVE')");
        // ==================================
        // PayPal Express Checkout Module
        // ==================================

        //'------------------------------------
        //' The paymentAmount is the total value of 
        //' the shopping cart, that was set 
        //' earlier in a session variable 
        //' by the shopping cart page
        //'------------------------------------
        $paymentAmount = round($order_amount/60);

        //'------------------------------------
        //' When you integrate this code 
        //' set the variables below with 
        //' shipping address details 
        //' entered by the user on the 
        //' Shipping page.
        //'------------------------------------
        $shipToName = "$ship_name";
        $shipToStreet = "$ship_address1";
        $shipToStreet2 = "$ship_address2"; //Leave it blank if there is no value
        $shipToCity = "$ship_city";
        $shipToState = "$ship_state";
        $shipToCountryCode = "IN"; // Please refer to the PayPal country codes in the API documentation
        $shipToZip = "$ship_pin";
        $phoneNum = "$ship_phone1";

        //'------------------------------------
        //' The currencyCodeType and paymentType 
        //' are set to the selections made on the Integration Assistant 
        //'------------------------------------
        $currencyCodeType = "USD";
        $paymentType = "Order";

        //'------------------------------------
        //' The returnURL is the location where buyers return to when a
        //' payment has been succesfully authorized.
        //'
        //' This is set to the value entered on the Integration Assistant 
        //'------------------------------------
        $returnURL = "https://www.dogspot.in/new/shop/response.php?tsid=$session_id&resource=paypal";

        //'------------------------------------
        //' The cancelURL is the location buyers are sent to when they hit the
        //' cancel button during authorization of payment during the PayPal flow
        //'
        //' This is set to the value entered on the Integration Assistant 
        //'------------------------------------
        $cancelURL = "https://www.dogspot.in/new/shop/response.php?tsid=$session_id&resource=paypal";

        //'------------------------------------
        //' Calls the SetExpressCheckout API call
        //'
        //' The CallMarkExpressCheckout function is defined in the file PayPalFunctions.php,
        //' it is included at the top of this file.
        //'-------------------------------------------------
        $resArray = CallMarkExpressCheckout ($paymentAmount, $currencyCodeType, $paymentType, $returnURL,
                                                                                  $cancelURL, $shipToName, $shipToStreet, $shipToCity, $shipToState,
                                                                                  $shipToCountryCode, $shipToZip, $shipToStreet2, $phoneNum
        );
		//$resArray = CallShortcutExpressCheckout( $paymentAmount, $currencyCodeType, $paymentType, $returnURL, $cancelURL);
        $ack = strtoupper($resArray["ACK"]);
        if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
        {
                $token = urldecode($resArray["TOKEN"]);
                $_SESSION['reshash']=$token;
                RedirectToPayPal ( $token );
				couponcheck($order_id);
        } 
        else  
        {
                //Display a user friendly Error on the page using any of the following error information returned by PayPal
                $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
                $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
                $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
                $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);
                
                echo "SetExpressCheckout API call failed. ";
                echo "Detailed Error Message: " . $ErrorLongMsg;
                echo "Short Error Message: " . $ErrorShortMsg;
                echo "Error Code: " . $ErrorCode;
                echo "Error Severity Code: " . $ErrorSeverityCode;
        }


?>
</body>

</html>