<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();


$title="Careers & Jobs at DogSpot.in";
$keyword="Careers & Jobs at DogSpot.in";
$desc="DogSpot is a niche organisation that breathes the culture of work hard and play hard. We’re a close knit team that is based on the pillars of trust and transparency. A bunch of young, fun loving and passionate people we support each other to continually develop new skills and knowledge. Careers & Jobs at DogSpot.in";
 
    $alternate="https://www.dogspot.in/jobs.php";
	$canonical="https://www.dogspot.in/jobs.php";
	$og_url=$canonical;
	$imgURLAbs="";
	
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>

<style>

	.aboutus {
    width: 100%;
    background: white;
}
.story-box a, .aboutus-box a{ color:#6c9d06;  text-decoration:none;}
.story-box a:hover, .aboutus-box a:hover{color: #ff8a00;text-decoration:none;}
.aboutus-box h1, .story-box h2, .aboutus-box h3{ color: #ff8a00;   
    font-size: 28px;
    font-weight: 600;
    text-transform: uppercase;
    line-height: 1;
    letter-spacing: 1px;
    margin: 0 auto 40px;}
	.aboutus-box h3{margin-top: 30px; font-size:22px;}
	.aboutus-box, .story-box {
    padding: 40px 0px;
}
	.aboutus-box p, .story-box p{font-size: 16px;
    letter-spacing: 1px;
    margin-top: 20px;
    line-height: 28px;
    color: #666;}
	.story-box{ background: #f8f8f8;}
	.openposition-box{}
</style>

<section  id="demo">
  <div id="owl-banner-slider" class="owl-carousel">
    <div class="item"><a href="#"><img src="https://ik.imagekit.io/2345/jobs/images/slider-03.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-03.jpg" /></a></div>
    <div class="item"><a href="#"><img src="https://ik.imagekit.io/2345/jobs/images/slider-04.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-04.jpg" /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-05.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-05.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-06.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-06.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-07.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-07.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-09.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-09.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-10.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-10.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-11.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-11.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-12.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-12.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-14.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-14.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-16.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-16.jpg"  /></a></div>
    <div class="item"><a href="#"><img src="https://ik.imagekit.io/2345/jobs/images/slider-17.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-17.jpg"  /></a></div>
    <div class="item"><a href="#"> <img src="https://ik.imagekit.io/2345/jobs/images/slider-18.jpg" data-thumb="https://ik.imagekit.io/2345/jobs/images/slider-18.jpg"  /></a></div>
  </div>
</section>
<div class="container">
<div class="aboutus-box text-center">
<h1>WHY DOGSPOT?</h1>
  <p> DogSpot is a niche organisation that breathes the culture of work hard and play hard. We’re a close knit team that is based on the pillars of trust and transparency.  A bunch of young, fun loving and passionate people we support each other to continually develop new skills and knowledge. </p>
  <p> Our founders - Rana, Shalesh and Vizal are pet lovers that bring to us years of experience to guide us at every stage and make every moment enjoyable. DogSpot gives you the rare chance to work for your fervour for pets within an environment that helps you strive for the best and gives you endless opportunities to learn. </p>
  <h3>Open Positions</h3>
  
  <div class="openposition-box text-left">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Telesales Executive(1)</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse">
        <div class="panel-body"><div class="senior_developor" id="senior_job">
 
      <div> Position: Telesales Executive</div>
    
    <div>Location - Gurgaon</div>
    <div>Nature - Full Time</div>
    <div>Salary - At par with industry standards</div>
    <div><strong>Responsibilities:</strong>
      <ul>
        <li> Contact existing customers to inform them about RC products </li>
        <li>Gather information about their pet and their eating habits</li>
        <li>Answer questions about products and their benefits</li>
        <li>Ask questions to understand customer requirements</li>
        <li>Enter and update customer information in the database</li>
        <li>Take and process orders in an accurate manner</li>
        <li>Handle objections or grievances if any</li>
        <li>Go the "extra mile" to meet sales quota and facilitate future sales</li>
        <li>Keep records of calls and sales and note useful information</li>
      </ul>
    </div>
    <div> <strong>Requirements:</strong>
      <ul>
        <li>Pet owner</li>
        <li>Proven experience as telesales representative or other sales role</li>
        <li>Proven track record of successfully meeting sales quota preferably over the phone</li>
        <li>Good knowledge of relevant computer programs (e.g. CRM software, MS Excel) and telephone systems</li>
        <li>Ability to learn about products and services and describe/explain them to prospects</li>
        <li>Excellent knowledge of English</li>
        <li>Excellent communication and interpersonal skills</li>
        <li>Cool-tempered and able to handle rejection</li>
        <li>Outstanding negotiation skills with the ability to resolve issues and address complaints</li>
      </ul>
    </div>
    <div>Please email your Resume to 
    <a href="mailto:meghna@dogspot.in">meghna@dogspot.in</a></div>
  </div></div>
    </div>
</div>
</div>
</div>
</div>
</div>

  
<script type="text/javascript" src="/bootstrap/js/owl.carousel.min.js"></script> 
<script>
    $(document).ready(function() {
      $("#owl-banner-slider").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
	  autoPlay: 4000,
      singleItem : true
	  

      });
    });
	
    </script>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
