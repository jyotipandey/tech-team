<?php

    //ini_set('memory_limit','512M');
	//ini_set('display_errors',1);
	//ini_set('display_startup_errors',1);
	//error_reporting(-1);
	   require_once('../constants.php');
   require_once($DOCUMENT_ROOT.'/database.php');
   require_once($DOCUMENT_ROOT.'/functions.php');
   require_once($DOCUMENT_ROOT . '/constants.php');
   require_once($DOCUMENT_ROOT . '/session.php');
   require_once($DOCUMENT_ROOT . '/shop/functions.php');
	
	$sitesection='adoption';
	$show=$section[3];
	if ($show != 0) {
    $ifpage = $show;
}
$maxshow = 10;
if (empty($show)) {
    $show = 0;
} else {
    $show = $show - 1;
}
$showRecord = $show * $maxshow;
$nextShow = $showRecord + $maxshow;

?>
<!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title>Pet Adoption | Dog Adoption | Free Adoption in India | DogSpot.in</title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="Pet Adoption India an initiative by DogSpot.in - find pets for free adoption rescued by animal shelters and pet lovers in India. Adopt a Pet and show that you care." />
    <meta name="keywords" content="Pets Adoption India, Search Free Pets for Adoption & Adopt a Pet" />
    <link rel="canonical" href="https://www.dogspot.in/adoption/" />
    <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1,initial-scale=1">

     <? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
     <style amp-custom> /**---------------------
		  *
		  * Pagination
		  *
		  *---------------------**/
		.pagination {
			text-align: center;
			margin: 15px 0 0
		}

		.pagination a {
			display: inline-block;
			margin: 0;
			border-radius: 50%;
			color: #212121;
			min-width: 30px;
			min-height: 30px;
			line-height: 30px;
		}

		.pagination a.active {
			color: #FFF;
			background-color: #f82e56
		}

        /**---------------------
		*
		* Product List Item
		*
		*---------------------**/
		.bones-product-list-item,
		.bones-product-list-item .preview{
			position: relative;
			display: block;
		}

		.bones-product-list-item .preview amp-img{
			position: relative;
			z-index: 1;
		}

		.bones-product-list-item .preview .badge{
			position: absolute;
			z-index: 2;
			top: 5px;
			left: 5px
		}

		.bones-product-list-item .preview .badge:last-child{
			left: auto;
			right: 5px;
		}

		.bones-product-list-item .categories{padding: 7px 0 0;}

		.bones-product-list-item .categories a{
			color: rgba(41,37,42,.54);
			font-size: 1.1rem;
			font-weight: 400;
		}

		.bones-product-list-item .categories a:not(:last-child):after{
			content: ', ';
			display: inline;
		}

		.bones-product-list-item > a h2{
			font-size: 1.2rem;
            margin: 0;
		}

		.bones-product-list-item .prices .old{
			font-size: 1rem;
			font-weight: 700;
			vertical-align: bottom;
			opacity: .24;
			text-decoration: line-through;
		}

		.bones-product-list-item .prices .current{
			font-size: 1.4rem;
			font-weight: 700;
			vertical-align: bottom;
		}

		.bones-product-list-item.sold-out:after{
			content: '';
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background-color: rgba(255,255,255,.54);

			z-index:5;
		}



        /**---------------------
		  *
		  * Title
		  *
		  *---------------------**/
		.bordered-title{
			border-bottom: 1px solid rgba(0,0,0,.12);
			margin: 0 -15px;
			padding: 0 15px 8px;
		}

		.bordered-title h3,
		.bordered-title .h3{
			margin: 0;
			line-height: 1.4;
		}

		.bordered-title h5,
		.bordered-title .h5{
			opacity: .54;
			margin: 0;
		}

        /*! CSS Used from: Embedded */
.adoption_ok{text-decoration:none;}
.adoption_ok{text-transform:uppercase;}
input,select{font-size:1em;line-height:1.3;font-family:lato,sans-serif;}
*{padding:0;}
*{margin:0;font-family:lato,sans-serif;box-sizing:border-box;}
.adoption_ok{display:inline-block;width:100%;}
.adoption_header{background:#f2f2f2;padding:10px 15px;margin:5px 5px 25px;border-radius:5px;}
.adoption_ok{height:auto;padding:8px 0;border-radius:3px;border:0;background:#ccc;color:#555;cursor:pointer;}

        /* /INDEX PAGE STYLES */
    </style> 
  <script type="application/ld+json">{ "@context":"http://schema.org", "@type":"Blog", "@id":"https://www.dogspot.in/<?=$section[0];?>/", "about":{ "@type":"CreativeWork", "name":"Pet Adoption" }, "headline":"Pet Adoption", "description":"Pet Adoption India an initiative by DogSpot.in - find pets for free adoption rescued by animal shelters and pet lovers in India. Adopt a Pet and show that you care."}</script>
<?php include($DOCUMENT_ROOT.'/new/header-banner.php');
            $breed=mysql_query("SELECT distinct pa.breed_nicename, db.breed_name FROM puppies_available as pa, dog_breeds as db WHERE pa.adoption='yes' AND publish_status='publish' AND pa.breed_name !=''  AND pa.breed_nicename=db.nicename ORDER BY db.breed_name ");
            //End Breed 
            
            /// Select Location 
            $location = mysql_query("SELECT distinct(city_id),city_nicename,city FROM puppies_available WHERE adoption='yes' AND publish_status='publish' AND city !='' AND publish_status !='markdel' ORDER BY city  ");
            // End Location
            
			 
			
				$selectart5t = query_execute_row("SELECT city_name, city_nicename FROM city WHERE city_id = '$citySearch' ");
				//echo"SELECT breed_name FROM dog_breeds WHERE nicename = '$dog_breedSearch' ";
	$selectdog_breedSearch = query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename = '$dog_breedSearch' ");
	$city_name=$selectart5t['city_name'];
	$city_nicename=$selectart5t['city_nicename'];
	$breed_name=$selectdog_breedSearch['breed_name'];
				if($type=='dog'){
			  $pets="Dogs";
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
				  
				//  echo $dog_breedSearch.$citySearch;
				if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
				$pageh=ucwords($breed_name).' Dogs for Adoption';
				$title='Adopt a '.ucwords($breed_name).' Dogs | '.ucwords($breed_name).' Dogs for Adoption in India ';
				$keyword='Adopt a '.ucwords($breed_name).' Dogs, '.ucwords($breed_name).' Dogs for Adoption in India ';
				$desc='List of '.ucwords($breed_name).' dogs and puppies for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers';	
  				$sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  				$sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){


				$pageh='Dogs for Adoption in '.$city_name;
				$title='Dogs for Adoption in '.$city_name.' | Adopt a Dog for free in '.$city_name;
				$keyword='Dogs for Adoption in '.$city_name.' , Adopt a Dog for free in '.$city_name;
				$desc='Listings for dog adoption in Delhi '.$city_name.'. Get information on free dog adoption from shelters in '.$city_name.' or adopt dogs from a dog lover';

  				$sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) )  AND breed_nicename!='cats' ORDER BY status ASC ";
  				$sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
			}
			elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){



	$pageh=ucwords($breed_name).' Dogs for Adoption in '.$city_name;
	$title=ucwords($breed_name).' for Adoption in '.$city_name.' | Adopt '.ucwords($breed_name).' Dog in '.$city_name;
	$keyword=ucwords($breed_name).' for Adoption in '.$city_name.' , Adopt '.ucwords($breed_name).' Dog in '.$city_name.' adopt '.ucwords($breed_name).' in'.ucwords($breed_name);
	$desc='Local listings for '.ucwords($breed_name).' adoption in '.$city_name.'. Information on where and how to adopt '.ucwords($breed_name).' dogs in '.$city_name;
	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
$pageh=ucwords($breed_name).' Dogs for Adoption';
$title=ucwords($breed_name).' Dogs for Adoption  | Find information on '.ucwords($breed_name).' Dogs for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Dogs for Adoption , Find information on '.ucwords($breed_name).' Dogs for Adoption , DogSpot.in ';
	$desc='Find information on '.ucwords($breed_name).' Dogs for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';	  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Puppies for Adoption';
	$title='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$keyword='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$desc='Find information on Puppies for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

   $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE adoption='yes'  AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
	$pageh='Dogs for Adoption in '.$city_name;
	$title='Dogs for Adoption in '.$city_name.' | Free Dog Adoption | Pet Adoption | DogSpot.in';
	$keyword=' Dogs for Adoption in '.$city_name.', Free Dog Adoption,Pet Adoption,DogSpot.in ';
	$desc='Find Dog for Adoption in '.$city_name.' at Dogspot.in. See Dog for Adoption, Pet for Adoption at DogSpot.in';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif(!$citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Dogs for Adoption';
$title='Adopt a '.ucwords($breed_name).' Dogs | '.ucwords($breed_name).' Dogs for Adoption in India ';
	$keyword='Adopt a '.ucwords($breed_name).' Dogs, '.ucwords($breed_name).' Dogs for Adoption in India ';
	$desc='List of '.ucwords($breed_name).' dogs and puppies for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers';	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}
elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
}else{
	$pageh=ucwords($breed_name).' Dogs for Adoption'.$city_name;
$title=ucwords($breed_name).' Dogs for Adoption in '.$city_name.' | Find '.ucwords($breed_name).' Dogs for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Dogs for Adoption in '.$city_name.' , Find '.ucwords($breed_name).' Dogs for Adoption , DogSpot.in ';
	$desc='Find '.ucwords($breed_name).' Dogs for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
 
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
//echo $sql;
$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Dogs for Adoption';
	$title='Dogs for Adoption | Adopt a Dog | Free Dog Adoptions India';
	$keyword='Dogs for Adoption , Adopt a Dog , Free Dog Adoptions India';
	$desc='Looking for a dog to adopt? Select from a list of dogs available for free adoption from shelters and dog lovers in India';
//echo "SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
   $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $sqlall="SELECT * FROM puppies_available WHERE adoption='yes'  AND publish_status ='publish' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC";
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats'  ORDER BY status ASC ";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` < YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {
	$totrecord=0;
  
}
		}elseif($type=='puppy')
		{
			 $pets="Puppies";
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Puppies for Adoption';
	$title='Adopt a '.ucwords($breed_name).' Puppies | '.ucwords($breed_name).' Puppies for Adoption in India';
	$keyword='Adopt a '.ucwords($breed_name).' Puppies , '.ucwords($breed_name).' Puppies for Adoption in India';
	
	$desc='List of '.ucwords($breed_name).' puppies and puppies for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers.';
	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){
	

	$pageh='Puppies for Adoption in '.$city_name;
	$title='Puppies for Adoption in '.$city_name.' | Adopt a Puppies for free in '.$city_name;
	$keyword='Puppies for Adoption in '.$city_name.' , Adopt a Puppies for free in '.$city_name;
	$desc='Listings for Puppies adoption in '.$city_name.'. Get information on free Puppies adoption from shelters in '.$city_name.' or adopt Puppies from a Puppies lover';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) )AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Puppies for Adoption in '.$city_name;
	$title=ucwords($breed_name).' for Adoption in '.$city_name.' | Adopt a '.ucwords($breed_name).' Puppies in '.$city_name;
	$keyword=ucwords($breed_name).' for Adoption in '.$city_name.' , Adopt '.ucwords($breed_name).' Puppies in '.$city_name.' , Adopt '.ucwords($breed_name).' in '.$city_name;
	$desc='Local listings for '.ucwords($breed_name).' adoption in '.$city_name.'. Information on where and how to adopt '.ucwords($breed_name).' Puppies in '.$city_name.'. ';
	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) )AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
	$pageh=ucwords($breed_name).' Puppies for Adoption';
	$title=ucwords($breed_name).' Puppies for Adoption  | Find information on '.ucwords($breed_name).' Puppies for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Puppies for Adoption , Find information on '.ucwords($breed_name).' Puppies for Adoption , DogSpot.in ';
	$desc='Find information on '.ucwords($breed_name).' Puppies for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Puppies for Adoption';
	$title='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$keyword='Puppies for Adoption  | Find information on Puppies for Adoption | DogSpot.in ';
	$desc='Find information on Puppies for Adoption , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

  $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}else{
	$pageh=ucwords($breed_name).' Puppies for Adoption in '.$city_name;
	$title=ucwords($breed_name).' Puppies for Adoption in '.$city_name.' | Find '.ucwords($breed_name).' Puppies for Adoption | DogSpot.in ';
	$keyword=ucwords($breed_name).' Puppies for Adoption in '.$city_name.' , Find '.ucwords($breed_name).' Puppies for Adoption , DogSpot.in ';
	$desc='Find '.ucwords($breed_name).' Puppies for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';

	
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
}

$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif(!$citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Puppies for Adoption';
	$title='Adopt Puppies | Free Puppy Adoption in India';
	$keyword='Adopt Puppies , Free Puppy Adoption in India';

	$desc='Find yourselves a puppy to adopt from a list of dog puppies up for adoption from puppy care-takers and pup shelters in India.';

  $sql="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE  adoption='yes' AND publish_status ='publish' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes' AND  `year` > YEAR( DATE_SUB( CURDATE( ) , INTERVAL 1 YEAR ) ) AND breed_nicename!='cats' ORDER BY status ASC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {
	$totrecord=0;
  
}
		}elseif($type=='cat')
		{
			
			$pets="Cats";
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
	
	$pageh='Cats for Adoption';
		$title='Cats for Adoption | Adopt a Cat | Free Cat Adoptions India';
	$keyword='Cats for Adoption , Adopt a Cat , Free Cat Adoptions India ';
	$desc='Looking for a cat to adopt? Select from a list of cats available for free adoption from shelters and cat lovers in India.';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' ORDER BY status ASC LIMIT $showRecord, $maxshow ";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  ORDER BY status ASC";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Find Cats for Adoption | DogSpot.in ';
	$keyword='Cats for Adoption in '.$city_name.' , Find Cats for Adoption , DogSpot.in ';
	$desc='Find Cats for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC";
}
elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){
	
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Adopt a Cats in '.$city_name;
	$keyword='Cats for Adoption in '.$city_name.' , Adopt Cat in '.$city_name.' , Adopt Cats in '.$city_name;
	$desc='Local listings for Cats adoption in '.$city_name.'. Information on where and how to adopt Cats in '.$city_name;
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status ASC";}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
	$pageh='Cats for Adoption';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status ASC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status ASC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Find Cats for Adoption | DogSpot.in ';
	$keyword='Cats for Adoption in '.$city_name.' , Find Cats for Adoption , DogSpot.in ';
	$desc='Find Cats for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish'  ORDER BY status DESC";
}
elseif(!$citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh='Cats for Adoption';
		$title='Cats for Adoption | Adopt a Cat | Free Cat Adoptions India';
	$keyword='Cats for Adoption , Adopt a Cat , Free Cat Adoptions India ';
	$desc='Looking for a cat to adopt? Select from a list of cats available for free adoption from shelters and cat lovers in India.';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch'  AND adoption='yes' AND publish_status ='publish' ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch'  AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}
elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}else{
	$pageh='Cats  for Adoption in '.$city_name;
	$title='Cats  for Adoption in '.$city_name.' | Find Cats for Adoption | DogSpot.in ';
	$keyword='Cats for Adoption in '.$city_name.' , Find Cats for Adoption , DogSpot.in ';
	$desc='Find Cats for Adoption in '.$city_name.' , And here you can list your Dogs & Puppies for Adoption at DogSpot.in ';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}

$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {
	$totrecord=0;
  
}
		
		
		}else
		{
		
		
			  if($dog_breedSearch || $citySearch || $dog_statusSearch ){
				  
			$pets="Pets";
if($dog_breedSearch && !$citySearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' Pets for Adoption';
	$title=ucwords($breed_name).' Pets for Adoption | '.ucwords($breed_name).' Puppies for Adoption in India';
	$keyword=ucwords($breed_name).' Pets for Adoption , '.ucwords($breed_name).' Puppies for Adoption in India';
	
	$desc='List of '.ucwords($breed_name).' and '.ucwords($breed_name).' for adoption in India. Information on adopting a '.ucwords($breed_name).' from local animal shelters and '.ucwords($breed_name).' lovers.';
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes' ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
}elseif($citySearch && !$dog_breedSearch && !$dog_statusSearch){
	$pageh='Pet Adoption '.$city_name;
	$title='Pet Adoption '.$city_name.' | Adopt Pets up for free Adoption in '.$city_name;

	$keyword='pets adoption '.$city_name.', pet adoptions '.$city_name.', pets for adoption '.$city_name.', free pet adoption '.$city_name.', adopt pets in '.$city_name.'';
		$desc='Local listings for pet adoption in '.$city_name.'. Adopt a pet rescued by pet shelters or up for adoption by other pet lovers in '.$city_name.'.';
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
}
elseif($citySearch && $dog_breedSearch && !$dog_statusSearch){
	$pageh=ucwords($breed_name).' for Adoption in '.$city_name;
	$title=ucwords($breed_name).' for Adoption in '.$city_name.' | Adopt a '.ucwords($breed_name).' in '.$city_name;
	$keyword=ucwords($breed_name).' for Adoption in '.$city_name.', Adopt '.ucwords($breed_name).'  in '.$city_name.' , Adopt '.ucwords($breed_name).' in '.$city_name;
	$desc='Local listings for '.ucwords($breed_name).' adoption in '.$city_name.'. Information on where and how to adopt '.ucwords($breed_name).'  in '.$city_name.'. ';
	
	$sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status DESC LIMIT ";
}
elseif(!$citySearch && $dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish' ORDER BY status DESC";
}
elseif($citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes'  AND publish_status ='publish'  ORDER BY status DESC";
}

elseif(!$citySearch && !$dog_breedSearch && $dog_statusSearch){
  $sql="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE  status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}else{
  $sql="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE breed_nicename = '$dog_breedSearch' AND city_id = '$citySearch' AND status = '$dog_statusSearch' AND adoption='yes' AND publish_status ='publish'  ORDER BY status DESC";
}

$selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}elseif($source !='')
{
	
				
				
 $sql="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC LIMIT $showRecord, $maxshow";
  $sqlall="SELECT * FROM puppies_available WHERE source = '$source' AND publish_status ='publish' AND adoption='yes'  ORDER BY status DESC";	
 
  $selecg = mysql_query ("$sql");
 if(!$selecg){	die(sql_error());  }
		
	$numrecord = mysql_query ("$sqlall");
	$totrecord = mysql_num_rows($numrecord);
	$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
}
else {

                $sql="SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' ORDER BY status ASC LIMIT $showRecord, $maxshow";
				$sqlall=mysql_query("SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' ORDER BY status ASC");
            $totrecord=mysql_num_rows($sqlall);
			$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
  
}
		
			
		}
			
            // Select puppies
          /*  if(isset($_POST['search'])){
$citySearch=$_POST['city'];
$dog_breedSearch=$_POST['breed'];
$dog_statusSearch=$_POST['status'];
				
				
				if($_POST['breed']!=""){
                    $sql_breed = " AND breed_nicename='".$_POST['breed']."'";
                }
                if($_POST['city']!=""){
                    $sql_city = " AND city_id='".$_POST['city']."'";
                }
                if($_POST['status']!=""){
                    $sql_status = " AND status='".$_POST['status']."'";
                }
                $sql="SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' $sql_breed $sql_city $sql_status ORDER BY status ASC LIMIT 0,100";
		}else{
                $sql="SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' ORDER BY status ASC LIMIT $showRecord, $maxshow";
				$sqlall=mysql_query("SELECT * FROM puppies_available WHERE publish_status='publish' AND adoption='yes' ORDER BY status ASC");
            $totrecord=mysql_num_rows($sqlall);
			$next = $show + $maxshow;  
	$previous = $show - $maxshow;
	
			}*/
			
            // End Puppies data
        ?> 

    <div class="container-fluid">
		<div class="space-2"></div>

		<div class="row">
			<div class="col-xs-12">
            <div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <p itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a>
                            <span itemprop="position" content="1">&raquo;</span>
                        </p>
                        <p itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="/adoption/amp/" itemprop="item"><span itemprop="name">Pet Adoption</span></a>
                           
                            <span itemprop="position" content="2">&raquo;</span>
                        </p>
                       </div>
				<div class="bordered-title">
					<h1 class="h3">Pet For Adoption</h1>
				</div><!-- TITLE ENDS -->
                
			</div><!-- COL-XS-12 ENDS -->
		</div><!-- ROW ENDS -->

		<!-- BONES-PRODUCT-GRID AND BONES-PRODUCT-LIST-ITEM STARTS -->
		<div class="row">
        
			<div class="col-xs-12">
				<div class="bones-products-grid cols-2">
                <? $adoptionData = mysql_query($sql);
			$perRow = 2; $i=1; $jk=0;
			$count = mysql_num_rows($adoptionData);
			if($count > 0){
				while($getRow = mysql_fetch_array($adoptionData)){
						$iid++;
		            $div_count++;
					$jk++;
					if($getRow['puppi_img']){
						$src = '/home/dogspot/public_html/puppies/images/'.$getRow['puppi_img'];
						$imageURL1='/puppies/images/400x300_'.$getRow['puppi_img'];
					}else{
						$src = '/home/dogspot/public_html/dogs/images/no-photo-t.jpg';
						$imageURL1='/dogs/images/no-photo-t.jpg';
					}
					$dest = "/home/dogspot/public_html".$imageURL1;
					createImgThumbIfnot($src,$dest,'400','300','ratiowh'); 
					if($getRow['status']=='rescue' || $getRow['status']=='foster'){
					 $colr='color:green;';
					 $status='Available';
					 }else
					 {
					 $colr='color:#f00;';
					 $status='Adopted';
					 
				     }
		?>
					<div class="bones-product-list-item">
						<a href="/adoption/<?=$getRow['puppy_nicename']; ?>/amp/" class="preview">
							<amp-img
									srcset="https://www.dogspot.in/puppies/images/<?=$getRow['puppi_img']; ?> 767w"
									width="165"
									height="225"
									layout="responsive"></amp-img>
							<span class="badge font-2 primary-bg"><?=$status?></span>
						</a>
						<div class="categories text-center">
							<a href="#"><?php if($getRow['puppi_sex']=='M'){ echo "Male"; }else{ echo "Female";} ?></a>
							<a href="#"><?=$getRow['city']; ?></a>
						</div>
						<a href="product-details.html"><h2 class="text-center"><?=$getRow['puppi_name']; ?></h2></a>
						<div class="prices text-center">
							<span class="current font-2"><?= $getRow['puppi_breed'];?></span>
						</div>
					</div><!-- BONES PRODUCT LIST ITEM ENDS -->
<? }}?>
				</div><!-- BONES-PRODUCTS-GRID ENDS -->
			</div><!-- COL-XS-12 ENDS -->
		</div><!-- ROW ENDS -->

		<div class="row">
			<div class="col-xs-12">
				<!-- PAGINATION STARTS -->
				<div class="pagination">
			<?php
                        $pageUrl = "/$section[0]/amp/page";
                    showPages_courses($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl, $url);
                    ?>		</div><!-- PAGINATION ENDS -->
			</div><!-- COL-XS-12 ENDS -->
		</div><!-- ROW ENDS -->

	</div><!-- CONTAINER-FLUID ENDS -->

<?
	//header("Content-type: application/javascript; charset=iso-8859-1");
include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>