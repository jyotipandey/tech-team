<?
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

header('Access-Control-Allow-Credentials: true');
header('AMP-Access-Control-Allow-Source-Origin: https://www.dogspot.in');
if(!empty($_GET))
{
   $breed = $_GET['breed'];
    $city = $_GET['city'];
	if($breed && $city)
	{
	$redirect_url="https://www.dogspot.in/amp/".$breed."-for-adoption-in-".$city."/";	
	}elseif($breed)
	{
	$redirect_url="https://www.dogspot.in/amp/".$breed."-for-adoption/";	
	}elseif($city)
	{
	$redirect_url="https://www.dogspot.in/amp/adoption-in-".$city."/";	
	}
	if( empty($redirect_url))
        {
            header("Access-Control-Expose-Headers: AMP-Access-Control-Allow-Source-Origin");
        }
        else
		{
            header("AMP-Redirect-To: ".$redirect_url);
            header("Access-Control-Expose-Headers: AMP-Redirect-To, AMP-Access-Control-Allow-Source-Origin"); 
			header("Location: ".$redirect_url);       }
            $data=array('subscription' => 'complete','message' =>"he");
            $das = json_encode($data);
	        echo $das;
	      die();
    }?>
