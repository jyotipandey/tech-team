<?php
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/dogs/arraybreed.php');
$ajaxbody    = "puppie";
$sitesection = "adoption";
$user_ip     = ipCheck();
$puppyDet       = puppyDetailsNice($puppy_nicename);
$puppy_owner    = $puppyDet['userid'];
$puppytatatus   = $puppyDet["publish_status"];
$breed_nicename = $puppyDet['breed_nicename'];
$puppi_breed    = $ArrDogBreed[$breed_nicename]; //fetch from array
$puppi_name        = $puppyDet['puppi_name'];
$source        = $puppyDet['source'];
$status            = $puppyDet['status'];
$condition1       = $puppyDet['condition1'];
$temperament           = $puppyDet['temperament'];
$day            = $puppyDet['day'];
$month          = $puppyDet['month'];
$year           = $puppyDet['year'];
$puppi_img      = $puppyDet['puppi_img'];
$c_date         = $puppyDet['c_date'];
$cityPuppy      = $puppyDet['city'];
$state          = $puppyDet['state'];
$puppi_desc = strip_tags($puppyDet['puppi_desc_amp']);
if($breed_nicename=='cats')
{
$puppi_breed='Cat';	
}
if (!$breed_nicename) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();
}


?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title><?= "$puppi_name | $puppi_breed for Adoption | $cityPuppy | $source "; ?> | DogSpot</title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="<?= "Adopt a Dog or Puppy in India.$puppi_name , $puppi_breed for Adoption, $cityPuppy , $puppy_owner" ?>" />
    <meta name="keywords" content="<?= "$puppi_name, $puppi_breed for Adoption, $cityPuppy , $puppy_owner" ?>" />
    <link rel="canonical" href="https://www.dogspot.in/adoption/<?=$section[2];?>/" />
  <? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
 <? $post_title = "$puppi_name | $puppi_breed | $cityPuppy | $source  | Adoption | DogSpot ";
$pageDesc = "Adopt a Dog or Puppy in India.$puppi_name , $puppi_breed , $cityPuppy , $puppy_owner , Adoption";
?>
  <script type="application/ld+json">{ "@context":"http://schema.org", "@type":"Blog", "@id":"https://www.dogspot.in/adoption/", "about":{ "@type":"CreativeWork", "name":"Dog Blog" }, "headline":"Dog Adoption", "description":"Adopt a Dog or Puppy in India", "blogPost":[ { "@type":"BlogPosting", "name":"<?= "$puppi_name | $puppi_breed for Adoption | $cityPuppy | $source | $puppi_id "; ?> | DogSpot", "headline":"<?=$post_title?>", "articleBody": "<?=strip_tags($pageDesc).'....';?>", "datePublished":"<?=$c_date?>", "dateModified":"<?=$c_date?>",
   "mainEntityOfPage":"https://www.dogspot.in/adoption/<?=$section[1];?>/", "author":"DogSpot", "publisher":{ "@type":"organization", "name": "DogSpot", "logo": { "@type": "ImageObject", "url": "https://ik.imagekit.io/2345/new/common/images/dogspot-new-logo-final.png", "width": 177, "height": 60 } }, "image": { "@type": "ImageObject", "url": "https://ik.imagekit.io/2345/images/dogspot-logo-new-white.png", "width": 696, "height": 391 } } ] }</script>
  	<div class="adoption_dsc_blk" style="float:left; width:100%">
                <div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a>
                            <span itemprop="position" content="1"></span>
                        </span><span>/</span>
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="/amp/adoption/" itemprop="item"><span itemprop="name">Pet Adoption</span></a>
                            <span itemprop="position" content="2"></span>
                        </span><span>/</span>
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="#" itemprop="item"><span itemprop="name"><?=$puppi_name;?>(<?=$puppi_breed;?>)</span></a>
                           <meta itemprop="position" content="3" />
                        </span>
                    </div>
                    <div class="space-2"></div>
    <h1 style="margin-top:0px;"><?=$puppi_name; ?>(<?=$puppi_breed; ?> for Adoption <? if($city){ echo "in ".$city; } ?>)</h1>
               
  <?
//pulish status starts here.............................................................
	$puppi_desc = makeClickableLinks($puppi_desc);
	$puppi_desc = stripslashes($puppi_desc);
	$puppi_desc = breakLongWords($puppi_desc, 30, " ");
	if($puppi_img){
	$src = $DOCUMENT_ROOT.'/puppies/images/'.$puppi_img;
	$imageURL1='/puppies/images/330x400_'.$puppi_img;
	}else{
	$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
	$imageURL1='/dogs/images/no-photo-t.jpg';
	}
	
	// Get Image Width and Height, Updated date: 18-Nov-2014
	$dest = $DOCUMENT_ROOT.$imageURL1;
	createImgThumbIfnot($src,$dest,'330','400','ratiohh'); 
?><amp-img src="<?=$imageURL1?>" width=330 height=400 alt="<?=$puppi_name?>" layout="responsive"></amp-img>
                     <? if($status=='adopted'){?><div style="position: absolute;opacity: 0.8;
"><amp-img src="https://www.dogspot.in/adoption/images/adopted-images.jpg" width="661" height="313"  alt="Aodpted Stamp" title="Aodpted Stamp" layout="responsive"
></div>
                <? }?>		<? if($source=='Sai'){ $source='Sai Ashram';} ?>
    		<div class="adoption_besic_info">Basic Information</div>
             <div class="space-1"></div>
        <table class="adoption_tab menu">
        <? if($puppi_name){?><tr><td>Dog Name:</td><td><?=$puppi_name; ?></td></tr><? }?>
        <? if($puppi_breed){?><tr><td>Breed Name:</td><td><?=$puppi_breed; ?></td></tr><? }?>
         <? if($source){?><tr><td>Source:</td><td> <?=$source; ?></td></tr><? }?>
          <? if($status){?>
        <tr> <? if($status=='rescue' || $status=='foster'){
					 $colr='color:green;';
					 $status='Available';
					 }else
					 {
					 $colr='color:#f00;';
					 $status='Adopted';
					 }?>
            <td>Status:</td><td> <span style=" <?=$colr?>"><?=$status; ?></span></td></tr><? }?>
         <? if($temperament){?><tr><td>Temperament:</td><td> <?=$temperament; ?></td></tr><? }?>
        <? if($condition1){?><tr><td>Condition:</td><td> <?=$condition1; ?></td></tr><? }?>
        <? if($puppi_sex){?><tr><td>Gender:</td><td> <? if($puppi_sex=='M' || $puppi_sex=='m'){ echo "Male"; }else{ echo "Female"; }; ?></td></tr><? }?> <? if($city){?><tr><td>Location:</td><td> <?=$city; ?></td></tr><? }?>
        <? if($day && $month && $year){?><tr><td>Age: </td><td><? print (pupDOB($day, $month, $year)); ?> </td></tr> <? }?></table><? if($status=='adopted'){ ?><div style="color:#F00"><b>This Pet has already  been adopted, However we can help you get furry friend home.</b></div><? }?><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="5144837740" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div>
               <? if($status!='adopted' && $userid !='Guest'){
					$cur_date2233=date("Y-m-d");
					$selectpup=query_execute_row("SELECT count(*) as cc FROM adoptionmail WHERE userid='".$userid."' AND c_date='".$cur_date2233."'");
				    $selectDatapup=query_execute_row("SELECT count(*) as cc FROM adoptionmail WHERE userid='".$userid."' AND c_date='".$cur_date2233."' AND dog_id='$puppi_id'");
					if($selectpup['cc']=='0'){
					?>
                    <form class="col-xs-12" method="post" action-xhr="/amp/assets/Adoption_mail/email.php" target="_top">
			<input  type="hidden" name="puppy_owner" value="<?=$puppy_owner?>">
			<input  type="hidden" name="userid" value="<?=$userid?>">
			<input  type="hidden" name="puppi_name" value="<?=$puppi_name?>">
			<input  type="hidden" name="puppi_id" value="<?=$puppi_id?>">
                    <button class="button button-large button-full grass-bg margin-0">Interested</button>
                    <div submit-success>
					<template type="amp-mustache">
						<div class="alert-box alert-box-success alert-box-with-icon">
							<i class="fa fa-check"></i>
							<p>Thank you for your interest. An e-mail has been sent to you!</p>
						</div>
					</template>
				</div>
				<div submit-error>
					<template type="amp-mustache">
						<div class="alert-box alert-box-error alert-box-with-icon">
							<i class="fa fa-times"></i>
							<p>Error in sending mail!</p>
						</div>
					</template>
				</div>
                    </form>
                     <span>	<b></b>
					Click here to adopt a pet, give them forever home
				</span>
                   	<? }
					elseif($selectDatapup['cc']=='1'){?>
                    <div><b>Thanks for your interest in adoption..</b></div>
     		<? }
					else{?>
                    <div><b>You can show interest to adopt a pet only once a day.</b></div>
					<? } }?></div>
					<div class="adoption_dsc_blk"><div class="adoption_text"><?=stripslashes(htmlspecialchars_decode(strip_tags($puppi_desc)));?></div></div><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="3640184387" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div>
							<? if($cityPuppy){$qdataM1=mysql_query("SELECT `puppi_img`, city, puppi_name, puppy_nicename FROM `puppies_available` WHERE city='$cityPuppy' AND `puppi_img`!='' AND puppi_name!='' AND publish_status='publish' AND  ORDER BY c_date DESC LIMIT 10");}else{ $qdataM1=mysql_query("SELECT `puppi_img`, city, puppi_name, puppy_nicename FROM `puppies_available` WHERE `puppi_img`!='' AND puppi_name!='' AND publish_status='publish' AND  ORDER BY c_date DESC LIMIT 10");}
		 $ttdtotal=mysql_num_rows($qdataM1);
		 if($ttdtotal>2){
		 ?>			<div class="col-xs-12 margin-bottom-0">
                    <h2 class="related_pets">More Pets From <?=$cityPuppy;?></h2> 
				<div class="bones-products-grid cols-2">
							<amp-carousel class="preview"
								  layout="responsive"
								  type="slides"
								  autoplay
								  delay="8500"
								  width="345"
								  height="470">
  <?

while($sqlgetqdata=mysql_fetch_array($qdataM1)){
	$imdet=$sqlgetqdata['puppi_img'];
	$puppi_name=$sqlgetqdata['puppi_name'];
	$puppy_nicename=$sqlgetqdata['puppy_nicename'];
		 if($imdet){
		$src = $DOCUMENT_ROOT.'/puppies/images/'.$imdet;
		$imageURL121='/puppies/images/330x400_'.$imdet;
		//$imageURLface='/puppies/images/350x210-'.$sqlimage1;
		
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL121='/dogs/images/no-photo-t.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL121;
	//echo "jyoti". $src.$dest;
	//$dest1 = $DOCUMENT_ROOT.$imageURLface;
	createImgThumbIfnot($src,$dest,'330','400','ratiohh');
	$image_info = getimagesize($dest);
$image_width = $image_info[0];
$image_height = $image_info[1]; 
?><a href="/amp/adoption/<?=$puppy_nicename?>/" style="text-align:center;"><amp-img src="<?=$imageURL121?>" width=<?=$image_width?> height=<?=$image_height?> alt="<?=$puppi_name?>"></amp-img></a><? }?></amp-carousel></div></div><? }?>
<? include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>