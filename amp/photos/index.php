<?php 
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/session.php");
$ecomm_pagetype='dog-images'; 
 $maxshow = 10;
 
if (empty($show)) {
    $show = 0;
} else {
    $show = $show - 1;
}
$showRecord  = $show * $maxshow;
$nextShow    = $showRecord + $maxshow;
if($breed)
{
$breed_name=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$breed'");
$b_name=$breed_name['breed_name'];
$h1=$b_name." Images";	
$title="$b_name Dog and Puppies Images | DogSpot.in";
$keys="$b_name Dog Images, $b_name Puppies Images, $b_name Dog Pictures, $b_name Dog Photos, $b_name Puppies Pictures, $b_name Puppies Photos, $b_name Pics of Puppies, $b_name Dog and Puppies Images.";
$desc="Every dog breed hazs a different type of appearance and by capturing these appearance at a right time we have made an great collection of images of these dogs. Come and check out the images of $b_name dog here at DogSpot.in";	
}else
{
$h1="Dog Images";	
$title="Dog Images | Puppies Photos Free Download | DogSpot.in";
$keys="Dog Images, Puppies Images, Dog Pictures, Dog Photos, Puppies Pictures, Puppies Photos, Pics of Puppies,Dog and Puppies Images";
$desc="Dogs are very important part of our life and everyone is very much interested in the images and pictures of their loved ones. So, find out the cute and adorable images of these cute dog and puppies.";	
}
?><!doctype html>
<html AMP lang="en">
<head>
<meta charset="utf-8">
<title><?=$title?></title>
<meta name="keywords" content="<?=$keys?>" />
<meta name="description" content="<?=$desc?>" />
<link rel="canonical" href="https://www.dogspot.in/photos/<? if($b_name){ echo "breed/$breed/"; }?>" />
<meta name="author" content="DogSpot" />
<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
<? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
          <div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage">Home</span></a>
                        <span itemprop="position" content="1">&raquo;</span></span> 
                  <?  if($b_name){?><span><span> > </span>
 <a href="https://www.dogspot.in/<?=$breed?>/" itemprop="item">
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<span itemprop="name"><?=$b_name?></span>
 <meta itemprop="position" content="2" /></span></a></span>
 <span> <span> > </span><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <span class="brd_font_bold" itemprop="name">Photos</span>
       <meta itemprop="position" content="3" /> </span></span>
<?  }else{?><span><span> > </span>
<a href="https://www.dogspot.in/photos/" itemprop="item">
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <span itemprop="name">Dog Photos</span>
     <meta itemprop="position" content="2" /> 
     </span></a></span>
     <? }?></div>
          <div class="space-2"></div>
          <h1 style="font-size:16px;"><?=$h1?></h1>
          <form action="/amp/photos/getphoto.php" method="get" name="filter" target="_top" style="float:left; width:100%;">
        	<div class="adoption_header">
            	<div class="adp_m_b">
                	<select name="breed" id="breed">
                    	<option value="">Select Breed</option>
					<?php  $getbreed=mysql_query("SELECT DISTINCT(breed_name),nicename FROM photos_image as pi,dog_breeds as db WHERE  new_keywords LIKE CONCAT('%',breed_name,'%') AND breed_data='yes' ORDER by breed_name ASC");
			  while($breedlist=mysql_fetch_array($getbreed)){ 	?>
 <option value="<?=$breedlist['nicename']?>" <?php if($dog_breedSearch==$breedlist['nicename']){ ?>selected="selected"<?php }?>><?=$breedlist['breed_name']; ?></option>   
                    <?php } ?>
                    </select>
	            </div>
    	        
            	<div>
        	    	<input type="submit" name="search" id="search" value="GO" class="adoption_ok"/>
				</div>
	        </div>
    	     </form>
        <div class="space-2"></div>
              <amp-image-lightbox id="lightbox1" layout="nodisplay"></amp-image-lightbox><div style="margin-top:117px;">
  <?      if($b_name){ 
				 $iid=0;
			 $breedI=query_execute("SELECT pi.image,new_keywords FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%' ORDER BY image_id ASC LIMIT $showRecord, $maxshow");
			 $breedIList=query_execute_row("SELECT count(pi.image) as s FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$b_name."%'");
			 $totrecord=$breedIList['s'];
			 while($dog_details=mysql_fetch_array($breedI)){
			 $image=$dog_details['image'];
			 $new_keywords=$dog_details['new_keywords'];
			 $iid++;
			 $src = $DOCUMENT_ROOT.'/photos/images/'.$image;
			 $imageURL = '/imgthumb/700x500-'.$image;
			 $dest = $DOCUMENT_ROOT.$imageURL;
			  $image_info01 = getimagesize($dest);		
		$image_width01 = $image_info01[0];
		$image_height01 = $image_info01[1];
		     createImgThumbIfnot($src,$dest,'700','500','ratiowh');?>
 <figure><amp-img on="tap:lightbox1" role="button" layout="responsive" tabindex="0" src="<?=$imageURL?>" width="<?=$image_width01?>" height="<?=$image_height01?>" alt="<?=$new_keywords?>"></amp-img>
                   </figure><div class="space-2"></div>
					<? if($show%2 !='0'){ if($iid==4){ ?><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="5144837740" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div><? } if($iid==8){ ?>
<amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="3640184387"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? } }else{if($iid==4){  ?>
               <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="7301949999"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? }if($iid==8){  ?>
               <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="7301949999"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? } } }}else{
						$iid=0;
		 $getbestImage=query_execute("SELECT image,dog_id FROM dogs_activity WHERE dog_image !='no' AND publish_status='publish' ORDER BY id ASC LIMIT $showRecord, $maxshow");  
		 $getbestImageList=query_execute_row("SELECT count(image) as s FROM dogs_activity WHERE dog_image !='no' AND publish_status='publish' ORDER BY id"); 
		  $totrecord=$getbestImageList['s'];
		 while($dog_details=mysql_fetch_array($getbestImage)){
			 $dog_id=$dog_details['dog_id'];
		//	 $img=$DOCUMENT_ROOT."/dogs/images/".$dog_details['image'];
		//	 if($img){
		//	$src = $DOCUMENT_ROOT.'/dogs/images/'.$dog_details['image'];
		//	$imageURL1='/dogs/images/700x500_'.$dog_details['image'];
		//	 }
	    $imageURL1='/dogs/images/700x500_'.$dog_details['image'];
		$dest = $DOCUMENT_ROOT.$imageURL1;
		//createImgThumbIfnot($src,$dest,'700','500','ratiowh'); 
	    $image_info01 = getimagesize($dest);		
		$image_width01 = $image_info01[0];
		$image_height01 = $image_info01[1];
		$iid++;?>
 <figure><amp-img on="tap:lightbox1" layout="responsive" role="button" tabindex="0" src="<?=$imageURL1?>" width="<?=$image_width01?>" height="<?=$image_height01?>" alt="<?=$dog_id?>"></amp-img></figure><div class="space-2"></div><? if($iid==4){$iid=0;?>
                    <div class="text-center margin-top-50 blog-sidebar-box"><amp-ad width=300 height=250
    type="doubleclick"
    data-slot="/21630298032/Puppies">
  <div placeholder></div>
  <div fallback></div>
</amp-ad></div><? } }}?></div><div class="pagination">
			 <?php
                    if ($section[3]) {
                        $pageUrl = "/amp/photos/breed/$section[3]";
                    } elseif($section[2]=='') {
                        $pageUrl = "/amp/photos";
                    }
                    showPages_courses($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl, $url);
                    ?></div>
		<div class="col-sm-3">
                <div class="blog-sidebar-box">
        <div class="relted-dog-name">
<h2>Most Searched Dog Photos By Breed</h2>
<ul>
<li class="rel-mb margin-top-50"><a data-ajax="false" href="/amp/photos/breed/golden-retriever/">Dog Photos of Golden Retriever <span class="rel-right">&gt;&gt;</span></a> </li>
<li class="rel-ml rel-mb margin-top-50"><a data-ajax="false" href="/amp/photos/breed/german-shepherd-dog-alsatian/">Dog Photos of German Shepherd<span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb margin-top-50"><a data-ajax="false" href="/amp/photos/breed/tibetan-mastiff/">Dog Photos of Tibetan Mastiff<span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb margin-top-50"><a data-ajax="false" href="/amp/photos/breed/rottweiler/">Dog Photos ofr Rottweiler <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb margin-top-50"><a data-ajax="false" href="/amp/photos/breed/great-dane/">Dog Photos of Great Dane <span class="rel-right">&gt;&gt;</span></a></li>
</ul>
</div></div></div><?
include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>
			