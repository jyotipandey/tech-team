<?php
$headers = getallheaders();

if( !isset( $headers['Origin'] ) && !isset( $headers['AMP-Same-Origin'] ) ){
	return;
}else if( isset( $headers['Origin'] ) && !empty( $headers['Origin'] ) ){

	$allow = [
		'https://'.str_replace('.', '-', 'localhost').'.cdn.ampproject.org',
		'https://cdn.ampproject.org',
		'https://'.str_replace('.', '-', 'localhost').'.amp.cloudflare.com',
		'http://localhost'
	];

	if( !in_array($headers['Origin'], $allow) ) return;

}else if( !isset( $headers['AMP-Same-Origin'] ) OR empty( $headers['AMP-Same-Origin'] ) OR $headers['AMP-Same-Origin'] != 'true' ){
	return;
}

header('Access-Control-Allow-Origin: *');
header('AMP-Access-Control-Allow-Source-Origin: ' . urldecode($_GET['__amp_source_origin']));
header('Access-Control-Expose-Headers: AMP-Access-Control-Allow-Source-Origin');

if(!empty( $_POST )){
	//WELCOME AJAX CONTACT FORM MAILER

	//Stripping tags so if even user writes tags like <div></div> or <a href=""></a>, these are just getting cleared.
	//Making ready the content for e-mail by the way
	$mailContent = '';
	foreach($_POST as $key => $val){

		$_POST[$key] = strip_tags($val);
		$mailContent .= $key.' : '.$_POST[$key].'<br>';
	}

	/**
	 * This example shows making an SMTP connection with authentication.
	 */

	//SMTP needs accurate times, and the PHP time zone MUST be set
	//This should be done in your php.ini, but this is how to do it if you don't have access to that
	date_default_timezone_set('Etc/UTC');

	require '../Mail_Class/PHPMailerAutoload.php';

	$mail = new PHPMailer;

	#$mail->SMTPDebug = 3;                          // Enable verbose debug output
	/*$mail->isSMTP();                                // Set mailer to use SMTP
	$mail->Host         = 'SMTP_SERVER_ADDRESS';    // Specify main SMTP server
	$mail->SMTPAuth     = true;                     // Enable SMTP authentication
	$mail->Username     = 'SMTP_USERNAME';          // SMTP username
	$mail->Password     = 'SMTP_PASSWORD';          // SMTP password
	$mail->SMTPSecure   = 'tls';                    // Enable TLS encryption, `ssl` also accepted
	$mail->Port         = 587;                      // TCP port to connect to

	$mail->setFrom('SENDER_EMAIL_ADDRESS', 'Mailer');
	$mail->addAddress('EMAIL_RECEIVER_ADDRESS', 'Joe User');    // Add a recipient
*/

	$mail->isSMTP();                                // Set mailer to use SMTP
	$mail->Host         = 'mail.mobius.studio';    // Specify main SMTP server
	$mail->SMTPAuth     = true;                     // Enable SMTP authentication
	$mail->Username     = 'info@mobius.studio';          // SMTP username
	$mail->Password     = '6744Tcg...';          // SMTP password
	$mail->SMTPSecure   = 'ssl';                    // Enable TLS encryption, `ssl` also accepted
	$mail->Port         = 465;                      // TCP port to connect to

	$mail->setFrom('info@mobius.studio', 'Mailer');
	$mail->addAddress('tlgcngnl@gmail.com', 'Joe User');    // Add a recipient

	$mail->isHTML(true);                                        // Set email format to HTML

	$mail->Subject = 'AMP Template Comment Form';
	$mail->Body    = $mailContent;
	$mail->AltBody = $mailContent;

	$mail->SMTPOptions = array(
		'ssl' => array(
			'verify_peer' => false,
			'verify_peer_name' => false,
			'allow_self_signed' => true
		)
	);

	//send the message, check for errors
	if ($mail->send()) {
		http_response_code(200);
		echo json_encode([]);
	} else {
		http_response_code(500);
		echo json_encode([]);
	}
}