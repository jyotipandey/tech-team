	<?php
//require_once('../../constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
$sitesection = "shop";
$session_id = session_id();
$ant_section = 'Brand';
$ant_page = '';
$ant_category ='';
$maxshow = 10;
if (empty($show)) {
	$show=0;
}else{
$show = $show - 1;
}
$showRecord = $show * $maxshow;
$nextShow = $showRecord + $maxshow;
// redirect it with 301 redirect to the page with /
makeProperURL($requestedUrl, $requested);
// redirect it with 301 redirect to the page with /
$qdata=query_execute("SELECT * FROM shop_brand WHERE brand_status='1' ORDER BY brand_id DESC LIMIT $showRecord, $maxshow");
$qdataall=query_execute("SELECT * FROM shop_brand WHERE brand_status='1'");
$totrecord = mysql_num_rows($qdataall);
// END
// Custom Variable for Google analytics
$CustomVar[2]='Category|Brand';
// Custom Variable for Google analytics
?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title>Pet Products by Brands | DogSpot.in</title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="Buy Branded Pet Products Online from DogSpot.in, India's Largest Online Pet Shop. Get complete range of pet food brands including Pedigree, Drools, Royal Canin and much more." />
    <meta name="keywords" content="Dog Food Brands, pet products brands, Best Dog Food Brand, Cat supplements Brands, Pet accessories Brands, Pet Food Brands In India." />
    <link rel="canonical" href="https://www.dogspot.in/brand/" />
  <? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
      <div id="topPageStat" style="margin-top:0px;" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a>
                            <span itemprop="position" content="1">&raquo;</span>
                        </span>
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="/amp/brand/" itemprop="item"><span itemprop="name">Pet Brands</span></a>
                           <span itemprop="position" content="2">&raquo;</span>
                        </span>
                   </div>
	     <div class="space-2"></div>
      	<h1 style="font-size:inherit;">Brands for Pet Products</h1><div class="ds_brandlisting_wrp">
<? $jk=0;
while($rowItem = mysql_fetch_array($qdata)){
	$jk++;
	if($rowItem["brand_logo"]){
	$imagepath = $DOCUMENT_ROOT.'/shop/item-images/'.$rowItem["brand_logo"];
	if(file_exists($imagepath)){
		$imglink='/imgthumb/30x30-'.$rowItem["brand_logo"];
//		$imglink1='/imgthumb/30x40-'.$rowItem["brand_logo"];
		$src = $DOCUMENT_ROOT.'/shop/item-images/'.$rowItem["brand_logo"];
	}else{
		$imglink='/imgthumb/30x30-'.$rowItem["brand_logo"];
		$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowItem["brand_logo"];
//		$imglink1='/imgthumb/30x40-'.$rowItem["brand_logo"];
	}	
	}else{
		$imglink='/imgthumb/30x30-no-photo.jpg';
		$src = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
	}
		$dest = $DOCUMENT_ROOT.$imglink;
//		$dest1 = $DOCUMENT_ROOT.$imglink1;
	createImgThumbIfnot($src,$dest,'30','30','ratiowh');
?><div class="ds_brandlisting">
<a href="/amp/<?=$rowItem["brand_nice_name"]?>/">
<amp-img src="<?=$imglink?>" width="30" height="30" alt="<?=$rowItem["brand_name"];?>"></amp-img></a>
<a class="ds_brandlist_name" href="/amp/<?=$rowItem["brand_nice_name"]?>/"><?=substr($rowItem["brand_name"],0,15);?></a>
</div>
<? if($jk==8){ $jk=0;?>
 <div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250
    type="doubleclick"
    data-slot="/21630298032/Puppies">
  <div placeholder></div>
  <div fallback></div>
</amp-ad></div>
<? }}?>
</div><div class="pagination">
			<?php           
                      $pageUrl="/amp/brand/page";
           showPages_courses($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl, $url);
                    ?>		</div></div></div>
<?
include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>