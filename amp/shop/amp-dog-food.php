<?
require_once('constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
/*require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');*/
require_once($DOCUMENT_ROOT.'/shop/arrays/brands.php');
$sitesection = "shop";
$ecomm_pagetype = 'category';
//-------------Show
?><!doctype html><html AMP lang="en"><head><meta charset="utf-8"><title>Dog Supplies Store | Online Shop for Dogs & Dog Products| DogSpot.in</title><meta name="author" content="DogSpot" /><meta name="description" content="High quality dog supplies and dog products store in India. Shop online for your dog's daily needs. Easy shopping, hassle-free shipping, pay cash on delivery." /> <meta name="keywords" content="Dog Store, Dog Store online, Best Dog Store, Online Dog store India" /><link rel="canonical" href="https://www.dogspot.in/dog-store/" /><? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?><div class="container-fluid"><div id="topPageStat" class="breadcumb" itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a><span itemprop="position" content="1">&raquo;</span></span><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="#" itemprop="item"><span itemprop="name">Dog Store</span></a><span itemprop="position" content="2">&raquo;</span></span></div><div class="dog_name_wrap" style="padding: 20px 5px 19px 3px;"><div class="dogname_content"><div class="dognameBlock"><div class="dog-nameText"><h1 class="h3">Dog Store</h1></div><div style="text-align:justify; padding-left:10px; padding-right:10px;padding-bottom: 14px;">Find a consolidated list of Dog Products ranging from toys to bowls to everything that falls in between by scrolling below. One of the most extensive collection of pet products in the nation. We at DogSpot aim to serve every pet owner of the country with all the demands their pets pose to them</div></div></div></div>
	<?
	if(is_numeric($show)){
	$maxshow  = 5;
if (empty($show)) {
    $show = 0;
} else {
    $show = $show - 1;
}}else{
	$show = 0;
}
$showRecord   = $show * $maxshow;
$nextShow     = $showRecord + $maxshow;
     $fetchprod=query_execute("SELECT category_id,category_name,category_nicename FROM `shop_category` where category_parent_id = 0 AND category_status = 'active' AND domain_id = '1' AND category_id NOT IN('105','75','174','29','227','173','172','848') ORDER BY `cat_display_order` ASC LIMIT $showRecord,$maxshow"); 
	 $fetchprodall=query_execute("SELECT category_id,category_name,category_nicename FROM `shop_category` where category_parent_id = 0 AND category_status = 'active' AND domain_id = '1' AND category_id NOT IN('105','75','174','29','227','173','172','848') ORDER BY `cat_display_order` ASC "); 
	 $totrecord=mysql_num_rows($fetchprodall);    
	 $iid=0;
     while($productdetail = mysql_fetch_array($fetchprod)){ $iid++;
		$qItem_count = query_execute_row("SELECT count(i.item_id) as ttcout FROM shop_item_category as c, shop_items as i WHERE c.category_id = '".$productdetail['category_id']."' AND i.visibility='visible' AND i.item_display_status!='delete' AND i.stock_status='instock' AND i.item_id=c.item_id AND i.type_id='simple' ORDER BY i.item_display_order ASC LIMIT 2"); 
		if($qItem_count['ttcout']!='0'){
     ?><div><h2 class="catpage_catenamebox" style="display:inline-block;"><a class="catpage_catname" aria-label="<?=$productdetail['category_name']?>" href="/amp/<?=$productdetail['category_nicename']?>/"><?=$productdetail['category_name']?></a></h2><h4 class="catpage_catenamebox" style="display: inline-block;float: right;"><a class="catpage_catname" aria-label="Cat Litter" href="/amp/<?=$productdetail['category_nicename']?>/">View all</a></h4><div class="product-listing-sec"><ul style="width: 102%;margin-left: -3px;border-left: 1px solid #ddd;"><? $qItem = query_execute("SELECT DISTINCT(i.item_id), i.name, i.nice_name, i.price, i.selling_price, i.item_parent_id,i.stock_status,i.item_brand FROM shop_item_category as c, shop_items as i WHERE c.category_id = '".$productdetail['category_id']."' AND i.visibility='visible' AND i.item_display_status!='delete' AND i.stock_status='instock' AND i.item_id=c.item_id AND i.type_id='simple' ORDER BY i.item_display_order ASC LIMIT 2");  
	while($rowItem = mysql_fetch_array($qItem)){
	$item_id=$rowItem["item_id"];
	$name=stripslashes($rowItem["name"]);
	$nice_name=$rowItem["nice_name"];
	$price=$rowItem["price"];
	$selling_price=$rowItem["selling_price"];
	$item_parent_id=$rowItem["item_parent_id"];
	$item_brand=$rowItem["item_brand"];
	$qdataM1=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	$countM=mysql_num_rows($qdataM1);
	if($countM>'0'){
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	}else{
		$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC");
	}
	$rowdatM = mysql_fetch_array($qdataM);
		if($rowdatM["media_file"]){
		$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
		$imageURL='/imgthumb/150x150-'.$rowdatM["media_file"];
	}else{
		$src = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
		$imageURL='/shop/image/no-photo-t.jpg';
	}
	
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'150','150','ratiowh');
	$image_info   = getimagesize($dest);
                    $image_width  = $image_info[0];
                    $image_height = $image_info[1];
		$countO=strlen($ArrayShopBrands[$item_brand]);
					  $dcnt=number_format(100-(($price/$selling_price)*100));
					 // echo strpos(substr($name,0,$countO+1),$ArrayShopBrands[$item_brand]);
					  if(strpos(substr($name,0,$countO+1),$ArrayShopBrands[$item_brand])==0){
					  $nameer=explode($ArrayShopBrands[$item_brand],substr($name,0,40));
					  if($nameer[1]){
					$name1=$nameer[1];
					  $name="<strong>".stripcslashes($ArrayShopBrands[$item_brand])."</strong>".$name1;
					  }
					  } 
	   	?><li><a href="/amp/<?=$nice_name;?>/" aria-label="<?=$name;?>"><? if($selling_price>$price){?><div class="discount-sticker"><?=number_format((($selling_price-$price)/$selling_price)*100); ?>%</div> <? } ?><amp-img src="<?=$imageURL?>" alt="<?=($name);?>" title=" <?=($name);?>" width="<?=$image_width?>" height="<?=$image_height?>"></amp-img><p><?=substr($name,0,40); ?><?	if(strlen($name)>40){echo'...';} ?></p><div class="price-list-tag"><? if($selling_price>$price){?><span class="price-color old "><del>Rs.<?=number_format($selling_price)?></del></span><? }?> <span class="price-color">Rs.<?=number_format($price)?></span> </div>  </a><div> <a href="/amp/<?=$nice_name;?>/" id="complete_order" class="ui-link buy-now-btn" ><div class="buy_now_new buy_cmplt">Add To Cart</div></a> </div></li><? }?></ul></div></div>
		<? } }?></div><div class="pagination">
			 <?php
			// echo "total".$totrecordinstockT;
                         $pageUrl = "/amp/$section[1]";
                    showPages_courses($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl, $url);
                    ?></div>
        <div class="bordered-title"><p>Welcome to the DogSpot Dog Store! This is your one-stop shop for all puppy and dog products. Here we have a huge inventory for you to choose from, and every day we put endless efforts to add more and more relevant and quality matter in this for you to shop. To make a start, how about buying some toys for your little one? Engage him into fun activities by choosing the correct toys for him from our widespread selection of dog toys, and keep him away from boredom. We promise that just having a glance on our product choices, will make you fall in love with us!</p><p>From dog kennels to food, grooming products to toys, bowls to clothes, our listed categories encompass almost everything that your dog may need ever in his lifespan. Just like you, a lot can be said about your dog’s personality by the stuff he uses every day. So pamper him with the array of toys we have got at your disposal! Select some cool ones like rope toys, squeaky toy, Kong toys etc. from our toys section. And while you spoil him, keep him on a bay using our training products like leashes, harnesses, and other training and behavioural aids and so on. We have a range of every day utility items also like bowls and feeders, variety of dog food and treats, regular and fancy grooming products, beds, mattresses and more. Apart from all these, we have got some nice clothes and accessories too for the budding little fashionista in your pooch.</p><p>While procuring all these products and making them available to you, we take special care of three things- utmost quality of the product, comfort of your dog, and ease of handling of these items for you. We take care that whatever goes into your household has to be safe and prove its completecworth. All our products are available in variants which suit dogs of different breeds and sizes, and are improvised as per customer feedback we keep getting every day. Our dog store is a big dog supermarket available on a single click at your web screen!</p></div>
<? include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>