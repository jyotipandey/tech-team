<?php 
include("/constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?><!doctype html>
<html AMP lang="en">
<head><meta charset="utf-8">
<title>Dog Show Result | Dog Event Results</title>
<meta name="keywords" content=" Dog Show Result, Dog Event Results" />
<meta name="description" content="Get the result of every Dog Show listed here with event photos and other details" />
<link rel="canonical" href="https://www.dogspot.in/show-results/" />
<?php require_once($DOCUMENT_ROOT .'/new/common/top-2-amp.php'); ?><div class="dog-show-sec"><div class="dog-show-banner" style="text-align: center;"> <amp-img src="/dog-events/images/show-schedules.jpg" height="85" width="360"></amp-img></div><div id="showsechedule_text"><h1>Dog Shows Results</h1></div><div class="dogshow_nav_header" id="dogshow_nav_header"><div id="wrapper" class="clearfix"><div id="ds_top_nav"><ul id="nav"><li><a href="/amp/dog-events/about-us/" >About us</a> </li><li style="margin-left: -3%;"><a href="/amp/dog-events/show-schedules/">Show schedules</a></li><li style="margin-left: -3%;margin-right: -24%;"><a href="/amp/show-results/" >Show results</a></li></ul></div></div></div><div>
     <div  class="dog-show-sec dog-result-sec" id="PhotoLoadDiv">
      <ul>
        <?
$selectShow = query_execute("SELECT * FROM show_description  ORDER BY date DESC");
 if(!$selectShow){	die(mysql_error());	}
	
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$album_nicename='';$ring_id='';
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT * FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 if($show_nicename!='122-123-sikc-breed-championship-dog-show'){
	 
	 if($ring_id || $album_nicename){ if($show_nicename){
?><a href="/amp/<?=$show_nicename?>/" title="<?=$show_name?> Dog Show Results"><li><h2><?=$show_name?></h2>
<? if($album_nicename){?>Photos|<? } if($ring_id ){?> Results |<? } print(showdate($date, "d M Y")); ?></li><? }}}?></a><? }?></ul>
    </div></div> </div><?php require_once($DOCUMENT_ROOT .'/new/common/bottom-amp.php'); ?>
</body></html>
