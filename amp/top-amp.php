  <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1,initial-scale=1">

    <!--*
        *   FavIcons
        **-->
    <meta name="mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="#1e1e1e">
	<link rel="apple-touch-icon" sizes="180x180" href="amp/assets/img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="amp/assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="amp/assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="mask-icon" href="amp/assets/img/favicons/safari-pinned-tab.svg" color="#212121">
	<link rel="manifest" href="/ds_push/new/manifest.json">

	<!-- Allow web app to be run in full-screen mode. -->
	<meta name="apple-mobile-web-app-capable"
	      content="yes">

	<!-- Configure the status bar. -->
	<meta name="apple-mobile-web-app-status-bar-style"
	      content="black">

	<!-- iPad retina portrait startup image -->
	<link href="amp/assets/img/splashScreens/apple-touch-startup-image-1536x2008.png"
	      media="(device-width: 768px) and (device-height: 1024px)
	                 and (-webkit-device-pixel-ratio: 2)
	                 and (orientation: portrait)"
	      rel="apple-touch-startup-image">

	<!-- iPhone 6 Plus portrait startup image -->
	<link href="amp/assets/img/splashScreens/apple-touch-startup-image-1242x2148.png"
	      media="(device-width: 414px) and (device-height: 736px)
	                 and (-webkit-device-pixel-ratio: 3)
	                 and (orientation: portrait)"
	      rel="apple-touch-startup-image">

	<!-- iPhone 6 startup image -->
	<link href="amp/assets/img/splashScreens/apple-touch-startup-image-750x1294.png"
	      media="(device-width: 375px) and (device-height: 667px)
	                 and (-webkit-device-pixel-ratio: 2)"
	      rel="apple-touch-startup-image">

	<!-- iPhone 5 startup image -->
	<link href="amp/assets/img/splashScreens/apple-touch-startup-image-640x1096.png"
	      media="(device-width: 320px) and (device-height: 568px)
	                 and (-webkit-device-pixel-ratio: 2)"
	      rel="apple-touch-startup-image">

    <!--*
        *   Fonts
        **-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700|Roboto:300,400">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!--*
        *   JavaScripts to Include
        **-->
        <script async custom-element="amp-install-serviceworker" src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js"></script>
        
        <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
        
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        
        <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
        
        <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
        
        <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
      
        
 <script async src="https://cdn.ampproject.org/v0.js"></script>
 <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
 <script async custom-element=amp-social-share src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
 <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
 <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
<?php /*?> <script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js"></script><?php */?>
 

    <!--*
        *   Structured Data
        **-->
    

    <!--*
        *   Required CSS Code (AMP Boilerplate)
        **-->
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <!--*
        *   Custom CSS Code
        **-->
    <style amp-custom>
        /* GLOBAL STYLES */
		/**---------------------
		  *
		  * Resets
		  *
		  *---------------------**/
		figure{margin: 0}
		fieldset{border:none;padding:0;margin: 0}
		*{box-sizing: border-box}
		button{background: none; border: none}
		a{text-decoration: none}
		:focus{outline: 0;}
		ul{padding-left: 20px}

		/**---------------------
		  *
		  * Global Styles
		  *
		  *---------------------**/
		html{font-size: 62.5%; box-sizing: border-box;}
		body{font-size: 1.3rem;  -webkit-font-smoothing: antialiased; color: #3e3e3e;    font-family: lato,sans-serif;}

		.font-1, html{font-family: lato, sans-serif; font-weight: 300}
		.font-2{font-family: lato, sans-serif ;}

		.text-center{text-align: center}
		.margin-0{margin: 0}
		.margin-top-0{margin-top: 0}
		.margin-bottom-0{margin-bottom: 0}
		.margin-left-0{margin-left: 0}
		.margin-right-0{margin-right: 0}
		.padding-left-0{padding-left: 0}
		.padding-right-0{padding-right: 0}
		.minus-margin-top-bottom-15{margin-top: -15px; margin-bottom: -15px}

		[class*="col-"].margin-bottom-0{margin-bottom: 0}

		.margin-top-80{margin-top: 80px}
		.margin-top-50{margin-top: 50px}
		.margin-top-minus-30{margin-top: -30px}

		.space{height: 10px;}
		.space-2{height: 20px;}
		.space-3{height: 30px;}

		.divider{margin: 13px 0;}
		.divider-30{margin: 30px 0;}
		.divider.colored{height: 1px; background: rgba(0,0,0,.12)}
		.divider-30.colored{height: 1px; background: rgba(0,0,0,.12)}

		.width-100{width: 100%}
		.width-90{width: 90%}
		.width-80{width: 80%}
		.width-70{width: 70%}

		.pull-left{float: left}
		.pull-right{float: right}

		.d-block{display: block}
		.d-inline-block{display: inline-block}
		.d-inline{display: inline}

		.clear-both{clear:both}

		.clearfix:after,
		.clearfix:before {
			display: table;
			content: "";
			line-height: 0
		} .clearfix:after {clear: both}

		h2{margin-bottom: 7.5px}
		p{margin: 7.5px 0 0;}
		small{font-size: 1rem; line-height: 1}
		strong,b{font-weight: 700}

		h1,h2,h3,h4,h5,h6,
		.h1,.h2,.h3,.h4,.h5,.h6{
			font-weight: 400;
			color: #212121;
			font-family: lato, sans-serif ;
		}

		.thin{font-weight: 300}
		.thicc{font-weight: 700}

		h1{font-size: 2.7rem}
		h2{font-size: 2.1rem}
		h3{font-size: 1.7rem}
		h4{font-size: 1.4rem}
		h5{font-size: 1.2rem}
		h6{font-size: 1rem}

		.h1{font-size: 2.7rem}
		.h2{font-size: 2.1rem}
		.h3{font-size: 1.7rem}
		.h4{font-size: 1.4rem}
		.h5{font-size: 1.2rem}
		.h6{font-size: 1rem}

		a,
		.primary-color{color: #6c9d06}
		.secondary-color{color: #e7a900}
		.light-color{color: #FFF}
		.light-color-2{color: rgba(255,255,255,.54)}
		.dark-color{color: #333030;}
		.ocean-color{color: #2b90d9;}
		.grass-color{color: #3ac569;}
		.salmon-color{color: #ff7473;}
		.sun-color{color: #feee7d;}
		.alge-color{color: #79a8a9;}
		.flower-color{color: #353866;}
		.grey-color{color: #c4c4c4;}

		.primary-bg{background: #6c9d06}
		.secondary-bg{background: #e7a900}
		.light-bg{background: #fff;}
		.dark-bg{background: #333030;}
		.ocean-bg{background: #2b90d9;}
		.grass-bg{background: #3ac569;}
		.salmon-bg{background: #ff7473;}
		.sun-bg{background: #feee7d;}
		.alge-bg{background: #79a8a9;}
		.flower-bg{background: #353866;}
		.grey-bg{background: #c4c4c4;}

		.circle{border-radius: 50%}

		[dir="rtl"] .pull-left{float: right}
		[dir="rtl"] .pull-right{float: left}
		body {text-align: left}
		body[dir="rtl"] {text-align: right}

		.text-center{text-align: center}

		code {
			padding: .2rem .4rem;
			font-size: 90%;
			color: #bd4147;
			background-color: #f7f7f9;
			border-radius: .25rem;
		}

		h2:only-child{margin: 0}

		/**---------------------
		  *
		  * Header Styles
		  *
		  *---------------------**/
		header{
			position: relative;
			min-height: 51px;
			padding: 0 5px;
			background: #6c9d06;
		}

		header .fa{
			color: #FFF;
			font-size: 17px;
			line-height: 52px;
			height: 51px;
			padding: 0 0px 0px 10px;
			margin: 0;
		}

		header a:last-child{
			position: relative;
		}

		header a:last-child span{
			position: absolute;
			top: 3px;
			right: 9px;
			font-size: 13px;
			color: #FFF;
			font-weight: 400;
		}
        .cms-container{ padding:0px 15px 0px 0px; }
		.toast {
			display: block;
			position: relative;
			height: 51px;
			padding-left: 15px;
			padding-right: 15px;
			width: 49px;
		}
        .am-search-box{    padding: 0px 15px 10px;
    width: 100%;
    background: #6c9d06;
    box-sizing: border-box;}
	.am-search-box input{ width: 84%;
    padding: 0px 10px;
    border: 0px;
    line-height: 31px;
    height: 35px;}
	.am-search-box button{background: #fff;
    width: 15%;
    box-sizing: border-box;
    margin-left: -4px;}
	
	.am-search-box button .fa{     line-height: 31px;
    height: 34px;
    color: #6c9d06;
    padding: 0px;
    margin-top: -1px}
		.toast:after,
		.toast:before,
		.toast span {
			position: absolute;
			display: block;
			width: 19px;
			height: 3px;
			border-radius: 2px;
			background-color: #FFF;
			-webkit-transform: translate3d(0,0,0) rotate(0deg);
			transform: translate3d(0,0,0) rotate(0deg);
		}

		.toast:after,
		.toast:before {
			content: '';
			width: 16px;
			left: 15px;
			-webkit-transition: all ease-in .4s;
			transition: all ease-in .4s;
		}

		.toast span {
			opacity: 1;
			top: 24px;
			-webkit-transition: all ease-in-out .4s;
			transition: all ease-in-out .4s;
		}

		.toast:before {
			top: 17px;
		}

		.toast:after {
			top: 31px;
		}

		#logo{
			display: block;
            line-height: 1;

			position: absolute;
			left: 100px;
			top: 28px;

			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
		}

		#logo img{
			display: block;
		}

		/**---------------------
		  *
		  * Sidebar Styles
		  *
		  *---------------------**/
		  .home-nav .fa, .home-nav a {color: #fff; }
		  .home-nav .fa, .home-nav a i{padding: 0px 10px 0px 0px;}
		#mainSideBar{
			min-width: 300px;
			padding-bottom: 30px;
		}

		#mainSideBar > div:not(.divider){
			padding: 17px 20px;
		}

		#mainSideBar figure{
			width: 300px;
			max-width: 100%;
			padding: 20px;
			position: relative;
		}

		#mainSideBar button{
			position: absolute;
			right: 20px;
			top: 20px;
		}

		#mainSideBar amp-img{
			margin-bottom: 5px;
		}

		#mainSideBar h3,
		#mainSideBar h5{
			margin: 0;
			line-height: 1.5;
		}

		#menu{margin-top: 15px}
		#menu div{padding: 0}

		#menu h6,
		#menu a{
			color: inherit;
			font-size: 1.3rem;
			font-weight: 300;
			padding:0;
			border: none;
		}

		#menu a,
		#menu span{
			padding: 14px 20px 14px 53px;
			display: block;
			color: inherit;
			position: relative;

			-webkit-transition: all ease-in-out .2s;
			transition: all ease-in-out .2s;
		}

		#menu section[expanded] > h6 span{
			background-color: rgba(0,0,0,.06);
			color: #f82e56;
		}

		#menu h6 span:after{
			position: absolute;
			right: 20px;
			top: 0;
			font-family: 'FontAwesome';
			font-size: 12px;
			line-height: 47px;
			content: '\f0dd';
		}

		#menu i,
		#mainSideBar li i{
			font-size: 1.7rem;
			position: absolute;
			left: 20px;
		}

		.social-ball{
			font-size: 1.6rem;
			display: inline-block;
			text-align: center;
			line-height: 30px;
			height: 30px;
			width: 30px;
			border-radius: 50%;
			color: #FFF;
			margin-right: 5px;
		}

		.sample-icons [class*="shopping-"]{
			font-size: 2.7rem;
		}

		.sample-icons .class-name{
			font-size: 1.1rem;
		}

		.sample-icons .col-xs-3:nth-child(4n + 1){
			clear: left
		}

		.social-ball.fa-facebook{background-color: #4867AA}
		.social-ball.fa-twitter{background-color: #00ACED}
		.social-ball.fa-linkedin{background-color: #0177B5}
		.social-ball.fa-behance{background-color: #010103}
		.social-ball.fa-dribbble{background-color: #E04C86}

		/**---------------------
		  *
		  * Badges
		  *
		  *---------------------**/
		.badge{
			font-weight: 400;
			font-size: 1.2rem;
			color: #FFF;
			line-height: 1.8rem;
			height: 1.8rem;
			padding: 0 5px;
			display: inline-block;
		}


		/**---------------------
		  *
		  * Background Row
		  *
		  *---------------------**/
		.background-row{
			background-image: url('https://img.mobius.studio/themes/bones/LTR/assets/img/row_bg_375x250.jpg');
			background-size: cover;
			background-position: center center;
			padding: 60px 0;
		}

		.colored-row{
			background-color: #f4f2f3
		}

		.inside-out-long-img{
			margin-right: 25px;
		}

		.inside-out-long-img amp-img{
			margin-bottom: -90px;
            margin-top: 10px;
		}

		.background-row-content{
			background-color: rgba(255,255,255,.87);
			display: inline-block;
			outline: 1px solid rgba(255,255,255,.87);
		    outline-offset: 3px;
            padding: 25px;
		}

		.background-row-content .row-1,
		.background-row-content .row-2,
		.background-row-content .row-3{
			margin: 0;
			font-weight: 600;
            line-height: 1;
		}

		.background-row-content .row-1{
			font-size: 17px;
		}

		.background-row-content .row-2{
			font-size: 25px;
			margin: 20px 0;
		}

		.background-row-content .row-3{
			font-size: 12px;
			font-weight: 400
		}


		/**---------------------
		  *
		  * Shopping Line Icons
		  *
		  *---------------------**/
		@font-face{font-family:shopping;src:url(assets/fonts/shopping.eot);src:url(assets/fonts/shopping.eot?#iefix) format("embedded-opentype"),url(assets/fonts/shopping.woff) format("woff"),url(assets/fonts/shopping.ttf) format("truetype"),url(assets/fonts/shopping.svg#shopping) format("svg");font-weight:400;font-style:normal}@media screen and (-webkit-min-device-pixel-ratio:0){@font-face{font-family:shopping;src:url(assets/fonts/shopping.svg#shopping) format("svg")}}[class*=" shopping-"]:before,[class^=shopping-]:before{font-family:shopping;font-size:inherit;font-style:normal}.shopping-alcohol-bottle:before{content:"\f100"}.shopping-ascending-line-chart:before{content:"\f101"}.shopping-atm-machine:before{content:"\f102"}.shopping-bag-for-woman:before{content:"\f103"}.shopping-baseball-cap:before{content:"\f104"}.shopping-big-barcode:before{content:"\f105"}.shopping-big-cargo-truck:before{content:"\f106"}.shopping-big-diamond:before{content:"\f107"}.shopping-big-diamond-1:before{content:"\f108"}.shopping-big-gift-box:before{content:"\f109"}.shopping-big-glasses:before{content:"\f10a"}.shopping-big-license:before{content:"\f10b"}.shopping-big-paper-bag:before{content:"\f10c"}.shopping-big-piggy-bank:before{content:"\f10d"}.shopping-big-search-len:before{content:"\f10e"}.shopping-big-shopping-trolley:before{content:"\f10f"}.shopping-big-television-with-two-big-speakers:before{content:"\f110"}.shopping-big-wall-clock:before{content:"\f111"}.shopping-blank-t-shirt:before{content:"\f112"}.shopping-bra:before{content:"\f113"}.shopping-calculator-symbols:before{content:"\f114"}.shopping-car-facin-left:before{content:"\f115"}.shopping-cash-bill:before{content:"\f116"}.shopping-cash-machine:before{content:"\f117"}.shopping-cash-machine-open:before{content:"\f118"}.shopping-check-machine:before{content:"\f119"}.shopping-checklist-and-pencil:before{content:"\f11a"}.shopping-cloakroom:before{content:"\f11b"}.shopping-closed-package:before{content:"\f11c"}.shopping-consultation:before{content:"\f11d"}.shopping-credential:before{content:"\f11e"}.shopping-cut-credit-card:before{content:"\f11f"}.shopping-delivery-trollery:before{content:"\f120"}.shopping-delivery-truck-facing-right:before{content:"\f121"}.shopping-disco-light:before{content:"\f122"}.shopping-discount-badge:before{content:"\f123"}.shopping-dollar-bag:before{content:"\f124"}.shopping-dollar-bill-from-automated-teller-machine:before{content:"\f125"}.shopping-electric-scalator:before{content:"\f126"}.shopping-fork-plate-and-knife:before{content:"\f127"}.shopping-gold-stack:before{content:"\f128"}.shopping-hambuger-with-soda:before{content:"\f129"}.shopping-hanger-with-towel:before{content:"\f12a"}.shopping-heart-with-line:before{content:"\f12b"}.shopping-high-hills-shoe:before{content:"\f12c"}.shopping-horn-with-handle:before{content:"\f12d"}.shopping-inclined-comb:before{content:"\f12e"}.shopping-inclined-open-umbrella:before{content:"\f12f"}.shopping-ladies-purse:before{content:"\f130"}.shopping-lipstick-with-cover:before{content:"\f131"}.shopping-long-dress:before{content:"\f132"}.shopping-long-pants:before{content:"\f133"}.shopping-manager-profile:before{content:"\f134"}.shopping-mannequin-with-necklace:before{content:"\f135"}.shopping-map-with-placeholder-on-top:before{content:"\f136"}.shopping-market-shop:before{content:"\f137"}.shopping-megaphone-facing-right:before{content:"\f138"}.shopping-men-shorts:before{content:"\f139"}.shopping-necklace:before{content:"\f13a"}.shopping-one-shoe:before{content:"\f13b"}.shopping-open-box:before{content:"\f13c"}.shopping-open-sign:before{content:"\f13d"}.shopping-parking-sign:before{content:"\f13e"}.shopping-perfume-bottle-with-cover:before{content:"\f13f"}.shopping-phone-ringing:before{content:"\f140"}.shopping-purse-with-bills:before{content:"\f141"}.shopping-purse-with-bills-1:before{content:"\f142"}.shopping-recepcionist:before{content:"\f143"}.shopping-recorder-machine:before{content:"\f144"}.shopping-road-banner:before{content:"\f145"}.shopping-round-bag:before{content:"\f146"}.shopping-round-bracelet:before{content:"\f147"}.shopping-sale-label:before{content:"\f148"}.shopping-security-cam:before{content:"\f149"}.shopping-shirt-and-tie:before{content:"\f14a"}.shopping-shopping-on-computer:before{content:"\f14b"}.shopping-shopping-on-smarphone:before{content:"\f14c"}.shopping-shopping-on-tablet:before{content:"\f14d"}.shopping-shopping-paper-bag:before{content:"\f14e"}.shopping-shopping-trolley-full:before{content:"\f14f"}.shopping-short-bag:before{content:"\f150"}.shopping-short-skirt:before{content:"\f151"}.shopping-sign-check:before{content:"\f152"}.shopping-small-sofa:before{content:"\f153"}.shopping-store-purchase:before{content:"\f154"}.shopping-supermarket-basket:before{content:"\f155"}.shopping-three-alcoholic-bottles:before{content:"\f156"}.shopping-three-coins:before{content:"\f157"}.shopping-three-ties:before{content:"\f158"}.shopping-two-boots:before{content:"\f159"}.shopping-two-credit-cards:before{content:"\f15a"}.shopping-two-earrings:before{content:"\f15b"}.shopping-two-hangers:before{content:"\f15c"}.shopping-two-rings:before{content:"\f15d"}.shopping-two-socks:before{content:"\f15e"}.shopping-wc-sign:before{content:"\f15f"}.shopping-wide-calculator:before{content:"\f160"}.shopping-women-hat:before{content:"\f161"}.shopping-women-shirt-with-sun:before{content:"\f162"}.shopping-women-swimsuit:before{content:"\f163"}


		/**---------------------
		  *
		  * Products Grid
		  *
		  *---------------------**/
		.bones-products-grid{
			margin: -7.5px;
			position: relative;
		}

		.bones-products-grid:after{
			content: '';
			display: table;
			clear: both;
		}

		.bones-products-grid > div{
			float: left;
			padding: 7.5px;
		}

		.bones-products-grid.cols-2 > div{width: 50%;}
		.bones-products-grid.cols-3 > div{width: 33.333333333333333%;}
		.bones-products-grid.cols-4 > div{width: 25%;}

		@media all and (min-width: 376px) {
			.bones-products-grid.cols-2 > div{width: 33.333333333333333%;}
			.bones-products-grid.cols-3 > div{width: 25%;}
			.bones-products-grid.cols-4 > div{width: 20%;}
		}

		@media all and (min-width: 768px) {
			.bones-products-grid.cols-2 > div{width: 25%;}
			.bones-products-grid.cols-3 > div{width: 20%;}
			.bones-products-grid.cols-4 > div{width: 16.666666666666667%;}
		}

		/**---------------------
		  *
		  * Grid
		  *
		  *---------------------**/
		[class*="col-"]{margin-bottom: 25px;}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{margin-right:-15px;margin-left:-15px}.row:after,.row:before{display:table;content:" "}.row:after{clear:both}.container-full,.container-full [class*="col-"]{padding-left: 0; padding-right: 0;}.container-full .row{margin-left:0; margin-right:0;}.no-gap [class*="col-"]{padding-right: 0;padding-left: 0;margin-bottom: 0;}.no-gap.row{margin-right: 0; margin-left: 0;}.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{float:left}.col-xs-12{width:100%}.col-xs-11{width:91.66666667%}.col-xs-10{width:83.33333333%}.col-xs-9{width:75%}.col-xs-8{width:66.66666667%}.col-xs-7{width:58.33333333%}.col-xs-6{width:50%}.col-xs-5{width:41.66666667%}.col-xs-4{width:33.33333333%}.col-xs-3{width:25%}.col-xs-2{width:16.66666667%}.col-xs-1{width:8.33333333%}@media (min-width:768px){.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-11{width:91.66666667%}.col-sm-10{width:83.33333333%}.col-sm-9{width:75%}.col-sm-8{width:66.66666667%}.col-sm-7{width:58.33333333%}.col-sm-6{width:50%}.col-sm-5{width:41.66666667%}.col-sm-4{width:33.33333333%}.col-sm-3{width:25%}.col-sm-2{width:16.66666667%}.col-sm-1{width:8.33333333%}}



		/**---------------------
		  *
		  * AMP Lightbox
		  *
		  *---------------------**/
		amp-lightbox{background-color: rgba(0,0,0,.87)}
		amp-lightbox.light{background-color: rgba(255,255,255,.87)}

		amp-lightbox > .lightbox{
			position: absolute;
			bottom: 0;
			top: 0;
			right: 0;
			left: 0;
		}

		amp-lightbox .middle{
			width: 80%;
			position: absolute;
			top: 50%;
			left: 50%;

			-webkit-transform: translate(-50%, -50%);
			-moz-transform: translate(-50%, -50%);
			-ms-transform: translate(-50%, -50%);
			-o-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
		}

		amp-lightbox .message{
			text-align: center;
		}

		amp-lightbox h1,
		amp-lightbox small{
			color: #FFF
		}

		amp-lightbox.light h1,
		amp-lightbox.light small{
			color: #212121
		}

		amp-lightbox small{
			font-size: 1.2rem;
		}

        /* /GLOBAL STYLES */

        /* INDEX PAGE STYLES */

        /**---------------------
		  *
		  * Title
		  *
		  *---------------------**/
		.bordered-title{
			border-bottom: 1px solid rgba(0,0,0,.12);
			margin: 0 -15px;
			padding: 0 15px 8px;
		}

		.bordered-title h3,
		.bordered-title .h3{
			margin: 0;
			line-height: 1.4;
		}

		.bordered-title h5,
		.bordered-title .h5{
			opacity: .54;
			margin: 0;
		}

        /**---------------------
          *
          * Icon Info Box 2
          *
          *---------------------**/
		.icon-info-box-2 .icon-wrap{
			width: 40px;
			height: 40px;
			border: 1px solid #f82e56;
			float: left;
            margin-top: 3px;
		}

		.icon-info-box-2 i{
			font-size: 26px;
			color: #f82e56;
			background-color: #fff;
			width: 40px;
			height: 40px;
			line-height: 40px;
			text-align: center;
		    display: block;
		    position: relative;
		    right: -3px;
		    top: -5px;
		}

		.icon-info-box-2 .contents{
			margin-left: 65px;
		}

		.icon-info-box-2 h4,
		.icon-info-box-2 h5{margin: 0;}

		.icon-info-box-2 h5{
			opacity: .24;
		}


        /**---------------------
		*
		* Product List Item
		*
		*---------------------**/
		.bones-product-list-item,
		.bones-product-list-item .preview{
			position: relative;
			display: block;
		}

		.bones-product-list-item .preview amp-img{
			position: relative;
			z-index: 1;
		}

		.bones-product-list-item .preview .badge{
			position: absolute;
			z-index: 2;
			top: 5px;
			left: 5px
		}

		.bones-product-list-item .preview .badge:last-child{
			left: auto;
			right: 5px;
		}

		.bones-product-list-item .categories{padding: 7px 0 0;}

		.bones-product-list-item .categories a{
			color: rgba(41,37,42,.54);
			font-size: 1.1rem;
			font-weight: 400;
		}

		.bones-product-list-item .categories a:not(:last-child):after{
			content: ', ';
			display: inline;
		}

		.bones-product-list-item > a h2{
			font-size: 1.2rem;
            margin: 0;
		}

		.bones-product-list-item .prices .old{
			font-size: 1rem;
			font-weight: 700;
			vertical-align: bottom;
			opacity: .24;
			text-decoration: line-through;
		}

		.bones-product-list-item .prices .current{
			font-size: 1.4rem;
			font-weight: 700;
			vertical-align: bottom;
		}

		.bones-product-list-item.sold-out:after{
			content: '';
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background-color: rgba(255,255,255,.54);

			z-index:5;
		}



        /**---------------------
          *
          * Grid Gallery with Lightbox
          *
          *---------------------**/
		.lightbox-item-with-caption figcaption{display: none;}
		.lightbox-item-with-caption amp-img{display: block;}
		.amp-image-lightbox-caption{padding: 15px}

        /**---------------------
		*
		* Horizontal Product List Item
		*
		*---------------------**/
		.bones-products-grid.cols-2 > div.bones-h-product-list-item{
			width: 100%;
			position: relative;
		}


		.bones-products-grid.cols-2 > div.bones-h-product-list-item:not(:last-child){
			margin-bottom: 30px;
		}

		.bones-h-product-list-item .preview{
			width: 28.99%;
		    display: block;
		    float: left;
            margin-right: 15px;
			background-color: #f4f2f3;
			border-radius: 0 100% 0 0;
		}

		.bones-h-product-list-item .preview amp-img{
			position: relative;
			z-index: 1;
			margin: auto;
			width: 63%;
			height: auto;
		}

		.bones-h-product-list-item .preview .badge{
			position: absolute;
			z-index: 2;
			bottom: 15px;
			right: 5px
		}

		.bones-h-product-list-item .preview .badge:last-child{
			left: auto;
			right: 5px;
			bottom: auto;
			top: 5px;
		}

		.bones-h-product-list-item .categories a{
			color: rgba(41,37,42,.54);
			font-size: 1.1rem;
			font-weight: 400;
		}

		.bones-h-product-list-item .categories a:not(:last-child):after{
			content: ', ';
			display: inline;
		}

		.bones-h-product-list-item > a h2{
			font-size: 1.3rem;
            margin: 0;
		}

		.bones-h-product-list-item .prices .old{
			font-size: 1rem;
			font-weight: 700;
			vertical-align: bottom;
			opacity: .24;
			text-decoration: line-through;
		}

		.bones-h-product-list-item .stars{
			display: block;
			margin: 5px 0 0;
			line-height: 1;
		}

		.bones-h-product-list-item .prices .current{
			font-size: 1.4rem;
			font-weight: 700;
			vertical-align: bottom;
		}

		.bones-h-product-list-item.sold-out:after{
			content: '';
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background-color: rgba(255,255,255,.54);

			z-index:5;
		}



        /**---------------------
		  *
		  * Comment Items
		  *
		  *---------------------**/
		.button-row .button{
			float: left;
		}

		.button{
			overflow: hidden;

			font-family: lato, sans-serif ;
			background-color: #f82e56;
			color: #FFF;

			margin: 10px;
			padding: 10px 20px;

			cursor: pointer;
			user-select: none;
			transition: all 60ms ease-in-out;
			text-align: center;
			white-space: nowrap;
			text-decoration: none;
			text-transform: capitalize;

			border: 0 none;
			border-radius: 0;

			font-size: 1.3rem;
			font-weight: 500;
			line-height: 1.3;

			-webkit-appearance: none;
			-moz-appearance:    none;
			appearance:         none;
		}

		.button-bordered{
			background-color: transparent;
			border: 2px solid #f82e56;
			color: #1a1a1a;
		}

		.button-full{width:100%; margin-left: 0; margin-right: 0; display: block}

		.button.primary-bg{background-color: #f82e56}
		.button.secondary-bg{background-color: #e7a900}
		.button.light-bg{background-color: #fff;}
		.button.dark-bg{background-color: #333030;}
		.button.ocean-bg{background-color: #2b90d9;}
		.button.grass-bg{background-color: #3ac569;}
		.button.salmon-bg{background-color: #ff7473;}
		.button.sun-bg{background-color: #feee7d;}
		.button.alge-bg{background-color: #79a8a9;}
		.button.flower-bg{background-color: #353866;}
		.button.grey-bg{background-color: #c4c4c4;}

		.button.margin-left-0{margin-left:0}

		.button-large{
			padding: 10px 20px;
			font-size: 1.7rem;
		}

		.button-small{
			padding: 7px 13px;
			font-size: 1.1rem;
		}

		.button .icon-at-right{
			margin-left: 10px;
			font-size: 1.4rem;
		}

		.clean-btn{
			padding: 0;
		}

        /**---------------------
	  *
	  * Form Inputs Styles
	  *
	  *---------------------**/
	.input{
		border: none;
		border-bottom: 1px solid rgba(0,0,0,.12);
		background: transparent;
		outline: none;
		display: block;
		width: 100%;
		line-height: 30px;
		height: 35px;
		margin-bottom: 20px;
		font-family: inherit;
	}

	textarea.input{
		height: auto;
		min-height: 35px;
	}

	.single-line-form *:first-child{
		width:70%;
	}

	.single-line-form *:last-child{
		width: 30%;
	    margin: 0;
	    height: 50px;
	    line-height: 50px;
	    padding: 0;
	}

	select.modern-select{
		height: 35px;
		border: none;
		border-bottom: 1px solid rgba(0,0,0,.12);
	}

        /**---------------------
		  *
		  * Alert And Notification Boxes
		  *
		  *---------------------**/
		.alert-box{
			padding: 15px 20px;
		}

		.alert-box p{
			display: inline-block;
			margin: 0 0 0 5px;
            font-weight: 600;
		}

		.alert-box i{
			border: 2px solid;
		    border-radius: 50%;
		    width: 30px;
		    height: 30px;
		    line-height: 28px;
		    padding: 0;
		    text-align: center;
		}

		.alert-box-success{
			background-color: #BFF9D0;
			color: #299c77;
		}

		.alert-box-success i{border-color: #299c77}

		.alert-box-info{
			background-color: #c3ebff;
			color: #65a6c7;
		}

		.alert-box-info i{border-color: #65a6c7}

		.alert-box-error{
			background-color: #ffd0d0;
			color: #d45757;
		}

		.alert-box-error i{border-color: #d45757}

		.alert-box-warning{
			background-color: #fff4b8;
			color: #e6ae15;
		}

		.alert-box-warning i{border-color: #e6ae15}

        

        /* /INDEX PAGE STYLES */
		/* category slider */
		
		.cat-section .cat-gallery, .blogger_sec .bloggers, .hot_selling_sec .hot_selling_carousel{
    overflow: scroll;
    overflow-y: hidden;
    white-space: nowrap;
    width: 100%;
 
  
}
.cat-section .cat-gallery ul, .bloggers ul, .hot_selling_carousel ul{ list-style:none; padding:25px 0px 0px; margin:0px;}
.cat-section .cat-gallery ul li:first-child, .blogger_sec .bloggers ul li:first-child{ margin:0px; padding:0px;}
.cat-section .cat-gallery ul li,  .blogger_sec .bloggers ul li, .hot_selling_carousel ul li {
    position: relative;
    display: inline-block;
    align-self: flex-start;
    border-radius: 0;
    background: #fff;
    cursor: pointer;
    margin-left: 10px;
}


.cat-section .cat-gallery a {
    font-size: 12.3px;
    font-weight: 700;
    letter-spacing: 0;
    color: #555;
    text-decoration: none;
}
.cat-section .cat-gallery div {
    margin-top: 2px;
	text-align:center;
}


.blogger_sec .bloggers ul li {
    width: 130px;
    text-align: center;
}
	.cat-section .cat-gallery::-webkit-scrollbar, .blogger_sec .bloggers::-webkit-scrollbar, .hot_selling_carousel::-webkit-scrollbar {
display: none;
}	

.breed-selection{padding: 0px 0px 15px; }
.blogger_sec h4 {
    font-size: 18px;
    padding: 0px 0px 0px 15px;
    color: #333;
	margin:0px;
	font-weight: 700;
}

.blogger_sec h4 span{ color: #6c9d06;}
.blogger_sec .bloggers figcaption {
    padding-top: 10px;
    font-weight: 600;
	font-size:14px;
}

.hot_selling_sec h1{font-size: 18px;
    padding: 0px 0px 0px 15px;
    color: #333;
    margin: 0px;
    font-weight: 700;}
	.product-price-s {
    padding-bottom: 10px;
    text-align: left;
}
.hot_selling_sec .product-slider-link {
    padding: 10px 0;
    text-align: left;
    line-height: 24px;
    overflow: hidden;
    max-width: 173px;
}
.hot_selling_sec .product-slider-link a {
    font-size: 14px;
    color: #444;
    letter-spacing: .6px;
}
.hot_selling_sec .p-slide-img{ text-align:center;}
.hot_selling_sec .product-slider-link {
    text-overflow: ellipsis;
    white-space: nowrap;
}
.p-ds-off {
    color: #444;
    margin-right: 13px;
}
.product-price-s .p-d-price {
    font-size: 14px;
    color: #999;
    margin-right: 13px;
}
.product-price-s .p-d-off {
    color: #ff7a22;
}
.amp-slider-col{ margin: 0px 0px 15px;}
.article-det-sec .petstories-h-sec h2{ width: 100%;
    font-size: 16px;
    letter-spacing: .6px;
    margin-top: 15px;}
 .article-det-sec h3 {
    font-size: 14px;
    font-weight: 700;
    margin: 10px 0 0;
    float: left;
    width: 100%;
}	

.article-det-sec p {
    font-size: 14px;
    color: #888;
    margin-top: 5px;
    float: left;
    width: 100%;
}
ul.dsdropdown li{display:block;padding:10px;border-bottom:1px solid #ddd;float:left;width:100%;}
ul.dsdropdown li:first-child{font-size:16px;background:#fff;}
ul.dsdropdown li:last-child{font-size:16px;border:0;}
ul.dsdropdown li:hover{background:#f9f9f9;}
.nf-img{float:left;width:20%;}
.nf-text{float:left;width:80%;margin-top:-10px;}
.nf-text p{margin: -16.5px 0 0;}
.nf-img img{max-width:50px;width:50px;height:auto;}
.nf-text h4{font-size:14px;padding-bottom:1px;color:#333;}
.nf-text p{color:#888;font-size:13px;}
.nf-active-bg{background:#f2f2f2;}
   </style>
</head>
<body dir="ltr">
    <amp-auto-ads type="adsense"
              data-ad-client="ca-pub-3238649592700932">
</amp-auto-ads>
<amp-analytics type="googleanalytics">
<script type="application/json">{"vars":{"account":"UA-1552614-5"},"triggers":{"trackPageview":{"on":"visible","request": "pageview"}}}    </script>
</amp-analytics>
<amp-analytics type="googleanalytics">
<script type="application/json">{"vars":{"account":"UA-1552614-17"},"triggers":{"trackPageview":{"on":"visible","request": "pageview"}}}    </script>
</amp-analytics>

<?	$Ndate=date('Y-m-d H:i:s');
$my_ip1=explode(',',$my_ip);
//echo 'hello'.$userid;
if($userid=='Guest'){
		$session_id=session_id();	
		$qGetMyCart2=query_execute("SELECT sc.cart_id FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.session_id = '$session_id' AND si.stock_status='instock' AND si.item_display_status!='delete' AND sc.cart_order_status='new' AND (sc.item_price!='0' OR sc. item_id='1283') AND sc.donate_bag='0' ");
	}else{
		$qGetMyCart2=query_execute("SELECT sc.cart_id FROM shop_cart AS sc JOIN shop_items AS si ON sc.item_id = si.item_id WHERE sc.userid =  '$userid'
AND sc.cart_order_status =  'new' AND si.stock_status =  'instock' AND si.item_display_status !=  'delete' AND ( sc.item_price !=  '0' OR sc.item_id =  '1283'
) AND sc.donate_bag='0'");
}
$totlrecord = mysql_num_rows($qGetMyCart2);
if($userid !='Guest'){ 
$getnotification=query_execute_row("SELECT noti_id,c_date,last_check_id FROM users_notifications WHERE (my_ip='".$my_ip1[0]."' OR userid='$userid') ORDER BY c_date DESC LIMIT 1");
}else
{
$getnotification=query_execute_row("SELECT noti_id,c_date,last_check_id FROM users_notifications WHERE my_ip='".$my_ip1[0]."' ORDER BY c_date DESC LIMIT 1");
}
$date_created=$getnotification['c_date'];
$last_check_id=$getnotification['last_check_id'];
//echo "SELECT * FROM browser_push_notifications WHERE date_created>='$date_created' AND date_created<'$Ndate' AND status='active' ORDER by noti_id DESC";
$getnotif=query_execute("SELECT * FROM browser_push_notifications WHERE  noti_id>'$last_check_id' AND date_created<'$Ndate' AND status='active' ORDER by noti_id DESC");
$countNO=mysql_num_rows($getnotif);
$getnotification2=query_execute_row("SELECT noti_id FROM browser_push_notifications WHERE status='active' AND date_created<'$Ndate' ORDER by noti_id DESC LIMIT 1");?>

    <header itemscope itemtype="https://schema.org/WPHeader">
        <button class="toast pull-left" on="tap:mainSideBar.toggle"><span></span></button>
       <div class="pull-right cms-container">
        	<a href="#" on="tap:mainnoti.toggle"><i class="fa fa-bell-o"><? if($countNO<4){echo $countNO; } ?></i></a>
        	<a href="/cart.php"><i class="fa fa-shopping-cart"><?=$totlrecord ?></i></a>
        	
        </div> <div class="am-search-box clear-both">
      	<form method="get" action="/search.php" target="_top">
      		<input autofocus class="" id="search-query-new" name="q" placeholder="Search Food, Products, Toys etc..." type="text">
      		<button class="ampbtn s-btn user-valid valid" id="btnSearchBox" type="submit" >
			<i class=" fa fa-search"></i>
			</button>
            <input name="query" id="query"  type="hidden" />
            <input name="type" id="type"  type="hidden" />
      	</form>
      </div>
      
    </header>

<? require($DOCUMENT_ROOT.'/new/common/category-list.php');  ?>