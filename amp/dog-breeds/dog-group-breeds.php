<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

include("constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$siteURL=''; 
$sel_grp_name=query_execute_row("SELECT * FROM breed_engine_att_att WHERE att_id='35' AND att_att_id='$group_id'");
$sel_grp_det=query_execute_row("SELECT * FROM breed_engine_group WHERE group_id='$group_id'");
$sel_odr_grp_name=mysql_query("SELECT * FROM breed_engine_att_att WHERE att_id='35' AND att_att_id!='$group_id'");
$sel_grp_members=mysql_query("SELECT breed_id FROM breed_engine_values WHERE att_id='35' AND value='$group_id'");
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = '';
$ant_category =ucwords(str_replace("-"," ",$section[0]));?>
<!doctype html>
<html AMP lang="en">
<head><meta charset="utf-8">
<? if($sel_grp_name['value']=='Gundog'){?>
<title>Gundog Group | Hunting Dog | Sporting Dogs</title>
<meta name="keywords" content="Gundog Dog Breeds, Gundog Group Dog Breeds, Gundog Dogs, hunting dog group, sporting dogs group." />
<meta name="description" content="Gundog Group Dog Breeds consists of sporting Dogs which are specially used in hunting, including Cocker Spaniel, Golden Retriever, Irish Setter, Labrador Retriever etc." />
<? }elseif($sel_grp_name['value']=='Pastoral'){ ?>
<title>Pastoral Group | Herding Dogs</title>
<meta name="keywords" content="Pastoral Dog Breeds, Pastoral Group Dog Breeds, Pastoral Dogs, Herding Dogs." />
<meta name="description" content="Pastoral Group consists of herding dogs which are commonly bred for a working life in the fields including Border Collie, German Shepherd Dog (Alsatian), Shetland Sheep dogs etc." />
<? }elseif($sel_grp_name['value']=='Utility'){ ?>
<title>Utility Group | Non-Sporting Dogs</title>
<meta name="keywords" content="Utility Dog Breeds, Utility Group Dog Breeds, Utility Dogs, Non-Sporting Dogs." />
<meta name="description" content="Utility group consists of miscellaneous dog breeds, mainly non sporting origin including bull dog, dalmatian, lhasa apso, shar pei etc. Find more about Utility dog breeds at DogSpot.in." />
<? }else{?>
<title><?=$sel_grp_name['value']; ?> | <?=$sel_grp_name['value']; ?> Group Dog Breeds | <?=$sel_grp_name['value']; ?> Dogs | DogSpot.In</title>
<meta name="keywords" content="<?=$sel_grp_name['value']; ?> Dog Breeds, <?=$sel_grp_name['value']; ?> Group Dog Breeds,<?=$sel_grp_name['value']; ?> Dogs" />
<meta name="description" content="Find <?=$sel_grp_name['value']; ?> Group Dog Breeds List and <?=$sel_grp_name['value']; ?> Dog Breeds List. Find more about <?=$sel_grp_name['value']; ?> dog breeds at DogSpot.in" />
<? }?>
    <link rel="canonical" href="https://www.dogspot.in/<?= $section[1] ?>/" />
    <?php require_once($DOCUMENT_ROOT .'/new/common/top-2-amp.php'); ?>
	<div class="dog_breeds_sec ui-content"><div class="breadcrumb" >
    <div  itemscope itemtype="http://schema.org/Breadcrumb">
    <div itemscope itemtype="http://schema.org/BreadcrumbList"> 
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <a href="/amp/dog-breeds/" itemprop="item" data-ajax="false"><span itemprop="name">Dog Breeds</span></a>
       <meta itemprop="position" content="1" /> </span>
     <span> / </span>
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> 
     <span itemprop="name" class="brd_font_bold"><?=$sel_grp_name['value']; ?> Group</span>
      <meta itemprop="position" content="2" /> </span></div>
    <div class="cb"></div></div></div>
    <div class="dogbreeds_content" style="float:left;width: 100%;background: #fff;">
<?
$sel_img=query_execute_row("SELECT db.image_name FROM dog_breeds as db ,breed_engine_values as bev WHERE bev.att_id='35' AND bev.value='$group_id' AND db.breed_id=bev.breed_id AND db.breed_engine='1'");
?>
    <div class="dogbreeds_blk"><amp-img src="https://www.dogspot.in/new/breed_engine/images/dog_images/<?= $sel_img['image_name']?>"alt="<?= $sel_img['image_name']?>" width="229" height="217" title="<?= $sel_img['image_name']?>" style="margin-top:40px;"></amp-img></div><h1><?=$sel_grp_name['value'] ?></h1><p><?=$sel_grp_det['group_desc']; ?></p></div><div class="be_ad_banner" style="width:100%; float:left; margin-top:10px; "><h4 style="margin-top:0px;">Dog in <?=$sel_grp_name['value'] ?> Group</h4>
<ul><? while($sel_grp_members1=mysql_fetch_array($sel_grp_members)){ 
  $sel_brd_name=query_execute_row("SELECT * FROM dog_breeds WHERE breed_id=$sel_grp_members1[breed_id]");
  $brd_grp_arr[]=$sel_brd_name['breed_name']."@@".$sel_brd_name['nicename']."@@".$sel_brd_name['image_name']."@@".
  $sel_brd_name['breed_engine'];
  }
  //print_r($brd_grp_arr);
  asort($brd_grp_arr);
  foreach($brd_grp_arr as $brd_grp_arr11){
	$brd_grp_arr1=explode("@@",$brd_grp_arr11); 
  if($brd_grp_arr1[3]=='1'){?>
  <li><a href="/<?=$brd_grp_arr1[1] ?>/">
<amp-img src="https://www.dogspot.in/new/breed_engine/images/dog_images/<?=$brd_grp_arr1[2] ?>" title="<?=$brd_grp_arr1[0] ?>" 
alt="<?=$brd_grp_arr1[0] ?>" width="148" height='140'></amp-img><div class="be_grpNam"><?=$brd_grp_arr1[0] ?></div>
</a></li><?  } }?>
</ul>
</div></div><?php require_once($DOCUMENT_ROOT .'/new/common/bottom-amp.php'); ?>
