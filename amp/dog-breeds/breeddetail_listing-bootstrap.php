<?php

include_once("constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = '';
$maxshow = 8;
if(is_numeric($show)){
if(empty($show)) {
	$show = 0;
}else{
 $show = $show - 1;
}}else{
	$show = 0;
}
 $showRecord = $show * $maxshow;
 $nextShow = $showRecord + $maxshow;
$ant_category =ucwords(str_replace("-"," ",$section[0]));
$seltitle=query_execute_row("SELECT title FROM  breed_engine_traits WHERE id='$att_id'");
if($att_id=='999'){
	$selectdata=mysql_query("SELECT breed_id FROM  dog_breeds WHERE breed_engine='1' ORDER BY height DESC LIMIT $showRecord,$maxshow");
	$selectdataT=mysql_query("SELECT breed_id FROM  dog_breeds WHERE breed_engine='1' ORDER BY height DESC ");
	}else{
	//	echo "SELECT breed_id,value FROM breed_engine_values WHERE att_id='$att_id' ORDER BY value DESC";
$selectdata=mysql_query("SELECT breed_id,value FROM breed_engine_values WHERE att_id='$att_id' ORDER BY value DESC LIMIT $showRecord,$maxshow");
$selectdataT=mysql_query("SELECT breed_id FROM  dog_breeds WHERE breed_engine='1' ORDER BY height DESC ");
}
$totrecordT=mysql_num_rows($selectdataT);
	$sel_filter=mysql_query("SELECT monthly_keeping_cost_premium,monthly_keeping_cost_standard FROM  dog_breeds WHERE breed_engine='1'");
	while($selectfil = mysql_fetch_array($sel_filter)){
		if($selectfil['monthly_keeping_cost_premium']){
	$mkcp[]=$selectfil['monthly_keeping_cost_premium'];
	$mkcs[]=$selectfil['monthly_keeping_cost_standard'];}
	}
	$Amkcp=array_unique($mkcp);
	$Amkcs=array_unique($mkcs);
	asort($Amkcp);asort($Amkcs);
	$Amkcpmin=min($Amkcp);$Amkcpmax=max($Amkcp);
	$Amkcsmin=min($Amkcs);$Amkcsmax=max($Amkcs);
	
$selectfiler=mysql_query("SELECT * FROM  breed_engine_values");
while($selectf = mysql_fetch_array($selectfiler)){
	
	if($selectf['value']>'9'){
	$trait_value[]=$selectf['value'];
	$trait_breed_id[]=$selectf['breed_id'];
	}else{
	$rating_array[]=$selectf['att_id'];	
	$rating_value[]=$selectf['att_id'].'-'.$selectf['value'];
	}
	}
	$Atrait_breed_id=array_unique($trait_breed_id);
	$Atrait_value=array_unique($trait_value);
	$Arating_value=array_unique($rating_value);
	$Arating_array=array_unique($rating_array);
	asort($Atrait_value);
	asort($Arating_value);
	asort($Arating_array);
	//print_r($Arating_array);
	//print_r($Arating_value);
	foreach($Arating_array as $ke){//1 to 19

	foreach($Arating_value as $keyArating_value){
		$key12=explode("-",$keyArating_value);
		$keyname12=$key12[0];
		$keyvalue12=$key12[1];
		if($keyname12=='1'){
		$firstfilter[]=$keyvalue12;
		}
		
		if($keyname12=='2'){ //Affection
		$Affection[]=$keyvalue12;
		}
		if($keyname12=='3'){ //Availibility
		$Availibility[]=$keyvalue12;
		}
		if($keyname12=='4'){ //Behavior With Other Dog
		$BehaviorD[]=$keyvalue12;
		}
		if($keyname12=='5'){ //Behavior With Other Pets
		$BehaviorP[]=$keyvalue12;
		}
		if($keyname12=='6'){ //Cuteness
		$Cuteness[]=$keyvalue12;
		}
		if($keyname12=='7'){ //Exercise Required
		$Exercise[]=$keyvalue12;
		}
		if($keyname12=='8'){ //Fluffyness
		$Fluffyness[]=$keyvalue12;
		}
		if($keyname12=='9'){ //Hair Density
		$Hair_Density[]=$keyvalue12;
		}
		if($keyname12=='10'){ //Hair Length
		$Hair_Length[]=$keyvalue12;
		}
		if($keyname12=='11'){ //Maintinance High/low
		$Maintinance[]=$keyvalue12;
		}
		if($keyname12=='12'){ //Noisy
		$Noisy[]=$keyvalue12;
		}
		if($keyname12=='13'){ //Playfullness
		$Playfullness[]=$keyvalue12;
		}
		if($keyname12=='14'){ //Popularity
		$Popularity[]=$keyvalue12;
		}
		if($keyname12=='15'){ //Shedding
		$Shedding[]=$keyvalue12;
		}
		if($keyname12=='16'){ //Tolerance To Cold
		$ToleranceC[]=$keyvalue12;
		}
		if($keyname12=='17'){ //Tolerance To Heat
		$ToleranceH[]=$keyvalue12;
		}
		if($keyname12=='18'){ //Trainibility
		$Trainibility[]=$keyvalue12;
		}
		if($keyname12=='19'){ //Guarding Potential
		$Guarding_Potential[]=$keyvalue12;
		}
				
	}
	}
	
	$Afirstfilter1=$Afirstfilter=array_unique($firstfilter);asort($Afirstfilter);
	$AMaintinance1=$AMaintinance=array_unique($Maintinance);asort($AMaintinance);
	$AAffection1=$AAffection=array_unique($Affection);asort($AAffection);
	$AAvailibility1=$AAvailibility=array_unique($Availibility);asort($AAvailibility);
	$ABehaviorD1=$ABehaviorD=array_unique($BehaviorD);asort($ABehaviorD);
	$ABehaviorP1=$ABehaviorP=array_unique($BehaviorP);asort($ABehaviorP);
	$ACuteness1=$ACuteness=array_unique($Cuteness);asort($ACuteness);
	$AExercise1=$AExercise=array_unique($Exercise);asort($AExercise);
	$AFluffyness=$AFluffyness=array_unique($Fluffyness);asort($AFluffyness);
	$AHair_Density1=$AHair_Density=array_unique($Hair_Density);asort($AHair_Density);
	$AHair_Length1=$AHair_Length=array_unique($Hair_Length);asort($AHair_Length);
	$ANoisy1=$ANoisy=array_unique($Noisy);asort($ANoisy);
	$APlayfullness1=$APlayfullness=array_unique($Playfullness);asort($APlayfullness);
	$APopularity1=$APopularity=array_unique($Popularity);asort($APopularity);
	$AShedding1=$AShedding=array_unique($Shedding);asort($AShedding);
	$AToleranceC1=$AToleranceC=array_unique($ToleranceC);asort($AToleranceC);
	$AToleranceH1=$AToleranceH=array_unique($ToleranceH);asort($AToleranceH);
	$ATrainibility1=$ATrainibility=array_unique($Trainibility);asort($ATrainibility);
	$AGuarding_Potential1=$AGuarding_Potential=array_unique($Guarding_Potential);asort($AGuarding_Potential);




	foreach($Atrait_value as $keys){
		$selectfilername=query_execute_row("SELECT * FROM  breed_engine_att_att WHERE att_att_id='$keys'");
		$keyatt_id[]=$selectfilername['att_id']."-".$selectfilername['value']."@".$selectfilername['att_att_id'];
		$attnames[]=$selectfilername['att_id'];	
		}
	$Akeyatt_id=array_unique($keyatt_id);
	asort($Akeyatt_id);
	$attnames=array_unique($attnames);
	asort($attnames);
	//print_r($Akeyatt_id);
	while($selectdata1=mysql_fetch_array($selectdata)){
		$selectf=query_execute_row("SELECT count(*) as cd FROM  dog_breeds WHERE breed_engine='1' AND breed_id='$selectdata1[breed_id]'");
	if($selectf['cd']>0){
	$unique[]=$selectdata1['breed_id'];}
}

?><!doctype html>
<html AMP lang="en">
<head><meta charset="utf-8">
 <title><?=$brdtitle?></title>
    <meta name="keywords" content="<?=$brdkey?>" />
    <meta name="description" content="<?=$brddesc?>" />
    <link rel="canonical" href="https://www.dogspot.in/<?=$section[1] ?>/" />
<?php require_once($DOCUMENT_ROOT .'/new/common/top-2-amp.php'); ?>
<div class="breadcrumbs">
  <div class="container">
    <div itemscope itemtype="http://schema.org/Breadcrumb">
      <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a href="/amp/dog-breeds/" itemprop="item"><span itemprop="name">Breeds</span></a>
        <meta itemprop="position" content="1" />
        </span> <span> / </span> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <span itemprop="item"> <span itemprop="name" class="active-bread">
        <?= ucwords(str_replace("-"," ",$section[1]));?>
        </span></span>
        <meta itemprop="position" content="2" />
        </span> </div>
    </div>
  </div>
</div>
<section class="breeds-section breed-tpe-sc">
<div class="container">
<div>
<div class="col-sm-9">
  <div class="title">
    <h1>
      <? 
if($att_id=='999'){echo "Choose the size that suits you the best";}else{
echo $seltitle['title'];
}

?>
    </h1>
  </div>
  <div id="show_breed"></div>
  <div class="be_rytDetail" id="productFilter">
  <?
//error_reporting(E_ALL);
 //ini_set("display_errors", 1);
if($filter=='filter'){
if($type_breed11){
	$type_breed11 = str_replace("undefined", "", $type_breed11);
	$type_breed11=trim($type_breed11,'|');
	$sbreedt=explode('|',$type_breed11);
	if($sbreedt[0]){
	foreach($sbreedt as $sbrt1){
		$sbreedtype=explode('-',$sbrt1);
		$keyss[]=$sbreedtype[0];
		$sbreedt12[]=$sbrt1;
	}
	$keyss=array_unique($keyss);
	$breedtsql='';
	if($sbreedt12[0]){
	foreach($keyss as $key1){
		foreach($sbreedt12 as $sbrt){
			$sbreedtypeT=explode('-',$sbrt);	
			$sbrt=str_replace('-','A',$sbrt);
			if($key1==$sbreedtypeT[0]){
				if($c==1){$breedtsql.= " attrib_value:$sbrt";$c=0;}else{
				$breedtsql.= " OR attrib_value:$sbrt";}$printattrib_value[]=$sbrt;
				}
			}$c=1;
		$breedtsql= trim($breedtsql,' OR ');
		$breedtsql= "($breedtsql) AND (";
		}
		$breedtsql= trim($breedtsql,' AND ()');
		if($c==1){$breedtsql= " AND ($breedtsql)";}else{$breedtsql= " AND ($breedtsql))";}
		
	}}//(((item_attribute:1A1) AND OR item_attribute:2A8))
}
//check for category id sale
if($type_breedlife){	//echo "cattttt ".$categoryid23;
	$type_breedlife = str_replace("undefined", "", $type_breedlife);
	$type_breedlife=trim($type_breedlife,'|');
	$stype_breedlife=explode('|',$type_breedlife);
	if($stype_breedlife[0]){
	foreach($stype_breedlife as $sb5){
		$bcatsql.=" OR life_stage:$sb5 ";
		$printlife_stage[]=$sb5;
	}
	$bcatsql = trim($bcatsql,' OR ');
	$bcatsql = "&fq=($bcatsql)";
	}
}

if($temperament){	if($temperament!='1A3'){	$sal=" AND attrib_value:$temperament" ;}	}
if($tAffection_txt){	if($tAffection_txt!='2A3'){	$ttAffection_txt=" AND attrib_value:$tAffection_txt" ;}	}
if($tAvailibility_txt){	if($tAvailibility_txt!='3A1'){	$ttAvailibility_txt=" AND attrib_value:$tAvailibility_txt" ;}	}
if($tBehaviorD_txt){	if($tBehaviorD_txt!='4A2'){	$ttBehaviorD_txt=" AND attrib_value:$tBehaviorD_txt" ;}	}
if($tBehaviorP_txt){	if($tBehaviorP_txt!='5A0'){	$ttBehaviorP_txt=" AND attrib_value:$tBehaviorP_txt" ;}	}
if($tCuteness_txt){	if($tCuteness_txt!='6A2'){	$ttCuteness_txt=" AND attrib_value:$tCuteness_txt" ;}	}
if($tExercise_txt){	if($tExercise_txt!='7A2'){	$ttExercise_txt=" AND attrib_value:$tExercise_txt" ;}	}
if($tFluffyness_txt){	if($tFluffyness_txt!='8A0'){	$ttFluffyness_txt=" AND attrib_value:$tFluffyness_txt" ;}	}
if($tHair_Density_txt){	if($tHair_Density_txt!='9A3'){	$ttHair_Density_txt=" AND attrib_value:$tHair_Density_txt" ;}	}
if($tHair_Length_txt){	if($tHair_Length_txt!='10A2'){	$ttHair_Length_txt=" AND attrib_value:$tHair_Length_txt" ;}	}	
if($tMaintinance_txt){	if($tMaintinance_txt!='11A2'){	$ttMaintinance_txt=" AND attrib_value:$tMaintinance_txt" ;}	}
if($tNoisy_txt){	if($tNoisy_txt!='12A2'){	$ttNoisy_txt=" AND attrib_value:$tNoisy_txt" ;}	}
if($tPlayfullness_txt){	if($tPlayfullness_txt!='13A3'){	$ttPlayfullness_txt=" AND attrib_value:$tPlayfullness_txt" ;}	}	
if($tPopularity_txt){	if($tPopularity_txt!='14A1'){	$ttPopularity_txt=" AND attrib_value:$tPopularity_txt" ;}	}
if($tShedding_txt){	if($tShedding_txt!='15A1'){	$ttShedding_txt=" AND attrib_value:$tShedding_txt" ;}	}
if($tToleranceC_txt){	if($tToleranceC_txt!='16A2'){	$ttToleranceC_txt=" AND attrib_value:$tToleranceC_txt" ;}	}	
if($tToleranceH_txt){	if($tToleranceH_txt!='17A2'){	$ttToleranceH_txt=" AND attrib_value:$tToleranceH_txt" ;}	}		
if($tTrainibility_txt){	if($tTrainibility_txt!='18A3'){	$ttTrainibility_txt=" AND attrib_value:$tTrainibility_txt" ;}	}		
if($tgp_txt){	if($tgp_txt!='19A1'){	$ttgp_txt=" AND attrib_value:$tgp_txt" ;}	}		
if($s_price1){
		$s_price1 = str_replace("Rs", "", $s_price1);
		$s_price1 = str_replace("-", "|", $s_price1);
		$s_price1=trim($s_price1,'|');
		$sprice=explode('|',$s_price1);
		$pc=count($sprice);
		if($pc>1){
		$pc=$pc-1;
		$sqlpp=" AND monthly_keeping_cost_premium:[$sprice[0] TO $sprice[$pc]]";
		}
	}	
if($s_price2){
		$s_price2 = str_replace("Rs", "", $s_price2);
		$s_price2 = str_replace("-", "|", $s_price2);
		$s_price2=trim($s_price2,'|');
		$sprice2=explode('|',$s_price2);
		$pc2=count($sprice2);
		if($pc2>1){
		$pc2=$pc2-1;
		$sqlpp2=" AND monthly_keeping_cost_standard:[$sprice2[0] TO $sprice2[$pc2]]";
		}
	}	

		$urls = "http://101.53.137.39/solr/dogspotbreedengine/select/?q=*:* $breedtsql$bcatsql$sal$ttAffection_txt$ttAvailibility_txt$ttBehaviorD_txt$ttBehaviorP_txt$ttCuteness_txt$ttExercise_txt$ttFluffyness_txt$ttHair_Density_txt$ttHair_Length_txt$ttMaintinance_txt$ttNoisy_txt$ttPlayfullness_txt$ttPopularity_txt$ttShedding_txt$ttToleranceC_txt$ttToleranceH_txt$ttTrainibility_txt$ttgp_txt$sqlpp$sqlpp2&version=2.2&rows=$maxshow&start = $showRecord&qf=name^2&df=text&wt=xml&indent=true";
		$urlsT = "http://101.53.137.39/solr/dogspotbreedengine/select/?q=*:* $breedtsql$bcatsql$sal$ttAffection_txt$ttAvailibility_txt$ttBehaviorD_txt$ttBehaviorP_txt$ttCuteness_txt$ttExercise_txt$ttFluffyness_txt$ttHair_Density_txt$ttHair_Length_txt$ttMaintinance_txt$ttNoisy_txt$ttPlayfullness_txt$ttPopularity_txt$ttShedding_txt$ttToleranceC_txt$ttToleranceH_txt$ttTrainibility_txt$ttgp_txt$sqlpp$sqlpp2&version=2.2&qf=name^2&df=text&wt=xml&indent=true";
		 $urls = str_replace(" ","%20",$urls);
		$resultsolr = get_solr_result($urls);
		$totrecord = $resultsolr['TOTALHITS'];
		 $urlsT = str_replace(" ","%20",$urlsT);
		$resultsolrT = get_solr_result($urlsT);
		$totrecordT = $resultsolrT['TOTALHITS'];
		foreach($resultsolr['HITS'] as $rowItemP11){
		$Aitem_ItemidP3[]=$rowItemP11['breed_id'];
		}
	$unique = array_map("unserialize", array_unique(array_map("serialize", $Aitem_ItemidP3)));
	}
	if($att_id=='999'){
	$word="Size";	
	}else{
	$sel_word=query_execute_row("SELECT top_text FROM breed_engine_traits WHERE id='$att_id'");
	$word=$sel_word['top_text'];
	}
?><div class="adoption_header">
          <div class="adp_m_b">
<select name="top_sort" data-role="none" id="top_sort" on="change:AMP.navigateTo(url=event.value)">
<option value="https://www.dogspot.in/amp/guard-dog-breeds/" <? if($word=='Guarding Potential'){?> selected="selected" <? } ?>>Guarding Potential</option>
<option value="https://www.dogspot.in/amp/friendly-dog-breeds/" <? if($word=='Friendliness'){?> selected="selected" <? } ?>>Friendliness</option>
<option value="https://www.dogspot.in/amp/big-dog-breeds/" <? if($word=='Size'){?> selected="selected" <? } ?>>Size</option>
<option value="https://www.dogspot.in/amp/kid-friendly-dog-breeds/" <? if($word=='Temperament with kids'){?> selected="selected" <? } ?>>Temperament with kids</option>
<option value="https://www.dogspot.in/amp/therapy-dog-breeds/" <? if($word=='Therapeutic Ability'){?> selected="selected" <? } ?>>Therapeutic Ability</option>
<option value="https://www.dogspot.in/amp/cute-dog-breeds/" <? if($word=='Cuteness & Furryness'){?> selected="selected" <? } ?>>Cuteness & Furryness</option>
</select>
          </div>
        </div> 
<div class="productListing542Load" ></div>
<div>
<?php
$count=0;
	if($att_id=='999'){foreach($unique as $rowItemitem125 ){
	$selbreedname12=mysql_query("SELECT db.breed_id,db.height,bev.value FROM dog_breeds as db , breed_engine_values as bev WHERE db.breed_engine='1' AND db.breed_id='$rowItemitem125' AND db.breed_id=bev.breed_id AND bev.att_id='41' AND (bev.value='195' OR bev.value='196') ORDER BY bev.value DESC , db.height DESC");
	while($selbreedname1=mysql_fetch_array($selbreedname12)){
	$uniquedisplay1[]=$selbreedname1['value']."**".$selbreedname1['height']."@@".$selbreedname1['breed_id'];
	}}
	rsort($uniquedisplay1,1);
	//print_r($uniquedisplay1);
	foreach($uniquedisplay1 as $uniquedisplay12){
	$exp_ht=explode("**",$uniquedisplay12);
	if($exp_ht[0]==196){
	$uniquedisplay_temp6[]=$exp_ht[1];
	}
	}
	rsort($uniquedisplay_temp6,1);
	foreach($uniquedisplay1 as $uniquedisplay12){
	$exp_ht=explode("**",$uniquedisplay12);
	if($exp_ht[0]==195){
	$uniquedisplay_temp5[]=$exp_ht[1];
	}
	}
	rsort($uniquedisplay_temp5,1);
	foreach($uniquedisplay1 as $uniquedisplay12){
	$exp_ht=explode("**",$uniquedisplay12);
	if($exp_ht[0]==194){
	$uniquedisplay_temp4[]=$exp_ht[1];
	}
	}
	rsort($uniquedisplay_temp4,1);
	foreach($uniquedisplay1 as $uniquedisplay12){
	$exp_ht=explode("**",$uniquedisplay12);
	if($exp_ht[0]==193){
	$uniquedisplay_temp3[]=$exp_ht[1];
	}
	}
	rsort($uniquedisplay_temp3,1);

	foreach($uniquedisplay_temp6 as $uniquedisplay_temp6_brd){
	$uniquedisplay[]=$uniquedisplay_temp6_brd;
	}
	foreach($uniquedisplay_temp5 as $uniquedisplay_temp5_brd){
	$uniquedisplay[]=$uniquedisplay_temp5_brd;
	}
	foreach($uniquedisplay_temp4 as $uniquedisplay_temp4_brd){
	$uniquedisplay[]=$uniquedisplay_temp4_brd;
	}	
	foreach($uniquedisplay_temp3 as $uniquedisplay_temp3_brd){
	$uniquedisplay[]=$uniquedisplay_temp3_brd;
	}	
	}
else{
	//print_r($unique);
	foreach($unique as $rowItemitem12 ){
	if($att_id=='44' && ($rowItemitem12!='104' && $rowItemitem12!='28' && $rowItemitem12!='96' && $rowItemitem12!='69' && $rowItemitem12!='134' && $rowItemitem12!='27' && $rowItemitem12!='623' && $rowItemitem12!='122' && $rowItemitem12!='112' && $rowItemitem12!='93')){
			
	$selbreedname=query_execute_row("SELECT breed_id,value1 FROM breed_engine_values WHERE  breed_id='$rowItemitem12' AND att_id='$att_id'");
	$uniquedisplay[]=$selbreedname['value1']."@@".$selbreedname['breed_id'];
	
	}elseif($att_id=='43' && ($rowItemitem12!='104' && $rowItemitem12!='28' && $rowItemitem12!='96' && $rowItemitem12!='69' && $rowItemitem12!='134' && $rowItemitem12!='27' && $rowItemitem12!='623' && $rowItemitem12!='122' && $rowItemitem12!='130' && $rowItemitem12!='113' && $rowItemitem12!='46' && $rowItemitem12!='93' && $rowItemitem12!='47' && $rowItemitem12!='67' && $rowItemitem12!='112')){	
	$selbreedname=query_execute_row("SELECT breed_id,value1 FROM breed_engine_values WHERE  breed_id='$rowItemitem12' AND att_id='$att_id'");
	$uniquedisplay[]=$selbreedname['value1']."@@".$selbreedname['breed_id'];
	}elseif($att_id=='53' && ($rowItemitem12!='86' && $rowItemitem12!='60' && $rowItemitem12!='648' && $rowItemitem12!='23' && $rowItemitem12!='26' && $rowItemitem12!='11' && $rowItemitem12!='47' && $rowItemitem12!='112' && $rowItemitem12!='113' && $rowItemitem12!='27' && $rowItemitem12!='43' && $rowItemitem12!='59' && $rowItemitem12!='10' && $rowItemitem12!='96' && $rowItemitem12!='94' && $rowItemitem12!='636' && $rowItemitem12!='67' && $rowItemitem12!='46' && $rowItemitem12!='93' && $rowItemitem12!='627' && $rowItemitem12!='28'
	 && $rowItemitem12!='69' && $rowItemitem12!='142' && $rowItemitem12!='57')){	
	$selbreedname=query_execute_row("SELECT breed_id,value1 FROM breed_engine_values WHERE  breed_id='$rowItemitem12' AND att_id='$att_id'");
	$uniquedisplay[]=$selbreedname['value1']."@@".$selbreedname['breed_id'];
	}elseif($att_id!='43' && $att_id!='44' && $att_id!='53'){
		// allbreed accept att id 44,43
	//echo "SELECT breed_id,value1 FROM breed_engine_values WHERE  breed_id='$rowItemitem12' AND att_id='$att_id'"."<br>";	
	$selbreedname=query_execute_row("SELECT breed_id,value1 FROM breed_engine_values WHERE  breed_id='$rowItemitem12' AND att_id='$att_id'");
	$uniquedisplay[]=$selbreedname['value1']."@@".$selbreedname['breed_id'];	
	}
	}

rsort($uniquedisplay);
}

if(count($uniquedisplay)==0){echo "<META NAME='ROBOTS' CONTENT='NOINDEX, NOFOLLOW'>";}
if(count($uniquedisplay)>0){
foreach($uniquedisplay as $rowItemitem ){
	$exbreed=explode("@@",$rowItemitem);
$selbreedname=query_execute_row("SELECT a.breed_id,a.breed_name,a.nicename,b.value FROM dog_breeds as a ,breed_engine_values as b WHERE  a.breed_id= b.breed_id AND a.breed_id='$exbreed[1]' ORDER BY b.value1 DESC");
$breed_id=$selbreedname['breed_id'];
$breedname=$selbreedname['breed_name'];
$nicename=$selbreedname['nicename'];
$value=$selbreedname['value'];
$selbreedname1=query_execute_row("SELECT breed_name,nicename,image_name,height,weight,be_name FROM dog_breeds WHERE breed_id='$breed_id'");
	$image_name=$selbreedname1['image_name'];

		
	
?><div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"><div class="breeds-box" style="height:auto;"><a href="/amp/<?=$nicename; ?>/">
<h3><?=$selbreedname1['be_name']?></h3><amp-img src="https://www.dogspot.in/new/breed_engine/images/dog_images/<?=$image_name?>" alt="<?= $breedname ?> " title="<?= $breedname ?>" width='229' height='217' style="margin-left: 36px;" /></amp-img></a><div class="be_detailBox"><? if($att_id!='999'){?><div class="be_rating">
<? 
$attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
$nRating=(($qRATING['value1']/1000)*100);
							
?><div class="meter orange nostripes">
	<span style="width: <?=$nRating?>%"></span>
</div>
  <span class="meter-span"><?= $attname['min_value'] ?></span>
<span class="meter-span"><?= $attname['max_value'] ?></span>
</div>
<? }?>
<div class="breed-wnh"><ul><li>
<label>Weight</label>
<span><?=$selbreedname1['weight']?> Kg<a style="cursor:pointer" id="Average for the breed" title="Average for the breed">*</a></span></li><li><label>Height</label>
<span><?=$selbreedname1['height']?> Inches<a style="cursor:pointer" id="This is an average height till head" title="This is an average height till head">*</a></span>
</li>
</ul>
</div>
<div class="breed-com">
<ul style="margin-top: 9px;height: 40px;">
<li><a  href="/amp/<?=$nicename; ?>/" style="font-weight: 600;">View Details</a></li>
</ul>
</div>
</div> 
</div>
</div>

<?
}?><? }else{?>
<?  if($att_id=='33'){$b="big-dog-breeds";}
  ?><div class="col-sm-12">
<div class="col-xs-12 col-sm-6 col-md-4 text-center"> <amp-img src="/new/breed_engine/images/notFound.jpg" width="250" height="100" alt="<?=$att_id?>" title="<?=$att_id?>"  /></amp-img> </div>
<div class="col-xs-12 col-sm-6 col-md-8 text-center">
<div class="be_noFtxt"><p>No Result Found</p>
<div class="be_stay">Stay tuned for more breeds</div>
</div>
</div></div>
	<? }?>
</div>
<div class="pagination">
			 <?php
			// echo "total".$totrecordinstockT;
                         $pageUrl = "/amp/$section[1]";
                    showPages_courses($totrecordT, $maxshow, $pageUrl, $show, $next, $previous, $spUrl, $url);
                    ?></div>
  </div>
</div>
</div>
</div>
</section>
<?php require_once($DOCUMENT_ROOT .'/new/common/bottom-amp.php'); ?>
