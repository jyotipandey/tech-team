<?php require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$ant_page = 'breedhome';
?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title>Dog Breeds - Complete Information of All Types of Dogs - Dogspot.in</title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="It will give you a comprehensive view for every individual breed; this includes the cost involved in getting a puppy home to the monthly expenditure." />
    <meta name="keywords" content="Dog breed name, Pet breeds in India, Indian dog breeds, Dog breed info, Dog breed types" />
    <link rel="canonical" href="https://www.dogspot.in/dog-breeds/" />
     <? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
  	<script type="application/ld+json">{ "@context":"http://schema.org", "@type":"Blog",
	 "@id":"https://www.dogspot.in/<?=$section[1];?>/", "about":{ "@type":"CreativeWork", "name":"Dog Breeds" }, "headline":"Dog Breeds", "description":"It will give you a comprehensive view for every individual breed; this includes the cost involved in getting a puppy home to the monthly expenditure."}</script>
     <div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a>
                            <span itemprop="position" content="1"></span>
                        </span><span>/</span>
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="/amp/dog-breeds/" itemprop="item"><span itemprop="name">Dog Breeds</span></a>
                            <span itemprop="position" content="2"></span>
                        </span>
                       </div>
      <div class="space-2"></div>
      <h1 style="text-align:center">Dog Breeds</h1>                 
 <div class="dog_breeds_sec"> 
				<div class="dogbreeds_blk">
				<ul class="dogbreeds_listing">
<? $breednameselect=mysql_query("SELECT * FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC");
$iid=0;
while($breeddetails=mysql_fetch_array($breednameselect)){
	$iid=$iid+1;
$breedid=$breeddetails['breed_id'];
$breedname=$breeddetails['breed_name'];
$breednice=$breeddetails['nicename'];
$imgname=$breeddetails['image_name'];
$icon=$breeddetails['icon'];
$ht=$breeddetails['height'];
$wt=$breeddetails['weight'];
$be_name=$breeddetails['be_name'];
$breedadjselect=mysql_query("SELECT * FROM breed_adj WHERE breed_id='$breedid' ORDER BY adj_name ASC LIMIT 4");
while($breedadjselect1=mysql_fetch_array($breedadjselect)){
$adjname[]=$breedadjselect1['adj_name'];
}
	/*if($imgname){
		$src = $DOCUMENT_ROOT."/new/breed_engine/images/dog_images/".$imgname;
	*/	$imageURL='/new/breed_engine/images/dog_images/165x225-'.$icon;
//	}
                    $image_info   = getimagesize($DOCUMENT_ROOT.$imageURL);
                    $image_width  = $image_info[0];
                    $image_height = $image_info[1];
	//createImgThumbIfnot($src,$dest,'165','225','ratiohh');
?> <li>
    					<a href="/amp/<?=$breednice; ?>/" aria-label="<?=$be_name?>">
						<div class="dogbreedsimg_blk"><amp-img src="<?=$imageURL;?>" width="<?=$image_width?>" height="<?=$image_height?>" alt="<?=$be_name?>" layout="responsive"></amp-img></div>
						<div class="dogbreedstext_blk"><?=$be_name?>
                            </div></a></li><?  if($iid==3){ ?><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="5144837740" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div><? } if($iid==6){ ?>
<amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="3640184387"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? }if($iid==9){ ?>
              <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="1993782992"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? } if($iid==12){ ?>
 <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="7301949999"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? } if($iid==15){ $iid=0;?>
             <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="9630877664"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? } ?><?
$adjname=''; }?></ul></div></div>
<div class="container-fluid"><p> Keeping pets is not exactly a child’s play. It requires a lot of research; you cannot go and get a pup just because he or she is cute or just because you are getting it easily. Having a pet is a lot of work and at times can be tedious and time consuming as well. Getting a puppy is like bringing a child home, it is a lot of hard work with excellent rewards. So, when you decide that it is time to own a pet it is absolutely essential to know a few things about the breed.<br />
The FCI (the world canine organization) recognises 343 dog breed types in the world. That is a whopping number to select a breed from, so what do you do in such cases. We will help you resolve this issue efficiently.<br /><br />
<strong>Recognised breeds</strong><br />
As we mentioned earlier there are numerous dogs breeds in the world and all of them are not even recognised by the Kennel Clubs. So, how do you know that which is the pedigree breed and has been recognised by the Kennel Clubs. Simple, in our breed write ups, we have dedicated an entire section to the breed standards that is how each and every part of the breed should be or you can say that the standards that are excepted by the kennel clubs across the  world. The standards have been taken from the esteemed kennel clubs such as AKC and UK Kennel Club.<br />
This will be for both the existing as well as the prospective dog owners. So now whichever breed you want, whether  it is the handsome German Shepherd or one of the adorable Retrievers, we will help you to find the perfect pet breeds in India and throughout the world as per your requirement.<br /><br />
<strong>Why do you want a dog?</strong><br />
Our six broad filters will help you to select the perfect breed. Not everyone has the same requirement. Every breed will have its pros and cons; we will help you to select the perfect breed for your home or estate. Whether it is to guard your big estate or just for companionship in your home. Our aim is simple to show the dog that will be best suited for you. <br /><br />
<strong>How will it help you?</strong><br />
Wagpedia will give you a comprehensive view for every individual breed; this includes the cost involved in getting a puppy home to the monthly expenditure that is incurred in the upkeep of the individual breed. This will include an average of expenditure incurred for the breed’s dry dog food, grooming and monthly vet visits.<br />
The write ups have been divided into sub categories for both present and prospective dog owner to help you read specific sections as well if you want.  Our aim is to help you select the appropriate breed for you home, whatever the need maybe.<br />
We plan to add more and more breeds to make it easier for our patrons. If we have forgotten a breed please then we apologise and will make a sincere effort to include them soon. So just watch out for this space for interesting reads or dog breed info.<br />
</p><div class="space-2"></div><div class="row">
<div class="col-xs-12">
<div class="cat-section">
<div class="cat-gallery" style="background-color:#6c9d06;height:23px;">
<ul style="margin:-38px 5px">
<li style="background-color:#6c9d06"><a href="/big-dog-breeds/" class="swiper-slide product-slide " aria-label="big 
dog"><div style="color:#fff;">BIG DOG</div></a></li>
<li style="background-color:#6c9d06"><a href="/therapy-dog-breeds/" class="swiper-slide product-slide " aria-label="stress 
buster"><div style="color:#fff;">STRESS BUSTER</div></a></li>
<li style="background-color:#6c9d06"><a href="/friendly-dog-breeds/" class="swiper-slide product-slide " aria-label="just a Friend"><div style="color:#fff;">JUST A FRIEND</div></a></li>
<li style="background-color:#6c9d06"><a href="/cute-dog-breeds/" class="swiper-slide product-slide " aria-label="cute & furry buddy"><div style="color:#fff;">CUTE & FURRY BUDDY</div></a></li>
<li style="background-color:#6c9d06"><a href="/kid-friendly-dog-breeds/" class="swiper-slide product-slide " aria-label="for kids"><div style="color:#fff;">FOR KIDS</div></a></li>
<li style="background-color:#6c9d06"><a href="/guard-dog-breeds/" class="swiper-slide product-slide " aria-label="guard dog"><div style="color:#fff;">GUARD DOG</div></a></li>
</ul></div></div></div></div></div><div class="breedComName_be" style="border-top:0px;">
  <h3>Top Breed Comparison</h3>
  <div class="be_comBrdNames">
    <ul>
      <li><a href="/amp/german-shepherd-dog-alsatian-vs-labrador-retriever-compare/" aria-label="German Shepherd Dog (Alsatian) vs Labrador Retriever">German Shepherd Dog (Alsatian) vs Labrador Retriever</a></li>
      <li><a href="/amp/pit-bull-terrier-american-vs-rottweiler-compare/" aria-label="Rottweiler vs Pit Bull Terrier (American)">Rottweiler vs Pit Bull Terrier (American)</a></li>
      <li><a href="/amp/rottweiler-vs-tibetan-mastiff-compare/" aria-label="Tibetan Mastiff vs Rottweiler">Tibetan Mastiff vs Rottweiler</a></li>
      <li><a href="/amp/golden-retriever-vs-labrador-retriever-compare/" aria-label="Golden Retriever vs Labrador Retriever">Golden Retriever vs Labrador Retriever</a></li>
    </ul>
  </div>
</div>
<? include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>