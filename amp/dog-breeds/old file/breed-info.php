<?php
require_once($DOCUMENT_ROOT . '/constants.php');
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
//require_once($DOCUMENT_ROOT . '/home-functions.php');
$sitesection='dog-breeds';
$ant_section = 'Dog Breeds';
$breednameselect = query_execute_row("SELECT breed_name,nicename,breed_life,monthly_keeping_cost_premium,monthly_keeping_cost_standard,icon,image_name,height,weight,img_link,img_dog_name,breed_engine,be_name,fb_img FROM dog_breeds WHERE breed_id='$breed_id'");
$breedname       = $breednameselect['breed_name'];
$breednice       = $breednameselect['nicename'];
$icon            = $breednameselect['icon'];
$img             = $breednameselect['image_name'];
$fb_img          = $breednameselect['fb_img'];
$ht              = $breednameselect['height'];
$wt              = $breednameselect['weight'];
$imglink         = $breednameselect['img_link'];
$img_dog_name    = $breednameselect['img_dog_name'];
$breed_engine    = $breednameselect['breed_engine'];
$ant_category = $be_name         = $breednameselect['be_name'];
$lifestage         = $breednameselect['breed_life'];
$costpr          = $breednameselect['monthly_keeping_cost_premium'];
$costst          = $breednameselect['monthly_keeping_cost_standard'];
$selorigin       = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='39'");
$selorigin1      = $selorigin['value'];
$selsize2       = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='41'");
$selsize    	 = $selsize2['value'];
if($selsize=='193'){
	$selsize1='2';
}else if($selsize=='194'){
	$selsize1='3';
}else if($selsize=='195'){
	$selsize1='5';
}else{
	$selsize1='6';
}
$selorigin1name  = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_id='39' AND att_att_id='$selorigin1'");
$selgroup        = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='35'");
$selgroup1       = $selgroup['value'];
$selgroup1name   = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_id='35' AND att_att_id='$selgroup1' ");
$seltag          = query_execute_row("SELECT data FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id='1'");
$selogn          = query_execute_row("SELECT data FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id='4'");
$sel_odr_grp_name=mysql_query("SELECT * FROM breed_engine_att_att WHERE att_id='35'");
$selecttitle=query_execute_row("SELECT * FROM breed_engine_breed_titles WHERE breed_id='$breed_id'");

$new_ht= round($new_ht=(266/72)*$ht);

?><!doctype html><html AMP lang="en"><head><meta charset="utf-8"><title><?=$selecttitle['title']?></title><meta name="author" content="DogSpot" /><meta name="description" content="<?=$selecttitle['description']?>" /><meta name="keywords" content="<?=$selecttitle['keyword']?>" /><link rel="canonical" href="https://www.dogspot.in/<?=$section[1]?>/" /><? require($DOCUMENT_ROOT.'/new/common/top-2-amp.php');  ?>
<? if($breed_engine!='1'){
     header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();	
?><META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"><? }?><div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a><span itemprop="position" content="1"></span>
</span><span>/</span><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?=$breedname?></span><span itemprop="position" content="2"></span></span></div><div class="space-2"></div><div class="dogbreeds_content"><div class="dogbreeds_blk"><amp-img src="https://www.dogspot.in/new/breed_engine/images/dog_images/<?= $img ?>" width="229" height="217" alt="Dog Breed"></amp-img> </div><div class="breeds-box-pc"><? if($img_dog_name!=''){ ?><h4>Picture Courtesy:</h4><label><? if($section[0]!='indian-pariah-dog'){?><a href="<?=$imglink?>" target="_blank" ><? }?><?=$img_dog_name; ?><? if($section[0]!='indian-pariah-dog'){?></a><? }?><? if($section[0]=='indian-pariah-dog'){?>(http://indog.co.in/)<? }?></label><? } ?></div><div class="breed_type"><h1><?=$breedname ?></h1></div><div><?= htmlspecialchars_decode(stripallslashes($seltag['data'])); ?></div><div><strong>Country Of Origin:</strong> &nbsp;<?= $selorigin1name['value'] ?></div><div><strong>Group:</strong> &nbsp;<a href="/<?=strtolower($selgroup1name['value'])?>-group-dog-breeds/" target="_blank" data-ajax="false"><?= $selgroup1name['value'] ?></a></div><div><strong>Origin of Name:</strong><?= htmlspecialchars_decode(stripallslashes($selogn['data'])); ?></div><div class="breed-info-box"><div class="breed-info-box-height"><div class="breed-info-box-icon"><? if($breed_id=='636'){ ?><p class="das_wc_height">Standard :<? }else{ ?><p><? } ?><?=$ht ?> Inches<span style="cursor:pointer; float:none; margin:0;" id="This is an average height till head" title="This is an average height till head">*</span></p><?
	//if($icon){
		//$src = $DOCUMENT_ROOT.'/new/breed_engine/images/icons/'.$icon;
		$imageURL='/new/breed_engine/images/icons/new-ht-'.$icon;
	//}else{
		//$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
	//	$imageURL='/dogs/images/no-photo-t.jpg';
	//}
	$dest = $DOCUMENT_ROOT.$imageURL;
$image_info   = getimagesize($dest);       $image_width  = $image_info[0];       $image_height = $image_info[1];
?><amp-img src="https://www.dogspot.in/new/breed_engine/images/icons/new-ht-<?=$icon?>"  width="<?=$image_width?>" height="<?=$image_height?>" alt="<?=$breedname?>"></div></div><div class="breed-info-weight-box"><? if($breed_id=='636'){ ?><div class="breed-info-box-weight">  <p>Miniature : 6 Inches    <label style="cursor:pointer" id="This an average height for the male dog" title="This an average height for the male dog">*</label></p></div><? } ?><? if($breed_id!='636'){ ?><div class="breed-info-box-weight">  <p>In Kg<span style="cursor:pointer" id="Average for the breed" title="Average for the breed" >*</span><br>    <?=$wt ?>  </p></div><? } if($breed_id=='636'){ ?><div class="breed-info-box-weight"> Standard  <p>In Kg<span style="cursor:pointer" id="Average for the breed" title="Average for the breed" >*</span><br>    <?=$wt ?>  </p></div><div class="breed-info-box-weight"> Miniature  <p>In Kg<span style="cursor:pointer" id="Average for the breed" title="Average for the breed" >*</span><br>    upto 4 Kg</p></div><? } ?></div></div><table class="breads_tab"><? $tabledata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='41'");
			while($tabledata1=mysql_fetch_array($tabledata)){
				$tablevalue=$tabledata1['value'];
				$tablevaluename=query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$tablevalue'");
				$size=$tablevaluename['value'];
			}
			$tabledata=mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='30'");
			while($tabledata1=mysql_fetch_array($tabledata)){
				$tablevalue=$tabledata1['value'];
				$tablevaluename=query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$tablevalue'");
				$att[]=$tablevaluename['value'];
			}
			$minmax=query_execute_row("SELECT min_value,max_value FROM breed_engine_traits WHERE id='15'");
			$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='15'");

			?><tr><td><strong>Size</strong></td><td><?=$size?></td></tr><tr><td><strong>Efforts</strong></td><td><?=$att[0]?></td></tr><tr><td><strong>Shedding</strong></td><? 
$minmax=query_execute_row("SELECT min_value,max_value FROM breed_engine_traits WHERE id='15'");
$qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='15'");
$nRating=(($qRATING['value1']/1000)*100); if ($nRating <= 0 || $nRating > 5) {   $finalratng = '0';   $ratvalue   = "rate1"; } if ($nRating > 0 && $nRating <= 1) {   $finalratng = '1';   $ratvalue   = "rate1"; } if ($nRating > 1 && $nRating <= 2) {   $finalratng = '2';   $ratvalue   = "rate2"; } if ($nRating > 2 && $nRating <= 3) {   $finalratng = '3';   $ratvalue   = "rate3"; } if ($nRating > 3 && $nRating <= 4) {   $finalratng = '4';   $ratvalue   = "rate4"; } if ($nRating > 4 && $nRating <= 5) {   $finalratng = '5';   $ratvalue   = "rate5"; }
?><td class="no_border"><div class="meter orange nostripes" style="margin:0px; padding:1px 0px;"> <span style="width:<?=$nRating?>%"></span> </div>  <span class="fl">  <?=$minmax['min_value']; ?>  </span> <span class="fr">  <?=$minmax['max_value']; ?>  </span></td></tr><tr><td colspan="2"><strong>Monthly keeping cost</strong></td></tr><tr><td><strong> Premium</strong></td><td><strong>Standard</strong></td></tr><tr><td>Rs.  <?= number_format($costpr); ?></td><td>Rs.  <?= number_format($costst); ?></td></tr>
         </table></div><? if($breed_nicename!='german-shepherd-dog-alsatian' && $breed_nicename!='beagle'){?><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="3640184387" data-auto-format="rspv" data-full-width> <div overflow></div></amp-ad><div class="divider-30 colored"></div><? }?>       
<? $selectdata = mysql_query("SELECT * FROM breed_engine_values WHERE breed_id='$breed_id' ORDER BY att_id DESC");
		while ($selectdata1 = mysql_fetch_array($selectdata)) {
			$att_id1[] = $selectdata1['att_id'];
			$att_value = $selectdata1['value'];
		}?><header class="text-center h3 heading-white">BREED INFO</header><div class="accordion-content">
      <div class="breed_info">
        <? foreach ($att_id1 as $att_id) {if ($att_id == '37' || $att_id == '3' || $att_id == '14' || $att_id == '27') {
        		$attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
	     $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        if ($att_id <= '20') { $nRating = (($qRATING['value1']/1000)*100);
	      ?><ul><li><label>        <?= $attname['traits']; ?>      </label>    </li>    <li class="no_border">      <div class="meter orange nostripes"> <span style="width: <?=$nRating?>%"></span> </div>      <span class="fl">      <?= $attname['min_value'] ?>      </span> <span class="fr">      <?=$attname['max_value'] ?>      </span> </li>  </ul>  <? } else { $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'"); ?>  <ul>    <li>      <label>        <?= $attname['traits']; ?>        <? if($att_id=='27'){ echo " <a href='#breedInfo' data-rel='popup' data-transition='pop'  title='Learn more'><i class='fa fa-info-circle' aria-hidden='true'></i></a>
<div data-role='popup' id='breedInfo' class='ui-content' data-theme='a' style='max-width:350px;'>
  <p>Average for the breed</p>
</div>";} ?>        <? if($att_id=='37'){ echo " <a href='#breedInfo' data-rel='popup' data-transition='pop'  title='Learn more'><i class='fa fa-info-circle' aria-hidden='true'></i></a>
<div data-role='popup' id='breedInfo' class='ui-content' data-theme='a' style='max-width:350px;'>
  <p>Average for the breed</p>
</div>";} ?>      </label>    </li>   <li class="no_border">      <?= $valuename['value']; 
  		if($att_id=='27'){ ?>      <? if($valuename['value']=='Economical'){ $tit_id='(upto Rs 5,000)';} else if($valuename['value']=='Pocket Friendly'){ $tit_id='(Rs 10,000 - Rs 20,000)';} else if
($valuename['value']=='Expensive'){$tit_id='(Rs 25,000 - Rs 30,000)';} else{$tit_id='(Rs 35,000 - Rs 50,000)';}?>      <a href="#popupInfo" data-rel="popup" data-transition="pop"  title="Learn more"><i class="fa fa-info-circle" aria-hidden="true"></i></a>      <div data-role="popup" id="popupInfo" class="ui-content" data-theme="a" style="max-width:350px;">        <p><?=$tit_id?> approximate cost,which can very according to the puppy and the location.</p>      </div>      <? } ?></li></ul><? }}}?><div class="be_detailTxtIntro"><? $selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (2,3,5,7,15,16)  ORDER BY  title_id ASC");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
    if ($articletitleid != '15' && $articletitleid != '16' && $articletitleid != '7' ) {
?><h3><?= htmlspecialchars_decode(stripallslashes($selecttitlename['title'])); ?></h3>
<? if(($breed_nicename=='german-shepherd-dog-alsatian') || ($breed_nicename=='beagle') || 
($breed_nicename=='belgian-malinois') || ($breed_nicename=='golden-retriever')){ ?>
<? $regs='';$result=''; if (preg_match_all('%(<p[^>]*>.*?</p>)%i', htmlspecialchars_decode(stripallslashes($articlecontent)), $regs)) {
    $result = $regs[0][0];
	if($selecttitlename['title']=='Introduction')
	{
		$result .= $regs[0][1];
	if($breed_nicename=='beagle')
	{
		$result .= $regs[0][2];
	//	$result .= $regs[0][3];
	}}
	
} echo $result;}else{ echo htmlspecialchars_decode(stripallslashes($articlecontent)); }?>
<? }?><?    if ($articletitleid == '15' || $articletitleid == '16'){
      if ($articletitleid == '15') {?></div></div><div class="be_pros"> <strong>Pros</strong>  <ul><li><?= $articlecontent = htmlspecialchars_decode(stripallslashes(preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', $articlecontent))); ?></li>  </ul></div><?
        } else {
?><div class="be_pros" > <strong>Cons</strong>  <ul><li>      <? $articlecontent = strip_a_tags(htmlspecialchars_decode(preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', $articlecontent))); 
 echo htmlspecialchars_decode(stripallslashes($articlecontent)); ?>    </li></ul></div><? }}
	if($articletitleid=='7'){ ?><div class="be_detailTxt be_funBox"> <div class="be_funImg"> <amp-img src="https://www.dogspot.in/new/breed_engine/images/mask1.png" width="66" height="66"></amp-img></div><h3><?=htmlspecialchars_decode($selecttitlename['title']); ?></h3>
<? echo htmlspecialchars_decode(stripallslashes($articlecontent)); ?></div><? } }?>
<? if($breed_nicename!='beagle'){ if($breednice=='indian-pariah-dog'  || $breednice=='jack-russell-terrier'){
	$selalb2=mysql_query("SELECT * FROM dogs_available WHERE breed_nicename='$breednice' AND publish_status='publish' AND dog_image !='' ORDER BY cdate DESC LIMIT 4");  
	  ?><div class="be_slideBox" >  <div class="be_funImg" id="picturewrap">  <amp-img src="https://www.dogspot.in/new/breed_engine/images/gall.png" width="68" height="57"></amp-img></div>  <h3><?=$be_name ?> Photos from our collection</h3>  <div id="columns">    <? 
	while ($row = mysql_fetch_array($selalb2)) {
	$dog_id  = $row["dog_id"];
	$dog_name  = ucfirst(strtolower($row["dog_name"]));
	$dog_image = $row["dog_image"];
	$dog_nicename = $row["dog_nicename"];
	$breed_nicename = $row["breed_nicename"];
	$dog_name = stripslashes($dog_name);
 
	$img_src = $DOCUMENT_ROOT."/dogs/images/thumb_$dog_image";

	
	if($dog_image){
		$src = $DOCUMENT_ROOT.'/dogs/images/'.$dog_image;
		$imageURL='/dogs/images/150-150-'.$dog_image;
	}else{
		$src = $DOCUMENT_ROOT.'/dogs/images/no-photo-t.jpg';
		$imageURL='/dogs/images/no-photo-t.jpg';
	}
	if(file_exists($src)){
	$dest = $DOCUMENT_ROOT.$imageURL;
	createImgThumbIfnot($src,$dest,'150','150','ratiowh');
//$compressed = compress_image($dest,$dest, 60);
 ?>    <div class="pin"><amp-img src="https://www.dogspot.in/dogs/images/150-150-<?=$dog_image?>" width="150" height="150"></amp-img></div><? } }?></div></div>
<? }else{
  $v=0;$cc=0;
 if($breed_id=='636'){
	 $query="dachshund";
 }
 else if($breed_id=='494'){
	 $query="chihuahua";
 }
 else if($breed_id=='58'){
	 $query="fox terrier weired";
 }
  else if($breed_id=='627'){
	 $query="cane";
 }  else if($breed_id=='60'){
	 $query="German Shepherd";
 }  else if($breed_id=='96'){
	 $query="Neapolitan Mastiff";
  } else if($breed_id=='104'){
	 $query="Pit Bull";
 }else if($breed_id=='93'){
	 $query="English Mastiff";
 }
 else if($breed_id=='26'){
	 $query='-"French%20Bull%20Dog"'.'"bulldog"';
 }else if($breed_id=='68'){
	 $query="spitz";
 }
 else{
  	$query=trim($breedname);
 }
	$query = stripslashes(str_replace(" ","%20AND%20",$query));//&sort=no_views desc
	if($breed_nicename=='german-shepherd-dog-alsatian' || $breed_nicename=='golden-retriever'){
		$url = "http://localhost/solr/dogspotphotos/select/?q=$query&wt=xml&mm=0&indent=true&defType=dismax&qf=title%5E2+text%5E1&sort=cdate+desc&start=1&rows=2";
	}else{
$url = "http://localhost/solr/dogspotphotos/select/?q=$query&wt=xml&mm=0&indent=true&defType=dismax&qf=title%5E2+text%5E1&sort=cdate+desc&start=1&rows=4";}
	$result = get_solr_result($url);
	$totrecord = $result['TOTALHITS'];
   if($totrecord > 0){?><div class="be_slideBox">  <div class="be_funImg" id="picturewrap"><amp-img src="https://www.dogspot.in/new/breed_engine/images/gall.png" width="68" height="57"></amp-img></div><h3><?=$be_name ?> Photos from our collection</h3><div id="columns"><? 
	$check_ex=0;
	$count_images='0';
	$check_random_img='0';
	$img_dis='0';
	// check for exhibition id n show id images----------------------------------------------------------------
	foreach($result['HITS'] as $row){
	$selalb=mysql_query("SELECT * FROM photos_album WHERE album_link_name='event' AND album_id='$row[album_id]'");
	$count_alb=mysql_num_rows($selalb);
	if($count_alb > 0){
	$album_id[] = $row["album_id"];
	$selimg=query_execute_row("SELECT image_desc FROM photos_image WHERE image_id='$row[image_id]'");
		if((strpos($selimg['image_desc'], "ex-")!== false) && (strpos($selimg['image_desc'], "Lineup")== '0') && (strpos($selimg['image_desc'], "BIS")== '0') && (strpos($selimg['image_desc'], "BOB")== '0')){
		$Aimage[]=$row["image_id"];
		$arr[]=$row["album_id"]."@@".$row["image_id"]."@@".$row["image_nicename"]."@@".$row["image"];	
		$check_ex=1;$count_images++;
		}

	}
	}
	// if exhibition id n show id images are not found----------------------------------------------------------------
	if($check_ex=='0' || $count_images < 20){	
	foreach($result['HITS'] as $row){
	$selalb=mysql_query("SELECT * FROM photos_album WHERE album_link_name='event' AND album_id='$row[album_id]'");
	$count_alb=mysql_num_rows($selalb);
	if($count_alb > 0){
	$count_images++;
	$album_id[] = $row["album_id"];
	$Aimage[]=$row["image_id"];
	$arr[]=$row["album_id"]."@@".$row["image_id"]."@@".$row["image_nicename"]."@@".$row["image"];	
	}	
	}
	}
    $Aalbum_id=array_unique($album_id);
	asort($Aalbum_id);
	$check_album_count=count($Aalbum_id);
	if($check_album_count<4){
		if($breednice=='rampur-hound'){
		$num_images='4';
		}elseif($breednice=='dogo-argentino')
		{
			$num_images='4';
		}else{
		$num_images='8';
		}
	}elseif($check_album_count==5 && $breednice=='rajapalayam'){
		if($breednice=='rajapalayam'){
		$num_images='4';
		}
	}else{
		$num_images='2';
	}
	//print_r($arr);
	if($count_images < 20){
	$check_random_img_var='1';
	}else{
	$check_random_img_var='2';
	}
	$arr=array_unique($arr);
	foreach($Aalbum_id as $rows){
	foreach($arr as $row1){
			$row11=explode("@@",$row1);
			if($rows==$row11[0]){	
		$check_random_img++;
	 $image_id = $row11[1];
	 $image = $row11[3];
	 $image_nicename = $row11[2];
	 $img_name = "https://www.dogspot.in/photos/images/thumb_$image";
     $cc++;
	 if($check_random_img==$check_random_img_var){
	 $check_random_img='0';
	 $v++;$cc++;	
	 $img_dis++; 
	if(file_exists($img_name)){
		

	$imgWH = @WidthHeightImg($img_name,'150','');
	}else{
		$imgWH[0] = 150;
		$imgWH[1] = 86;
	}
	if(($breednice=='rampur-hound' || $breednice=='mudhol-hound' || $breednice=='weimaraner' || $breednice=='chippiparai' || 
	$breednice=='border-collie' || $breednice=='alaskan-malamute') && $img_dis<='4'){
		if($image!='kanpur-dog-show-2011_156.jpg' && $image !='kanpur-dog-show-2011_155.jpg'){
	 ?><div class="pin"><amp-img role="button"  tabindex="0" src="<?="https://www.dogspot.in/photos/images/thumb_$image"?>" width="101" height="150"></amp-img></div><? }}
if($img_dis<='8' && ($breednice!='rampur-hound' && $breednice!='mudhol-hound' && $breednice!='weimaraner' &&  $breednice!='chippiparai' && 
$breednice!='border-collie' && $breednice!='alaskan-malamute')){
 ?><div class="pin"><amp-img role="button" tabindex="0" src="<?="https://www.dogspot.in/photos/images/thumb_$image"?>" width="101" height="150"></amp-img></div>
      <?  }
			}}if($v==$num_images){
				$v='0';
				break;
			}
}
if($cc=='32'){break;}
}
if($breednice=='border-collie'){
?>
<figure><amp-img role="button" tabindex="0" src="https://www.dogspot.in//photos/images/thumb_bangalore-dog-show_686.jpg" width="101" height="150"></amp-img></figure>
<figure><amp-img role="button" tabindex="0" src="https://www.dogspot.in//photos/images/thumb_bangalore-dog-show_683.jpg" width="101" height="150"></amp-img></figure>
<? } ?></div></div><? }}}?></div></div><? if($breed_nicename!='golden-retriever' && $breed_nicename!='beagle'){?><amp-ad width="100vw" height=320
     type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="1993782992" data-auto-format="rspv"
     data-full-width> <div overflow></div></amp-ad><div class="divider-30 colored"></div><? }?>
 <div class="accordion-container"><header class="text-center h3 heading-white">MAINTENANCE & EFFORT</header> 
<div class="accordion-content"><div class="breed_info">  <?
foreach ($att_id1 as $att_id) {
    if ($att_id == '22' || $att_id == '28' || $att_id == '7'  || $att_id == '34' || $att_id == '16' || $att_id == '17') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') { //if($qRATING['value']!='0' && $qRATING['value']<='9') { $nRating = (($qRATING['value1'] / 1000)*100);
/*            if ($nRating <= 0 || $nRating > 5) {   $finalratng = '0';   $ratvalue   = "zerostar"; } if ($nRating > 0 && $nRating <= 1) {   $finalratng = '1';   $ratvalue   = "rate1"; } if ($nRating > 1 && $nRating <= 2) {   $finalratng = '2';   $ratvalue   = "rate2"; } if ($nRating > 2 && $nRating <= 3) {   $finalratng = '3';   $ratvalue   = "rate3"; } if ($nRating > 3 && $nRating <= 4) {   $finalratng = '4';   $ratvalue   = "rate4"; } if ($nRating > 4 && $nRating <= 5) {   $finalratng = '5';   $ratvalue   = "rate5"; }*/ 
?><ul><li><label><?= $attname['traits']; ?></label></li><li class="no_border"><div class="meter orange nostripes"><span style="width: <?=$nRating?>%"></span></div><span class="fl"><?= $attname['min_value'] ?></span><span class="fr"><?=$attname['max_value'] ?></span> </li></ul><?
        } else { $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?><ul><li><label><?= $attname['traits']; ?><? if($att_id=='11'){echo "<a style='cursor:pointer' id='Average for the breed' title='Average for the breed'>*</a>";} ?></label></li><li class="no_border"><?= $valuename['value']; ?></li></ul><?
        }
    }
}
?></div><?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id ='9'");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
   // echo "SELECT * FROM breed_engine_titles WHERE id='$articletitleid'";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?><strong><?= $selecttitlename['title']; ?></strong>
<? if(($breed_nicename=='german-shepherd-dog-alsatian') || ($breed_nicename=='beagle') || ($breed_nicename=='golden-retriever')){ ?>
<? $regs1='';$result1=''; 
if (preg_match_all('%(<p[^>]*>.*?</p>)%i', htmlspecialchars_decode(stripallslashes($articlecontent)), $regs1)) {
	//print_r($regs1);
	if($breed_nicename=='german-shepherd-dog-alsatian'){
    $result1 = $regs1[0][0];
	$result1.= $regs1[0][1];
	$result1.= $regs1[0][5];
	$result1.= $regs1[0][6];
	}elseif($breed_nicename=='golden-retriever'){
    $result1 = $regs1[0][1];
	$result1.= $regs1[0][2];
	$result1.= $regs1[0][3];
	$result1.= $regs1[0][4];
	}else{$result1 = $regs1[0][0];}
} echo $result1;}else{ echo htmlspecialchars_decode(stripallslashes($articlecontent)); }?><? }?></div></div><div class="accordion-container"><header class="text-center h3 heading-white">HAIR & COAT</header><div class="accordion-content"><div class="breed_info"><?
foreach ($att_id1 as $att_id) {
    if ($att_id == '24' || $att_id == '25' || $att_id == '9' || $att_id == '10' || $att_id == '46') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>  <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') { //if($qRATING['value']!='0' && $qRATING['value']<='9') { $nRating = (($qRATING['value1'] / 1000)*100);
/*            if ($nRating <= 0 || $nRating > 5) {   $finalratng = '0';   $ratvalue   = "zerostar"; } if ($nRating > 0 && $nRating <= 1) {   $finalratng = '1';   $ratvalue   = "rate1"; } if ($nRating > 1 && $nRating <= 2) {   $finalratng = '2';   $ratvalue   = "rate2"; } if ($nRating > 2 && $nRating <= 3) {   $finalratng = '3';                $ratvalue   = "rate3"; } if ($nRating > 3 && $nRating <= 4) {   $finalratng = '4';   $ratvalue   = "rate4"; } if ($nRating > 4 && $nRating <= 5) {   $finalratng = '5';   $ratvalue   = "rate5"; }*/ 
?><ul><li><label><?= $attname['traits'] ?></label></li><li class="no_border"><div class="meter orange nostripes"><span style="width: <?=$nRating?>%"></span></div><span class="fl"><?= $attname['min_value'] ?></span><span class="fr"><?=$attname['max_value'] ?></span> </li></ul><?
        } else { $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?><ul><li><label><?= $attname['traits'] ?></label></li><li class="no_border"><?= $valuename['value'] ?></li></ul><?
        }
    }
}
?></div></div></div><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="5144837740" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div><div class="accordion-container"><header class="text-center h3 heading-white">HEALTH</header><div class="accordion-content"><div class="breed_info">
<?
foreach ($att_id1 as $att_id) {
    if ($att_id == '40' || $att_id == '47') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");     
?>  <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        if ($att_id <= '20') { //if($qRATING['value']!='0' && $qRATING['value']<='9') { $nRating = (($qRATING['value1'] / 1000)*100);
/*            if ($nRating <= 0 || $nRating > 5) {   $finalratng = '0';   $ratvalue   = "zerostar"; } if ($nRating > 0 && $nRating <= 1) {   $finalratng = '1';   $ratvalue   = "rate1"; } if ($nRating > 1 && $nRating <= 2) {   $finalratng = '2';   $ratvalue   = "rate2"; } if ($nRating > 2 && $nRating <= 3) {   $finalratng = '3';   $ratvalue   = "rate3"; } if ($nRating > 3 && $nRating <= 4) {   $finalratng = '4';   $ratvalue   = "rate4"; } if ($nRating > 4 && $nRating <= 5) {   $finalratng = '5';   $ratvalue   = "rate5"; }*/ 
?><ul><li><label><?= $attname['traits'] ?></label></li><li class="no_border"><div class="meter orange nostripes"><span style="width: <?=$nRating?>%"></span></div><span class="fl"><?= $attname['min_value'] ?></span><span class="fr"><?=$attname['max_value'] ?></span> </li></ul><?
        } else { $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?><ul><li><label><?= $attname['traits'] ?></label></li><li class="no_border"><?= $valuename['value'] ?></li></ul><?
        }
    }
}
?></div><span class="be_detailTxtIntro"><?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id ='8'");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?><h3><?=htmlspecialchars_decode(stripallslashes($selecttitlename['title'])); ?></h3>
<? if(($breed_nicename=='german-shepherd-dog-alsatian') || ($breed_nicename=='beagle') || ($breed_nicename=='golden-retriever')){ ?>
<? $regs='';$result=''; if (preg_match_all('%(<p[^>]*>.*?</p>)%i', htmlspecialchars_decode(stripallslashes($articlecontent)), $regs)) {
	 if($breed_nicename!='beagle') {
    $result = $regs[0][0];
	$result .= $regs[0][1];
	 }else{
		 $result = $regs[0][1];
	//$result .= $regs[0][1]; 
		 }
	// print_r($regs);
} echo $result;}else{ echo htmlspecialchars_decode(stripallslashes($articlecontent)); }?><?
}
?></span></div></div><? if($breed_nicename!='beagle' && ($breed_nicename!='golden-retriever')){?><amp-ad width="100vw" height=320
     type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="9630877664" data-auto-format="rspv" data-full-width>
  <div overflow></div></amp-ad><? }?><div class="accordion-container"><header class="text-center h3 heading-white">BEHAVIOR</header><div class="accordion-content"><div class="breed_info"><?
   foreach ($att_id1 as $att_id){
	   if ($att_id == '21' ||  $att_id == '36') {
		   $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
		    $qRATING = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
		   $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?><ul><li><label><?= $attname['traits']; ?></label></li><li class="no_border"><?= htmlspecialchars_decode($valuename['value']); ?>    </li></ul><?
        }
   }
foreach ($att_id1 as $att_id) {
 if ($att_id == '1' || $att_id == '2'  || $att_id == '4' || $att_id == '5' || $att_id == '18' || $att_id == '33' || $att_id == '19' 
 || $att_id == '12' || $att_id == '13' || $att_id == '43' || $att_id == '44') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>  <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
       if($breed_nicename!='beagle'){ 
        if ($att_id <= '20' || $att_id == '43' || $att_id == '44' || $att_id == '33') { //if($qRATING['value']!='0' && $qRATING['value']<='9') { $nRating = (($qRATING['value1'] / 1000)*100);
        /*    if ($nRating <= 0 || $nRating > 5) {   $finalratng = '0';   $ratvalue   = "zerostar"; } if ($nRating > 0 && $nRating <= 1) {   $finalratng = '1';   $ratvalue   = "rate1"; } if ($nRating > 1 && $nRating <= 2) {   $finalratng = '2';   $ratvalue   = "rate2"; } if ($nRating > 2 && $nRating <= 3) {   $finalratng = '3';   $ratvalue   = "rate3"; } if ($nRating > 3 && $nRating <= 4) {   $finalratng = '4';   $ratvalue   = "rate4"; } if ($nRating > 4 && $nRating <= 5) {   $finalratng = '5';   $ratvalue   = "rate5"; }*/ 
?><ul><li><label><?= $attname['traits'] ?></label></li><li class="no_border"><div class="meter orange nostripes"> <span style="width: <?=$nRating?>%"></span></div><span class="fl"><?= $attname['min_value'] ?></span> <span class="fr"><?=$attname['max_value'] ?></span> </li></ul><?
  } else { $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?><ul><li><label><?= $attname['traits']; ?></label></li><li class="no_border"><?= htmlspecialchars_decode($valuename['value']); ?>    </li></ul><?
        }
    }
}}
?></div><span class="be_detailTxtIntro"><?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (6,11,10)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?><h3><?= $selecttitlename['title']; ?></h3>
<? if(($breed_nicename=='german-shepherd-dog-alsatian') || ($breed_nicename=='beagle') || ($breed_nicename=='belgian-malinois') || ($breed_nicename=='golden-retriever')){ ?>
<? $regs='';$result='';$result1=''; 
if(preg_match_all('%(<p[^>]*>.*?</p>)%i', htmlspecialchars_decode(stripallslashes($articlecontent)), $regs)) {
	$result = $regs[0][0];
	if($breed_nicename!='beagle' && $breed_nicename!='golden-retriever'){
	$result .= $regs[0][1];
	}
} echo $result;}else{echo htmlspecialchars_decode(stripallslashes($articlecontent));} ?><?  }?></span></div></div><? if($breed_nicename!='beagle'){?> <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="1993782992"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? }?><div class="accordion-container"> <header class="text-center h3 heading-white">APPEARANCE</header><div class="accordion-content"><div class="breed_info"><?
foreach ($att_id1 as $att_id) {
    if ($att_id == '8' || $att_id == '6' || $att_id == '29' || $att_id == '31' || $att_id == '42' || $att_id == '48' || $att_id == '23') {
        $attname = query_execute_row("SELECT * FROM breed_engine_traits WHERE id='$att_id'");
        
?>  <?
        $qRATING = query_execute_row("SELECT value,value1 FROM breed_engine_values WHERE breed_id='$breed_id' AND att_id='$att_id'");
        
        if ($att_id <= '20') { //if($qRATING['value']!='0' && $qRATING['value']<='9') { $nRating = (($qRATING['value1'] / 1000)*100);
/*            if ($nRating <= 0 || $nRating > 5) {   $finalratng = '0';   $ratvalue   = "zerostar"; } if ($nRating > 0 && $nRating <= 1) {   $finalratng = '1';   $ratvalue   = "rate1"; } if ($nRating > 1 && $nRating <= 2) {   $finalratng = '2';   $ratvalue   = "rate2"; } if ($nRating > 2 && $nRating <= 3) {   $finalratng = '3';   $ratvalue   = "rate3"; } if ($nRating > 3 && $nRating <= 4) {   $finalratng = '4';   $ratvalue   = "rate4"; } if ($nRating > 4 && $nRating <= 5) {   $finalratng = '5';   $ratvalue   = "rate5"; }*/ 
?><ul><li><label><?= $attname['traits'] ?></label></li><li class="no_border"><div class="meter orange nostripes"><span style="width: <?=$nRating?>%"></span></div><span class="fl"><?= $attname['min_value'] ?></span> <span class="fr"><?=$attname['max_value'] ?></span> </li></ul><?
        } else { $valuename = query_execute_row("SELECT value FROM breed_engine_att_att WHERE att_att_id='$qRATING[value]'");
?><ul><li><label><?= $attname['traits']; ?></label></li><li class="no_border"><?= htmlspecialchars_decode($valuename['value']); ?>    </li></ul><?
        }
    }
}
?></div><? if($breed_nicename!='beagle'){?><span class="be_detailTxtIntro"><?
$selectarticle = mysql_query("SELECT * FROM breed_engine_titles_data WHERE breed_id='$breed_id' AND title_id in (14)");
while ($selectarticle1 = mysql_fetch_array($selectarticle)) {
    $articlecontent  = $selectarticle1['data'];
    $articletitleid  = $selectarticle1['title_id'];
    // echo $articletitleid."<br>";
    $selecttitlename = query_execute_row("SELECT * FROM breed_engine_titles WHERE id='$articletitleid'");
?><h3><?= $selecttitlename['title']; ?></h3>
<? if(($breed_nicename=='german-shepherd-dog-alsatian') || ($breed_nicename=='golden-retriever')){ ?>
<? $regs='';$regs1='';$result='';$result1=''; 
if(preg_match_all('%(<p[^>]*>.*?</p>)%i', htmlspecialchars_decode(stripallslashes($articlecontent)), $regs)) {
	$result = $regs[0][0];
	$result .= $regs[0][1];
	} echo $result;}else{ echo htmlspecialchars_decode(stripallslashes($articlecontent)); } ?><?
}?></span><? }?> </div></div></div></div></div></div></div><? include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>