<?php 
   require_once($DOCUMENT_ROOT.'/database.php');
   require_once($DOCUMENT_ROOT.'/home-functions.php');
   require_once($DOCUMENT_ROOT . '/session.php');
$sitesection = 'HOME';
 ?><!doctype html>
<html AMP lang="en">
 <head>
    <meta charset="utf-8">
    <title>DogSpot.in-Online Pet Supplies Store | Shop for Dog,Cat,Birds Products</title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="DogSpot.in Pet Supplies Store - India's best online shopping site for pets supplies, accessories & products for pets. Shop for Dog, Cat, Birds and small pets at great price from anywhere." />
    <meta name="keywords" content="DogSpot.in, Online Pet Supplies Store, Dog Store, Cat shop, Birds products, pet shopping India, puppies shop, cat supplies, dog shop online." />
    <link rel="canonical" href="https://www.dogspot.in/" />
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
  <? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
   <? $tdate=date("Y-m-d H:i:s");
$sql = mysql_query("SELECT img,banner_name,banner_link FROM banners WHERE featured_on='mobile-app' AND width='1080' AND height='419' AND start_date <= '$tdate' AND end_date > '$tdate' AND img!='' AND status='active' order by start_date DESC");
?><div class="container-fluid container-full" style="margin-bottom: -34px;">
		<div class="row">
			<div class="col-xs-12">
  				<amp-carousel width="690" height="471" layout="responsive" type="slides" autoplay delay="5000">
                      <?	while($rowCatData=mysql_fetch_array($sql)){
						  $destB=$DOCUMENT_ROOT."/allbanners/".$rowCatData["img"];
						  $image_info2   = getimagesize($destB);
                    $image_width  = $image_info2[0];
                    $image_height = $image_info2[1];?>
		<a href="<?=$rowCatData["banner_link"]?>/">           
				<amp-img srcset="/allbanners/<?=$rowCatData["img"]?>" width="<?=$image_width?>" height="<?=$image_height?>" layout="responsive" alt="<?=$rowCatData["banner_name"]?>"></amp-img></a>
                             <? }?>
				</amp-carousel>
			</div></div>
	</div><div class="container-fluid container-full" ><div class="row"><div class="col-xs-12"><div class="blogger_sec"><h4> Top Bloggers At <span>DogSpot</span></h4><div class="bloggers"><ul><li><a href="/dog-blog/author/rana_vr1/" aria-label="Rana"><figure><amp-img src="/images/rana-atheya-blogger.jpg"width="110" height="110" alt="Rana"></amp-img></figure><figcaption>Rana Atheya</figcaption></a></li><li><a href="/dog-blog/author/neha.manchanda/" aria-label="Neha"><figure><amp-img src="/images/neha-manchanda-bloggers.jpg"  width="110" height="110" alt="Neha"></amp-img></figure><figcaption>Neha Manchanda</figcaption></a></li><li><a href="/dog-blog/author/adnan-khan/" aria-label="Adnan"><figure><amp-img src="/images/adnan-khan-blogger.jpg"  width="110" height="110" alt="Adnan"></amp-img></figure><figcaption>Adnan Khan</figcaption></a>
</li><li><a href="/dog-blog/author/krithika1/" aria-label="kritika"><figure><amp-img src="/images/kritika-kumar.png" width="110" height="110" alt="kritika"></amp-img></figure><figcaption>Krithika</figcaption></a></li><li><a href="/dog-blog/author/228573/" aria-label="Dipshikha"><figure><amp-img src="/images/dipshika-thalani.png"  width="110" height="110" alt="Dipshikha"></amp-img></figure><figcaption>Deepshikha</figcaption></a></li><li><a href="/dog-blog/author/ritwikmishra/" aria-label="ritwik"><figure><amp-img src="/images/ritwik-mihsra-blogger.png"  width="110" height="110" alt="ritwik"></amp-img></figure><figcaption>Ritwik Mishra</figcaption></a></li></ul></div></div></div></div></div><div class="container-fluid container-full" style="margin-top: -20px;"><div class="breed-selection"><a href="/amp/dog-breeds/?utm_source=website&amp;utm_medium=dog-breeds-home-banner&amp;utm_campaign=banner" 
aria-label="Dog Breeds"><amp-img src="/images/dog-breeds-banner.jpg" width="480" height="60" layout="responsive" alt="Dog Breeds"></amp-img></a></div><div class="cat-section"><div class="cat-gallery" style="margin-top: -20px;"><ul><li><a href="/amp/beagle/"><amp-img src="https://www.dogspot.in/new/images/beagle-home.jpg" alt="beagle" title="beagle" width="100" height="100"><div>Beagle</div></a> </li>
<li><a href="/amp/labrador-retriever/"> <amp-img src="https://www.dogspot.in/new/images/lebrador-home.jpg" alt="Labrador Retriever" title="Labrador Retriever" width="100" height="100"><div style="margin-left: 5px;">Labrador Retriever</div></a></li><li><a  href="/amp/german-shepherd-dog-alsatian/"><amp-img src="https://www.dogspot.in/new/images/german-home.jpg" alt="German Shepherd" title="German Shepherd" width="100" height="100"><div>German Shepherd</div></a></li><li> <a href="/amp/golden-retriever/"> <amp-img src="https://www.dogspot.in/new/images/golden-home.jpg" alt="Golden Retriever" title="Golden Retriever" width="100" height="100">
<div>Golden Retriever</div></a></li><li><a href="/amp/pug/"> <amp-img src="https://www.dogspot.in/new/images/pug-home.jpg" alt="Pug" title="Pug" width="100" height="100"><div>Pug</div></a></li></ul></div></div></div><div class="container-fluid container-full">
<div class="row"><div class="col-xs-12"><a href="/amp/touchdog/?utm_source=website&amp;utm_medium=dog-bowls-home-banner&amp;utm_campaign=banner" aria-label="Touch"><amp-img src="/images/tou-dd-bbb.jpg"  width="1020" height="311" layout="responsive" alt="Touch"></amp-img></a></div></div></div><div class="container-fluid container-full"><div class="row"><div class="col-xs-12"><div class="hot_selling_sec"><h1>Hot Selling Products</h1><div class="hot_selling_carousel"><ul>
    <?  $item_idSell=hotSelling();
	   foreach($item_idSell as $item){$qItemq=query_execute_row("SELECT item_id,name,stock_status,nice_name,price,selling_price,item_parent_id FROM shop_items WHERE item_id='$item'");
					$item_id=$qItemq["item_id"];
					$name=$qItemq["name"];
					$nice_name=$qItemq["nice_name"];
					$price=number_format($qItemq["price"],0);
					$selling_price=number_format($qItemq["selling_price"],0);
					$discountPer=number_format(($selling_price-$price)/$selling_price*100,0);	
					$item_parent_id=$qItemq["item_parent_id"];
					$qdataM1=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	 				$count=mysql_num_rows($qdataM1);
	 				if($count>0)
	 				{
	  				$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");	 
	 				}else{
					if($item_parent_id == '0'){
			   	    $qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC LIMIT 1");
	   				}else{		
					$qdataM=mysql_query("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_parent_id' ORDER BY position ASC LIMIT 1");
					}
	 			}
				$rowdatM = mysql_fetch_array($qdataM);
				$src = $DOCUMENT_ROOT.'/shop/item-images/orignal/'.$rowdatM["media_file"];
			    $imageURL = '/imgthumb/130x130-'.$rowdatM["media_file"];
			    $dest = $DOCUMENT_ROOT.$imageURL;
		        createImgThumbIfnot($src,$dest,'130','130','ratiowh');
				 $image_info   = getimagesize($dest);
                    $image_width  = $image_info[0];
                    $image_height = $image_info[1];
		   
		   ?><li><div class="p-slide-img"><a href="/amp/<?=$nice_name?>/" aria-label="<?=$name?>"><amp-img src="<?=$imageURL?>" class="p-s-image" width="<?=$image_width?>" height="<?=$image_height?>" alt="<?=$name?>"></amp-img></a></div><div class="product-slider-link"><a href="/amp/<?=$nice_name?>/" aria-label="<?=$name?>"><?=$name?></a></div><div class="product-price-s"> <span class="p-ds-off"><i class="fa fa-rupee"></i><?=$price?></span><span class="p-d-price"><? if($selling_price>$price){?><del><i class="fa fa-rupee"></i><?=$selling_price?></del></span> <span class="p-d-off"><?=$discountPer?>%</span><? }?></div></li><? }?></ul></div></div></div></div></div><amp-ad style='margin-top:10px;margin-bottom:10px;' type="doubleclick" width=300 height=250 data-slot="/21630298032/Mobile_300x250_home"><div placeholder></div><div fallback></div></amp-ad><div class="container-fluid container-full"><div class="row"><div class="col-xs-12"><amp-img src="/images/cat-banner-home-new.jpg" width="1020" height="312" layout="responsive" alt="Cat banner"></amp-img></div></div></div>
           <?php /*?><div style="float: left;width: 100%;text-align: center;"> <div style="color:#333;font-size: 26px;font-weight: 500;">DogSpot Trusted Partners</div>
        <ul style="border: 1px solid #eee;float: left;width: 100%;padding: 0;margin:10px auto;
    list-style-type: none;">
          <a href="https://www.petshopdelhi.in/" target="_blank">
          <li style="width:100%;text-align:center;color:#000;"><amp-img style="margin-left: auto;margin-right: auto;display: block;" src="https://www.dogspot.in/new/images/doggy-world-new.jpg" alt=" Doggy World" width="250" height="150" ></amp-img></li><li><div style="font-size: 16px;font-weight: 300;margin-bottom: 10px;"> Doggy World</div><div style="margin-left:10px;">Doggy World provides unmatched care & service for your lovable pet </div><div style="color:#F30">Veterinary Hospital, Pet Grooming and Pet Shop</div><div> <amp-img src="https://www.dogspot.in/new/images/map-icons.jpg" alt="Map Icons" title="Map Icons" width="20" height="29" style="vertical-align:middle; max-width:14px; height:auto; margin-right:2px;" ></amp-img>B-6/147-148, Sector - 8, Rohini, Delhi</div></li><li style="display:inline-block;"><div style="text-align:center;margin:5px;float:left;"> <amp-img style="margin-left: auto;margin-right: auto;display: block;" src="https://www.dogspot.in/new/images/aradhana.jpg" height="128" width="122"  alt="Dr. Aradhana Pandey"></amp-img><div class="dr-name">Dr. Aradhana Pandey</div></div><div style="text-align:center;margin:5px;float:left;"> <amp-img style="margin-left: auto;margin-right: auto;display: block;" src="https://www.dogspot.in/new/images/sk-panday.jpg" height="128" width="122"  alt="Dr. Aradhana Pandey"></amp-img><div class="dr-name">Dr. S. K. Pandey</div></div></li></a></ul></div><?php */?>
<div class="container-fluid" style="margin-left:10px;margin-right: 10px;"><div class="row">
	<div class="col-xs-12">
	<div class="petstories-h-sec">
       <h2>PET STORIES</h2>
     <? 
  header("Content-type: text/html; charset=iso-8859-1");
$selectTqna = query_execute("SELECT DISTINCT(wpp.ID) , wpp.post_content , wpp.post_title , wpp.post_name FROM wp_posts as wpp , wp_term_relationships as wptr ,wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wptr.object_id=wpp.ID AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' GROUP BY wpp.ID ORDER BY wpp.post_date DESC LIMIT 1");
   $rowTqna = mysql_fetch_array($selectTqna);
	$art_subject   = stripallslashes($rowTqna["post_title"]);
    $art_body      = stripallslashes($rowTqna["post_content"]);
    $art_name      = $rowTqna["post_name"];
	 $feat_imgURL    = get_first_image($art_body, $DOCUMENT_ROOT);
     $imgURLAbs = make_absolute($feat_imgURL, 'https://www.dogspot.in');
    $art_subject1 = substr($art_subject,0,40);
	  // Get Post Teaser
	 if(strlen($art_subject)>40)
	 {
	$art_subject1=$art_subject1.'..';	 
	 }
    $art_body = htmlspecialchars_decode(strip_tags($art_body));
    $art_body = substr($art_body, 0, 160);
   //------------------Database change
	//Check for featured image--------------------------------------------------
	$check_img  = query_execute_row("SELECT guid FROM wp_posts WHERE post_parent='$article_id' AND post_type='attachment' AND post_mime_type like '%image%' ORDER BY ID ASC");
	//--------------------ends----------
	$rowUser  = query_execute_row("SELECT f_name, image FROM users WHERE userid='$userid_artdb'");
		if (strpos($imgURLAbs,'wordpress') !== false) {
    	$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/470x287-'.$image_name;
		}else if($check_img['guid'] !=''){
		$imgURLAbs=$check_img['guid'];	
		$exp=explode("https://www.dogspot.in",$imgURLAbs);
		$src = $exp[1];
		$pos = strrpos($imgURLAbs,'/');
	    $image_name = substr($imgURLAbs,$pos+1);
		$imageURL='/imgthumb/470x287-'.$image_name;
		}
		else if ($imgURLAbs!='/new/pix/dogspot-logo-beta.gif') {
		$URL=$imgURLAbs;
	   $image_name = (stristr($URL,'?',true))?stristr($URL,'?',true):$URL;
	   $pos = strrpos($image_name,'/');
	   $image_name = substr($image_name,$pos+1);
		$extension = stristr($image_name,'.');
		if($extension == '.jpg' || $extension == '.png' || $extension == '.gif' || $extension == '.jpeg'){
		$image_name;
		}
		$src = '/userfiles/images/'.$image_name;
		$imageURL='/imgthumb/470x287-'.$image_name;
		} 
		else if($rowUser["image"]) {
		$src ='/profile/images/'.$rowUser["image"];
		$imageURL='/imgthumb/470x287-'.$image_name;
		} 
		else  {
        $src ='/images/noimg.gif';
		$imageURL = '/imgthumb/470x287-noimg.gif';
		}
		$image_info1   = getimagesize($DOCUMENT_ROOT.$imageURL);
                    $image_width  = $image_info1[0];
                    $image_height = $image_info1[1];?><div class="article-det-sec"><a href="/amp/<?=$art_name?>/">
       <amp-img  src="<?=$imageURL;?>" layout="responsive" width="<?=$image_width?>" height="<?=$image_height?>" alt="<?=$art_subject1?>" ></amp-img>
	   <h3><?=$art_subject1?></h3>
       <p><?=$art_body;?></p></a></div></div></div></div></div>
   <? include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>