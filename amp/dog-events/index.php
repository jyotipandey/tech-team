<?php
include("../constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?><!doctype html>
<html AMP lang="en">
<head><meta charset="utf-8">
<title>Dog Show | Dog Events | Dog Championship | Kennel Club India</title>
<meta name="keywords" content="Dog Show, Dog Events, Dog Championship, dog show albums, KCI, Kennel Club India events." />
<meta name="description" content="Find the Dog show results, online show entry details, recent articles on dogs as well as old events albums." />
<link rel="canonical" href="https://www.dogspot.in/dog-events/" />
<?php require_once($DOCUMENT_ROOT .'/new/common/top-2-amp.php'); ?>
<div class="dog-show-sec"><div class="dog-show-banner" style="text-align: center;"><amp-img src="/dog-events/images/dog-show-banner.jpg" title="Dog Show Banner" alt="Dog Show Banner" style="width:100%" height="200" width="319px"></amp-img></div><div class="dogshow_nav_header" id="dogshow_nav_header"><div id="wrapper" class="clearfix"><div id="ds_top_nav"><ul id="nav"><li><a href="/amp/dog-events/about-us/" >About us</a> </li><li style="margin-left: -3%;"><a href="/amp/dog-events/show-schedules/">Show schedules</a></li><li style="margin-left: -3%;margin-right: -24%;"><a href="/amp/show-results/" >Show results</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="box_list">
  <div class="box_show_title">Show Results</div>
  <ul style="margin:0px 2%;">
    <?
$selectShow = mysql_query("SELECT * FROM show_description WHERE show_id != '2' AND show_id != '123' AND show_id != '124' ORDER BY date DESC LIMIT 55");
 if(!$selectShow){	die(mysql_error());	}
	$dit=0;
 while($rowShow = mysql_fetch_array($selectShow)){
	$show_id = $rowShow["show_id"];
	$show_name = $rowShow["show_name"];
	$show_nicename = $rowShow["show_nicename"];
	$show_desc = $rowShow["show_desc"];
	$location = $rowShow["location"];
	$date = $rowShow["date"];
	$rowAlbum = query_execute_row("SELECT album_id, album_nicename FROM photos_album WHERE album_link_id = '$show_id'");
	$album_id = $rowAlbum["album_id"]; 
 	$album_nicename = $rowAlbum["album_nicename"];
	
	 $rowPri = query_execute_row("SELECT * FROM show_ring WHERE show_id = '$show_id'");
	 $ring_id = $rowPri["ring_id"];
	 
	 if($ring_id){ 
	 	$dit=$dit+1;
?><li><? if($ring_id ){?><a id="" href="/amp/<?=$show_nicename?>/" style="color:#162E44;" title="<?=$show_name?> Dog Show Results" >
      <?=$show_name?>, <span class="date"><? print(showdate($date, "d M Y")); ?></span></a><? }?></li>
    <? }if($dit==5){break;}}?><li> <a href="/amp/show-results/" class="view-more">view more &raquo;</a></li>
  </ul>
</div>
<div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250 type="doubleclick" data-slot="/21630298032/Puppies"><div placeholder></div><div fallback></div></amp-ad></div>
<div id="box_news" >
<?
						$selectMC = mysql_query("SELECT * FROM articles WHERE (art_body like '%dog shows%' OR art_subject like '%dog shows%') AND publish_status='publish' ORDER BY c_date DESC limit 2");
						
					  if(!$selectMC){	die(mysql_error());	} ?>
<? $i=0;
							  while($rowArt = mysql_fetch_array($selectMC)){
								$i=$i+1;  
								$articlecat_id = $rowArt["articlecat_id"];
								$article_id = $rowArt["article_id"];
								$art_subject = $rowArt["art_subject"];
								$art_body = $rowArt["art_body"];
								$c_date = $rowArt["c_date"];
								$artuser = $rowArt["userid"];
								$art_name = $rowArt["art_name"];
								  
						$art_subject = stripslashes($art_subject);
						$art_subject = breakLongWords($art_subject, 12, " ");
					
					// Get Post Teaser
						$art_body = stripslashes($art_body);
						$art_body = strip_tags($art_body);
						$art_body = trim($art_body);
						$art_body = substr($art_body,0,100);
					
						$art_body = stripslashes($art_body);
						$art_body = breakLongWords($art_body, 30, " ");
					 
					  ?><div class="dog_show_recentNews"><p class="recent_news_title"><a href="<? echo"/amp/$art_name/";?>"><? echo"$art_subject"; ?></a></p><p><? echo"$art_body...";?><? echo " <a href='/amp/$art_name/' class='recent_news_readmore'> Read More</a>"; ?></p></div><? } ?></div>
<div  id="box_show" class="margin_left">
<div class="recent_show_albem">
  <h2>Recent Albums </h2>
</div>
<?  $iid=0;?><div class="dog-show-list">
    <?php
$qItem1=query_execute("SELECT album_name,album_id, album_nicename, cdate, album_link_name, album_link_id FROM photos_album
WHERE  album_link_name ='event' order by cdate desc limit 0,30");
$div_count=0;
while($rowItem = mysql_fetch_array($qItem1)){
				$iid++;$div_count++;
	$album_id=$rowItem["album_id"];
	$album_link_name=$rowItem["album_link_name"];
	$album_name=$rowItem["album_name"];
	$nice_name=$rowItem["album_nicename"];
	$qdataM12=query_execute_row("SELECT cover_img FROM photos_image where album_id='$album_id' AND cover_img =  '1'");
	if($qdataM12) {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id' AND cover_img='1'");
	}
	else {
	$qdataM=query_execute_row("SELECT image,image_nicename,title, date(cdate) as cdate FROM photos_image where album_id='$album_id'");	
	}
	$sqldatsh=query_execute_row("SELECT date FROM show_description WHERE show_id='".$rowItem['album_link_id']."'");
		$image=$qdataM["image"];
		$imagenice_name=$qdataM["image_nicename"];
		$title=$qdataM["title"];
		if($sqldatsh['date']!=''){ $date=showdate($sqldatsh["date"], "d M o"); }else{ $date=showdate($qdataM["cdate"], "d M o");}
	if($image){
	$src = $DOCUMENT_ROOT.'/photos/images/'.$image;
	$destm = $DOCUMENT_ROOT.'/imgthumb/147x98-'.$image;
	//echo $src;
	
	createImgThumbIfnot($src,$destm,'147','98','ratiowh');
 $image_info   = getimagesize($destm);
                    $image_width  = $image_info[0];
                    $image_height = $image_info[1];
 ?><div class="dogshows_wrapperBox"><div class="dog_shows_thumbnail"> <a href="/amp/photos/album/<?=$nice_name?>/" target="_blank"><amp-img src="https://www.dogspot.in/imgthumb/200x134-<?=$image?>" alt="<?=$title?>" title="<?=$title?>" height="<?= $image_height?>" width="<?= $image_width?>"></amp-img></a></div><div class="dog_shows_title"><p class="dog_name"><a href="/amp/photos/album/<?=$nice_name?>/"  target="_blank"><? echo snippetwop($album_name, 26, '');?></a></p>
        <?
 $qcount= query_execute_row("SELECT count(*) as image from photos_image where album_id='$album_id'");?>
        <p class="dog_date"><? echo $qcount["image"]." "."Photos";?> |<?=$date?></p></div></div>
    <? if($div_count=='2'){ 
			$div_count='0';
			?>
  </div><div class="dog-show-list"><? if($iid==8) {?>
  <div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250 type="doubleclick" data-slot="/21630298032/Puppies"><div placeholder></div><div fallback></div></amp-ad></div><? }?> 
  
<? }}}?></div></div></div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-amp.php'); ?>
</body></html>

