<?php
include("/constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
?><!doctype html>
<html AMP lang="en">
<head><meta charset="utf-8">
<title>Dog Show About Us | Dog Show Details </title>
<meta name="keywords" content="Dog Show About Us,Dog Show Details" />
<meta name="description" content="Dog Show About us Information. Check More About DogSpot Show Sction." />
<link rel="canonical" href="https://www.dogspot.in/dog-events/about-us/" />
<?php require_once($DOCUMENT_ROOT .'/new/common/top-2-amp.php'); ?>
<div class="dog-show-sec">
  <div class="dog-show-banner" style="text-align: center;"><amp-img src="https://www.dogspot.in/dog-show/Images/photos/about-us-dog.jpg" alt="About Us" height="76" width="320"></amp-img> </div><div id="showsechedule_text"><h1  style="color:#333;">About Dog Show</h1></div><div class="dogshow_nav_header" id="dogshow_nav_header">
    <div id="wrapper" class="clearfix"><div id="ds_top_nav"><ul id="nav"><li><a href="/amp/dog-events/about-us/" >About us</a> </li><li style="margin-left: -3%;"><a href="/amp/dog-events/show-schedules/">Show schedules</a></li><li style="margin-left: -3%;margin-right: -24%;"><a href="/amp/show-results/" >Show results</a></li></ul></div></div></div></div><div id="dogshow_filter_div">
<div class="open_detailBox_dg"><amp-accordion class="accordion"><section expanded>
                        <header class="accordion-title h3">About us</header>
<div class="ui-collapsible" style="font-weight: 500;font-size: 14px;"><p>
      DogSpot, since its inception in 2007 as a blogging website, has been a synonymous name with the Dog Show community.  The operations evolved towards the e-commerce dimension in 2011, with the initial focus of the entire blogging and commercial concept being targeted towards the show circles of breeders and judges which can be credited towards being the initial influencers of the setup and success.</p><p>In some instances, it may appear that you are paying more [dependent upon entry] but if you take into consideration the increased profit on catalogue sales you will find that in real terms our costs have not increased.</p><p>DogSpot owes as much to the national and regional kennel clubs for their growth as the committees do to DogSpot for their development. Till date, DogSpot handles the offline information systems such as data entry, photo albums and printing of certificates for various Kennel Clubs pan-India.<br>
      </p></div></section></amp-accordion>
   
      </div>
 <div class="open_detailBox_dg"><amp-accordion class="accordion"><section><header class="accordion-title h3">What We Do</header>
<div class="ui-collapsible" style="font-weight: 500;font-size: 14px;"> <div class="intro">DogSpot is elated to announce a new and advanced section for ‘Dog Shows’. Here in addition, to the prevalent facilities (photography, results, certificates) we are introducing a plethora of new features to make the administrative process of Dog Shows simpler and digitally accessible.</div>
      <p>The services provided are as follows: </p>
      <ol style="list-style-type: none;">
        <li>&#8226;Register Your Dog for a show online</li>
        <li>&#8226;Organize a dog show</li>
        <li>&#8226;Keep up to date with dog show results and pictures</li>
        <li>&#8226;Know the winners and connect via Wag Club</li>
      </ol>
      <p><strong>Furthermore, we also provide some additional services to the kennel clubs such as: </strong></p>
      <ol style="list-style-type: none;">
        <li>&#8226;Printing and presenting of show certificates</li>
        <li>&#8226;Printing and submitting of the ‘show return’ to KCI of every dog show</li>
        <li>&#8226;Information about Kennel Clubs and respective breed standards</li>
        <li>&#8226;SMS updates to confirm new registration as well as event and venue details a day before the dog show</li>
      </ol>
      <p>In addition to the primary features listed above. Users can also check out the history and profiles of the following: </p>
      <ol style="list-style-type: none;">
        <li>&#8226;Breeder</li>
        <li>&#8226;Show Judge</li>
        <li>&#8226;Ring Steward</li>
        <li>&#8226;Participant and Winner Dogs (via Wag Club)</li>
      </ol></div></section></amp-accordion>
   
      </div></div>
<?php require_once($DOCUMENT_ROOT .'/new/common/bottom-amp.php'); ?>
<!-- cart page start--> 

