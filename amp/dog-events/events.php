<?php
include("../constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$sitesection = "dog-events";
$date_session_array=''; 
?><!doctype html>
<html AMP lang="en">
<head><meta charset="utf-8">
<title>Dog Show, Dog Events, Dog Championship, KCI | Dog Spot</title>
<meta name="keywords" content="Dog Show, Dog Events, Dog Championship, KCI, Dog Spot" />
<meta name="description" content="Dog Show, Dog Events, Dog Championship, KCI, Dog Spot" />
<link rel="canonical" href="https://www.dogspot.in/dog-events/" />
<?php require_once($DOCUMENT_ROOT .'/new/common/top-2-amp.php'); ?>
<div class="dog-show-sec"><div class="dog-show-banner"> <img src="/dog-events/images/show-schedules.jpg" alt="Show Secheduls"  height="200px" width="319px"></div><div id="showsechedule_text"><h1>Shows Schedules</h1></div><div class="dogshow_nav_header" id="dogshow_nav_header"><div id="wrapper" class="clearfix"><div id="ds_top_nav"><ul id="nav"><li><a href="/amp/dog-events/about-us/" class="butt_2">About us</a> </li><li style="margin-left: -3%;"><a href="/amp/dog-events/show-schedules/">Show schedules</a></li><li style="margin-left: -3%;margin-right: -24%;"><a href="/amp/show-results/" >Show results</a></li></ul></div></div></div>
<div id="wrapper" class="clearfix">
<div id="content">
  <form id="formOrderSearch" name="formOrderSearch" method="post">
    <div id="dogshow_filter_div">
      <div id="shows_frm">
        <select name="typeid" id="typeid" class="dogshow_filter_drop">
          <option value="">Display by...</option>
          <? $seshow=query_execute("SELECT a.type_name, a.type_id FROM show_type as a, events as b WHERE b.show_type=a.type_id AND b.end_date>NOW() AND b.publish_status='publish' GROUP BY a.type_name ORDER BY a.type_name ASC");
 while($getre=mysql_fetch_array($seshow)){
	 $type_id=$getre['type_id'];
	 $type_name=$getre['type_name'];
	 ?>
          <option  value="<?=$type_id?>"<? if($typeid=='$type_id'){ echo "selected='selected'";} ?>>
          <?=$type_name?>
          </option>
          <? }?>
        </select>
        <input name="searchOrder" id="searchOrder" class="dogshow_gobtn" src="" alt="Go" style="" value="go" type="submit" />
      </div>
    </div>
  </form>
  <div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250 type="doubleclick" data-slot="/21630298032/Puppies"><div placeholder></div><div fallback></div></amp-ad></div>
  <div class="dogshow_sechduletable">
    <table id="" border="0" cellpadding="0" cellspacing="0" width="100%" >
      <tbody>
        <tr>
          <th>Show</th>
          <th> Location</th>
          <th> Closing date</th>
          <th> Days left</th>
        </tr>
        <? 
if($typeid!=''){
$showype = " AND show_type='$typeid'";
	}
		$showdes=query_execute("SELECT * FROM events WHERE end_date>NOW() AND publish_status='publish' $showype ORDER BY start_date ASC");

$s=1;
while($getshowdata=mysql_fetch_array($showdes)){
$show_name=$getshowdata['event_name'];
$location=$getshowdata['venue'];
$type_id=$getshowdata['type_id'];
$date=$getshowdata['start_date'];
$edate=$getshowdata['end_date'];
$show_id=$getshowdata['link_id'];
$event_id=$getshowdata['event_id'];
$organized_by=$getshowdata['organized_by'];
$event_nice_name=$getshowdata['event_nice_name'];
$selke=query_execute_row("SELECT kennel_name, city FROM kennel_club WHERE kennel_id='$organized_by'");

$toDis=date("Y-m-d H:i:s");
$startTimeStamp = strtotime($edate, "YY "/" mm "/" dd");
$endTimeStamp = strtotime($toDis, "YY "/" mm "/" dd"); 
 $timeDiff = abs($endTimeStamp - $startTimeStamp);
$numberDays = $timeDiff/86400;  // 86400 seconds in one day
// and you might want to convert to integer
$numberDays = intval($numberDays);
$sqldentry=query_execute_row("SELECT entry FROM show_description WHERE show_id='$show_id'");
$sqlcdate=$sqldentry['entry'];
?><tr>
          <td  class=<?php if($s % 2 == 0){?>"icon" <? }else{ ?>"dark icon" <? }?> valign="top" ><amp-img src="https://m.dogspot.in/dog-show/Images/buttons/tbl_paw.png" alt="Paw Icon" height="14" width="14" /></td>
            <td id="" <? if($s % 2 == 0){?> <? }else{ ?> class="dark" <? }?> valign="top" width="341">
          <a id="" class="show_title_link" href="/amp/dog-events/<?=$event_nice_name;?>/">
          <?=$selke['kennel_name']."</br>".$show_name;?>
          </a>
            </td>
            <td id="" <? if($s % 2 == 0){?>  <? }else{ ?> class="dark" <? }?> valign="top" width="130">
          <span class="state_name">
          <?=$selke['city'];?>
          </span>
            </td>
            <td id="" <? if($s % 2 == 0){?>  <? }else{ ?> class="dark" <? }?> valign="top" width="129">
          <? print(showdate($edate, "d M Y"));?>
            </td>
            <td id="" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="117">
            <span <? if($s % 2 == 0){?> class="blueborder" <? }else{ ?> class="dark blueborder" <? }?>>
            </span>
          <?=$numberDays?>
            </td>
            <td id="" rowspan="2" <? if($s % 2 == 0){?> class="closing_date" <? }else{ ?> class="dark closing_date" <? }?> valign="top" width="171">
          <? if($show_id){?>
          <? if($sqlcdate!='close'){?>
          <a id="" class="showschedule_enter_online" href="/dog-show/user-dogs.php?show_id=<?=$show_id?>">Enter on-line</a>
          <? }?>
          <? }?>
            <a id="" <? if($s % 2 == 0){?> class="showschedule_results  show_neg" <? }else{ ?> class="showschedule_results  show_neg" <? }?> href="/amp/dog-events/<?=$event_nice_name?>/">
Show details
            </a>
            </td>
        </tr>
        <tr>
            <td id="" <? if($s % 2 == 0){?>  <? }else{ ?>  <? }?> valign="top" width="62">
          <img src="/dog-show/Images/buttons/tbl_calendar.jpg" alt="Calendar" height="13" width="15">
            </td>
            <td id="" colspan="4" <? if($s % 2 == 0){?> class="show_date blueborder" <? }else{ ?> class="dark show_date blueborder" <? }?> valign="top">
          <? print(showdate($date, "d M Y"));?>
          <? if($date!=$edate){ ?>
          - <? print(showdate($edate, "d M Y")); }?>
            </td>
        </tr>
        <? 
			$s=$s+1;
			}?>
      </tbody>
    </table>
  </div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-amp.php'); ?>
