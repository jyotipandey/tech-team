<?php 
include_once("constants.php");
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT . '/session.php');
header("Content-type: text/html; charset=iso-8859-1"); 
$ecomm_pagetype='dog-names'; 
 $maxshow = 20;
if (empty($show)) {
    $show = 0;
} else {
    $show = $show - 1;
}
 
$showRecord  = $show * $maxshow;
$nextShow    = $showRecord + $maxshow;
if($section[2] !='page'){
$exp_value=explode('-', $section[2],2);
$dog_breed_name=query_execute_row("SELECT breed_name, nicename FROM dog_breeds WHERE nicename='$breed_nname'");
$dog_breed_name_sex=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
$sex_bred=$dog_breed_name_sex['breed_name'];
$breed=$dog_breed_name['breed_name'];
$br_nicename=$dog_breed_name['nicename'];
if(($exp_value[0]=="male" || $exp_value[0]=="female") && $exp_value[1]!=""){
$select_sex_options=query_execute("SELECT dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND breed_nicename='$exp_value[1]' AND dog_sex!='' GROUP BY dog_sex");
}else{
$select_sex_options=query_execute("SELECT dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex!='' GROUP BY dog_sex");
}
if($exp_value[0]=="male"){
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex='M' AND (breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND breed_nicename!='bull-terrier-miniature' AND  breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') GROUP BY breed_nicename ORDER BY dog_breed ASC");
}elseif($exp_value[0]=="female"){
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1'  AND dog_sex='F' AND (breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND breed_nicename!='bull-terrier-miniature' AND  breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') GROUP BY breed_nicename ORDER BY dog_breed ASC");
}else{
$select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND (dog_breed!='' AND breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND  breed_nicename!='bull-terrier-miniature' AND breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY breed_nicename ORDER BY dog_breed ASC");
}
}
//strlen($section[2]);
//echo "num".is_nan($section[2]);
if($sex=="male"){$sex="M";}if($sex=="female"){$sex="F";}
if(strlen($section[2])=='1' && !is_nan($section[2])){
$sc_dog_query=" AND dog_name LIKE '$section[2]%'";	
}elseif($sex!='' && $breed==''){
$sc_dog_query=" AND dog_sex='$sex'";
$option_sex=$sex;	
}elseif($sex=='' && $breed!=''){
if($br_nicename=='bull-terrier'){
$sc_dog_query=" AND (breed_nicename='bull-terrier' OR breed_nicename='bull-terrier-miniature')";	
}elseif($br_nicename=='kombai'){
$sc_dog_query=" AND (breed_nicename='kombai' OR breed_nicename='combai')";		
}elseif($br_nicename=='dachshund-miniature-long-smooth-wire-haired'){
$sc_dog_query=" AND (breed_nicename='dachshund-miniature-long-haired' OR breed_nicename='dachshund-miniature-smooth-haired' OR breed_nicename='dachshund-miniature-long-smooth-wire-haired')";		
}elseif($br_nicename=='dachshund-standard-long-smooth-wire-haired'){
$sc_dog_query=" AND (breed_nicename='dachshund-standard-long-haired' OR breed_nicename='dachshund-standard-smooth-haired' OR breed_nicename='dachshund-standard-long-smooth-wire-haired' OR breed_nicename='dachshundstandard(smoothhaired)')";		
}elseif($br_nicename=='poodle-standard-miniature-toy'){
$sc_dog_query=" AND (breed_nicename='poodle-miniature' OR breed_nicename='poodle-standard' OR breed_nicename='poodle-toy'  OR breed_nicename='poodle-standard-miniature-toy')";		
}elseif($br_nicename=='chihuahua-long-smooth-coat'){
$sc_dog_query=" AND (breed_nicename='chihuahua' OR breed_nicename='chihuahua-long-smooth-coat')";		
}elseif($br_nicename=='spitz'){
$sc_dog_query=" AND (breed_nicename='spitz-indian' OR breed_nicename='spitz' OR breed_nicename='spitz-german')";		
}else{
$sc_dog_query=" AND breed_nicename='$br_nicename'";		
}	
$breed_n_name=query_execute_row("SELECT nicename FROM dog_breeds WHERE breed_name='$breed'");
$option_breed=$breed_n_name['nicename'];
}elseif($sex!='' && $breed!=''){
$sc_dog_query=" AND breed_nicename='$br_nicename' AND dog_sex='$sex'";
$option_sex=$sex;
$option_breed=$breed;
}elseif($exp_value[0]=="male"){
$sc_dog_query=" AND breed_nicename='$exp_value[1]' AND dog_sex='M'";
$option_sex='M';
$option_breed=$exp_value[1];
}elseif($exp_value[0]=="female"){
$sc_dog_query=" AND breed_nicename='$exp_value[1]' AND dog_sex='F'";
$option_sex='F';
$option_breed=$exp_value[1];
}elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[2]!=''){
$sc_dog_query=" AND breed_nicename='$breed_nname'";
$option_breed=$breed_nname;	
}else{
	$sc_dog_query=" AND dog_name LIKE 'a%'";
	$option_sex='';
$option_breed='';
}
$url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$seach_dog_filter=query_execute("SELECT dog_name, breed_nicename, dog_breed, dog_nicename, dog_sex,dog_image FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY dog_name ORDER BY dog_name ASC LIMIT $showRecord, $maxshow");
$seach_dog_filter_all=query_execute("SELECT dog_name, breed_nicename, dog_breed, dog_nicename, dog_sex,dog_image FROM dogs_available WHERE publish_status='publish' AND dog_image !=' ' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' $sc_dog_query GROUP BY dog_name");
$totrecord = mysql_num_rows($seach_dog_filter_all);
$titlepage   = $show+1;
 ?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
<? if($section[1]=='' && $section[2]==''){?>
<title>Dog Names | Complete list of popular dog names | Dogspot.in</title>
<meta name="keywords" content="Dog name, puppy names, cute dog names, popular dog names, cool dog names, friendly dog name, funny puppy namess" />
<meta name="description" content="Names are very important for our little furry friends. Here is the complete list of popular male and female dog names at dogspot.in" />
<? $h1_data='Dog Names'; ?>
<? }elseif($section[2]=='female'){?>
<title>Best Dog Name For Female | Popular Puppy Names for Girl</title>
<meta name="keywords" content="Dog name for females, female dog name, list of dog names for girl, best dog name for girl." />
<meta name="description" content="Search best dog names for your female puppy. Discover list of cute and funny dog names for girl." />
<? $h1_data='Dog Names for Female Puppy'; ?>
<? }elseif($section[2]=='male'){?>
<title>Best Dog Name For Male | Popular Puppy Names for Boy</title>
<meta name="keywords" content="Dog name for Males, male dog name, list of dog names for boy, best dog name for boy." />
<meta name="description" content="Search best dog names for your male puppy. Discover list of cute and funny dog names for boy." />
<? $h1_data='Dog Names for Male Puppy'; ?>
<? }elseif(strlen($section[2])=='1' && !is_nan($section[2])){?>
<title>Dog Name Start with Letter <?=ucwords($section[2])?> | Popular Puppy Names Begin with Alphabet <?=ucwords($section[2])?></title>
<meta name="keywords" content="Dog name with letter <?=ucwords($section[2])?>, <?=ucwords($section[2])?> letter Names for puppy, best dog name start with letter <?=ucwords($section[2])?>, popular puppy name with Alphabet <?=ucwords($section[2])?>" />
<meta name="description" content="Search Dog Names starts with Letter <?=ucwords($section[2])?> for your male and female puppy. Discover best and funny dog names begin with Alphabet <?=ucwords($section[2])?>." />
<? $h1_data='Dog Names Start with Letter'.' '. ucwords($section[2]); ?>
<? }elseif($exp_value[1] == "female"){?>
<title>Best Dog Names for Female <?=$sex_bred?> | Popular Puppy Names for Girl <?=$sex_bred?></title>
<meta name="keywords" content="Best Dog Names for female <?=$sex_bred;?>, <?=$sex_bred;?> female dog names, best <?=$sex_bred;?> nicknames for girl." />
<meta name="description" content="Search best dog names for your female <?=$sex_bred;?>. Discover list of cute and funny <?=$sex_bred?> dog names for girl." />
<? $h1_data='Dog Names for Female'.' '. $sex_bred; ?>
<? $h2_data='Here is the popular dog names for Female '. ' ' 
.$sex_bred; ?>
<? }elseif($exp_value[1] == "male"){?>
<title>Best Dog Names for Male <?=$sex_bred?> | Popular Puppy Names for Boy <?=$sex_bred?></title>
<meta name="keywords" content="Best Dog Names for male <?=$sex_bred?>, <?=$sex_bred?> male dog names, best <?=$sex_bred?> nicknames for boy." />
<meta name="description" content="Search best dog names for your male <?=$sex_bred?>. Discover list of cute and funny <?=$sex_bred?> dog names for boy." />
<? $h1_data='Dog Names for Male'.' '. $sex_bred; ?>
<? $h2_data='Here is the popular dog names for Male'.' '. $sex_bred; ?>
<? }
elseif($exp_value[1] != "male" && $exp_value[1]!="female" && $section[2]!=''){?>
<title>Best Dog Names for <?=$breed;?> | Popular Puppy Names for <?=$breed;?></title>
<meta name="keywords" content="Best Dog Names for <?=$breed;?>, <?=$breed;?> dog names, best <?=$breed;?> nicknames" />
<meta name="description" content="Search best dog names for your <?=$breed;?>. Discover list of cute and funny <?=$breed;?> dog names." />
<? $h1_data='Dog Names for'.' '. $breed; ?>
<? }?>
 <link rel="canonical" href="https://www.dogspot.in/dog-names/<? if($section[2]!=''){ ?><?=$section[2];?>/<? }?>" />
 <meta name="author" content="DogSpot" />
<? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
<? if(strlen($section[2])=='1' && !is_nan($section[2])){
	$bread_crum='Starts with Letter'.' '.$section[2];
    }elseif($section[2]=="male"){
    $bread_crum='Male';
}elseif($section[2]=="female"){
	$bread_crum='Female';
	}elseif($exp_value[0] != "male" && $exp_value[0]!="female" && $section[2]!=''){
	$bread_crum=$breed;
    $dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$section[2]'");	
	 $firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$section[2];
	}elseif($exp_value[0] == "male" && $exp_value[1]!=""){
		$dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
		$firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$exp_value[1];
		$bread_crum='Male'.' '.$dog_breed_name_bredcrum['breed_name'];
    }elseif($exp_value[0] == "female" && $exp_value[1]!=""){
		$dog_breed_name_bredcrum=query_execute_row("SELECT breed_name FROM dog_breeds WHERE nicename='$exp_value[1]'");
		$firstBread=$dog_breed_name_bredcrum['breed_name'];
		$firstBreadcum=$exp_value[1];
		$bread_crum='Female'.' '.$dog_breed_name_bredcrum['breed_name'];
	}
?><div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage">Home</span></a>
                        <span itemprop="position" content="1"></span></span> 
                        <span> / </span>
                  <? $wag_dog_brrrd = query_execute_row("SELECT breed_engine FROM dog_breeds WHERE nicename='$firstBreadcum' ");
 if($wag_dog_brrrd['breed_engine'] == '1'){?><span>
 <a href="https://www.dogspot.in/<?=$firstBreadcum?>/" itemprop="item">
 <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<span itemprop="name"><?=$firstBread?></span>
 <meta itemprop="position" content="2" /></span></a></span>
<?  }else{?><span>
<a href="https://www.dogspot.in/dog-names/" itemprop="item">
     <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
     <span itemprop="name">Dog Names</span>
     <meta itemprop="position" content="2" /> 
     </span></a></span>
     <? }?> 
       <? if($section[1]!=''){?> <span> / </span>
   <span><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <span class="brd_font_bold" itemprop="name"><?=ucwords($bread_crum);?> Names</span>
       <meta itemprop="position" content="3" /> </span></span><? }?>               
                </div><div class="dog_name_wrap">
<div class="dogname_content"><div class="dognameBlock"><? if($section[0]==''){?><h1><? }?> <div class="dog-nameText">Dog Names</div><? if($section[0]==''){?></h1><? }?><div style="text-align:justify; padding-left:10px; padding-right:10px;">
Have you met new parents or parents - to - be? One of the most interesting discussions that often arise in the company of new parents is "what would be the new baby called?", followed by a myriad of suggestions by family and friends. Just like human babies, new pet parents also face a similar predicament "what to name their new furry baby". Your search for the perfect name ends right here. We bring to you a list of names that you can select and choose from! Happy searching!! 
Here is the popular dog names <? if($firstBreadcum){?> for
       <a style="color:#6c9d06; text-decoration:underline;" href="/amp/<?=$firstBreadcum?>/"><?=$breed;?></a><? }?>.  
          <form action="/amp/dog-names/getBreedName.php" target="_top" method="get"><div><div class="col2" style="margin-top:10px;">
                    <div class="ui-btn selectWrapper"><select name="breed" id="breed" class="selectBox">
                             <option value="">Select Breed</option>
                             <? $select_dog_data=query_execute("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND (dog_breed!=''  AND breed_nicename!='bull-terrier-miniature' AND breed_nicename!='combai' AND  breed_nicename!='bull-terrier-miniature' AND breed_nicename!='dachshund-miniature-long-haired' AND breed_nicename!='dachshund-miniature-smooth-haired' AND breed_nicename!='dachshund-standard-long-haired' AND breed_nicename!='dachshund-standard-smooth-haired' AND breed_nicename!='poodle-miniature' AND breed_nicename!='poodle-standard' AND breed_nicename!='poodle-toy' AND breed_nicename!='chihuahua' AND breed_nicename!='dachshundstandard(smoothhaired)' AND breed_nicename!='spitz-indian' AND breed_nicename!='spitz-german') AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1'GROUP BY breed_nicename ORDER BY dog_breed ASC");
							 while($get_breed=mysql_fetch_array($select_dog_data)){
				             $dg_breed=$get_breed['breed_nicename'];?>
                             <option value="<?=$dg_breed;?>" <? if($dg_breed==$option_breed && $option_breed !=''){echo'selected="selected"';}?>><?=$get_breed['dog_breed'];?></option><? }?></select>
                              </div></div>
                     <div class="col2" style="margin-top:10px;"><div class="ui-btn selectWrapper"><select name="sex" id="sex" class="selectBox">
                            <option value="">Select Gender</option>
                            <? $select_sex_options=query_execute("SELECT dog_sex FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex!='' GROUP BY dog_sex");

							while($get_sex_options=mysql_fetch_array($select_sex_options)){
				           $sex_data_opt=$get_sex_options['dog_sex'];
				           if($sex_data_opt=='M'){
					$v_data_opt='male';
					$dis_data_opt='Male';
					}
					if($sex_data_opt=='F'){
					$v_data_opt='female';
					$dis_data_opt='Female';
					}
				?>
            <option value="<?=$v_data_opt?>" <? if($option_sex==$sex_data_opt){echo'selected="selected"';}?> id="<?=$sex_data_opt; ?>"><?=$dis_data_opt;?></option>
            <? }?></select></div></div>    </div>
                      <button name="go" class="dogname-btn" id="redirecttopage41" >Go</button>
                    </form>  
           <ul class="filter-a-z">
          <li><strong>Names A-Z</strong></li>
          <li><a href="/amp/dog-names/a/"  <? if($section[2]=='a' || $section[2]==''){?>class="current"<? }?>>A</a></li>
          <li><a href="/amp/dog-names/b/" <? if($section[2]=='b'){?>class="current"<? }?>>B</a></li>
          <li><a href="/amp/dog-names/c/"  <? if($section[2]=='c'){?>class="current"<? }?>>C</a></li>
          <li><a href="/amp/dog-names/d/" <? if($section[2]=='d'){?>class="current"<? }?>>D</a></li>
          <li><a href="/amp/dog-names/e/"  <? if($section[2]=='e'){?>class="current"<? }?>>E</a></li>
          <li><a href="/amp/dog-names/f/" <? if($section[2]=='f'){?>class="current"<? }?>>F</a></li>
          <li><a href="/amp/dog-names/g/"  <? if($section[2]=='g'){?>class="current"<? }?>>G</a></li>
          <li><a href="/amp/dog-names/h/"  <? if($section[2]=='h'){?>class="current"<? }?>>H</a></li>
          <li><a href="/amp/dog-names/i/"  <? if($section[2]=='i'){?>class="current"<? }?>>I</a></li>
          <li><a href="/amp/dog-names/j/"  <? if($section[2]=='j'){?>class="current"<? }?>>J</a></li>
          <li><a href="/amp/dog-names/k/"  <? if($section[2]=='k'){?>class="current"<? }?>>K</a></li>
          <li><a href="/amp/dog-names/l/"  <? if($section[2]=='l'){?>class="current"<? }?>>L</a></li>
          <li><a href="/amp/dog-names/m/"  <? if($section[2]=='m'){?>class="current"<? }?>>M</a></li>
          <li><a href="/amp/dog-names/n/"  <? if($section[2]=='n'){?>class="current"<? }?>>N</a></li>
          <li><a href="/amp/dog-names/o/"  <? if($section[2]=='o'){?>class="current"<? }?>>O</a></li>
          <li><a href="/amp/dog-names/p/"  <? if($section[2]=='p'){?>class="current"<? }?>>P</a></li>
          <li><a href="/amp/dog-names/q/"  <? if($section[2]=='q'){?>class="current"<? }?>>Q</a></li>
          <li><a href="/amp/dog-names/r/"  <? if($section[2]=='r'){?>class="current"<? }?>>R</a></li>
          <li><a href="/amp/dog-names/s/"  <? if($section[2]=='s'){?>class="current"<? }?>>S</a></li>
          <li><a href="/amp/dog-names/t/"  <? if($section[2]=='t'){?>class="current"<? }?>>T</a></li>
          <li><a href="/amp/dog-names/u/"  <? if($section[2]=='u'){?>class="current"<? }?>>U</a></li>
          <li><a href="/amp/dog-names/v/"  <? if($section[2]=='v'){?>class="current"<? }?>>V</a></li>
          <li><a href="/amp/dog-names/w/"  <? if($section[2]=='w'){?>class="current"<? }?>>W</a></li>
          <li><a href="/amp/dog-names/x/"  <? if($section[2]=='x'){?>class="current"<? }?>>X</a></li>
          <li><a href="/amp/dog-names/y/"  <? if($section[2]=='y'){?>class="current"<? }?>>Y</a></li>
          <li><a href="/amp/dog-names/z/"  <? if($section[2]=='z'){?>class="current"<? }?>>Z</a></li>
        </ul> <amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="7301949999"
     data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div>
<div class="dog_name_filter"><? if($section[1]==''){?><h2 style="margin-left:-10px;">Dog Names A to Z List</h2><? }else{?><h1><?=$h1_data;?></h1><? }?></div>
         <table class="A-Z dog_name_table dog-name-tab">
                    <thead><th>Dog Name</th>
                    <th>Breed</th>
                     <th>Gender</th>
                    </thead>
                    <?
					$jk=0;$iid=0;
					while($get_search_data=mysql_fetch_array($seach_dog_filter)){
						$jk=$jk+1;$iid=$iid+1;
			 $dg_name=$get_search_data['dog_name'];
			  $string=preg_replace('/[^A-Za-z0-9\-]/', ' ', ucwords(strtolower($get_search_data['dog_name'])));
			  $wordlist = array("ch", "biss", "Ind Ch", "Bis", "Ch", "Grand Ind", "sale", "month", "imp", "Sale", "Month", "Imp", "Import", "Dogs", "-");
              foreach ($wordlist as &$word) {
              $word = '/\b' . preg_quote($word, '/') . '\b/';
              }
			  $string = preg_replace('/[0-9]+/', '', $string);
              $string = preg_replace($wordlist, '', $string);
			  $breed_engine_data=query_execute_row("SELECT nicename FROM dog_breeds WHERE breed_name='".$get_search_data['dog_breed']."' AND breed_engine='1'");
			  $breed_engine_nicename=$breed_engine_data['nicename'];?>
             <tr><td class="td-highlight"><?=$string;?></td>
            <td><? $breed_engine_nicename?> <?=$get_search_data['dog_breed'];?></td>
            <td><? if($get_search_data['dog_sex']=="M"){echo "Male";}else{echo "Female";}?></td></tr>
          <? }?></table> <div class="pagination">
					<?php
                    if ($section[2]) {
                        $pageUrl = "/amp/dog-names/$section[2]";
                    } elseif($section[2]=='') {
                        $pageUrl = "/amp/dog-names/a";
                    }
                    showPages_courses($totrecord, $maxshow, $pageUrl, $show, $next, $previous, $spUrl, $url);
                    ?>
               </div></div>
		</div>
        <amp-ad width="100vw" height=320 type="adsense"  data-ad-client="ca-pub-3238649592700932" data-ad-slot="7301949999"     data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div><div class="col-sm-3">
                <div class="blog-sidebar-box">
        <div class="relted-dog-name">
<h2>Most Searched Dog Names By Breed</h2>
<ul>
<li class="rel-mb"><a data-ajax="false" href="/amp/dog-names/golden-retriever/">Dog Names for Golden Retriever <span class="rel-right">&gt;&gt;</span></a> </li>
<li class="rel-ml rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/german-shepherd-dog-alsatian/">Dog Names for German Shepherd<span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/tibetan-mastiff/">Dog Names for Tibetan Mastiff<span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/rottweiler/">Dog Names for Rottweiler <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/great-dane/">Dog Names for Great Dane <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/labrador-retriever/">Dog Names for Labrador Retriever <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/pug/">Dog Names for Pug <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/siberian-husky/">Dog Names for Siberian Husky <span class="rel-right">&gt;&gt;</span></a></li>
<li class=" rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/beagle/">Dog Names for Beagle <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml rel-mb margin-top-50"><a data-ajax="false" href="/amp/dog-names/pomeranian/">Dog Names for Pomeranian <span class="rel-right">&gt;&gt;</span></a></li>
<li class="margin-top-50"><a data-ajax="false" href="/amp/dog-names/stbernard/">Dog Names for St.Bernard <span class="rel-right">&gt;&gt;</span></a></li>
<li class="rel-ml margin-top-50"><a  data-ajax="false" href="/amp/dog-names/bull-dog/">Dog Names for Bull Dog <span class="rel-right">&gt;&gt;</span></a></li>
</ul>
</div></div><div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250 type="doubleclick" data-slot="/21630298032/Puppies"><div placeholder></div><div fallback></div></amp-ad></div><div class="blog-sidebar-box relted-dog-name">                    <h3>Dog Names by Gender</h3><ul>
<li class="rel-mb"><a aria-label="Dog Names for Male" href="/amp/dog-names/male/">
Male Dog Names <span class="rel-right">>></span></a> </li>
<li class="rel-ml rel-mb"><a aria-label="Dog Names for Female" href="/amp/dog-names/female/">
Female Dog Names<span class="rel-right">>></span></a></li>
</ul></div></div></div></div>
<? include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>