<?php require_once('constants.php');
   require_once($DOCUMENT_ROOT.'/database.php');
   require_once($DOCUMENT_ROOT.'/functions.php');
   require_once($DOCUMENT_ROOT . '/session.php');
$ecomm_pagetype='articles'; 
$sitesection='articles'; 
 header("Content-type: text/html; charset=iso-8859-1");
$rowpost_detail=query_execute_row("SELECT wpp.ID , wpp.post_author,wpp.views , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id, wu.display_name,wu.ID as article_user,wpp.atag,wpp.post_content_amp FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt,wp_users as wu WHERE wptr.object_id=wpp.ID  AND wpp.post_type='post' AND wu.ID=wpp.post_author AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wpp.ID='$article_id'");
		//$post_detail=mysql_query($sqlall);
	//	$rowpost_detail=mysql_fetch_array($post_detail);
		$article_id=$rowpost_detail['ID'];
		$artuser_wp        = $rowpost_detail["post_author"];
        $rowUser_id  = query_execute_row("SELECT user_login FROM wp_users WHERE ID ='$artuser_wp'");
        $artuser = $rowUser_id['user_login'];
		$date=$rowpost_detail['post_date'];
		$reformatted_date = showdate($date, "d M y");
		$art_name=$rowpost_detail['post_name'];
		$post_title=$rowpost_detail['post_title'];
		$author=$rowpost_detail['display_name'];
		$post_content_amp=stripcslashes($rowpost_detail['post_content_amp']);
		$articlecat_id  = $rowpost_detail["term_id"];
		$sel_cat=query_execute_row("SELECT * FROM wp_terms WHERE term_id='$articlecat_id'");
		$cat_name=$sel_cat['name'];
		$cat_nice=$sel_cat['slug'];
		$post_title = stripslashes($post_title);
		$post_title = breakLongWords($post_title, 30, " ");
		$post_content = stripslashes($post_content_amp);
				if($title_tag){
    $title = $title_tag;
}else{
    $title = $post_title;
}

if($art_tag){
    $keyword = $art_tag;
}else{
	$title = str_replace( array( '\'', '"', ',' , ';', '<', '>' ), ' ', $post_title);
    $keyword = $title;
}

		if ($post_content) {
			$pageDesc = $post_content;
			$pageDesc = html_entity_decode(strip_tags($pageDesc));
			$pageDesc = myTruncate($pageDesc, 200); // funtion to split 200 words
			
		}				
if (!$article_id) {
    header("HTTP/1.0 404 Not Found");
    require_once($DOCUMENT_ROOT . '/404.php');
    die(mysql_error());
    exit();

}
$trend_Desc=query_execute_row("SELECT wpp.ID , wpp.post_title, wpp.post_name , wpp.post_content , wpm.meta_value FROM wp_posts as wpp , wp_postmeta as wpm WHERE wpp.post_status = 'publish' AND wpp.ID='$article_id' AND wpp.domain_id='1' AND wpp.post_type='post' AND wpm.post_id=wpp.ID AND wpm.meta_key like 'Description' ORDER BY wpm.meta_id DESC LIMIT 1");
$trend_page_Desc=$trend_Desc['meta_value'];
 ?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$title?> | Dogspot.in</title>
    <meta name="author" content="DogSpot" />
    <? if($trend_page_Desc){?>
    <meta name="description" content="<?=preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $trend_page_Desc)?>" />
    <? }else{?>
    <meta name="description" content="<? $pageDesc = strip_tags($pageDesc); echo preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $pageDesc); ?>" />
 <? }?>
    <meta name="keywords" content="<?= $keyword ?>" />
    <link rel="canonical" href="https://www.dogspot.in/<?=$section[1];?>/" />
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
    <script async custom-element="amp-facebook-comments" src="https://cdn.ampproject.org/v0/amp-facebook-comments-0.1.js"></script>
<script async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>
  <? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
<div class="breadcrumb"> <div id="topPageStat" itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a>
                            <span itemprop="position" content="1"></span>
                        </span><span>/</span>
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="/amp/dog-blog/" itemprop="item"><span itemprop="name">Dog Blog</span></a>
                            <span itemprop="position" content="2"></span>
                        </span><span>/</span>
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="#" itemprop="item"><span itemprop="name"><?=$post_title?></span></a>
                           <meta itemprop="position" content="3" />
                        </span>
                    </div></div><div class="art_blk">
    <div class="art_view_blk"><h1 style="font-size:16px;"><?=$post_title?></h1>                   
                   <div class="art_post_detail"><a href="/amp/dog-blog/<?=$cat_nice?>/" class="art_cat_name" data-ajax="false"><?=$cat_name?></a>,<?=$reformatted_date?>, By <?=$author?></div><div class="details">
                       <?=htmlspecialchars_decode(html_entity_decode($post_content_amp));?>
                    </div><div class="space"></div>
                     <? 
			if($GetUser['about_me']!=''){ 
			$GetUser = query_execute_row("SELECT about_me,f_name,image,userlevel FROM users WHERE userid='".$artuser."'");
    if ($GetUser['userlevel'] == '8' || $artuser=='sunur' || $GetUser['userlevel'] == '2') {
        if ($GetUser["image"]) {
            $profileimgurl = "/profile/images/" . $GetUser["image"];
        } else {
            $profileimgurl = '/images/noimg.gif';
        }
    	 $imgWH[0] =100;
		$imgWH[1] =100;
?>
<div><h3 class="margin-0">About the Author</h3>
                   <amp-img src="<?=$profileimgurl?>" layout="fixed" width="100" height="100" class="pull-left circle"></amp-img>
                    <h4 style="margin-bottom:5px;text-align: center;}"><?=$author?></h4>
              <p style="font-size:14px; margin-bottom:20px;"><?=stripslashes($GetUser['about_me']); ?><? $selectuset = query_execute_row("select meta_value from users_meta where meta_key='google_userid' AND userid='".$artuser."'");
				if($selectuset['meta_value']){?><br /><p style="padding-left:110px">Connect with the author via: <a href="https://plus.google.com/<?=$selectuset['meta_value']?>" target="_blank" rel="author nofollow">Google+</a></p></p>
              <? } } }?></div><div class="divider-30 colored"></div>
                <div class="blog-sidebar-box"><h3 class="margin-0">Comment</h3>
             <div class="blog-sidebar-box"> <amp-facebook-comments width=486 height=657 layout="responsive" 
             data-href="<?="https://www.dogspot.in/$section[1]/";?>" data-num-posts="5"></amp-facebook-comments></div></div></div></div>           
  <script type="application/ld+json">{ "@context":"http://schema.org", "@type":"Blog", "@id":"https://www.dogspot.in/<?=$section[1];?>/", "about":{ "@type":"CreativeWork", "name":"Dog Blog" }, "headline":"Dog Blog", "description":"Dog Blog Category, & Articles from Dogs Experts Dogs India DogSpot.in.", "blogPost":[ { "@type":"BlogPosting", "name":"<?=$title?>", "headline":"<?=$title?>", "articleBody": "<?=strip_tags($pageDesc).'....';?>", "datePublished":"<?=$date?>", "dateModified":"<?=$date?>",
   "mainEntityOfPage":"https://www.dogspot.in/<?=$section[1];?>/", "author":"<?=$author?>", "publisher":{ "@type":"organization", "name": "DogSpot", "logo": { "@type": "ImageObject", "url": "https://www.dogspot.in/new/common/images/dogspot-new-logo-final.png", "width": 177, "height": 60 } }, "image": { "@type": "ImageObject", "url": "https://www.dogspot.in/new/common/images/dogspot-new-logo-final.png", "width": 696, "height": 391 } } ] }</script>
<? include_once($DOCUMENT_ROOT."/new/common/bottom-amp.php");?>
</body>
</html>