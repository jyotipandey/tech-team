<?php 
include("../constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$sitesection = "dog-business"; 
?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title>Dog Business Listing | DogSpot</title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="<? echo"$pageTitle";?> Pet Business Find Phone Numbers, Addresses, Best Deals, Latest Reviews & Ratings of Kennel Owner Breeder, Pet Shop, Dog Trainer, Pet Services, Pet Boarding, Veterinarian, Kennel Club | DogSpot" />
    <meta name="keywords" content="<? echo"$pageTitle";?> Kennel Owner Breeder, Pet Shop, Dog Trainer, Pet Services, Pet Boarding, Veterinarian, Kennel Club | Pet Business in India | DogSpot" />
    <link rel="canonical" href="https://www.dogspot.in/dog-listing/" />
  <? require($DOCUMENT_ROOT.'/new/common/top-2-amp.php');  ?>
<div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a><span itemprop="position" content="1"></span></span><span>/</span><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/amp/dog-listing/" itemprop="item"><span itemprop="name">Dog Listing</span></a><span itemprop="position" content="2"></span></span></div><div class="space-1"></div>
 <div class="ui-content"><div class="clfieds_blk">
<div class="clfieds_head"><div class="clfieds_search_box"><div class="clfieds_heading"><h1 style="font-weight:normal;text-align: center;margin-top: 15px;margin-bottom: 25px;font-size: 22px;color: #fff;">Find Pet Shops, Veterinarians, Kennels & Many More</h1></div>
<form target="_top" action="/amp/dog-listing/searchA.php" method="get">    <div class="art_search"><div class="input_field"><div class="col21 input_field">
             <select data-role="none" name="drop_category" id="drop_category">
        <option value="">Select Category </option> 
                <? 
					$sql1=mysql_query("SELECT city_nicename, city_name FROM business_listing WHERE city_name!='' GROUP BY city_name ORDER BY city_name ASC");
					while($row1=mysql_fetch_array($sql1)){if($row1['city_nicename']){
				?><option value="<?=$row1['city_nicename']?>"><?=ucfirst($row1['city_name']);?></option>
                        <? } }?>
            </select></div><input type="submit" name="search" id="search" value="SEARCH" class="adoption_ok"/></div></div></form>
 </div></div></div><div class="clfieds_cat">Categories</div><div class="clfieds_sec"><div class="clfieds_list"><a href="/amp/dog-listing/category/kennel-owner-breeder/" aria-label="Breeder/Kennels"><amp-img src="/checkout/images/dog-breed-icon.png" width='105' height='105'  alt="Breeder/Kennels"></amp-img><div class="clfieds_cat_name">Breeder/Kennels</div></a></div><div class="clfieds_list"><a href="/amp/dog-listing/category/services/" aria-label="Grooming"><amp-img src="/checkout/images/gromming-icons.jpg" width="105" height="105" alt="Grooming"></amp-img><div class="clfieds_cat_name">Grooming</div></a></div><div class="clfieds_list"><a href="/amp/dog-listing/category/kennel-club/" aria-label="Kennel Club">
<amp-img src="/checkout/images/&#9;kennel-club-icon.png" width="105" height="105" alt="Kennel Club" title="Kennel Club" ></amp-img><div class="clfieds_cat_name">Kennel Club</div></a></div><div class="clfieds_list"><a href="/amp/dog-listing/category/veterinarian/" aria-label="Veterinarian"><amp-img src="/checkout/images/vet-icons.jpg" width="105" height="105" alt="Veterinarian" title="Veterinarian"> </amp-img><div class="clfieds_cat_name">Veterinarian</div></a></div><div class="clfieds_list"><a href="/amp/dog-listing/category/pet-boarding/" aria-label="Boarding"><amp-img src="/checkout/images/day-care-small.png" width="105" height="105" alt="Boarding" title="Boarding"></amp-img><div class="clfieds_cat_name">Boarding</div></a></div><div class="clfieds_list"><a href="/amp/dog-listing/category/dog-trainer/" aria-label="Dog Trainer">
<amp-img src="/checkout/images/trainer-small-icon.png" width="105" height="105" alt="Dog Trainer" title="Dog Trainer"></amp-img>
<div class="clfieds_cat_name">Dog Trainer</div></a></div><div class="clfieds_list"><a href="/amp/dog-listing/category/pet-shop/" aria-label="Pet Shop"><amp-img src="/checkout/images/petshop-icons.jpg"  width="105" height="105" alt="Pet Shop" title="Pet Shop"></amp-img><div class="clfieds_cat_name">Pet Shop</div></a></div><div class="clfieds_list"><a href="/amp/dog-listing/category/others/" > <amp-img src="/checkout/images/other-icon.png" width="105" height="105" alt="Pet Shop" title="Pet Shop"  class="clfieds_icon_other"></amp-img><div class="clfieds_cat_name">Other</div></a></div></div>	
	<div class="add_your_bs_btn" style="text-align:center; height: 20px; padding-top: 0px;"><a href="https://m.dogspot.in/dog-listing/add-business-form.php">Add Your Business</a></div>
	<div style="float:left; width:100%;">
<amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="5144837740" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div></div><div class="best_services_sec" style="background:#f2f2f2; "><div class="best_services_text"><? if($sessionUserCity){?>Best Services in <?=$sessionUserCity;?><? }else{ echo "Best Services"; }?></div><ul class="clfieds_listing" style="text-transform:none;"> 
            <?php 
				if($sessionUserCity){
 	               	$sql = "SELECT * FROM business_listing WHERE city_nicename = '$sessionUserCity' ORDER BY business_id DESC LIMIT 5";
				}else{
					$sql = "SELECT * FROM business_listing ORDER BY business_id DESC LIMIT 5";
				}
				$selectArt = mysql_query("$sql");
                if(!$selectArt){ die(mysql_error()); }
				$ii=0;
                while($rowArt1 = mysql_fetch_array($selectArt)){
                    $ii++;$username=$rowArt1["userid"];
                    $comp_org = $rowArt1["comp_org"];
                    $address = $rowArt1["address"];
                    $address = stripslashes($address);
                    $address =  breakLongWords($address, 30, " ");
                    $city_name = $rowArt1["city_name"];
                    $country_name = $rowArt1["country_name"];
                    $bus_nicename = $rowArt1["bus_nicename"];
                    $mobile_main = $rowArt1["mobile_main"];
					
					$cat_nice_name1 = $rowArt1["cat_nice_name1"];
					$cat_nice_name2 = $rowArt1["cat_nice_name2"];
					$cat_nice_name3 = $rowArt1["cat_nice_name3"];
					$cat_nice_name4 = $rowArt1["cat_nice_name4"];
					
					$rowUser1=query_execute_row("SELECT cat_logo FROM business_category WHERE (cat_nice_name='$cat_nice_name1' OR cat_nice_name='$cat_nice_name2' OR cat_nice_name='$cat_nice_name3' OR cat_nice_name='$cat_nice_name4') LIMIT 1");
                    if($rowUser1["cat_logo"]){
                        $profileimgurl='/checkout/images/'.$rowUser1["cat_logo"];
                    }else{
                       // $profileimgurl='/checkout/images/photo.jpg'; 
                    }
            ?><li><a href="/amp/dog-listing/<?=$bus_nicename;?>/" class="ui-link"><div class="clfiedsimg_blk"><amp-img src="<?=$profileimgurl; ?>" alt="<?=$city_name; ?>" title="<?=$city_name; ?>" class="clfieds_icon_shop" width='108' height='108'></amp-img></div><div class="clfiedstext_blk"><div class="clfieds_right_icon" style="text-align: right;margin-top:-12%;margin-right:0px;"><amp-img src="/checkout/images/right-arrow.png" width='10' height='17'></amp-img></div><div class="clfieds_shop_name"><? print ucfirst(breakLongWords($comp_org, 11, " "));?></div><div class="clfieds_shop_address"><amp-img src="/checkout/images/Maps-icon.png" style="margin-right:3px;" width='10' height='10'></amp-img><? echo trim(trim($address,',')).", ".trim(trim($city_name,',')).", +91-".trim(trim($mobile_main,',')); ?></div></div></a></li>
            <? if($ii==2){?><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="5144837740" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><? } } ?>
            </ul>
        </div>
		</div>
            
    <?php require_once($DOCUMENT_ROOT.'/new/common/bottom-amp.php'); ?> 
</body></html>  


