<?php 
include("../constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
$sitesection = "dog-business"; 
	 function getLocationInfoByIp(){
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = @$_SERVER['REMOTE_ADDR'];
    $result  = array('country'=>'', 'city'=>'');
    if(filter_var($client, FILTER_VALIDATE_IP)){
        $ip = $client;
    }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
        $ip = $forward;
    }else{
        $ip = $remote;
    }
    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));    
    if($ip_data && $ip_data->geoplugin_countryName != null){
        $result['country'] = $ip_data->geoplugin_countryCode;
        $result['city'] = $ip_data->geoplugin_city;
    }
    return $result;
}
?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title>Dog Business Listing | DogSpot</title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="<? echo"$pageTitle";?> Pet Business Find Phone Numbers, Addresses, Best Deals, Latest Reviews & Ratings of Kennel Owner Breeder, Pet Shop, Dog Trainer, Pet Services, Pet Boarding, Veterinarian, Kennel Club | DogSpot" />
    <meta name="keywords" content="<? echo"$pageTitle";?> Kennel Owner Breeder, Pet Shop, Dog Trainer, Pet Services, Pet Boarding, Veterinarian, Kennel Club | Pet Business in India | DogSpot" />
    <link rel="canonical" href="https://www.dogspot.in/<?=$section[1]?>/" />
  <? require($DOCUMENT_ROOT.'/new/common/top-amp.php');  ?>
     <div class="container-fluid">
     	<div class="space-2"></div>
        <div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="/amp/" itemprop="item"><span property="item" typeof="WebPage" itemprop="name">Home</span></a>
                            <span itemprop="position" content="1">&raquo;</span>
                        </span>
                        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a href="/amp/dog-listing/" itemprop="item"><span itemprop="name">Dog Listing</span></a>
                           <span itemprop="position" content="2">&raquo;</span>
                        </span>
                   </div>
     <div class="space-2"></div>              
    <div class="row">
      <h1>Find Pet Shops, Veterinarians, Kennels & Many More</h1></div>
    <div class="col-xs-12 row">
<h2>Categories</h2>
<div class="bones-products-grid cols-2">
<div class="bones-product-list-item" >
<a href="/amp/dog-listing/category/kennel-owner-breeder/" class="preview" aria-label="Breeder/Kennels">
<amp-img src="/checkout/images/dog-breed-icon.png" width="125" height="125" alt="Breeder/Kennels"></amp-img>
<div class="categories text-center">Breeder/Kennels</div></a>
</div>
<div class="bones-product-list-item" >
<a href="/amp/dog-listing/category/services/" class="preview" aria-label="Grooming">
<amp-img src="/checkout/images/gromming-icons.jpg" width="125" height="125" alt="Grooming"></amp-img>
<div class="categories text-center">Grooming</div></a>
</div>
<div class="bones-product-list-item" >
<a href="/amp/dog-listing/category/kennel-club/" class="preview" aria-label="Kennel Club">
<amp-img src="/checkout/images/&#9;kennel-club-icon.png" width="125" height="125" alt="Kennel Club" title="Kennel Club" ></amp-img>
<div class="categories text-center">Kennel Club</div></a>
</div>
<div class="bones-product-list-item" >
<a href="/amp/dog-listing/category/veterinarian/" class="preview" aria-label="Veterinarian">
<amp-img src="/checkout/images/vet-icons.jpg" width="125" height="125" alt="Veterinarian" title="Veterinarian"> </amp-img>
<div class="categories text-center">Veterinarian</div></a>
</div>
<div class="bones-product-list-item" >
<a href="/amp/dog-listing/category/pet-boarding/" class="preview" aria-label="Boarding">
<amp-img src="/checkout/images/day-care-small.png" width="125" height="125" alt="Boarding" title="Boarding"></amp-img>
<div class="categories text-center">Boarding</div></a>
</div>
<div class="bones-product-list-item" >
<a href="/amp/dog-listing/category/dog-trainer/" class="preview" aria-label="Dog Trainer">
<amp-img src="/checkout/images/trainer-small-icon.png" width="125" height="125" alt="Dog Trainer" title="Dog Trainer"></amp-img>
<div class="categories text-center">Dog Trainer</div></a>
</div>
<div class="bones-product-list-item" >
<a href="/amp/dog-listing/category/pet-shop/" class="preview" aria-label="Pet Shop">
<amp-img src="/checkout/images/petshop-icons.jpg" width="125" height="125" alt="Pet Shop" title="Pet Shop"></amp-img><div class="categories text-center">Pet Shop</div></a>
</div>
></div>
</div><div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250 type="doubleclick" data-slot="/21630298032/Puppies"><div placeholder></div><div fallback></div></amp-ad></div>
    <div class="bones-h-product-list-item">
    <div class="row">
    <a href="/amp/dog-listing/dr-aradhna-pandey/" target="_blank" aria-label="Doggy World"><amp-img src="/checkout/images/doggy-world.jpg" title="Doggy World" alt="Doggy World"   width="302" height="252"/></a>
    </div>
    <div class="divider colored"></div>
    <div class="row">
    <a href="#" target="_blank" aria-label="Cp Vet"><amp-img src="/checkout/images/cp-vet.jpg" width="302" height="252" alt="Cp Vet"  title="Cp Vet"/></a>
    </div>
    <div class="divider colored"></div>
    <div class="row">
    <a href="/amp/dog-listing/dr-satbir-singh-josan/" target="_blank" aria-label="Joshan Pet Care"><amp-img src="/checkout/images/joshan-petcare.jpg" title="Joshan Pet Care" alt="Joshan Pet Care" width="302" height="252"  /></a>
    </div></div>
    <div class="container-fluid container-full">
	<div class="breed-selection">
       <a href="https://www.petspot.in/" aria-label="PetSpot">    
       <amp-img src="/checkout/images/petspot-680.jpg" width="320" height="98" layout="responsive" alt="PetSpot"></amp-img>
	</a></div>
  </div>
  <div class="space-2"></div>
    <div class="container-fluid container-full">
	<div class="breed-selection">
       <a href="/contactus.php" aria-label="DogSpot">    
       <amp-img src="/checkout/images/advertise-with-us-680.jpg" width="480" height="60" layout="responsive" alt="Advertise with us"></amp-img>
	</a></div>
  </div>
<div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250 type="doubleclick" data-slot="/21630298032/Puppies"><div placeholder></div><div fallback></div></amp-ad></div>
<div class="row">
			<div class="col-xs-12">
<h5>Best Services</h5>
        
<?php $city=getLocationInfoByIp(); 
 if($city['city']){
				  $city1=$city['city'];
				   $sql = "SELECT * FROM business_listing WHERE city_name LIKE '%$city1%' AND publish_status='publish'  ORDER BY business_id DESC LIMIT 0,4";}else{
$sql = "SELECT * FROM business_listing WHERE  publish_status='publish' ORDER BY business_id DESC LIMIT 0,4";
				   }
		
				$selectArt = mysql_query("$sql");
                if(!$selectArt){ die(mysql_error()); }
                while($rowArt1 = mysql_fetch_array($selectArt)){
                    $username=$rowArt1["userid"];
                    $comp_org = $rowArt1["comp_org"];
					$logo = $rowArt1["logo"];
                    $address = $rowArt1["address"];
                    $address = stripslashes($address);
                    $address =  breakLongWords($address, 30, " ");
                    $city_name = $rowArt1["city_name"];
                    $bus_nicename = $rowArt1["bus_nicename"];
                    $mobile_main = $rowArt1["mobile_main"];
					$category_name1= $rowArt1["category_name1"];
					$cat_nice_name1 = $rowArt1["cat_nice_name1"];
					$cat_nice_name2 = $rowArt1["cat_nice_name2"];
					$cat_nice_name3 = $rowArt1["cat_nice_name3"];
					$cat_nice_name4 = $rowArt1["cat_nice_name4"];
					
					 $rowUser1=query_execute_row("SELECT f_name, image FROM users WHERE userid='". $username."'");
                    if($logo){
		$src = $DOCUMENT_ROOT.'/dog-listing/images/'.$logo;
	
	$destm = $DOCUMENT_ROOT.'/imgthumb/126x126-'.$logo;
	
createImgThumbIfnot($src,$destm,'126','126','ratiowh');
$destm="https://www.dogspot.in".'/imgthumb/126x126-'.$logo;
 }else if($category_name1=='Breeder/Kennels')
 {
	$destm="https://www.dogspot.in/checkout/images/dog-breed-icon.png"; 
 }elseif($category_name1=='Grooming')
 {
	$destm="https://www.dogspot.in/checkout/images/gromming-icons.jpg";  
 }
 elseif($category_name1=='Kennel Club' || $category_name1=='Kennel Owner Breeder'  )
 {
	$destm="https://www.dogspot.in/checkout/images/kennel-club-icon.png";  
 }
elseif($category_name1=='Veterinarian')
 {
	$destm="https://www.dogspot.in/checkout/images/vet-icons.jpg";  
 }
 elseif($category_name1=='Boarding' || $category_name1=='Pet Boarding')
 {
	$destm="https://www.dogspot.in/checkout/images/day-care-small.png";  
 }
 elseif($category_name1=='Dog Trainer')
 {
	$destm="https://www.dogspot.in/checkout/images/trainer-small-icon.png";  
 }
 elseif($category_name1=='Pet Shop')
 {
	$destm="https://www.dogspot.in/checkout/images/petshop-icons.jpg";  
 }else
 {
	$destm="https://www.dogspot.in/checkout/images/other-icon.png";  
 }
?><div class="bones-h-product-list-item col-xs-12">
			<a href="/amp/dog-listing/<?=$bus_nicename?>/" class="preview">
							<amp-img src="<?=$destm?>" width="150" alt="<?=$comp_org?>" height="160" layout="responsive"></amp-img></a>
                  <a href="/amp/dog-listing/<?=$bus_nicename?>/" ><h2><? print ucfirst(substr($comp_org,0,10));?></h2>
						<div class="prices"><amp-img src="/checkout/images/Maps-icon.png"  alt="map" title="map" width="9" height="13"></amp-img><? echo trim(trim($address,',')).", ".trim(trim($city_name,',')).", +91-".trim(trim($mobile_main,',')); ?></div></a> </div><? }?></div></div> </div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-amp.php'); ?> 
</body></html>  


