<?php 
include($DOCUMENT_ROOT."/constants.php");
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
 $sitesection = "dog-business-listing"; 
function getLocationInfoByIp(){
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = @$_SERVER['REMOTE_ADDR'];
    $result  = array('country'=>'', 'city'=>'');
    if(filter_var($client, FILTER_VALIDATE_IP)){
        $ip = $client;
    }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
        $ip = $forward;
    }else{
        $ip = $remote;
    }
    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));    
    if($ip_data && $ip_data->geoplugin_countryName != null){
        $result['country'] = $ip_data->geoplugin_countryCode;
        $result['city'] = $ip_data->geoplugin_city;
    }
    return $result;
}
if($cat_nice_nameBoth){
$rowCat = getSingleRow("category_name, description, cat_type_id", "business_category", "cat_nice_name = '$cat_nice_nameBoth'");
$category_name=$rowCat["category_name"];
$description=stripslashes($rowCat["description"]);
$cat_type_id=$rowCat["cat_type_id"];
$rowBusinessCity = getSingleRow("city_name,business_id", "business_listing", "(cat_nice_name1 = '$cat_nice_nameBoth' OR cat_nice_name2 = '$cat_nice_nameBoth' OR cat_nice_name3 = '$cat_nice_nameBoth' OR cat_nice_name4 = '$cat_nice_nameBoth' OR cat_nice_name5 = '$cat_nice_nameBoth') AND city_nicename='$city_nicenameBoth'");
$city_name=$rowBusinessCity["city_name"];	
$business_id=$rowBusinessCity["business_id"];
if($city_name=='') {
$rowCity = getSingleRow("city_name", "city", "city_nicename = '$city_nicename'");
$city_name=$rowCity["city_name"];
}
$sqlall="SELECT business_id FROM business_listing WHERE (cat_nice_name1 = '$cat_nice_nameBoth' OR cat_nice_name2 = '$cat_nice_nameBoth' OR cat_nice_name3 = '$cat_nice_nameBoth' OR cat_nice_name4 = '$cat_nice_nameBoth' OR cat_nice_name5 = '$cat_nice_nameBoth') AND city_nicename = '$city_nicenameBoth'";
$sql="SELECT business_id FROM business_listing WHERE (cat_nice_name1 = '$cat_nice_nameBoth' OR cat_nice_name2 = '$cat_nice_nameBoth' OR cat_nice_name3 = '$cat_nice_nameBoth' OR cat_nice_name4 = '$cat_nice_nameBoth' OR cat_nice_name5 = '$cat_nice_nameBoth') AND city_nicename = '$city_nicenameBoth' LIMIT 0,190";
	$h1Text="$category_name in $city_name";
	$n1="$cat_nice_nameBoth";
	$titleText=  "$category_name in $city_name  | Dog Business Listings DogSpot" ;
	$DescText="$category_name in $city_name, Find $category_name Phone Numbers, Addresses, Best Deals, Latest Reviews & Ratings. Visit DogSpot for $category_name in $city_name";
	$KeyeordText="$category_name in $city_name, $category_name in $city_name, $category_name $city_name, $category_name $city_name, list of $category_name in $city_name, $category_name list in $city_name, $category_name in $city_name";
}elseif($member_id){
$sqlall="SELECT business_id FROM business_listing WHERE userid = '$member_id'";
$sql="SELECT business_id FROM business_listing WHERE userid = '$member_id' ORDER BY u_date DESC LIMIT 0,190";
	$pageT = dispUname($member_id);
	$h1Text="$sessionName";
	$titleText="$sessionName";
	$DescText="$sessionName";
	$KeyeordText="$sessionName";
	if(trim($pageT)==''){
		header("HTTP/1.0 404 Not Found");
		require_once($DOCUMENT_ROOT.'/404.php');
		die(mysql_error());
		exit();
	}
}elseif($city_nicename){
$sqlall="SELECT business_id FROM business_listing WHERE city_nicename = '$city_nicename'";
$sql="SELECT business_id FROM business_listing WHERE city_nicename = '$city_nicename' ORDER BY u_date DESC LIMIT 0, 190";
$rowBusinessCity = getSingleRow("city_name", "business_listing", "city_nicename = '$city_nicename'");
$city_name=$rowBusinessCity["city_name"];	
if($city_name=='') {
	$rowCity = getSingleRow("city_name", "city", "city_nicename = '$city_nicename'");
$city_name=$rowCity["city_name"];
}
	$h1Text="Pet Business in $city_name";
	$n1="$city_nicename";
	$titleText="Pet Business in $city_name | DogSpot";
	$KeyeordText="Kennel Owner Breeder in $city_name, Pet Shop in $city_name, Dog Trainer in $city_name, Pet Services in $city_name, Pet Boarding in $city_name, Veterinarian in $city_name, Kennel Club in $city_name | Pet Business in $city_name | DogSpot";
	$DescText="Pet Business in $city_name Find Phone Numbers, Addresses, Best Deals, Latest Reviews & Ratings of Kennel Owner Breeder, Pet Shop, Dog Trainer, Pet Services, Pet Boarding, Veterinarian, Kennel Club | Pet Business | DogSpot";
	$h1=$city_name;
}elseif($cat_nice_name){
		$rowCat = getSingleRow("category_name, description,cat_title,cat_keyword,cat_type_id", "business_category", "cat_nice_name = '$cat_nice_name'");
		$category_name = $rowCat["category_name"];
		$cat_title = $rowCat["cat_title"];
		$cat_keyword = $rowCat["cat_keyword"];
		$description = stripslashes($rowCat["description"]);
		$cat_type_id = $rowCat["cat_type_id"];
		$titleText = "$cat_title | Pet Business";
		$DescText = "$category_name, Find $category_name Phone Numbers, Addresses, Best Deals, Latest Reviews & Ratings. Visit DogSpot for $category_name";	
		$KeyeordText="$cat_keyword, list of $category_name, $category_name list, $category_name";	
		$h1Text="$category_name ";
		$n1="$category_name";
		if($cat_nice_name == "pet-shop" || $cat_nice_name == "veterinarian"){
			$cond = "ORDER BY business_id DESC";	
		}else{
			$cond = "ORDER BY u_date DESC";	
		}		
		$sqlall="SELECT business_id FROM business_listing WHERE publish_status='publish' AND cat_nice_name1 = '$cat_nice_name'";
		$sql="SELECT business_id FROM business_listing WHERE publish_status='publish' AND (cat_nice_name1 = '$cat_nice_name' OR cat_nice_name2 = '$cat_nice_name' OR cat_nice_name3 = '$cat_nice_name' OR cat_nice_name4 = '$cat_nice_name' OR cat_nice_name5 = '$cat_nice_name') $cond LIMIT 0,190";
}else{
$sqlall="SELECT business_id FROM business_listing WHERE publish_status='publish' ORDER BY u_date DESC";
$sql="SELECT business_id FROM business_listing WHERE publish_status='publish' ORDER BY u_date DESC LIMIT 0,190";
	$h1Text="Pet Business";
	$titleText="$cat_nice_nameBoth | Pet Business";
	$DescText="$cat_nice_nameBoth | Pet Business";
	$KeyeordText="$cat_nice_nameBoth | Pet Business";
}
$selectArt = mysql_query("$sql");
	if (!$selectArt){	die(mysql_error());	}

$selectArtall = mysql_query("$sqlall");
	if (!$selectArtall){	die(mysql_error());	}
$totrecord = mysql_num_rows($selectArtall);
if($totrecord=='0') {
header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	exit();	
}
while($rowArt = mysql_fetch_array($selectArt)){
	$business[]=$rowArt["business_id"];
}
?><!doctype html>
<html AMP lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$titleText?></title>
    <meta name="author" content="DogSpot" />
    <meta name="description" content="<?=$DescText?>" />
    <meta name="keywords" content="<?=$KeyeordText?>" />
    <? if($section[2]=='city'){?>
    <link rel="canonical" href="https://www.dogspot.in/dog-listing/city/<?=$section[3];?>/" />
    <? }elseif($section[2]=='page'){?>
    <link rel="canonical" href="https://www.dogspot.in/dog-listing/" />
    <? }else{?>
    <link rel="canonical" href="https://www.dogspot.in/dog-listing/category/<?=$section[3];?>/" />
    <? }?>
    <? require($DOCUMENT_ROOT.'/new/common/top-2-amp.php');  ?>
<div id="topPageStat" class="bread" itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/amp/dog-listing/" itemprop="item"><span itemprop="name">Dog Listing</span></a><span itemprop="position" content="1"></span></span><span>/</span><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="#" itemprop="item"><span itemprop="name"><?=$h1Text;?></span></a><span itemprop="position" content="2"></span></span></div>
     <div class="clfieds_list_blk" > 
	<div class="clfieds_dropdown">
    <h1 class="category_name"><?=$h1Text?></h1></div>
    <ul class="clfieds_listing"><?  if($business){
			$iid=0;$j=0;
			foreach($business as $Business_id){
			   //echo "select * from business_listing where business_id=$Business_id";
			   $query1 = mysql_query("SELECT * FROM business_listing WHERE  publish_status='publish' AND business_id = '$Business_id'");
			 	  if(!$query1){   die(mysql_error());	 }
			  		while($rowArt1 = mysql_fetch_array($query1)){
					  $iid=$iid+1;$j=$j+1;
			   		  $comp_org = $rowArt1["comp_org"];
					  $logo = $rowArt1["logo"];
			   		  $bus_nicename = $rowArt1["bus_nicename"];
					  $address = $rowArt1["address"];
					  $address = stripslashes($address);
					  $address =  breakLongWords($address, 30, " ");
			          $city_name = $rowArt1["city_name"];
					  $date = $rowArt1["c_date"];
					  $category=$rowArt1["category_name1"];
					  $username=$rowArt1["userid"];
					  $businessuserid = $rowArt1["userid"]; 
			   		  $phone_main = $rowArt1["phone_main"];
			   		  $website = $rowArt1["website"];
					  $photo_album_id = $rowArt1["photo_album_id"];
			   		  $video_album_id = $rowArt1["video_album_id"];
			   		  $num_review = $rowArt1["num_review"];
					  $category_name1 = $rowArt1["category_name1"];
					  $category_name2 = $rowArt1["category_name2"];
					  $category_name3 = $rowArt1["category_name3"];
					  $category_name4 = $rowArt1["category_name4"];
					  $category_name5 = $rowArt1["category_name5"];
					  $rowUser1=query_execute_row("SELECT f_name, image FROM users WHERE userid='". $username."'");
               if($logo){
		$src = $DOCUMENT_ROOT.'/dog-listing/images/'.$logo;
	
	$destm = $DOCUMENT_ROOT.'/imgthumb/126x126-'.$logo;
	
createImgThumbIfnot($src,$destm,'126','126','ratiowh');
$destm="https://www.dogspot.in".'/imgthumb/126x126-'.$logo;
 } else if($category_name1=='Breeder/Kennels')
 {
	$destm="https://www.dogspot.in/checkout/images/dog-breed-icon.png"; 
 }elseif($category_name1=='Grooming')
 {
	$destm="https://www.dogspot.in/checkout/images/gromming-icons.jpg";  
 }
 elseif($category_name1=='Kennel Club' || $category_name1=='Kennel Owner Breeder'  )
 {
	$destm="https://www.dogspot.in/checkout/images/kennel-club-icon.png";  
 }
elseif($category_name1=='Veterinarian')
 {
	$destm="https://www.dogspot.in/checkout/images/vet-icons.jpg";  
 }
 elseif($category_name1=='Boarding' || $category_name1=='Pet Boarding')
 {
	$destm="https://www.dogspot.in/checkout/images/day-care-small.png";  
 }
 elseif($category_name1=='Dog Trainer')
 {
	$destm="https://www.dogspot.in/checkout/images/trainer-small-icon.png";  
 }
 elseif($category_name1=='Pet Shop')
 {
	$destm="https://www.dogspot.in/checkout/images/petshop-icons.jpg";  
 }else
 {
	$destm="https://www.dogspot.in/checkout/images/other-icon.png";  
 }
		?><li><a href="<?="/amp/dog-listing/".$bus_nicename."/";?>" aria-label="<? print breakLongWords($comp_org, 8, " ");
  ?>"><div class="clfiedsimg_blk"><amp-img src="<?=$destm?>" width="110" height="110" alt="<? print breakLongWords($comp_org, 8, " "); ?>" class="clfieds_icon_shop"></div><div class="clfiedstext_blk"><div class="clfieds_shop_name"><? print breakLongWords($comp_org, 11, " ");  ?></div><div class="clfieds_shop_address"><amp-img src="/checkout/images/Maps-icon.png" width="9" height="13" alt="Map" title="Map"></amp-img><?=$address.", ". $city_name.", ". $country_name.", ". $phone_main;  ?></div></div></a></li>
          <? if($j==6){ ?><amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="5144837740" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div><? } if($j==12){ ?>
<amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="3640184387" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div><? }if($j==18){ ?>
<amp-ad width="100vw" height=320 type="adsense" data-ad-client="ca-pub-3238649592700932" data-ad-slot="1993782992" data-auto-format="rspv" data-full-width><div overflow></div></amp-ad><div class="divider-30 colored"></div><? }if($j==24){  ?>
               <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="7301949999"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? }if($j==30){ $j=0;  ?>
               <amp-ad width="100vw" height=320
     type="adsense"
     data-ad-client="ca-pub-3238649592700932"
     data-ad-slot="7301949999"
     data-auto-format="rspv"
     data-full-width>
  <div overflow></div>
</amp-ad><div class="divider-30 colored"></div><? } } }}?></ul>
    </div>
    <div class="blog-sidebar-box">
     <div class="bones-h-product-list-item">
    <div class="row">
    <a href="/amp/dog-listing/dr-aradhna-pandey/" target="_blank" aria-label="Doggy World"><amp-img src="/checkout/images/doggy-world.jpg" title="Doggy World" alt="Doggy World"   width="302" height="252"/></a>
    </div>
    <div class="divider colored"></div>
    <div class="row">
    <a href="#" target="_blank" aria-label="Cp Vet"><amp-img src="/checkout/images/cp-vet.jpg" width="302" height="252" alt="Cp Vet"  title="Cp Vet"/></a>
    </div>
    <div class="divider colored"></div>
    <div class="row">
    <a href="/amp/dog-listing/dr-satbir-singh-josan/" target="_blank" aria-label="Joshan Pet Care"><amp-img src="/checkout/images/joshan-petcare.jpg" title="Joshan Pet Care" alt="Joshan Pet Care" width="302" height="252"  /></a>
    </div></div>
    <div class="text-center margin-top-50 blog-sidebar-box"> <amp-ad width=300 height=250 type="doubleclick" data-slot="/21630298032/Puppies"><div placeholder></div><div fallback></div></amp-ad></div></div><div class="blog-sidebar-box">
                    <h3 class="margin-0">Top Grooming Center</h3>
                    <ul class="media-list">
                    <?   $city=getLocationInfoByIp(); 
			  if($city['city']){
				  $city1=$city['city'];
				   $sql = "SELECT * FROM business_listing WHERE `category_name1` LIKE '%Services%' AND city_name LIKE '%$city1%' AND publish_status='publish'  ORDER BY business_id DESC LIMIT 0,2";}else{$sql = "SELECT * FROM business_listing WHERE `category_name1` LIKE '%Services%' AND publish_status='publish' ORDER BY business_id DESC LIMIT 0,2"; }
				   //echo $sql;
				   			$selectArt = mysql_query("$sql");
                if(!$selectArt){ die(mysql_error()); }
                while($rowArt1 = mysql_fetch_array($selectArt)){
                    $username=$rowArt1["userid"];
                    $comp_org = $rowArt1["comp_org"];
                    $address = $rowArt1["address"];
                    $address = stripslashes($address);
                    $address =  breakLongWords($address, 30, " ");
                    $city_name = $rowArt1["city_name"];
                    $country_name = $rowArt1["country_name"];
                    $bus_nicename = $rowArt1["bus_nicename"];
                    $mobile_main = $rowArt1["mobile_main"];
					
					$cat_nice_name1 = $rowArt1["cat_nice_name1"];
					$cat_nice_name2 = $rowArt1["cat_nice_name2"];
					$cat_nice_name3 = $rowArt1["cat_nice_name3"];
					$cat_nice_name4 = $rowArt1["cat_nice_name4"];
					
					$rowUser1=query_execute_row("SELECT cat_logo FROM business_category WHERE (cat_nice_name='$cat_nice_name1' OR cat_nice_name='$cat_nice_name2' OR cat_nice_name='$cat_nice_name3' OR cat_nice_name='$cat_nice_name4') LIMIT 1");
                    if($rowUser1["cat_logo"]){
                        $profileimgurl='/dog-listing/images/'.$rowUser1["cat_logo"];
                    }else{
                        $profileimgurl='/dog-listing/images/photo.jpg'; 
                    }
?><li><a class="clearfix" href="/amp/dog-listing/<?=$bus_nicename?>/"><amp-img src="<?=$profileimgurl; ?>" layout="fixed" width="72" height="72" alt="<?=$comp_org?>" class="pull-left circle"></amp-img><div><span><? print ucfirst(breakLongWords($comp_org, 7, " "));?></span></div></a></li>
                  <? }?></ul>
                </div>
    </div>   
    </div>
   
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-amp.php'); ?></body></html>
