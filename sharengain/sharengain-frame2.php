<?php
header('Content-type: text/html; charset=UTF-8');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

$html = file_get_contents_curl($sharengainurl);

//parsing begins here:
$doc = new DOMDocument();
@$doc->loadHTML($html);
$nodes = $doc->getElementsByTagName('title');

//get and display what you need:
$title = $nodes->item(0)->nodeValue;

$metas = $doc->getElementsByTagName('meta');

for ($i = 0; $i < $metas->length; $i++)
{
    $meta = $metas->item($i);
    if($meta->getAttribute('name') == 'description')
        $description = $meta->getAttribute('content');
    if($meta->getAttribute('name') == 'keywords')
        $keywords = $meta->getAttribute('content');
}
$sharengainTitle=$title;
$sharengainDesc=$description;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot <?=$sharengainTitle?></title>
<meta name="description" content="DogSpot <?=$sharengainDesc?>" />

<meta property="fb:app_id" content="119973928016834" />
<meta property="og:site_name" content="indogspot"/>
<meta property="og:title" content="<?=$sharengainTitle?>" />
<meta property="og:description" content="<?=$sharengainDesc?>" />
<meta property="og:url" content="<?=$sharengainurl?>" />
<meta property="og:site_name" content="DogSpot Store" />
<meta property="og:image" content="<?=$sharengainImage?>" /> 

<meta name="twitter:title" content="<?=$sharengainTitle?>">
<meta name="twitter:description" content="<?=$sharengainDesc?>">
<meta name="twitter:image:src" content="<?=$sharengainImage?>">

<?php /*?>
<script src="/jquery/jquery-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/shaajax2.1.js"></script>
<link rel="stylesheet" type="text/css" href="/sharengain/sharengain.css" media="all"  /><?php */?>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;s
	margin-bottom: 0px;
}
</style>
<?
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<?php
include($DOCUMENT_ROOT.'/new/common/shop-new-css.php');
include($DOCUMENT_ROOT.'/new/common/shop-new-js.php');
?>
</head>
<body>
<div class="pop_banner"> 
<img src="/sharengain/pop_banner.jpg" height="400" width="400" /> </div>
<div class="pop_content">
<h2>Like Us on Facebook and get a Rs 100 off on your next purchase</h2>
<p>1: “Like” us on Facebook by clicking the options below </p>
<p>2:  A coupon code will appear on your screen, make a note of it and use if while shopping.a </p>
<span class="like_box_dog">
<div id="vmintCode" style=" padding:4px 10px;">
<fb:like href = "<?=$sharengainurl?>" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></fb:like>
<a href="http://twitter.com/share" class="twitter-share-button" data-count="" data-url="<?=$sharengainurl?>">Tweet</a>
<?php /*?><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<?php */?>
<script id="deferedjs" type="text/javascript">
var b = document.createElement('script');
b.type = 'text/javascript';
b.src = ('http://platform.twitter.com/widgets.js');
var a=document.getElementById("deferedjs");
a.parentNode.insertBefore(b,a);
</script>

<script type="text/javascript">
    function tweetIntentToVmint(intentEvent) {
		if (!intentEvent) return;
		ShaAjaxJquary('vmint-ajax.php', '#vmintCode', '', '', 'GET', 'vmintCode', '...', 'REP');
    }
	twttr.ready(function (twttr) {	
    twttr.events.bind('tweet',    tweetIntentToVmint);
	});
 </script>
<div class="g-plusone" data-href="<?=$sharengainurl?>" data-callback="plusOneClick" ></div>
</div>
</span>
<span class="like_line">'Like / Tweet / +1' to claim your exclusive coupon!</span>

</div>

<!-- Place this tag where you want the share button to render. -->


<!-- Place this tag after the last share tag. -->
<script type="text/javascript">
function plusOneClick() {
	ShaAjaxJquary('vmint-ajax.php', '#vmintCode', '', '', 'GET', 'vmintCode', '...', 'REP');
}
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

<div id="fb-root"></div>
    <script>
         window.fbAsyncInit = function () {
            FB.init({
                appId: '119973928016834',
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true  // parse XFBML
            });
			FB.Event.subscribe('edge.create', function(response) {
  				//alert('you liked this');
				
				ShaAjaxJquary('vmint-ajax.php', '#vmintCode', '', '', 'GET', 'vmintCode', '...', 'REP');
				
			});
        };
        // Load the SDK Asynchronously
        (function (d) {
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
        } (document));

    </script>
</body>
</html>