<?php
$redUrl=$refUrl;
ob_start();
session_start();

include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/facebookconnect/facebookfunctions.php");
require_once ($DOCUMENT_ROOT.'/googleconnect/openid.php');
global $database;

$openid = new LightOpenID("https://www.dogspot.in");
 
if ($openid->mode) {
    if ($openid->mode == 'cancel') {
        echo "User has canceled authentication !";
		header("Location: /");
		exit();
		
    } elseif($openid->validate()) {
        $data = $openid->getAttributes();
		
		$socialArray["so_domain"] = 'google';
		$socialArray["so_uemail"] = $data['contact/email'];
        $socialArray["so_uid"] = $data['contact/email'];
        $first = $data['namePerson/first'];
		$last = $data['namePerson/last'];
		$socialArray["so_uname"] = "$first $last";
		$socialArray["so_udob"] = $data['birthDate'];
		$socialArray["so_ugender"] = $data['person/gender'];
		$socialArray["so_rurl"] = $redUrl;
		
		$postcode=$data['contact/postalCode/home'];
		$country=$data['contact/country/home'];
		$language=$data['pref/language']; 
		$timezone=$data['pref/timezone'];
		
		echo "Identity : '".$pro_link=$openid->identity."' <br>";
		echo "User Id '".$user_id."'";
		echo "Email :'". $email."' <br>";
        echo "First name :'". $first."' <br>";
		echo "Last name :'". $last."'<br>";
		echo "DOB :'". $dob ."' <br>";
		echo "Gender : '".$gender."' <br>";
		echo "Postcode :'". $postcode."' <br>";
		echo "Country :'". $country."' <br>";
		echo "Language :'". $language."' <br>";
		echo "Timezone :'". $timezone."' ";
		//exit();
		if($email){
		social_login_register($socialArray);
		}
    } else {
        echo "The user has not logged in";
		header("Location: /login.php");
		exit();
    }
} else {
    echo "Go to index page to log in.";
	header("Location: /login.php");
	exit();
}
?>