<?php
#ini_set('display_errors',-1);
#ini_set('display_startup_errors',1);
#error_reporting(0);

$DOCUMENT_ROOT='/home/dogspot/public_html';
header('Content-type: text/html; charset=UTF-8');
include($DOCUMENT_ROOT."/database.php");
// ---------------------------------------
function makeCorrectURL($rootUrl, $requestedUrl, $requested, $blockUrl, $isNumUrl){
	$DOCUMENT_ROOT=$rootUrl;
	$pos = strpos($requestedUrl, '?');
	if($pos==0){
		if($isNumUrl){
			if(!is_numeric($isNumUrl)){
				header("HTTP/1.0 404 Not Found");
				require_once($DOCUMENT_ROOT.'/404.php');
				exit();
				
			}
		}
		if($blockUrl){
			header("HTTP/1.0 404 Not Found");
			require_once($DOCUMENT_ROOT.'/404.php');
			exit();
		}
		if($requestedUrl){
			$rest = substr($requestedUrl, -1);
			if($rest != '/'){
				header( "HTTP/1.1 301 Moved Permanently" );
				header( "Location: /$requested/" );
				exit();
			}
		}
	}
}



// ---------------------------------------

//Check for mobile device--------------------------------------------------
function is_mobile(){
	
	// Get the user agent

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	// Create an array of known mobile user agents
	// This list is from the 21 October 2010 WURFL File.
	// Most mobile devices send a pretty standard string that can be covered by
	// one of these.  I believe I have found all the agents (as of the date above)
	// that do not and have included them below.  If you use this function, you 
	// should periodically check your list against the WURFL file, available at:
	// http://wurfl.sourceforge.net/


	$mobile_agents = Array(


		"240x320",
		"acer",
		"acoon",
		"acs-",
		"abacho",
		"ahong",
		"airness",
		"alcatel",
		"amoi",	
		"android",
		"anywhereyougo.com",
		"applewebkit/525",
		"applewebkit/532",
		"asus",
		"audio",
		"au-mic",
		"avantogo",
		"becker",
		"benq",
		"bilbo",
		"bird",
		"blackberry",
		"blazer",
		"bleu",
		"cdm-",
		"compal",
		"coolpad",
		"danger",
		"dbtel",
		"dopod",
		"elaine",
		"eric",
		"etouch",
		"fly " ,
		"fly_",
		"fly-",
		"go.web",
		"goodaccess",
		"gradiente",
		"grundig",
		"haier",
		"hedy",
		"hitachi",
		"htc",
		"huawei",
		"hutchison",
		"inno",
		"ipad",
		"ipaq",
		"ipod",
		"jbrowser",
		"kddi",
		"kgt",
		"kwc",
		"lenovo",
		"lg ",
		"lg2",
		"lg3",
		"lg4",
		"lg5",
		"lg7",
		"lg8",
		"lg9",
		"lg-",
		"lge-",
		"lge9",
		"longcos",
		"maemo",
		"mercator",
		"meridian",
		"micromax",
		"midp",
		"mini",
		"mitsu",
		"mmm",
		"mmp",
		"mobi",
		"mot-",
		"moto",
		"nec-",
		"netfront",
		"newgen",
		"nexian",
		"nf-browser",
		"nintendo",
		"nitro",
		"nokia",
		"nook",
		"novarra",
		"obigo",
		"palm",
		"panasonic",
		"pantech",
		"philips",
		"phone",
		"pg-",
		"playstation",
		"pocket",
		"pt-",
		"qc-",
		"qtek",
		"rover",
		"sagem",
		"sama",
		"samu",
		"sanyo",
		"samsung",
		"sch-",
		"scooter",
		"sec-",
		"sendo",
		"sgh-",
		"sharp",
		"siemens",
		"sie-",
		"softbank",
		"sony",
		"spice",
		"sprint",
		"spv",
		"symbian",
		"tablet",
		"talkabout",
		"tcl-",
		"teleca",
		"telit",
		"tianyu",
		"tim-",
		"toshiba",
		"tsm",
		"up.browser",
		"utec",
		"utstar",
		"verykool",
		"virgin",
		"vk-",
		"voda",
		"voxtel",
		"vx",
		"wap",
		"wellco",
		"wig browser",
		"wii",
		"windows ce",
		"wireless",
		"xda",
		"xde",
		"zte"
	);

	// Pre-set $is_mobile to false.

	$mobile_browser = '0';

	// Cycle through the list in $mobile_agents to see if any of them
	// appear in $user_agent.

	foreach ($mobile_agents as $device) {

		// Check each element in $mobile_agents to see if it appears in
		// $user_agent.  If it does, set $is_mobile to true.

		if (stristr($user_agent, $device)) {

			$mobile_browser = '1';

			// break out of the foreach, we don't need to test
			// any more once we get a true value.

			break;
		}
	}

	return $mobile_browser;
}

$requestedUrl = empty($_SERVER['REQUEST_URI']) ? false : $_SERVER['REQUEST_URI'];
$requestedUrl=mysql_real_escape_string($requestedUrl);
$requested = trim($requestedUrl, "/");



//makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[0],'');

$section = explode("/", $requested);
if($section[0]=='treats-food'){
	header('Location: https://www.dogspot.in/dog-food/');
	exit();	
}
ob_start();
session_start();
if($section[0]=='amp')
{ 
switch( $section[1] ) {
case 'sales':
        $show=$section[2];
		
		include 'amp/shop/amp-product_listing.php';
		break;
	case 'dog-sales':
$show=$section[2];
		include 'amp/shop/amp-product_listing.php';
		break;
	case 'cat-sales':
$show=$section[2];
		include 'amp/shop/amp-product_listing.php';
		break;
	case 'new-arrivals':
$show=$section[2];
		include 'amp/shop/amp-product_listing.php';
		break;
	case 'brand':
              $show=$section[3];
		makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4], $show);
		include 'amp/shop/amp-brand-all.php';
		break;
	case 'dogspot':
		include '/amp/shop/amp-product_listing.php';
		exit;

	case 'pet-food':
            $show=$section[2];
		include 'amp/shop/amp-food_listing.php';
		break;
	case 'dog-store':
		include 'amp/shop/amp-dog-food.php';
		break;
	if ($section[1] == 'toy-group-dog-breeds')
		{
		$group_id = '155';
		include 'amp/dog-breeds/dog-group-breeds.php';
        exit();
		}
	if ($section[1] == 'gundog-group-dog-breeds')
		{
		$group_id = '154';
		include 'amp/dog-breeds/dog-group-breeds.php';
		exit();
		}
	if ($section[1] == 'terrier-group-dog-breeds')
		{
		$group_id = '156';
		include 'amp/dog-breeds/dog-group-breeds.php';
		exit();
		}
	if ($section[1] == 'hound-group-dog-breeds')
		{
		$group_id = '157';
		include 'amp/dog-breeds/dog-group-breeds.php';
		exit();
		}
	if ($section[1] == 'utility-group-dog-breeds')
		{
		$group_id = '160';
		include 'amp/dog-breeds/dog-group-breeds.php';
		exit();
		}
	if ($section[1] == 'pastoral-group-dog-breeds')
		{
		$group_id = '158';
		include 'amp/dog-breeds/dog-group-breeds.php';
		exit();
		}
	if ($section[1] == 'working-group-dog-breeds')
		{
		$group_id = '159';
		include 'amp/dog-breeds/dog-group-breeds.php';
		exit();
		}
		if ($section[1] == 'big-dog-breeds')
		{
		$att_id='999';
		$brdtitle='Big Dog Breeds | Large Dog Breeds | Extra Large Dog Breeds | DogSpot.In';
		$brdkey='Big Dog Breeds, Large Dog Breeds, Extra Large Dog Breeds, Large Dog, Giant Dog Breeds';
		$brddesc='Find Big Dog Breeds List and Big Dog Breeds have many unique characteristics. Learn more about Big dog breeds at DogSpot.in';
		makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');
		include 'amp/dog-breeds/breeddetail_listing-bootstrap.php';
		exit();
		}
		if ($section[1] == 'kid-friendly-dog-breeds')
		{
		$att_id='43';
		$brdtitle='Kid Friendly Dog Breeds | Top 10 Dog Breeds for Kids | DogSpot.in';
		$brdkey='Kid friendly dog breeds, Top 10 dog breeds for kids, Best Dogs for Kids,child friendly dog breeds';
		$brddesc='Find Kid Friendly Dog Breeds List and Kid Friendly Dog Breeds have many unique characteristics. Learn more about Kid Friendly dog breeds at DogSpot.in';
        makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
        include 'amp/dog-breeds/breeddetail_listing-bootstrap.php';
		exit();
		}
		
		if ($section[1] == 'cute-dog-breeds')
		{
		$att_id='53';
		$brdtitle='Cute Dog Breeds | Cutest Breeds Of Dogs | Cute Dog Breeds Medium | DogSpot.in';
		$brdkey='Cute Dog Breeds , Cutest Breeds Of Dogs , Cute Dog Breeds List, Cute Dog Breeds Small, Cute Dog Breeds In The World';
		$brddesc='Find Cute Dog Breeds List and Cute Dog Breeds have many unique characteristics. Learn more about Cute dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'amp/dog-breeds/breeddetail_listing-bootstrap.php';
		exit();
		}
		if ($section[1] == 'friendly-dog-breeds')
		{
		$att_id='33';	
		$brdtitle='Friendly Dog Breeds | Family Dog Breeds | Friendly Dog List | DogSpot.in';
		$brdkey='Friendly Dog Breeds, Family Dog Breeds, Friendly Dog List Large Friendly Dog Breeds, Small Friendly Dog Breeds';
		$brddesc='Find Friendly Dog Breeds List and Friendly Dog Breeds have many unique characteristics. Learn more about Friendly dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'amp/dog-breeds/breeddetail_listing-bootstrap.php';
		exit();
		}
		
		if ($section[1] == 'therapy-dog-breeds')
		{
			$att_id='44';
		$brdtitle='Friendly Dog Breeds | Family Dog Breeds | Friendly Dog List | DogSpot.in';
		$brdkey='Friendly Dog Breeds, Family Dog Breeds, Friendly Dog List Large Friendly Dog Breeds, Small Friendly Dog Breeds';
		$brddesc='Find Friendly Dog Breeds List and Friendly Dog Breeds have many unique characteristics. Learn more about Friendly dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'amp/dog-breeds/breeddetail_listing-bootstrap.php';
		exit();
		}
		if ($section[1] == 'guard-dog-breeds')
		{
			$att_id='19';
		$brdtitle='Guard Dog Breeds | Best Guard | DogSpot.in';
		$brdkey='Guard Dog Breeds, Best Guard Dog, Breeds Guard Dogs Best Guard Dogs';
		$brddesc='Find Guard Dog Breeds List and Guard Dog Breeds have many unique characteristics. Learn more about Guard dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'amp/dog-breeds/breeddetail_listing-bootstrap.php';
		exit();
		}
		case 'qna':
		if($section[2] == "page"){
			$show=$section[3];
			//include 'qna/index.php';
			include 'amp/qna/index.php';
			break;
		}if($section[3] == "page" && $section[2]){
			$show=$section[4];
			$cat_nicename=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'amp/qna/index.php';
			break;
		}elseif($section[2] == "filter"){
			$filter=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			include 'amp/qna/index.php';
			break;	
		}elseif($section[2] == "category"){
			$cat_nicename=$section[3];
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/amp/qna/$cat_nicename/");
			//include 'qna/index.php';
			break;
		}elseif($section[2] == "author"){
			$qnauser=$section[3];
			$show=$section[4];
			include 'amp/qna/index.php';
			//include 'qna/index.php';
			break;
		}elseif($section[2] == "search"){
			$blogsearch = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'amp/qna/qnasearch.php';
			break;
			
		}else{
			$qna_name=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'amp/qna/view.php';
			break;
			
		}
case 'dog-names':
      $exp_value=explode('-', $section[2], 2);

	        if($section[1] == "dog-names" && $section[2] == "page"){
			include 'amp/dog-names/index.php';
			break;
		}elseif($section[1] == "dog-names" && $section[2] == "male"){
			$sex=$section[2];
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4], $show);
			include 'amp/dog-names/index.php';
			break;	
		}elseif($section[1] == "dog-names" && $section[2] == "female"){
			$sex=$section[2];
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4], $show);
			include 'amp/dog-names/index.php';
			break;	
		}elseif($section[1] == "dog-names" && strlen($section[2])=='1' && !is_nan($section[2])){
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'amp/dog-names/index.php';
			break;	
		}elseif($section[1] == "dog-names" && $exp_value[0]=="male"){
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'amp/dog-names/index.php';
			break;	
		}elseif($section[1] == "dog-names" && $exp_value[0]=="female"){
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'amp/dog-names/index.php';
			break;	
		}elseif($exp_value[0] != "male" && $exp_value[0]!="female"){
			$breed_nname=$section[2];
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'amp/dog-names/index.php';
			exit;
			}
	
case 'photos':
		if($section[2] == "member"){
			$member_id = $section[3];
			$show = $section[4];
			include 'amp/photos/index.php';
			break;	
		}elseif($section[2] == "filter"){
			$filter = $section[3];
			$show=$section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			include 'amp/photos/index2.php';
			break;
		}elseif($section[2] == "page"){
			$show=$section[3];			
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'amp/photos/index.php';
			break;	
		}elseif($section[2] == "album"){
			$album_nicename = $section[3];
			$show=$section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);		
			include 'amp/photos/album.php';
		    break;	
		}elseif($section[2] == "search"){
		   $tag=$section[3];
		   $album_nicename=$section[4];
		   $show=$section[5];
		   include 'amp/photos/search.php';
		   break;
	    }elseif($section[2] == "shownext"){
		   $show = $section[3];
		   $image_nicename = $section[4];
		   include 'amp/photos/view.php';
		   break;
	    }elseif($section[2] == "breed"){
		   $breed = $section[3];
                 $show=$section[4];
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);		
			 //  $image_nicename = $section[3];
		   include 'amp/photos/index.php';
		   break;
	    }else{
		   $image_nicename = $section[2];
                 if(!$searchString){
$show=$section[3];
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
}else{
$show=$section[4];
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);

}	   include 'amp/photos/view.php';

		   exit;
	    }
case 'puppies':
		if($section[2] == "page"){
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'amp/puppies/index.php';
			break;
		}elseif($section[2] == "breeds"){
			$type = "pippy";
			include 'amp/puppies/dogbreeds.php';
			break;
		}elseif($section[2] == "breed"){
			$breed = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'amp/puppies/index.php';
			break;
		}elseif($section[2] == "category"){
			$cattype = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'amp/puppies/index.php';
			break;
		}else{$puppy_nicename=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'amp/puppies/view.php';
			exit;
		}
	case 'adoption':
		if($section[2] == "page"){
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'amp/adoption/index.php';
			break;
		}elseif($section[2] == "breeds"){
			$type = "pippy";
			include 'amp/adoption/adoptionbreeds.php';
			break;
		}elseif($section[2] == "breed"){
			$breed = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'amp/adoption/index.php';
			break;
		}else{$puppy_nicename=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'amp/adoption/view.php';
			break;
		}
			
	case 'dog-blog':
		if($section[2] == "page"){
			//include 'new/articles/articles.php';
			include 'amp/articles/index.php';
			break;	
		}elseif($section[1] == "dog-blog" && ($section[2] != "filter" && $section[2] != "search") && $section[2] != "author" ){
			$cat_nicename = $section[2];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],'');
			
			include 'amp/articles/index.php';
			break;
		}/*elseif($section[1] == "filter"){

			$filter = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			//include 'new/articles/articles1.php';
			include 'new/articles/index.php';
			break;
		}elseif($section[2] == "search"){
			$blogsearch = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			//include 'new/articles/articlesearch.php';
			include 'new/articles/articlesearch.php';
			break;
		}*/elseif($section[2] == "author"){
			$author_id = $section[3];
			$show = $section[5];
                     makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[6],'');
			include 'amp/articles/author-post.php';
			break;
		}elseif($section[1] == "dog-blog" && $section[2]==''){
			$art_name = $section[3];
			include 'amp/articles/index.php';
			break;
		}else{
			$DOCUMENT_ROOT='/home/dogspot/public_html';
			$cat_nicename = $section[2];
		     $show = $section[4];
		
             makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],'');
		    			//if($section[0]=='dog-blog'){
			include 'amp/articles/index.php';
			break;//}
		}
		
	case 'dog-listing':
	   if($section[2] == "member"){
			$member_id = $section[3];
			$show = $section[4];
			//include 'dog-listing/listing-cat.php';
			if($section[4]) { include '404.php'; break;}
			include 'amp/dog-listing/classifieds-cat.php';
			break;		
		}elseif($section[2] == "search"){
			$searchKey = $section[3];
			$show = $section[5];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],'');
			include 'dog-listing/search.php';
			break;
		}elseif($section[2] == "blogsearch"){
			$blogsearch = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			include 'new/dog-listing/bus-search.php';
			break;
			
		}elseif($section[2] == "category" && !$section[4]){
			$cat_nice_name = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			include 'amp/dog-listing/classifieds-cat.php';
			break;	
		}elseif($section[2] == "category" && $section[4] = "city"){
			$city_nicenameBoth = $section[5];
			$cat_nice_nameBoth = $section[3];
			
			include 'amp/dog-listing/classifieds-cat.php';
			break;	
		}elseif($section[2] == "city"){
			$city_nicename = $section[3];
			//$b_id = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			
			include 'amp/dog-listing/classifieds-cat.php';
			break;	
		}elseif($section[2] == "state"){
			$state_nicename = $section[3];
			$show = $section[4];
			$nice = $section[5];
			include 'dog-listing/listing-cat.php';
			break;	
		}elseif($section[1] == "country"){
			$country_nicename = $section[3];
			$show = $section[4];
			$nice=$section[5];
			include 'dog-listing/listing-cat.php';
			break;	
		}elseif($section[2] == "page"){
			$show = $section[3];
			
			include 'amp/dog-listing/classifieds-cat.php';
			break;	
		}else{
			$bus_nicename = $section[2];
			
			if($section[3]) { include '404.php'; break;}
			include 'amp/dog-listing/classifieds-view.php';
			break;
		}
		
	case 'dog-events':
		if($section[2] == "about-us"){
			//$member_id = $section[2];
			//$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');			
			include 'amp/dog-events/about-us.php';
			break;		
		}
		if($section[2] == "what-we-do"){
			//$member_id = $section[2];
			//$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');			
			include 'amp/dog-events/what-we-do.php';
			break;		
		}
		if($section[2] == "member"){
			$member_id = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],'');			
			include 'amp/dog-events/list.php';
			break;		
		}elseif($section[2] == "state"){
			$state_nicename = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'amp/dog-events/list.php';
			break;		
		}elseif($section[2] == "country"){
			$country_nicename = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'amp/dog-events/list.php';
			break;		
		}elseif($section[2] == "alpha"){
			$alpha = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'amp/dog-events/list.php';
			break;		
		}elseif($section[2] == "city"){
			$city_nicename = $section[3];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],'');
			include 'amp/dog-events/list.php';
			break;		
		}elseif($section[2] == "page"){
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			include 'amp/dog-events/list.php';
			break;		
		}elseif($section[2] == "month"){
			$mon = $section[4];
			$year = $section[3];
			$show = $section[5];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[6],$show);
			include 'amp/dog-events/list.php';
			break;		
		}
		elseif($section[2] == "show-schedules"){
			$mon = $section[4];
			$year = $section[3];
			$show = $section[5];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[6],$show);
			include 'amp/dog-events/events.php';
			break;		
		}elseif($section[2]){
			$event_nice_name = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'amp/dog-events/view.php';
			break;
		}		
	
	
	default:
		
		if($section[1]){
			$pos2 = strpos('a'.$section[1], '?');
			if($pos2==0){
				$art_name = $section[1];
				//include 'article.php';
				//include 'new/articles/articles_insidepage.php';
				include 'site-section-select.php';
				exit();
			}
		
	    makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[1],'');
		include '/amp/index.php';
		exit();
	
	}elseif($section[2]=='amp'){
		header( "HTTP/1.1 301 Moved Permanently" );
	    header( "location: https://www.dogspot.in/"); 
	    exit();
}
}
}
if($to=='dt'){
	$DogSpotDevice='DogSpotDesktop';
	session_register("DogSpotDevice");
	if($nnname!=''){
	header('Location: https://www.dogspot.in/'.$nnname);
	exit();	
		}else{
	header('Location: https://www.dogspot.in/');
	exit();
	}
	
}

if($_SESSION['DogSpotDevice']=='DogSpotDesktop'){
	$device='dt';
}else{
	//change this when mobile is update date 22/05/2014
	$device='mo';
	//$device='dt';
}
//echo $device;

	// echo $session_id;
if($device=='mo'){
	 $mobile_browser=is_mobile();
	 if ($mobile_browser > 0) {
		if(!$section[0]){
		header('Location: https://www.dogspot.in/amp/');
		exit();	
	   }else{
			include($DOCUMENT_ROOT."/database.php");
			include($DOCUMENT_ROOT."/functions.php");
			include($DOCUMENT_ROOT."/session.php");
			$checkSt=$_SERVER['REQUEST_URI'];
			$rowPagecat=query_execute_row("SELECT category_id FROM shop_category WHERE category_nicename='$section[0]' AND category_status='active'");
			$rowPageItem=query_execute_row("SELECT item_id FROM shop_items WHERE nice_name='$section[0]' AND type_id='simple'");
			$rowPageqna=query_execute_row("SELECT qna_id FROM qna_questions WHERE qna_name='$section[1]' AND qna_name!=''");
			$rowPageclub=query_execute_row("SELECT club_id FROM club_main WHERE club_nicename='$section[0]' AND club_nicename!=''");
			$rowPagesec=query_execute_row("SELECT id FROM mobile_section WHERE section_name='$section[0]' AND status='active'");
            $rowCheckMobile=query_execute_row("SELECT id FROM mobile_section WHERE section_name LIKE '%$checkSt%'  AND status='active'");
			if( strpos( $section[0], 'adoption' ) !== false ){
				if($section[1]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/");
				exit();
				}else{
				header("Location: https://www.dogspot.in/amp/$section[0]/");
				exit();
				}
			}
			if( strpos( $section[0], 'puppies' ) !== false ){
				if($section[2]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/$section[2]/");
				exit();
				}elseif($section[1]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/");
				exit();
				}else{
				header("Location: https://www.dogspot.in/amp/$section[0]/");
				exit();
				}
			}
			if( strpos( $section[0], 'dog-blog' ) !== false ){
				if($section[1]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/");
				exit();
				}else{
				header("Location: https://www.dogspot.in/amp/$section[0]/");
				exit();
				}
			}
			if( strpos( $section[0], 'dog-listing' ) !== false ){
				if($section[4]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/$section[2]/$section[3]/$section[4]/");
				exit();
				}elseif($section[3]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/$section[2]/$section[3]/");
				exit();
				}elseif($section[2]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/$section[2]/");
				exit();
				}elseif($section[1]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/");
				exit();
				}else{
				header("Location: https://www.dogspot.in/amp/$section[0]/");
				exit();
				}
			}
			if( strpos( $section[0], 'dog-events' ) !== false ){
				if($section[2]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/$section[2]/");
				exit();
				}elseif($section[1]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/");
				exit();
				}else{
				header("Location: https://www.dogspot.in/amp/$section[0]/");
				exit();
				}
			}
			if( strpos( $section[0], 'photos' ) !== false ){
				if($section[3]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/$section[2]/$section[3]/");
				exit();
				}elseif($section[2]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/$section[2]/");
				exit();
				}elseif($section[1]){
				header("Location: https://www.dogspot.in/amp/$section[0]/$section[1]/");
				exit();
				}else{
				header("Location: https://www.dogspot.in/amp/$section[0]/");
				exit();
				}
			}
			if($rowCheckMobile['id']){
				header('Location: https://m.dogspot.in'.$checkSt);
				exit();
			} if($section[0]=='cart')
{
include 'amp/shop/cart.php';
exit();

}elseif($section[0]=='shipping-address')
{
include 'amp/shop/shipping-address.php';
exit();

}elseif($section[0]=='response')
{
include 'amp/shop/order-status.php';
exit();

}elseif($section[0]=='checkoutL')
{
include 'amp/shop/checkout.php';
exit();

}elseif($section[0]=='payment')
{
include 'amp/shop/payment-options.php';
exit();

}elseif($rowPagecat['category_id']){
				header('Location: https://m.dogspot.in'.$requestedUrl);
				exit();
			}elseif($rowPageItem['item_id']){
				header('Location: https://www.dogspot.in/amp'.$requestedUrl);
				exit();
			}elseif($rowPageqna['qna_id']){
				header('Location: https://www.dogspot.in/amp'.$requestedUrl);
				exit();
			}elseif($rowPageclub['club_id']){
				header('Location: https://m.dogspot.in'.$requestedUrl);
				exit();	
			}elseif(strpos($requestedUrl, '?') != 0){
				header('Location: https://m.dogspot.in'.$requestedUrl);
				exit();	
			}
            $request=str_replace("/","",$_SERVER['REQUEST_URI']);
			$rowPagesec=query_execute_row("SELECT id FROM mobile_section WHERE section_name='$request' AND status='active'");
			if($rowPagesec['id'])
			{
			header('Location: https://m.dogspot.in/'.$request);
			exit();	
			}
            
	}
  }
	
}
//Check for mobile device END -----------------------------------
 
$requestedUrlqw = empty($_SERVER['REQUEST_URI']) ? false : $_SERVER['REQUEST_URI'];
$check_url="https://www.dogspot.in".$requestedUrlqw;
// Search Redirect section---------------------------------------------------------------------- Start
$redriectpagedetail=mysql_query("SELECT to_url FROM redirect_url WHERE from_url='$check_url' AND status='yes' AND from_url !=''");
	$redriectPageh=mysql_fetch_array($redriectpagedetail);
	$to_urlqw=$redriectPageh['to_url'];
   // $to_urlqw=$redriectPageh['to_url'];
      if($to_urlqw)
        {
header("Location: $to_urlqw", true, 301);
		exit();
		}

// Site section for micro site
 if($section[1]=='' ||  $section[0]=='huseoflife' || $section[0]=='iiptf' || $section[0]=='pedigreeoffers' || $section[0]=='dogspotoffers' || $section[0]=='tailwaggingdeals'
 || $section[0]=='petfed' || $section[0]=='febsale' || $section[0]=='k9school' || $section[0]=='galenindia2017' ){
	include($DOCUMENT_ROOT."/database.php");
	include($DOCUMENT_ROOT."/functions.php");
	if($section[0]){
		$section = explode("__", $section[0]);
		$RowUserData = getSingleRow("*", "pages", "nice_name = '$section[0]'");//replace sessionUserID with view_userId
		$section_name = $RowUserData['section_name'];
		$section_id = $RowUserData['section_id'];
		
		if($section_name == "microsite"){
			//echo $section_id;
			include 'clubs/view.php';
			exit();
		}
	}
 }
 

// Site section for micro site END 
switch( $section[0] ) {
	case 'wagtaginfolog':
		$temp='1';
		include 'wagtag/index-wt.php';
		break;
	case 'wagfund':
		$qna_name = $section[1];
		include 'wagfund1/wagfund.php';
		//include 'qna/index.php';
		break;
	case 'animal-activist':
		include 'wagfund1/animal-activist.php';
		break;
	case 'sales':
		include 'new/shop/product_listing.php';
		break;
	case 'dog-sales':
		include 'new/shop/product_listing.php';
		break;
	case 'cat-sales':
		include 'new/shop/product_listing.php';
		break;
	case 'new-arrivals':
		include 'new/shop/product_listing.php';
		break;
	
	case 'pet-food':
		include 'new/shop/food_listing.php';
		break;
	case 'dog-store':
		include 'new/shop/dog-food.php';
		break;
		
    case 'pedigree-oral-care':
	    include 'new/pedigree/pedigree-oral-care.php';
		break;
	case 'wag':
		include 'wagtag/index-wt.php';
		break;
	case 'wagtag-info':
		include 'wagtag/index-wt.php';
		break;
	case 'dogspot':
		include 'new/shop/product_listing.php';
		break;
			
	case 'wagclub':
              include 'dogs/index.php';
              break;
	case 'wagpedia':
              include '/new/breed_engine/index.php';
              break;
	case 'dog-breeds':
		makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');
		include 'new/breed_engine/index.php';
		break;
	case 'big-dog-breeds':
		$att_id='999';
		$brdtitle='Big Dog Breeds | Large Dog Breeds | Extra Large Dog Breeds | DogSpot.In';
		$brdkey='Big Dog Breeds, Large Dog Breeds, Extra Large Dog Breeds, Large Dog, Giant Dog Breeds';
		$brddesc='Find Big Dog Breeds List and Big Dog Breeds have many unique characteristics. Learn more about Big dog breeds at DogSpot.in';
		makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');
		include 'new/breed_engine/breedDetail_listing.php';
		break;
	case 'kid-friendly-dog-breeds':
		$att_id='43';
		$brdtitle='Kids Friendly Dog Breeds - Dogs for Families - DogSpot.in';
		$brdkey='Kid friendly dog breeds, Top 10 dog breeds for kids, Best Dogs for Kids,child friendly dog breeds';
		$brddesc='Find Kid Friendly Dog Breeds List and Kid Friendly Dog Breeds have many unique characteristics. Learn more about Kid Friendly dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/breedDetail_listing.php';
		break;
	case 'cute-dog-breeds':
		$att_id='53';
		$brdtitle='List of Cutest Dog Breeds - DogSpot.in';
		$brdkey='Cute Dog Breeds , Cutest Breeds Of Dogs , Cute Dog Breeds List, Cute Dog Breeds Small, Cute Dog Breeds In The World';
		$brddesc='Find Cute Dog Breeds List and Cute Dog Breeds have many unique characteristics. Learn more about Cute dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/breedDetail_listing.php';
		break;
	case 'friendly-dog-breeds':
		$att_id='33';
		$brdtitle='List of Friendly Dog Breeds - DogSpot.in';
		$brdkey='Friendly Dog Breeds, Family Dog Breeds, Friendly Dog List Large Friendly Dog Breeds, Small Friendly Dog Breeds';
		$brddesc='Find Friendly Dog Breeds List and Friendly Dog Breeds have many unique characteristics. Learn more about Friendly dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/breedDetail_listing.php';
		break;
	case 'therapy-dog-breeds':
		$att_id='44';
		$brdtitle='List of Therapy Dogs - Dog Breeds - DogSpot.in';
		$brdkey='Dog Breeds For Therapy, Therapy Dog Breeds List, Small Therapy Dog Breeds, Good Therapy Dog Breeds';
              $brddesc='Find Best therapy dogs available in India in all sizes and according to requirements. Learn more about therapy dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/breedDetail_listing.php';
		break;
 	case 'guard-dog-breeds':
		$att_id='19';
		$brdtitle='List of Guard Dog Breeds - DogSpot.in';
		$brdkey='Guard Dog Breeds, Best Guard Dog, Breeds Guard Dogs Best Guard Dogs';
		$brddesc='Find Guard Dog Breeds List and Guard Dog Breeds have many unique characteristics. Learn more about Guard dog breeds at DogSpot.in';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/breedDetail_listing.php';
		break;
	case 'toy-group-dog-breeds':
		$group_id='155';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/group-info.php';
		break;
			case 'gundog-group-dog-breeds':
		$group_id='154';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/group-info.php';
		break;
			case 'terrier-group-dog-breeds':
		$group_id='156';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/group-info.php';
		break;
			case 'hound-group-dog-breeds':
		$group_id='157';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/group-info.php';
		break;
			case 'utility-group-dog-breeds':
		$group_id='160';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/group-info.php';
		break;
			case 'pastoral-group-dog-breeds':
		$group_id='158';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/group-info.php';
		break;
			case 'working-group-dog-breeds':
		$group_id='159';
makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested,'','');		
include 'new/breed_engine/group-info.php';
		break;
	case 'recommend':
		include 'new/shop/product_listing_solr.php';
		break;
			case 'deal-of-the-day':
		include 'new/shop/deal.php';
		break;

    case 'food-subscription':
	  if($section[1] == "dog")
	  {
	  include 'food-subscription/select-dog-range.php';
			break;	  
	  }elseif($section[1] == "type")
	  {
	  $type=$section[2];
	   include 'food-subscription/dog-type.php';
	   break;		  
	  }elseif($section[1] == "pet")
	  {
	  $cat=$section[2];
	   $list=$section[3];
	   if($list=='size'){
	    $type_breed11=$section[4];
	   }elseif($list=='breed')
	   {
		$breedname=$section[4];   
	   }
	  include 'food-subscription/select-product.php';
	   break;		  
	  }elseif($section[1] == "finder")
	  {
	    $cat=$section[2];
		$finder='find';
	   $breedname=$section[3];
	   $type_breed11=$section[4];
	   $type_life=$section[5];
	  include 'food-subscription/select-product.php';
	   break;		  
	  }

elseif($section[1] == "home")
	  {
	   include 'food-subscription/home.php';
	   break;		  
	  }elseif($section[1] == "cat")
	  {
	   include 'food-subscription/product-cat-finder.php';
	   break;		  
	  }
	case 'brand':
		include 'new/shop/brand-all.php';
		break;
	case 'zipdial':
		include 'zipdial/phonevarify.php';
		break;
	case 'kennel-club':
		include 'new/dog-events/club.php';
		break;

       case 'contest':
		include 'new/contest/index.php';
		break;
	case 'qna':
		if($section[1] == "page"){
			$show=$section[2];
			//include 'qna/index.php';
			include 'new/qna/index.php';
			break;
		}elseif($section[1] == "filter"){
			$filter=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'new/qna/index2.php';
			break;	
		}elseif($section[1] == "category"){
			$cat_nicename=$section[2];
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/qna/$cat_nicename/");
			//include 'qna/index.php';
			break;
		}elseif($section[1] == "author"){
			$qnauser=$section[2];
			$show=$section[3];
			include 'new/qna/index.php';
			//include 'qna/index.php';
			break;
		}elseif($section[1] == "search"){
			$blogsearch = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'new/qna/qnasearch.php';
			break;
			
		}else{
			$qna_name=$section[1];
		//	makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			include 'new/qna/view.php';
			//include 'qna/view.php';
			break;
		}
case 'dog-names':
	$exp_value=explode('-', $section[1], 2);
	if($section[0] == "dog-names" && $section[1] == ""){
			include 'dog-names/index.php';
			break;
		}elseif($section[0] == "dog-names" && $section[1] == "male"){
			$sex=$section[1];
			$show=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3], $show);
			include 'dog-names/index.php';
			break;	
		}elseif($section[0] == "dog-names" && $section[1] == "female"){
			$sex=$section[1];
			$show=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3], $show);
			include 'dog-names/index.php';
			break;	
		}elseif($section[0] == "dog-names" && strlen($section[1])=='1' && !is_nan($section[1])){
			$show=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'dog-names/index.php';
			break;	
		}elseif($section[0] == "dog-names" && $exp_value[0]=="male"){
			$show=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'dog-names/index.php';
			break;	
		}elseif($section[0] == "dog-names" && $exp_value[0]=="female"){
			$show=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'dog-names/index.php';
			break;	
		}elseif($exp_value[0] != "male" && $exp_value[0]!="female"){
			$breed_nname=$section[1];
			$show=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'dog-names/index.php';
			break;
			}

	break;	
	case 'qna_questions':
		$qna_name = $section[1];
		include 'new/qna/index2.php';
		//include 'qna/index.php';
		break;
		
		

	case 'search':
		$photoskey = $section[1];
		$show = $section[2];
		$searchtype='ul';
		include 'search/search.php';
		break;
		
	case 'photos':
		if($section[1] == "member"){
			$member_id = $section[2];
			$show = $section[3];
			include 'photos/index.php';
			break;	
		}elseif($section[1] == "filter"){
			$filter = $section[2];
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			include 'photos/index2.php';
			break;
		}elseif($section[1] == "page"){
			$show=$section[2];			
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'photos/index.php';
			break;	
		}elseif($section[1] == "album"){
			$album_nicename = $section[2];
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);		
			include 'photos/album.php';
		    break;	
		}elseif($section[1] == "search"){
		   $tag=$section[2];
		   $album_nicename=$section[3];
		   $show=$section[4];
		   include 'photos/search.php';
		   break;
	    }elseif($section[1] == "shownext"){
		   $show = $section[2];
		   $image_nicename = $section[3];
		   include 'photos/view.php';
		   break;
	    }elseif($section[1] == "breed"){
		   $breed = $section[2];
		 //  $image_nicename = $section[3];
		   include 'photos/index.php';
		   break;
	    }else{
		   $image_nicename = $section[1];
		   makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],$show);
		   include 'photos/view.php';
		   break;
	    }


	case 'videos':
		if($section[1]=="member"){
			$member_id=$section[2];
			$show=$section[3];
			include 'videos/index.php';
			break;	
		}elseif($section[1]=="page"){
			$show=$section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'videos/index.php';
			break;	
		}elseif($section[1]=="search"){
			$tag=$section[2];
			$show=$section[3];
			include 'videos/search.php';
			break;	
		}elseif($section[1]=="album"){
			$album_nicename=$section[2];
			$show=$section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'videos/album.php';
			break;
		}elseif($section[1] == "shownext"){
			$show = $section[2];
			$video_nicename = $section[3];
			include 'videos/view.php';
			break;
		}else{
			$video_nicename = $section[1];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			include 'videos/view.php';
			break;
		}
	
	case 'profile':
		if($section[1] == "new1"){
		 $wishlist = $section[2];
		   makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
		   $status = "block";
		   include 'wag_club/user-profile.php';
		   break;
	   }elseif($section[1] == "gift"){
		 $gift = $section[1];
		   makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
		   $status = "block";
		   include 'wag_club/user-profile.php';
		   break;
	   }else{
		  $memberid = $section[1];
		  $wishlist = $section[2];
		  makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
		  include 'wag_club/user-profile.php';
		  break;
	    }
	case 'wishlist':
		   makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
		   $wishlist = "1";
		   include 'wag_club/user-profile.php';
		   break;
	   	
	case 'scrapbook':
		$member_id=$section[1];
		$show = $section[2];
		makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
		include 'scrapbook/index.php';
		break;
		
	case 'friends':
		if($section[2] == "pending"){
			$member_id = $section[1];
			$show = $section[3];
			$status = "P";
			include 'new/friends/index.php';
			break;
		}elseif($section[2] == "created"){
			$member_id = $section[1];
			$show = $section[3];
			$status = "M";
			include 'new/friends/index.php';
			break;
		}elseif($section[2] == "myfriend"){
			$member_id = $section[1];
			$show = $section[3];
			$status = "A";
			include 'new/friends/index.php';
			break;
		}
		elseif($section[2] == "suggest"){
			$member_id = $section[1];
			$show = $section[3];
			$status = "S";
			include 'new/friends/index.php';
			break;
		}else{
			$member_id = $section[1];
			$show = $section[3];
			include 'new/friends/index.php';
			break;
		}

	

		
	case 'dogs':
		if($section[1] == "page"){
			$show = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'dogs/index.php';
			break;
		}elseif($section[1] == "breeds"){
			$type = "dog";
			include 'dogbreeds.php';
			break;
		}elseif($section[1] == "breed"){
			$breed = $section[2];			
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show); 
			include 'wag_club/search.php';
			//include 'dogs/index.php';
			break;
		}elseif($section[1] == "category"){
			$cattype = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show); 
			//include 'dogs/index.php';
			include 'wag_club/search.php';
			break;
		}elseif($section[1] == "owner"){
			$doguserid = $section[2];
			$show = $section[3];
			if($section[4]) { include '404.php'; break;}
			//include 'dogs/index.php';
			include 'wag_club/search.php';
			break;
		}elseif($section[1] == "user"){
			$useridother = $section[2];
			$show = $section[3];
			if($section[4]) { include '404.php'; break;}
			//include 'dogs/index.php';
			include 'wag_club/search.php';
			break;
		}elseif($section[1] == "sort"){
			$sort = $section[2];
			$order = $section[3];
			$show = $section[4];
			include 'dogs/index.php';
			break;
		}elseif($section[2] == "location"){
			$citySearch = $section[3];
			$show = $section[4];
			include 'dogs/search.php';
			break;
		}elseif($section[2] == "breed"){
			$dog_breedSearch = $section[3];
			$show = $section[4];
			include 'dogs/search.php';
			break;
		}elseif($section[1] == "search"){
			$dog_breedSearch = $section[2];
			//$citySearch = $section[3];
			//$show = $section[4];
			//include 'dogs/search.php';
			
			include 'wag_club/search.php';

			break;
		}elseif($section[1] == "searchkeyword"){
			$searchkeyword= $section[2];
			//$citySearch = $section[3];
			//$show = $section[4];
			//include 'dogs/search.php';
			
			include 'wag_club/search.php';

			break;
		}elseif($section[1] == "citysearch"){
			//$dog_breedSearch = $section[2];
			$citySearch = $section[2];
			//$show = $section[4];
			//include 'dogs/search.php';
			
			include 'wag_club/search.php';

			break;
		}elseif($section[1] == "dogsearch"){
			$blogsearch = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'dogs/dogsearch.php';
			break;
			
		}else{
			$dog_nicename = $section[1];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			include 'wag_club/dog_profile.php'; 
			break;

			//include 'dogs/view.php';
			//break;
		}
		
       case 'wag_club':
if($section[1] == "activity"){
$activity_id_name=$section[2];
$dog_url_send=$section[3];

makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');

		include 'wag_club/comment-page.php';
		break;
}elseif($section[1] == "allsearch")
		  {
			 $search_breed=$section[2];
			 $search_location=$section[3];
			 $search_name=$section[4];
			//makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'wag_club/search.php';
			break;  
		  }

	case 'puppies':
		if($section[1] == "page"){
			$show = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'puppies/index.php';
			break;
		}elseif($section[1] == "breeds"){
			$type = "pippy";
			include 'dogbreeds.php';
			break;
		}elseif($section[1] == "breed"){
			$breed = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'puppies/index.php';
			break;
		}elseif($section[1] == "category"){
			$cattype = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'puppies/index.php';
			break;
		}elseif($section[1] == "owner"){
			$puppyUser = $section[2];
			$show = $section[3];
			include 'puppies/index.php';
			break;
		}elseif($section[1] == "sort"){
			$sort = $section[2];
			$order = $section[3];
			$show = $section[4];
			include 'puppies/index.php';
			break;
		}elseif($section[2] == "location"){
			$citySearch = $section[3];
			$show = $section[4];
			include 'puppies/search.php';
			break;
		}elseif($section[2] == "breed"){
			$dog_breedSearch = $section[3];
			$show = $section[4];
			include 'puppies/search.php';
			break;
		}elseif($section[1] == "search"){
			$dog_breedSearch = $section[2];
			$citySearch = $section[3];
			$show = $section[4];
			include 'puppies/search.php';
			break;
		}elseif($section[1] == "puppysearch"){
			$blogsearch = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');

			include 'puppies/puppysearch.php';
			break;
			
		}else{$puppy_nicename=$section[1];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			include 'puppies/view.php';
			break;
		}
	case 'adoption':
		if($section[1] == "page"){
			$show = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],$show);
			include 'adoption/index.php';
			break;
		}elseif($section[1] == "breeds"){
			$type = "pippy";
			include 'adoption/adoptionbreeds.php';
			break;
		}elseif($section[1] == "breed"){
			$breed = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'adoption/index.php';
			break;
		}elseif($section[1] == "category"){
			$cattype = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'adoption/index.php';
			break;
		}elseif($section[1] == "owner"){
			$puppyUser = $section[2];
			$show = $section[3];
			include 'adoption/index.php';
			break;
		}elseif($section[1] == "sort"){
			$sort = $section[2];
			$order = $section[3];
			$show = $section[4];
			include 'adoption/index.php';
			break;
		}elseif($section[2] == "location"){
			$citySearch = $section[3];
			$show = $section[4];
			include 'adoption/search.php';
			break;
		}elseif($section[2] == "breed"){
			$dog_breedSearch = $section[3];
			$show = $section[4];
			include 'adoption/search.php';
			break;
		}elseif($section[1] == "source"){
			$source = $section[2];
			//$show = $section[4];
			include 'adoption/search.php';
			break;
		}elseif($section[1] == "search"){
			$dog_breedSearch = $section[2];
			$citySearch = $section[3];
			$dog_statusSearch=$section[4];
			//$show = $section[4];
			include 'adoption/search.php';
			break;
		}elseif($section[1] == "puppysearch"){
			$blogsearch = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'adoption/puppysearch.php';
			break;
			
		}else{$puppy_nicename=$section[1];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			include 'adoption/view.php';
			break;
		}
			
	case 'dog-blog':
		if($section[1] == "page"){
			//include 'new/articles/articles.php';
			include 'new/articles/articles.php';
			break;	
		}elseif($section[1] == "category"){
			$cat_nicename = $section[2];
	header( "HTTP/1.1 301 Moved Permanently" );
	header("Location: https://www.dogspot.in/dog-blog/$cat_nicename/");
	exit();
	}elseif($section[0] == "dog-blog" && ($section[1] != "filter" && $section[1] != "search") && $section[1] != "author" ){
			$cat_nicename = $section[1];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			//include 'new/articles/articles1.php';
			include 'new/articles/articles.php';
			break;
		}/*elseif($section[1] == "filter"){

			$filter = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			//include 'new/articles/articles1.php';
			include 'new/articles/articles.php';
			break;
		}*/elseif($section[1] == "search"){
			$blogsearch = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			//include 'new/articles/articlesearch.php';
			include 'new/articles/articlesearch.php';
			break;
		}elseif($section[1] == "author"){
			$author_id = $section[2];
			//$show = $section[3];
			include 'new/articles/author-post.php';
			break;
		}elseif($section[1] == "approve"){
			$art_name = $section[2];
			include 'new/articles/new-approved.php';
			break;
		}elseif($section[0] == "dog-blog" && $section[1]==''){
			$art_name = $section[2];
			include 'new/articles/articles.php';
			break;
		}else{
			$DOCUMENT_ROOT='/home/dogspot/public_html';
			$cat_nicename = $section[2];
			//if($section[0]=='dog-blog'){
			include 'new/articles/articles.php';
			break;//}
		}
		
	case 'dog-listing':
		if($section[1] == "member"){
			$member_id = $section[2];
			$show = $section[3];
			//include 'dog-listing/listing-cat.php';
			if($section[3]) { include '404.php'; break;}
			include 'new/dog-listing/listing-cat.php';
			break;		
		}elseif($section[1] == "search"){
			$searchKey = $section[2];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');
			include 'dog-listing/search.php';
			break;
		}elseif($section[1] == "blogsearch"){
			$blogsearch = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'new/dog-listing/bus-search.php';
			break;
			
		}elseif($section[1] == "category" && !$section[3]){
			$cat_nice_name = $section[2];
			include 'new/dog-listing/listing-cat.php';
			break;	
		}elseif($section[1] == "category" && $section[3] = "city"){
			$city_nicenameBoth = $section[4];
			$cat_nice_nameBoth = $section[2];
			
			include 'new/dog-listing/listing-cat.php';
			break;	
		}elseif($section[1] == "city"){
			$city_nicename = $section[2];
			//$b_id = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			
			include 'new/dog-listing/listing-cat.php';
			break;	
		}elseif($section[1] == "state"){
			$state_nicename = $section[2];
			$show = $section[3];
			$nice = $section[4];
			include 'dog-listing/listing-cat.php';
			break;	
		}elseif($section[1] == "country"){
			$country_nicename = $section[2];
			$show = $section[3];
			$nice=$section[4];
			include 'dog-listing/listing-cat.php';
			break;	
		}elseif($section[1] == "page"){
			$show = $section[2];
			
			include 'new/dog-listing/listing-cat.php';
			break;	
		}else{
			$bus_nicename = $section[1];
			
			if($section[2]) { include '404.php'; break;}
			include 'new/dog-listing/view.php';
			break;
		}
		
	case 'dog-events':
		if($section[1] == "about-us"){
			//$member_id = $section[2];
			//$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');			
			include 'dog-show/about-us.php';
			break;		
		}
		if($section[1] == "what-we-do"){
			//$member_id = $section[2];
			//$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');			
			include 'dog-show/what-we-do.php';
			break;		
		}
		if($section[1] == "member"){
			$member_id = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],'');			
			include 'dog-events/list.php';
			break;		
		}elseif($section[1] == "state"){
			$state_nicename = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'new/dog-events/list.php';
			break;		
		}elseif($section[1] == "country"){
			$country_nicename = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'dog-events/list.php';
			break;		
		}elseif($section[1] == "alpha"){
			$alpha = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[4],$show);
			include 'dog-events/list.php';
			break;		
		}elseif($section[1] == "city"){
			$city_nicename = $section[2];
			$show = $section[3];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'new/dog-events/list.php';
			break;		
		}elseif($section[1] == "page"){
			$show = $section[2];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'new/dog-events/list.php';
			break;		
		}elseif($section[1] == "month"){
			$mon = $section[3];
			$year = $section[2];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'new/dog-events/list.php';
			break;		
		}
		elseif($section[1] == "show-schedules"){
			$mon = $section[3];
			$year = $section[2];
			$show = $section[4];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[5],$show);
			include 'new/dog-events/events.php';
			break;		
		}elseif($section[1]){
			$event_nice_name = $section[1];
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			include 'new/dog-events/view.php';
			break;
		}		
	
		case 'poll':
		if($section[1] != ""  && !$section[2]){
			$q_niceName = $section[1];
			include 'poll/view.php';
			break;
		 }elseif($section[2] == "vote"){
			$q_niceName = $section[1];
			include 'poll/vote.php';
			break;
		}elseif($section[2] == "edit"){
			$q_niceName = $section[1];
			include 'poll/create_poll.php';
			break;
		 }else{
			include 'poll/index.php';
			break;
		  }
		  
	
	case 'mobile-photos':
		if($section[1] == "member"){
			$member_id = $section[2];
			$show = $section[3];
			include 'mobile-photos/index.php';
			break;
		}elseif($section[1] == "page"){
			$show = $section[2];
			include 'mobile-photos/index.php';
			break;
		}elseif($section[1] == "all-photos"){
			$show = $section[3];
			include 'mobile-photos/all-photos.php';
			break;
		}else{
           include 'mobile-photos/index.php';
		   break;
		}
		
	case 'clubs':
		   $clubNiceName = $section[1];
           include 'clubs/view.php';
		   break;
	
	case 'royalcanin':
		 //$art_name = $section[1];
         include 'royalcanin/index.php';
		 break;		 
		 
	/*case 'shop':
		if($section[1] == "category"){
			//$show = $section[4];			
			//if($section[3]) { include '404.php'; break;}
			//include 'shop/category-list.php';
			//include 'new/shop/category.php';
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'new/shop/category-chq.php';
		 	break;
		}elseif($section[1] == "brand" && !$section[2]){
			$show = $section[4];
			//include 'shop/brand-all.php';
			include 'new/shop/brand-all.php';
		 	break;
		}elseif($section[1] == "brand"){
			//if($section[3]) { include '404.php'; break;}
			//$show = $section[4];
			//include 'shop/brand.php';
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[3],'');
			include 'new/shop/brand.php';
		 	break;
		}else{
			makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[2],'');
			include 'new/shop/product-individual.php';
		 	break;
		} */      	 
	
	default:
		
		if($section[0]){
			$pos2 = strpos('a'.$section[0], '?');
			if($pos2==0){
				$art_name = $section[0];
				//include 'article.php';
				//include 'new/articles/articles_insidepage.php';
				include 'site-section-select.php';
				exit();
			}
		}else
		{
			if($section[0] == '0'){
	header( "HTTP/1.1 301 Moved Permanently" );
	header( "location: https://www.dogspot.in/"); 
	die(mysql_error());
	exit();
}elseif($section[0] == 'amp')
{
	    makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[1],'');
		include '/amp/index.php';
		exit();
	
}
		}
		
		makeCorrectURL($DOCUMENT_ROOT, $requestedUrl, $requested, $section[1],'');
		include 'new/index.php';
		//include 'home.php';
		//include 'shop/index.php';
		break;
}
//}
?>


