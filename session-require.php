<?php 
ob_start();
session_start();
//get the referrer
if($_SERVER['REQUEST_URI']){
	$refUrl = $_SERVER['REQUEST_URI'];
}else{
	$refUrl = '/';
}

//save it in a session
$_SESSION['refUrl'] = $refUrl; 
// store session data
$userid = $_SESSION['sessionuserid'];
$sessionName = $_SESSION['sessionName'];
$sessionDPid = $_SESSION['sessionDPid'];
$sessionLevel = $_SESSION['sessionLevel'];
$sessionProfileImg = $_SESSION['sessionProfileImg'];
$sessionFacebook = $_SESSION['sessionFacebook'];
$sessionTwitter = $_SESSION['sessionTwitter'];
$sessionSocialaction = $_SESSION['sessionSocialaction'];

/*if(isset($_COOKIE['cookid']) && isset($_COOKIE['cookLevel']) && isset($_COOKIE['cookName'])){
         $userid = $_SESSION['sessionuserid'] = $_COOKIE['cookid'];
		 $sessionDPid = $_SESSION['sessionDPid'] = $_COOKIE['DPid'];
		 $sessionName = $_SESSION['sessionName'] = $_COOKIE['cookName'];
         $sessionLevel   = $_SESSION['sessionLevel']   = $_COOKIE['cookLevel'];
		 $sessionProfileImg   = $_SESSION['sessionProfileImg']   = $_COOKIE['cookProfileImg'];
		 $sessionFacebook   = $_SESSION['sessionFacebook']   = $_COOKIE['cookFacebook'];
		 $sessionTwitter   = $_SESSION['sessionTwitter']   = $_COOKIE['cookTwitter'];
		 $sessionSocialaction   = $_SESSION['sessionSocialaction']   = $_COOKIE['cookSocialaction'];
}*/

if (!$userid){
	$userid="Guest";
	header("Location: /login.php");
	ob_end_flush();
	exit(); 
}
?>