/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/
 
//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;
 
//loading popup with jQuery magic!
function loadPopup(divPopup){
	if(!divPopup){
		divPopup='#popupContact';
	}
	//loads popup only if it is disabled
	if(popupStatus==0){
		jQuery("#backgroundPopup").css({
			"opacity": "0.7"
		});
		jQuery("#backgroundPopup").fadeIn("slow");
		jQuery(divPopup).fadeIn("slow");
		popupStatus = 1;
	}
}
 
 function loadPopupimage(imageid,backgraundid){
	//loads popup only if it is disabled
	if(popupStatus==0){
		jQuery(backgraundid).css({
			"opacity": "0.7"
		});
		jQuery(backgraundid).fadeIn("slow");
		jQuery(imageid).fadeIn("slow");
		popupStatus = 1;
	}
}

function centerPopupimage(imageid,backgraundid){
	//request data for centering
	var windowWidth = jQuery(window).width();
	var windowHeight = jQuery(window).height();
	var popupHeight = jQuery(imageid).height();
	var popupWidth = jQuery(imageid).width();
	//centering
	jQuery(imageid).css({
		"position": "fixed",
		"bottom": "1px",
		"bottom": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	jQuery(backgraundid).css({
		"height": windowHeight
	});
	
}




 
//disabling popup with jQuery magic!
function disablePopupimage(imageid,backgraundid){
	//disables popup only if it is enabled
	if(popupStatus==1){
		jQuery(backgraundid).fadeOut("slow");
		jQuery(imageid).fadeOut("slow");
		popupStatus = 0;
	}
}
 
 
 function disablePopup(divPopup){
	 if(!divPopup){
		divPopup='#popupContact';
	}
	//disables popup only if it is enabled
	if(popupStatus==1){
		jQuery("#backgroundPopup").fadeOut("slow");
		jQuery(divPopup).fadeOut("slow");
		popupStatus = 0;
	}
}
//centering popup
function centerPopup(divPopup){
	if(!divPopup){
		divPopup='#popupContact';
	}
	//request data for centering
	var windowWidth = jQuery(window).width();
	var windowHeight = jQuery(window).height();
	var popupHeight = jQuery(divPopup).height();
	var popupWidth = jQuery(divPopup).width();
	//centering
	jQuery(divPopup).css({
		//"position": "fixed",
		//"bottom": "1px",
		"bottom": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	jQuery("#backgroundPopup").css({
		"height": windowHeight
	});
	
}
 

 
//CONTROLLING EVENTS IN jQuery
jQuery(document).ready(function(){
	
	//LOADING POPUP
	//Click the button event!
	jQuery("#loadpopupevent").click(function(){
		//centering with css
		centerPopup();
		//load popup
		loadPopup();
	});
				
	//CLOSING POPUP
	//Click the x event!
	jQuery("#popupContactClose").click(function(){
		disablePopup();
	});
	//Click out event!
//	jQuery("#backgroundPopup").click(function(){
//		disablePopup();
//	});
	//Press Escape event!
	jQuery(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup();
		}
	});
 
});