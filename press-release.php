<?php

// address_type_id = 1 for Shipping Address
// address_type_id = 2 for Billing Address

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();
 if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/press-release-bootstrap.php');
  exit;
  }


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot.in launches India's largest exclusive Pet Products E-Shop | Press Release</title>
<meta name="keywords" content="DogSpot.in launches India's largest exclusive Pet Products E-Shop | Press Release" />
<meta name="description" content="DogSpot.in launches India's largest exclusive Pet Products E-Shop | Press Release | DogSpot" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>
<![endif]-->

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>
<div class="cont980">
<div class="vs10"></div>
<div style="float:left; width:170px;">
<div id="leftnav">
<a href="/about-us.php" class="userbutton">About Us</a>
<a href="/press-release.php" class="userbutton">Press Release</a>
<a href="/contactus.php" class="userbutton">Contact Us</a>
</div>
</div>
<div style="float:right; margin-left:5px; margin-top:5px; width:780px;">
  <h1>DogSpot.in launches India's largest exclusive Pet Products E-Shop</h1>
  <h2> Targets a break-even by 2015 with a sales revenue of Rs. 20 Crores</h2></br>
  <p><strong>New Delhi, July 15, 2011:</strong> Whoever  coined the phase – &lsquo;leading a dog&rsquo;s life,&rsquo; sure wasn&rsquo;t smart enough to get the  drift entirely. Crafted by dog enthusiasts, Dogspot.in is an idea developed  &amp; mastered by a team with passion for dogs. Today <a href="https://www.dogspot.in/">www.dogspot.in</a> is an online pet products e-shop  addressing all requirements for dogs &amp; dogs related queries. This online  platform not only offers products &amp; services but makes up for a platform for  meaningful exchange of ideas &amp; information surrounding various pet lovers. Having  achieved the distinction of being <strong>The  No. 1 Dog Portal of India</strong>, team DogSpot.in has embarked on yet another  journey to provide Pet Products and Accessories to dog lovers. What started as  a modest story few years back has in a span of 3 years managed to be on of the most  promising startup story in India! <strong>(Outlook  Money)</strong></p></br>
  <p>DogSpot.in  is a one stop platform for all dog &amp; dog related needs.&nbsp; It aspires to  solve problems in the dog world, by aggregating and organizing information  &amp; products, bridging gaps and hence bringing the online pet lover&rsquo;s community  closer. With a million page views a month and a year on year growth of over  200%,  DogSpot.in is not only India&rsquo;s  most visited Dog portal but also the most content rich website in its domain. </p></br>
  <p>Starting July,  DogSpot.in will launch <strong>India&rsquo;s largest  exclusive Pet Products E-Shop</strong>. With a rich collection of over 700 products,  the E-Shop aims at making it convenient for dog lovers to buy their favorite products  online. The E-Shop will be only selling Pet Products &amp; Accessories though no  live pets will be sold. The company aims to breakeven with sales revenue of Rs.  20 crores in 2015. The Indian Pet market is pegged at Rs. 800 crore while  witnessing a double digit growth owing to it low sales base. </p></br>
  <p>On the  launch, Mr<strong>. Rana Atheya, Co-founder of  Dogspot.in</strong> pointed out that, &ldquo;DogSpot.in is fast moving towards its tipping  point and its time our consumers get the convenience and flexibility of online  shopping for their favorite pet products. This is yet another pioneering step  towards providing our customers with an organized platform for all their dog&rsquo;s  needs&rdquo;</p></br>
  <p>With a  start of over 700 SKU&rsquo;s, DogSpot.in intends to add over 15 SKU&rsquo;s every month to  be able to address most needs of dog lovers by the end of the first year  itself. In the past, DogSpot.in has associated itself with to create value for  all engaged in the&nbsp;Indian Pet industry. DogSpot.in promises delivery  within 7-10 days, however, the team is confident of exceeding the expectations  here. DogSpot has tied up with key distributors in India to ensure timely  availability of products.  </p></br>
  <p>While Pet  Shops and Veterinary Clinics will continue to be the leading channels of  distribution, being the first E-shop, DogSpot surely will play a role in  organizing the otherwise unorganized market. </p>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
