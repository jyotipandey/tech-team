<?php

function make_solr_xml($input_file, $output_file, $delimiter){
	$handle = fopen($input_file, "r");
	$out_handle = fopen($output_file, "w");
	fwrite($out_handle,"<add>\n");
	$fields = fgetcsv($handle, 0, $delimiter);
	
	while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
		fwrite($out_handle,"<doc>\n");
	    $num = count($data);
    	for ($c=0; $c < $num; $c++) {
			//$stripped_data = strip_tags($data[$c]);
			//$stripped_data = htmlentities($stripped_data);
			$stripped_data = utf8_encode(htmlspecialchars(trim($data[$c])));
			$stripped_data = preg_replace('@[\x00-\x08\x0B\x0C\x0E-\x1F]@', ' ', $stripped_data);
			if($fields[$c] == "created_at" || $fields[$c] == "updated_at"){
				list($datedate, $crapdata) = split(" ", $stripped_data);
				$stripped_data=$datedate.'T'.$crapdata.'Z'; 
			}			
			fwrite($out_handle,"<field name=\"$fields[$c]\">$stripped_data</field>\n");
    	}
		fwrite($out_handle,"</doc>\n");
	}
	fwrite($out_handle,"</add>");
	fclose($out_handle);
	fclose($handle);
}

if($argc<3){
 # print "Usage: ./generate_solr_xml.php <input-filename> <output-filename>";
  sys.exit(1);
}

$input_file = $argv[1];
$output_file = $argv[2];
make_solr_xml($input_file,$output_file,"\t");

?>
