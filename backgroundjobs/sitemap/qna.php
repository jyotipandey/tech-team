<?php
$baseURL='/home/dogspot/public_html';
require_once($baseURL.'/database.php');
require_once($baseURL.'/functions.php');


$outputdata='<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
        xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';	
		
$xmldate=date("Y-m-d H:m:s");
$xmldate=explode(' ',$xmldate);
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/sales/</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>1.0</priority>
</url>
<url>
<loc>https://www.dogspot.in/contactus.php</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>1.0</priority>
</url>
<url>
<loc>https://www.dogspot.in/about-us.php</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>1.0</priority>
</url>
<url>
<loc>https://www.dogspot.in/animal-activist/</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>1.0</priority>
</url>';

$outputdata.='
<url>
<loc>https://www.dogspot.in/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>always</changefreq>
<priority>1.0</priority>
</url>
<url>
<loc>https://www.dogspot.in/adoption/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>always</changefreq>
<priority>0.9</priority>
</url>
<url>
<loc>https://www.dogspot.in/wag_club/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>weekly</changefreq>
<priority>0.8</priority>
</url>
';

////////////////////////////////////////////////////General ///////////////////////////
//////////////////////////////////////////////PHoto Breed///////////////////////
$getbreed=query_execute("SELECT DISTINCT(breed_name),nicename FROM photos_image as pi,photos_album as pa,dog_breeds as db WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE CONCAT('%',breed_name,'%') ORDER by breed_name ASC");
			  while($breedlist=mysql_fetch_array($getbreed)){

				  $breedI=query_execute_row("SELECT count(image_id) as id FROM photos_image as pi,photos_album as pa WHERE  pa.album_id=pi.album_id AND album_link_name='event' AND publish_status='publish' AND new_keywords LIKE '%".$breedlist['breed_name']."%'");
			  if($breedI['id']>20)
			  {
				 $breed_nameList[]=$breedlist['nicename'];
			  }}
			//  $breed_nameListS=sort($breed_nameList);
			  foreach($breed_nameList as $breedV){
				//$breedT=explode('@@',$breedV);
$breedIT=query_execute_row("SELECT id FROM mobile_section WHERE  section_name LIKE '%/photos/breed/".$breedV."%'");
if(!$breedIT['id'])
{
$insert=query_execute("INSERT INTO `mobile_section` (`section_name`, `status`) VALUES ('/photos/breed/".$breedV."/', 'active');");	
}
$outputdata.='
<url>
<loc>https://www.dogspot.in/photos/breed/'.$breedV.'/</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>1.0</priority>
</url>
';
}
////////////////////////////////////////////////////PHoto Breed ///////////////////////////
//////////////////////////////////////////////Cat Adotion in city///////////////////////
/*$result1 = mysql_query("SELECT nice_name,city FROM section_nice_name WHERE trend_id='123'");
while($rowArt1 = mysql_fetch_array($result1)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z';
 $city=$rowArt1['city'];
	//$breed_name=$rowArt1['breed_name'];

	$getbreed=mysql_query("SELECT puppi_id FROM puppies_available WHERE city_nicename='$city' AND puppi_breed='cat' AND publish_status='publish' AND adoption='yes'");
  $num_rows=mysql_num_rows($getbreed);
if($num_rows>0){

$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt1["nice_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
}
}
////////////////////////////////////////////////////Cat Adotion in city ///////////////////////////
//////////////////////////////////////////////Breed Adotion in city///////////////////////
$result2 = mysql_query("SELECT nice_name,city,breed_name FROM section_nice_name WHERE trend_id='122'");
while($rowArt2 = mysql_fetch_array($result2)){
	$city=$rowArt2['city'];
	$breed_name=$rowArt2['breed_name'];
	//echo "SELECT puppi_id FROM puppies_available WHERE city_nicename='$city' AND breed_nicename='$breed_name' AND publish_status='publish'";
	$getbreed1=mysql_query("SELECT puppi_id FROM puppies_available WHERE city_nicename='$city' AND breed_nicename='$breed_name' AND publish_status='publish' AND adoption='yes'");
  $num_rows1=mysql_num_rows($getbreed1);
if($num_rows1>0){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt2["nice_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
}
}*/

////////////////////////////////////////////////////Breed Adotion in city ///////////////////////////
//////////////////////////////////////////////Breed Engine comparison in city///////////////////////
$selectsqlb=query_execute("SELECT breed_name, nicename FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC");
while($getda=mysql_fetch_array($selectsqlb)){
	$bcsd[]=$getda['nicename'];
}
$v=0;
for($c=0;$c<count($bcsd);$c++){
		$breedn=$bcsd[$c];
		$v++;
		for($c1=$v;$c1<count($bcsd);$c1++){
			$nnname=$breedn."-vs-".$bcsd[$c1]."-compare";
$xmldate=date("Y-m-d H:m:s");
$xmldate=explode(' ',$xmldate);
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$nnname.'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.6</priority>
</url>';
}}

////////////////////////////////////////////////////Breed Adotion in city ///////////////////////////
//////////////////////////////////////////////Breed Engine comparison in city///////////////////////
$result3 = mysql_query("SELECT nicename,image_name,breed_name FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC");
while($rowArt3 = mysql_fetch_array($result3)){
	$xmldate=date("Y-m-d H:m:s");
$xmldate=explode(' ',$xmldate);
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$nameb = preg_replace('/\&/', ' ', $rowArt3['breed_name']);
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt3["nicename"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.6</priority>
<image:image>
     <image:loc>https://www.dogspot.in/new/breed_engine/images/dog_images/'.$rowArt3["image_name"].'</image:loc>
	 <image:title>'.$nameb.'</image:title>
   </image:image>
</url>';
}
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-breeds/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.7</priority>
</url>
<url>
<loc>https://www.dogspot.in/big-dog-breeds/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.6</priority>
</url>
<url>
<loc>https://www.dogspot.in/therapy-dog-breeds/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.6</priority>
</url>
<url>
<loc>https://www.dogspot.in/friendly-dog-breeds/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.6</priority>
</url>
<url>
<loc>https://www.dogspot.in/cute-dog-breeds/</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>0.6</priority>
</url>
<url>
<loc>https://www.dogspot.in/kid-friendly-dog-breeds/</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>0.6</priority>
</url>
<url>
<loc>https://www.dogspot.in/guard-dog-breeds/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.6</priority>
</url>';
////////////////////////////////////////////////////Breed/////////////////////////////
////////////////////////////////////////////////////Breed///////////////////////////////
$result4 = mysql_query("SELECT category_nicename FROM shop_category where category_status='active'");
while($rowArt4 = mysql_fetch_array($result4)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt4["category_nicename"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Daily</changefreq>
<priority>0.9</priority>
</url>';
}
$result5 = mysql_query("SELECT url_index FROM index_sitemap");
while($rowArt5 = mysql_fetch_array($result5)){		
$xmldate=date("Y-m-d H:m:s");
$xmldate=explode(' ',$xmldate);
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>'.$rowArt5['url_index'].'</loc>
<lastmod>'.$e_date.'</lastmod>
<priority>1.0</priority>
</url>
';
}
////////////////////////////////////////////////////Breed/////////////////////////////
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.9</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/a/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/b/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/c/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/d/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/e/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/f/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/g/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/h/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/i/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/j/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/k/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/l/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/m/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/n/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/o/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/p/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/q/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/r/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/s/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/t/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/u/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/v/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/w/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/x/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/y/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/z/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/male/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/female/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';		
		
$result6 = mysql_query("SELECT breed_nicename, dog_breed FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1'  GROUP BY dog_breed ORDER BY dog_breed ASC");

while($rowArt6 = mysql_fetch_array($result6)){

$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/'.$rowArt6['breed_nicename'].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
}

$result7 = mysql_query("SELECT breed_nicename FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex='M' GROUP BY breed_nicename ORDER BY dog_breed ASC");
while($rowArt7 = mysql_fetch_array($result7)){

$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$url_str='male'.'-'.$rowArt7['breed_nicename'];
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/'.$url_str.'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
}

$result8 = mysql_query("SELECT breed_nicename FROM dogs_available WHERE publish_status='publish' AND dog_name!='' AND dog_breed!='' AND CHAR_LENGTH(dog_name) <= 18 AND CHAR_LENGTH(dog_name) >= 3 AND dog_name NOT REGEXP '[0-9\'-]+' AND LENGTH(dog_name) - LENGTH(REPLACE(dog_name, ' ', ''))+1='1' AND dog_sex='F' GROUP BY breed_nicename ORDER BY dog_breed ASC");
while($rowArt8 = mysql_fetch_array($result8)){

$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$url_str_female='female'.'-'.$rowArt8['breed_nicename'];
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-names/'.$url_str_female.'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>
</url>';
}

$outputdata.='
<url>
<loc>https://www.dogspot.in/qna/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>daily</changefreq>
<priority>0.8</priority>
</url>';
$result8 = mysql_query("SELECT cat_nicename FROM qna_cat ");
while($rowArt8 = mysql_fetch_array($result8)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/qna/'.$rowArt8["cat_nicename"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>weekly</changefreq>
<priority>0.7</priority>
</url>';
}
// Create Sitemap for the Posts--------------------------
$result9 = mysql_query("SELECT qna_cdate, qna_edate, qna_name FROM  qna_questions WHERE publish_status = 'publish'  ORDER BY qna_edate DESC");
while($rowArt9 = mysql_fetch_array($result9)){
$xmldate=date("Y-m-d H:m:s");
$xmldate=explode(' ',$xmldate);
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/qna/'.$rowArt9["qna_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>weekly</changefreq>
<priority>0.7</priority>
</url>';
}
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-listing/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.6</priority>
</url>';		
		
$result10= mysql_query("SELECT cat_nice_name FROM business_category ");
while($rowArt10 = mysql_fetch_array($result10)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-listing/category/'.$rowArt10["cat_nice_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>yearly</changefreq>
<priority>0.5</priority>
</url>';
}
// Create Sitemap for the Posts--------------------------
$result11 = mysql_query("SELECT c_date, u_date, bus_nicename FROM business_listing where publish_status='publish' ORDER BY u_date DESC");
while($rowArt11 = mysql_fetch_array($result11)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-listing/'.$rowArt11["bus_nicename"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>yearly</changefreq>
<priority>0.5</priority>
</url>';
}
$outputdata.='
<url>
<loc>https://www.dogspot.in/brand/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>yearly</changefreq>
<priority>0.5</priority>
</url>';

////////////////////////////////////////////////////Breed/////////////////////////////
$result13 = mysql_query("SELECT brand_name,brand_nice_name,brand_logo FROM shop_brand WHERE brand_status='1'");
while($rowArt13 = mysql_fetch_array($result13)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$album_idall[]='';
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.strtolower($rowArt13["brand_nice_name"]).'/</loc>
<image:image>
     <image:loc>https://www.dogspot.in/imgthumb/360x130-'.$rowArt13["brand_logo"].'</image:loc>
</image:image>

<lastmod>'.$e_date.'</lastmod>

<changefreq>Weekly</changefreq>
<priority>0.7</priority>
</url>';
}
////////////////////////////////////////////Brand////////////////////////////////////////////
$outputdata.='
<url>
<loc>https://www.dogspot.in/photos/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>weekly</changefreq>
<priority>0.5</priority>
</url>';		
// Create Sitemap for the Posts--------------------------
$result14 = mysql_query("SELECT pa.cdate, pa.u_date, album_nicename,pa.album_id,alb_display_on,count(pi.image) as a FROM photos_album as pa,photos_image as pi WHERE pi.album_id=pa.album_id AND  publish_status='publish' group by pa.album_id ");

while($rowArt14 = mysql_fetch_array($result14)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
if($rowArt14['a']>10){
if($rowArt14["album_nicename"]){
$outputdata.='
<url>
<loc>https://www.dogspot.in/photos/album/'.$rowArt14["album_nicename"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
//echo $rowArt14["album_id"].'<br/>';
$album_idall[$rowArt14["album_id"]]=$rowArt14["album_nicename"];
}}
}
//echo 'HI';
//print_r($album_idall);
foreach($album_idall as $album_id=>$album_nicename)
{
	if($album_id){
$selSql = "SELECT keywords FROM photos_image WHERE album_id = '$album_id'";  

$tagCloud = tagCloud($selSql);
$numTags = count($tagCloud); //echo"<b>$numTags </b>";
if($numTags){
$tagCloud=array_unique($tagCloud);
sort($tagCloud);
  foreach($tagCloud as $tc){ 
	$tc = trim($tc);
	$pos=stripos($tc, "ex-");
	if($pos === false) {
	$tc_nicename = createSlugNoLower($tc);
	$tc = makeTeaser($tc,50);
		if($album_nicename){
if($rowArt14["alb_display_on"] == 'event'){
			$outputdata.='
<url>
<loc>https://www.dogspot.in/photos/search/'.$tc_nicename.'/'.$album_nicename.'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
		}}}
  }
}
	}
}

//$result22 = mysql_query("SELECT image_nicename FROM  photos_image as pi,photos_album as ap WHERE ap.album_id=pi.album_id AND ap.publish_status='publish' AND ap.`album_link_name` = 'event'  ORDER BY ap.u_date DESC LIMIT 0,20000");
//while($rowArt22 = mysql_fetch_array($result22)){
//
//$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
//$outputdata.='
//<url>
//<loc>https://www.dogspot.in/photos/'.$rowArt22["image_nicename"].'/</loc>
//<lastmod>'.$e_date.'</lastmod>
//<changefreq>Monthly</changefreq>
//<priority>0.5</priority>
//</url>';
//}
//// Create Sitemap for the Posts--------------------------
//// Create Sitemap for the Posts--------------------------
//$result23 = mysql_query("SELECT image_nicename,pi.image FROM  photos_image as pi,photos_album as ap WHERE ap.album_id=pi.album_id AND publish_status='publish' AND `album_link_name` = 'event'  ORDER BY ap.u_date DESC LIMIT 20001,20000");
//while($rowArt23 = mysql_fetch_array($result23)){
//
//$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
//$outputdata.='
//<url>
//<loc>https://www.dogspot.in/photos/'.$rowArt23["image_nicename"].'/</loc>
//<lastmod>'.$e_date.'</lastmod>
//<changefreq>Monthly</changefreq>
//<priority>0.5</priority>
//<image:image>
//     <image:loc>https://www.dogspot.in/photos/images/'.$rowArt23["image"].'</image:loc>
//   </image:image>
//</url>';
//}
// Create Sitemap for the Posts--------------------------
$result45 = mysql_query("SELECT name,nice_name,item_id FROM shop_items WHERE item_display_status!='delete' AND type_id!='configurable' ORDER BY updated_at DESC");
while($rowArt45 = mysql_fetch_array($result45)){
	$sqitm=mysql_query("SELECT media_file, item_id FROM shop_item_media WHERE item_id = '".$rowArt45['item_id']."'");
$xmldate=date("Y-m-d H:m:s");
$xmldate=explode(' ',$xmldate);
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt45["nice_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Weekly</changefreq>
<priority>0.8</priority>';


while($getii=mysql_fetch_array($sqitm)){
	$sqloi=mysql_query("SELECT name FROM shop_items as a, shop_item_media as b WHERE a.item_id='".$getii['item_id']."' AND a.item_id=b.item_id");
	$nameb = preg_replace('/\&/', ' ', $sqloi['name']);
$outputdata.='<image:image>
     <image:loc>https://www.dogspot.in/shop/item-images/'.$getii["media_file"].'</image:loc>';
	 for($j=1; $onlinecheck1 = mysql_fetch_object($sqloi); $j++){
		 $page = $onlinecheck1->name;
		  if (!isset($used[$page]) || $used[$page] !== true) {
                $used[$page] = true;
				$page= preg_replace('/\&/', ' ', $page);
		$outputdata.='<image:title>'.$page.'</image:title>';
		  }}
  $outputdata.=' </image:image>';
}
$outputdata.='</url>';
}
//////////////////////////////////////Puppy Sale///////////////////////////////////////////////
/*$result34 = mysql_query("SELECT nice_name,city,breed_name FROM section_nice_name WHERE trend_id='3' ");
while($rowArt34 = mysql_fetch_array($result34)){
	$city=$rowArt34['city'];
	$breed_name=$rowArt34['breed_name'];
	$breed_idList[]='';
	//echo "SELECT puppi_id FROM puppies_available WHERE city_nicename='$city' AND breed_nicename='$breed_name' AND publish_status='publish'";
	$getbreed23=mysql_query("SELECT puppi_id FROM puppies_available WHERE city_nicename='$city' AND breed_nicename='$breed_name' AND publish_status='publish'");
  $num_rows=mysql_num_rows($getbreed23);
if($num_rows>0){
//	echo 'jyoti'.'<br>';
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt34["nice_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
$breed_idList[]=$rowArt34["breed_name"];
}
}
$listbreedArray=array_unique($breed_idList);
$outputdata.='
<url>
<loc>https://www.dogspot.in/puppies/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';

foreach($listbreedArray as $dogBreed){
if($dogBreed){
$outputdata.='
<url>
<loc>https://www.dogspot.in/puppies/breed/'.$dogBreed.'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
}
}
/////////////////////////////////////////////////////Puppies uRl//////////////
$result35 = mysql_query("SELECT nice_name,city FROM section_nice_name WHERE trend_id='2' ");
while($rowArt35 = mysql_fetch_array($result35)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z';
$city1=$rowArt35['city'];
	//$breed_name=$rowArt1['breed_name'];
	//echo "SELECT puppi_id FROM puppies_available WHERE city_nicename='$city' AND breed_nicename='$breed_name' AND publish_status='publish'";
	$getbreed2=mysql_query("SELECT puppi_id FROM puppies_available WHERE city_nicename='$city1' AND publish_status='publish'");
  $num_rows=mysql_num_rows($getbreed2);
if($num_rows>0){
 
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt35["nice_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
}
}
//////////////////////////////////////////Breed Type Food////////////////////////////////////////
		
$result36 = mysql_query("SELECT nice_name FROM section_nice_name WHERE trend_id='15' ");
while($rowArt36 = mysql_fetch_array($result36)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt36["nice_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>Monthly</changefreq>
<priority>0.5</priority>
</url>';
}*/

///////////////////////////////////////////////////////////////////
$breed_section_list='';
$result49 = mysql_query("SELECT nice_name,breed_name FROM section_nice_name WHERE result_count > 0");
while ($rowArt49 = mysql_fetch_array($result49)) {
	# code...
	$e_date=$xmldate[0].'T'.$xmldate[1].'Z';
	$outputdata.='
	<url>
	<loc>https://www.dogspot.in/'.$rowArt49["nice_name"].'/</loc>
	<lastmod>'.$e_date.'</lastmod>
	<changefreq>Monthly</changefreq>
	<priority>0.5</priority>
	</url>
	';
$breed_section_list[]=$rowArt49["breed_name"];
}
$listbreedArray=array_unique($breed_section_list);
foreach($listbreedArray as $breedpupp)
{
	if($breedpupp){
$outputdata.='
	<url>
	<loc>https://www.dogspot.in/puppies/breed/'.$breedpupp.'/</loc>
	<lastmod>'.$e_date.'</lastmod>
	<changefreq>Monthly</changefreq>
	<priority>0.5</priority>
	</url>
	';	
}
}
$outputdata.='
<url>
<loc>https://www.dogspot.in/puppies/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>daily</changefreq>
<priority>0.7</priority>
</url>';		

/////////////////////////////////////////////////////////
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-blog/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>daily</changefreq>
<priority>0.7</priority>
</url>';		

//mysql_select_db("test");		
$result = mysql_query("SELECT slug, name FROM wp_terms WHERE term_id<='7'");
while($rowArt = mysql_fetch_array($result)){
$e_date=$xmldate[0].'T'.$xmldate[1].'Z'; 
$outputdata.='
<url>
<loc>https://www.dogspot.in/dog-blog/'.$rowArt["slug"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>daily</changefreq>
<priority>0.7</priority>
</url>';
}
//Create Sitemap for the Posts--------------------------
//mysql_select_db("test");
$result1 = query_execute("SELECT wpp.ID , wpp.post_author , wpp.post_date , wpp.post_content , wpp.post_title , wpp.post_status , wpp.post_name ,wpp.views , wpp.domain_id , wpp.art_tag , wpp.title_tag , wptr.term_taxonomy_id , wtt.term_id FROM wp_posts as wpp , wp_term_relationships as wptr , wp_term_taxonomy as wtt WHERE wpp.post_status = 'publish' AND wpp.domain_id='1' AND wpp.post_type='post' AND wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wptr.object_id=wpp.ID AND wtt.term_id<='7' ORDER BY wpp.ID DESC");
while($rowArt1 = mysql_fetch_array($result1)){
$xmldate=date("Y-m-d H:m:s");
$xmldate=explode(' ',$xmldate);	
$e_date=$xmldate[0].'T'.$xmldate[1].'Z';
$outputdata.='
<url>
<loc>https://www.dogspot.in/'.$rowArt1["post_name"].'/</loc>
<lastmod>'.$e_date.'</lastmod>
<changefreq>weekly</changefreq>
<priority>0.6</priority>
</url>';
}
// Create Sitemap for the Posts--------------------------

/////////////////////////////////////////////Photo/////////////////////////////////////////////
$outputdata.='</urlset>';
$fp = fopen($baseURL."/sitemap.xml", 'w'); 
// save the contents of output buffer to the file
fwrite($fp, $outputdata);
fclose($fp);

?>