<?
require_once($DOCUMENT_ROOT.'/arrays.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');

function extract_emails_from($string){
  preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $string, $matches);
  return $matches[0];
}

function insertIMAPimage($To, $subject, $msgBody, $image){
//$albuser_imapmail=$To;
  $seletUname = mysql_query("SELECT userid, f_name FROM users WHERE imapmail = '$To'"); //fetch user details
	if(!$seletUname){   die(sql_error());	}
	
	$numUserR = mysql_num_rows($seletUname);
	if($numUserR > 0){
		$rowUser = mysql_fetch_array($seletUname);
		$album_user_id = $rowUser['userid'];
		$user_display_name = $rowUser['f_name'];
		$album_name = "mobile upload";
		$album_nicename = $album_user_id."_mobile_upload"; //for unique album name
		
		$chkAlb = mysql_query("SELECT * FROM users_meta WHERE userid = '$album_user_id'");
		if(!$chkAlb){	die(mysql_error());  }
		$numRow = mysql_num_rows($chkAlb);
		
		if($numRow == 0){
			$creatAlb = mysql_query ("INSERT INTO photos_album (userid, album_name, album_nicename, album_desc, orderseq, cdate, m_upload) 
									VALUES ('$album_user_id', '$album_name', '$album_nicename', '$album_desc', '$orderseq', NULL, '1')");
			$photo_album_id = mysql_insert_id();
			if(!$creatAlb){	die(mysql_error());   }
			updateUserNum("users", "num_album", $album_user_id); // Update num_album Count on users Table
			$userMeta = mysql_query("INSERT INTO users_meta (userid, meta_key, meta_value) 
									VALUES ('$album_user_id', 'email_photo_album_id', '$photo_album_id')");
			if(!$userMeta){	die(mysql_error());   }
		}else{
			$RowMeta = mysql_fetch_array($chkAlb);
    		$meta_key = $RowMeta["meta_key"];
    		$photo_album_id = $RowMeta["meta_value"];
		}
	
		// Insert image to album
		//define a maxim size for the uploaded images
		define ("MAX_SIZE","10000");
		// define the width and height for the thumbnail
		// note that theese dimmensions are considered the maximum dimmension and are not fixed, 
		// because we have to keep the image ratio intact or it will be deformed
		define ("WIDTH","160"); 
		define ("HEIGHT","120");
		
		define ("MWIDTH","360");
		define ("MHEIGHT","270");

		define ("LWIDTH","800");
		define ("LHEIGHT","600");
		
		// This variable is used as a flag. The value is initialized with 0 (meaning no error found) 
		//and it will be changed to 1 if an errro occures. If the error occures the file will not be uploaded.
		//$siteRoot = "D:/xampp/htdocs/dogspot";
		$siteRoot = "/home/dogspot/public_html";
		$imgFolder = "/photos";
		$errors=0;
		// checks if the form has been submitted
		/*
		insert the new photos(photos_image) in that album with title as subject body as 
		album_id, image, title="Subject', image_nicename create new nice name, image_desc='mail Body', cdate NULL, u_date=NULL.
		
		*/
		// Insert photo into photos_image table
		$userid = $album_user_id;
		$album_id = $photo_album_id;
		$title = $subject;
		$title = stripslashes($title);
		$image_desc = "$msgBody";
		$image_desc = stripslashes($image_desc);
		$image_desc = strip_tags($image_desc);  // For strip html entities..........
		
		// get the size of the image in bytes
		$temp_dir = $siteRoot."/backgroundjobs/temp_email_photo/";
		$temp_img_file = "$temp_dir$image";  //temp dir for download img
		echo"$temp_img_file";
		$size = getimagesize($temp_img_file);
		$sizekb = filesize($temp_img_file);
		
		//compare the size with the maxim size we defined and print error if bigger
		if($sizekb > MAX_SIZE*1024){
			echo'<h1>You have exceeded the size limit!</h1>';
			$errors = 1;
		}
		
		$extension = getExtension($image);
		$extension = strtolower($extension);
		
		//we will give an unique name
		$image_nicename = createSlug($title);
		$image_nicename = checkSlugAll('photos_image', 'image_nicename', $image_nicename);
		$p_img = $image_nicename.'.'.$extension; //new img name
		//the new name will be containing the full path where will be stored (images folder)
		$newname = "$siteRoot$imgFolder/images/".$p_img;
		$copied = copy($temp_img_file, $newname);
		//we verify if the image has been uploaded, and print error instead
		
		if(!$copied){
			echo '<h1>Copy Unsuccessfull!</h1>';
			$errors=1;
		}else{
			// the new thumbnail image will be placed in images/thumbs/ folder
			$thumb_name= $siteRoot.$imgFolder.'/images/thumb_'.$p_img;
			$large_name=$siteRoot.$imgFolder.'/images/m_'.$p_img;
			// call the function that will create the thumbnail. The function will get as parameters 
			//the image name, the thumbnail name and the width and height desired for the thumbnail
			$thumb=make_thumb($newname,$thumb_name,WIDTH,HEIGHT);
			$reSizeM=make_thumb($newname,$large_name,MWIDTH,MHEIGHT);
			$reSize=make_thumb($newname,$newname,LWIDTH,LHEIGHT);
		 
			$DelFilePath = $temp_img_file; 
			if(file_exists($DelFilePath)){  unlink ($DelFilePath);  } //delete download photos from test folder
		
			//If no errors registred, print the success message and show the thumbnail image created
			// If Photo Upload END............................
			if($p_img){
				$insertImg = mysql_query("INSERT INTO photos_image (userid, album_id, image, title, image_nicename, keywords, image_desc, orderseq, cdate, u_date, m_upload) VALUES ('$userid', '$photo_album_id', '$p_img', '$title', '$image_nicename', '$keywords', '$image_desc', '$orderseq', NULL, NULL, '1')");
				if(!$insertImg){   die(mysql_error());    }
				$NewImg_id = mysql_insert_id();
			
				updateFriendsFeed($userid, "photos", $NewImg_id, $image_nicename, $p_img);  // update friends feeds
				upNumCount("photos_album", "num_photos", "album_id", $meta_photo_album_id);  // update friends feeds
				upNumCount("users", "num_photo", "userid", $userid);  // update friends feeds

				// Publish of facebook
				require_once($siteRoot.'/facebookconnect/facebooksettings.php');
				require_once($siteRoot.'/facebookconnect/facebookfunctions.php');
				require_once($siteRoot.'/twitterconnect/twitterfunctions.php');
				
				$publink="https://www.dogspot.in/photos/$image_nicename/";
				$pubtypmsg="added a new photo via Mobile on DogSpot.in&picture=https://www.dogspot.in/photos/images/$p_img";
				checkfacebooklink($userid, $publink, $pubtypmsg, "f");
				// Publish of facebook END

				//publish on twitter...
				$msg = "added a new photo via Mobile on DogSpot.in";				
				publishontwitter($userid, $msg, $publink, "t");
				//publish on twitter End...

			}//End if p_img
		}//End if copied
	}else{
		echo"---Invalid User---";
	}
}

function get_mime_type(&$structure) {
   $primary_mime_type = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");
   if($structure->subtype) {
   	return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;
   }
   	return "TEXT/PLAIN";
}

function get_part($stream, $msg_number, $mime_type, $structure = false, $part_number = false) {
   
   	if(!$structure) {
   		$structure = @imap_fetchstructure($stream, $msg_number);
   	}
   	if($structure) {
   		if($mime_type == get_mime_type($structure)) {
   			if(!$part_number) {
   				$part_number = 1;
   			}
   			$text = imap_fetchbody($stream, $msg_number, $part_number);
   			if($structure->encoding == 3) {
   				return imap_base64($text);
   			} else if($structure->encoding == 4) {
   				return imap_qprint($text);
   			} else {
   			return $text;
   		}
   	}
   
		if($structure->type == 1) /* multipart */ {
   		while(list($index, $sub_structure) = each($structure->parts)) {
   			if($part_number) {
   				$prefix = $part_number . '.';
   			}
   			$data = get_part($stream, $msg_number, $mime_type, $sub_structure,$prefix .    ($index + 1));
   			if($data) {
   				return $data;
   			}
   		} // END OF WHILE
   		} // END OF MULTIPART
   	} // END OF STRUTURE
   	return false;
} // END OF FUNCTION

$hostname = '{67.210.114.31:143/notls}INBOX';
$username = 'mobile@m.dogspot.in';
$password = 'mobile123';

/* try to connect */
$imap = imap_open($hostname,$username,$password) or die('Cannot connect to Mail Server: ' . imap_last_error());
$nrmsgs = imap_num_msg($imap);
echo"Total Emails: $nrmsgs<br>";
for($j = $nrmsgs; $j > 0; $j--){
	$structure = imap_fetchstructure($imap, $j);
	echo"Mail : $j <br>";
	$attach=0;
   	foreach($structure->parts as $part){
		
		if($part->subtype == "HTML")
		continue;

		if($part->dparameters[0]->value){
			// Print Header
			$overview = imap_fetch_overview($imap,$j,0);
			$Subject=$overview[0]->subject;
			//$Subject=preg_replace("/[^a-zA-Z0-9 ]/", "", $Subject);
			echo "Subject: $Subject<br>";
			echo 'From: '.$overview[0]->from.'<br>';
			$To=$overview[0]->to;
			$Toemails = extract_emails_from($To);
			$To=$Toemails[0];
			echo "To: $To<br>";
			echo 'Date on: '.$overview[0]->date.'<Br>';
			
			$seletUname = mysql_query("SELECT userid, f_name FROM users WHERE imapmail = '$To'"); //fetch user details
			if(!$seletUname){
			   die(sql_error());
			}
			$numUserR = mysql_num_rows($seletUname);
			if($numUserR > 0){
			
				// GET TEXT BODY
				$dataTxt = get_part($imap, $j, "TEXT/PLAIN");
				// GET HTML BODY
				$dataHtml = get_part($imap, $j, "TEXT/HTML");
				if ($dataHtml != "") {
					$msgBody = $dataHtml;
					$mailformat = "html";
				} else {
					$msgBody = ereg_replace("\n","<br>",$dataTxt);
					$mailformat = "text";
				}
				$msgBody = trim($msgBody);
				echo "Messagebody: $msgBody";
				// Print Header END
				
				//for decode header
				$filename = $part->dparameters[0]->value;
				$elements = imap_mime_header_decode($filename);
				for ($i=0; $i<count($elements); $i++){
					echo "Charset: {$elements[$i]->charset} <br>";
					//echo "Text: {$elements[$i]->text} <br>";
					$strFileName="{$elements[$i]->text}";
					//echo"File Name: $strFileName";
					$strFileType = strrev(substr(strrev($strFileName),0,4));
					$strFileType = strtolower($strFileType);
					if(($strFileType == ".jpg") || ($strFileType == ".jpeg") || ($strFileType == ".gif")){
						echo"<br>File Type: $strFileType";
						$fileLength=strlen($strFileName);
						$strFileName=substr($strFileName,0,$fileLength-4);
						$nsub=$strFileName;
						$strFileName = goodFileName($strFileName);
						echo"<br>New File name: $strFileName";
						
						if($Subject == ''){ $Subject = trim($nsub); }
						
						$fileContent = imap_fetchbody($imap, $j, $attach+2);
						$fileContent = imap_base64($fileContent);
						$temp_dir = $DOCUMENT_ROOT."/backgroundjobs/temp_email_photo/";
						$temp_img_dir = "$temp_dir$strFileName$strFileType";
						$image="$strFileName$strFileType";
						echo"<br>$temp_img_dir : $attach";
						$temp_img_dir = fopen($temp_img_dir, 'w') or die("can't open file");
						fwrite($temp_img_dir, $fileContent);
						fclose($temp_img_dir);
						$attach=$attach+1;
						insertIMAPimage($To, $Subject, $msgBody, $image);
					}
				}
			}else{
				echo"Invalid Email";
			}
		}
		echo"<hr>";
   }
	imap_delete($imap, $j);
	imap_expunge($imap);
  // @imap_mail_move($imap, $j, "INBOX.uploaded");
}
imap_close($imap);
?>