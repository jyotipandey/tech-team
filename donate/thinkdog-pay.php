<?php
// Basic Setting ---------------------------------------------------------------------------------------------
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-ArrayShopAttributeValue.php');
if($submit){
	echo "$owner_name";
	}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Checkout | DogSpot Shop</title>
<meta name="keywords" content="Billing / Shipping Information" />
<meta name="description" content="Billing / Shipping Information" />
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-css.php'); ?>
<?php require_once($DOCUMENT_ROOT.'/new/common/shop-new-js.php'); ?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->


<meta http-equiv="X-UA-Compatible" content="IE=100" >
<script>
$(function() {    // Makes sure the code contained doesn't run until
                  //     all the DOM elements have loaded

    $('#payment').change(function(){
        $('#showfield').toggle();
        //$('#' + $(this).val()).show();
    });

});
</script>

</head>
<body>
<div align="center">
										<table border="0" width="100%" cellpadding="2">
											<form method="POST" action="" name="reg" id="reg">
											<tr>
												<td valign="top">
												<fieldset style="padding: 2; font-family:Arial; font-size:12pt; color:#B01317; font-weight:bold">
												<legend><b>
												<font face="Arial">
												Registration Details</font></b></legend>
												<table border="0" width="100%" cellpadding="2">
													<tr>
														<td width="152">
												<font face="Arial" size="2">
												<font color="#333333">Owner 
												Name</font> <font color="#B01317">*</font></font></td>
														<td width="10" align="center">
														:</td>
														<td><font face="Arial" size="2">
												<input type="text" name="owner_name" size="40" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="owner_name"></font></td>
													</tr>
													<tr>
														<td width="152">
												<font face="Arial" size="2">
												<font color="#333333">Dog's Name 
												</font> <font color="#B01317">*</font></font></td>
														<td width="10" align="center">
														:</td>
														<td><font face="Arial" size="2">
												<input type="text" name="dog_name" size="40" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="dog_name"></font></td>
													</tr>
													<tr>
														<td width="152">
												<font face="Arial" size="2">
												<font color="#333333">Email</font> <font color="#B01317">*</font></font></td>
														<td width="10" align="center">
														:</td>
														<td>
												<font face="Arial" size="2">
												<input type="text" name="email" size="40" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="email"></font></td>
													</tr>
													<tr>
														<td width="152">
												<font face="Arial" size="2" color="#333333">
												Contact</font></td>
														<td width="10" align="center">
														:</td>
														<td>
												<font face="Arial" size="2">
												<input type="text" name="contact" size="40" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="contact"></font></td>
													</tr>
													<tr>
													  <td><font face="Arial" size="2" color="#333333">
												Payment</font></td>
													  <td align="center">:</td>
													  <td><font face="Arial" size="2">
                                                       <select name="payment" id="payment">
                                                  <option id="payment" value="" >select</option>     
                                                <option id="payment" value="3500" >per person</option>
                                                <option id="payment" value="6000">for a couple</option>
                                                </select></font></td>
												  </tr>
													</table>
												</fieldset></td>
											</tr>
											<tr>
												<td valign="top">&nbsp;
												</td>
											</tr>
											<tr>
												<td valign="top">
												<fieldset style="padding: 2">
												<legend><b>
												<font face="Arial" size="3" color="#B01317">
												Participants Details</font></b></legend>
												<table border="0" width="100%" cellpadding="2">
													<tr>
														<td width="40"><b>
														<font face="Arial" size="2" color="#333333">
														Sr.no.</font></b></td>
														<td align="left" width="187">														<b><font face="Arial" size="2" color="#333333">Name</font></b></td>
														<td align="left">
														<p >
														<b>
														<font face="Arial" size="2" color="#333333">
														Age</font></b></td>
														<td rowspan="11" width="250">
														  <p align="center"></td>
													</tr>
													<tr>
														<td align="center" height="39" width="40">
														<font face="Arial" size="2" color="#333333">
														1.</font></td>
														<td height="39" width="187">
														<font face="Arial">
														<input type="text" name="name1" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td height="39">
														<font face="Arial" size="1">
														<input type="text" name="age1" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age1"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														2.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name2" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age2" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age2"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
                                                    <div id="showfield">
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														3.</font></td>
                                                        
														<td width="187">
														<font face="Arial">
														<input type="text" name="name3" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age3" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age3"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														4.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name4" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age4" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age4"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														5.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name5" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age5" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age5"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														6.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name6" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age6" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age6"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														7.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name7" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age7" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age7"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														8.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name8" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age8" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age8"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														9.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name9" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age9" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age9"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
													<tr>
														<td align="center" width="40">
														<font face="Arial" size="2" color="#333333">
														10.</font></td>
														<td width="187">
														<font face="Arial">
														<input type="text" name="name10" size="30" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;"></font></td>
														<td>
														<font face="Arial" size="1">
														<input type="text" name="age10" size="5" style="border: 1px solid #e1e1e1; padding: 5px; font-family:Arial; font-size:10pt; color:#333333; background-color:#f8f8f8;" id="age10"><font color="#333333">
														<span style="font-size: 8pt">
														years</span></font></font></td>
													</tr>
                                                    </div>
												</table>
												</fieldset></td>
											</tr>
											
											
											
											<tr>
												<td>&nbsp;<input type="submit" name="submit" id="submit" value="Register" />
												</td>
											</tr>
											<tr>
												<td>
												</td>
											</tr>
											</form>
										</table>
									</div>   
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>