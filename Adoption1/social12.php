<?php
require("shareCount.php");
$obj=new shareCount($surl); ?>  
    <ul class="ds_social_buttons">
                    <li class="ds_social_list ds_facebook_share">
                <a style="text-decoration:none;" href="https://www.facebook.com/share.php?u=<?= $surl ?>" onClick="return fbs_click()" target="_blank" title="Share This on Facebook">
                    <span class="ds_social_icons"></span>
                    <span>Share</span>
                   
                </a>
                <span class="ds_social_count"><?= $obj->get_fb() ?></span>
            </li>
        <li class="ds_social_list ds_twitter_share">
                <a style="text-decoration:none;" href="https://twitter.com/intent/tweet?url=https://www.dogspot.in/<?= $page_nice ?>/&original_referer=https://www.dogspot.in/<?= $page_nice ?>/&source=dogspot&text=<?= $pageDesc ?>" target="_blank" onclick="return windowpop(this.href, 545, 433)">
                    <span class="ds_social_icons"></span>
                   <span> Tweet</span>                                                    
                </a>
                <span class="ds_social_count"><?= $obj->get_tweets() ?></span>
            </li>
        <li class="ds_social_list ds_linkedin_share">
                <a style="text-decoration:none;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $surl ?>&title=<?= $post_title ?>&summary=<?= trim($pageDesc) ?>&source=dogspot.in" onclick="return windowpop(this.href, 545, 433)">
                    <span class="ds_social_icons"></span>
                    <span>Linkedin</span>
                </a>
                <span class="ds_social_count"><?= $obj->get_linkedin() ?></span>
            </li>
         
            <li class="ds_social_list ds_google_share">
                <a style="text-decoration:none;"href="https://plus.google.com/share?url=<?= $surl ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                    <span class="ds_social_icons"></span>
                    <span>Google+</span>
                </a>
                <span class="mailCounter ds_social_count"><?= $obj->get_plusones() ?></span>
            </li>
            <li class="ds_social_list ds_pinterest_share">
                <a style="text-decoration:none;" href="https://pinterest.com/pin/create/button/?url=<?= $surl ?>&media='.$surl.'&description="  target="_blank" onclick="return windowpop(this.href, 545, 433)">
                    <span class="ds_social_icons"></span>
                    <span>Pinterest</span>
                </a>
                <span class="mailCounter ds_social_count"><?= $obj->get_pinterest() ?></span>
            </li>
            <?     
  unset($post_title);
  unset($pageDesc);
  unset($page_nice);  
?>
          </ul>