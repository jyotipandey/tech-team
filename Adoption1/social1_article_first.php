<?php

require("shareCount.php");
$obj=new shareCount($surl); ?>
<section class="sharebtn_container" style="float: left;">
     <div class="share-btn" ><a style="text-decoration:none;" href="https://twitter.com/intent/tweet?url=https://www.dogspot.in/<?= $art_name ?>/&original_referer=https://www.dogspot.in/<?= $art_name ?>/&source=dogspot&text=<?= $post_title ?>" target="_blank" onclick="return windowpop(this.href, 545, 433)">
      <span class="share-btn-action share-btn-tweet"><i class="fa fa-twitter"></i>Tweet</span></a>
      <span class="share-btn-count center s_count"><?= $obj->get_tweets() ?></span>
    </div>
     <div class="share-btn">  <a style="text-decoration:none;" href="https://www.facebook.com/share.php?u=<?= $surl ?>" onClick="return fbs_click()" target="_blank" title="Share This on Facebook">
      <span class="share-btn-action  share-btn-share"><i class="fa fa-facebook"></i>Share</span></a>
      <span class="share-btn-count center s_count"><?= $obj->get_fb() ?></span>
    </div>
   <div class="share-btn">  <a style="text-decoration:none;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $surl ?>&title=<?= $post_title ?>&summary=<?= trim($pageDesc) ?>&source="dogspot.in"  onclick="return windowpop(this.href, 545, 433)">
      <span class="share-btn-action share-btn-linkedin"><i class="fa fa-linkedin"></i>Linkedin</span></a>
      <span class="share-btn-count center s_count"><?= $obj->get_linkedin() ?></span>
    </div> 
    <div class="share-btn">  <a style="text-decoration:none;" href="https://plus.google.com/share?url=<?= $surl ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
      <span class="share-btn-action share-btn-plus"><i class="fa fa-google-plus"></i>Google+</span></a>
      <span class="share-btn-count center s_count"><?= $obj->get_plusones() ?></span>
    </div>
     
  </section>

