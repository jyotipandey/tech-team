<?php
require_once('constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
$sitesection = "shop";
$session_id = session_id();
 if($userid=='jyoti05')
  {
  require_once($DOCUMENT_ROOT . '/contactus-bootstrap.php');
  exit;
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="canonical" href="https://www.dogspot.in/contactus.php" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.dogspot.in/contactus.php" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact DogSpot - Online Pet Supply Store</title>
<meta name="keywords" content="Contact Dogspot, Contact Pet supplies store, get in touch with Pet Shop, Dog store." />
<meta name="description" content="Contact DogSpot for any questions regarding pets supplies, accessories & online products purchase. You can get in touch via phone calls or by filling inquiry form." />

<div itemscope itemtype="http://schema.org/Organization">
<a itemprop="url" href="https://www.dogspot.in/"><meta itemprop="name" content="DogSpot.in">
</a>
<meta itemprop="description" content="DogSpot.in offers pet products including dogs, cats and small pets online.">
<meta itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<?php /*?><meta itemprop="streetAddress" content="Plot no - 545, S.Lal Tower Sector - 20, Dundahera">
<meta itemprop="addressLocality" content="Gurgaon">
<meta itemprop="addressRegion" content="Haryana">
<meta itemprop="postalCode" content="122016">
<meta itemprop="addressCountry" content="India"><?php */?>
</div>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<!--[if lt IE 7]>
<script type="text/javascript" src="/new/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/new/js/jquery/jquery.dropdown.js"></script>

<![endif]-->


<script type="text/javascript">
 jQuery(document).ready(function() {
	  jQuery("#formC").validate({
		  
	});
});
</script>



<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>

<div class="cont980">
<div class="vs10"></div>
<h1>Send us an Email</h1>
<div class="vs10"></div>
  <div style="float:left; width:470px; ">
  
 <script type="text/javascript" src="https://assets.freshdesk.com/widget/freshwidget.js"></script>
 <link rel="stylesheet" type="text/css" href="https://assets.freshdesk.com/widget/freshwidget.css">
<?php /*?><style type="text/css" media="screen, projection">
	@import url(http://assets.freshdesk.com/widget/freshwidget.css); 
</style> <?php */?>
<script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
<style type="text/css" media="screen, projection">
	@import url(https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.css); 
</style> 
<iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://dogspotsupport.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&screenshot=No&attachFile=no&captcha=yes" scrolling="no" height="500px" width="100%" frameborder="0" >
</iframe>
  </div>
  <div style="float:right; width:495px;" class="contactDetails_wrapper">
 <h3>Call Us:</h3> 
  <p>Customer Care +91- 9818011567<br /></p>
  <p> Timing: 9 AM to 6 PM - Mon to Sat <br />
  <span>(Standard Calling Charges Apply)</span>
  </p> 
   <br />   <p> Marketing Alliances - marketing@dogspot.in </p>
  <br />
  <h3>Mail Us:</h3>
  <table><tr>
  <td style="padding-left:90px;">
  Om Sai Complex, First Floor,<br /> 
Plot No. 37/19/22, <br />
Behind IGL CNG Petrol Pump,<br /> 
Kapashera, New Delhi-110037
</td></tr></table>
  <br /><h3>Please Note:  </h3>
  <p>DogSpot.in is an open platform for information sharing and is not involved in
the sale or purchase of puppies and dogs. The users can interact with each other
directly for adoption, sale or purchase of Puppies and Dogs. DogSpot.in does not
undertake any responsibility for Transactions made based on information posted
on the website.</p>
  </div>
</div>
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?>
