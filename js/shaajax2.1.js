// JavaScript Document
<!--Ajax With jquery-->
/*
pageUrl
divToload
divNewid
formName
formMethod "POST/GET"
divLoading
divLoadingText
divAction APE/REP
*/
function ShaForm(FormName){
	comp = "document." + FormName;
	var frm = eval(comp);
	Cps = "";
	for (i=0; i<frm.length; i++){
		Cps = Cps + frm.elements[i].name + "=" + encodeURIComponent(frm.elements[i].value) + "&";
	}
	Cps = Cps.substring(0,Cps.length -1);
	return Cps;
}

function ShaAjaxJquary(pageUrl, divToload, divNewid, formName, formMethod, divLoading, divLoadingText, divAction){
	if(divLoading){
		jQuery(divLoading).fadeIn(400).html(divLoadingText);
	}
	if(formName){
		var dataString = ShaForm(formName);
	}
     jQuery.ajax({
        type: formMethod,
        url: pageUrl,
		cache: false,
		data: dataString,
		dataType: "html",
		context: document.body,
        success: function(data) {
			if(divAction == "REP"){
				jQuery(divToload).hide();
				jQuery(divToload).html(data);
				jQuery(divToload).fadeIn("slow");
			}else{
				jQuery(divToload).append(data);
				jQuery(divNewid).hide();
				jQuery(divNewid).fadeIn("slow");
			}
			if(divLoading){
				if(divLoading!=divToload){
					jQuery(divLoading).fadeOut("slow");
				}
			}
		}
     });
}
<!--Ajax With jquery END-->