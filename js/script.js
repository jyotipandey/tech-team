// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#search-query-new').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: '/new/shop/getall.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#slidingDiv').show();
				$('#slidingDiv').html(data);
			}
		});
	} else {
		$('#slidingDiv').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#search-query-new').val(item);
	// hide proposition list
	$('#slidingDiv').hide();
}