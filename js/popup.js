// JavaScript Document
jQuery(document).ready(function(){
	jQuery.fn.closeBox = function(){
		jQuery('#fade , .popup_block').fadeOut(); //fade them both out
		jQuery('#fade').remove();
	}
//Close Popups and Fade Layer
	jQuery('a.close, #fade').live('click', function() { //When clicking on the close or fade layer...
	  	jQuery('#fade , .popup_block').fadeOut(); //fade them both out
		jQuery('#fade').remove();
		//return false;
	});
 
jQuery.fn.popOpen = function(){
     	var popID = 'poplight'//$(this).attr('rel'); //Get Popup Name
		//var popURL = $(this).attr('href'); //Get Popup href to define size
				
		//Pull Query & Variables from href URL
		//var query= popURL.split('?');
		//var dim= query[1].split('&');
		var popWidth = '500';//dim[0].split('=')[1]; //Gets the first query string value
 
		//Fade in the Popup and add close button
		jQuery('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="/images/close_pop.png" class="btn_close" title="Close Window" alt="Close" border="0" /></a>');
		
		//Define margin for center alignment (vertical + horizontal) - we add 80 to the height/width to accomodate for the padding + border width defined in the css
		var popMargTop = ($('#' + popID).height() + 80) / 2;
		var popMargLeft = ($('#' + popID).width() + 80) / 2;
		
		//Apply Margin to Popup
		jQuery('#' + popID).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		//Fade in Background
		jQuery('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
		jQuery('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer 
};

});