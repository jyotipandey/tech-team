<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="https://d3srewv59jy33q.cloudfront.net/new/css/recommend.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
function callmainpage_pl(){
	var dog_breed1=$("#dogbreed1").val();
	var dog_name1=$("#dogname1").val();
	var dog_age1=$("#dogage1").val();

if(dog_breed1=='0' || dog_age1=='0' || dog_name1==''){
document.getElementById("errormsg").style.display="inline";
}else{
	if(dog_breed1=='11'){
		breedtype='11-3';
	}else if(dog_breed1=='636'){
		breedtype='636-3';
	}else if(dog_breed1=='37'){
		breedtype='37-3';
	}else if(dog_breed1=='60'){
		breedtype='60-5';
	}else if(dog_breed1=='65'){
		breedtype='65-5';
	}else if(dog_breed1=='67'){
		breedtype='67-7';
	}else if(dog_breed1=='86'){
		breedtype='86-5';
	}else if(dog_breed1=='108'){
		breedtype='108-2';
	}else if(dog_breed1=='106'){
		breedtype='106-2';
	}else{
		breedtype='112-5';
	}
	
window.location="/recommend/?item_breed="+dog_breed1+"&life_stage="+dog_age1+"&dog_name="+dog_name1;}
}

function callmainpage_pl1(){
	var dog_breed2=$("#dogbreed1").val();
	var dog_name1=$("#dogname1").val();
	var dog_age1=$("#dogage1").val();

if(dog_breed2=='0' || dog_age1=='0' || dog_name1==''){
document.getElementById("errormsg").style.display="inline";
}else{
	if(dog_breed2=='2'){
		var btype="Small-Breed";
	}else if(dog_breed2=='3'){
		var btype="Medium-Breed";
	}else if(dog_breed2=='5'){
		var btype="Large-Breed";
	}else{
		var btype="Giant-Breed";
	}
var res = dog_breed2.split("@@");
var dog_breed1=res[0];
var brd_type=res[1];

/*	if(dog_breed1=='11'){
		breedtype='11-3';
	}else if(dog_breed1=='636'){
		breedtype='636-3';
	}else if(dog_breed1=='37'){
		breedtype='37-3';
	}else if(dog_breed1=='60'){
		breedtype='60-5';
	}else if(dog_breed1=='65'){
		breedtype='65-5';
	}else if(dog_breed1=='67'){
		breedtype='67-7';
	}else if(dog_breed1=='86'){
		breedtype='86-5';
	}else if(dog_breed1=='108'){
		breedtype='108-2';
	}else if(dog_breed1=='106'){
		breedtype='106-2';
	}else{
		breedtype='112-5';
	} */
	if(dog_breed1=='11' || dog_breed1=='636' || dog_breed1=='37' || dog_breed1=='60' || dog_breed1=='65' || dog_breed1=='67' || dog_breed1=='86' || dog_breed1=='108' || dog_breed1=='106' || dog_breed1=='112'){
window.location="/recommend/?item_breed="+dog_breed1+"&life_stage="+dog_age1+"&dog_name="+dog_name1;

	}else{
window.location="/recommend/?item_breeds=g&breed_type="+brd_type+"&life_stage="+dog_age1+"&dog_name="+dog_name1;
	}
}
}
</script>
</head>

<body>
<div class="pop_pr_box">
<div class="hlp_box1 top_mar_pr">
<div class="hlp_head">
<h2>Help us know your pet better</h2>
<span class="cls_hlp">
<a href="" class="close"  >
<img src="https://d3srewv59jy33q.cloudfront.net/Recommend-Product/images/btn5.png" width="18" height="18" alt="" /></a>
</span>
</div>
<div class="hlp_detail">
<ul>
<li class="hlp_li">
<p>Dog Name</p>
<p><input name="" type="text" id="dogname1" /></p>
</li>
<li class="hlp_li">
<p>Breed </p>
<p>
      <select name="select" id="dogbreed1" >
      <option value="0">Select</option>
      <? $qrevRowItem1=query_execute("SELECT * FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC");
		while($rowMyord2 = mysql_fetch_array($qrevRowItem1)){ 
		$id=$rowMyord2['breed_id']; 
		$name=$rowMyord2['be_name']; 
$selsize2       = query_execute_row("SELECT value FROM breed_engine_values WHERE breed_id='$id' AND att_id='41'");
$selsize    	 = $selsize2['value'];
if($selsize=='193'){
	$selsize1='2';
}else if($selsize=='194'){
	$selsize1='3';
}else if($selsize=='195'){
	$selsize1='5';
}else{
	$selsize1='6';
}
		 ?>
    <?php /*?><option value="<?=$id?>@@<?=$selsize1?>"> <?=$name?> <? } ?> </option> <?php */?> 
    <option value="<?=$id?>@@<?=$selsize1?>"> <?=$name?> </option><? } ?>
      </select> 
<?php /*?>      <? }else{ ?>
<select name="" id="dogbreed1">
<option value="0">Select</option>
<option value="2">Small Breed <span>(Upto 10 Kg)</span></option>
<option value="3">Medium Breed <span>(10-25 Kg)</span></option>
<option value="5">Large Breed <span>(25-45 Kg)</span></option>
<option value="6">Giant Breed <span>(Above 45 Kg)</span></option>
</select>
<? } ?><?php */?>
<?php /*?>      <? }else{ ?>
<select name="" id="dogbreed1">
<option value="0">Select</option>
<option value="11">Beagle</option>
<option value="636">Dachshund</option>
<option value="37">English Cocker Spaniel</option>
<option value="60">German Shepherd</option>
<option value="65">Golden Retriever</option>
<option value="67">Great Dane</option>
<option value="86">Labrador Retriever</option>
<option value="108">Pug</option>
<option value="106">Pomeranian</option>
<option value="112">Rottweiler</option>
</select<?php */?>
<? //} ?>
</p>
</li>
<li class="hlp_li">
<p>Age</p>
<p><select name="" id="dogage1">
<option value="0">Select</option>
<option value="1">Upto 2 months</option>
<option value="2">2 to 24 months</option>
<option value="5">2 to 7 years</option>
<option value="6">More than 7 years</option>
</select>
</p>
</li>
</ul>
<div class="btn_box">
<a><label id="errormsg" style="display:none; color:#F00; font-weight:200;text-decoration: none;float: left;margin-bottom: 5px;width: 100%;">All details are mandatory</label><br />
<?php /*?><? if($userid=='abhi2290'){ ?>
<img src="/Recommend-Product/images/btn3.png" width="192" height="37" alt="" onclick="callmainpage_pl()" style="cursor:pointer" />
<? }else{ ?><?php */?>
<img src="https://d3srewv59jy33q.cloudfront.net/Recommend-Product/images/btn3.png" width="192" height="37" alt="" onclick="callmainpage_pl1()" style="cursor:pointer" />
<?php /*?><? } ?><?php */?>
</a>
</div>
 </div>
 </div>
 </div>
</body>
</html>
