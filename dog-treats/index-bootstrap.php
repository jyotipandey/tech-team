<?php 
include($DOCUMENT_ROOT."/session-no.php"); 
include($DOCUMENT_ROOT."/functions.php");
include($DOCUMENT_ROOT."/database.php");
include($DOCUMENT_ROOT."/arrays.php");
$mobile_browser=is_mobile2();
	 if ($mobile_browser > 0) {
		    $request=str_replace("/","",$_SERVER['REQUEST_URI']);
			//echo "SELECT id FROM mobile_section WHERE section_name='$request' AND status='active'";		
			$rowPagesec=query_execute_row("SELECT id FROM mobile_section WHERE section_name='$request' AND status='active'");
			if($rowPagesec['id'])
			{
			header('Location: https://m.dogspot.in/'.$request.'/');
			exit();	
			}
			
		  }

$title="Feed Your Dog The Real Love";
$keyword="Dog Chicken and Duck Treats, Dog Chicken and Duck Treats Online";
$desc="Buy Dog Chicken and Duck Treats online in India, Dog Chicken and Duck Treats, Dog Treats at DogSpot.in. Cash on Delivery";
 
    $alternate="https://m.dogspot.in/dog-treats/";
	$canonical="https://www.dogspot.in/dog-treats/";
	$og_url=$canonical;
	$imgURLAbs="";
	
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');
?> 
<link type="text/css" rel="stylesheet" href="/bootstrap/css/dog-treats.css?v=2" />
 
 


<!-- treats header start-->
<section class="dog-treats-header">
<div class="container">

<div class="">
<h1>Feed Your Dog the real Love</h1>
<div class="treats-slider-sec" style="    padding-top: 6%;
">

 
<div id="owl-product-slider" class="owl-carousel" style="max-width:900px;margin-left: 3.1%">
         
        


<div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-chicken-sandwich-100-gm/"> <img src="https://ik.imagekit.io/2345/imgthumb/300x280-159820535914762578191.jpg">
        <p>Mini Chicken Slices</p>
 </a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
     
      
      
      
<div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-chicken-sandwich-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-72210916214762579880.JPG">
  <p>Chicken Sandwich</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
    
    <div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-chickencakes-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-201302261714762580741.jpg">
  <p>Chicken Cakes</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
    
    <div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-duck-slices-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-83089324314762571221.jpg">
  <p> Duck Slices</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
    
      <div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-softduck-jerky-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-127499705114762572721.jpg">
  <p> Soft Duck Jerky</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>


<div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-soft-chicken-slices-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-146534228614762584742.jpg">
  <p>Soft Chicken Slices</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
 
 <div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-chickenjerky-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-61915255714762582161.jpg">
  <p>Chicken Jerky</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
 <div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-mini-duck-slices-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-204164508614762567681.jpg">
  <p>Duck Sandwich</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
 
 <div class="item">
<div class="productinfo text-center p-slide-img"> 
<a href="https://www.dogspot.in/petspot-duck-sandwich-100-gm/">
  <img src="https://ik.imagekit.io/2345/imgthumb/300x280-118079571014762576501.jpg">
  <p>Mini Duck Slices</p></a>
 	<div class="product-price-s text-center">
         <span class="p-ds-off">Rs. 325</span>
                        </div>
                        </div>
</div>
 </div>
</div>
</div>
<img src="https://ik.imagekit.io/2345/dog-treats/images/dog-image.png" class="dog-image visible-lg visible-md" />
</div>

<!-- treats header end-->
<!--eat back gurrenty section-->
</section>
<section class="eat-back">
<div class="container">

"If your dog will not eat it. Send us back we will eat it." 
<img src="https://ik.imagekit.io/2345/images/eat-back.jpg" style="    vertical-align: middle;" />
</div>
</section>
<!-- about the products-->
<section class="about-dog-treats">
<div class="container">



<h3>About the products</h3> 
<p class="about-dog-treats-p-b">
Since dogs are our extended family we understand the love and care that they need.
 brings 100%; REAL CHICKEN and DUCK Treats Which are highly palatable for your dogs to go bonkers. We offer the largest range of REAL Meats Treats.</p>
<p >These are easy to chew and suitable for dogs of all breed and age. The treats are completely natural, free of sugar, artificial coloring and are rich in protein to keep your dog healthy and nutritious.</p>
</div>

</section>
<!-- about the products-->
<section class="benifits-sectiion">
<div class="container">

<div class=""><div class="benifits-sectiion-bg">
<h3>BENEFITS </h3>
<div class="benifits-sectiion-l">
<img src="https://ik.imagekit.io/2345/dog-treats/images/treats-new.png" class="visible-lg visible-md" />
</div>
<div class="benifits-sectiion-l">
<ul>
<li>
<strong>Rich in Nutrients</strong>- Good for overall health 

</li>
<li><strong>High Palatable & Easy to Chew</strong>- Good for Teeth</li>
<li><strong>Free of Sugar</strong>-  Good for Coat & Hair</li>
<li>
<strong>Rich in Protein</strong>- Good for Muscles</li>
</ul>
</div>
</div>
</div>

</div>
</section>
<section class="comparision-section">
<div class="container">


<h3 style="text-align:center;    margin: 0px;
    margin-bottom: 30px; ">EXPLORE & BUY NOW</h3>
<div class="treats-sec" >
<div class="row">
   <div class="col-xs-12 col-sm-6 col-md-6">  <a href="https://www.dogspot.in/duck-treats/"> 
     <img src="https://ik.imagekit.io/2345/dog-treats/images/duck-treats.jpg" width="" height="" class="img-responsive"/></a>
      
      </div>
    <div class="col-xs-12 col-sm-6 col-md-6">   <a href="https://www.dogspot.in/chicken-treats/">
      <img src="https://ik.imagekit.io/2345/dog-treats/images/chicken-treats.jpg" width="" height="" class="img-responsive" />
      </a>
     
        
     </div>   
	</div>
 
	






</div>
</div>
</section>


<script type="text/javascript" src="/bootstrap/js/owl.carousel.min.js"></script> 
<script type="text/javascript">
$(document).ready(function() {
 $('#owl-product-slider').owlCarousel({
    items:3,
    lazyLoad:true,
    loop:false,
	slideSpeed : 400,
	autoPlay: false,
	paginationSpeed : 400,
	navigation:true,
    margin:10,
	rewindNav:false,
});
});
</script>



<?php require_once($DOCUMENT_ROOT.'/new/common/bottom-bootstrap.php'); ?>
