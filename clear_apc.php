<?php
if (in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', '172.16.213.86', '125.62.123.199')))
{
  apc_clear_cache();
  apc_clear_cache('user');
  apc_clear_cache('opcode');
  echo json_encode(array('success' => true));
}
else
{
  die('No valid IP');
}
?>
