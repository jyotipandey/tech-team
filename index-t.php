<?php 
require_once('session-no.php');

if ($logoff == "logoff"){
session_destroy();
// refresh the page
header ("Location: index-t.php"); 
ob_end_flush();
exit(); 
}
$sitesection = "HOME";
$ajaxbody = "HOME";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Dog lovers Community Health Information</title>
<meta name="keywords" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<script type="text/javascript" src="/js/highslide.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '/images/graphics/';
</script>
<link href="/css/highslide.css" rel="stylesheet" type="text/css" />
<script src="/js/ajax.js" type="text/javascript"></script>
<script type="text/javascript" language="JavaScript">
<!--
function ShowDog(d) {
var i;
	for(i=0;i<=3;i++){
		var hd="d"+i;
		if(d == hd){
		document.getElementById(d).style.display = "block";
		}else{
		document.getElementById(hd).style.display = "none";
		}
	}
}

function ShowPuppy(d) {
var i;
	for(i=0;i<=3;i++){
		var hp="p"+i;
		if(d == hp){
		document.getElementById(d).style.display = "block";
		}else{
		document.getElementById(hp).style.display = "none";
		}
	}
}
//-->
</script>
<script language="javascript">
<!--
function LoadHAjax(){
OpenAjaxPostCmd('hr-articles.php','hrarticles','','Loading....','hrarticles','2','2');
OpenAjaxPostCmd('hf-articles.php','hfarticles','','Loading....','hrarticles','2','2');
OpenAjaxPostCmd('adog.php','adog','','Loading....','adog','2','2');
OpenAjaxPostCmd('apuppy.php','apuppy','','Loading....','apuppy','2','2');
OpenAjaxPostCmd('home-top-qna.php','topqna','','Loading....','topqna','2','2');
OpenAjaxPostCmd('home-rec-qna.php','recqna','','Loading....','recqna','2','2');
}
//-->
</script>

<?php require_once('common/top.php'); ?>
<div id="pagebody">

<div id="HflotL">
<div id="OutFrame">
        <div id="InFrame">
          <div id="hfarticles">
          
          </div>
        </div>
    </div>
<div id="OutFrame">
        <div id="InFrame">
          <div id="recqna">
          </div> 
        </div>
    </div>
    <div id="OutFrame">
        <div id="InFrame">
          <div id="adog">
          
          </div>
            
        </div>
    </div>
     
</div>
<div id="HflotR">
<div id="OutFrame">
        <div id="InFrame">
        <div id="hrarticles">
          
          </div> 
        </div>
    </div>
    <div id="OutFrame">
        <div id="InFrame">
          <div id="topqna">
          </div>         
        </div>
    </div>
    <div id="OutFrame">
        <div id="InFrame">
          <div id="apuppy">
          
          </div>
          
        </div>
    </div>
    
</div>
<div id="clearall"></div>          
</div>
<?php require_once('common/bottom.php'); ?>
