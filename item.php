<?php
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys-nice.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
$sitesection = "shop";
$session_id = session_id();

$qdata=query_execute("SELECT item_id, type_id, name, item_about, title, keyword, description, price, weight, stock_status, num_views, item_brand FROM shop_items WHERE nice_name='$section[1]'");
$rowTotal = mysql_num_rows($qdata);
if($rowTotal==0){
	header("HTTP/1.0 404 Not Found");
	require_once($DOCUMENT_ROOT.'/404.php');
	die(mysql_error());
	exit();
}

$rowdat = mysql_fetch_array($qdata);
$item_id = $rowdat["item_id"];
$type_id = $rowdat["type_id"];
$price = $rowdat["price"];
$weight = $rowdat["weight"];
$brand_id = $rowdat["item_brand"];

$upView=query_execute("UPDATE shop_items SET num_views = num_views+1 WHERE item_id='$item_id'");

$qCat=query_execute("SELECT category_id FROM shop_item_category WHERE item_id='$item_id'");
$rowCat = mysql_fetch_array($qCat);

$qBrand=query_execute("SELECT brand_name FROM shop_brand WHERE brand_id='$brand_id'");
$rowBrand = mysql_fetch_array($qBrand);
$brandName=$rowBrand["brand_name"];
// Get all images
$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
while($rowdatM = mysql_fetch_array($qdataM)){
	$ArrayAllImg[]=$rowdatM["media_file"].';'.$rowdatM["label"];
}
$imgFLBase=explode(';',$ArrayAllImg[0]);
// END

$catName=$ArrayShopCategorys[$rowCat["category_id"]];
$pageHone=$rowdat["name"];
$art_body = stripslashes($rowdat["item_about"]);
$art_body = strip_tags($art_body);
$art_body = trim($art_body);
$art_body = substr($art_body,0,240);
$art_body = stripslashes($art_body);

$pageTitle=$pageHone.', '.$catName.', '.$brandName.', Pet Shop India DogSpot.in';
$pageKeyword=$pageHone.', '.$catName.', '.$brandName.', Pet Shop India, Pet Shop, DogSpot.in';
$pageDesc=$pageHone.' '.$catName.' '.$art_body.' Pet Shop India DogSpot.in';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$pageTitle?></title>
<meta name="keywords" content="<?=$pageKeyword?>" />
<meta name="description" content="<?=$pageDesc?>" />
<meta property="og:image" content="<?='http://www.dogspot.in/shop/item-images/thumb_'.$imgFLBase[0];?>"/>
<link href="/shop/css/shop.css" rel="stylesheet" type="text/css" />
<link href="/js/starmatrix.css" rel="stylesheet" type="text/css" />
<script src="/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/jquery/jquery.validate.js"></script>
<script type='text/javascript' src='/js/shaajax2.js'></script>
<script type="text/javascript">
 jQuery(document).ready(function() {
	  jQuery("#formShopItem").validate({
		  
	});
});

function updatePrice(optprice, childatt){
	if(optprice!=""){
		if(childatt==1){
		var attval=optprice.split("-");
		ShaAjaxJquary('/shop/ajax/load-attribute.php?item_id='+attval[3]+'&att_id='+attval[2]+'', '#att_2', '', '', 'GET', '#att_Load', 'Loding...', 'REP');
		}
		optprice=parseFloat(optprice);
		//if(optprice!=0){
			var itembprice=$('#itembasceprice').val();
			itembprice=parseFloat(itembprice);
			if(optprice==''){
				var newprice = itembprice;
			}else if(optprice == -1){
				var newprice = itembprice;
			}else{
				var newprice = itembprice + optprice;
			}
			$('#ItemPriceOnly').html(newprice);
			$('#item_price').val(newprice);
		//}
	}	
}
</script>
<?php require_once($DOCUMENT_ROOT.'/shop/common/top.php'); ?>
<div id="pagebody">

<p class="sitenavi"><a href="/">Home</a> &gt; <a href="/shop/">Shop</a> &gt; <?=$catName?></p>
<div style="float:left; width:716px;">
<?
if($imgFLBase[0]){
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/'.$imgFLBase[0];
		$imageURL='/shop/item-images/'.$imgFLBase[0];
	}else{
		$imagepath = $DOCUMENT_ROOT.'/shop/image/no-photo.jpg';
		$imageURL='/shop/image/no-photo.jpg';
	}

if(file_exists($imagepath)){
	$new_w = 360;
	$new_h = 312;
	$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
}
?>
<div id="ShopItemImgBox">
  <div id="ShopItemImgMain" align="center"><img src="<?=$imageURL;?>" alt="<?=$imgFLBase[1]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  title="<?=$imgFLBase[1]?>" align="middle"/></div>
<div id="ShopItemImgThubm">
<? 
if($ArrayAllImg){
foreach($ArrayAllImg as $imgFileLabel){
	$imgFL=explode(';',$imgFileLabel);
	if($imgFL[0]){
		$imagepath = $DOCUMENT_ROOT.'/shop/item-images/'.$imgFL[0];
		$imageURL='/shop/item-images/thumb_'.$imgFL[0];
	}else{
		$imagepath = $DOCUMENT_ROOT.'/shop/image/no-photo-t.jpg';
		$imageURL='/shop/image/no-photo-t.jpg';
	}
	if(file_exists($imagepath)){
	  $new_w = 80;
      $new_h = 80;
      $imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
 	}
?>
<div id="ShopItemThumBox" align="center">
 <a href="<? echo '/shop/item-images/'.$imgFL[0];?>" class="highslide" onclick="return hs.expand(this, {captionId: 'caption1'})">
<img src="<?=$imageURL?>" alt="<?=$imgFL[1]?>" width="<? echo"$imgWH[0]";?>" height="<? echo"$imgWH[1]";?>"  border="0"  align="middle" title="<?=$imgFL[1]?>"/></a></div>
<? }} ?>
</div>
<div style="clear:both"></div>
</div>

<div id="ShopItemDesBox">
<form id="formShopItem" name="formShopItem" method="post" action="/shop/cart.php">
<div id="ShopItemHone">
<h1><?=$pageHone;?></h1>
<span class="ShopGray">Item #: <?=$item_id?></span>
<div class="ShopGray">Availability: <?=$AShopStock[$rowdat["stock_status"]]?> <? if($sessionLevel == 1){?>
  <a href="/shop/adminshop/add-item.php?action=edit&item_id=<?=$item_id?>">Edit</a>
  <? }?></div>
</div>
<div id="ShopItemPrice">Rs. <span id="ItemPriceOnly"><?=number_format($price)?></span></div>

<div id="att_Load" style=" position:relative; margin-top:30px; margin-right:400px; float:right"></div>
<div id="clearall"></div>
<div id="ShopItemCustom">
<?
if($type_id=='simple'){
	$qAtt=query_execute("SELECT attribute_set_id, attribute_value_id FROM shop_item_attribute WHERE item_id = '$item_id'");
		while($rowAtt = mysql_fetch_array($qAtt)){
			$attribute_set_id=$rowAtt["attribute_set_id"];
			$attribute_value_id=$rowAtt["attribute_value_id"];
			if($attribute_set_id){
				echo $Ashop_attribute_set[$attribute_set_id].': '.$Ashop_attribute_value[$attribute_value_id].'<br>';
			}
		}
}else{
	$qChild=query_execute("SELECT item_id FROM shop_items WHERE item_parent_id='$item_id'");
	while($rowChild = mysql_fetch_array($qChild)){
			$Childitem_id=$rowChild["item_id"];
			
	
	$qAtt=query_execute("SELECT DISTINCT attribute_set_id, attribute_value_id, attribute_value FROM shop_item_attribute WHERE item_id='$Childitem_id' AND attribute_set_id != 0 ORDER BY attribute_set_id");
		while($rowAtt = mysql_fetch_array($qAtt)){
			
			$attribute_set_id = $rowAtt["attribute_set_id"];
			$attribute_value_id = $rowAtt["attribute_value_id"];
			$attribute_value=$rowAtt["attribute_value"]-$price;
			//if($attribute_value==$price){$attribute_value=0;}
			if($attribute_set_id != 0){
				$Aattribut[] = "$attribute_set_id-$attribute_value_id-$attribute_value";
			}
			//echo 'Choose a '.$Ashop_attribute_set[$attribute_set_id].': '.$Ashop_attribute_value[$attribute_value_id].' '.$attribute_value.'.<br>';
		}
	}
	$Aattribut = array_unique($Aattribut);
	asort($Aattribut);
	//print_r($Aattribut);
	$keyold=0;
	foreach($Aattribut as $aatt){
		$attkey=explode('-', $aatt);
		if($attkey[0]==$keyold){
			//echo $Ashop_attribute_value[$attkey[1]].' '.$attkey[2].'.<br>';
			echo '<option value="'.$attkey[2].'-'.$attkey[0].'-'.$attkey[1].'-'.$item_id.'">'.$Ashop_attribute_value[$attkey[1]].'';if($attkey[2]){echo' +Rs '.$attkey[2].'';} echo'</option>';
		}else{
			if($selstart==1){ echo'</select></div>';}
			?>
            <div id="att_<?=$attkey[0]?>">
            <?
			echo 'Choose a '.$Ashop_attribute_set[$attkey[0]].': <br>';
			?>
            
            <select name="ItemOption[<?=$attkey[0]?>]" id="select<?=$attkey[0]?>" onchange="Javascript:updatePrice($(this).val(), '<?=$attkey[0]?>');" class="required"<? if($keyold!=0){echo'disabled="disabled"';}?>>
            <option value="">Select</option>
            <?
			echo '<option value="'.$attkey[2].'-'.$attkey[0].'-'.$attkey[1].'-'.$item_id.'">'.$Ashop_attribute_value[$attkey[1]]; if($attkey[2]){echo ' +Rs '.$attkey[2].'';} echo '</option>';
            //echo $Ashop_attribute_value[$attkey[1]].' '.$attkey[2].'.<br>';			
		}
		$selstart=1;
		$keyold=$attkey[0];
	}
	?></select></div><?
}
?>

<div>
</div>
<div id="ShopQtyBox">
Qty. <input name="item_qty" type="text" class="ShopQty required" style="text-align:center; width:20px;" id="item_qty" value="1" />
  <input name="itembasceprice" type="hidden" id="itembasceprice" value="<?=$price?>" />
  <input name="item_id" type="hidden" id="item_id" value="<?=$item_id;?>" />
<input name="item_price" type="hidden" id="item_price" value="<?=$price?>" />
<input name="type_id" type="hidden" id="type_id" value="<?=$type_id?>" />
</div><div id="ShopAddtoCart"><input type="image" src="/shop/image/addtocart.jpg" alt="Submit" style="border:0px;"/></div>

</div>
</form>
<div style="width:300px;">
<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://www.dogspot.in/shop/<?=$section[1]?>" width="300" show_faces="false" font=""></fb:like>
<p><?=$rowdat["num_views"]?> times viewed</p>
<div id="rateIt">
<ul class="rating <? print(getRating("SELECT rating, num_votes FROM section_rating WHERE section_id = '$item_id' AND site_section = 'item'"));?>">
	<li class="one"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=1&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="1 Star">1</a></li>
	<li class="two"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=2&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="2 Stars">2</a></li>
	<li class="three"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=3&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="3 Stars">3</a></li>
	<li class="four"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=4&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="4 Stars">4</a></li>
	<li class="five"><a href="Javascript:ShaAjaxJquary('/rateIt-new.php?rateThis=rateThis&rating=5&section_id=<?=$item_id?>&site_section=item', '#rateIt', '', '', 'GET', '#rateLoad', 'Rating...', 'REP')" title="5 Stars">5</a></li>
</ul>
</div>
<div id="rateLoad"></div>
<div style="clear:both"></div>
<!--<p><? ?> Reviews</p>
<p><a href="#" >Write your own Review</a></p>-->

</div>
</div>

<div style="clear:both"></div>
<div id="ShopItemDetail" style="margin-top:52px;">
<h2>Product Summary:</h2>
<?=$rowdat["item_about"]?>
</div>

<!--<div id="listHeading"><span>Reviews</span></div>
<div id="listHeading"><span>Post your Review</span></div>
<form id="formReview" name="formReview" method="post" action="">

<div id="postReview">
  <div id="reviewImg"><img src="/profile/images/1199979738.jpg" width="50" height="50" /></div>
<div id="yourReview">Review</div>
<div style="clear:both"></div>
</div>
<div id="postReview">
<div id="reviewImg"><img src="/profile/images/1199979738.jpg" width="50" height="50" /></div>
<div id="yourReview">Review</div>
<div style="clear:both"></div>
</div>
<div id="postReview">
<div id="reviewImg"><img src="/profile/images/1199979738.jpg" width="50" height="50" /></div>
<div id="yourReview">
<textarea name="review_body" id="review_body" cols="45" rows="5" class="textWrite"></textarea>
<div style="float:right"><input type="submit" name="button" id="button" value="Post" /></div>
</div>
<div style="clear:both"></div>
</div>
</form>-->
<?
$qChild=query_execute("SELECT related_items_id FROM shop_items_related WHERE item_id='$item_id'");
	while($rowChild = mysql_fetch_array($qChild)){
			$arrayRelated[]=$rowChild["related_items_id"];
	}
$qChild=query_execute("SELECT item_id FROM shop_items_related WHERE related_items_id='$item_id'");
	while($rowChild = mysql_fetch_array($qChild)){
			$arrayRelated[]=$rowChild["item_id"];
	}

if($arrayRelated){
$arrayRelated=array_unique($arrayRelated);
$totalRelated=count($arrayRelated);
}
if($totalRelated > 0){
?>
<div id="clearall" style="height:5px;"></div>
<div id="listHeading"><span>Related Products</span></div>
<?
$i=0;
foreach($arrayRelated as $relatedid){
$qItem=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price FROM shop_items as i WHERE i.visibility='visible' AND item_id='$relatedid'");

$rowItem = mysql_fetch_array($qItem);
	$item_id=$rowItem["item_id"];
	$name=$rowItem["name"];
	$nice_name=$rowItem["nice_name"];
	$price=$rowItem["price"];
	
	$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id'");
	$rowdatM = mysql_fetch_array($qdataM);
	//$rowdatM["media_file"].';'.$rowdatM["label"];
	$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
	if(file_exists($imagepath)){
		$new_w = 200;
		$new_h = 150;
		$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
    }
?>
<div id="ShopItemBox" style="height: 215px; width: 215px;">
<div id="ShopItemImage" style="height:162px;"><a href="/shop/<?=$nice_name?>"><img src="/shop/item-images/thumb_<?=$rowdatM["media_file"]?>" alt="<?=$rowdatM["label"]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/></a></div>
<div id="ShopItemDet"><a href="/shop/<?=$nice_name?>"><?=snippetwop($name,$length=30,$tail="...")?></a></div>
<div id="ShopItemPriceList">Rs. <?=number_format($price)?></div>
</div>
<?
$i=$i+1;
if($i==8){break;}
}}
?>
</div>
<div style="float:left; width:240px;">
<div id="quickText">
<div id="qtext" ><span class="tBig">Free Shipping</span> <span class="tSmall">All Products</span></div>
<div id="qtext" style="margin-top:2px;"><span class="tBig">24x7 Care</span> <span class="tSmall">+91-9818671863</span></div>
<div id="qtext" style="margin-top:2px;"><span class="tBig">Original</span> <span class="tSmall">Branded Products</span></div>
</div>
<?
$qItem=query_execute("SELECT i.item_id, i.name, i.nice_name, i.price FROM shop_items as i WHERE i.item_brand = '$brand_id' AND i.visibility='visible' LIMIT 10");
while($rowItem = mysql_fetch_array($qItem)){
	$item_id=$rowItem["item_id"];
	$name=$rowItem["name"];
	$nice_name=$rowItem["nice_name"];
	$price=$rowItem["price"];
	
	$qdataM=query_execute("SELECT media_file, label FROM shop_item_media WHERE item_id='$item_id' ORDER BY position ASC");
	$rowdatM = mysql_fetch_array($qdataM);
	//$rowdatM["media_file"].';'.$rowdatM["label"];
	$imagepath = $DOCUMENT_ROOT.'/shop/item-images/thumb_'.$rowdatM["media_file"];
	if(file_exists($imagepath)){
		$new_w = 150;
		$new_h = 140;
		$imgWH = WidthHeightImg($imagepath,$new_w,$new_h); 
    }
?>
<div id="ShopItemBox" style="width:228px; height:200px;">
<div id="ShopItemImage" style="height:162px;"><a href="/shop/<?=$nice_name?>"><img src="/shop/item-images/thumb_<?=$rowdatM["media_file"]?>" alt="<?=$rowdatM["label"]?>" width="<?=$imgWH[0];?>" height="<?=$imgWH[1];?>" border="0"  align="middle" title="<?=$rowdatM["label"]?>"/></a></div>
<div id="ShopItemDet"><a href="/shop/<?=$nice_name?>"><?=$name;?></a></div>
<div id="ShopItemPriceList">Rs. <?=number_format($price)?></div>
</div>
<? }?>
</div>
<div style="clear:both"></div>
</div>
<?php require_once($DOCUMENT_ROOT.'/common/bottom.php'); ?>