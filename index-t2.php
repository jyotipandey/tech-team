<?php 
ob_start();
session_start();
$userid = $_SESSION['sessionuserid'];

if ($logoff == "logoff"){
session_destroy();
// refresh the page
header ("Location: index-t.php"); 
ob_end_flush();
exit(); 
}
$sitesection = "HOME";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DogSpot: Dog lovers Community Health Information</title>
<meta name="keywords" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />
<meta name="description" content="Dog lovers Community, dogs, dogs india, dogs world, puppies, dog" />


<?php require_once('common/top.php'); ?>

    <div id="pagebody">
      <h1>About DogSpot</h1>
      <p>I have a dog. And I sometimes think of all those wonderful people who own a dog. I wish I could meet them at one place -- something like 101 Dalmatians -- a home full of dogs &amp; dog lovers, sharing experiences, tips to dog grooming, dog training, dog food, places to buy dog merchandise, anything at all under the sun. I wish I could share my experiences, the kind of mate I am looking for my dog, coz I best know his likes and dislikes. I wish I knew of the nearest vet to take my dog for his shots, and oh! His food is finishing too… sometimes, I really wish, I had DogSpot -- a Dog Lover’s community </p>
      <h3>Coming Soon… </h3>
    <?php require_once('common/bottom.php'); ?>
