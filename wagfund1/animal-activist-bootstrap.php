<?php

require_once($DOCUMENT_ROOT . "/constants.php");
require_once($DOCUMENT_ROOT . '/session.php');
require_once($DOCUMENT_ROOT . '/database.php');
require_once($DOCUMENT_ROOT . '/shop/functions.php');
require_once($DOCUMENT_ROOT . '/functions.php');
require_once($DOCUMENT_ROOT . '/shop/arrays/arrays.php');
require_once($DOCUMENT_ROOT . '/shop/wagfund_function.php');

$user_ip     = ipCheck();
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];



 

if($userid!='Guest'){
$sel_items=mysql_query("SELECT item_id FROM shop_cart WHERE userid='$userid' AND cart_order_status='new' AND item_qty!=''");
while($row1=mysql_fetch_assoc($sel_items)){
$item_arr[]=$row1['item_id'];
}
}else{
$sel_items=mysql_query("SELECT item_id FROM shop_cart WHERE session_id='$session_id' AND cart_order_status='new' AND item_qty!=''");
while($row1=mysql_fetch_assoc($sel_items)){
$item_arr[]=$row1['item_id'];
}
}
$title="Social Animal Activists | Animal Lovers Story | Animal Welfare Foundations";
$keyword="Animal Activists, social activists, animal lovers, animal welfare foundations";
$desc="Animal social activists works for animal welfare. Here is the list of few active animal lovers and foundations’ stories who religiously work for these adorable creatures.";
 
    $alternate="https://www.dogspot.in/animal-activist/";
	$canonical="https://www.dogspot.in/animal-activist/";
	$og_url=$canonical;
	$imgURLAbs="https://www.dogspot.in/new/pix/animal-ativist2.jpg";
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css" rel="stylesheet" href="/bootstrap/css/wagfund.css?=5" />
<div class="breadcrumbs">
  <div class="container">
    <div class="row" itemscope itemtype="http://schema.org/Breadcrumb">
      <div class="col-xs-12" itemscope itemtype="http://schema.org/BreadcrumbList"> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a href="/" itemprop="item"><span itemprop="name">Home</span></a>
        <meta itemprop="position" content="1" />
        </span> <span> / </span> <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <span itemprop="item"> <span itemprop="name" class="active-bread">Animal Activist </span></span>
        <meta itemprop="position" content="2" />
        </span> </div>
    </div>
  </div>
</div>

<section class="animal-activist">
  <div class="container">
    <div class="share-box" style="margin:20px 0px">
      <? 
                            $surl= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        	include($DOCUMENT_ROOT."/new/articles/social-bootstrap.php"); 
                        ?>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <h1>Animal Activists Stories</h1>
        <p>There are many wonderful souls across the nation who work extra hours and put in efforts beyond their capacity. These animal activists feed and take care of dogs in and around their vicinity. </p>
        <p>DogSpot introduces some of them to you. Their amazing story will melt your heart. Watch the video to know more about them individually. </p>
      </div>
      <form name="formcomnt" id="formcomnt">
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="wf_recboder_new">
            <h2>
              <label for="radio6" class="css-label">Nadir Khan</label>
            </h2>
            <div class="wf_productDetail"> 
              <!--<div class="wf_productNsme">
    
     </div>-->
              <div class="wf_productImg wf_userImg text-center"> <img src="https://ik.imagekit.io/2345/wagfund1/images/video-sceenshot-wagfund.png" alt="Nadir Khan - social activists" title="Nadir Khan - social activists" border="0" align="middle" width="300px" class="img-responsive" > </div>
              <div class="wf_productTxtdetail wf_userTxtdetail">
                <div class="wf_productNsme">
                  <p>Nadir Khan, a social activist talks about dog neutering, timely vaccinations and helping the homeless stray. It is inspirational to see how much efforts he puts for the welfare of these mute species. Watch this video to know more.</p>
                </div>
              </div>
            </div>
            <div class="youtube-video text-center">
              <iframe width="450" height="236" src="//www.youtube.com/embed/qIyizaYMXKs" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="wf_recboder_new wf_recboder_newm_left">
            <h2>
              <label for="radio6" class="css-label">Dinesh Sharma</label>
            </h2>
            <div class="wf_productDetail"> 
              <!--<div class="wf_productNsme">

 </div>-->
              <div class="wf_productImg wf_userImg text-center"> <img src="https://ik.imagekit.io/2345/wagfund1/images/Sharma-ji.jpg" alt="dinesh sharma - pet lover" border="0" title="dinesh sharma - pet lover" align="middle"   width="300px" class="img-responsive"> </div>
              <div class="wf_productTxtdetail wf_userTxtdetail">
                <div class="wf_productNsme">
                  <p>Stories of pet lovers ave always intriguied us. Watch this heart warming video of Mr Dinesh and Mrs Shushma a couple in love with Dogs. Their amazing story will melt your heart.</p>
                </div>
              </div>
            </div>
            <div class="youtube-video text-center">
              <iframe width="450" height="236" src="//www.youtube.com/embed/KYUdWH3rGME" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="wf_recboder_new">
            <h2>
              <label for="radio6" class="css-label">Posh Foundation (Adopt A Pet)</label>
            </h2>
            <div class="wf_productDetail"> 
              <!--<div class="wf_productNsme">

 </div>-->
              <div class="wf_productImg wf_userImg text-center"> <img src="https://ik.imagekit.io/2345/wag_club/images/posh-foundation.jpg" alt="Posh Foundation (Adopt A Pet)" border="0" align="middle" title="Posh Foundation (Adopt A Pet)" width="300px" > </div>
              <div class="wf_productTxtdetail wf_userTxtdetail">
                <div class="wf_productNsme">
                  <p>“More than machinery, we need humanity. We think too much, heal too little” says Posh Foundation that promotes stray welfare and adoption. Watch this video to know more about this foundation.</p>
                </div>
              </div>
            </div>
            <div class="youtube-video text-center" style="margin-bottom: 40px;">
              <iframe width="450" height="236" src="//www.youtube.com/embed/SpivwO6MFSg" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="wf_recboder_new wf_recboder_newm_left">
            <h2>
              <label for="radio6" class="css-label">Shawlinee</label>
            </h2>
            <div class="wf_productDetail"> 
              <!--<div class="wf_productNsme">

 </div>-->
              <div class="wf_productImg wf_userImg text-center"> <img src="https://ik.imagekit.io/2345/wagfund1/images/shawlinee1.jpg" alt="shawlinee - dog lover" border="0" align="middle" title="shawlinee - dog lover" width="300px" > </div>
              <div class="wf_productTxtdetail wf_userTxtdetail">
                <div class="wf_productNsme">
                  <p>Having been initially afraid of dogs, Shawlinee got over her fear after bringing home her first dog. She is a self-confessed dog lover who now feeds and cares for the numerous dogs that reside around her office complex. And her compassion does not end there. Shawlinee has also helped with adoptions, sterilization drives and rescues. Watch her story unfold.</p>
                </div>
              </div>
            </div>
            <div class="youtube-video text-center">
              <iframe width="450" height="236" src="//www.youtube.com/embed/ObDFCj18h1w" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<?php
require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');
?>
