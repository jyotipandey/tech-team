<?php
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/arrays.php');
include($DOCUMENT_ROOT."/dogs/arraybreed.php");



$sitesection = "HTML Sitemap";
$session_id = session_id();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">



<head>
<meta name="verify-v1" content="S4buEED+dnbQpGb/4G2mfZijE/+9tuZfJqToVisU4Bo=" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>DogSpot Sitemap</title>
<meta name="keywords" content="DogSpot Sitemap" />
<meta name="description" content="Navigate all Pages of Dog Products, Photos, Brands and Breeds of Dog through DogSpot Sitemap." />
<meta name="robots" content="noindex, nofollow">
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<script type="text/javascript" src="/new/js/sitemap-jquery.js"></script>

<script type="text/javascript"> 
$(document).ready(function(){
$(".paperflip").click(function(){
	   $(".paper").slideToggle("slow");
  });
  
  $(".filmflip").click(function(){
    $(".film").slideToggle("slow");
  });
  
  $(".wovenflip").click(function(){
    $(".woven").slideToggle("slow");
  });
  
  $(".eventflip").click(function(){
    $(".event").slideToggle("slow");
  });
  
   $(".shopflip").click(function(){
    $(".shop").slideToggle("slow");
  });
  
   $(".microflip").click(function(){
    $(".micro").slideToggle("slow");
  });
  $(".trendflip").click(function(){
    $(".trend").slideToggle("slow");
  });
   $(".photoalbum").click(function(){
    $(".photo").slideToggle("slow");
  });
   $(".shopbrands").click(function(){
    $(".brands").slideToggle("slow");
  });
});


</script>
  
<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
}
/* Zebra striping */

th { 
  background:#ddd;
  color: #fff; 
  font-weight:normal;
  font-size:16px;
}
td { 
  
  width:20%;
  
}
td, th { 
  padding: 5px; 
  border: 1px solid #ccc; 
  text-align: left; 
  font-weight:normal;
  font-size:12px;
}
</style>

<?php require_once($DOCUMENT_ROOT.'/new/common/header.php'); ?>   
 <!-- breadcrumb -->

        	<div class="breadcrumb">
            	<div class="header" style="height:0;">
                <div class="cont980"> 
                    <div class="fl">
                        <p align="left"><a href="/">Home</a> &gt; Sitemap</p>
                     </div>
                 
                     <div class="fr">
              <p align="right">
                        <a href="https://www.facebook.com/indogspot"><img src="/new/pix/bdcmb_fbicon.gif" alt="facebook" title="facebook"  /></a><a href="https://twitter.com/indogspot"><img src="/new/pix/bdcmb_twitter.gif" alt="twitter" title="twitter"  /></a></p></div>
                     </div>
                     <div class="cb"></div>
                 </div>
            
            </div><!-- breadcrumb -->  
<div class="cont980"> 
<div class="paperflip" >

     <p><h1 style="background:#ddd; float:left; width:100%" >Dog Breeds</h1></p>
     <hr>
     <div class="paper">
      <table>
<thead>
<tr>
      <? $result1 = mysql_query("SELECT breed_name,nicename FROM dog_breeds WHERE breed_engine='1' ORDER BY breed_name ASC ");
	  $s=0;
while($rowArt1 = mysql_fetch_array($result1)){     ?>
    <td ><a href="/<?=$rowArt1['nicename']?>/"><?=ucwords(strtolower($rowArt1['breed_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>


<? // sitemap for articles ?>
     <div class="paperflip" >
     <p><h3 style="background:#ddd;">Articles</h3></p>
     <hr>
     <div class="paper">
      <table>
<thead>
<tr>
      <? 
	 // mysql_connect("localhost", "dogspot", "india123dogs");
//mysql_select_db("test");
	  $result1 = mysql_query("SELECT DISTINCT(wptr.term_taxonomy_id) , wtt.term_id , wt.name , wt.slug FROM wp_term_relationships as wptr , wp_term_taxonomy as wtt , wp_terms as wt WHERE wptr.term_taxonomy_id=wtt.term_taxonomy_id AND wtt.taxonomy='category' AND wt.term_id=wptr.term_taxonomy_id");
	  $s=0;
while($rowArt1 = mysql_fetch_array($result1)){     ?>
    <td ><a href="https://www.dogspot.in/dog-blog/<?=$rowArt1['slug'];?>/"><?=ucwords(strtolower($rowArt1['name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      <br />
      <div class="filmflip" >
     <p><h3 style="background:#ddd;"> Qna</h3></p>
     <hr>
     <div class="film">
      <table>
<thead>
<tr>
      <? 	  //mysql_connect("localhost", "dogspot", "india123dogs");
//mysql_select_db("dogspot_dogspot");
	   $result3 = mysql_query("SELECT cat_nicename,cat_desc FROM qna_cat ");
	  $s=0;
while($rowArt3 = mysql_fetch_array($result3)){     ?>
    <td ><a href="https://www.dogspot.in/qna/<?=$rowArt3['cat_nicename'];?>/"><?=ucwords(strtolower($rowArt3['cat_desc']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      <br />
      <div class="wovenflip" >
     <p><h3 style="background:#ddd;"> Business</h3></p>
     <hr>
     <div class="woven">
      <table>
<thead><tr>

<tr>
      <? $result4 = mysql_query("SELECT category_name,cat_nice_name FROM business_category ");
	  $s=0;
while($rowArt4 = mysql_fetch_array($result4)){     ?>
    <td ><a href="https://www.dogspot.in/dog-listing/category/<?=$rowArt4['cat_nice_name'];?>/"><?=ucwords(strtolower($rowArt4['category_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      <br />
      <div class="eventflip" >
     <p><h3 style="background:#ddd;"> Dog Gallery</h3></p>
     <hr>
     <div class="event">
      <table>
<thead>
<tr>
      
<!--   <td ><a href="/dogs/category/sale/"  title="Looking for a Dogs"> Looking for a Dog</a></td>-->
<!--<td ><a href="/dogs/category/buy/"  title="Dogs for Sale"> Available for Sale</a></td>-->
<td ><a href="/dogs/category/dating/"  title="Breeding Dogs">For Dating</a></td>
<td ><a href="/dogs/category/male/"  title="Dogs India">Male Dogs</a></td>

</tr><tr>
<td ><a href="/dogs/category/female/"  title="Bitch Dog India">Female Dogs</a></td>
      </tr>
      </thead></thead></table></div></div>
      <br />
      
      <table>
<thead><tr>
<th colspan="4" ><a href="https://www.dogspot.in/puppies/">
<h3>Puppies Available</h3></a></th>
</tr>
<tr>
<td ><a href="/puppies/category/sale/"  title="Puppies for Sale">Puppies for Sale</a></td>
<td ><a href="/puppies/category/buy/"  title="Puppies India"> Puppies Wanted</a></td>
<td ><a href="/puppies/category/male/"  title="Puppy Dog"> Male Puppies</a></td>
<td ><a href="/puppies/category/female/"  title="Puppy Bitch"> Female Puppies</a> </td>     
</tr>
      </thead></thead></table>
     <br />
      <div class="shopflip" >
     <p><h3 style="background:#ddd;"> Shop</h3></p>
     <hr>
     <div class="shop">
     <table>
<thead>
<tr>
      <? 
	  $result2 = mysql_query("SELECT category_name,category_nicename FROM shop_category WHERE domain_id='1' OR domain_id='3'");
	  $s=0;
while($rowArt2 = mysql_fetch_array($result2)){     ?>
    <td ><a href="https://www.dogspot.in/<?=rtrim($rowArt2['category_nicename']);?>/"><?=ucwords(strtolower($rowArt2['category_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      
  <br />
  <div class="shopbrands" >
     <p><h3 style="background:#ddd;"> Brands</h3></p>
     <hr>
     <div class="brands">
     <table>
<thead>
<tr>
      <? $result_brand = mysql_query("SELECT brand_name,brand_nice_name FROM shop_brand WHERE brand_status='1' AND domain_id='1'");
	  $s=0;
while($rowArt_brand1 = mysql_fetch_array($result_brand)){     ?>
    <td ><a href="https://www.dogspot.in/<?=$rowArt_brand1['brand_nice_name'];?>/"><?=ucwords(strtolower($rowArt_brand1['brand_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      
  <br />
  <div class="microflip" >
     <p><h3 style="background:#ddd;"> Microsite</h3></p>
     <hr>
     <div class="micro">
  <table>
<thead>
<tr>
      <? $result124 = mysql_query("SELECT club_name,club_nicename FROM club_main ");
	  $s=0;
while($rowArt124 = mysql_fetch_array($result124)){     ?>
    <td ><a href="https://www.dogspot.in/<?=$rowArt124['club_nicename'];?>/"><?=ucwords(strtolower($rowArt124['club_name']));?></a></td>  
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
      
      <br />
      <div class="photoalbum" >
     <p><h3 style="background:#ddd;"> Photo Album</h3></p>
     <hr>
     <div class="photo">
  <table>
<thead>
<tr >
      <? $result22 = mysql_query("SELECT distinct(pa.album_id) as album_id FROM photos_image as pid,photos_album as pa WHERE pa.album_id=pid.album_id AND pa.album_name!='' AND pa.publish_status='publish' AND pa.album_nicename!='' ORDER BY pa.album_id desc limit 600");
	  $s=0;
while($rowArt22 = mysql_fetch_array($result22)){  
$album_id=$rowArt22['album_id'];
$result23=query_execute_row("SELECT album_nicename,album_name FROM photos_album WHERE album_id='$album_id'");
$alb=snippetwop($result23['album_name'], 20, "...");
 
  ?>
    <td ><a href="https://www.dogspot.in/photos/album/<?=$result23['album_nicename'];?>/"><?=ucwords(strtolower($alb));?></a></td>  <? //} ?>
	  
	  <?
$s=$s+1;
if($s=='4') {
echo "</tr> <tr>";	
$s=0;
}

 }?>
      </tr>
      </thead></thead></table></div></div>
    
              
      <br />

     <p><h3 style="background:#ddd;"> Search Trends</h3></p>
     <hr>
     
      <table>
<thead>
<tr>
<td ><a href="/breed-puppies-for-sale/"  title="Puppy Breed For Sale">Search Puppy Breed For Sale By City</a></td>
<td ><a href="/search-dog-for-sale-by-city/"  title="Dog For Sale">Search Dog For Sale By City</a></td>
<td ><a href="/search-puppies-for-sale-by-city/"  title="Puppy For Sale"> Search Puppy For Sale By City</a></td>
<td ><a href="/dog-trainers/"  title="Dog Trainer"> Search Dog Trainers By City</a></td>
<td ><a href="/dog-breeders/"  title="Dog Breeder"> Search Dog Breeders By City</a></td>
</tr>
<tr>
<td ><a href="/breed-dogs-for-sale/"  title="Dog Breed For Sale"> Search Dogs Breed For Sale By City</a></td>
<td ><a href="/dog-price/"  title="Search Dogs Price By City"> Search Dogs Price By City</a></td>
<td ><a href="/puppy-price/"  title=" Search Puppy Price By City"> Search Puppy Price By City</a></td>
<td ><a href="/breed-dog-price/"  title=" Search Breed Dog Price By City"> Search Breed Dog Price By City</a></td>
<td ><a href="/breed-puppy-price/"  title="Search Breed Puppy Price By City<"> Search Breed Puppy Price By City</a></td>   
</tr>
<tr>
<td ><a href="/dog-kennels/"  title="Dog Kennels"> Search Dogs Kennels By City</a></td>
<td ><a href="/dry-dog-food-by-lifestage/"  title="Search Dry Dog Food By Lifestage"> Search Dry Dog Food By Lifestage</a></td>
<td ><a href="/dry-dog-food-by-breed/"  title="Search Dry Dog Food By Breed"> Search Dry Dog Food By Breed</a></td>
<td ><a href="/dry-dog-food-by-breed-type/"  title="Search Dry Dog Food By Breed Type"> Search Dry Dog Food By Breed Type</a></td>
<td ><a href="/dog-breeds-comparison/"  title="Dog Breeds Comparison"> Dog Breeds Comparison</a></td>
</tr>
<tr>
<td ><a href="/trends-for-breeds-products/"  title="Trends For Breeds Products"> Trends For Breeds Products</a></td>
<td ><a href="/dog-food-by-city/"  title="Dog Food By City">Dog Food By City</a></td>
<td ><a href="/dogs-for-adoption/"  title="Dogs for adoption">Dogs For Adoption</a></td>
<td ><a href="/puppies-for-adoption/"  title="Puppies for adoption">Puppies for Adoption</a></td>
<!--<td ><a href="/dogs-for-adoption-by-city/"  title="Dogs For Adoption By City">Dogs For Adoption By City</a></td>-->
<td ><a href="/puppies-for-adoption-by-city/"  title="Puppies For Adoption By City">Puppies For Adoption By City</a></td>

</tr>
<tr>

<!--<td ><a href="/breed-puppies-for-adoption/"  title="Puppies Breeds for adoption  with city">Puppies Breeds For Adoption By City</a></td>-->
<!--<td ><a href="/breed-for-adoption/"  title="Breeds for adoption  with city">Breeds For Adoption  By City</a></td>-->
<td ><a href="/search-breed-for-adoption/"  title="Breeds for adoption">Breeds For Adoption</a></td>
<td ><a href="/search-breed-dogs-for-adoption/"  title="Dogs Breeds for adoption">Dogs Breeds For Adoption</a></td>
<td ><a href="/search-breed-puppies-for-adoption/"  title="Puppies Breeds for adoption">Puppies Breeds For Adoption</a></td>
<td ><a href="/cats-for-adoption/"  title="Cats for adoption">Cats For Adoption</a></td>
<td ><a href="/search-cats-for-adoption/"  title="Cats for adoption with city">Cats For Adoption By City</a></td>
</tr><tr>

<!--<td ><a href="/breed-dog-for-adoption/"  title="Dogs Breeds for adoption with city">Dogs Breeds For Adoption By City</a></td>-->
<td ><a href="/adoption-for-city/"  title="Adoption By City">Adoption By City</a></td>
<td ><a href="/dog-names/"  title="Dog Names">Dog Names</a></td>
</tr>

      </thead></thead></table>
      
      
      
       
      
      
      
      
     
    	</div> <!-- main container -->
<?php require_once($DOCUMENT_ROOT.'/new/common/bottom.php'); ?> 