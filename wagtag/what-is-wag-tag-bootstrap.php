<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection = "wag-tag";
$title="What is Wag Tag | Dog Tag | DogSpot.in";
$keyword="What is Wag Tag, Dog Tag, Wag Tag Information, Full Detail On what is Wag Tag";
$desc="What is Wag Tag, Dog Tag, know full information about Wag Tag";
 
    $alternate="https://www.dogspot.in/wagtag/what-is-wag-tag-bootstrap.php";
	$canonical="http://m.dogspot.in/wagpage.php";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='wag-tag';
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css" rel="stylesheet" href="/bootstrap/css/wagtag.css?=version1">
<section class="wagtag-section">
  <div class="container-fluid wagtag-nav">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-9">
          <nav class="navbar">
            <ul class="nav navbar-nav">
              <li><a href="/wagtag-info/">Home</a></li>
              <li><a href="/wagtag/what-is-wag-tag-bootstrap.php">What Is Wagtag?</a></li>
              <li><a href="/wagtag/how-it-works-bootstrap.php">How It works?</a></li>
              <li><a href="/wagtag/faq-bootstrap.php" class="wt_last">Faq</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 text-right">
          <div class="wagtagact-btn-sec">
            <? if($userid !='Guest') { ?>
            <? if($dog_count=='1'){ ?>
            <a href="https://www.dogspot.in/dogs/<?=$sel_dog_details['dog_nicename'] ?>/">Activate your tag</a>
            <? }else if($dog_count > '1'){ ?>
            <a href="https://www.dogspot.in/dogs/owner/<?=$userid ?>/">
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? }else{ ?>
            <a href="https://www.dogspot.in/wag_club/add_dog.php" >
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? }?>
            <? }else{ ?>
            <a href="#modal" id="modal_trigger2" class="bharat" >
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? } ?>
          </div>
        </div>
      </div>
    </div>
  </div>



<div class="container">
<div class="row" style="margin-top:30px">
<div class="col-xs-12 col-sm-6 col-md-4 text-center">
<div>
  <img src="/wagtag/images/wt-logo.png" width="279" height="250" alt="" class="img-responsive">
</div>

</div>
<div class="col-xs-12 col-sm-6 col-md-8 text-center">
<div class="wt_whatImg"><img src="/wagtag/images/wag-id-banner.jpg" width="581" height="281" alt="" class="img-responsive" style="margin:auto"></div>
</div>

</div>

<div class="row">
<div class="col-sm-12">
<div class="wt_infoBox">
<h1>What is Wag Tag?</h1>
<p>wag tag is a high grade stainless steel tag that can be easily attached to your pet’s collar and will ensure safety of your pets. Giving you respite from sleepless nights.</p>
<p>The wag tag helps you to not only get your pet back but maintains confidentiality for the pet owner. Each of the tag comes with a unique code, if your pet gets lost the finder will call the number given on the tag and it will be connected to DogSpot. Our customer care will in turn connect the call to the rightful owner. </p>
</div>

 <div class="wt_howBox">
<h2> Information on your tag</h2>
<p>The tag will contain the following details that will help the pet to be reunited with its owners</p>
<div class="wt_fillInfo">
<ul>
<li>The tag will contain information of the owner, such as his number, name and address though this is not visible to the owner.</li>
<li>In addition it contains details of the pet such as medical information such as allergies or any specific medications.</li>
<li>Vaccination details of the lost pet</li>
<li> Dietary requirements of the pet in case any special requirements are there</li>
<li>It will also give information if the pet is spayed or natured and a description of his behavior</li>
</ul>
</div>
</div>
<div class="wt_howBox">
<h2>How will it help to get your pet back?</h2>
<div class="wt_whatTxt">
<ul style="float:left">
<li>Every dog is allocated a unique id eliminating chances of any errors</li>
<li>The owners number is kept confidential, the call is routed to the owner through DogSpot customer care</li>
<li>The tag is transferred when the owner is changed</li>
<li>wagtag can be easily activated by logging on to DogSpot or through the customer service IVR</li>
<li> Pet owners can update the information as and when required</li>
</ul>
</div>
<div class="wt_howBox">
<h2>What to do when you lose a tag?</h2>
<p>You will have to place an order for another tag which will have a unique identification number that will be different from the previous one. The moment the new tag is generated the older tag will get deactivated. Once you receive the new tag update the information on your pet's profile</p>
</div>
<div class="wt_howBox">
<h2>What happens when the owner is changed?</h2>
<p>The wag tag comes with a unique transfer feature. If you give your dog to someone; then you can also transfer the wag tag to him or her. You just have to log on the profile page of your pet and select the Transfer your tag option.</p>
</div>
<div class="wt_howBox">
<h2>Comparison</h2>
<table class="wt_paraBox table">
<tr>
<th>Features</th>
<th>Wag tag</th>
<th>Traditional tag</th>
</tr>
<tr>
<td>Privacy of information</td>
<td>yes</td>
<td>no</td>
</tr>
<tr>
<td>Visibility</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td>Scanning without smartphone</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td>Value for money</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td>Dietary information</td>
<td>yes</td>
<td>no</td>
</tr>
<tr>
<td>Medical information</td>
<td>yes</td>
<td>no</td>
</tr>
<tr>
<td>Updation of information </td>
<td>yes</td>
<td>no</td>
</tr>
</table>
</div>
</div>
</div>
</div>
</div>
</section>
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');?>