<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection = "wag-tag";
?>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wagtag/css/wt_style.css" />
<title>How Wag Tag Works | Dog Tag | DogSpot.in</title>
<meta name="keywords" content="How Wag Tag Works, Dog Tag, Wag Tag Information, Full Detail On Wag Tag" />
<meta name="description" content="How Wag Tag Works Dog Tag, Full Information on Wag Tag working" />
<link rel="canonical" href="https://www.dogspot.in/wagtag/how-it-works.php" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.dogspot.in/how_to_activate.php" />

<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>

<div class="wc_headerBar">
<div class="cont980">
<div class="wt_topNav">
<ul>
<li><a href="/wagtag-info/">HOME</a></li>
<li><a href="/wagtag/what-is-wag-tag.php">WHAT IS WAGTAG?</a></li>
<li><a href="/wagtag/how-it-works.php">HOW IT WORKS?</a></li>

<li><a href="/wagtag/faq.php" class="wt_last">FAQ</a></li>
</ul>
</div>


</div>
</div>

<div class="">
<div class="wt_cycle wt_whatBox">
<h1 style="    text-align: center;
    margin-top: 0;
    font-size: 26px;
    background: #555;
    color: #fff;
    padding: 10px 0">How it works?</h1>
<div class="cont980"><img src="/wagtag/images/tag-cycle.jpg" alt="how-it-works" title="how-it-works"></div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>