<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection = "wag-tag";
?>
<?php
require_once($DOCUMENT_ROOT . '/new/common/shop-new-css.php');
require_once($DOCUMENT_ROOT . '/new/common/shop-new-js.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>What is Wag Tag | Dog Tag | DogSpot.in</title>
<meta name="keywords" content="What is Wag Tag, Dog Tag, Wag Tag Information, Full Detail On what is Wag Tag" />
<meta name="description" content="What is Wag Tag, Dog Tag, know full information about Wag Tag" />
<link rel="canonical" href="https://www.dogspot.in/wagtag/what-is-wag-tag.php" />
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.dogspot.in/wagpage.php" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<link rel="stylesheet" href="/wagtag/css/style.css?=v2889">
<link rel="stylesheet" href="fonts.css" type="text/css" charset="utf-8" />
<script type="text/javascript">
function showhideques(quesid,action1,action2){
$(".wt_ansTxt").css("display","none");
$(".wt_quesHide").css("display","none");
$(".wt_quesShow").css("display","block");
$("#"+action1).css("display","none");
$("#"+action2).css("display","block");
$("#"+quesid).css("display","block");
}
function showhideques1(quesid,action1,action2){
$("#"+action1).css("display","none");
$("#"+action2).css("display","block");
$("#"+quesid).css("display","none");
}
</script>
<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>
<section class="wagtag-section">	
    <div class="container-fluid" style="padding:0;position: relative;">
			<img src="images/wagtag-banner.png" class="img-responsive hidden-xs tag-icon-main">
			<img src="images/wagtag-banner-1.png" class="img-responsive visible-xs tag-icon-mo">
			<img src="images/tag-icon.png" class="img-responsive hidden-xs tag-icon">
		</div>
        <div class="main-tag-menu">
			<ul class="tag-menu hidden-xs">
				<li><a style="font-weight:600;" href="https://www.dogspot.in/wagtag/index-new.php"  >Activate WAGTAG</a></li>
				<li><a  href="https://www.dogspot.in/wagtag/what-is-wag-tag-new.php">What is WAGTAG?</a></li>
				<li><a  href="https://www.dogspot.in/wagtag/how-it-works-new.php" >How does it work?</a></li>
				<li><a class="active" href="https://www.dogspot.in/wagtag/faq-new.php" >FAQs</a></li>
                <li><a href="/wag_club/" >Wag Club</a></li>
			</ul>
			
			<ul class="tag-menu-mo visible-xs">
				<li><a href="#"  >Activate WAGTAG</a></li>
				<li><a href="https://www.dogspot.in/wagtag/what-is-wag-tag.php"  >What is WAGTAG?</a></li>
				<li><a href="https://www.dogspot.in/wagtag/how-it-works.php"  >How does it work?</a></li>
				<li><a href="https://www.dogspot.in/wagtag/faq.php"  class="active">FAQs</a></li>
                <li style="border-right:none;"><a href="/wag_club/" >Wag Club</a></li>
			</ul>
		</div>
        
        <div class="container" style="margin-top: 0px;">
      <div class="row">
      <div class="col-sm-12">
<div class="wt_faqSec" style="margin-bottom:20px;">
<h1 style="    margin-top: 0px">faq</h1>
<ul class="wt_faqBox">
  <li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques1" onclick="showhideques('ques1','plus_ques1','minus_ques1')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques1" onclick="showhideques1('ques1','minus_ques1','plus_ques1')" style="cursor:pointer;display:none">-</div>
<label>Why does my dog need a wag tag?</label>
</div>
<div class="wt_ansTxt" id="ques1" style="display:none;"> 
Every dog needs a wag tag to ensure its safety. A wag tag will help your pet to reunite with you in case of any unfortunate and unforeseen circumstances. The unique id on the tag will help the finder to contact you while maintaining confidentiality of your information. The call will be connected to DogSpot and they will reroute it to the rightful owner.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques2" onclick="showhideques('ques2','plus_ques2','minus_ques2')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques2" onclick="showhideques1('ques2','minus_ques2','plus_ques2')" style="cursor:pointer;display:none">-</div>
<label>How much does it cost?</label>
</div>
<div class="wt_ansTxt" id="ques2" style="display:none">
 The wag tag is worth 300 INR but you can avail it now at rupees 125 by as an introductory offer.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques3" onclick="showhideques('ques3','plus_ques3','minus_ques3')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques3" onclick="showhideques1('ques3','minus_ques3','plus_ques3')" style="cursor:pointer;display:none">-</div>
<label>Do you charge any monthly fees for wag tag?</label>
</div>
<div class="wt_ansTxt" id="ques3" style="display:none">
No, there is no fee charged for using wag tag. There are no hidden subscriptions involved with this product.
</div>
</li>

<?php /*?><li>
<div class="wt_queTxt">
<div class="wt_quesShow">+</div>
<div class="wt_quesHide">-</div>
<label>I want more than one tag?</label>
</div>
<div class="wt_ansTxt" id="ques4">
If you want more than one tag, then you will have to create individual profiles for each of your pet. Then you can generate wag id for each by placing a request.
</div>
</li><?php */?>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques4" onclick="showhideques('ques4','plus_ques4','minus_ques4')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques4" onclick="showhideques1('ques4','minus_ques4','plus_ques4')" style="cursor:pointer;display:none">-</div>
<label>I have damaged the activation code that came with the product. What shall I do next?</label>
</div>
<div class="wt_ansTxt" id="ques4" style="display:none">
Every wag tag is unique in its way. The tag will be activated only with the activation code provided in the pack. If you have accidentally damaged the code then just call our customer service number and they will help you.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques5" onclick="showhideques('ques5','plus_ques5','minus_ques5')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques5" onclick="showhideques1('ques5','minus_ques5','plus_ques5')" style="cursor:pointer;display:none">-</div>
<label>How secure is your data?</label>
</div>
<div class="wt_ansTxt" id="ques5" style="display:none">
Your data is absolutely secure and no one will be able to accsess it. In fact to ensure privacy and confidentiality of the owner the number is also not displayed on the tag. The call is routed to the rightful owner through DogSpot.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques6" onclick="showhideques('ques6','plus_ques6','minus_ques6')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques6" onclick="showhideques1('ques6','minus_ques6','plus_ques6')" style="cursor:pointer;display:none">-</div>
<label> How will the finder contact me?</label>
</div>
<div class="wt_ansTxt" id="ques6" style="display:none">
The finder will call the number given on the tag and give in the unique wag tag id given on the tag and the call will be routed to the rightful owner.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques7" onclick="showhideques('ques7','plus_ques7','minus_ques7')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques7" onclick="showhideques1('ques7','minus_ques7','plus_ques7')" style="cursor:pointer;display:none">-</div>
<label>Is it an alternative to micro chipping?</label>
</div>
<div class="wt_ansTxt" id="ques7" style="display:none">
No, micro chipping and the tag are two different things. A wag tag allows the finder to contact you instantly, whereas in a microchip the individual needs a hand scanner that can scan the information on the microchip. Your pet will require a surgery for a microchip, whereas the tag can be easily attached to any of the commercially available dog collars.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques8" onclick="showhideques('ques8','plus_ques8','minus_ques8')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques8" onclick="showhideques1('ques8','minus_ques8','plus_ques8')" style="cursor:pointer;display:none">-</div>
<label> I have a tag but do not have a profile on Wag Club?</label>
</div>
<div class="wt_ansTxt" id="ques8" style="display:none">
To activate your wag tag it is absolutely integral to have a profile of your dog on the wag club. Otherwise you cannot activate the tag.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques9" onclick="showhideques('ques9','plus_ques9','minus_ques9')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques9" onclick="showhideques1('ques9','minus_ques9','plus_ques9')" style="cursor:pointer;display:none">-</div>
<label>Benefits over Traditional tag?</label>
</div>
<div class="wt_ansTxt" id="ques9" style="display:none">
A traditional tag will display your information on the tag itself, whereas the wag tag will maintain the privacy for the owner and route the call through DogSpot. wag tag allows you to store your pet’s vital information and if required allows you to update it as well.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques10" onclick="showhideques('ques10','plus_ques10','minus_ques10')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques10" onclick="showhideques1('ques10','minus_ques10','plus_ques10')" style="cursor:pointer;display:none">-</div>
<label>How do I activate my tag?</label>
</div>
<div class="wt_ansTxt" id="ques10" style="display:none">
The tag is activated with the help of an activation code provided at the back of the product. You just need to create a profile for your dog. There is an activate tab on the page that has to be clicked  and you just have to enter the activation code provided in the pack and the tag is activated with all the required information.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques11" onclick="showhideques('ques11','plus_ques11','minus_ques11')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques11" onclick="showhideques1('ques11','minus_ques11','plus_ques11')" style="cursor:pointer;display:none">-</div>
<label>What happens if my dog losses my tag?</label>
</div>
<div class="wt_ansTxt" id="ques11" style="display:none">
if accidentally the tag is lost, you can always buy a new tag. The moment you activate the new tag, the older tag will be de-activated.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques12" onclick="showhideques('ques12','plus_ques12','minus_ques12')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques12" onclick="showhideques1('ques12','minus_ques12','plus_ques12')" style="cursor:pointer;display:none">-</div>
<label>How many numbers can I add in my wag tag?</label>
</div>
<div class="wt_ansTxt" id="ques12" style="display:none">
You can add up to three numbers on your wag tag profile.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques13" onclick="showhideques('ques13','plus_ques13','minus_ques13')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques13" onclick="showhideques1('ques13','minus_ques13','plus_ques13')" style="cursor:pointer;display:none">-</div>
<label> Are there any sizes to chose from?</label>
</div>
<div class="wt_ansTxt" id="ques13" style="display:none">
No there is a standard size for all your dogs, which suits and fits all.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques14" onclick="showhideques('ques14','plus_ques14','minus_ques14')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques14" onclick="showhideques1('ques14','minus_ques14','plus_ques14')" style="cursor:pointer;display:none">-</div>
<label>My friends wants a tag, can I give them mine?</label>
</div>
<div class="wt_ansTxt" id="ques14" style="display:none">
 No you cannot share or give your tag to anyone else as the tag has been generated with your personal details and your pets. If your friend wants one then he or she has to buy a new wag tag.
</div>
</li>

<li>
<div class="wt_queTxt">
<div class="wt_quesShow" id="plus_ques15" onclick="showhideques('ques15','plus_ques15','minus_ques15')" style="cursor:pointer">+</div>
<div class="wt_quesHide" id="minus_ques15" onclick="showhideques1('ques15','minus_ques15','plus_ques15')" style="cursor:pointer;display:none" >-</div>
<label> What is the tag made of and how durable is it?</label>
</div>
<div class="wt_ansTxt" id="ques15" style="display:none">
The tag is made of durable quality stainless steel.
</div>
</li>

</ul>
</div>
</div>
</div>
</div>
        
        
        
        
        </section>
        





<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>