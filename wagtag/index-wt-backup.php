<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wagtag/css/wt_style.css" />
<title>Wag Tag Information | Dog Tag | Full Detail | DogSpot.in</title>
<meta name="keywords" content="Wag Tag Information,Full Detail On Wag Tag, Dog Tag" />
<meta name="description" content="Wag Tag, Dog Tag Full Information on Wag Tag like,A unique id for every dog,Activate it easily through your phone or Internet" />

<?php //require_once($DOCUMENT_ROOT . '/new/common/header.php');?>

<div class="wc_headerBar">
<div class="cont980">
<div class="wt_topNav">
<ul>
<li><a href="/wag-tag/index.php/">HOME</a></li>
<li><a href="/wag-tag/what-is-wag-tag.php/">WHAT IS WAGTAG?</a></li>
<li><a href="#">HOW IT WORKS?</a></li>
<li><a href="#">SAMPLE PROFILE</a></li>
<li><a href="/wag-tag/faq.php/" class="wt_last">FAQ</a></li>
</ul>
</div>

<div class="wt_search">
<div class="wc_search " style="width: 200px;margin: 6px 5px 0 10px;">
<input id="search_name" type="text" placeholder="search by wag id">
</div>
<div class="wc_srchbtn" onclick="keyworddog()"><img src="/wag_club/images/search.png" width="16" height="16"></div>
</div>
</div>
</div>

<div class="cont980">
<div class="wt_topSec">
<div class="wt_logo">
  <img src="/wag-tag/images/wt-logo.png" width="279" height="250" alt="" />
</div>
<div class="wt_pupSec">
  <img src="/wag-tag/images/pup.png" width="200" height="336" alt="" />
</div>
<div class="wt_Story">
<!--<div class="wt_leftArw"><img src="images/left-arw.png" width="61" height="64" alt="" /></div>-->
<div class="wt_reaBox">
<div class="wt_stryBox">
<h2>A boon for your pets</h2>
<div class="wt_pointsBox">
<ul>
<li>Your pet always finds his or her way back home</li>
<li>Unique id for every pet</li>
<li>Maintains confidentiality of owner information</li>
<li>Finder calls and the number to reunite the pet with the owner</li>
<li>Pet and owner information can be update any time</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="wt_midSec">
<div class="wt_leftMid">
<iframe width="560" height="315" src="//www.youtube.com/embed/sn2qepWagbc" frameborder="0" allowfullscreen></iframe>
</div>
<div class="wt_rytMid">
<div class="wt_grnStrip">
</div>
<div class="wt_pinkStrip">
Watch how it works?
</div>
</div>
</div>

</div>

<div class="wt_bottomSec">
<div class="cont980">
<div class="wt_tabs">
<div class="wt_tab_box">
<div class="wt_tabHead"><h2>Get your pet its very own Wag Tag</h2></div>
<div class="wt_tabImg">
  <img src="/wag-tag/images/get-a-tag.jpg" width="180" height="215" alt="" />
</div>
<div class="wt_tabTxt">
Get a Wagtag for your pet by creating his or her very own profile on Wag Club
</div>
<div class="wt_tabBtn">
<a href="#">Click Here</a>
</div>
</div>
<div class="wt_tabSpace"></div>
<div class="wt_tab_box ">
<div class="wt_tabHead"><h2>Have a Wagtag?</h2></div>
<div class="wt_tabImg">
  <img src="/wag-tag/images/activate.jpg" width="180" height="215" alt="" />
</div>
<div class="wt_tabTxt">
To activate your Wagtag you have to sign up your dog on Wag Club
</div>
<div class="wt_tabBtn">
<a href="#">Click Here</a>
</div>
</div>
<div class="wt_tabSpace"></div>
<div class="wt_tab_box">
<div class="wt_tabHead"><h2>Found a pet</h2></div>
<div class="wt_tabImg">
  <img src="/wag-tag/images/finder.jpg" width="180" height="215" alt="" />
</div>
<div class="wt_tabTxt">
Help the pet connect with his or her owner
</div>
<div class="wt_tabBtn">
<a href="#">Click Here</a>
</div>
</div>
</div>
</div>
</div>

<div class="dog_slider">

<div class="left_slider_dg">
<h1 style="font-size: 36px;margin-bottom: 10px">Got your Tag?</h1>
<p>Activate your wagtag through the activation code given at the back of the product</p>
</div>
<div class="ryt_text_dg"><a href="/wag_club/add_dog.php">
<div class="ad_dg_btn wt_actBtn">ACTIVATE TAG</div></a>
</div>
</div>

<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>