<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/wag_club/css/dg_style.css" />
<link rel="stylesheet" href="/wagtag/css/wt_style.css" />
<title>Lost your Wag tag? | Dog Tag | DogSpot.in</title>
<meta name="keywords" content="Lost your Wag tag? | Dog Tag | DogSpot.in" />
<meta name="description" content="Lost your Wag Rag? know how to report and get new Wag Tag | Dog Tag | DogSpot.in" />

<?php require_once($DOCUMENT_ROOT . '/new/common/header.php');?>

<div class="wc_headerBar">
<div class="cont980">
<div class="wt_topNav">
<ul>
<li><a href="/wagtag/index-wt.php/">HOME</a></li>
<li><a href="/wagtag/what-is-wag-tag.php/">WHAT IS WAGTAG?</a></li>
<li><a href="/wagtag/how-it-works.php">HOW IT WORKS?</a></li>
<li><a href="/wagtag/lost-transfer.php">Lost/Transder</a></li>
<?php /*?><li><a href="#">SAMPLE PROFILE</a></li><?php */?>
<li><a href="/wagtag/faq.php/" class="wt_last">FAQ</a></li>
</ul>
</div>

<?php /*?><div class="wt_search">
<div class="wc_search " style="width: 200px;margin: 6px 5px 0 10px;">
<input id="search_name" type="text" placeholder="search by wag id">
</div>
<div class="wc_srchbtn" onclick="keyworddog()"><img src="/wagtag/images/search.png" width="16" height="16"></div>
</div><?php */?>
</div>
</div>

<div class="cont980">
<div class="wt_lostBox">
<h2>Lost your tag?</h2>
<div class="wt_lostTagline">No need to worry,just log on to DogSpot.in an follow these simple steps</div>
<div class="wt_lostStep">

</div>
</div>
</div>
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom.php');?>