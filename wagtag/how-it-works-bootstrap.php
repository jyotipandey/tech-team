<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection = "wag-tag";
$title="How Wag Tag Works | Dog Tag | DogSpot.in";
$keyword="How Wag Tag Works, Dog Tag, Wag Tag Information, Full Detail On Wag Tag";
$desc="How Wag Tag Works Dog Tag, Full Information on Wag Tag working";
 
    $alternate="https://www.dogspot.in/wagtag/how-it-works-bootstrap.php";
	$canonical="http://m.dogspot.in/how_to_activate.php";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='wag-tag';
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css" rel="stylesheet" href="/bootstrap/css/wagtag.css?=version1">





<section class="wagtag-section">
<div class="container-fluid wagtag-nav">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-9">
          <nav class="navbar">
            <ul class="nav navbar-nav">
              <li><a href="/wagtag-info/">Home</a></li>
              <li><a href="/wagtag/what-is-wag-tag-bootstrap.php">What Is Wagtag?</a></li>
              <li><a href="/wagtag/how-it-works-bootstrap.php">How It works?</a></li>
              <li><a href="/wagtag/faq-bootstrap.php" class="wt_last">Faq</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 text-right">
          <div class="wagtagact-btn-sec">
            <? if($userid !='Guest') { ?>
            <? if($dog_count=='1'){ ?>
            <a href="https://www.dogspot.in/dogs/<?=$sel_dog_details['dog_nicename'] ?>/">Activate your tag</a>
            <? }else if($dog_count > '1'){ ?>
            <a href="https://www.dogspot.in/dogs/owner/<?=$userid ?>/">
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? }else{ ?>
            <a href="https://www.dogspot.in/wag_club/add_dog.php" >
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? }?>
            <? }else{ ?>
            <a href="#modal" id="modal_trigger2" class="bharat" >
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="container">

<h1 style="text-align: center;
    font-size: 26px;
    color: #6c9d06;
    padding: 15px 0">How it works?</h1>
<img src="/wagtag/images/tag-cycle.jpg" alt="how-it-works" title="how-it-works" class="img-responsive" style="margin:auto">
</div>
</section>
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');?>