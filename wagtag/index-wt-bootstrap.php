<?php
require_once($DOCUMENT_ROOT.'/constants.php');
require_once($DOCUMENT_ROOT.'/session.php');
require_once($DOCUMENT_ROOT.'/database.php');
require_once($DOCUMENT_ROOT.'/functions.php');
require_once($DOCUMENT_ROOT.'/shop/functions.php');
require_once($DOCUMENT_ROOT.'/shop/arrays/shop-categorys.php');
require_once($DOCUMENT_ROOT.'/arrays.php');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$sitesection = "wag-tag";

if($temp){
$sel_dogs=mysql_query("SELECT * FROM dogs_available WHERE userid='$userid'");
$dog_count=mysql_num_rows($sel_dogs);
if($dog_count=='0'){
	header("Location: https://www.dogspot.in/wag_club/add_dog.php"); 
	exit();
}
if($dog_count=='1'){
	$sel_dog_details=query_execute_row("SELECT dog_nicename FROM dogs_available WHERE userid='$userid' AND publish_status='publish'");
	header("Location: https://www.dogspot.in/dogs/".$sel_dog_details['dog_nicename']); 
	exit();
}
if($dog_count > '1'){
	header("Location: https://www.dogspot.in/dogs/owner/".$userid."/?wagtag=1"); 
	exit();
}
}
if($userid !='Guest') { 
$sel_dogs=mysql_query("SELECT * FROM dogs_available WHERE userid='$userid'");
$dog_count=mysql_num_rows($sel_dogs);
if($dog_count=='1'){
	$sel_dog_details=query_execute_row("SELECT dog_nicename FROM dogs_available WHERE userid='$userid'");
}
}
$title="Wag Tag Information | Dog Tag | Full Detail | DogSpot.in";
$keyword="Wag Tag Information,Full Detail On Wag Tag, Dog Tag, DogSpot Wag Tag";
$desc="Wag Tag, Dog Tag Full Information on Wag Tag like,A unique id for every dog,Activate it easily through your phone or Internet";
 
    $alternate="https://m.dogspot.in/wagtag-info/";
	$canonical="https://www.dogspot.in/wagtag-info/";
	$og_url=$canonical;
	$imgURLAbs="";
	$page_type='wag-tag';
	
require_once($DOCUMENT_ROOT . '/new/common/header-bootstrap.php');?>
<link type="text/css" rel="stylesheet" href="/bootstrap/css/wagtag.css?=version5">
<script type="text/javascript">
function know_more(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top},
        'slow');
}
</script>
<section class="wagtag-section">
  <div class="container-fluid wagtag-nav">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-9">
          <nav class="navbar">
            <ul class="nav navbar-nav">
              <li><a href="/wagtag-info/">Home</a></li>
              <li><a href="/wagtag/what-is-wag-tag-bootstrap.php">What Is Wagtag?</a></li>
              <li><a href="/wagtag/how-it-works-bootstrap.php">How It works?</a></li>
              <li><a href="/wagtag/faq-bootstrap.php" class="wt_last">Faq</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 text-right">
          <div class="wagtagact-btn-sec">
            <? if($userid !='Guest') { ?>
            <? if($dog_count=='1'){ ?>
            <a href="https://www.dogspot.in/dogs/<?=$sel_dog_details['dog_nicename'] ?>/">Activate your tag</a>
            <? }else if($dog_count > '1'){ ?>
            <a href="https://www.dogspot.in/dogs/owner/<?=$userid ?>/">
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? }else{ ?>
            <a href="https://www.dogspot.in/wag_club/add_dog.php" >
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? }?>
            <? }else{ ?>
            <a href="#modal" id="modal_trigger2" class="bharat" >
            <div class="wagtagact-btn">Activate your tag</div>
            </a>
            <? } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="userid" id="userid" value="<?=$userid?>" />
  <div class="container">
    <div class="row" >
      <div class="col-xs-12 col-sm-4 col-md-2 text-center">
        <div class="wt_pupSec"> <img src="/wagtag/images/pup.png" width="200" height="336" alt="dog-image" title="dog-image" class="img-responsive"  /></div>
      </div>
      <div class="col-xs-12 col-sm-5 col-md-7"> 
        <!--<div class="wt_leftArw"><img src="/wagtag/images/left-arw.png" width="61" height="64" alt="" /></div>-->
        
        <div class="wt_stryBox">
          <h1>Wag tag vs Traditional tag</h1>
          <table class="table">
            <tr>
              <th>Features</th>
              <th>Wag tag
                <?php /*?><div class="wt_paraImg headImg_wt"><img src="/wagtag/images/tag-back.png" width="20"></div><?php */?>
              </th>
              <th>Traditional tag</th>
            </tr>
            <tr>
              <td><div class="wt_paraImg"><img src="/wagtag/images/lock-24.png" width="20" alt="lock-image" title="lock-image"></div>
                <div class="wt_paraHead">Privacy of information</div></td>
              <td><div class="wt_wagPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/tick-32.png" width="" style="width: 26px;" alt="tick-image" title="tick-image"></div>
                </div></td>
              <td><div class="wt_tradPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/cross-32.png" width="" style="width: 26px;" alt="cross-image" title="cross-image"></div>
                </div></td>
            </tr>
            <tr>
              <td><div class="wt_paraImg"><img src="/wagtag/images/money_bag.png" width="20" alt="money-bag-image" title="money-bag-image"></div>
                <div class="wt_paraHead">Value for money</div></td>
              <td><div class="wt_wagPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/tick-32.png" width="" style="width: 26px;" alt="tick-image" title="tick-image"></div>
                </div></td>
              <td><div class="wt_tradPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/tick-32.png" width="" style="width: 26px;" alt="tick-image" title="tick-image"></div>
                </div></td>
            </tr>
            <tr>
              <td><div class="wt_paraImg"><img src="/wagtag/images/diet-24.png" width="20" alt="diet-image" title="diet-image"></div>
                <div class="wt_paraHead">Dietary information</div></td>
              <td><div class="wt_wagPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/tick-32.png" width="" style="width: 26px;" alt="tick-image" title="tick-image"></div>
                </div></td>
              <td><div class="wt_tradPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/cross-32.png" width="" style="width: 26px;" alt="cross-image" title="cross-image"></div>
                </div></td>
            </tr>
            <tr>
              <td><div class="wt_paraImg"><img src="/wagtag/images/first-aid-24.png" width="20" alt="first-aid-image" title="first-aid-image"></div>
                <div class="wt_paraHead">Medical information</div></td>
              <td><div class="wt_wagPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/tick-32.png" width="" style="width: 26px;"  alt="tick-image" title="tick-image"></div>
                </div></td>
              <td><div class="wt_tradPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/cross-32.png" width="" style="width: 26px;" alt="cross-image" title="cross-image"></div>
                </div></td>
            </tr>
            <tr>
              <td><div class="wt_paraImg"><img src="/wagtag/images/update-24.png" width="20" alt="update-image" title="update-image"></div>
                <div class="wt_paraHead">Update information </div></td>
              <td><div class="wt_wagPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/tick-32.png" width="" style="width: 26px;" alt="tick-image" title="tick-image"></div>
                </div></td>
              <td><div class="wt_tradPara">
                  <div class="wt_paraImg1"><img src="/wagtag/images/cross-32.png" width="" style="width: 26px;" alt="cross-image" title="cross-image"></div>
                </div></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 text-center">
        <div class="wt_logo"> <img src="/wagtag/images/wt-logo.png" width="279" height="250" alt="wag-tag-logo" title="wag-tag-logo" class="img-responsive" /> </div>
      </div>
    </div>
  </div>
  <div class="knwBtnFull_wt">
    <div class="container text-center">
      <div class="button_wt knwBtn_wt" onclick="know_more('div2')" style="cursor:pointer"> Know more <i class="fa fa-play" aria-hidden="true"></i></div>
    </div>
  </div>
  <div class="boon_wt" id="div2">
    <div class="container"  >
      <div class="row" style="margin:0px 5%;">
        <div class="col-md-5">
          <div class="boonTxt_wt">
            <div class="boonCon_wt boontxt_Con_wt1"> A unique id for every dog </div>
            <div class="boonCon_wt boontxt_Con_wt3">
              <div class="boon-box-text">Activate it easily through your phone or Internet</div>
            </div>
          </div>
        </div>
        <div class="col-md-2 visible-lg">
          <h3>BOON <br>
            &nbsp;  FOR<br>
            &nbsp;  &nbsp;  &nbsp; PET<br>
            &nbsp;  &nbsp; &nbsp; &nbsp;&nbsp;  AND<br>
            &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;PET <br>
            &nbsp; &nbsp;&nbsp;&nbsp; OWNERS</h3>
        </div>
        <div class="col-md-5">
          <div class="boonCon_wt boontxt_Con_wt2">
            <div class="boon-box-text_1">Tag can be transferred with the dog</div>
          </div>
          <div class="boonCon_wt boontxt_Con_wt4">
            <div class="boon-box-text_2">Pet owner’s information is kept confidential</div>
          </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12">
          <div class="know-more-wag-btn" onclick="know_more('div3')" style="cursor:pointer">Know more about Wag tag <i class="fa fa-play" aria-hidden="true"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="about-wag-tag"  id="div3">
    <div class="container">
      <div class="row">
        <div class="col-md-5"> <img src="/wagtag/images/wag-tag.jpg" width="432" height="377" alt="Wagtag" title="Wagtag" class="img-responsive"> </div>
        <div class="col-md-7">
          <div class="tagSpec_wt"> Wag tag is a high grade stainless steel tag that will ensure your pet's safety. It is easy to attach to the collar. </div>
        </div>
        <div class="col-sm-12">
          <div class="tagAbt_wt">
            <p>Wag tag will ensure that your pet comes back to you in case of any unfortunate event in which you lose him/ her. The wag tag from DogSpot will help you to reunite with your pet while maintaining confidentiality of the owner’s information. </p>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="button_wt how-to-actiave" onclick="know_more('div4')" style="cursor:pointer">How to Activate <i class="fa fa-play" aria-hidden="true"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="tagActivate_wt">
    <div class="container" id="div4">
      <div class="row">
        <div class="col-md-3 visible-lg"> <img src="/wagtag/images/how_act.png" width="199" height="250" alt="how-to-activate" title="how-to-activate" style="margin-top:60%;"> </div>
        <div class="col-md-9">
          <div class="prodAct_wt"> <img src="/wagtag/images/wag-bannner-n.jpg" width="652" height="581" alt="activate-tag" title="activate-tag" class="img-responsive"></div>
        </div>
        <div class="col-sm-12">
          <div class="button_wt lost-btn" style="cursor:pointer" onclick="know_more('div5')">Lost Pet <i class="fa fa-play" aria-hidden="true"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="lostTag_wt" id="div5">
    <div class="container" >
      <div class="row">
        <div class="col-sm-12 text-center"> <img src="/wagtag/images/lost-pet-banner.jpg" width="934" height="549" alt="lost-tag" title="lost-tag" class="img-responsive"> </div>
        <div class="col-sm-12">
          <div class="button_wt get-wag-tag-btn" style="cursor:pointer" onclick="know_more('div6')">Get your own Tag <i class="fa fa-play" aria-hidden="true"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="buyBox_wt" id="div6">
    <div class="container">
      <div class="row">
        <div>
          <div class="col-sm-12 text-center">
            <h4>INTRODUCING WAG TAG</h4>
            <p>Helping pet owners to reunite with their pets</p>
          </div>
          <div class="col-md-6 text-center">
            <div class="buyImg_wt"><img src="/wagtag/images/product.png" width="280" height="292" alt="product-tag" title="product-tag"> </div>
          </div>
          <div class="col-md-6 text-left">
            <div class="buyPrice_wt">
              <p>Get your pet its very own Wag tag at an <!--introductory--> price of </p>
              <div class="price_wt mrp_wt" style="border:none;"> <i class="fa fa-rupee"></i> 300</div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 text-center"> <a class=" add-to-cart" style="cursor:pointer">
          <div class="buy-now-btn">Buy now <i class="fa fa-play" aria-hidden="true"></i></div>
          </a> </div>
      </div>
    </div>
  </div>
  <div class="wt_activateNow">
    <div class="container">
      <div class="row">
        <div class="actiNowTxt_wt">
          <div class="col-sm-6 text-center">
            <label>Already have a wag tag?</label>
          </div>
          <div class="col-sm-6 text-center">
            <? if($userid !='Guest') { ?>
            <? if($dog_count=='1'){ ?>
            <a class="button_wt actNowBtn_wt" href="https://www.dogspot.in/dogs/<?=$sel_dog_details['dog_nicename'] ?>/" style="cursor:pointer">
            <div class="activate-now-btn"> Activate now <i class="fa fa-play" aria-hidden="true"></i></div>
            </a>
            <? }else if($dog_count > '1'){ ?>
            <a class="button_wt actNowBtn_wt" href="https://www.dogspot.in/dogs/owner/<?=$userid ?>/" style="cursor:pointer">
            <div class="activate-now-btn"> Activate now <i class="fa fa-play" aria-hidden="true"></i></div>
            </a>
            <? }else{ ?>
            <a class="button_wt actNowBtn_wt" href="https://www.dogspot.in/wag_club/add_dog.php" style="cursor:pointer">
            <div class="activate-now-btn"> Activate now <i class="fa fa-play" aria-hidden="true"></i></div>
            </a>
            <? }}else{ ?>
            <a class="button_wt actNowBtn_wt" href="https://www.dogspot.in/login.php?refurl_wag=/wagtaginfolog/" style="cursor:pointer">
            <div class="activate-now-btn"> Activate now <i class="fa fa-play" aria-hidden="true"></i></div>
            </a>
            <? } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
	$('.add-to-cart').on('click',function(e){
		
		var item_id='8253';
		var item_qty='1';
		var user=$('#userid').val();
		ShaAjaxJquary('/new/shop/carts.php?item_id='+item_id+'&item_qty='+item_qty+'&userid='+user+'&session_id=<?=$session_id?>&cart=cart', '#cartItemCountheader', '', '', 'GET', '#cartItemCountheader', '', 'REP');
		window.location.href = "https://www.dogspot.in/new/shop/new-cart.php";
		 e.preventDefault();
	});
});
</script>
<?php require_once($DOCUMENT_ROOT . '/new/common/bottom-bootstrap.php');?>
